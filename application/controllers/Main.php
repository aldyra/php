<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function content($param = '')
	{
		$post = array();
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$Max = sizeof($ExpLing);	
		$link = isset($ExpLing[$Max-1]) ? $ExpLing[$Max-1]:"";
		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		$SiteActive = isset($_SESSION['site_active']) ? $_SESSION['site_active']:$link;

		$GetModule = $this->Module_model->getmodule($link);
		$PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
		$MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";

		// MENU PARENT -1
		$Q_MP = $this->db->query("SELECT * FROM MS_MENU WHERE ID = '$MENU_PARENT'")->row_array();
		if ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) {
			// $MENU_PARENT = ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) ? $Q_MP['MENU_PARENT'] : "";
			$MENU_PARENT = $Q_MP['MENU_PARENT'];
		}

		$IS_CONTENT = isset($GetModule['IS_CONTENT']) ? $GetModule['IS_CONTENT']:"";
		$_SESSION['site_active'] = $link;
		$post['controller'] = $link;

		$FILE_EXIST = $_SERVER['DOCUMENT_ROOT']."bumi_artha/application/controllers/".ucwords(strtolower($link)).".php";

		if(sizeof($GetModule) > 0){
			$ID_MENU = isset($GetModule['ID']) ? $GetModule['ID']:"";
			$Getleftmodule = $this->Module_model->getleftmodule($MENU_PARENT);
			$GetContent = $this->Content_model->getcontent($ID_MENU);
			$post['leftmodule'] = $Getleftmodule;

			$MENU_PARENT = isset($GetModule['PARENT']['NAME']) ? $GetModule['PARENT']['NAME']:"";
			$MENU_PARENT_LINK = isset($GetModule['PARENT']['LINK']) ? $GetModule['PARENT']['LINK']:"";
			$MENU_PARENT_FILE_BANNER = isset($GetModule['PARENT']['FILE_BANNER']) ? $GetModule['PARENT']['FILE_BANNER']:"";
			$MENU_CHILD = isset($GetModule['NAME']) ? $GetModule['NAME']:"";
			$MENU_CHILD_LINK = isset($GetModule['LINK']) ? $GetModule['LINK']:"";
			$MENU_CHILD_FILE_BANNER = isset($GetModule['FILE_BANNER']) ? $GetModule['FILE_BANNER']:"";
			$TITLE_CONTENT = isset($GetContent['TITLE']) ? $GetContent['TITLE']:"";
			$DESCRIPTION = isset($GetContent['DESCRIPTION']) ? $GetContent['DESCRIPTION']:"";

			$content['MENU_PARENT'] = $MENU_PARENT;
			$content['MENU_PARENT_LINK'] = $MENU_PARENT_LINK;
			$content['MENU_PARENT_FILE_BANNER'] = $MENU_PARENT_FILE_BANNER;
			$content['MENU_CHILD'] = $MENU_CHILD;
			$content['MENU_CHILD_LINK'] = $MENU_CHILD_LINK;
			$content['MENU_CHILD_FILE_BANNER'] = $MENU_CHILD_FILE_BANNER;
			$content['TITLE'] = $TITLE_CONTENT;
			$content['DESCRIPTION'] = $DESCRIPTION;
			$content['IS_CONTENT'] = $IS_CONTENT;
			$content['FILE_CONTENT'] = $link;
			$post['content'] = $content;

			if(!empty($IS_CONTENT)){
				$this->data['Content'] = "content";
			}else{
				redirect(base_url().$SiteLang."/".$link."/index");
			}
		}else{
			if($link == ""){
				redirect(base_url().$SiteLang);
			}else{
				$Getfootermodule = $this->Module_model->getfootermodule();
				foreach($Getfootermodule as $footer){
					$LINK_FOOTER = isset($footer['LINK']) ? $footer['LINK']:"";
					if($LINK_FOOTER == $link){
						redirect(base_url().$SiteLang."/".$link."/index");
					}
				}

				if(file_exists($FILE_EXIST)){
					redirect(base_url().$SiteLang."/".$link."/index");
				}else{
					$this->data['Content'] = "notfound";
				}
			}
		}
		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['data'] = $post;
		$this->load->view('template', $this->data);
	}

	public function citykab(){
		$param = isset($_POST['id']) ? $_POST['id']:"";
		$id_city = isset($_POST['id_city']) ? $_POST['id_city']:"";
		$exp = explode("-",$param);
		$id = $exp[0];
		$citykab = $this->Main_model->citykab($id);
	?>
		<select name="citykab" id="citykab" class="form-control font-13">
            <option><?php echo $this->lang->line('cabang-3') ?></option>
            <?php
            foreach($citykab as $row){
            	$CITY_KAB_ID = isset($row['CITY_KAB_ID']) ? $row['CITY_KAB_ID']:"";
            	$CITY_KAB_NAME = isset($row['CITY_KAB_NAME']) ? $row['CITY_KAB_NAME']:"";
            	$DESCRIPTION = isset($row['DESCRIPTION']) ? $row['DESCRIPTION']:"";
            	if($CITY_KAB_ID == $id_city){
            		$Selected = "selected";
            	}else{
            		$Selected = "";
            	}
            ?>
            	<option value="<?php echo $CITY_KAB_ID.'-'.$CITY_KAB_NAME ?>" <?php echo $Selected ?>><?php echo $DESCRIPTION." ".$CITY_KAB_NAME ?></option>
            <?php
            }
            ?>
        </select>
	<?php
	}

	public function ppr()
	{
		$harga = isset($_POST['harga']) ? $_POST['harga']:"";
		$bunga = isset($_POST['bunga']) ? $_POST['bunga']:"";
		$tenor = isset($_POST['tenor']) ? $_POST['tenor']:"";
		$dp = isset($_POST['dp']) ? $_POST['dp']:"";

		$dp_awal = ($harga*$dp)/100;
        $pokok_pinjaman = $harga - $dp_awal;
        $jangka_kredit = $tenor * 12;
        $jumlah_bunga = ($bunga*$pokok_pinjaman/100) * $tenor;

        $anuitas = ($pokok_pinjaman * ($bunga/12/100)) / (1-1/(pow(1+$bunga/12/100, $jangka_kredit)));
        
        $JSON['harga'] = $harga;
        $JSON['dp'] = $dp_awal;
        $JSON['plafond'] = $pokok_pinjaman;
        $JSON['tenor'] = $jangka_kredit;
        $JSON['bunga'] = $bunga;
        $JSON['anuitas'] = $anuitas;

        die(json_encode($JSON));
	}

	public function show_row()
	{
		$ID = isset($_POST['id']) ? $_POST['id']:"";
		$SHOW = isset($_POST['show']) ? $_POST['show']:"";
		$_SESSION['pagination'][$ID] = $SHOW;
		die(json_encode($_SESSION['pagination']));
	}
}
