<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_p extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('modulep_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "module_p/view";
        $data['authorize'] = $this->data['authorize'];

        $this->load->view('templates/view', $data);
    }

    public function get_data()
    {
        header('Content-Type: application/json');

        // $col = "ID,MODULE_NAME,ACTIVE,CREATED_DATE";
        $col = implode(',', $this->input->post('arr', true));
        $res = $this->modulep_model->get_data($col);
        $tes = json_decode($res);
        foreach ($tes->data as $row) {

            if ($row->CREATED_DATE != "0000-00-00" && $row->CREATED_DATE != null) {
                $row->CREATED_DATE = convertDate($row->CREATED_DATE);
            } else {
                $row->CREATED_DATE = "";
            }
            $row->STATUS = $row->ACTIVE == 1 ? "88" : "0";
            $row->ACTIVE = $row->ACTIVE == 1 ? "Active" : "Not Active";

        }

        echo json_encode($tes);
    }

}
