<?php 
$changelang = base_url();
$Host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "https");
$Url = $Host."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

$LanguageActive = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$ExpLang = explode(":", $this->config->item('ID_LANGUAGE'));
?>

<?php 
if($LanguageActive == "en"){
    $Urlppr = base_url().$LanguageActive."/ppr-simulation/index";
    $Urlppm = base_url().$LanguageActive."/ppm-simulation/index";
}elseif($LanguageActive == "id"){
    $Urlppr = base_url().$LanguageActive."/simulasi-ppr/index";
    $Urlppm = base_url().$LanguageActive."/simulasi-ppm/index";
}
?>

<script type="text/javascript">
    $(function(){
        $("nav.shift ul li").hover(function () {
            $(this).find(".dropdown").stop(true, true).delay(100).slideDown(200);
        }, function () {
            $(this).find(".dropdown").stop(true, true).delay(300).slideUp(200);
        });
    });
</script>
<style type="text/css">
    @media only screen and (min-width: 992px){
        .img-logo {
            height: auto !important;
            width: 370px !important;
        }
    }

    nav.shift ul li div.dropdown div.card-dropdown{
        height: 0;
        border: none;
        visibility: hidden;
    }

    nav.shift ul li div.dropdown div.card-dropdown div.card-header{
        display: none;
    }

    nav.shift ul li div.dropdown div.card-dropdown div.card-body{
        display: none;
        transition-duration: 0.4s;
    }

    nav.shift ul li:hover div.dropdown div.card-dropdown{
        height: 450px !important;
        visibility: visible;
        border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    }

    nav.shift ul li:hover div.dropdown div.card-dropdown div.card-header{
        display: inline-block !important;
    }

    nav.shift ul li:hover div.dropdown div.card-dropdown div.card-body{
        display: inline-block !important;
    }
</style>

<script type="text/javascript"> 
</script>

<header>
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 15px;">
    <a class="navbar-brand" href='<?php echo base_url().$LanguageActive."/"?>'>
        <img src="<?php echo base_url('assets/image/logo-bba.png')?>" alt="Bank Bumi Artha" class="img-logo">
    </a>
    <span class="navbar-toggler row-language-mobile" data-toggle="collapse" aria-expanded="false">
        <?php
        for($i=0; $i<sizeof($ExpLang); $i++){
            $Language = isset($ExpLang[$i]) ? $ExpLang[$i]:"";
            if($LanguageActive == strtolower($Language)){
                $textactive = "active";
            }else{
                $textactive = "";
            }
        ?>
            <a href='javascript:void(0)' class="language <?php echo $textactive ?>" onclick="changelang('<?php echo strtolower($Language)?>', '<?php echo $changelang ?>', '<?php echo $Url ?>')"><?php echo $Language ?></a>
            <?php 
            if($i == 0){
            ?>
                &nbsp;<span class="seperator"></span>&nbsp;
            <?php
            }
            ?>
        <?php
        }
        ?>
    </span>
        
    <button class="navbar-toggler" id="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <section class="section-navbar">
            <nav class="shift">
                <ul>
                    <?php
                    $i = 0;
                    foreach($nav_menu as $menu){
                        $NAME = isset($menu['NAME']) ? $menu['NAME']:"";
                        $LINK = isset($menu['LINK']) ? $menu['LINK']:"";
                        $CHILD = isset($menu['CHILD']) ? $menu['CHILD']:array();
                        $MENU_PARENT = isset($menu['MENU_PARENT']) ? $menu['MENU_PARENT']:array();

                        $UrlController = base_url().$LanguageActive."/".$LINK;
                        
                        if(!empty($CHILD)){
                            $UrlActive = "javascript:void(0)";
                        }else{
                            $UrlActive = $UrlController;
                        }
                        ?>
                        <li id="navbarDropdown-<?php echo $i?>" class="navbarDropdown">
                            <a href="<?php echo $UrlActive;?>" style="color: black !important;" class="parent" <?php if(empty($CHILD)){?>id='<?php echo $LINK?>'<?php } ?>><?php echo $NAME ?></a>
                            <?php
                            if(!empty($CHILD)){
                            ?>
                            <div class="row dropdown" id="dropdown-<?php echo $i ?>" style="text-align: left;">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm col-12">
                                    <!-- <div class="card" style="border-radius: 0; width: 100%; height: 450px;"> -->
                                    <div class="card card-dropdown" style="border-radius: 0; width: 100%; height: 0;">
                                        <div class="card-header">
                                            <b style="color: #243E8B !important;"><?php echo $NAME ?></b>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm col-12 d-padding">
                                                    <?php 
                                                    $j = 0;
                                                    foreach($CHILD as $submenu){
                                                        $NAME_SUB = isset($submenu['NAME']) ? $submenu['NAME']:"";
                                                        $LINK_SUB = isset($submenu['LINK']) ? $submenu['LINK']:"";
                                                        $CHILD_SUB = isset($submenu['CHILD']) ? $submenu['CHILD']:array();
                                                        $UrlControllerSub = base_url().$LanguageActive."/".$LINK_SUB;

                                                        if(!empty($CHILD_SUB)){
                                                            $UrlControllerSub = "javascript:void(0)";
                                                        }else{
                                                            $UrlControllerSub = $UrlControllerSub;
                                                        }
                                                    ?>
                                                        <div class="row-child" id="row-child-<?php echo $i.$j?>">
                                                            <a href="<?php echo $UrlControllerSub?>" class="child-1" <?php if(empty($CHILD_SUB)){?>id='<?php echo $LINK_SUB?>'<?php } ?>>
                                                                <?php echo $NAME_SUB ?>
                                                                <?php 
                                                                if(!empty(sizeof($CHILD_SUB))){
                                                                ?>
                                                                <div class="subchild">
                                                                    <i class="fas fa-chevron-right"></i>
                                                                </div>
                                                                <?php } ?>
                                                            </a>
                                                            <?php
                                                            if(!empty($CHILD_SUB)){
                                                            ?>
                                                            <div class="submenu-child">
                                                                <?php 
                                                                if(!empty(sizeof($CHILD_SUB))){
                                                                    $k = 0;
                                                                    foreach($CHILD_SUB as $submenu_child){
                                                                        $NAME_SUB_CHILD = isset($submenu_child['NAME']) ? $submenu_child['NAME']:"";
                                                                        $LINK_SUB_CHILD = isset($submenu_child['LINK']) ? $submenu_child['LINK']:"";
                                                                        $CHILD_SUB_CHILD = isset($submenu_child['CHILD']) ? $submenu_child['CHILD']:array();
                                                                        $UrlControllerSubChild = base_url().$LanguageActive."/".$LINK_SUB_CHILD;
                                                                        if(!empty(sizeof($CHILD_SUB_CHILD))){
                                                                            $UrlActiveSubChild = "javascript:void(0)";
                                                                        }else{
                                                                            $UrlActiveSubChild = $UrlControllerSubChild;
                                                                        }
                                                                ?>
                                                                        <div id="row-child-two-<?php echo $i.$j.$k ?>" class="row-child-two">
                                                                            <a href="<?php echo $UrlActiveSubChild?>" class="child-2" <?php if(empty($CHILD_SUB_CHILD)){?>id='<?php echo $LINK_SUB_CHILD?>'<?php } ?>>
                                                                                <?php echo $NAME_SUB_CHILD ?>
                                                                                <?php 
                                                                                if(!empty(sizeof($CHILD_SUB_CHILD))){
                                                                                ?>
                                                                                <div class="subchild">
                                                                                    <i class="fas fa-chevron-right"></i>
                                                                                </div>
                                                                                <?php } ?>
                                                                            </a>
                                                                            <?php
                                                                            if(!empty($CHILD_SUB_CHILD)){
                                                                            ?>
                                                                                <div class="submenu-child-tree">
                                                                                <?php 
                                                                                    $l = 0;
                                                                                    foreach($CHILD_SUB_CHILD as $submenu_child_2){
                                                                                        $NAME_SUB_CHILD_2 = isset($submenu_child_2['NAME']) ? $submenu_child_2['NAME']:"";
                                                                                        $LINK_SUB_CHILD_2 = isset($submenu_child_2['LINK']) ? $submenu_child_2['LINK']:"";
                                                                                        $CHILD_SUB_CHILD_2 = isset($submenu_child_2['CHILD']) ? $submenu_child_2['CHILD']:array();
                                                                                        $UrlControllerSubChild_2 = base_url().$LanguageActive."/".$LINK_SUB_CHILD_2;
                                                                                        if(!empty($CHILD_SUB_CHILD_2)){
                                                                                            $UrlActiveSubChild_2 = "javascript:void(0)";
                                                                                        }else{
                                                                                            $UrlActiveSubChild_2 = $UrlControllerSubChild_2;
                                                                                        }
                                                                                    ?>
                                                                                        <div id="row-child-tree-<?php echo $i.$j.$k.$l ?>" class="row-child-tree">
                                                                                            <a href="<?php echo $UrlActiveSubChild_2?>" class="child-3" <?php if(empty($CHILD_SUB_CHILD_2)){?>id='<?php echo $LINK_SUB_CHILD_2?>'<?php } ?>>
                                                                                                <?php echo $NAME_SUB_CHILD_2 ?>
                                                                                                <?php
                                                                                                if(!empty(sizeof($CHILD_SUB_CHILD_2))){
                                                                                                ?>
                                                                                                <div class="subchild">
                                                                                                    <i class="fas fa-chevron-right"></i>
                                                                                                </div>
                                                                                                <?php } ?>
                                                                                            </a>
                                                                                            <?php
                                                                                            if(!empty($CHILD_SUB_CHILD_2)){
                                                                                            ?>
                                                                                                <div class="submenu-child-four">
                                                                                                <?php 
                                                                                                    foreach($CHILD_SUB_CHILD_2 as $submenu_child_3){
                                                                                                        $NAME_SUB_CHILD_3 = isset($submenu_child_3['NAME']) ? $submenu_child_3['NAME']:"";
                                                                                                        $LINK_SUB_CHILD_3 = isset($submenu_child_3['LINK']) ? $submenu_child_3['LINK']:"";
                                                                                                        $CHILD_SUB_CHILD_3 = isset($submenu_child_3['CHILD']) ? $submenu_child_3['CHILD']:array();
                                                                                                        $UrlControllerSubChild_3 = base_url().$LanguageActive."/".$LINK_SUB_CHILD_3;
                                                                                                        if(!empty($CHILD_SUB_CHILD_3)){
                                                                                                            $UrlActiveSubChild_3 = "javascript:void(0)";
                                                                                                        }else{
                                                                                                            $UrlActiveSubChild_3 = $UrlControllerSubChild_3;
                                                                                                        }
                                                                                                    ?>
                                                                                                         <a href="<?php echo $UrlActiveSubChild_3?>" class="child-4" <?php if(empty($CHILD_SUB_CHILD_3)){?>id='<?php echo $LINK_SUB_CHILD_3?>'<?php } ?>>
                                                                                                            <?php echo $NAME_SUB_CHILD_3 ?>
                                                                                                            <?php
                                                                                                            if(!empty(sizeof($CHILD_SUB_CHILD_3))){
                                                                                                            ?>
                                                                                                            <div class="subchild">
                                                                                                                <i class="fas fa-chevron-right"></i>
                                                                                                            </div>
                                                                                                            <?php } ?>
                                                                                                        </a>
                                                                                                    <?php
                                                                                                    }
                                                                                                ?>
                                                                                                </div>
                                                                                            <?php 
                                                                                            } 
                                                                                            ?>
                                                                                        </div>
                                                                                    <?php
                                                                                    $l++;
                                                                                    }
                                                                                ?>
                                                                                </div>
                                                                            <?php 
                                                                            } 
                                                                            ?>
                                                                        </div>
                                                                <?php 
                                                                    $k++;
                                                                    } 
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php
                                                        $j++;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </li>
                    <?php
                    $i++;
                    }
                    ?>
                </ul>
            </nav>
        </section> 

        <form action="<?php echo base_url().$LanguageActive.'/'.$this->lang->line('search-0').'/index' ?>" method="get" role="search" novalidate="novalidate" style="position: absolute; bottom: -38px; right: 80px; background-color: white;">
            <div class="input-group" id="group-search" style="display: none; width: 240 !important;">
                 <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-search" style="color: gray;"></i></span>
                </div>
                <input type="search" name="search" id="input-search" class="form-control" placeholder="<?php echo $this->lang->line('nav_search')?>">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="icon-close">
                        <i class="fas fa-times"></i>
                    </span>
                </div>
            </div>
        </form>
        
        <div style="text-align: center; padding-right: 10px;">
            <i class="fa fa-search icon-search" id="icon-search" style="cursor: pointer;font-size: 30px; color: #707070;"></i>
        </div>

        <nav class="shift-mobile">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12" style="text-align: right;">
                    <i class="fas fa-times" style="color: white; font-size: 25px; cursor: pointer;margin-top: 10px;" id="navbar-close-mobile"></i>
                </div>
                <div class="col-md-12 col-sm-12 col-12 d-pad-10" style="padding:0;">
                    <form action="<?php echo base_url().$LanguageActive.'/'.$this->lang->line('search-0').'/index' ?>" method="get" role="search" novalidate="novalidate">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-group-mobile">
                                    <i class="fas fa-search"></i>
                                </span>
                            </div>
                            <input type="search" name="search" id="input-search-mobile" class="form-control" id="iconified" placeholder="<?php echo $this->lang->line('nav_search')?>">
                        </div>
                    </form>
                    <hr width="100%" style="margin:0; margin-top: 5px; border-bottom: 1px solid white;">
                </div>
                <div class="col-md-12 col-sm-12 col-12" style="padding-left: 0; padding-right:0; padding-top: 20px; ">
                    <div class="accordion" id="accordion0">
                        <div class="card" style="background-color: transparent">
                            <?php
                            $i = 0;
                            foreach($nav_menu as $CHILD1){
                                if(empty(sizeof($CHILD1['CHILD']))){
                                    $UrlChild = base_url().$_SESSION['site_lang']."/".$CHILD1['LINK'];
                                }else{
                                    $UrlChild = "javascript:void(0)";
                                }

                                $active = "";
                                // $active = "style='color: white;'";
                            ?>
                                <div data-toggle="collapse" data-target="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>" class="card-body" id="heading<?php echo $i?>" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-bottom: 1px solid #213e8b; background-color: #213e8b">
                                    <a href="<?php echo $UrlChild ?>" <?php echo $active ?>><?php echo $CHILD1['NAME'];?></a>
                                    <?php 
                                    if(!empty(sizeof($CHILD1['CHILD']))){
                                    ?>
                                    <button class="btn btn-link" type="button" style="position: absolute; right: 0; padding-top: 0;">
                                        <i class="fas fa-chevron-down" style="font-size: 14px; color: white;"></i>
                                    </button>
                                    <?php } ?>
                                </div>

                                <div id="collapse<?php echo $i ?>" class="collapse" aria-labelledby="heading<?php echo $i?>" data-parent="#accordion0">
                                    <div class="accordion" id="accordion1">
                                        <div class="card" style="border: none">
                                            <?php
                                            $j = 0;
                                            foreach($CHILD1['CHILD'] AS $CHILD2){
                                                if(empty(sizeof($CHILD2['CHILD']))){
                                                    $UrlChild2 = base_url().$_SESSION['site_lang']."/".$CHILD2['LINK'];
                                                }else{
                                                    $UrlChild2 = "javascript:void(0)";
                                                }
                                                
                                                // $active = "style='margin-bottom: 0; font-size: 14px;'";
                                                $active = "style='color: white;'";
                                            ?>
                                                <div data-toggle="collapse" data-target="#collapse<?php echo $i.$j ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j ?>" class="card-body" id="heading<?php echo $i.$j ?>" style="padding-left: 30px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px;border: none; border-bottom: 1px solid #213e8b; background-color: #213e8b;">
                                                    <a href="<?php echo $UrlChild2 ?>" style="color: white; "><?php echo $CHILD2['NAME'];?></a>
                                                    <?php 
                                                    if(!empty(sizeof($CHILD2['CHILD']))){
                                                    ?>
                                                    <button class="btn btn-link" type="button" style="position: absolute; right: 0; padding-top: 0;">
                                                        <i class="fas fa-chevron-down" style="font-size: 14px;color: white;"></i>
                                                    </button>
                                                    <?php } ?>
                                                </div>
                                                <div id="collapse<?php echo $i.$j ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j ?>" data-parent="#accordion1">
                                                    <div class="accordion" id="accordion2">
                                                        <div class="card" style="border: none">
                                                            <?php
                                                            $k = 0;
                                                            foreach($CHILD2['CHILD'] AS $CHILD3){
                                                                if(empty(sizeof($CHILD3['CHILD']))){
                                                                    $UrlChild3 = base_url().$_SESSION['site_lang']."/".$CHILD3['LINK'];
                                                                }else{
                                                                    $UrlChild3 = "javascript:void(0)";
                                                                }
                                                                
                                                                // $active = "style='margin-bottom: 0; font-size: 14px;'";
                                                                $active = "style='color: white;'";
                                                            ?>
                                                                <div data-toggle="collapse" data-target="#collapse<?php echo $i.$j.$k ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j.$k ?>" class="card-body" id="heading<?php echo $i.$j.$k ?>" style="padding-left: 50px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; border: none; border-bottom: 1px solid #213e8b; background-color: #213e8b;">
                                                                    <a  href="<?php echo $UrlChild3 ?>" style="color: white; "><?php echo $CHILD3['NAME'];?></a>
                                                                    <?php 
                                                                    if(!empty(sizeof($CHILD3['CHILD']))){
                                                                    ?>
                                                                    <button class="btn btn-link" type="button"  style="position: absolute; right: 0; padding-top: 0;">
                                                                        <i class="fas fa-chevron-down" style="font-size: 14px; color: white;"></i>
                                                                    </button>
                                                                    <?php } ?>
                                                                </div>
                                                                <div id="collapse<?php echo $i.$j.$k ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j.$k ?>" data-parent="#accordion2">
                                                                    <div class="accordion" id="accordion3">
                                                                        <div class="card" style="border: none">
                                                                            <?php
                                                                            $l = 0;
                                                                            foreach($CHILD3['CHILD'] AS $CHILD4){
                                                                                if(empty(sizeof($CHILD4['CHILD']))){
                                                                                    $UrlChild4 = base_url().$_SESSION['site_lang']."/".$CHILD4['LINK'];
                                                                                }else{
                                                                                    $UrlChild4 = "javascript:void(0)";
                                                                                }
                                                                                
                                                                                // $active = "style='margin-bottom: 0; font-size: 14px;'";
                                                                                $active = "style='color: white;'";
                                                                            ?>
                                                                                <div data-toggle="collapse" data-target="#collapse<?php echo $i.$j.$k.$l ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j.$k.$l ?>" class="card-body" id="heading<?php echo $i.$j.$k.$l ?>" style="padding-left: 70px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; border: none; border-bottom: 1px solid #213e8b; background-color: #213e8b !important;">
                                                                                    <a  href="<?php echo $UrlChild4 ?>" style="color: white; "><?php echo $CHILD4['NAME'];?></a>
                                                                                    <?php 
                                                                                    if(!empty(sizeof($CHILD4['CHILD']))){
                                                                                    ?>
                                                                                    <button class="btn btn-link" type="button" style="position: absolute; right: 0; padding-top: 0;">
                                                                                        <i class="fas fa-chevron-down" style="font-size: 14px; color: white;"></i>
                                                                                    </button>
                                                                                    <?php } ?>
                                                                                </div>
                                                                                <!-- <div id="collapse<?php echo $i.$j.$k.$l ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j.$k.$l ?>" data-parent="#accordion3">
                                                                                    
                                                                                </div> -->
                                                                            <?php 
                                                                            $l++;
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php 
                                                            $k++;
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php 
                                            $j++;
                                            } 
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php 
                            $i++;
                            } 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <div class="row-language">
            <?php
            for($i=0; $i<sizeof($ExpLang); $i++){
                $Language = isset($ExpLang[$i]) ? $ExpLang[$i]:"";
                if($LanguageActive == strtolower($Language)){
                    $textactive = "active";
                }else{
                    $textactive = "";
                }
            ?>
                <a href='javascript:void(0)' class="language <?php echo $textactive ?>" onclick="changelang('<?php echo strtolower($Language)?>', '<?php echo $changelang ?>', '<?php echo $Url ?>')"><?php echo $Language ?></a>
                <?php 
                if($i == 0){
                ?>
                    &nbsp;<span class="seperator"></span>&nbsp;
                <?php
                }
                ?>
            <?php
            }
            ?>
        </div>
    </div>
</nav>

<div class="row" id="module-right">
    <div class="col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module inactive" id="module-network" onclick="openmodright('module-network', 'network')" data='network'>
        <img src="<?php echo base_url('assets/image/network.png') ?>" class="img-nav-right" id="img-nav-network">
        <p><?php echo $this->lang->line('mod_right1') ?></p>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module inactive" id="module-sbdk" onclick="openmodright('module-sbdk', 'sbdk')" data='sbdk'>
        <img src="<?php echo base_url('assets/image/sbdk.png') ?>" class="img-nav-right" id="img-nav-sbdk">
        <p><?php echo $this->lang->line('mod_right2') ?></p>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module inactive" id="module-kurs" onclick="openmodright('module-kurs', 'kurs')" data='kurs'>
        <img src="<?php echo base_url('assets/image/kurs.png') ?>" class="img-nav-right" id="img-nav-kurs">
        <p><?php echo $this->lang->line('mod_right3') ?></p>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module inactive" id="module-simulasi" onclick="openmodright('module-simulasi', 'simulasi')" data='simulasi'>
        <img src="<?php echo base_url('assets/image/simulasi.png') ?>" class="img-nav-right" id="img-nav-simulation">
        <p><?php echo $this->lang->line('mod_right4') ?></p>
    </div>
</div>

</header>

<div class="card card-module-detail" style="display: none; height: 70%;">
    <!-- <span class="times" onclick="hidemoduleright()">&times;</span> -->
    <div class="card-body card-module-network" id="card-module-network" style="display: none;">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <h5><?php echo $this->lang->line('mod_right1') ?></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <span class="times" onclick="hidemoduleright()" style="float: right !important; position: unset !important; padding-top: 0 !important;">&times;</span>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                <hr width="100%" style="border-bottom: 1px solid white;margin-top: 0;">
                <form id="formDataBranch" name="formDataBranch" action="javascript:void(0)" method="post" data-url="<?php echo base_url().$LanguageActive.'/'.$this->lang->line('cabang-0').'/view' ?>">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10" style="padding-left: 0; padding-right: 0;">
                            <select name="province" id="province" class="form-control font-13">
                                <option value=""><?php echo $this->lang->line('cabang-2') ?></option>
                                <?php 
                                foreach ($province as $row) {
                                    $PROVINCE_ID = isset($row->PROVINCE_ID) ? $row->PROVINCE_ID:"";
                                    $PROVINCE_NAME = isset($row->PROVINCE_NAME) ? $row->PROVINCE_NAME:"";
                                ?>
                                    <option value="<?php echo $PROVINCE_ID.'-'.$PROVINCE_NAME ?>"><?php echo $PROVINCE_NAME ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10" style="padding-left: 0; padding-right: 0;">
                            <select name="citykab" id="citykab" class="form-control font-13">
                                <option><?php echo $this->lang->line('cabang-3') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10" style="padding-left: 0; padding-right: 0;">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d126920.3746158166!2d106.8072958!3d-6.2291876!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f682cab14665%3A0x69fd7708f2aac3f8!2sBank%20Bumi%20Arta!5e0!3m2!1sid!2sid!4v1617062173346!5m2!1sid!2sid" style="border:0;width: 100%; height: auto;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10" style="padding-left: 0; padding-right: 0;">
                            <button class="btn btn-light btn-block font-14" type="button" onclick="search()" style="color: #28448f;">
                                <?php echo $this->lang->line('cabang-9') ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card-body card-module-sbdk" id="card-module-sbdk" style="display: none;">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <h5><?php echo $this->lang->line('mod_right2') ?></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <span class="times" onclick="hidemoduleright()" style="float: right !important; position: unset !important; padding-top: 0 !important;">&times;</span>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                <hr width="100%" style="border-bottom: 1px solid white; margin-top: 0;">
                <?php
                foreach($sbdk as $row){
                    $EFFECTIVE_DATE = convertDateLang($row['EFFECTIVE_DATE'], 'dmyhis');
                    $CORPORATE_LOAN = $row['CORPORATE_LOAN'];
                    $RETAIL_LOAN = $row['RETAIL_LOAN'];
                    $MICRO_LOAN = $row['MICRO_LOAN'];
                    $MORTGAGE = $row['MORTGAGE'];
                    $NONMORTGAGE = $row['NONMORTGAGE'];
                ?>
                    <div>
                        <?php echo $this->lang->line('mod_right2') ?> per
                        (<?php echo $EFFECTIVE_DATE ?>)
                    </div>
                    <div class="table-responsive" style="padding-top: 20px;">
                        <table class="table table-bordered table-sbdk">
                            <thead>
                                <tr align="center">
                                    <td><?php echo $this->lang->line('sbdk-1') ?></td>
                                    <td><?php echo $this->lang->line('sbdk-2') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $this->lang->line('sbdk-3') ?></td>
                                    <td align="center"><?php echo $CORPORATE_LOAN ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $this->lang->line('sbdk-4') ?></td>
                                    <td align="center"><?php echo $RETAIL_LOAN ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $this->lang->line('sbdk-5') ?></td>
                                    <td align="center"><?php echo $MICRO_LOAN ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php 
                }
                ?>
                <button class="btn btn-light btn-block t-default font-14" type="button" onclick="window.location.href='<?php echo base_url().$LanguageActive?>/sbdk'">
                    <?php echo $this->lang->line('sbdk-0') ?>
                </button>
            </div>
        </div>
    </div>

    <div class="card-body card-module-kurs" id="card-module-kurs" style="display: none;">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <h5><?php echo $this->lang->line('mod_right3') ?></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <span class="times" onclick="hidemoduleright()" style="float: right !important; position: unset !important; padding-top: 0 !important;">&times;</span>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                <hr width="100%" style="border-bottom: 1px solid white; margin-top: 0;">
                <?php 
                $i=0;
                if(sizeof($kurs[0]['DETAIL']) > 4){
                ?>
                    <style type="text/css">
                        @media all and (max-width: 992px) {
                            table.table-kurs tbody {
                                display: block;
                                height: 127px;
                                overflow: auto;
                                scrollbar-width: thin;
                                -webkit-scrollbar-width: thin;
                            }
                            table.table-kurs thead, table.table-kurs tbody tr {
                                display: table;
                                width: 100%;
                                table-layout: fixed;
                            }
                            table.table-kurs thead {
                                width: calc( 100% - 1px )
                            }
                        }

                        @media all and (min-width: 992px) {
                            table.table-kurs tbody {
                                display: block;
                                height: 127px;
                                overflow: auto;
                                scrollbar-width: thin;
                                -webkit-scrollbar-width: thin;
                            }
                            table.table-kurs thead, table.table-kurs tbody tr {
                                display: table;
                                width: 100%;
                                table-layout: fixed;
                            }
                            table.table-kurs thead {
                                width: calc( 97% - 1px )
                            }

                            ::-webkit-scrollbar {
                                height: 12px;
                                width: 7px;
                                background: #000;
                            }

                            ::-webkit-scrollbar-thumb {
                                background: gray;
                                -webkit-border-radius: 1ex;
                                -webkit-box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.75);
                            }

                            ::-webkit-scrollbar-corner {
                                background: #000;
                            }
                        }
                    </style>
                <?php
                }
                foreach($kurs as $row){
                    $DETAIL = $kurs[$i]['DETAIL'];
                    $EFFECTIVE_DATE = date("d-m-Y H:i:s", strtotime($kurs[$i]['EFFECTIVE_DATE']));

                ?>
                    <div>
                        <?php echo $this->lang->line('kurs-3') ?> TT Counter<br>
                        (<?php echo $EFFECTIVE_DATE ?>)
                    </div>
                    <div class="table-responsive responsive-tkurs" style="padding-top: 20px;">
                        <table class="table table-bordered table-kurs">
                            <thead>
                                <tr align="center">
                                    <td><?php echo $this->lang->line('kurs-0') ?></td>
                                    <td><?php echo $this->lang->line('kurs-1') ?></td>
                                    <td><?php echo $this->lang->line('kurs-2') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $k=0;
                                foreach ($DETAIL as $data) { 
                                    // if($k < 4){
                                        $CURRENCY_CODE = isset($data['CURRENCY_CODE']) ? $data['CURRENCY_CODE']:"";
                                        $IS_HIGHLIGHT = isset($data['IS_HIGHLIGHT']) ? $data['IS_HIGHLIGHT']:"";
                                        $BUY = isset($data['BUY']) ? $data['BUY']:"";
                                        $SELL = isset($data['SELL']) ? $data['SELL']:"";
                                        $LINK = isset($data['LINK']) ? $data['LINK']:"";
                                        if($IS_HIGHLIGHT == '1'){
                                ?>
                                            <tr align="center">
                                                <td align="left" style="font-size: 13px !important;">
                                                    <img src="<?php echo $LINK ?>" style="width: 30px; height: 15px;border:1px solid #ddcfcf;"> &nbsp; <?php echo $CURRENCY_CODE ?>
                                                </td>
                                                <td style="font-size: 13px !important;"><?php echo $BUY ?></td>
                                                <td style="font-size: 13px !important;"><?php echo $SELL ?></td>
                                            </tr>
                                <?php 
                                        }
                                    // }
                                $k++;
                                } 
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php
                $i++;
                } 
                ?>
                <button class="btn btn-light btn-block t-default font-14" type="button" onclick="window.location.href='<?php echo base_url().$LanguageActive?>/kurs'">
                    <?php echo $this->lang->line('kurs-4') ?>
                </button>
            </div>
        </div>
    </div>

    <div class="card-body card-module-simulasi" id="card-module-simulasi" style="display: none;">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <h5><?php echo $this->lang->line('mod_right4') ?></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                <span class="times" onclick="hidemoduleright()" style="float: right !important; position: unset !important; padding-top: 0 !important;">&times;</span>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                <!-- <h5><?php echo $this->lang->line('mod_right4') ?></h5> -->
                <hr width="100%" style="border-bottom: 1px solid white; margin-top: 0;">
                <div id="accordion-simulation">
                    <span id="simulasi-ppr" onclick="showhidesimulation('simulasi-ppr')" class="t-default box-simulation show">
                        PPR
                    </span>&nbsp;&nbsp;
                    <span id="simulasi-ppm" onclick="showhidesimulation('simulasi-ppm')" class="t-default box-simulation">
                        PPM
                    </span>

                    <form action="<?php echo $Urlppr ?>" method="post">
                    <div id="simulasippr" class="box-form" style="padding-top: 25px; display: block;">
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-1') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="amountppr" name="amount" class="input-simulation font-13" onkeydown="return numbersonly(this, event);">
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">DP %</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="dp" name="downpayment" class="input-simulation font-13" onkeypress="return isNumber(event)">
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-2') ?> %</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="bunga" name="interest" class="input-simulation font-13" onkeypress="return isNumber(event)">
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-3') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <select class="input-simulation" id="tenor" name="tenor" style="font-size: 13px !important;">
                                    <option><?php echo $this->lang->line('simulation-4') ?></option>
                                    <?php 
                                    for($i=1; $i<=15; $i++){
                                    ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-5') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="anuitas" name="anuitas" style="background-color: #cccccc; font-size: 13px !important;" class="input-simulation" value="Anuitas" readonly="">
                            </div>
                        </div>
                        <div class="row d-padtop-20" style="padding-top: 20px;">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                                <button type="submit" class="btn btn-light btn-block t-default" id="calculate-ppr" style="font-size:14px;"><?php echo $this->lang->line('simulation-6') ?> PPR</button>
                            </div>
                        </div>
                    </div>
                    </form>

                    <div id="result-simulasi-ppr" class="result-simulasi" style="padding-top: 25px; display: none;">
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                </span> <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-1') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textAmount" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">DP</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textDownpayment" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-12') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textPlafon" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-3') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span id="textTenor" class="font-13"></span> <span class="font-13">Bulan</span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-2') ?> / <?php echo $this->lang->line('simulation-14') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span id="textInterest" class="font-13"></span> <span class="font-13">%</span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-16') ?> / <?php echo $this->lang->line('simulation-15') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textInstallments" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding" style="text-align: right;">
                                <button type="button" class="btn btn-light btn-block" id="btn-reset-ppr"><span>Reset</span></button>
                            </div>
                        </div>
                    </div>
                    
                    <form action="<?php echo $Urlppm ?>" method="post">
                    <div id="simulasippm" class="box-form" style="padding-top: 25px; display: none;">
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-18') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="amountppm" name="amountppm" class="input-simulation font-13" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">DP %</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="dp" name="downpaymentppm" class="input-simulation font-13" onkeypress="return isNumber(event)">
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-2') ?> %</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <input type="text" id="bunga" name="interestppm" class="input-simulation font-13" onkeypress="return isNumber(event)">
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-3') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <select class="input-simulation" id="tenorppm" name="tenorppm" style="font-size: 13px !important;">
                                    <option><?php echo $this->lang->line('simulation-4') ?></option>
                                    <?php 
                                    for($i=1; $i<=5; $i++){
                                    ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row d-padtop-20" style="padding-top: 20px;">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                                <button type="submit" class="btn btn-light btn-block t-default" id="calculate-ppm" style="font-size:14px;"><?php echo $this->lang->line('simulation-6') ?> PPM</button>
                            </div>
                        </div>
                    </div>
                    </form>

                    <div id="result-simulasi-ppm" class="result-simulasi" style="padding-top: 25px; display: none;">
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                </span> <span style="font-size: 13px;"><?php echo $this->lang->line('simulation-18') ?></span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textAmountppm" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">DP</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textDownpaymentppm" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">Plafon</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textPlafonppm" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">Jangka Waktu</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span id="textTenorppm" class="font-13"></span> <span class="font-13">Bulan</span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">Bunga / Tahun</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span id="textInterestppm" class="font-13"></span> <span class="font-13">%</span>
                            </div>
                        </div>
                        <div class="row d-padtop-10">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
                                <span style="font-size: 13px;">Angsuran / Bulan</span>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding" style="text-align: right;">
                                <span class="font-13">Rp.</span> <span id="textInstallmentsppm" class="font-13"></span>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding" style="text-align: right;">
                                <button type="button" class="btn btn-light btn-block" id="btn-reset-ppm"><span>Reset</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #input-search::placeholder {
        /*font-family: 'Font Awesome 5 Free';*/
        font-family: 'Nunito-Light' !important;
        font-style: normal;
        font-weight: 599;
        text-decoration: inherit;
        font-size: 14px;
        opacity: 50%;
    }

    #input-search-mobile::placeholder {
        /*font-family: 'Font Awesome 5 Free';*/
        font-family: 'Nunito-Light' !important;
        font-style: normal;
        font-weight: 599;
        text-decoration: inherit;
        font-size: 14px;
    }

    nav.shift ul li a {
        font-size: 13px !important;
        color: #243E8B;
        transition-delay: 5s;
    }

    div.accordion div.card div.card-body a:hover{
        color: #243E8B !important;
    }

    nav.shift-mobile div.accordion div.card-body a{
        color: white !important;
    }

    nav.shift-mobile div.accordion div.card-body:hover{
        background-color: #132c79 !important;
    }

    nav.shift-mobile div.accordion div.card-body a:hover{
        color: #fff;
        background-color: #132c79 !important;
    }

    div.card-module-detail{
        /*height: 60% !important;*/
    }

    .row-header{
        height: 250px;
    }


    @media all and (min-width: 992px) {
        div.card-module-detail{
            -webkit-transition: all 500ms ease-in;
            -moz-transition: all 500ms ease-in;
            -o-transition: all 500ms ease-in;
            transition: all 500ms ease-in;
            right: -300px;
        }

        div#module-right{
            text-align: center;
            float: right;
            width: 80px;
            position: fixed;
            right: -80px;
            z-index: 1;
            margin-top: 25px;
            -webkit-transform:translateZ(0) scale(1,1);
            -webkit-transition:all .3s linear;
            transition:all .3s linear;
        }

        div.row-page-module{
            display: block;
        }
    }
    @media all and (max-width: 992px) {
        div#module-right{
            text-align: center;
            float: none;
            width: 100%;
            color: #28448f !important;
            font-size: xx-large !important;
            position: fixed;
            bottom: 0 !important;
            background-color: white;
            z-index: 100;
            right: 0;
        }

        div.card-module-detail{
            -webkit-transition: all 500ms ease-in;
            -moz-transition: all 500ms ease-in;
            -o-transition: all 500ms ease-in;
            transition: all 500ms ease-in;
        }

        div.row-page-module{
            display: none;
        }
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        setInterval(function(){
            sethover();
        },200);

        var ppr = document.getElementById('amountppr');
        ppr.addEventListener('keyup', function(e){
            ppr.value = formatRupiah(this.value, '');
        });

        var ppm = document.getElementById('amountppm');
        ppm.addEventListener('keyup', function(e){
            ppm.value = formatRupiah(this.value, '');
        });

        var header = $('header').innerHeight();
        var LanguageActive = "<?php echo $LanguageActive ?>";
        var base_url = '<?php echo base_url() ?>';
        var find3 = $('nav ul').find('.navbarDropdown');
        var w_screen = screen.width;

        find3.each(function(){
            var getid = $(this).attr("id");
            $("#"+getid).hover(function(){
                $(this).removeClass().addClass('navbarDropdown show');
            }, function(){
                $(this).removeClass().addClass('navbarDropdown');
            });
        });

        var find1 = $('.card-body div.row div').find('.row-child');
        find1.each(function(){
            var getid = $(this).attr("id");
            $("#"+getid).hover(function(){
                $(this).removeClass().addClass('row-child show');
            }, function(){
                $(this).removeClass().addClass('row-child');
            });
        });

        var find2 = $('.card-body div.row div').find('.row-child-two');
        find2.each(function(){
            var getid = $(this).attr("id");
            $("#"+getid).hover(function(){
                $(this).removeClass().addClass('row-child-two show');
            }, function(){
                $(this).removeClass().addClass('row-child-two');
            });
        });

        var find4 = $('.card-body div.row div').find('.row-child-tree');
        find4.each(function(){
            var getid = $(this).attr("id");
            $("#"+getid).hover(function(){
                $(this).removeClass().addClass('row-child-tree show');
            }, function(){
                $(this).removeClass().addClass('row-child-tree');
            });
        });

        $('#navbar-close-mobile').click(function(){
            $('#navbarSupportedContent').removeClass().addClass('collapse navbar-collapse');
        });

        $('#module-network').hover(function(){
            $('#img-nav-network').attr({'src': base_url+'assets/image/network-hover.png'});
        }, function(){
            setdefaultimg();
        });

        $('#module-sbdk').hover(function(){
            $('#img-nav-sbdk').attr({'src': base_url+'assets/image/sbdk-hover.png'});
        }, function(){
            setdefaultimg();
        });

        $('#module-kurs').hover(function(){
            $('#img-nav-kurs').attr({'src': base_url+'assets/image/kurs-hover.png'});
        }, function(){
            setdefaultimg();
        });

        $('#module-simulasi').hover(function(){
            $('#img-nav-simulation').attr({'src': base_url+'assets/image/simulasi-hover.png'});
        }, function(){
            setdefaultimg();
        });

        if(w_screen > 992){
            $('#logo-otoritas').attr({'src':base_url+'assets/image/logo-otoritas-new.png'});
            $('body').hover(function(){
                $('div.card-module-detail').css({'right':'80px'});
                $('div#module-right').css({'right':'0'});

                // $('div.dropdown').css({'display':'none'});
            }, function(){
                setTimeout(function(){
                    $('div#module-right').css({'right':'-80px'});
                    $('div.card-module-detail').css({'right':'0'});
                }, 10000);
                
            });
        }else{ 
            $('body').hover(function(){
                $('div.card-module-detail').css({'bottom':'77px', 'overflow-y':'scroll'});
            }, function(){
                $('div.card-module-detail').css({'bottom':'-500px', 'overflow-y':'none'});
            });
            $('#logo-otoritas').attr({'src':base_url+'assets/image/logo-otoritas-mobile.png'});
        }

        $('#calculate-ppr').click(function(){
            return;
            var url = '<?php echo base_url()?>main/ppr';
            $('#simulasippr').css('display','none');
            $('#result-simulasi-ppr').fadeIn();
            var harga = $('input[name=amount]').val();
            harga = harga.replaceAll(".","");
            var dp = $('input[name=downpayment]').val();
            var bunga = $('input[name=interest]').val();
            var tenor = document.getElementById('tenor').value;

            var data = {
                'harga' : harga,
                'bunga' : bunga,
                'tenor' : tenor,
                'dp' : dp
            }

            $.ajax({
                url : url,
                type : 'post',
                cache : false,
                dataType : 'json',
                data : data,
                success : function(res){
                    $('#textAmount').html(formatMoney(res['harga']));
                    $('#textDownpayment').html(formatMoney(res['dp']));
                    $('#textPlafon').html(formatMoney(res['plafond']));
                    $('#textTenor').html(res['tenor']);
                    $('#textInterest').html(res['bunga']);
                    $('#textInstallments').html(formatMoney(res['anuitas']));
                }
            });
        });

        $('#btn-reset-ppr').click(function(){
            $('#simulasippr').fadeIn();
            $('#result-simulasi-ppr').css('display','none');
        });

        $('#btn-reset-ppm').click(function(){
            $('#simulasippm').fadeIn();
            $('#result-simulasi-ppm').css('display','none');
        });

        $('#calculate-ppm').click(function(){
            return;
            $('#simulasippm').css('display','none');
            $('#result-simulasi-ppm').fadeIn();
            var harga = $('input[name=amountppm]').val();
            harga = harga.replaceAll(".","");
            var dp = $('input[name=downpaymentppm]').val();
            var bunga = $('input[name=interestppm]').val();
            var jangkawaktu = document.getElementById('tenorppm').value;

            const search = ',';
            const replaceWith = '.';

            var dp_awal = (harga*dp)/100;
            var pokok_pinjaman = harga - dp_awal;
            var jangka_kredit = jangkawaktu * 12;
            var jumlah_bunga = (bunga*pokok_pinjaman/100) * jangkawaktu;
            var cicilan = (parseInt(pokok_pinjaman) + parseInt(jumlah_bunga))/jangka_kredit;

            if(cicilan == "Infinity" || cicilan == "NaN"){
                cicilan = 0;
            }

            $('#textAmountppm').html(formatMoney(harga));
            $('#textDownpaymentppm').html(formatMoney(dp_awal));
            $('#textPlafonppm').html(formatMoney(pokok_pinjaman));
            $('#textTenorppm').html(jangka_kredit);
            $('#textInterestppm').html(bunga);
            $('#textInstallmentsppm').html(formatMoney(cicilan));
        });

        $('#province').change(function(){
            var id = $(this).val();
            $.ajax({
                url : base_url+LanguageActive+'/main/citykab',
                data : {'id' : id},
                type : 'post',
                cache : false,
                success : function(res){
                    $('#citykab').html(res);
                }
            });
        });
    });
    
    function sethover(){
        var nav = $('nav.shift ul').find('li.navbarDropdown.show');
        var length = nav.length;
        if(length == 0){
            // $('div.dropdown').css({'display':'none'});
        }
    }

    function setdefaultimg(){
        var base_url = '<?php echo base_url() ?>';
        $('#module-network img').attr({'src': base_url+'assets/image/network.png'});
        $('#module-sbdk img').attr({'src': base_url+'assets/image/sbdk.png'});
        $('#module-kurs img').attr({'src': base_url+'assets/image/kurs.png'});
        $('#module-simulasi img').attr({'src': base_url+'assets/image/simulasi.png'});

        $('#module-network.active img').attr({'src': base_url+'assets/image/network-hover.png'});
        $('#module-sbdk.active img').attr({'src': base_url+'assets/image/sbdk-hover.png'});
        $('#module-kurs.active img').attr({'src': base_url+'assets/image/kurs-hover.png'});
        $('#module-simulasi.active img').attr({'src': base_url+'assets/image/simulasi-hover.png'});
    }

    function changeimgnavbar(){
        var base_url = '<?php echo base_url() ?>';
        var card = $('#module-right').find('.card-module.active');
        card.each(function(){
            var getid = $(this).attr('id');
            var idimg = $('#'+getid+' img').attr('id');
            if(idimg == "img-nav-network"){
                $('#'+idimg).attr({'src': base_url+'assets/image/network-hover.png'});
            }else if(idimg == "img-nav-sbdk"){
                $('#'+idimg).attr({'src': base_url+'assets/image/sbdk-hover.png'});
            }else if(idimg == "img-nav-kurs"){
                $('#'+idimg).attr({'src': base_url+'assets/image/kurs-hover.png'});
            }else if(idimg == "img-nav-simulation"){
                $('#'+idimg).attr({'src': base_url+'assets/image/simulasi-hover.png'});
            }
        });
    }

    function hidemoduleright(){
        var modright = $('#module-right');
        var row = modright.find('div.card-module');
        row.each(function(){
            var getid = $(this).attr("id");
            var getdt = $(this).attr("data");
            $('#'+getid).attr({'class':'col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module'});
            $('#'+getid+' img').attr({'src':'https://demo.nieve.id/bumi_artha/assets/image/'+getdt+'.png'});
            $('#card-'+getid).css({'display':'none'});
        });
        $('body').css({'overflow-y':'scroll'});
        $('section#section1').css({'position':'relative'});
        $('.card-module-detail').fadeOut(500);
    }

    function openmodright(id, name){
        var base_url = '<?php echo base_url() ?>';
        var w_screen = screen.width;
        var modright = $('#module-right');
        var row = modright.find('div.card-module');
        if(w_screen < 992){
            $('div.card-module-detail').css({'display':'block', '-webkit-transform':'translateZ(0) scale(1,1)', '-webkit-transition':'all .3s linear', 'transition':'all .3s linear', 'bottom':'-500px'});
        }else{
            $('div.card-module-detail').css({'display':'block', '-webkit-transform':'translateZ(0) scale(1,1)', '-webkit-transition':'all .3s linear', 'transition':'all .3s linear', 'right':'-300px'});
        }
        
        setTimeout(function(){
            row.each(function(){
                var getid = $(this).attr("id");
                var getclass = $(this).attr("class");
                var getdata = $(this).attr("data");
                var splclass =  getclass.split(" ");

                $('#'+getid).attr({'class':'col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module'});
                $('#card-'+getid).css({'display':'none'});
                

                $('#card-'+getid).fadeIn();
                if(getid == id){
                    $('#'+getid).attr({'class':'col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module active'});
                    setTimeout(function(){
                        $('#'+getid).find('img').attr({'src':base_url+'assets/image/'+getdata+'-hover.png'});
                    },500);
                    var heightmod = $('#card-'+getid).innerHeight();
                    if(w_screen < 992){
                        $('#'+getid).fadeIn();
                        $('.card.card-module-detail').css({'display':'block', 'height': '404px', 'bottom':'77px'});
                        $('#card-'+getid).css({'display':'block'});
                    }else{
                        $('div.card-module-detail').css({'display':'block', '-webkit-transform':'translateZ(0) scale(1,1)', '-webkit-transition':'all .3s linear', 'transition':'all .3s linear', 'right':'80px'});
                        setmodalright(); 
                    }
                }else{
                    setTimeout(function(){
                        $('#'+getid).find('img').attr({'src':base_url+'assets/image/'+getdata+'.png'});
                    },500);
                    $('.card.card-module-detail').css({'display':'block'});
                    $('#'+getid).attr({'class':'col-xl-12 col-lg-12 col-md-3 col-sm-3 col-3 card-module'});
                    $('#card-'+getid).css({'display':'none'});
                }
                changeimgnavbar();

            });
        },300);
        
    }

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    function setmodalright(){
        var w_screen = screen.width;
        var h_screen = screen.height;
        var h_body = $('body').innerHeight();
        var h_nav_Search = $('.row-search').innerHeight();
        var h_nav_module = $('nav.navbar').innerHeight();
        var h_module_right = $('#module-right').innerHeight();
        var h_navbar = $('div.header-fixed').innerHeight();

        var h_navbar_mobile = parseInt(h_nav_Search) + parseInt(h_nav_module);
        var height_modal = parseInt(h_screen) - parseInt(h_navbar) - parseInt(h_module_right);

        if(w_screen > 992){
            $('div.card-module-detail').css({'height':h_module_right});
        }else{
            $('section#section1').css({'position':'absolute', 'z-index':'0'});
        }
    }

    function search(){
        var form = $('#formDataBranch');
        var data = form.serialize();
        var url = form.data('url');
        $.ajax({
            url : url,
            data : data,
            cache : false,
            dataType : 'json',
            type : 'post',
            beforeSend : function(){
                $('.preloader').fadeIn();
            },
            success : function(res){
                $('.preloader').fadeOut();
                var ErrorMessage = res.ErrorMsg;
                var ErrorCode = res.ErrorCode;
                var nextpage = res.Url;
                if(ErrorCode != "EC:0000"){
                    Swal.fire({
                        type: 'error',
                        html: ErrorMessage,
                        confirmButton: true,
                        confirmButtonColor : '#1FB3E5',
                        confirmButtonText : 'Close'
                    });
                }else{
                    setTimeout(function(){
                        window.location.href=nextpage;
                    },500);
                }     
            }
        });
    }
</script>