<?php
$lang['home'] = 'Beranda';
$lang['home1'] = 'Wujudkan Impian Anda';

$lang['nav_search'] = "Cari di sini";
$lang['menu1'] = "Hubungan Investor";
$lang['share'] = "Bagikan";
$lang['result'] = "Hasil";
$lang['opening'] = "Tersedia";

$lang['mod_right1'] = 'Jaringan';
$lang['mod_right2'] = 'SBDK';
$lang['mod_right3'] = 'Kurs';
$lang['mod_right4'] = 'Simulasi';

$lang['lowker-0'] = 'posisi';
$lang['lowker-1'] = 'Temukan Minat Anda';
$lang['lowker-2'] = 'Posisi Yang Tersedia';
$lang['lowker-3'] = 'Karir';
$lang['lowker-4'] = 'Lowongan Kerja';
$lang['lowker-5'] = 'Posisi';
$lang['lowker-6'] = 'Waktu pendaftaran';
$lang['lowker-7'] = 'Rincian';
$lang['lowker-8'] = 'Kode Lowongan';
$lang['lowker-9'] = 'Tanggal Posting';
$lang['lowker-10'] = 'Deskripsi Pekerjaan';
$lang['lowker-11'] = 'Persyaratan';
$lang['lowker-12'] = 'Letakkan kode posisi di subjek email atau di kiri atas amplop Anda. Kirimkan Surat Lamaran, Curriculum Vitae, Gelar Akademik, Transkrip Nilai, dan 2 buah Foto 4 x 6 ke: ';
$lang['lowker-13'] = 'Ayo bergabung dengan BBA';
$lang['lowker-14'] = 'Kembangkan karirmu bersama kami';

$lang['kurs-0'] = 'Mata Uang';
$lang['kurs-1'] = 'Beli';
$lang['kurs-2'] = 'Jual';
$lang['kurs-3'] = "Kurs";
$lang['kurs-4'] = "Kurs Lainnya";
$lang['kurs-5'] = "Nilai Beli dan Jual Kurs TT Counter";
$lang['kurs-6'] = "Penolakan";
$lang['kurs-7'] = "Nilai tukar dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya ";
$lang['kurs-8'] = "Silakan hubungi Kantor Cabang BBA terdekat kami untuk mendapatkan informasi nilai tukar khusus untuk transaksi dengan jumlah nominal tertentu .";
$lang['kurs-9'] = "Terakhir Diperbaharui";


$lang['sbdk-0'] = 'SBDK Lainnya';
$lang['sbdk-1'] = 'Produk';
$lang['sbdk-2'] = 'Suku Bunga';
$lang['sbdk-3'] = 'Kredit Korporasi';
$lang['sbdk-4'] = 'Kredit Retail';
$lang['sbdk-5'] = 'Kredit Mikro';
$lang['sbdk-6'] = "(efektif % per tahun)";
$lang['sbdk-7'] = "SUKU BUNGA DASAR KREDIT RUPIAH (PRIME LENDING RATE)";
$lang['sbdk-8'] = "Suku Bunga Dasar Kredit";

$lang['keterangan_sbdk'] = '<p><strong>Keterangan :</strong></p>
							<ol>
								<li>Suku Bunga Dasar Kredit (SBDK) digunakan sebagai dasar penetapan suku bunga kredit yang akan dikenakan oleh Bank kepada nasabah. SBDK belum memperhitungkan komponen estimasi premi risiko yang besarnya tergantung dari penilaian Bank terhadap risiko masing-masing debitur atau kelompok debitur. Dengan demikian, besarnya suku bunga kredit yang dikenakan kepada debitur belum tentu sama dengan SBDK</li>
								<li>Dalam kredit konsumsi non KPR tidak termasuk penyaluran dana melalui kartu kredit dan kredit tanpa agunan (KTA).</li>
								<li>Informasi SBDK yang berlaku setiap saat dapat dilihat pada publikasi di setiap kantor Bank dan/atau website Bank.</li>
							</ol>

							<p>*Suku bunga dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu</p>';
$lang['efektif_pertahun'] = '(efektif % per tahun)';

$lang['berdasarkan_segmen'] = 'Berdasarkan Segmen Kredit';
$lang['kredit_korporasi'] = 'Kredit Korporasi';
$lang['kredit_retail'] = 'Kredit Retail';
$lang['kredit_mikro'] = 'Kredit Mikro';
$lang['kredit_konsumsi'] = 'Kredit Konsumsi';
$lang['kpr'] = 'KPR';
$lang['non_kpr'] = 'Non-KPR';

$lang['tabungan'] = 'Tabungan';
$lang['giro'] = 'Giro';
$lang['deposito'] = 'Deposito';
$lang['suku_bunga_ket'] = '*Suku bunga dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu';

$lang['suku_bunga_0'] = 'Bulan';
$lang['suku_bunga_1'] = 'Saldo';
$lang['suku_bunga_2'] = 'Bunga';
$lang['suku_bunga_3'] = 'Suku Bunga';
$lang['suku_bunga_4'] = 'Suku Bunga Sebelumnya';
$lang['suku_bunga_5'] = 'Suku Bunga Selanjutnya';

$lang['penghargaan_0'] = 'Penghargaan';

$lang['cabang-0'] = "cabang";
$lang['cabang-1'] = "lokasi";
$lang['cabang-2'] = "Silahkan Pilih Provinsi";
$lang['cabang-3'] = "Silahkan Pilih Kabupaten / Kota";
$lang['cabang-4'] = "Kantor Cabang";
$lang['cabang-5'] = "Kantor Capem";
$lang['cabang-6'] = "Kantor Kas";
$lang['cabang-7'] = "Payment Point";
$lang['cabang-8'] = "Lokasi Cabang, Capem, Kantor Kas dan Payment Point di";
$lang['cabang-9'] = "Selengkapnya";

$lang['manajemen_0'] = 'Selengkapnya';
$lang['manajemen_1'] = 'Riwayat Pekerjaan';
$lang['manajemen_2'] = 'Tahun';
$lang['manajemen_3'] = 'Jabatan';

$lang['search-0'] = "cari";
$lang['search-1'] = "katakunci";
$lang['search-2'] = "Kata Kunci";
$lang['search-3'] = "Tanggal posting";
$lang['search-4'] = "Data tidak ditemukan";
$lang['search-5'] = "Hasil pencarian";
$lang['search-6'] = "Video Terkini";

$lang['simulation-0'] = "Simulasi";
$lang['simulation-1'] = "Harga Rumah";
$lang['simulation-2'] = "Bunga";
$lang['simulation-3'] = "Lama Kredit (Tahun)";
$lang['simulation-4'] = "Pilih Tahun";
$lang['simulation-5'] = "Perhitungan";
$lang['simulation-6'] = "Hitung Simulasi";
$lang['simulation-7'] = "Simulasi PPR";
$lang['simulation-8'] = "Simulasi PPM";
$lang['simulation-9'] = "Mulai";
$lang['simulation-10'] = "Informasi PPR";
$lang['simulation-11'] = "Informasi PPM";
$lang['simulation-12'] = "Jumlah Pinjaman";
$lang['simulation-13'] = "Lama Kredit (Bulan)";
$lang['simulation-14'] = "Tahun";
$lang['simulation-15'] = "Bulan";
$lang['simulation-16'] = "Angsuran";
$lang['simulation-17'] = "Perhitungan ini hanya bersifat simulasi. Untuk keterangan lebih lanjut dapat menghubungi pihak bank.";
$lang['simulation-18'] = "Harga Mobil";

$lang['footer-0'] = "Kantor Pusat Bank Bumi Arta";
$lang['footer-1'] = "Layanan 24 Jam";

$lang['pemenang-0'] = "Pengumuman Undian Tabungan Kesra";
$lang['pemenang-1'] = "Periode";
$lang['pemenang-2'] = "Tanggal";
$lang['pemenang-3'] = "Daftar Pemenang";
$lang['pemenang-4'] = "Klik disini untuk mengunduh";
$lang['pemenang-5'] = "Tidak ada data";