<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        TITLE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" name="title" class="form-control" id="title" placeholder="" required="">
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        VIDEO<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="file" id="file" name="file" class="dropify" required="" />
        <a href="#" onclick="openTnC('<?php echo base_url('video/tncupload') ?>')">Terms and Conditions Upload</a>

    </div>
</div>
<!-- <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        DESCRIPTION
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea type="text" name="description" class="form-control ckeditor" id="description"></textarea>
    </div>
</div> -->
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>

</div>

<script type="text/javascript">
$(function() {
    changeformfile();
    $('.dropify').dropify();

    // CKEDITOR.replace('description');
});
</script>