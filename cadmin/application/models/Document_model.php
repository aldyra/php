<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Document_model extends MY_Model
{

    protected $_table_name = 'MS_DOCUMENT';
    protected $_primary_key = 'ID';
    protected $_order_by = 'ID';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }

    public function updateaction($param)
    {
        $LINK = isset($param['LINK']) ? $param['LINK'] : "";
        if (!empty($LINK)) {
            $DATA['LINK'] = isset($param['LINK']) ? $param['LINK'] : "";
        }

        $ID = isset($param['ID']) ? $param['ID'] : "";
        $DATA['TITLE'] = isset($param['TITLE']) ? $param['TITLE'] : "";
        $DATA['STATUS'] = isset($param['STATUS']) ? $param['STATUS'] : "";
        $DATA['USER_LOG'] = isset($param['USER_LOG']) ? $param['USER_LOG'] : "";
        $DATA['DATE_LOG'] = isset($param['DATE_LOG']) ? $param['DATE_LOG'] : "";

        $WHERE['ID'] = $ID;
        $run = $this->db->where($WHERE)->update($this->_table_name, $DATA);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID'] = isset($param['ID']) ? $param['ID'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }
}