<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('gallery/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Gallery = isset($Data['gallery']) ? $Data['gallery'] : array();
?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Content</a>
                <a href="#" class="breadcrumb-item active">Gallery</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Gallery', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Gallery
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Image</th>
                    <th width="200">Title</th>
                    <th width="200">Link</th>
                    <th>Created By</th>
                    <th data-orderable="false"  width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Gallery as $gallery) {
    $ID = isset($gallery['ID']) ? $gallery['ID'] : "";
    $TITLE = isset($gallery['TITLE']) ? $gallery['TITLE'] : "";
    $LINK = isset($gallery['LINK']) ? $gallery['LINK'] : "";
    $STATUS = isset($gallery['STATUS']) ? $gallery['STATUS'] : "";
    $CREATED_BY = isset($gallery['CREATED_BY']) ? $gallery['CREATED_BY'] : "";
    $CREATED_DATE = isset($gallery['CREATED_DATE']) ? $gallery['CREATED_DATE'] : "";
    $USER_LOG = isset($gallery['USER_LOG']) ? $gallery['USER_LOG'] : "";
    $DATE_LOG = isset($gallery['DATE_LOG']) ? $gallery['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <a href='<?php echo $LINK ?>' data-popup='lightbox' data-fancybox><img src='<?php echo $LINK ?>'
                                class='img-thumbnail' width='80px' heigh='80px'></a>
                    </td>
                    <td>
                        <?php echo $TITLE ?>
                    </td>
                    <td>
                        <a data-toggle="tooltip" data-placement="top" title="Copy Link" href="#"
                            onclick="copy_text('<?php echo $LINK ?>')">COPY LINK</a>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $param['Controllers'] = $Controllers;
        $param['Url'] = base_url() . $Controllers;
        $param['Data'] = $gallery;
        $param['Modal'] = "";
        $enc = base64_encode(json_encode($param));
        ?>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_update)) {?>
                                    <a onclick="updateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-pencil"></i> Edit</a>

                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i>
                                        Activate</a>

                                    <?}elseif($STATUS == "88"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?}elseif($STATUS == "99"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?php }?>
                                    <?php }?>
                                    <?php if (!empty($is_delete)) {?>
                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(6).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function copy_text(text) {

    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    Swal.fire({
        type: 'success',
        title: 'Link Image Copied',
        showConfirmButton: false,
        timer: 1000
    });
}
</script>