<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('report_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "report/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['report'] = $this->report_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Allowed files are ' . str_replace("|", ", ", $this->config->item('allowed_types_file')) . ' <br>
                    Max file size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }

    public function add()
    {
        $REPORT_NAME_ID = isset($_POST['report_name_id']) ? $_POST['report_name_id'] : "";
        $REPORT_NAME_EN = isset($_POST['report_name_en']) ? $_POST['report_name_en'] : "";
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $FILE_ID = isset($_FILES["file_id"]['name']) ? $_FILES["file_id"]['name'] : "";
        $FILE_EN = isset($_FILES["file_en"]['name']) ? $_FILES["file_en"]['name'] : "";

        if (empty($FILE_ID)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File  Indonesia is required!");
            die(json_encode($JSON));
        }
        if (empty($FILE_EN)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File English is required!");
            die(json_encode($JSON));
        }

        $pathreport = $this->config->item('save_report');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size');
        $config['max_width'] = $this->config->item('max_width');
        $config['min_width'] = $this->config->item('min_width');
        $config['max_height'] = $this->config->item('max_height');
        $config['min_height'] = $this->config->item('min_height');
        $config['upload_path'] = $this->config->item('path_report');

        $path_parts_en = pathinfo($_FILES["file_en"]["name"]);

        $path_parts_id = pathinfo($_FILES["file_id"]["name"]);

        $forUpload['extension_en'] = $path_parts_en['extension'];
        $forUpload['extension_id'] = $path_parts_id['extension'];

        $Extension = $this->config->item('allowed_types_file');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension_id']), $Expfile)) {
            $config['file_name'] = 'REPORT_ID_' . date('ymdHis');

            $this->upload->initialize($config);
            $forUpload['file_name_id'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }
            if (!$this->upload->do_upload('file_id')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add file Indonesia! " . $error['error']);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add file Indonesia! Extension must " . $Extension);
            die(json_encode($JSON));
        }
        if (in_array(strtoupper($forUpload['extension_en']), $Expfile)) {
            $config['file_name'] = 'REPORT_EN_' . date('ymdHis');

            $this->upload->initialize($config);
            $forUpload['file_name_en'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }
            if (!$this->upload->do_upload('file_en')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add file English! " . $error['error']);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add file English! Extension must " . $Extension);
            die(json_encode($JSON));
        }

        $param['REPORT_NAME_ID'] = $REPORT_NAME_ID;
        $param['REPORT_NAME_EN'] = $REPORT_NAME_EN;
        $param['CATEGORY'] = $CATEGORY;
        $param['LINK_ID'] = $pathreport . $forUpload['file_name_id'] . "." . $forUpload['extension_id'];
        $param['LINK_EN'] = $pathreport . $forUpload['file_name_en'] . "." . $forUpload['extension_en'];
        $param['CREATED_BY'] = $_SESSION['username'];
        $Insert = $this->report_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));

    }

    public function view()
    {
        $GetData = $this->report_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $FILE_ID = isset($_FILES["file_id"]['name']) ? $_FILES["file_id"]['name'] : "";
        $FILE_EN = isset($_FILES["file_en"]['name']) ? $_FILES["file_en"]['name'] : "";
        $ID_REPORT = isset($_POST['id_report']) ? $_POST['id_report'] : "";
        $REPORT_NAME_ID = isset($_POST['report_name_id']) ? $_POST['report_name_id'] : "";
        $REPORT_NAME_EN = isset($_POST['report_name_en']) ? $_POST['report_name_en'] : "";
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $STATUS = isset($_POST['status']) ? $_POST['status'] : "";
        $FILE_EN_URL = isset($_POST['file_en_url']) ? $_POST['file_en_url'] : "";
        $FILE_ID_URL = isset($_POST['file_id_url']) ? $_POST['file_id_url'] : "";

        $pathreport = $this->config->item('save_report');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size');
        $config['max_width'] = $this->config->item('max_width');
        $config['min_width'] = $this->config->item('min_width');
        $config['max_height'] = $this->config->item('max_height');
        $config['min_height'] = $this->config->item('min_height');
        $config['upload_path'] = $this->config->item('path_report');
        $Extension = $this->config->item('allowed_types_file');
        $Expfile = explode("|", $Extension);

        if (!empty($FILE_ID)) {
            if (empty($FILE_ID)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File Indonesia is required!");
                die(json_encode($JSON));
            }

            $config['file_name'] = 'REPORT_ID_' . date('ymdHis');

            $path_parts_id = pathinfo($_FILES["file_id"]["name"]);
            $forUpload['extension_id'] = $path_parts_id['extension'];

            if (in_array(strtoupper($forUpload['extension_id']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name_id'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file_id')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add file Indonesia! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $FILE_ID_URL = $pathreport . $forUpload['file_name_id'] . "." . $forUpload['extension_id'];
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add file Indonesia! Extension must " . $Extension);
                die(json_encode($JSON));
            }

        }
        if (!empty($FILE_EN)) {
            if (empty($FILE_EN)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File English is required!");
                die(json_encode($JSON));
            }

            $config['file_name'] = 'REPORT_EN_' . date('ymdHis');

            $path_parts_en = pathinfo($_FILES["file_en"]["name"]);
            $forUpload['extension_en'] = $path_parts_en['extension'];

            if (in_array(strtoupper($forUpload['extension_en']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name_en'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file_en')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add file English! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $FILE_EN_URL = $pathreport . $forUpload['file_name_en'] . "." . $forUpload['extension_en'];
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add file English! Extension must " . $Extension);
                die(json_encode($JSON));
            }

        }

        $param['ID_REPORT'] = $ID_REPORT;
        $param['REPORT_NAME_ID'] = $REPORT_NAME_ID;
        $param['REPORT_NAME_EN'] = $REPORT_NAME_EN;
        $param['CATEGORY'] = $CATEGORY;
        $param['LINK_ID'] = $FILE_ID_URL;
        $param['LINK_EN'] = $FILE_EN_URL;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $Update = $this->report_model->updateaction($param);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_REPORT = isset($Data['ID_REPORT']) ? $Data['ID_REPORT'] : "";
        $param['ID_REPORT'] = $ID_REPORT;
        $Delete = $this->report_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_REPORT']) ? $Data['ID_REPORT'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_REPORT' => $id);
        $Active = $this->report_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_REPORT']) ? $Data['ID_REPORT'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_REPORT' => $id);
        $Active = $this->report_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_REPORT']) ? $Data['ID_REPORT'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_REPORT' => $id);
        $Active = $this->report_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}