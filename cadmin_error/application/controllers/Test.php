<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "test/view";
        // $data['authorize'] = $this->data['authorize'];
        $data['data']['menu'] = $this->menu_model->view();
        $data['data']['datamenu'] = $this->get_menu();
        $this->load->view('templates/view', $data);
    }
    public function get_menu()
    {
        $data_menu = $this->menu_model->get();
        $result = array();
        foreach ($data_menu as $data) {
            $param['MENU_PARENT'] = $data->MENU_PARENT;
            $param['ID'] = $data->ID;
            $param['NAME_ID'] = $data->NAME_ID;
            $param['NAME_EN'] = $data->NAME_EN;
            $param['MENU_LEVEL'] = $data->MENU_LEVEL;

            $result[] = $param;
        }
        return $result;
    }

}