<?php
function slugify($str, $replace=array('rsquo'), $delimiter='-')
{
    setlocale(LC_ALL, 'en_US.UTF8');

    if( !empty($replace) )
    {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}

function getIdFromSlug($link, $col, $rows){
    $fIndex = NULL;

    foreach ($rows as $key => $value) {
        if (slugify($value->$col) == $link) {
            $fIndex = $key;
            break;
        }
    }

    if(isset($fIndex))
        return $rows[$fIndex]->ID;
    else
        return FALSE;
}

function limitKalimat($kalimat, $limit=40){
    $synopsis='';
    if (strpos($kalimat, "</p>")) {
        if (strlen($kalimat) > $limit){
            $synopsis = explode("</p>", $kalimat);
            $synopsis = reset($synopsis);
            $synopsis = substr($synopsis, 0, $limit) . '...</p>';
        }else {
            $synopsis = $kalimat;
            $synopsis = explode("</p>", $synopsis);
            $synopsis = reset($synopsis).'</p>';
        }
    }

    else {
        if (strlen($kalimat) > $limit){
            $synopsis = substr($kalimat, 0, $limit) . '...';
        }
        else {
            $synopsis = $kalimat;
        }
    }
    return $synopsis;
}

function convertDate($source, $cond = 'default'){
    $date = new DateTime($source);
    switch ($cond) {
        case 'tgl':
            $r = $date->format('d');
            break;

        case 'bln':
            $r = $date->format('F');
            break;

        case 'thn':
            $r = $date->format('Y');
            break;

        case 'bthn':
            $r = $date->format('M Y');
            break;

        case 'db':
            $r = $date->format('Y-m-d'); 
            break;

        case 'datepick':
            $r = $date->format('d-M-Y'); 
            break;

        default:
            $r = $date->format('d M Y');
            break;
    }

    return $r;
}

function convertDateLang($source, $cond = ''){
    $Day = date('d', strtotime($source));
    $Month = date('m', strtotime($source));
    $Years = date('Y', strtotime($source));
    $Hours = date('H', strtotime($source));
    $Minute = date('i', strtotime($source));
    $Seconds = date('s', strtotime($source));

    $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
    if($SiteLang == "id"){
        switch ($Month) {
            case '01':
                $month = "Januari";
                break;
            
            case '02':
                $month = "Februari";
                break;
            
            case '03':
                $month = "Maret";
                break;
            
            case '04':
                $month = "April";
                break;
            
            case '05':
                $month = "Mei";
                break;
            
            case '06':
                $month = "Juni";
                break;
            
            case '07':
                $month = "Juli";
                break;
            
            case '08':
                $month = "Agustus";
                break;
            
            case '09':
                $month = "September";
                break;
            
            case '10':
                $month = "Oktober";
                break;
            
            case '11':
                $month = "November";
                break;
            
            case '12':
                $month = "Desember";
                break;
        }
    }else{
        $month = date("F", strtotime($source));
    }

    switch ($cond) {
        case 'd':
        $r = $Day;
        break;

        case 'm':
        $r = $month;
        break;

        case 'dm':
        $r = $Day." ".$month;
        break;

        case 'dmy':
        $r = $Day." ".$month." ".$Years;
        break;

        case 'dmyhis':
        $r = $Day." ".$month." ".$Years." ".$Hours.":".$Minute.":".$Seconds;
        break;
    }
    return $r;
}