function init() {
	// Image lightbox
	$('[data-popup="lightbox"]').fancybox({
		padding: 3,
	});
}
$("#tableData thead th").each(function () {
	var title = $(this).text();
	if (title == "Action" || title == "Image" || title == "Video") {
	} else if (title == "Type Log") {
		$(this).append(
			'<br><select onchange="changeSelectType(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="UPDATE">UPDATE</option><option value="INSERT">INSERT</option></select>'
		);
	} else if (title == "Language") {
		$(this).append(
			'<br><select onchange="changeSelectLang(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="Indonesia">Indonesia</option><option value="English">English</option></select>'
		);
	} else if (title == "Article") {
		$(this).append(
			'<br><select onchange="changeSelectArticle(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
		);
	} else if (title == "Highlight") {
		$(this).append(
			'<br><select onchange="changeSelectHighlight(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
		);
	} else if (title == "Footer") {
		$(this).append(
			'<br><select onchange="changeSelectFooter(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
		);
	} else if (title == "Status") {
		$(this).append(
			'<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option><option value="99">Publish</option></select>'
		);
	} else if (title == "Status Active") {
		$(this).append(
			'<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option></select>'
		);
	} else if (title == "Status Approve") {
		$(this).append(
			'<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Not Approved</option><option value="99">Approved</></select>'
		);
	} else if (title == "Menu Level") {
		$(this).append(
			'<br><select onchange="changeSelectLevel(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="Menu Parent">Menu Parent</option><option value="Menu Child">Menu Child</></select>'
		);
	} else if (title == "Menu Position") {
		$(this).append(
			'<br><select onchange="changeSelectPosition(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="1">Header</option><option value="2">Footer</option><option value="3">Header & Footer</option></select>'
		);
	} else if (title == "Created Date") {
		$(this).append(
			'<br> <div class="input-group search-date" style="color:#333">' +
				'<span class="input-group-prepend">' +
				'<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
				"</span>" +
				'<input type="text" onchange="changeSelectDate(this)" name="created_date" id="created_date" class="form-control pickadate-selectors" placeholder="Select Date..">' +
				"</div>"
		);
		$(".pickadate-selectors").pickadate({
			selectYears: true,
			selectMonths: true,
			format: "dd mmm yyyy",
			formatSubmit: "dd mmm yyyy",
			hiddenName: true,
		});
	} else if (title == "Approved Date") {
		$(this).append(
			'<br> <div class="input-group search-date" style="color:#333">' +
				'<span class="input-group-prepend">' +
				'<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
				"</span>" +
				'<input type="text" onchange="changeSelectApprovedDate(this)" name="effective_date" id="approved_date" class="form-control approved-date" placeholder="Select Date..">' +
				"</div>"
		);
		$(".approved-date").pickadate({
			selectYears: true,
			selectMonths: true,
			format: "dd mmm yyyy",
			formatSubmit: "dd mmm yyyy",
			hiddenName: true,
		});
	} else if (title == "Effective Date" || title == "Date") {
		$(this).append(
			'<br> <div class="input-group search-date" style="color:#333">' +
				'<span class="input-group-prepend">' +
				'<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
				"</span>" +
				'<input type="text" onchange="changeSelectEffectiveDate(this)" name="effective_date" id="effective_date" class="form-control effective-date" placeholder="Select Date..">' +
				"</div>"
		);
		$(".effective-date").pickadate({
			selectYears: true,
			selectMonths: true,
			format: "dd mmm yyyy",
			formatSubmit: "dd mmm yyyy",
			hiddenName: true,
		});
	} else {
		$(this).append(
			'<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
		);
	}
});
var t = $("#tableData").DataTable({
	initComplete: function (settings, json) {
		init();
	},
	serverSide: false,
	autoWidth: false,
	keys: !0,
	order: [],
	autofill: true,
	select: true,
	responsive: true,
	// scrollX: true,
	// scrollCollapse: true,
	// fixedColumns: {
	// leftColumns: 1,
	// rightColumns: 1,
	// },
	buttons: true,
	dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',
});

t.columns().every(function () {
	var that = this;
	$("input", this.header()).on("keyup", function (e) {
		if (event.keyCode === 8) {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		} else {
			if (this.value.length >= 1) {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			}
		}
	});
});

$("#tableData thead input").on("click", function (e) {
	e.stopPropagation();
});
