<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function tahunan()
	{
		$post = array();
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$link = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$category_id = 1;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		// echo "<pre>";
		// print_r($provideData);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function bulanan()
	{
		$category_id = 2;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function keuangan_publikasi()
	{
		$category_id = 3;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function keuangan_interim()
	{
		$category_id = 4;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function keuangan_konsolidasi()
	{
		$category_id = 5;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function pengungkapan_informasi_kuantitatif()
	{
		$category_id = 6;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function total_eksposur()
	{
		$category_id = 7;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function perhitungan_rasio_pengungkit()
	{
		$category_id = 8;

		$provideData = $this->provideData($category_id);

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "laporan";
		$this->data['categoryLaporan'] = $category_id;
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;

		$this->load->view('template', $this->data);
	}

	public function list_lapor()
	{
		// $post = $_POST;
		$param = $_POST['param'];

		// echo json_encode($post);
		// exit;

		// --------PAGING--------- //
		$page = 1;
		$perPage = 10;
		if(isset($param['paramPage'])) $page = $param['paramPage'];
		if(isset($param['paramPerPage'])) $perPage = $param['paramPerPage'];
		$offset = ($page - 1) * $perPage;
		// --------PAGING--------- //

		// ---- SEARCH ----- //
		$search = "";
		if(isset($param['paramSearch'])) $search = $param['paramSearch'];
		// ---- SEARCH ----- //

		$where['STATUS'] = '99';
		$where['CATEGORY'] = $param['paramCategory'];

		$like = array();
		if(!empty($search)){
			$like[$this->cekLang('REPORT_NAME')] = $search;
		}

		$getLaporan = $this->db->select("*")
		->where($where)
		->like($like)
		->from('MS_REPORT')
		->order_by('ID_REPORT', 'DESC')
		->limit($perPage, $offset)
		->get()
		->result_array();

		$total_laporan = $this->db->select("*")
		->where($where)
		->like($like)
		->from('MS_REPORT')
		// ->limit($perPage, $offset)
		->get()
		->num_rows();

		$totalPerPage = $perPage;
		if(sizeof($getLaporan) < $totalPerPage){
			$totalPerPage = sizeof($getLaporan);
		}
		
		$recFrom = $offset + 1;
		$recTo = ($recFrom - 1) + $totalPerPage;
		$record = $recFrom .'-'. $recTo;
		if($recFrom == $recTo){
			$record = $recFrom.'';
		}

		foreach ($getLaporan as $key => $value) {

				$getLaporan[$key]['REPORT_NAME'] = $value[$this->cekLang('REPORT_NAME')];
				$getLaporan[$key]['LINK'] = $value[$this->cekLang('LINK')];

				$fileName = explode('/', $value[$this->cekLang('LINK')]);
				$fileName = $fileName[sizeof($fileName) - 1];

				$fileExt = explode('.', $fileName);
				$fileExt = $fileExt[sizeof($fileExt) - 1];

				$getLaporan[$key]['FILE_NAME'] = $fileName;
				$getLaporan[$key]['FILE_EXT'] = $fileExt;
		}

		$result = array(
			'getLaporan' => $getLaporan,
			'totalLaporan' => $total_laporan,
			'record' => $record
		);

		echo json_encode($result);
	}

	function provideData($category_id)
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$GetModule = $this->Module_model->getmodule($linkingg);
		$PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
		$MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";

		// MENU PARENT -1
		$Q_MP = $this->db->query("SELECT * FROM MS_MENU WHERE ID = '$MENU_PARENT'")->row_array();
		if ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) {
			// $MENU_PARENT = ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) ? $Q_MP['MENU_PARENT'] : "";
			$MENU_PARENT = $Q_MP['MENU_PARENT'];
		}

		$ID_MENU_PARENT = $MENU_PARENT;
		$getMenuLeft = array();
		$footerContentEn = "";
		$footerContentId = "";
		$getMenu1 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->result_array();
		$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->row_array();

		if(($getMenu2['FOOTER_NOTE_EN'] != '') && ($getMenu2['FOOTER_NOTE_ID'] != '')){
			$footerContentEn = $getMenu2['FOOTER_NOTE_EN'];
			$footerContentId = $getMenu2['FOOTER_NOTE_ID'];
		}

		foreach ($getMenu1 as $key => $value) {
			$getMenuLeft[$key]['menu_1']['NAME'] = $value[$this->cekLang('NAME')];
			$getMenuLeft[$key]['menu_1']['LINK'] = $value[$this->cekLang('LINK')];

			// cek aktif ga menu 1 single
			$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = ( $value[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

			$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value['ID'])->result_array();

			if (count($getMenu2) > 0) {
				// deklarasi aktif ga menu 1 multiple
				$stakMenu2 = array();
				$linkMenu1 = $value[$this->cekLang('LINK')];

				foreach ($getMenu2 as $key2 => $value2) {
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['NAME'] = $value2[$this->cekLang('NAME')];
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['LINK'] = $value2[$this->cekLang('LINK')];

					// cek aktif ga menu single
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = ( $value2[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

					$getMenu3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value2['ID'])->result_array();

					if (count($getMenu3) > 0) {
						// deklarasi aktif ga menu 2 multiple
						$stakMenu3 = array();
						$linkMenu2 = $value2[$this->cekLang('LINK')];

						foreach ($getMenu3 as $key3 => $value3) {
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['NAME'] =
							 	$value3[$this->cekLang('NAME')];
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['LINK'] =
							 	$value3[$this->cekLang('LINK')];

							 $getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['ACTIVE_URL'] = ( $value3[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

							$stakMenu3[] = $value3[$this->cekLang('LINK')];
						}

						// cek aktif ga menu 2 multiple
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = (in_array($linkingg, $stakMenu3)) ? TRUE : FALSE;

					}else{
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'] = array();
					}

					$stakMenu2[] = $value2[$this->cekLang('LINK')];
				}

				// cek aktif ga menu 1 multiple
				$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = (in_array($linkingg, $stakMenu2)) ? TRUE : FALSE;

			}else{
				$getMenuLeft[$key]['menu_1']['menu_2'] = array();
			}
		}

		$getMenuDetail = $this->db->query("SELECT * FROM MS_MENU WHERE LINK_ID = '".$linkingg."' OR LINK_EN = '".$linkingg."'")->row_array();
		$getMenuDetail['FOOTER_NOTE'] = $getMenuDetail[$this->cekLang('FOOTER_NOTE')];
		$getMenuDetail['FILE_BANNER'] = $getMenuDetail[$this->cekLang('FILE_BANNER')];

		// return array('totalLaporan' => $total_laporan, 'laporan' => $getLaporan, 'menuLeft' => $getMenuLeft, 'menuDetail' => $getMenuDetail, 'footerContentId' => $footerContentId, 'footerContentEn'=>$footerContentEn);
		return array('menuLeft' => $getMenuLeft, 'menuDetail' => $getMenuDetail, 'footerContentId' => $footerContentId, 'footerContentEn'=>$footerContentEn);
		// return array('laporan' => $getLaporan, 'menuLeft' => $getMenuLeft);
	}

	function cekLang($name)
	{
		if ($_SESSION['lang'] == 'id') {
			$nameLang = $name.'_ID';
		}else{
			$nameLang = $name.'_EN';
		}

		return $nameLang;
	}
}