<style>
.field-icon {
    position: absolute;
    top: 0;
    color: #333;
    padding-left: .875rem;
    padding-right: .875rem;
    line-height: calc(1.5385em + .875rem + 2px);
    min-width: 1rem;
    right: 0;
}

/* .container {
    padding-top: 50px;
    margin: auto;
} */
</style>
<?php
$group = isset($rows['group']) ? $rows['group'] : array();
?>
<div class="form-group row">
    <label for="username" class="col-sm-3 col-form-label">Username<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="username" required="" pattern="/^([a-zA-Z0-9 ])+$/" data-parsley-maxlength="15"
            data-parsley-minlength="2" data-parsley-required-message="Username is required."
            data-parsley-pattern-message="Username cannot contain any special characters."
            data-parsley-maxlength-message="Username is too long. It should have 15 characters or fewer."
            data-parsley-maxlength-message="Username is too short. It should have 2 characters or more."
            placeholder="Enter Username" class="form-control" id="username">
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-3 col-form-label">Name<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="name" required="" pattern="/^([a-zA-Z0-9 ])+$/" data-parsley-maxlength="50"
            data-parsley-minlength="2" data-parsley-pattern-message="Name cannot contain any special characters."
            data-parsley-required-message="Name is required."
            data-parsley-maxlength-message="Name is too long. It should have 50 characters or fewer."
            data-parsley-maxlength-message="Name is too short. It should have 2 characters or more."
            placeholder="Enter Name" class="form-control" id="name">
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-sm-3 col-form-label">Email<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="email" name="email" required="" data-parsley-type-message="Email should be a valid email."
            data-parsley-required-message="Email is required." placeholder="Enter Email" class="form-control"
            id="email">
    </div>
</div>

<div class="form-group row">
    <label for="group_user" class="col-sm-3 col-form-label">User Group<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <select id="group_user" class="form-control select2modal" name="group_user" required=""
            data-parsley-required-message="User Group is Required">
            <option selected disabled>Choose Group</option>
            <?php
foreach ($group as $groupuser) {
    $ID = isset($groupuser['ID']) ? $groupuser['ID'] : "";
    $USER_GROUP_NAME = isset($groupuser['USER_GROUP_NAME']) ? $groupuser['USER_GROUP_NAME'] : "";
    ?>
            <option value="<?php echo $ID ?>"><?php echo $USER_GROUP_NAME ?></option>
            <?php
}
?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="position" class="col-sm-3 col-form-label">Position<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="position" required="" data-parsley-required-message="Position is required."
            placeholder="Enter Position" class="form-control" id="position">
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword32" class="col-sm-3 col-form-label">Password<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="password" name="password" data-parsley-trigger="focusin focusout"
            class="form-control passwordValidator" id="inputPassword32" placeholder="Password" required=""
            data-parsley-required-message="Password is required">
        <span toggle="#inputPassword32" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
        <span class="text-muted font-size-sm" style="color:red">Must contain at
            least an uppercase letter, a number, and
            a special characters.</span>

    </div>
</div>
<div class="form-group row">
    <label for="inputPassword52" class="col-sm-3 col-form-label">Re Password<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="password" data-parsley-equalto="#inputPassword32" name="retypepassword" class="form-control"
            id="inputPassword52" placeholder="Retype Password" required="" data-parsley-trigger="keyup"
            data-parsley-required-message="Re Password is required"
            data-parsley-equalto-message="Password should be the same.">
        <span toggle="#inputPassword52" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>

        <span class="text-muted font-size-sm" style="color:red">Must contain at
            least an uppercase letter, a number, and
            a special characters.</span>

    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/passwordValidator.js"></script>
<script type="text/javascript">
$(function() {
    $(".select2modal").select2({
        dropdownParent: $('#modal')
    });
});
$(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
</script>