<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Modal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $data = isset($_POST) ? $_POST : "";
        $this->load->view('modals', $data);
    }
}