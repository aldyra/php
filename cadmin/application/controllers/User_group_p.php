<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_group_p extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('usergroupp_model');
        $this->load->model('Groupprivilege_model');
        $this->load->model('Modulep_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "user_group_p/view";
        $data['authorize'] = $this->data['authorize'];

        $this->load->view('templates/view', $data);
    }

    public function get_data()
    {
        header('Content-Type: application/json');
        $col = implode(',', $this->input->post('arr', true));
        $tbl = $this->input->post('tb', true);
        $getAction = $this->data['authorize'];
        $where = array('STATUS' => '88');
        $res = $this->usergroupp_model->get_data($col, $tbl, $getAction, $where);
        $tes = json_decode($res);
        foreach ($tes->data as $row) {
            if ($row->CREATED_DATE != "0000-00-00" && $row->CREATED_DATE != null) {
                $row->CREATED_DATE = convertDate($row->CREATED_DATE);
            } else {
                $row->CREATED_DATE = "";
            }
        }

        echo json_encode($tes);
    }

    public function get_PrivilegeCb_old($uGroupid)
    {

        $listMainMenu = $this->usergroupp_model->getListMM();

        $arraystate = array('opened' => true);
        $data['dataTree'] = array();
        $i = 0;
        foreach ($listMainMenu as $row) {

            // CHECK MAIN SUB MENU
            $resMSM = $this->usergroupp_model->getMSM($row->ID);
            // /CHECK MAIN SUB MENU

            if ($resMSM->num_rows() > 0) {

                $arraychildlvl1 = array();
                foreach ($resMSM->result() as $MSM) {

                    // CHECK SUB SUB MENU
                    $resSSM = $this->usergroupp_model->getSSM($MSM->ID);
                    // /CHECK SUB SUB MENU

                    if ($resSSM->num_rows() > 0) {

                        $arraychildlvl2 = array();
                        foreach ($resSSM->result() as $SSM) {

                            // CHECK MODULE ACTIONS (SSM)
                            $resModAct = $this->usergroupp_model->getModAct($SSM->ID, $uGroupid);
                            // /CHECK MODULE ACTIONS (SSM)

                            $datacheckbox = array(
                                'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $SSM->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" ' . ($resModAct['IS_CREATE'] == 0 ? '' : 'checked') . '>',
                                'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                                'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                                'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" ' . ($resModAct['IS_DELETE'] == 0 ? '' : 'checked') . '>',
                                'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" ' . ($resModAct['IS_EXPORT'] == 0 ? '' : 'checked') . '>',
                                'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" ' . ($resModAct['IS_IMPORT'] == 0 ? '' : 'checked') . '>',
                                'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px;"> Check/Uncheck</button>',
                            );

                            // LEVEL 1 NO CHILD
                            $arraytmpchildlvl2 = array(
                                'text' => $SSM->MODULE_NAME,
                                'type' => 'Default',
                                'state' => $arraystate,
                                'data' => $datacheckbox,
                            );
                            array_push($arraychildlvl2, $arraytmpchildlvl2);
                            $i++;

                        }
                        // LEVEL 1 NO CHILD
                        $arraytmpchildlvl1 = array(
                            'text' => $MSM->MODULE_NAME,
                            'type' => 'Default',
                            'state' => $arraystate,
                            'children' => $arraychildlvl2,
                        );
                        array_push($arraychildlvl1, $arraytmpchildlvl1);

                    } else {

                        // CHECK MODULE ACTIONS (MSM)
                        $resModAct = $this->usergroupp_model->getModAct($MSM->ID, $uGroupid);
                        // /CHECK MODULE ACTIONS (MSM)

                        if ($MSM->MODULE_NAME == 'User Group Privilege') {
                            $datacheckbox = array(
                                'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $MSM->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" disabled>',
                                'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                                'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                                'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" disabled>',
                                'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" disabled>',
                                'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" disabled>',
                                'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px;"> Check/Uncheck</button>',
                            );
                        } elseif ($MSM->MODULE_NAME == 'Module Privilege') {
                            $datacheckbox = array(
                                'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $MSM->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" disabled >',
                                'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                                'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" disabled>',
                                'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" disabled>',
                                'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" disabled>',
                                'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" disabled>',
                                'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px;"> Check/Uncheck</button>',
                            );
                        } else {
                            $datacheckbox = array(
                                'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $MSM->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" ' . ($resModAct['IS_CREATE'] == 0 ? '' : 'checked') . '>',
                                'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                                'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                                'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" ' . ($resModAct['IS_DELETE'] == 0 ? '' : 'checked') . '>',
                                'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" disabled>',
                                'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" disabled>',
                                'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px;"> Check/Uncheck</button>',
                            );
                        }

                        // LEVEL 1 NO CHILD
                        $arraytmpchildlvl1 = array(
                            'text' => $MSM->MODULE_NAME,
                            'type' => 'Default',
                            'state' => $arraystate,
                            'data' => $datacheckbox,
                        );
                        array_push($arraychildlvl1, $arraytmpchildlvl1);
                        $i++;

                    }
                }

                // LEVEL 1 NO CHILD
                $arraytmp = array(
                    'text' => $row->MODULE_NAME,
                    'type' => 'Default',
                    'state' => $arraystate,
                    'children' => $arraychildlvl1,
                );

            } else {

                // CHECK MODULE ACTIONS
                $resModAct = $this->usergroupp_model->getModAct($row->ID, $uGroupid);
                // /CHECK MODULE ACTIONS

                if ($row->MODULE_NAME == "Dashboard") {
                    $datacheckbox = array(
                        'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $row->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" disabled>',
                        'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                        'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '"  disabled>',
                        'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" disabled>',
                        'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" disabled>',
                        'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" disabled>',
                        'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px;"> Check/Uncheck</button>',
                    );
                } else {
                    $datacheckbox = array(
                        'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $row->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" ' . ($resModAct['IS_CREATE'] == 0 ? '' : 'checked') . '>',
                        'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                        'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                        'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" ' . ($resModAct['IS_DELETE'] == 0 ? '' : 'checked') . '>',
                        'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" ' . ($resModAct['IS_EXPORT'] == 0 ? '' : 'checked') . '>',
                        'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" ' . ($resModAct['IS_IMPORT'] == 0 ? '' : 'checked') . '>',
                        'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px;"> Check/Uncheck</button>',
                    );
                }

                // LEVEL 0 NO CHILD
                $arraytmp = array(
                    'text' => $row->MODULE_NAME,
                    'type' => 'Default',
                    'state' => $arraystate,
                    'data' => $datacheckbox,
                );
                $i++;

            }

            array_push($data['dataTree'], $arraytmp);
        }

        echo json_encode($data);
    }
    public function get_PrivilegeCb($uGroupid, $fnc = false)
    {

        $listMainMenu = $this->Groupprivilege_model->getListMM();

        $arraystate = array('opened' => true);
        $data['dataTree'] = array();
        $i = 0;
        foreach ($listMainMenu as $row) {

            // CHECK MAIN SUB MENU
            $resMSM = $this->Groupprivilege_model->getMSM($row->ID);
            // /CHECK MAIN SUB MENU

            if ($resMSM->num_rows() > 0) {

                $arraychildlvl1 = array();
                foreach ($resMSM->result() as $MSM) {

                    // CHECK SUB SUB MENU
                    $resSSM = $this->Groupprivilege_model->getSSM($MSM->ID);
                    // /CHECK SUB SUB MENU

                    if ($resSSM->num_rows() > 0) {

                        $arraychildlvl2 = array();
                        foreach ($resSSM->result() as $SSM) {

                            // CHECK MODULE ACTIONS (SSM)
                            $resModAct = $this->Groupprivilege_model->getModAct($SSM->ID, $uGroupid);
                            // /CHECK MODULE ACTIONS (SSM)

                            $datacheckbox = array(
                                'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $SSM->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_CREATE'] == 0 ? '' : 'checked') . '>',
                                'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                                'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                                'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_DELETE'] == 0 ? '' : 'checked') . '>',
                                'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_EXPORT'] == 0 ? '' : 'checked') . '>',
                                'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_IMPORT'] == 0 ? '' : 'checked') . '>',
                                'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px; line-height: 20px"> Check/Uncheck</button>',
                            );

                            // LEVEL 1 NO CHILD
                            $arraytmpchildlvl2 = array(
                                'text' => $SSM->MODULE_NAME,
                                'type' => 'Default',
                                'state' => $arraystate,
                                'data' => $datacheckbox,
                            );
                            array_push($arraychildlvl2, $arraytmpchildlvl2);
                            $i++;

                        }
                        // LEVEL 1 NO CHILD
                        $arraytmpchildlvl1 = array(
                            'text' => $MSM->MODULE_NAME,
                            'type' => 'Default',
                            'state' => $arraystate,
                            'children' => $arraychildlvl2,
                        );
                        array_push($arraychildlvl1, $arraytmpchildlvl1);

                    } else {

                        // CHECK MODULE ACTIONS (MSM)
                        $resModAct = $this->Groupprivilege_model->getModAct($MSM->ID, $uGroupid);
                        // /CHECK MODULE ACTIONS (MSM)

                        $datacheckbox = array(
                            'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $MSM->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_CREATE'] == 0 ? '' : 'checked') . '>',
                            'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                            'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                            'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_DELETE'] == 0 ? '' : 'checked') . '>',
                            'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_EXPORT'] == 0 ? '' : 'checked') . '>',
                            'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_IMPORT'] == 0 ? '' : 'checked') . '>',
                            'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px; line-height: 20px"> Check/Uncheck</button>',
                        );

                        // LEVEL 1 NO CHILD
                        $arraytmpchildlvl1 = array(
                            'text' => $MSM->MODULE_NAME,
                            'type' => 'Default',
                            'state' => $arraystate,
                            'data' => $datacheckbox,
                        );
                        array_push($arraychildlvl1, $arraytmpchildlvl1);
                        $i++;

                    }
                }

                // LEVEL 1 NO CHILD
                $arraytmp = array(
                    'text' => $row->MODULE_NAME,
                    'type' => 'Default',
                    'state' => $arraystate,
                    'children' => $arraychildlvl1,
                );

            } else {

                // CHECK MODULE ACTIONS
                $resModAct = $this->Groupprivilege_model->getModAct($row->ID, $uGroupid);
                // /CHECK MODULE ACTIONS

                $datacheckbox = array(
                    'Add' => '<input type="hidden" name="moduleMenuID[]" value="' . $resModAct['MODULE_PREVILEGE_ID'] . '"> <input type="hidden" name="MenuID[]" value="' . $row->ID . '"> <input type="checkbox" data-index=' . $i . ' name="addcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_CREATE'] == 0 ? '' : 'checked') . '>',
                    'Read' => '<input type="checkbox" data-index=' . $i . ' name="readcb' . $i . '" onclick="readToggle(' . $i . ', this)" ' . ($resModAct['IS_READ'] == 0 ? '' : 'checked') . '>',
                    'Edit' => '<input type="checkbox" data-index=' . $i . ' name="editcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_UPDATE'] == 0 ? '' : 'checked') . '>',
                    'Delete' => '<input type="checkbox" data-index=' . $i . ' name="delcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_DELETE'] == 0 ? '' : 'checked') . '>',
                    'Export' => '<input type="checkbox" data-index=' . $i . ' name="expcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_EXPORT'] == 0 ? '' : 'checked') . '>',
                    'Import' => '<input type="checkbox" data-index=' . $i . ' name="impcb' . $i . '" onclick="otherToggle(' . $i . ', this)" ' . ($resModAct['IS_IMPORT'] == 0 ? '' : 'checked') . '>',
                    'Btn' => '<button class="btn btn-success waves-effect waves-light" type="button" onclick="chkrow(' . $i . ')" style="padding: 0 3px; font-size: 10px; line-height: 20px"> Check/Uncheck</button>',
                );

                // LEVEL 0 NO CHILD
                $arraytmp = array(
                    'text' => $row->MODULE_NAME,
                    'type' => 'Default',
                    'state' => $arraystate,
                    'data' => $datacheckbox,
                );
                $i++;

            }

            array_push($data['dataTree'], $arraytmp);
        }

        if ($fnc) {
            return $data;
        } else {
            echo json_encode($data);
        }

    }

    public function saveCb()
    {

        if ($this->data['authorize']['is_update']) {
            $this->db->trans_start();

            for ($i = 0; $i < count($this->input->post('moduleMenuID', true)); $i++) {
                if ($this->input->post('moduleMenuID')[$i] == "") {
                    $Cselector = "addcb" . $i;
                    $Rselector = "readcb" . $i;
                    $Uselector = "editcb" . $i;
                    $Dselector = "delcb" . $i;
                    $Eselector = "expcb" . $i;
                    $Iselector = "impcb" . $i;

                    $datainsert = array(
                        'USER_GROUP_ID' => $this->input->post('uGroupCBID', true),
                        'MODULE_PREVILEGE_ID' => $this->input->post('MenuID')[$i],
                        'IS_CREATE' => ($this->input->post($Cselector, true) == "on" ? "1" : "0"),
                        'IS_READ' => ($this->input->post($Rselector, true) == "on" ? "1" : "0"),
                        'IS_UPDATE' => ($this->input->post($Uselector, true) == "on" ? "1" : "0"),
                        'IS_DELETE' => ($this->input->post($Dselector, true) == "on" ? "1" : "0"),
                        'IS_EXPORT' => ($this->input->post($Eselector, true) == "on" ? "1" : "0"),
                        'IS_IMPORT' => ($this->input->post($Iselector, true) == "on" ? "1" : "0"),
                        'CREATED_DATE' => date('Y-m-d H:i:s'),
                        'CREATED_BY' => $_SESSION['username'],
                        'DATE_LOG' => date('Y-m-d H:i:s'),
                        'USER_LOG' => $_SESSION['username'],
                    );

                    // query INSERT
                    $this->db->insert("MS_USER_GROUP_PREVILEGE", $datainsert);

                } else {
                    $Cselector = "addcb" . $i;
                    $Rselector = "readcb" . $i;
                    $Uselector = "editcb" . $i;
                    $Dselector = "delcb" . $i;
                    $Eselector = "expcb" . $i;
                    $Iselector = "impcb" . $i;
                    $dataupdate = array(
                        'USER_GROUP_ID' => $this->input->post('uGroupCBID', true),
                        'MODULE_PREVILEGE_ID' => $this->input->post('MenuID')[$i],
                        'IS_CREATE' => ($this->input->post($Cselector, true) == "on" ? "1" : "0"),
                        'IS_READ' => ($this->input->post($Rselector, true) == "on" ? "1" : "0"),
                        'IS_UPDATE' => ($this->input->post($Uselector, true) == "on" ? "1" : "0"),
                        'IS_DELETE' => ($this->input->post($Dselector, true) == "on" ? "1" : "0"),
                        'IS_EXPORT' => ($this->input->post($Eselector, true) == "on" ? "1" : "0"),
                        'IS_IMPORT' => ($this->input->post($Iselector, true) == "on" ? "1" : "0"),
                        'DATE_LOG' => date('Y-m-d H:i:s'),
                        'USER_LOG' => $_SESSION['username'],
                    );

                    // query UPDATE
                    $this->db->where("USER_GROUP_ID", $this->input->post('uGroupCBID', true));
                    $this->db->where("MODULE_PREVILEGE_ID", $this->input->post('MenuID')[$i]);
                    $this->db->update("MS_USER_GROUP_PREVILEGE", $dataupdate);
                }
            }

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $result = 'Save User Group Privilege Failed. Please Try again';
            } else {
                $this->db->trans_commit();
                $result = 'Save User Group Privilege Success';
            }

        } else {
            $result = 'Save User Group Privilege Failed. You dont have access';
        }

        echo json_encode($result);

    }

    public function get_edit($id)
    {
        $res = $this->usergroup_model->get($id);
        echo json_encode($res);
    }

    public function create_action()
    {

        $cek = $this->usergroup_model->cekDuplicate($this->input->post('user_group_name', true));
        if ($cek) {
            $datainsert = array(
                'USER_GROUP_NAME' => $this->input->post('user_group_name', true),
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            );
            // insert user group table
            $id_group = $this->usergroup_model->insert($datainsert);

            // group privilege
            $datainsert = array(
                'USER_GROUP_ID' => $id_group,
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
                'DATE_LOG' => date('Y-m-d H:i:s'),
                'USER_LOG' => $_SESSION['username'],
            );

            $insGroupP = $this->usergroupp_model->insert($datainsert);
            $insGroupP ? $res = "success" : $res = "failed";
        } else {
            $res = "0: Duplicate Group Name";
        }

        echo json_encode($res);
    }

    public function update_action()
    {

        $id = $this->input->post('IDUserGroup', true);

        $dataupdate = array(
            'USER_GROUP_NAME' => $this->input->post('user_group_name', true),
            'DATE_LOG' => date('Y-m-d H:i:s'),
            'USER_LOG' => $_SESSION['username'],
        );

        $cek = $this->usergroup_model->cekDuplicate($this->input->post('user_group_name', true), $id);
        if ($cek) {
            $this->usergroup_model->update($dataupdate, array('ID' => $id));
            $res = "Success!";
        } else {
            $res = "Failed: Duplicate Name";
        }

        echo json_encode($res);

    }

    public function delete_action($id)
    {
        $res = $this->usergroup_model->delete_row($id);

        echo json_encode($res);
    }

    public function createmenu()
    {
        $Username = isset($_SESSION['username']) ? $_SESSION['username'] : "admin";

        $menu0 = $this->Modulep_model->getModule($Username);
        $menu = array();
        foreach ($menu0 as $menu_0) {
            $ID = isset($menu_0->ID) ? $menu_0->ID : "";
            $CONTROLLER = isset($menu_0->CONTROLLER) ? $menu_0->CONTROLLER : "";
            $CONTROLLER = str_replace(" ", "", $CONTROLLER);
            $ICON = isset($menu_0->ICON) ? $menu_0->ICON : "";
            // $ICON = str_replace(" ", "", $ICON);
            $FUNCTION = isset($menu_0->FUNCTION) ? $menu_0->FUNCTION : "";
            $FUNCTION = str_replace(" ", "", $FUNCTION);
            $MODULE_NAME = isset($menu_0->MODULE_NAME) ? $menu_0->MODULE_NAME : "";

            if ($CONTROLLER != "" && $FUNCTION != "" && $ICON != "") {
                $parent['ID'] = $ID;
                $parent['MODULE_NAME'] = $MODULE_NAME;
                $parent['CONTROLLER'] = $CONTROLLER;
                $parent['FUNCTION'] = $FUNCTION;
                $parent['ICON'] = $ICON;
                $parent['SUBMENU'] = array();
                $menu[] = $parent;
            } else {
                $menu1 = $this->Modulep_model->getModule($_SESSION['username'], array('MENU_LEVEL' => 1, 'MENU_PARENT' => $menu_0->ID));

                $menu_child = array();
                if (!empty(sizeof($menu1))) {
                    foreach ($menu1 as $menu_1) {
                        $child['ID'] = $menu_1->ID;
                        $child['MODULE_NAME'] = $menu_1->MODULE_NAME;
                        $child['CONTROLLER'] = $menu_1->CONTROLLER;
                        $child['FUNCTION'] = $menu_1->FUNCTION;
                        $child['ICON'] = $menu_1->ICON;
                        $menu_child[] = $child;
                    }
                    $parent['ID'] = $ID;
                    $parent['MODULE_NAME'] = $MODULE_NAME;
                    $parent['CONTROLLER'] = $CONTROLLER;
                    $parent['FUNCTION'] = $FUNCTION;
                    $parent['ICON'] = $ICON;
                    $parent['SUBMENU'] = $menu_child;
                    $menu[] = $parent;
                }
            }
        }
        $_SESSION['menu'] = $menu;
        return $menu;
    }

}