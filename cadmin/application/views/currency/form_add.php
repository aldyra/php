<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Currency Code</label><span style="color: orange">*</span><br>
            <input type="text" name="currency_code" class="form-control" id="currency_code" placeholder="">

        </div>
        <div class="form-group">
            <label>Currency Name</label><span style="color: orange">*</span><br>
            <input type="text" name="currency_name" class="form-control" id="currency_name" placeholder="">

        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="1" name="is_highlight" id="is_highlight">
                HIGHLIGHT
            </label>
        </div>
        <div class="form-group">
            <label>Upload Flag</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" required="" />
            <a href="#" onclick="openTnC('<?php echo base_url('currency/tncupload') ?>')">Terms and Conditions
                Upload</a>

        </div>

    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    changeformfile();
});
</script>