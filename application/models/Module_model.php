<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Module_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getmodule($param)
    {
        $Getparent = array();
    	$Sitelang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"id";
        $JSON = array();
        $JSON_PARENT = array();
        $where['STATUS'] = "99";

        $LINK = "";
    	if($Sitelang == "id")
    	{
    		$LINK = "LINK_ID";
    	}elseif($Sitelang == "en")
    	{
    		$LINK = "LINK_EN";
    	}

        $Query = "SELECT * FROM MS_MENU WHERE STATUS = '".$where['STATUS'] ."' AND ".$LINK." = '".$param."' AND POSITION IN (1,2,3) ORDER BY SEQUENCE_HEADER ASC";
        $run = $this->db->query($Query);
        $Getmodule = $run->row_array();

        if(sizeof($Getmodule) > 0)
        {
            $JSON['ID'] = isset($Getmodule['ID']) ? $Getmodule['ID']:"";
            if($Sitelang == "id")
            {
                $JSON['NAME'] = isset($Getmodule['NAME_ID']) ? $Getmodule['NAME_ID']:"";
                $JSON['LINK'] = isset($Getmodule['LINK_ID']) ? $Getmodule['LINK_ID']:"";
                $JSON['FILE_BANNER'] = isset($Getmodule['FILE_BANNER_ID']) ? $Getmodule['FILE_BANNER_ID']:"";

            }elseif($Sitelang == "en")
            {
                $JSON['NAME'] = isset($Getmodule['NAME_EN']) ? $Getmodule['NAME_EN']:"";
                $JSON['LINK'] = isset($Getmodule['LINK_EN']) ? $Getmodule['LINK_EN']:"";
                $JSON['FILE_BANNER'] = isset($Getmodule['FILE_BANNER_EN']) ? $Getmodule['FILE_BANNER_EN']:"";
            }
            $JSON['MENU_PARENT'] = isset($Getmodule['MENU_PARENT']) ? $Getmodule['MENU_PARENT']:"";
            $JSON['IS_CONTENT'] = isset($Getmodule['IS_CONTENT']) ? $Getmodule['IS_CONTENT']:"";

            if($JSON['MENU_PARENT'] != "0"){
                $Getparent = $this->db->where("ID", $Getmodule['MENU_PARENT'])->get('MS_MENU')->row_array();
                $JSON_PARENT['ID'] = isset($Getparent['ID']) ? $Getparent['ID']:"";
                if($Sitelang == "id")
                {
                    $JSON_PARENT['NAME'] = isset($Getparent['NAME_ID']) ? $Getparent['NAME_ID']:"";
                    $JSON_PARENT['LINK'] = isset($Getparent['LINK_ID']) ? $Getparent['LINK_ID']:"";
                    $JSON_PARENT['FILE_BANNER'] = isset($Getparent['FILE_BANNER_ID']) ? $Getparent['FILE_BANNER_ID']:"";
                }elseif($Sitelang == "en")
                {
                    $JSON_PARENT['NAME'] = isset($Getparent['NAME_EN']) ? $Getparent['NAME_EN']:"";
                    $JSON_PARENT['LINK'] = isset($Getparent['LINK_EN']) ? $Getparent['LINK_EN']:"";
                    $JSON_PARENT['FILE_BANNER'] = isset($Getparent['FILE_BANNER_ID']) ? $Getparent['FILE_BANNER_ID']:"";
                }

                $JSON_PARENT['MENU_PARENT'] = isset($Getparent['MENU_PARENT']) ? $Getparent['MENU_PARENT']:"";
                $JSON_PARENT['IS_CONTENT'] = isset($Getparent['IS_CONTENT']) ? $Getparent['IS_CONTENT']:"";
            }
            $JSON['PARENT'] = $JSON_PARENT;
        }
    	return $JSON;
    }

    public function getleftmodule($param){
        $REQUEST_URI = $_SERVER['REQUEST_URI'];
        $ExpLing = explode("/", $REQUEST_URI);
        $linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

        $Sitelang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $JSON = array();
        $ROW = array();
        $CHILD2 = array();
        
        $where['STATUS'] = "99";
        $where['MENU_PARENT'] = $param;

        $Query = $this->db->query("SELECT * FROM MS_MENU WHERE STATUS = '".$where['STATUS'] ."' AND MENU_PARENT = '".$param."' AND POSITION IN (1,3) ORDER BY SEQUENCE_HEADER ASC");
        $run = $Query->result_array();
        foreach($run as $row){
            $CHILD = array();
            $STAK_MENU2 = array();

            $ROW['ID'] = isset($row['ID']) ? $row['ID']:"";
            if($Sitelang == "id")
            {
                $ROW['NAME'] = isset($row['NAME_ID']) ? $row['NAME_ID']:"";
                $ROW['LINK'] = isset($row['LINK_ID']) ? $row['LINK_ID']:"";
            }elseif($Sitelang == "en")
            {
                $ROW['NAME'] = isset($row['NAME_EN']) ? $row['NAME_EN']:"";
                $ROW['LINK'] = isset($row['LINK_EN']) ? $row['LINK_EN']:"";
            }
            $ROW['MENU_PARENT'] = isset($row['MENU_PARENT']) ? $row['MENU_PARENT']:"";
            $ROW['IS_CONTENT'] = isset($row['IS_CONTENT']) ? $row['IS_CONTENT']:"";

            $Query2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
            $run2 = $Query2->result_array();
            foreach($run2 as $row2){
                $CHILD3 = array();
                $CHILD2['ID'] = isset($row2['ID']) ? $row2['ID']:"";
                if($Sitelang == "id")
                {
                    $CHILD2['NAME'] = isset($row2['NAME_ID']) ? $row2['NAME_ID']:"";
                    $CHILD2['LINK'] = isset($row2['LINK_ID']) ? $row2['LINK_ID']:"";
                }elseif($Sitelang == "en")
                {
                    $CHILD2['NAME'] = isset($row2['NAME_EN']) ? $row2['NAME_EN']:"";
                    $CHILD2['LINK'] = isset($row2['LINK_EN']) ? $row2['LINK_EN']:"";
                }

                $CHILD2['ACTIVE_URL'] = ($linkingg == $CHILD2['LINK']) ? TRUE : FALSE;

                $STAK_MENU2[] = $CHILD2['LINK'];

                $CHILD2['MENU_PARENT'] = isset($row2['MENU_PARENT']) ? $row2['MENU_PARENT']:"";
                $CHILD2['IS_CONTENT'] = isset($row2['IS_CONTENT']) ? $row2['IS_CONTENT']:"";
                if($row2['MENU_PARENT'] == $row['ID']){
                    $Query3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row2['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
                    $run3 = $Query3->result_array();
                    foreach($run3 as $row3){
                        $CHILD4 = array();
                        $ROW3['ID'] = isset($row3['ID']) ? $row3['ID']:"";
                        if($Sitelang == "id")
                        {
                            $ROW3['NAME'] = isset($row3['NAME_ID']) ? $row3['NAME_ID']:"";
                            $ROW3['LINK'] = isset($row3['LINK_ID']) ? $row3['LINK_ID']:"";
                        }elseif($Sitelang == "en")
                        {
                            $ROW3['NAME'] = isset($row3['NAME_EN']) ? $row3['NAME_EN']:"";
                            $ROW3['LINK'] = isset($row3['LINK_EN']) ? $row3['LINK_EN']:"";
                        }
                        $ROW3['MENU_PARENT'] = isset($row3['MENU_PARENT']) ? $row3['MENU_PARENT']:"";
                        $ROW3['IS_CONTENT'] = isset($row3['IS_CONTENT']) ? $row3['IS_CONTENT']:"";
                        if($row3['MENU_PARENT'] == $row2['ID']){
                            $Query4 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row3['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
                            $run4 = $Query4->result_array();
                            foreach($run4 as $row4){
                                $ROW4['ID'] = isset($row4['ID']) ? $row4['ID']:"";
                                if($Sitelang == "id")
                                {
                                    $ROW4['NAME'] = isset($row4['NAME_ID']) ? $row4['NAME_ID']:"";
                                    $ROW4['LINK'] = isset($row4['LINK_ID']) ? $row4['LINK_ID']:"";
                                }elseif($Sitelang == "en")
                                {
                                    $ROW4['NAME'] = isset($row4['NAME_EN']) ? $row4['NAME_EN']:"";
                                    $ROW4['LINK'] = isset($row4['LINK_EN']) ? $row4['LINK_EN']:"";
                                }
                                $ROW4['MENU_PARENT'] = isset($row4['MENU_PARENT']) ? $row4['MENU_PARENT']:"";
                                $ROW4['IS_CONTENT'] = isset($row4['IS_CONTENT']) ? $row4['IS_CONTENT']:"";
                                if($row4['MENU_PARENT'] == $row4['ID']){
                                    $CHILD4[] = $ROW4;
                                }
                            }
                            $ROW3['CHILD'] = $CHILD4;
                            $CHILD3[] = $ROW3;
                        }
                    }
                    $CHILD2['CHILD'] = $CHILD3;
                    $CHILD[] = $CHILD2;
                } 
            }
            $ROW['CHILD'] = $CHILD;

            if (count($STAK_MENU2) > 0) {
                $ROW['ACTIVE_URL'] = (in_array($linkingg, $STAK_MENU2)) ? TRUE : FALSE;
            }else{
                $ROW['ACTIVE_URL'] = ($linkingg == $ROW['LINK']) ? TRUE : FALSE;
            }
            
            $JSON[] = $ROW;
        }
        return $JSON;
    }

    public function getallmodule(){
        $Sitelang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $where['STATUS'] = "99";
        $where['MENU_PARENT'] = "0";
        $Query = $this->db->query("SELECT * FROM MS_MENU WHERE STATUS = '".$where['STATUS'] ."' AND MENU_PARENT = '0' AND POSITION IN (1,3) ORDER BY SEQUENCE_HEADER ASC");
        $Row = $Query->result_array();
        $JSON = array();

        foreach($Row as $row){
            $CHILD = array();

            $ROW['ID'] = isset($row['ID']) ? $row['ID']:"";
            if($Sitelang == "id")
            {
                $ROW['NAME'] = isset($row['NAME_ID']) ? $row['NAME_ID']:"";
                $ROW['LINK'] = isset($row['LINK_ID']) ? $row['LINK_ID']:"";
                $ROW['FILE_BANNER'] = isset($row['FILE_BANNER_ID']) ? $row['FILE_BANNER_ID']:"";
            }elseif($Sitelang == "en")
            {
                $ROW['NAME'] = isset($row['NAME_EN']) ? $row['NAME_EN']:"";
                $ROW['LINK'] = isset($row['LINK_EN']) ? $row['LINK_EN']:"";
                $ROW['FILE_BANNER'] = isset($row['FILE_BANNER_EN']) ? $row['FILE_BANNER_EN']:"";
            }
            $ROW['MENU_PARENT'] = isset($row['MENU_PARENT']) ? $row['MENU_PARENT']:"";

            $Query2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
            $Row2 = $Query2->result_array();
            foreach($Row2 as $row2){
                $ROW2['ID'] = isset($row2['ID']) ? $row2['ID']:"";
                if($Sitelang == "id")
                {
                    $ROW2['NAME'] = isset($row2['NAME_ID']) ? $row2['NAME_ID']:"";
                    $ROW2['LINK'] = isset($row2['LINK_ID']) ? $row2['LINK_ID']:"";
                    $ROW2['FILE_BANNER'] = isset($row2['FILE_BANNER_ID']) ? $row2['FILE_BANNER_ID']:"";
                }elseif($Sitelang == "en")
                {
                    $ROW2['NAME'] = isset($row2['NAME_EN']) ? $row2['NAME_EN']:"";
                    $ROW2['LINK'] = isset($row2['LINK_EN']) ? $row2['LINK_EN']:"";
                    $ROW2['FILE_BANNER'] = isset($row2['FILE_BANNER_EN']) ? $row2['FILE_BANNER_EN']:"";
                }
                $ROW2['MENU_PARENT'] = isset($row2['MENU_PARENT']) ? $row2['MENU_PARENT']:"";
                if($row2['MENU_PARENT'] == $row['ID']){
                    $Query3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row2['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
                    $Row3 = $Query3->result_array();
                    $CHILD2 = array();
                    foreach($Row3 as $row3){
                        $ROW3['ID'] = isset($row3['ID']) ? $row3['ID']:"";
                        if($Sitelang == "id")
                        {
                            $ROW3['NAME'] = isset($row3['NAME_ID']) ? $row3['NAME_ID']:"";
                            $ROW3['LINK'] = isset($row3['LINK_ID']) ? $row3['LINK_ID']:"";
                            $ROW3['FILE_BANNER'] = isset($row3['FILE_BANNER_ID']) ? $row3['FILE_BANNER_ID']:"";
                        }elseif($Sitelang == "en")
                        {
                            $ROW3['NAME'] = isset($row3['NAME_EN']) ? $row3['NAME_EN']:"";
                            $ROW3['LINK'] = isset($row3['LINK_EN']) ? $row3['LINK_EN']:"";
                            $ROW3['FILE_BANNER'] = isset($row3['FILE_BANNER_EN']) ? $row3['FILE_BANNER_EN']:"";
                        }
                        $ROW3['MENU_PARENT'] = isset($row3['MENU_PARENT']) ? $row3['MENU_PARENT']:"";
                        
                        if($row3['MENU_PARENT'] == $row2['ID']){
                            $Query4 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row3['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
                            $Row4 = $Query4->result_array();
                            $CHILD3 = array();
                            foreach($Row4 as $row4){
                                $ROW4['ID'] = isset($row4['ID']) ? $row4['ID']:"";
                                if($Sitelang == "id")
                                {
                                    $ROW4['NAME'] = isset($row4['NAME_ID']) ? $row4['NAME_ID']:"";
                                    $ROW4['LINK'] = isset($row4['LINK_ID']) ? $row4['LINK_ID']:"";
                                    $ROW3['FILE_BANNER'] = isset($row4['FILE_BANNER_ID']) ? $row4['FILE_BANNER_ID']:"";
                                }elseif($Sitelang == "en")
                                {
                                    $ROW4['NAME'] = isset($row4['NAME_EN']) ? $row4['NAME_EN']:"";
                                    $ROW4['LINK'] = isset($row4['LINK_EN']) ? $row4['LINK_EN']:"";
                                    $ROW3['FILE_BANNER'] = isset($row4['FILE_BANNER_EN']) ? $row4['FILE_BANNER_EN']:"";
                                }
                                $ROW4['MENU_PARENT'] = isset($row4['MENU_PARENT']) ? $row4['MENU_PARENT']:"";
                                if($row4['MENU_PARENT'] == $row3['ID']){
                                    $Query5 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = '".$row4['ID']."' AND STATUS = '".$where['STATUS']."' ORDER BY SEQUENCE_HEADER ASC");
                                    $Row5 = $Query5->result_array();
                                    $CHILD4 = array();
                                    foreach($Row5 as $row5){
                                        $ROW5['ID'] = isset($row5['ID']) ? $row5['ID']:"";
                                        if($Sitelang == "id")
                                        {
                                            $ROW5['NAME'] = isset($row5['NAME_ID']) ? $row5['NAME_ID']:"";
                                            $ROW5['LINK'] = isset($row5['LINK_ID']) ? $row5['LINK_ID']:"";
                                        }elseif($Sitelang == "en")
                                        {
                                            $ROW5['NAME'] = isset($row5['NAME_EN']) ? $row5['NAME_EN']:"";
                                            $ROW5['LINK'] = isset($row5['LINK_EN']) ? $row5['LINK_EN']:"";
                                        }
                                        $ROW5['MENU_PARENT'] = isset($row5['MENU_PARENT']) ? $row5['MENU_PARENT']:"";
                                        if($row5['MENU_PARENT'] == $row4['ID']){
                                            $CHILD4[] = $ROW5;
                                        }
                                    }
                                    $ROW4['CHILD'] = $CHILD4;
                                    $CHILD3[] = $ROW4;
                                }
                            }
                            $ROW3['CHILD'] = $CHILD3;
                            $CHILD2[] = $ROW3;
                        }
                    }
                    $ROW2['CHILD'] = $CHILD2;
                    $CHILD[] = $ROW2;
                }
            }
            $ROW['CHILD'] = $CHILD;
            $JSON[] = $ROW;
        }
        return $JSON;
    }

    public function getfootermodule(){
        $Sitelang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        
        $JSON = array();
        $Query = $this->db->query("SELECT * FROM MS_MENU WHERE STATUS = '99' AND POSITION IN (2,3) ORDER BY SEQUENCE_FOOTER ASC");
        $Getmodule = $Query->result_array();
        
        foreach($Getmodule as $row){
            $CHILD = array();

            $ROW['ID'] = isset($row['ID']) ? $row['ID']:"";
            if($Sitelang == "id")
            {
                $ROW['NAME'] = isset($row['NAME_ID']) ? $row['NAME_ID']:"";
                $ROW['LINK'] = isset($row['LINK_ID']) ? $row['LINK_ID']:"";
            }elseif($Sitelang == "en")
            {
                $ROW['NAME'] = isset($row['NAME_EN']) ? $row['NAME_EN']:"";
                $ROW['LINK'] = isset($row['LINK_EN']) ? $row['LINK_EN']:"";
            }
            $ROW['MENU_PARENT'] = isset($row['MENU_PARENT']) ? $row['MENU_PARENT']:"";
            $ROW['IS_CONTENT'] = isset($row['IS_CONTENT']) ? $row['IS_CONTENT']:"";
            $JSON[] = $ROW;
        }
        return $JSON;
    }

    public function socialmedia(){
        $JSON = $this->db->where("STATUS", "99")->get('MS_SOCIAL_MEDIA')->result_array();
        return $JSON;
    }

    public function getnavbarlink(){
        $JSON = array();
        $REQUEST_URI = $_SERVER['REQUEST_URI'];
        $ExpLing = explode("/", $REQUEST_URI);
        $Max = sizeof($ExpLing);    
        $link = isset($ExpLing[$Max-2]) ? $ExpLing[$Max-2]:"";
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";

        $GetModule = $this->getmodule($link);
        $PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
        $MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";
        $IS_CONTENT = isset($GetModule['IS_CONTENT']) ? $GetModule['IS_CONTENT']:"";

        if(sizeof($GetModule) > 0){
            $ID_MENU = isset($GetModule['ID']) ? $GetModule['ID']:"";

            $MENU_PARENT = isset($GetModule['PARENT']['NAME']) ? $GetModule['PARENT']['NAME']:"";
            $MENU_PARENT_LINK = isset($GetModule['PARENT']['LINK']) ? $GetModule['PARENT']['LINK']:"";
            $MENU_CHILD = isset($GetModule['NAME']) ? $GetModule['NAME']:"";
            $MENU_CHILD_LINK = isset($GetModule['LINK']) ? $GetModule['LINK']:"";

            $JSON['MENU_PARENT'] = $MENU_PARENT;
            $JSON['MENU_PARENT_LINK'] = $MENU_PARENT_LINK;
            $JSON['MENU_CHILD'] = $MENU_CHILD;
            $JSON['MENU_CHILD_LINK'] = $MENU_CHILD_LINK;
            $JSON['IS_CONTENT'] = $IS_CONTENT;
            $JSON['FILE_CONTENT'] = $link;
        }

        return $JSON;
    }

    public function redirectmodule($param)
    {
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";

        $where2['ID'] = $param;
        $Query2 = $this->db->where($where2)->get("MS_MENU");
        $row2 = $Query2->row_array();

        if($SiteLang == "id")
        {
            $ROW['NAME'] = isset($row2['NAME_ID']) ? $row2['NAME_ID']:"";
            $ROW['LINK'] = isset($row2['LINK_ID']) ? $row2['LINK_ID']:"";
        }elseif($SiteLang == "en")
        {
            $ROW['NAME'] = isset($row2['NAME_EN']) ? $row2['NAME_EN']:"";
            $ROW['LINK'] = isset($row2['LINK_EN']) ? $row2['LINK_EN']:"";
        }

        return $ROW;
    }

    public function cekLang($name)
    {
        if ($_SESSION['site_lang'] == 'id') {
            $nameLang = $name.'_ID';
        }elseif ($_SESSION['site_lang'] == 'en'){
            $nameLang = $name.'_EN';
        }

        return $nameLang;
    }
}