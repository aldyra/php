<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <!-- Forms -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">MENU</div> <i class="icon-menu"
                        title="Forms"></i>
                </li>
                <?php if (isset($_SESSION['navMenu'])) {?>


                <?php foreach ($_SESSION['navMenu'] as $row) {?>

                <?php if ($row['CHILD'] != null) {?>

                <li class="nav-item nav-item-submenu">
                    <a href="<?php echo ($row['LINK'] == '#') ? '#' : base_url($row['LINK']); ?>" class="nav-link"><i
                            class="<?php echo $row['ICON'] ?>"></i><span><?php echo $row['NAME'] ?></span>
                    </a>
                    <ul class="nav nav-group-sub" data-submenu-title="<?php echo $row['NAME'] ?>">
                        <?php foreach ($row['CHILD'] as $MSM) {?>

                        <?php if ($MSM['CHILD'] != null) {?>

                        <li class="nav-item nav-item-submenu">
                            <a href="<?php echo ($MSM['LINK'] == '#') ? '#' : base_url($MSM['LINK']); ?>"
                                class="nav-link"><i class="<?php echo $MSM['ICON'] ?>"></i><?php echo $MSM['NAME'] ?>
                            </a>
                            <ul class="nav nav-group-sub">

                                <?php foreach ($MSM['CHILD'] as $SSM) {?>

                                <li class="nav-item">
                                    <a href="<?php echo ($SSM['LINK'] == '#') ? '#' : base_url($SSM['LINK']); ?>"
                                        class="nav-link"><i
                                            class="<?php echo $MSM['ICON'] ?>"></i><?php echo $SSM['NAME'] ?></a>
                                </li>

                                <?php }?>
                                <!-- /END FOREACH SSM -->

                            </ul>
                        </li>

                        <?php } else {?>

                        <li class="nav-item">
                            <a href="<?php echo ($MSM['LINK'] == '#') ? '#' : base_url($MSM['LINK']); ?>"
                                class="nav-link"><i
                                    class="<?php echo $MSM['ICON'] ?>"></i><?php echo $MSM['NAME'] ?></a>
                        </li>

                        <?php }?>
                        <!-- /END MSM ELSE -->

                        <?php }?>
                        <!-- /END FOREACH MSM -->
                    </ul>

                </li>

                <?php } else {?>

                <li class="nav-item">
                    <a href="<?php echo ($row['LINK'] == '#') ? '#' : base_url($row['LINK']); ?>" class="nav-link"><i
                            class="<?php echo $row['ICON'] ?>"></i><span><?php echo $row['NAME'] ?></span>
                    </a>
                </li>

                <?php }?>
                <!-- /END ELSE -->

                <?php }?>
                <!-- /END FOREACH -->

                <?php }?>

                <!-- /forms -->
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->