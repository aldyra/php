<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Forgot Password - Web Admin</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <link href="<?php echo base_url(); ?>assets/lib/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <!-- /theme JS files -->
    <!-- <script src="<?php echo base_url(); ?>assets/js/public.js"></script> -->
    <!-- Validation js (Parsleyjs) -->
    <script src="<?php echo base_url(); ?>assets/lib/parsleyjs/parsley.min.js"></script>
    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url(); ?>assets/lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- Sweet alert init js-->
    <script src="<?php echo base_url(); ?>assets/lib/sweetalert2/sweet-alerts.init.js"></script>


</head>

<body>
    <?php
$result = isset($DATA['result']) ? $DATA['result'] : "";
$ERROR_CODE = isset($result['ErrorCode']) ? $result['ErrorCode'] : "";
$ERROR_MESSAGE = isset($result['ErrorMessage']) ? $result['ErrorMessage'] : "";
$EMAIL = isset($result['EMAIL']) ? $result['EMAIL'] : "";
$CODE_VERIFICATION = isset($result['CODE_VERIFICATION']) ? $result['CODE_VERIFICATION'] : "";
$EXPIRED_DATE = isset($result['EXPIRED_DATE']) ? $result['EXPIRED_DATE'] : "";
?>
    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">
            <?php if ($ERROR_CODE != "EC:0000") {?>
            <script type="text/javascript">
            $(function() {
                window.location.href = '<?php echo base_url() ?>';
            })
            </script>
            <?php }

if ($ERROR_CODE == "EC:0000") {
    if (strtotime(date('Y-m-d H:i:s')) > strtotime($EXPIRED_DATE)) {
        ?>
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Container -->
                <div class="flex-fill">

                    <!-- Error title -->
                    <div class="text-center mb-3">
                        <h1 class="error-title offline-title">Expired</h1>
                        <h5>Your password change time has ended, please try again!</h5>
                    </div>
                    <!-- /error title -->


                    <!-- Error content -->
                    <div class="row">
                        <div class="col-xl-4 offset-xl-4 col-md-8 offset-md-2">

                            <!-- Buttons -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?php echo base_url() ?>" class="btn btn-primary btn-block"><i
                                            class="icon-home4 mr-2"></i>
                                        Home</a>
                                </div>

                                <div class="col-sm-6">
                                    <a href="<?php echo base_url('forgotpassword') ?>"
                                        class="btn btn-light btn-block mt-3 mt-sm-0"><i class="icon-lock2 mr-2"></i>
                                        Forgot Password</a>
                                </div>
                            </div>
                            <!-- /buttons -->

                        </div>
                    </div>
                    <!-- /error wrapper -->

                </div>
                <!-- /container -->

            </div>
            <!-- /content area -->

            <?php } else {?>
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Password recovery form -->
                <form class="login-form" method="POST" data-url="<?php echo base_url('forgotpassword/process') ?>"
                    name="forgotForm" id="forgotForm">
                    <div class="card mb-0">

                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i
                                    class="icon-lock2 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Password recovery</h5>
                                <span class="d-block text-muted">We'll send you instructions in email</span>
                            </div>
                            <div class="row form-group form-group-feedback form-group-feedback-right">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label style="font-weight: bold;">Email</label>
                                    <input type="email" name="email" id="email" class="form-control"
                                        value="<?php echo $EMAIL; ?>" readonly>
                                    <input type="hidden" name="code_verification" id="code_verification"
                                        class="form-control" value="<?php echo $CODE_VERIFICATION; ?>" readonly>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label style="font-weight: bold;">Password</label>

                                    <label style="font-weight: bold;">
                                        <?php echo $this->lang->line('ov_15') ?>
                                    </label>
                                    <input type="password" name="password_1" class="form-control">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label style="font-weight: bold;">Repeat Password</label>

                                    <label style="font-weight: bold;">
                                        <?php echo $this->lang->line('ov_16') ?>
                                    </label>
                                    <input type="password" name="password_2" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="form-group form-group-feedback form-group-feedback-right">
                                <input type="email" id="email" name="email" class="form-control"
                                    placeholder="Your email">
                                <div class="form-control-feedback">
                                    <i class="icon-mail5 text-muted"></i>
                                </div>
                            </div> -->

                            <button type="submit" class="btn bg-blue btn-block">
                                Reset password</button>
                        </div>
                    </div>
                </form>
                <!-- /password recovery form -->

            </div>
            <!-- /content area -->
            <?php }}?>

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script>
    $('#forgotForm').on('submit', function(e) {
        e.preventDefault();
        $("#forgotForm").parsley().validate();
        var form = $("#forgotForm");
        var data = form.serialize();
        var Url = form.data("url");

        if ($("#forgotForm").parsley().isValid()) {
            $.ajax({
                url: Url,
                type: "POST",
                data: data,
                cache: false,
                dataType: "json",
                beforeSend: function() {},
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 1500);
                    }
                },
            });
        }
    });
    </script>
</body>

</html>