<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jobvacancy extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('jobvacancy_model');
        $this->load->model('group_jobvacancy_model');
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "jobvacancy/view";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['jobvacancy'] = $this->jobvacancy_model->view();
        $this->load->view('templates/view', $data);
    }
    public function add_jobvacancy()
    {
        $data = array();
        $data['content'] = "jobvacancy/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'jobvacancy';
        $data['data']['group_jobvacancy'] = $this->group_jobvacancy_model->get_group();

        $this->load->view('templates/view', $data);
    }
    public function edit_jobvacancy($id)
    {
        $data = array();
        $data['content'] = "jobvacancy/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['jobvacancy'] = $this->jobvacancy_model->get_edit($id);
        $data['data']['back'] = base_url() . 'jobvacancy';

        $this->load->view('templates/view', $data);
    }
    public function detail_jobvacancy($id)
    {
        $data = array();
        $data['content'] = "jobvacancy/form_detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['jobvacancy'] = $this->get_jobvacancy_detail($id);
        $data['data']['back'] = base_url() . 'jobvacancy';

        $this->load->view('templates/view', $data);
    }
    public function get_jobvacancy_detail($id)
    {
        $this->db->select('A.*, B.NAME');
        $this->db->from('MS_JOBVACANCY A');
        $this->db->join('MS_GROUP_JOBVACANCY B', 'A.ID_GROUP_JOBVACANCY = B.ID', 'left');
        $this->db->where('A.ID_JOBVACANCY', $id);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function add()
    {
        $CODE = isset($_POST['code']) ? $_POST['code'] : "";
        $POSITION = isset($_POST['position']) ? $_POST['position'] : "";
        $ID_GROUP_JOBVACANCY = isset($_POST['group_jobvacancy']) ? $_POST['group_jobvacancy'] : "";
        $JOB_DESCRIPTION = isset($_POST['job_description']) ? $_POST['job_description'] : "";
        $REQUIREMENT = isset($_POST['requirement']) ? $_POST['requirement'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";
        $STATUS_JOB = isset($_POST['status_job']) ? $_POST['status_job'] : "";
        $DETAIL = isset($_POST['detail']) ? $_POST['detail'] : "";
        $START_DATE = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : "";
        $END_DATE = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : "";

        $param['CODE'] = $CODE;
        $param['ID_GROUP_JOBVACANCY'] = $ID_GROUP_JOBVACANCY;
        $param['POSITION'] = $POSITION;
        $param['JOB_DESCRIPTION'] = $JOB_DESCRIPTION;
        $param['REQUIREMENT'] = $REQUIREMENT;
        $param['LANGUAGE'] = $LANGUAGE;
        $param['STATUS_JOB'] = $STATUS_JOB;
        $param['DETAIL'] = $DETAIL;

        $param['START_DATE'] = $START_DATE;
        $param['END_DATE'] = $END_DATE;
        $param['CREATED_BY'] = $_SESSION['username'];
        $param['CREATED_DATE'] = date('Y-m-d H:i:s');
        $Insert = $this->jobvacancy_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));
    }
    public function update()
    {
        $ID_JOBVACANCY = isset($_POST['id_jobvacancy']) ? $_POST['id_jobvacancy'] : "";
        $ID_GROUP_JOBVACANCY = isset($_POST['group_jobvacancy']) ? $_POST['group_jobvacancy'] : "";
        $CODE = isset($_POST['code']) ? $_POST['code'] : "";
        $POSITION = isset($_POST['position']) ? $_POST['position'] : "";
        $JOB_DESCRIPTION = isset($_POST['job_description']) ? $_POST['job_description'] : "";
        $STATUS_JOB = isset($_POST['status_job']) ? $_POST['status_job'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";
        $REQUIREMENT = isset($_POST['requirement']) ? $_POST['requirement'] : "0";
        $DETAIL = isset($_POST['detail']) ? $_POST['detail'] : "0";
        $START_DATE = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : "";
        $END_DATE = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : "";

        $param['ID_JOBVACANCY'] = $ID_JOBVACANCY;
        $param['CODE'] = $CODE;
        $param['POSITION'] = $POSITION;
        $param['ID_GROUP_JOBVACANCY'] = $ID_GROUP_JOBVACANCY;
        $param['JOB_DESCRIPTION'] = $JOB_DESCRIPTION;
        $param['STATUS_JOB'] = $STATUS_JOB;
        $param['LANGUAGE'] = $LANGUAGE;
        $param['REQUIREMENT'] = $REQUIREMENT;
        $param['DETAIL'] = $DETAIL;
        $param['START_DATE'] = $START_DATE;
        $param['END_DATE'] = $END_DATE;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $Update = $this->jobvacancy_model->updateaction($param);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID_JOBVACANCY']) ? $Data['ID_JOBVACANCY'] : "";
        $param['ID_JOBVACANCY'] = $ID;
        $Delete = $this->jobvacancy_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }

    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_JOBVACANCY']) ? $Data['ID_JOBVACANCY'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_JOBVACANCY' => $id);
        $Active = $this->jobvacancy_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_JOBVACANCY']) ? $Data['ID_JOBVACANCY'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_JOBVACANCY' => $id);
        $Active = $this->jobvacancy_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_JOBVACANCY']) ? $Data['ID_JOBVACANCY'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_JOBVACANCY' => $id);
        $Active = $this->jobvacancy_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }

}