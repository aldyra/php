<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md sidebar-custom">
    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase line-height-xs">Menu</div> 
                    <i class="icon-menu" title="Main"></i>
                </li>
                <?php
                // $HOST = $_SERVER['REQUEST_URI'];
                // $Exp = explode("/",$HOST);
                // $Max = sizeof($Exp)-1;
                $Controllers = $this->uri->segment(1);

                $menu = isset($_SESSION['menu']) ? $_SESSION['menu']:array();
                foreach($menu as $submenu){
                    $ID = isset($submenu['ID']) ? $submenu['ID']:"";
                    $MODULE_NAME = isset($submenu['MODULE_NAME']) ? $submenu['MODULE_NAME']:"";
                    $CONTROLLER = isset($submenu['CONTROLLER']) ? $submenu['CONTROLLER']:"";
                    $CONTROLLER = str_replace(" ", "", $CONTROLLER);
                    $FUNCTION = isset($submenu['FUNCTION']) ? $submenu['FUNCTION']:"";
                    $FUNCTION = str_replace(" ", "", $FUNCTION);
                    $ICON = isset($submenu['ICON']) ? $submenu['ICON']:"";
                    $SUBMENU = isset($submenu['SUBMENU']) ? $submenu['SUBMENU']:"";

                    $Url = strtolower(base_url().$CONTROLLER);
                    if(empty(sizeof($SUBMENU))){
                        if(strtolower($CONTROLLER) == $Controllers){
                            $active = "active";
                        }else{
                            $active = "";
                        }
                    ?>
                        <li class="nav-item">
                            <a href="<?php echo $Url ?>" class="nav-link <?php echo $active ?>">
                                <i class="<?php echo $ICON; ?>"></i>
                                <span>
                                    <?php echo $MODULE_NAME ?>
                                </span>
                            </a>
                        </li>
                    <?php
                    }else{
                    ?>
                        <li class="nav-item nav-item-submenu">
                            <a href="javascript:void(0)" class="nav-link"><i class="<?php echo $ICON ?>"></i>
                                <span>
                                    <?php echo $MODULE_NAME ?>
                                </span>
                            </a>
                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <?php foreach($SUBMENU as $SUB_MENU): 
                                $Url = strtolower(base_url().$SUB_MENU['CONTROLLER']);
                                if(strtolower($SUB_MENU['CONTROLLER']) == $Controllers){
                                    $active = "active";
                                }else{
                                    $active = "";
                                }
                                ?>
                                <li class="nav-item">
                                    <a href="<?php echo $Url ?>" class="nav-link <?php echo $active ?>"><?php echo $SUB_MENU['MODULE_NAME']?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</div>