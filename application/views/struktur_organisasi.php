<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"> -->
			<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<!-- </a> / -->
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<style type="text/css">
	#myInput {
	    background-image: url(<?=base_url('assets/image/searchicon.png')?>);
	    background-position: 10px 12px;
	    background-repeat: no-repeat;
	    width: 100%;
	    font-size: 16px;
	    padding: 12px 20px 12px 40px;
	    border: 1px solid #ddd;
	    margin-bottom: 12px;
	    max-width: 90%;
	}
	/*.box-laporan{
		padding-left: 1.5rem;
	}*/
	.item-laporan{
		box-shadow: 2px 0px 4px #e5e5e5;
	    max-width: 90%;
	    padding: 12px 15px;
	    display: flex;
	    align-items: center;
	}
	.item-laporan .ext_file{
		text-transform: uppercase;
		padding: 5px;
	    margin-right: .5rem;
	    font-size: 11px;
	    width: 35px;
    	text-align: center;
		color: #fff;
	}
	.item-laporan .ext_file.pdf{
		background: #233F8A;
	}
	.item-laporan .ext_file.other{
		background: #FFD35D;
	}
	div.card-loker{
		box-shadow: 0px 1px 3px 1px #dfdfde;
		cursor: pointer;
	}

	div.card-loker:hover{
		background-color: #28448f;
		color: white;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-sbdk {
		    max-width: 100%;
		}
		.title-laporan{
			margin-top: 1rem;
			font-size: 20px; 
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}
		#myInput, .item-laporan{
			max-width: 100%;
		}
	}
</style>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
            <?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-9 d-padtop-10">
			<span class="mr-3 title-laporan" style="color: #242424;font-weight: bold;margin-bottom: 1.5rem; font-size: 18px;"><?php echo $MENU_CHILD; ?></span><br><br>
			<input type="text" id="myInput" onkeyup="mySearch()" placeholder="Search">
			<div id="myUL" class="box-laporan" style="font-size: 14px;">
				<?php echo $contentData['getContent']['DESCRIPTION']; ?>
			</div>

			<div class="audit-ket mt-4">
				<?php echo $contentData['menuDetail']['FOOTER_NOTE'] ?>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		var getdiv = $('#myUL').find('.item-laporan a');
		$.each(getdiv, function(index, value){
			var text = this.textContent;
			var attr = value.getAttribute('href');
			var spl = attr.split('.');
			var max = spl.length;
			var ext = spl[max-1].toLowerCase();
			var doc = ["pdf", "docx"];
			var img = ["jpeg", "jpg", "png"];
			if(doc.includes(ext)){
				var style = "font-family: Nunito-Light; background:#233F8A;";
			}else if(img.includes(ext)){
				var style = "font-family: Nunito-Light; color: black;";
			}else{
				var style = "font-family: Nunito-Light; background:gray;";
			}

			var html = "<span class='ext_file other' style='"+style+"padding:8px;'>"+ext+"</span>&nbsp;&nbsp; ";
			$(this).prepend($(html));
		});
	});

	function mySearch() {
	    var input, filter, ul, li, a, i, txtValue;
	    input = document.getElementById("myInput");
	    filter = input.value.toUpperCase();
	    ul = document.getElementById("myUL");
	    li = ul.getElementsByTagName("div");
	    for (i = 0; i < li.length; i++) {
	        a = li[i].getElementsByTagName("a")[0];
	        txtValue = a.innerText;
	        if (txtValue.toUpperCase().indexOf(filter) > -1) {
	            li[i].style.display = "";
	        } else {
	            li[i].style.display = "none";
	        }
	    }
	}
</script>