<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authentication extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Authentication_model');
        $this->load->helper('joy_helper');
        $this->load->model('Modulep_model');
    }

    public function index()
    {
        if (empty($_SESSION['iduser'])) {
            $this->load->view('auth/login_view');
        } else if ($_SESSION['pro'] != PROJECT_NAME) {
            $this->load->view('auth/login_view');
        } else {
            redirect(base_url("dashboard"));
        }
    }

    public function checkLastUp($date)
    {
        $today = new DateTime();
        $db_date = new DateTime($date);

        $diff = $today->diff($db_date);

        $hari_ini = $today->format('m');
        $hari_db = $db_date->format('m');

        if ($hari_ini >= $hari_db) {
            if ($diff->m >= 6) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public function doLogin()
    {
        $uname = $this->input->post('uname', true);
        $pass = $this->input->post('pass', true);
        // var_dump($_POST);

        $this->db->where('USERNAME', $uname);
        $user = $this->db->get('MS_USERS')->row();
        // var_dump($user);
        if ($user) {

            $password = md5($this->input->post('pass', true) . $user->HASH);
            // var_dump($password);exit();
            $pass = $password;
            $res = $this->Authentication_model->AuthLogin($uname, $pass);

        } else {
            $res = "Failed: Invalid username or password. Please Try Again";
            $res = false;
            echo json_encode($res);
            exit();

        }

        if ($res) {
            foreach ($res as $data) {
                $sess_username = $data->USERNAME;
                $sess_name = $data->NAME;
                $sess_email = $data->EMAIL;
                $sess_position = $data->POSITION;
                $sess_group = $data->GROUP_ID;
                $sess_iduser = $data->ID;
                $sess_status = $data->STATUS;
                $sess_last_update_password = $data->LAST_UPDATE_PASSWORD;
                $sess_pro = "bumiartha";
            }

            if ($this->checkLastUp($sess_last_update_password)) {
                $res = "Warning: Your password has expired. Please change the password";
                $_SESSION['user'] = $sess_username;
                $_SESSION['old_pass'] = $this->input->post('pass', true);
                $_SESSION['changePass'] = true;
            } else if ($sess_status == "88") {
                if ($sess_group != '' || $sess_group != null) {

                    $resQueryPrivilege = $this->Authentication_model->getPrivilege($uname, $pass);

                    if ($resQueryPrivilege->num_rows() > 0) {

                        $dataPrivilege = array();
                        foreach ($resQueryPrivilege->result() as $row) {
                            $arraytmp = array(
                                'MODULE_NAME' => $row->MODULE_NAME,
                                'IS_CREATE' => $row->IS_CREATE,
                                'IS_READ' => $row->IS_READ,
                                'IS_UPDATE' => $row->IS_UPDATE,
                                'IS_DELETE' => $row->IS_DELETE,
                                'IS_EXPORT' => $row->IS_EXPORT,
                                'IS_IMPORT' => $row->IS_IMPORT,
                            );

                            array_push($dataPrivilege, $arraytmp);
                        }
                        $resPrivilege = $resQueryPrivilege->result();

                        $_SESSION['privilege'] = $dataPrivilege;
                        $_SESSION['username'] = $sess_username;
                        $_SESSION['name'] = $sess_name;
                        $_SESSION['email'] = $sess_email;
                        $_SESSION['position'] = $sess_position;
                        $_SESSION['level'] = $sess_group;
                        $_SESSION['iduser'] = $sess_iduser;
                        $_SESSION['status'] = $sess_status;
                        $_SESSION['pro'] = $sess_pro;
                        $_SESSION['navMenu'] = $this->getNavmenu($resPrivilege);
                        // echo '<pre>', print_r($resPrivilege, 1), '</pre>';
                        // echo '<pre>', print_r($_SESSION['navMenu'], 1), '</pre>';exit();

                        $this->createmenu();
                        $this->removeSessPass();
                    } else {
                        $res = "Failed: Your Account Does Not Have Privilege Yet";
                    }
                }
            } else {
                $res = "Failed: Your Account Not Active.";
            }
        }

        echo json_encode($res);
    }
    public function getNavmenu($resMod)
    {

        $modName = array();
        foreach ($resMod as $listMenu) {
            array_push($modName, $listMenu->MODULE_NAME);
        }

        // var_dump($modName);

        $smodName = "'" . (implode("','", $modName)) . "'";

        $queryMainMenu = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=0 AND ACTIVE=1
							ORDER BY MENU_SEQ ASC";
        $listMainMenu = $this->db->query($queryMainMenu)->result();

        $data['NavMenu'] = array();

        foreach ($listMainMenu as $row) {

            // CHECK MAIN SUB MENU
            $queryMSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=1 AND MENU_PARENT=" . $row->ID . " AND ACTIVE=1 ORDER BY MENU_SEQ ASC";
            $resMSM = $this->db->query($queryMSM);
            // /CHECK MAIN SUB MENU

            if ($resMSM->num_rows() > 0) {

                $arraychildlvl1 = array();
                foreach ($resMSM->result() as $MSM) {

                    // CHECK SUB SUB MENU
                    $querySSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=2 AND MENU_PARENT=" . $MSM->ID . " AND ACTIVE=1 ORDER BY MENU_SEQ ASC";
                    $resSSM = $this->db->query($querySSM);
                    // /CHECK SUB SUB MENU

                    if ($resSSM->num_rows() > 0) {

                        $arraychildlvl2 = array();
                        foreach ($resSSM->result() as $SSM) {

                            // CHECK SUB SUB SUB MENU
                            $querySSSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=3 AND MENU_PARENT=" . $SSM->ID . " AND ACTIVE=1 ORDER BY MENU_SEQ ASC";
                            $resSSSM = $this->db->query($querySSSM);
                            // /CHECK SUB SUB SUB MENU

                            if ($resSSSM->num_rows() > 0) {

                                $arraychildlvl3 = array();
                                foreach ($resSSSM->result() as $SSSM) {
                                    // LEVEL 3
                                    $arraytmpchildlvl3 = array(
                                        'NAME' => $SSSM->MODULE_NAME,
                                        'CONTROLLER' => $SSSM->CONTROLLER,
                                        'LINK' => ($SSSM->CONTROLLER == null) ? '#' : $SSM->CONTROLLER . '/' . $SSM->FUNCTION,
                                        'ICON' => $SSSM->ICON,
                                    );
                                    if (in_array($SSM->MODULE_NAME, $modName)) {
                                        array_push($arraychildlvl3, $arraytmpchildlvl3);
                                    }
                                }

                                // LEVEL 2
                                $arraytmpchildlvl2 = array(
                                    'NAME' => $SSM->MODULE_NAME,
                                    'CONTROLLER' => $SSM->CONTROLLER,
                                    'LINK' => ($SSM->CONTROLLER == null) ? '#' : $SSM->CONTROLLER . '/' . $SSM->FUNCTION,
                                    'ICON' => $SSM->ICON,
                                    'CHILD' => $arraychildlvl3,
                                );
                                if (in_array($SSM->MODULE_NAME, $modName)) {
                                    array_push($arraychildlvl2, $arraytmpchildlvl2);
                                }
                            } else {
                                // LEVEL 2
                                $arraytmpchildlvl2 = array(
                                    'NAME' => $SSM->MODULE_NAME,
                                    'CONTROLLER' => $SSM->CONTROLLER,
                                    'LINK' => ($SSM->CONTROLLER == null) ? '#' : $SSM->CONTROLLER . '/' . $SSM->FUNCTION,
                                    'ICON' => $SSM->ICON,
                                    'CHILD' => null,
                                );
                                if (in_array($SSM->MODULE_NAME, $modName)) {
                                    array_push($arraychildlvl2, $arraytmpchildlvl2);
                                }
                            }

                        }
                        // LEVEL 1
                        $arraytmpchildlvl1 = array(
                            'NAME' => $MSM->MODULE_NAME,
                            'CONTROLLER' => $MSM->CONTROLLER,
                            'LINK' => ($MSM->CONTROLLER == null) ? '#' : $MSM->CONTROLLER . '/' . $MSM->FUNCTION,
                            'ICON' => $MSM->ICON,
                            'CHILD' => $arraychildlvl2,
                        );

                        if (!(empty($arraychildlvl2))) {
                            array_push($arraychildlvl1, $arraytmpchildlvl1);
                        }

                    } else {

                        // LEVEL 1 NO CHILD
                        $arraytmpchildlvl1 = array(
                            'NAME' => $MSM->MODULE_NAME,
                            'CONTROLLER' => $MSM->CONTROLLER,
                            'LINK' => ($MSM->CONTROLLER == null) ? '#' : $MSM->CONTROLLER . '/' . $MSM->FUNCTION,
                            'ICON' => $MSM->ICON,
                            'CHILD' => null,
                        );

                        if (in_array($MSM->MODULE_NAME, $modName)) {
                            array_push($arraychildlvl1, $arraytmpchildlvl1);
                        }

                    }
                }

                // LEVEL 0
                $arraytmp = array(
                    'NAME' => $row->MODULE_NAME,
                    'CONTROLLER' => $row->CONTROLLER,
                    'LINK' => ($row->CONTROLLER == null) ? '#' : $row->CONTROLLER . '/' . $row->FUNCTION,
                    'ICON' => $row->ICON,
                    'CHILD' => $arraychildlvl1,
                );

                if (!(empty($arraychildlvl1))) {
                    array_push($data['NavMenu'], $arraytmp);
                }

            } else {

                // LEVEL 0 NO CHILD
                $arraytmp = array(
                    'NAME' => $row->MODULE_NAME,
                    'CONTROLLER' => $row->CONTROLLER,
                    'LINK' => ($row->CONTROLLER == null) ? '#' : $row->CONTROLLER . '/' . $row->FUNCTION,
                    'ICON' => $row->ICON,
                    'CHILD' => null,
                );

                if (in_array($row->MODULE_NAME, $modName)) {
                    array_push($data['NavMenu'], $arraytmp);
                }

            }
        }

        // echo json_encode($data['NavMenu']);
        // var_dump($data['NavMenu']);

        return $data['NavMenu'];

    }

    public function changePass()
    {
        (isset($_SESSION['user']) && isset($_SESSION['old_pass']) && $_SESSION['changePass']) || redirect(base_url('authentication'));
        $this->load->view('auth/change_pass');
    }

    public function actChangePass()
    {
        $post = $this->input->post(null, true);

        $uname = $_SESSION['user'];
        $old_pass = hash_string($_SESSION['old_pass']);

        $result = $this->Authentication_model->AuthLogin($uname, $old_pass);

        if ($result) {
            if ($old_pass != hash_string($post['new_pass'])) {
                $date_now = date('Y-m-d H:i:s');
                $array_where = array('ID' => $result[0]->ID);
                $array_update = array(
                    'PASSWORD' => hash_string($post['new_pass']),
                    'LAST_UPDATE_PASSWORD' => $date_now,
                    'DATE_LOG' => $date_now,
                    'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                );

                $this->Authentication_model->update($array_update, $array_where);

                $res = "Successfully changed the password!";
                $this->removeSessPass();
            } else {
                $res = "Failed: The new password cannot be the same as the old password.";
            }
        } else {
            $res = "Failed: Can't change password.";
        }

        echo json_encode($res);
    }

    public function removeSessPass()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        if (isset($_SESSION['old_pass'])) {
            unset($_SESSION['old_pass']);
        }

        if (isset($_SESSION['changePass'])) {
            unset($_SESSION['changePass']);
        }

    }

    public function logout()
    {
        unset($_SESSION['username']);
        unset($_SESSION['name']);
        unset($_SESSION['email']);
        unset($_SESSION['position']);
        unset($_SESSION['level']);
        unset($_SESSION['iduser']);
        unset($_SESSION['status']);
        unset($_SESSION['pro']);
        unset($_SESSION['menu']);

        $this->removeSessPass();

        redirect(base_url());
    }

    public function createmenu()
    {
        $Username = isset($_SESSION['username']) ? $_SESSION['username'] : "admin";

        $menu0 = $this->Modulep_model->getModule($Username);
        $menu = array();
        $active_parent = array();
        foreach ($menu0 as $menu_0) {
            $ID = isset($menu_0->ID) ? $menu_0->ID : "";
            $CONTROLLER = isset($menu_0->CONTROLLER) ? $menu_0->CONTROLLER : "";
            $CONTROLLER = str_replace(" ", "", $CONTROLLER);
            $ICON = isset($menu_0->ICON) ? $menu_0->ICON : "";
            $FUNCTION = isset($menu_0->FUNCTION) ? $menu_0->FUNCTION : "";
            $FUNCTION = str_replace(" ", "", $FUNCTION);
            $MODULE_NAME = isset($menu_0->MODULE_NAME) ? $menu_0->MODULE_NAME : "";

            if ($CONTROLLER != "" && $FUNCTION != "" && $ICON != "") {
                $parent['ID'] = $ID;
                $parent['MODULE_NAME'] = $MODULE_NAME;
                $parent['CONTROLLER'] = $CONTROLLER;
                $parent['FUNCTION'] = $FUNCTION;
                $parent['ICON'] = $ICON;
                $parent['SUBMENU'] = array();
                $menu[] = $parent;
            } else {
                $menu1 = $this->Modulep_model->getModule($_SESSION['username'], array('MENU_LEVEL' => 1, 'MENU_PARENT' => $menu_0->ID));

                $menu_child = array();
                $active_parent = pushArray($menu1, 'CONTROLLER');
                if (!empty(sizeof($menu1))) {
                    foreach ($menu1 as $menu_1) {
                        $child['ID'] = $menu_1->ID;
                        $child['MODULE_NAME'] = $menu_1->MODULE_NAME;
                        $child['CONTROLLER'] = $menu_1->CONTROLLER;
                        $child['FUNCTION'] = $menu_1->FUNCTION;
                        $child['ICON'] = $menu_1->ICON;
                        $menu_child[] = $child;
                    }
                    $parent['ID'] = $ID;
                    $parent['MODULE_NAME'] = $MODULE_NAME;
                    $parent['CONTROLLER'] = $CONTROLLER;
                    $parent['FUNCTION'] = $FUNCTION;
                    $parent['ICON'] = $ICON;
                    $parent['SUBMENU'] = $menu_child;
                    $menu[] = $parent;
                }
            }
        }
        $_SESSION['menu'] = $menu;
        $_SESSION['active_parent'] = $active_parent;
        return $menu;
    }

}