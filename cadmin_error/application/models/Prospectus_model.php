<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Prospectus_model extends MY_Model
{

    protected $_table_name = 'MS_PROSPECTUS';
    protected $_primary_key = 'ID_PROSPECTUS';
    protected $_order_by = 'ID_PROSPECTUS';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_publish()
    {
        $this->db->where('STATUS', '99');
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }

    public function updateaction($param, $id)
    {
        $ID = isset($id) ? $id : "";
        $WHERE[$this->_primary_key] = $ID;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_PROSPECTUS'] = isset($param['ID_PROSPECTUS']) ? $param['ID_PROSPECTUS'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

}