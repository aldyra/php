<?php
$param = isset($param) ? $param : "";
$ID_PROSPEKTUS = isset($param['ID_PROSPEKTUS']) ? $param['ID_PROSPEKTUS'] : "";
$LINK_ID = isset($param['LINK_ID']) ? $param['LINK_ID'] : "";
$LINK_EN = isset($param['LINK_EN']) ? $param['LINK_EN'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <input type="hidden" id="id_prospektus" name="id_prospektus" value="<?php echo $ID_PROSPEKTUS ?>">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="form-group">
                    <label>Upload File Indonesia</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_id" name="file_id" class="dropify" value="<?php echo $LINK_ID ?>"
                        data-default-file="<?php echo $LINK_ID ?>" />
                    <input type="hidden" name="file_id_url" id="file_id_url" value="<?php echo $LINK_ID ?>">
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="form-group">
                    <label>Upload File English</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_en" name="file_en" class="dropify" value="<?php echo $LINK_EN ?>"
                        data-default-file="<?php echo $LINK_EN ?>" />
                    <input type="hidden" name="file_en_url" id="file_en_url" value="<?php echo $LINK_EN ?>">

                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    changeformfile();
});
</script>