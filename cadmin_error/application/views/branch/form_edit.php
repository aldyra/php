<!-- Theme JS files -->
<script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>

<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
<!-- /theme JS files -->
<?php
$is_update = $authorize['is_update'];
$province = isset($data['province']) ? $data['province'] : array();
$back = isset($data['back']) ? $data['back'] : '';
$branch = isset($data['branch']) ? $data['branch'] : array();
$ID_BRANCH = isset($data['branch']->ID_BRANCH) ? $data['branch']->ID_BRANCH : '';
$BRANCH_NAME = isset($data['branch']->BRANCH_NAME) ? $data['branch']->BRANCH_NAME : '';
$PHONE_NUMBER = isset($data['branch']->PHONE_NUMBER) ? $data['branch']->PHONE_NUMBER : '';
$FAX_NUMBER = isset($data['branch']->FAX_NUMBER) ? $data['branch']->FAX_NUMBER : '';
$WEBSITE = isset($data['branch']->WEBSITE) ? $data['branch']->WEBSITE : '';
$EMAIL = isset($data['branch']->EMAIL) ? $data['branch']->EMAIL : '';
$ADDRESS = isset($data['branch']->ADDRESS) ? $data['branch']->ADDRESS : '';
$LONGITUDE = isset($data['branch']->LONGITUDE) ? $data['branch']->LONGITUDE : '';
$LATITUDE = isset($data['branch']->LATITUDE) ? $data['branch']->LATITUDE : '';
$CATEGORY = isset($data['branch']->CATEGORY) ? $data['branch']->CATEGORY : '';
$ID_PROVINCE = isset($data['branch']->ID_PROVINCE) ? $data['branch']->ID_PROVINCE : '';
$ID_CITY = isset($data['branch']->ID_CITY) ? $data['branch']->ID_CITY : '';
$ID_KECAMATAN = isset($data['branch']->ID_KECAMATAN) ? $data['branch']->ID_KECAMATAN : '';
$ID_KELURAHAN = isset($data['branch']->ID_KELURAHAN) ? $data['branch']->ID_KELURAHAN : '';

?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Branch</a>
                <a href="#" class="breadcrumb-item active">Edit</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Add Branch</h5>
        </div>

        <div class="card-body">
            <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveBranch">
                <fieldset>
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-file-text2 mr-2"></i>
                        Enter your information
                        <a href="#" class="float-right text-default" data-toggle="collapse" data-target="#demo1">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </legend>

                    <div class="collapse show" id="demo1">
                        <div class="form-group">
                            <input type="hidden" name="id" id="id" value="<?php echo $ID_BRANCH ?>">
                            <label>Branch Name<span style="color:orange">*</span></label>
                            <input type="text" class="form-control" id="branch_name" name="branch_name"
                                placeholder="Branch Name" required="" parsley-trigger="change"
                                value="<?php echo $BRANCH_NAME ?>">
                        </div>
                        <div class="form-group">
                            <label>Category Branch<span style="color:orange">*</span></label>
                            <select data-placeholder="Select category" class="form-control select2" id="category"
                                name="category" required="" parsley-trigger="change">
                                <option disabled selected>Select</option>
                                <option value="1">Kantor Pusat/Head Office</option>
                                <option value="2">Kantor Cabang/Branch Office</option>
                                <option value="3">Kantor Capem/Sub Branch Office</option>
                                <option value="4">Kantor Kas/Cash Offices</option>
                                <option value="5">Payment Point</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number<span style="color:orange">*</span></label>
                                    <input type="text" class="form-control" id="phone_number" name="phone_number"
                                        placeholder="Phone Number" onkeyup="validate(this);" required=""
                                        parsley-trigger="change" value="<?php echo $PHONE_NUMBER ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fax Number</label>
                                    <input type="text" class="form-control" id="fax_number" name="fax_number"
                                        placeholder="Fax Number" onkeyup="validate(this);" parsley-trigger="change"
                                        value="<?php echo $FAX_NUMBER ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Website</label>
                                    <input type="url" class="form-control" id="website" name="website"
                                        placeholder="Website" parsley-trigger="change" value="<?php echo $WEBSITE ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                        parsley-trigger="change" value="<?php echo $EMAIL ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-reading mr-2"></i>
                        Add details
                        <a class="float-right text-default" data-toggle="collapse" data-target="#demo2">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </legend>

                    <div class="collapse show" id="demo2">
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="form-group">
                                    <label for="address">Address<span style="color: orange">*</span></label>
                                    <textarea class="form-control" name="address" id="ADDRESS" rows="3" required=""
                                        parsley-trigger="change"><?php echo $ADDRESS ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Longitude<span style="color: orange">*</span></label>
                                    <input type="text" class="form-control" name="longitude" id="LONGITUDE"
                                        data-parsley-pattern="/^[0-9_ .,-]*$/" data-parsley-trigger="focusin focusout"
                                        required="" placeholder="" value="<?php echo $LONGITUDE ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Latitude<span style="color: orange">*</span></label>
                                    <input type="text" class="form-control" name="latitude" id="LATITUDE"
                                        data-parsley-pattern="/^[0-9_ .,-]*$/" data-parsley-trigger="focusin focusout"
                                        placeholder="" required value="<?php echo $LATITUDE ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Province<span style="color: orange">*</span></label>
                                    <select class="form-control select2" data-live-search="true" name="province"
                                        id="Province" required>
                                        <option disabled selected>Select</option>
                                        <?php foreach ($province as $prov) {?>
                                        <option value="<?php echo $prov->PROVINCE_ID; ?>"
                                            data-id="<?php echo $prov->PROVINCE_ID ?>">
                                            <?php echo $prov->PROVINCE_NAME; ?></option>
                                        <?php }?>
                                    </select>
                                    <input type="hidden" id="PROVINCE_ID" name="province_id">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City<span style="color: orange">*</span></label>
                                    <select class="form-control select2" data-live-search="true" name="city" id="City"
                                        required>
                                        <option disabled selected>Select</option>
                                    </select>
                                    <input type="hidden" id="CITY_KAB_ID" name="city_kab_id">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kecamatan<span style="color: orange">*</span></label>
                                    <select class="form-control select2" data-live-search="true" name="kecamatan"
                                        id="Kecamatan" required data-parsley-errors-container="#validation-error-block">
                                        <option disabled selected>Select</option>
                                    </select>
                                    <div class="" id="validation-error-block"></div>

                                    <input type="hidden" id="KECAMATAN_ID" name="kecamatan_id">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kelurahan<span style="color: orange">*</span></label>
                                    <select class="form-control select2" data-live-search="true" name="kelurahan"
                                        id="Kelurahan" required
                                        data-parsley-errors-container="#validation-error-block2">
                                        <option disabled selected>Select</option>
                                    </select>
                                    <div class="" id="validation-error-block2"></div>

                                    <input type="hidden" id="KELURAHAN_ID" name="kelurahan_id">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Postal Code<span style="color: orange">*</span></label>
                                    <input type="text" class="form-control" name="postal_code" id="POSTAL_CODE"
                                        placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light"> Cancel</button></a>

                </div>
            </form>
        </div>
    </div>
    <?php endif;?>

</div>

<script type="text/javascript">
$('.select2').select2();
$(document).ready(function() {
    $('#category').val("<?php echo $CATEGORY ?>").trigger('change');
    $('#Province').val("<?php echo $ID_PROVINCE ?>").trigger('change');
    $('#City').val("<?php echo $ID_CITY ?>").trigger('change');
    $('#Kecamatan').val("<?php echo $ID_KECAMATAN ?>").trigger('change');
    $('#Kelurahan').val("<?php echo $ID_KELURAHAN ?>").trigger('change');
});
$('#Province').change(function() {
    $("#City").val('');
    $("#Kecamatan").val('');
    $("#Kelurahan").val('');
    var province = $(this).find(':selected').data('id');;
    $("#PROVINCE_ID").val(province);
    var id = $(this).find(':selected').data('id');
    $.ajax({
        url: "<?php echo base_url('branch/getCity'); ?>",
        method: "POST",
        data: {
            id: id
        },
        async: false,
        dataType: 'json',
        success: function(data) {
            var html = '';
            var i;
            html += '<option disabled selected> Select </option>';
            for (i = 0; i < data.length; i++) {

                html += '<option value="' + data[i].CITY_KAB_ID + '" data-id="' + data[i]
                    .CITY_KAB_ID + '">' + data[i].DESCRIPTION + ' ' + data[i].CITY_KAB_NAME +
                    '</option>';
            }
            $('#City').html(html);
            $('#Kecamatan').html('');
            $('#Kelurahan').html('');



        },
        beforeSend: function() {
            $('.loadingBox').show();
        },
        complete: function() {
            $('.loadingBox').fadeOut();
        }
    });
});
$('#City').change(function() {
    $("#Kecamatan").val('');
    $("#Kelurahan").val('');
    var city = $(this).find(':selected').data('id');;
    $("#CITY_KAB_ID").val(city);
    var id = $(this).find(':selected').data('id');
    $.ajax({
        url: "<?php echo base_url('branch/getKecamatan'); ?>",
        method: "POST",
        data: {
            id: id
        },
        async: false,
        dataType: 'json',
        success: function(data) {
            var html = '';
            var i;
            html += '<option disabled selected> Select </option>';
            for (i = 0; i < data.length; i++) {

                html += '<option value="' + data[i].KECAMATAN_ID + '" data-id="' + data[i]
                    .KECAMATAN_ID + '">' + data[i].KECAMATAN_NAME + '</option>';
            }
            $('#Kecamatan').html(html);
            $('#Kelurahan').html('');



        },
        beforeSend: function() {
            $('.loadingBox').show();
        },
        complete: function() {
            $('.loadingBox').fadeOut();
        }
    });
});
$('#Kecamatan').change(function() {
    $("#Kelurahan").val('');
    var kec = $(this).find(':selected').data('id');;
    $("#KECAMATAN_ID").val(kec);
    var id = $(this).find(':selected').data('id');
    $.ajax({
        url: "<?php echo base_url('branch/getKelurahan'); ?>",
        method: "POST",
        data: {
            id: id
        },
        async: false,
        dataType: 'json',
        success: function(data) {
            var html = '';
            var i;
            html += '<option disabled selected> Select </option>';
            for (i = 0; i < data.length; i++) {

                html += '<option value="' + data[i].KELURAHAN_ID + '" data-id="' + data[i]
                    .KELURAHAN_ID + '" data-postalcode="' + data[i].POSTAL_CODE + '">' + data[i]
                    .KELURAHAN_NAME + '</option>';
            }
            $('#Kelurahan').html(html);


        },
        beforeSend: function() {
            $('.loadingBox').show();
        },
        complete: function() {
            $('.loadingBox').fadeOut();
        }
    });
});
$('#Kelurahan').change(function() {
    var kel = $(this).find(':selected').data('id');
    var postalcode = $(this).find(':selected').data('postalcode');
    $("#KELURAHAN_ID").val(kel);
    $("#POSTAL_CODE").val(postalcode);
});
$('#frmsaveBranch').on('submit', function(e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        $.ajax({
            url: "<?php echo base_url('branch/update'); ?>",
            data: $(this).serialize(),
            dataType: "JSON",
            type: 'POST',
            cache: false,
            success: function(res) {
                var ErrorMessage = res.ErrorMessage;
                var ErrorCode = res.ErrorCode;
                if (ErrorCode != "EC:0000") {
                    Swal.fire({
                        type: "error",
                        html: ErrorMessage,
                        confirmButton: true,
                        confirmButtonColor: "#1FB3E5",
                        confirmButtonText: "Close",
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        type: "success",
                        text: ErrorMessage,
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    setTimeout(function() {
                        var uri = "<?php echo $back; ?>";
                        window.location.href = uri;
                    }, 1500);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire(textStatus).then(function() {
                    Swal.fire({
                        type: "error",
                        title: textStatus,
                    });
                });
            },
            beforeSend: function() {
                $('#loadingBox').show();
            },
            complete: function() {
                $('#loadingBox').fadeOut();
            }
        });
    } else {
        ;
    }
});
</script>