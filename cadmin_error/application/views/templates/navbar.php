    <!-- Main navbar -->
    <div class="navbar navbar-expand-md">
        <div class="navbar-brand" style="padding: 0px;">
            <a href="<?php echo base_url(); ?>" class="d-inline-block">
                <img style="margin-top:10px; height: 50px !important; object-position: center;object-fit: contain;"
                    class="logo-topbar" src="<?php echo base_url(); ?>assets/images/logo-bumi-artha.png"
                    alt="Logo Bank Bumi Arta">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-user"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-md-auto">

                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                        data-toggle="dropdown">
                        <!-- <img src="<?php echo base_url(); ?>assets/images/demo/users/face11.jpg" -->
                        <!-- class="rounded-circle mr-2" height="34" alt=""> -->
                        <img class="rounded-circle mr-2" height="34" alt="" avatar="<?php echo $_SESSION['name'] ?>">
                        <span><?php echo $_SESSION['name']; ?></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a onclick="openEditData('<?php echo $_SESSION['iduser'] ?>')" class="dropdown-item"
                            data-toggle="modal" data-target="#modalchProfile">
                            <i class="icon-user-plus"></i> My profile
                        </a>
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalchPass">
                            <i class="icon-cog5"></i> Change Password</a>

                        <div class="dropdown-divider"></div>
                        <a href="<?php echo base_url('authentication/logout') ?>" class="dropdown-item"><i
                                class="icon-switch2"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- MODAL CHANGE PROFILE -->
    <div class="modal fade" id="modalchProfile" role="dialog" aria-labelledby="lblmodalchProfile" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lblmodalchProfile">Edit Profil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="parsleyy" data-parsley-validate="" novalidate="" id="frmsavechProfile">

                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" id="chProfilenamee" class="form-control" parsley-trigger="change"
                                name="chProfilenamee" placeholder="Fullname" required="">
                            <input type="hidden" name="IDchProfile" id="IDchProfile" required="">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" id="chProfileemailAddress" class="form-control"
                                        parsley-trigger="change" name="chProfileemailAddress"
                                        placeholder="example@example.com" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Position</label>
                                    <input type="text" id="chProfilepositionUser" class="form-control"
                                        parsley-trigger="change" name="chProfilepositionUser" placeholder="Position"
                                        required="">
                                </div>
                            </div>
                        </div>

                        <!-- </div> -->
                        <div class="rows" style="float: right;">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger ml-2" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- END MODAL CHANGE PROFILE -->

    <!-- MODAL CHANGE PASS -->
    <div class="modal fade" id="modalchPass" role="dialog" aria-labelledby="lblmodalchPass" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lblmodalchPass">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="parsleyy" data-parsley-validate="" novalidate="" id="frmsavechPass">

                        <div class="form-group">
                            <label>Old Password</label>
                            <input type="password" id="chPass" class="form-control" parsley-trigger="change"
                                name="chPass" required="" placeholder="Old Password">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" id="NewchPass" class="form-control passwordvalidator"
                                        parsley-trigger="change" data-parsley-minlength="5" data-parsley-maxlength="12"
                                        data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1"
                                        data-parsley-special="1" name="NewchPass" required=""
                                        placeholder="New Password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Repeat New Password</label>
                                    <input type="password" id="RechPass" class="form-control passwordvalidator"
                                        parsley-trigger="change" data-parsley-equalto="#NewchPass"
                                        data-parsley-minlength="5" data-parsley-maxlength="12"
                                        data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1"
                                        data-parsley-special="1" required name="RechPass"
                                        placeholder="Re-enter New  Password">
                                </div>
                            </div>
                        </div>

                        <!-- </div> -->
                        <div class="rows" style="float: right;">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger ml-2" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- END MODAL CHANGE PASS -->
    <script>
var url = "<?php echo base_url(); ?>";

function openModal(idm) {
    $("#" + idm).modal("show");
}
//Open Edit Form
function openEditData(id) {
    $.ajax({
        url: "<?php echo base_url('Users/get_edit/'); ?>" + id,
        dataType: "JSON",
        type: "GET",
        success: function(data) {
            $("#chProfilenamee").val(data.profile.NAME);
            $("#chProfileemailAddress").val(data.profile.EMAIL);
            $("#chProfilepositionUser").val(data.profile.POSITION);
            $("#IDchProfile").val(data.profile.ID);

            $("#modalchProfile").modal("show");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Swal.fire(errorThrown);
            document.getElementById("frmeditUser").reset();
        },
    });
}
//END Open Edit Form
$(document).ready(function() {

    //EDIT PROFILE
    $("#frmsavechProfile").on("submit", function(e) {
        e.preventDefault();
        if ($(this).parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('users/update_profile') ?>",
                data: $(this).serialize(),
                dataType: "JSON",
                type: "POST",
                success: function(data) {
                    if (!(data.indexOf("Failed") != -1)) {
                        Swal.fire({
                            type: "success",
                            title: "Update Profile Success",
                        }).then(function() {
                            location.reload();
                            document.getElementById("frmsavechProfile").reset();
                            $("#modalchProfile").modal("hide");
                        });
                    } else {
                        Swal.fire({
                            type: "error",
                            title: data,
                        });
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire(textStatus);
                    document.getElementById("frmsavechProfile").reset();
                    $("#modalchProfile").modal("hide");
                },
            });
        }
    });
    //END EDIT PROFILE

    //EDIT CHPASS
    $("#frmsavechPass").on("submit", function(e) {
        e.preventDefault();
        if ($(this).parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('Users/changepass') ?>",
                data: $(this).serialize(),
                dataType: "JSON",
                type: "POST",
                success: function(data) {
                    if (!(data.indexOf("Failed") != -1)) {
                        Swal.fire({
                            type: "success",
                            title: "Change Password Success",
                        }).then(function() {
                            location.reload();
                            document.getElementById("frmsavechPass").reset();
                            $("#modalchPass").modal("hide");
                        });
                    } else {
                        Swal.fire({
                            type: "error",
                            title: data,
                        });
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire(textStatus);
                    document.getElementById("frmsavechPass").reset();
                    $("#modalchPass").modal("hide");
                },
            });
        }
    });
    //END EDIT CHPASS
});
    </script>