<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Branch extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('branch_model');
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "branch/view";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['branch'] = $this->branch_model->view();
        $this->load->view('templates/view', $data);
    }
    public function add_branch()
    {
        $data = array();
        $data['content'] = "branch/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['province'] = $this->branch_model->province();
        $data['data']['back'] = base_url() . 'branch';

        $this->load->view('templates/view', $data);
    }
    public function edit_branch($id)
    {
        $data = array();
        $data['content'] = "branch/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['branch'] = $this->branch_model->get_edit($id);
        $data['data']['province'] = $this->branch_model->province();
        $data['data']['back'] = base_url() . 'branch';

        $this->load->view('templates/view', $data);
    }
    public function detail_branch($id)
    {
        $data = array();
        $data['content'] = "branch/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['branch'] = $this->branch_model->get_edit_branch($id);
        $data['data']['back'] = base_url() . 'branch';

        $this->load->view('templates/view', $data);
    }

    public function add()
    {
        $BRANCH_NAME = isset($_POST['branch_name']) ? $_POST['branch_name'] : "";
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $PHONE_NUMBER = isset($_POST['phone_number']) ? $_POST['phone_number'] : "";
        $FAX_NUMBER = isset($_POST['fax_number']) ? $_POST['fax_number'] : "";
        $WEBSITE = isset($_POST['website']) ? $_POST['website'] : "";
        $EMAIL = isset($_POST['email']) ? $_POST['email'] : "";
        $ADDRESS = isset($_POST['address']) ? $_POST['address'] : "";
        $LONGITUDE = isset($_POST['longitude']) ? $_POST['longitude'] : "";
        $LATITUDE = isset($_POST['latitude']) ? $_POST['latitude'] : "";
        $ID_PROVINCE = isset($_POST['province_id']) ? $_POST['province_id'] : "";
        $ID_CITY = isset($_POST['city_kab_id']) ? $_POST['city_kab_id'] : "";
        $ID_KECAMATAN = isset($_POST['kecamatan_id']) ? $_POST['kecamatan_id'] : "";
        $ID_KELURAHAN = isset($_POST['kelurahan_id']) ? $_POST['kelurahan_id'] : "";
        $POSTAL_CODE = isset($_POST['postal_code']) ? $_POST['postal_code'] : "";

        $param['BRANCH_NAME'] = $BRANCH_NAME;
        $param['CATEGORY'] = $CATEGORY;
        $param['PHONE_NUMBER'] = $PHONE_NUMBER;
        $param['FAX_NUMBER'] = $FAX_NUMBER;
        $param['WEBSITE'] = $WEBSITE;
        $param['EMAIL'] = $EMAIL;
        $param['ADDRESS'] = $ADDRESS;
        $param['LONGITUDE'] = $LONGITUDE;
        $param['LATITUDE'] = $LATITUDE;
        $param['ID_PROVINCE'] = $ID_PROVINCE;
        $param['ID_CITY'] = $ID_CITY;
        $param['ID_KECAMATAN'] = $ID_KECAMATAN;
        $param['ID_KELURAHAN'] = $ID_KELURAHAN;
        $param['POSTAL_CODE'] = $POSTAL_CODE;
        $param['CREATED_BY'] = $_SESSION['username'];
        $param['CREATED_DATE'] = date('Y-m-d H:i:s');
        $Insert = $this->branch_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));
    }

    public function view()
    {
        $GetData = $this->branch_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID_BRANCH = isset($_POST['id']) ? $_POST['id'] : "";
        $BRANCH_NAME = isset($_POST['branch_name']) ? $_POST['branch_name'] : "";
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $PHONE_NUMBER = isset($_POST['phone_number']) ? $_POST['phone_number'] : "";
        $FAX_NUMBER = isset($_POST['fax_number']) ? $_POST['fax_number'] : "0";
        $WEBSITE = isset($_POST['website']) ? $_POST['website'] : "0";
        $EMAIL = isset($_POST['email']) ? $_POST['email'] : "";
        $ADDRESS = isset($_POST['address']) ? $_POST['address'] : "";
        $LONGITUDE = isset($_POST['longitude']) ? $_POST['longitude'] : "";
        $LATITUDE = isset($_POST['latitude']) ? $_POST['latitude'] : "";
        $ID_PROVINCE = isset($_POST['province_id']) ? $_POST['province_id'] : "";
        $ID_CITY = isset($_POST['city_kab_id']) ? $_POST['city_kab_id'] : "";
        $ID_KECAMATAN = isset($_POST['kecamatan_id']) ? $_POST['kecamatan_id'] : "";
        $ID_KELURAHAN = isset($_POST['kelurahan_id']) ? $_POST['kelurahan_id'] : "";
        $POSTAL_CODE = isset($_POST['postal_code']) ? $_POST['postal_code'] : "";

        $param['ID_BRANCH'] = $ID_BRANCH;
        $param['BRANCH_NAME'] = $BRANCH_NAME;
        $param['CATEGORY'] = $CATEGORY;
        $param['PHONE_NUMBER'] = $PHONE_NUMBER;
        $param['FAX_NUMBER'] = $FAX_NUMBER;
        $param['WEBSITE'] = $WEBSITE;
        $param['EMAIL'] = $EMAIL;
        $param['ADDRESS'] = $ADDRESS;
        $param['LONGITUDE'] = $LONGITUDE;
        $param['LATITUDE'] = $LATITUDE;
        $param['ID_PROVINCE'] = $ID_PROVINCE;
        $param['ID_CITY'] = $ID_CITY;
        $param['ID_KECAMATAN'] = $ID_KECAMATAN;
        $param['ID_KELURAHAN'] = $ID_KELURAHAN;
        $param['POSTAL_CODE'] = $POSTAL_CODE;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $Update = $this->branch_model->updateaction($param);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID_BRANCH']) ? $Data['ID_BRANCH'] : "";
        $param['ID_BRANCH'] = $ID;
        $Delete = $this->branch_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function getCity()
    {
        $id = $this->input->post('id', true);
        $this->db->where('PROVINCE_ID', $id);
        $data = $this->db->get('MS_CITY_KAB')->result();
        echo json_encode($data);
    }
    public function getKecamatan()
    {
        $id = $this->input->post('id', true);
        $this->db->where('CITY_KAB_ID', $id);
        $data = $this->db->get('MS_KECAMATAN')->result();
        echo json_encode($data);
    }
    public function getKelurahan()
    {
        $id = $this->input->post('id', true);
        $this->db->where('KECAMATAN_ID', $id);
        $data = $this->db->get('MS_KELURAHAN')->result();
        echo json_encode($data);
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_BRANCH']) ? $Data['ID_BRANCH'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_BRANCH' => $id);
        $Active = $this->branch_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_BRANCH']) ? $Data['ID_BRANCH'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_BRANCH' => $id);
        $Active = $this->branch_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_BRANCH']) ? $Data['ID_BRANCH'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_BRANCH' => $id);
        $Active = $this->branch_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }

}