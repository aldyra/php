<?php
$menu = isset($data['menu']) ? $data['menu'] : array();
$is_create = $authorize['is_create'];
$back = isset($data['back']) ? $data['back'] : '';

?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Content</a>
                <a href="#" class="breadcrumb-item active">Add</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
    <?php if ($is_create): ?>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Add Content</h5>
        </div>

        <div class="card-body">
            <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveContent">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <label>
                            MENU PARENTS<span style="color:orange">*</span>
                        </label>
                        <select id="id_menu" class="form-control select2" name="id_menu" required>
                            <option selected disabled>Choose Menu</option>
                            <?php foreach ($menu as $mn) {
    $ID = isset($mn['ID']) ? $mn['ID'] : "";
    $NAME_ID = isset($mn['NAME_ID']) ? $mn['NAME_ID'] : "";
    $NAME_EN = isset($mn['NAME_EN']) ? $mn['NAME_EN'] : "";
    $LINK_ID = isset($mn['LINK_ID']) ? $mn['LINK_ID'] : "";
    $LINK_EN = isset($mn['LINK_EN']) ? $mn['LINK_EN'] : "";

    ?>

                            <option value="<?php echo $ID ?>">
                                <?php echo $NAME_ID . ' / ' . $NAME_EN . ' (' . $LINK_ID . ' / ' . $LINK_EN . ')' ?>
                            </option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        TITLE ENG<span style="color:orange">*</span>
                        <input type="text" name="title_en" class="form-control" id="title_en" placeholder="" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label>DESCRIPTION ENG<span style="color:orange">*</span></label>
                        <div class="form-group">

                            <textarea type="text" name="description_en" class="form-control" id="description_en"
                                required></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        TITLE IND<span style="color:orange">*</span>
                        <input type="text" name="title_id" class="form-control" id="title_id" placeholder="" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label>DESCRIPTION ID<span style="color:orange">*</span></label>
                        <div class="form-group">
                            <textarea type="text" name="description_id" class="form-control" id="description_id"
                                required></textarea>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light"> Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
    <?php endif;?>
</div>
<script type="text/javascript">
$(function() {
    CKEDITOR.editorConfig = function(config) {
        config.line_height = "1em;1.1em;1.2em;1.3em;1.4em;1.5em";
    };
    $(".select2").select2();
    CKEDITOR.plugins.addExternal('videoembed',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/videoembed/plugin.js', '');
    CKEDITOR.plugins.addExternal('html5video',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/html5video/plugin.js', '');
    CKEDITOR.plugins.addExternal('lineheight',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/lineheight/plugin.js', '');
    CKEDITOR.plugins.addExternal('spicingsliders',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/spacingsliders/plugin.js', '');




    CKEDITOR.replace('description_id', {
        extraPlugins: 'videoembed, html5video, lineheight,spacingsliders',

    });

    CKEDITOR.replace('description_en', {
        extraPlugins: 'videoembed, html5video, lineheight, spacingsliders'

    });




    CKEDITOR.on('instanceReady', function() {
        $.each(CKEDITOR.instances, function(instance) {
            CKEDITOR.instances[instance].on("change", function(e) {
                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();
            });
        });
    });
    $('#frmsaveContent').on('submit', function(e) {
        e.preventDefault();
        if ($(this).parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('content/add'); ?>",
                data: $(this).serialize(),
                dataType: "JSON",
                type: 'POST',
                cache: false,
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            var uri = "<?php echo $back; ?>";
                            window.location.href = uri;
                        }, 1500);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire(textStatus).then(function() {
                        Swal.fire({
                            type: "error",
                            title: textStatus,
                        });
                    });
                },
                beforeSend: function() {
                    $('#loadingBox').show();
                },
                complete: function() {
                    $('#loadingBox').fadeOut();
                }
            });
        } else {
            ;
        }
    });


});
</script>