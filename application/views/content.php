<?php 
$FILE_BANNER = isset($data['content']['MENU_CHILD_FILE_BANNER']) ? $data['content']['MENU_CHILD_FILE_BANNER']:"";
$stylebanner = "height:250px";
if(!empty($FILE_BANNER)){
	$stylebanner = "background-image:url('".$FILE_BANNER."'); background-size:cover;height:250px;";
}
?>
<div class="row row-header" style="<?php echo $stylebanner ?>">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 class="title_menu_name" style="margin-bottom: 0 !important;"><?php echo (!empty($data['content']['MENU_PARENT'])) ? $data['content']['MENU_PARENT'] : $data['content']['MENU_CHILD']; ?></h4>
		</div>
	</div>
</div>

<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($data['content']['MENU_PARENT'])){
		?>
			<label style="color:#707070"><?php echo $data['content']['MENU_PARENT']?></label> /
		<!-- <a href="<?php echo $data['content']['MENU_PARENT_LINK']?>" style="color:#707070"><?php echo $data['content']['MENU_PARENT']?></a> / -->
		<?php } ?> 
		<label style="color:#707070"><?php echo $data['content']['MENU_CHILD']; ?></label>
	</div>
</div>

<div class="row">
	<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 d-pad-10 content-left" style="padding-left: 15px; padding-right: 15px;">
		<div class="accordion" id="accordionparent">
			<div class="card">
				<div class="card-body" style="padding: 0px; background-color: #28448f; color: white;">

					<?php 
					if(!empty($data['content']['MENU_PARENT'])){
					?>
					<button id="btnMobParent" class="btn mobile-parent-accordion btn-block text-left d-flex align-items-center">
						<span class="mr-auto"><?php echo $data['content']['MENU_PARENT'] ?></span>
						<i class="fa fa-angle-down"></i>
					</button>
					<?php } ?>

					<div id="accordion_parent">
						<div class="accordion" id="accordion0">
							<div class="card" id="cardPrepend">
								<?php
								$i = 0;
								foreach($data['leftmodule'] as $CHILD1){
									if(empty(sizeof($CHILD1['CHILD']))){
										$UrlChild = base_url().$_SESSION['site_lang']."/".$CHILD1['LINK'];
									}else{
										$UrlChild = "javascript:void(0)";
									}

									if($data['controller'] == $CHILD1['LINK']){
										$active = "style='color: #243E8B !important; font-weight: bold;'";
									}else{
										$active = "style='color: #707070;'";
									}
								?>
									<div class="card-body <?php echo $CHILD1['ACTIVE_URL'] ? 'active' : '' ?>" id="heading<?php echo $i?>" style="padding-left: 14px; padding-right: 14px; padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #dadada;	">
										<button type="button" class="btn btn-link d-flex d-padding" data-toggle="collapse" data-target="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>" style="width: 100%;">
											<a href="<?php echo $UrlChild ?>" <?php echo $active ?>><?php echo $CHILD1['NAME'];?></a>
											<?php 
											if(!empty(sizeof($CHILD1['CHILD']))){
											?>
											<span class="btn btn-link" type="button" style="position: absolute; right: 0; padding-top: 0;">
												<i class="fas fa-chevron-down" style="font-size: 14px; color: #707070;"></i>
											</span>
											<?php } ?>
										</button>
										
										<div id="collapse<?php echo $i ?>" class="collapse <?php echo $CHILD1['ACTIVE_URL'] ? 'show' : '' ?>" aria-labelledby="heading<?php echo $i?>" data-parent="#accordion0">
											<div class="accordion" id="accordion1">
												<div class="card" style="border: none">
													<?php
													$j = 0;
													foreach($CHILD1['CHILD'] AS $CHILD2){
							                    		if(empty(sizeof($CHILD2['CHILD']))){
															$UrlChild2 = base_url().$_SESSION['site_lang']."/".$CHILD2['LINK'];
														}else{
															$UrlChild2 = "javascript:void(0)";
														}
														
														$active = "style='margin-bottom: 0; font-size: 14px;'";
							                    	?>
														<div class="card-body <?php echo $CHILD2['ACTIVE_URL'] ? 'active':'' ?>" id="heading<?php echo $i.$j ?>" style="padding-left: 30px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px;border: none; ">
															<button type="button" class="btn btn-link d-flex d-padding" data-toggle="collapse" data-target="#collapse<?php echo $i.$j ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j ?>" style="width: 100%;">
																<a href="<?php echo $UrlChild2 ?>" style="<?php echo $CHILD2['ACTIVE_URL'] ? 'color: #243E8B;font-weight:bold' : 'color: #707070' ?>; "><?php echo $CHILD2['NAME'];?></a>
																<?php 
																if(!empty(sizeof($CHILD2['CHILD']))){
																?>
																<span class="btn btn-link" type="button" style="position: absolute; right: 0; padding-top: 0;padding-right: 3px;">
																	<i class="fas fa-chevron-down" style="font-size: 14px;color: #707070;"></i>
																</span>
																<?php } ?>
															</button>
														</div>
														<div id="collapse<?php echo $i.$j ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j ?>" data-parent="#accordion1">
															<div class="accordion" id="accordion2">
																<div class="card" style="border: none">
																	<?php
																	$k = 0;
																	foreach($CHILD2['CHILD'] AS $CHILD3){
											                    		if(empty(sizeof($CHILD3['CHILD']))){
																			$UrlChild3 = base_url().$_SESSION['site_lang']."/".$CHILD3['LINK'];
																		}else{
																			$UrlChild3 = "javascript:void(0)";
																		}
																		
																		$active = "style='margin-bottom: 0; font-size: 14px;'";
											                    	?>
											                    		<div class="card-body" id="heading<?php echo $i.$j.$k ?>" style="padding-left: 50px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; border: none;">
											                    			<button type="button" class="btn btn-link d-flex d-padding" data-toggle="collapse" data-target="#collapse<?php echo $i.$j.$k ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j.$k ?>" style="width: 100%; text-align: left;">
																				<a  href="<?php echo $UrlChild3 ?>" style="color: #707070; "><?php echo $CHILD3['NAME'];?></a>
																				<?php 
																				if(!empty(sizeof($CHILD3['CHILD']))){
																				?>
																				<span class="btn btn-link" type="button" style="position: absolute; right: 0; padding-top: 0;">
																					<i class="fas fa-chevron-down" style="font-size: 14px; color: #707070;"></i>
																				</span>
																				<?php } ?>
																			</button>
																		</div>
																		<!-- <div id="collapse<?php echo $i.$j.$k ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j.$k ?>" data-parent="#accordionq">
																			
																		</div> -->
											                    	<?php 
											                    	$k++;
												                    }
												                    ?>
																</div>
															</div>
														</div>
													<?php 
													$j++;
													} 
													?>
												</div>
											</div>
										</div>
									</div>

								<?php 
								$i++;
								} 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 d-pad-10" id="row-content-detail" style="padding-top: 0; padding-left: 15px; padding-right: 15px;">
		<p>
			<h5 style="color: #2A2C2F !important; font-size: 18px !important;"><b><?php echo $data['content']['TITLE']; ?></b></h5>
		</p>
		<div class="table-responsive">
			<p><?php echo $data['content']['DESCRIPTION']; ?></p>
		</div>
		<hr width="100%">
		<div style="max-width: 100%" class="mb-3 mt-3">
			<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
			<div style="clear:both"></div>
		</div>
		
	</div>
</div>

<style type="text/css">
	a:hover{
		text-decoration: none;
	}

	ul, ol{
		margin-left: -17px;
	}

	div.accordion a{
		font-size: 14px;
	}

	table.table-content{
		width: 100% !important;
	}

	table.table-content tbody tr td{
		border: 1px solid rgba(0, 0, 0, 0.125) !important;
		padding: 5px !important;
	}

	p {
		/*color: #2A2C2F !important;*/
	}

	div.accordion div.card div.card-body{
		background-color : #f7f7f7;
	}

	div.accordion div.card div.card-body a:hover{
		color: #243E8B !important;
		font-weight: bold;
		text-decoration: none;
	}

	.sidebar-child-1{
		padding-left: 0;
		padding-top: 0; 
		padding-bottom: 0; 
		margin-bottom: 5px;
	}

	div.subchild-1{
		display: none;
		
	}

	.dropdown-1.show div.subchild-1{
		display: block;
		-webkit-transition: all 0.5s ease-in-out;
		-moz-transition: all 0.5s ease-in-out;
		-ms-transition: all 0.5s ease-in-out;
		-o-transition: all 0.5s ease-in-out;
		transition: all 0.5s ease-in-out;
	}

	div.subchild-2{
		display: none;
	}

	div.subchild-2.show{
		display: block;
	}

	.left-child{
		position: absolute;
		right: 20px;
		margin-top: -16px;
	}

	.btn.mobile-parent-accordion{
		display: none !important;
	}

	.card-body > a{
		font-size: 14px;
	}

	.row-page-module{
		font-size: 14px;
	}


	@media all and (min-width: 992px) {
		.content-left{
			padding-left: 35px;
		}

		/*.box-header{
			padding: 20px;
			position: absolute; 
			bottom: 0;
			-webkit-backdrop-filter: blur(10px);
	  		backdrop-filter: blur(10px);
		}*/
	}
	@media all and (max-width: 992px) {
		.content-left{
			padding-left: 10px;
		}

		/*.box-header{
			padding: 20px;
			-webkit-backdrop-filter: blur(10px);
	  		backdrop-filter: blur(10px);
	  		position: absolute;
	  		bottom: 0px;
	  		left: 0;
	  		right: 0;
		}*/
	}
	

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		#accordion_parent{
			display: none;
		}
		.btn.mobile-parent-accordion{
			display: flex !important;
			background: #243E8B;
			color: #fff;
			padding: 10px 20px;
			border-radius: unset;
		}
		.row-page-module{
			display: none;
		}
		.title_menu_name{
			text-align: center;
		    left: 0;
		    right: 0;
		}
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		// $("#accordion > .card.active").addClass('to-remove');
		// $("#accordion").prepend($("#accordion > .card.active").html());
		$("#cardPrepend").prepend($("#cardPrepend > .card-body.active"));
		$("#cardPrepend > .card-body.active > .collapse.show > .accordion > .card").prepend($("#cardPrepend > .card-body.active .card > .card-body.active"));

		$("#cardPrepend > .card-body.active > a").css("color", "#233F8A");
		$("#cardPrepend > .card-body.active > a").css("font-weight", "bold");
		// $("#accordion > .card.active").before($("#accordion > .card:nth-child(2)"));

		$("#btnMobParent").on('click', function(){
			$("#accordion_parent").slideToggle('fast');
		});
	});

	$(function(){
		$('#row-content-detail p img').removeClass().addClass('img img-fluid');
		$('#row-content-detail p img').css({'object-fit':'contain', 'object-position':'center'});
		$('video').attr({'controls':'controls'});
		$('video').css({'width':'100%'});

		var child1 = $('ul.sidebar-child-1').find('li div.dropdown-1');
		child1.each(function(){
			var id = $(this).attr('id');
		  	$('div#'+id).click(function(){
		        var getclass = $(this).attr('class');
		        var split = getclass.split(" ");
		        if(split.includes("show")){
		            $(this).removeClass().addClass('dropdown-1');
		        }else{
		            $(this).removeClass().addClass('dropdown-1 show');
		        }
		    });
		});

		var table = $('#row-content-detail').find('table');
		table.each(function(){
		  $(this).attr({'class':'table table-striped table-content'});
		});

		var h_navbar = $('div.header-fixed').innerHeight();
		setTimeout(function(){
			$('div.row-header').css({'padding-top':h_navbar});
		},500);
		
	});
</script>