<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends MY_Model
{

    protected $_table_name = 'MS_USERS';
    protected $_primary_key = 'ID';
    protected $_order_by = 'ID';
    protected $_order_by_type = 'DESC';

    public $rules = array(
        'username' => array( // include no special char
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|min_length[2]|max_length[15]',
        ),
        'name' => array( // include no special char
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|min_length[2]|max_length[50]',
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|min_length[2]|max_length[60]',
        ),
        'group_user' => array(
            'field' => 'group_user',
            'label' => 'Position',
            'rules' => 'required',
        ),
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function cekDuplicate($name, $email, $id = null)
    {
        if ($id == null) {
            $this->db->where('USERNAME', $name);
            $this->db->or_where('EMAIL', $email);
            $num = $this->db->get($this->_table_name)->num_rows();
            if ($num >= 1) {
                return 0;
            } else {
                return 1;
            }

        } else {
            $this->db->group_start(); // Open bracket
            $this->db->where('USERNAME', $name);
            $this->db->or_where('EMAIL', $email);
            $this->db->group_end(); // End bracket
            $this->db->where_not_in('ID', $id);
            $num = $this->db->get($this->_table_name)->num_rows();
            if ($num >= 1) {
                return 0;
            } else {
                return 1;
            }

        }
    }

    public function select_col($col, $getAction, $where = null)
    {
        $this->datatables->select($col);
        $this->datatables->from($this->_table_name);

        if ($where) {
            foreach ($where as $key => $value) {
                $this->datatables->where($key, $value);
            }
        }

        $edtdelete = '';
        $edtdelete .= '<a onclick="openDetail(\'$1\')" class="dropdown-item"><i class="icon-eye"></i> Detail</a>';
        if ($getAction['is_update'] == 1) {
            $edtdelete .= '<a onclick="statusChg(\'$1\')" class="dropdown-item"><i class="$icon"></i> $status</a>';
            $edtdelete .= '<a onclick="openEdit(\'$1\')" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        }

        // if ($getAction['is_delete'] == 1) {
        //     $edtdelete .= '<a onclick="confirmDelete(\'$1\')" class="dropdown-item"><i class="icon-trash"></i> Delete</a>';
        // }

        $this->datatables->add_column('ACTION',
            '<div class="list-icons">
                <div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        ' . $edtdelete . '
                    </div>
                </div>
            </div>', 'ID'
        );

        return $sql = $this->datatables->generate();
    }

    public function getProfile($where, $tbl)
    {
        $this->db->where($where);
        $result = $this->db->get($tbl)->row();
        $this->db->where(array('USERNAME' => $_SESSION['username']));
        return $this->db->get('v_tblUsers')->row();
    }
    public function update_action($dataupdate, $id)
    {
        $cekrow = $this->db->get_where($this->_table_name, $dataupdate)->num_rows();
        if ($cekrow == 1) {
            return "1: No update, data is same";
        } else {
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name, $dataupdate);

            return $this->db->affected_rows() . " Affected Rows";
        }
    }
    public function changePass($dataupdate, $id = null)
    {
        if ($id != null) {
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name, $dataupdate);

            return $this->db->affected_rows() . " Affected Rows";
        } else {
            return false;
        }
    }
    public function cekoldpass($pass, $id = null)
    {
        if ($id != null) {
            $this->db->where($this->_primary_key, $id);
            $this->db->where("PASSWORD", $pass);
            $num = $this->db->get($this->_table_name)->num_rows();

            if ($num <= 0) {
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
    public function dupliUser($name, $email, $id = null)
    {
        if ($id != null) {
            $num = $this->db->query("SELECT * FROM " . $this->_table_name . "
                                    WHERE ID <> '" . $id . "' AND
                                    ( USERNAME = '" . $name . "' OR EMAIL = '" . $email . "' )")->num_rows();

            if ($num <= 0) {
                return 1;
            } else {
                return 0;
            }

        } else {
            $this->db->where("USERNAME", $name);
            $this->db->or_where("EMAIL", $email);
            $num = $this->db->get($this->_table_name)->num_rows();

            if ($num <= 0) {
                return 1;
            } else {
                return 0;
            }

        }
    }

    public function getProfileNotYet($where = null, $limit = null, $offset = null, $single = false, $select = null)
    {
        // $this->db->join('{PRE}USER_GROUP', '{PRE}USER_GROUP.ID = {PRE}'.$this->_table_name.'.GROUP_ID');
        $this->db->join('USER_GROUP', 'USER_GROUP.ID = USERS.GROUP_ID');
        return parent::get_by($where, $limit, $offset, $single, $select);
    }

    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        return $this->db->get($this->_table_name)->row_array();
    }

    public function last_update()
    {
        $this->db->select(array('DATE_LOG'));
        $this->db->order_by('DATE_LOG', 'DESC');
        return $this->db->get($this->_table_name)->row_array();
    }

    public function delete_row($id)
    {
        $this->db->where($this->_primary_key, $id);
        $this->db->delete($this->_table_name);

        return $this->db->affected_rows() . " Affected Rows";
    }

}