<?php
defined('BASEPATH') or exit('No direct script access allowed');

define('DB_NAME', 'compro_bumiartha');
define('DB_USER', 'usr_bumiartha');
define('DB_PASS', 'usrbumiartha1234');
define('DB_HOST', 'dc01dbs05m.int');
define('PROJECT_NAME', 'bumiartha');

$config['dbss'] = array(
    'user' => DB_USER,
    'pass' => DB_PASS,
    'db' => DB_NAME,
    'host' => DB_HOST,
);

$config['B2C'] = "bumi_artha/";
$config['EMAIL'] = "nieve@nieve.id";
$config['HOST'] = "https://" . $_SERVER['HTTP_HOST'] . "/";
$config['LANGUAGE'] = "English:Bahasa Indonesia";
$config['ID_LANGUAGE'] = "EN:ID";

//user
// $config['approval_kurs'] = "84";
// $config['approval_sbdk'] = "85";
// $config['approval_deposit_ir'] = "86";
// $config['approval_giro_ir'] = "86";
// $config['approval_saving_ir'] = "86";

$config['approval_kurs'] = array("84", "55");
$config['approval_sbdk'] = array("85", "55");
$config['approval_deposit_ir'] = array("86", "55");
$config['approval_giro_ir'] = array("86", "55");
$config['approval_saving_ir'] = array("86", "55");

$config['REPORT_ID'] = "Laporan Tahunan:Laporan Bulanan:Laporan Keuangan Publikasi:Laporan Keuangan Interim:Laporan Keuangan Konsolidasi:Laporan Pengungkapan Informasi Kuantitatif Eksposur Risiko:Laporan Perhitungan Rasio Pengungkit:Laporan Total Eksposur";
$config['REPORT_EN'] = "Annual Financial Reports:Monthly Financial Reports:Published Financial Statement:Interim Financial Statement:Consolidated Financial Statement:Disclosure of Quantitative Information Risk Exposure:Everage Ratio Calculation Report:Total Exposure Report";
$config['ID_CATEG_REPORT'] = "1:2:3:4:5:6:7:8";

$config['DEPOSIT_ID'] = "Deposito Umum:Deposito Plus:Deposito Premium:Deposito Perpanjangan:Deposito USD";
$config['DEPOSIT_EN'] = "Deposit Umum:Deposit Plus:Deposit Premium:Deposit Perpanjangan:Deposit USD";
$config['ID_CATEG_DEPOSIT'] = "1:2:3:4:5";

$config['SAVING_ID'] = "Tabungan BBA:Tabungan Kesra:Tabungan BBA USD:Tabungan Pensiun:TabunganKu:Tabungan Berjangka Super BBA(TangKas BBA):Tabungan MultiGuna(TaMu)";
$config['SAVING_EN'] = "Tabungan BBA:Tabungan Kesra:Tabungan BBA USD:Tabungan Pensiun:TabunganKu:Tabungan Berjangka Super BBA(TangKas BBA):Tabungan MultiGuna(TaMu)";
$config['ID_CATEG_SAVING'] = "1:2:3:4:5:6:7";

$config['GIRO_ID'] = "Giro Rupiah:Giro USD";
$config['GIRO_EN'] = "Giro Rupiah: Giro USD";
$config['ID_CATEG_GIRO'] = "1:2";

$config['POSITION_ID'] = "Board of Commisioners:Board of Directors:Corporate Secretary:Audit Commitee:Risk Monitoring Commitee:Remuneration and Nomination Committee";
$config['POSITION_EN'] = "Dewan Komisaris:Dewan Direksi:Sekertaris Perusahaan:Komite Audit:Komite Pemantau Resiko:Komite Remunerasi dan Nominasi";
$config['ID_CATEG_POSITION'] = "1:2:3:4:5:6";

$config['STATUS'] = "Inactive:Active:Publish";
$config['ID_STATUS'] = "0:88:99";

$config['allowed_types'] = "*";
$config['allowed_types_file'] = 'PDF|DOCX|DOC|XLSX|XLS|ZIP|RAR';
$config['allowed_types_image'] = 'JPEG|JPG|PNG|GIF';
$config['allowed_types_video'] = 'MPEG|MJPEG|MP4|WMV|AVI|M4V';
$config['allowed_types_image_video'] = 'JPEG|JPG|PNG|GIF|MPEG|MJPEG|MP4|WMV|AVI|M4V';

$config['max_size'] = 1024 * 10; // <= 10Mb;
$config['max_width'] = '1000';
$config['max_height'] = '1000';
$config['min_width'] = '1';
$config['min_height'] = '1';

$config['max_size_banner'] = '3000';
$config['path_banner'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/banner/" . strtoupper(date('Y/M')) . "/";
$config['save_banner'] = $config['HOST'] . $config['B2C'] . "assets/file/banner/" . strtoupper(date('Y/M')) . "/";
$config['min_width_banner'] = '1';
$config['min_height_banner'] = '1';
$config['max_width_banner'] = '5000';
$config['max_height_banner'] = '5000';

$config['max_size_gallery'] = '3000';
$config['path_gallery'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/gallery/" . strtoupper(date('Y/M')) . "/";
$config['save_gallery'] = $config['HOST'] . $config['B2C'] . "assets/file/gallery/" . strtoupper(date('Y/M')) . "/";
$config['min_width_gallery'] = '1';
$config['min_height_gallery'] = '1';
$config['max_width_gallery'] = '5000';
$config['max_height_gallery'] = '5000';

$config['max_size_menu'] = '3000';
$config['path_menu'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/header_banner/" . strtoupper(date('Y/M')) . "/";
$config['save_menu'] = $config['HOST'] . $config['B2C'] . "assets/file/header_banner/" . strtoupper(date('Y/M')) . "/";
$config['min_width_menu'] = '1';
$config['min_height_menu'] = '1';
$config['max_width_menu'] = '5000';
$config['max_height_menu'] = '5000';

$config['max_size_award'] = '3000';
$config['path_award'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/award/" . strtoupper(date('Y/M')) . "/";
$config['save_award'] = $config['HOST'] . $config['B2C'] . "assets/file/award/" . strtoupper(date('Y/M')) . "/";
$config['min_width_award'] = '1';
$config['min_height_award'] = '1';
$config['max_width_award'] = '5000';
$config['max_height_award'] = '5000';

$config['max_size_management'] = '3000';
$config['path_management'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/management/" . strtoupper(date('Y/M')) . "/";
$config['save_management'] = $config['HOST'] . $config['B2C'] . "assets/file/management/" . strtoupper(date('Y/M')) . "/";
$config['min_width_management'] = '1';
$config['min_height_management'] = '1';
$config['max_width_management'] = '5000';
$config['max_height_management'] = '5000';

$config['max_size_document'] = '5000';
$config['path_document'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/document/" . strtoupper(date('Y/M')) . "/";
$config['save_document'] = $config['HOST'] . $config['B2C'] . "assets/file/document/" . strtoupper(date('Y/M')) . "/";
$config['min_width_document'] = '1';
$config['min_height_document'] = '1';
$config['max_width_document'] = '5000';
$config['max_height_document'] = '5000';

$config['path_news'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/news/" . strtoupper(date('Y/M')) . "/";
$config['save_news'] = $config['HOST'] . $config['B2C'] . "assets/file/news/" . strtoupper(date('Y/M')) . "/";

$config['path_currency'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/flag/" . strtoupper(date('Y/M')) . "/";
$config['save_currency'] = $config['HOST'] . $config['B2C'] . "assets/file/flag/" . strtoupper(date('Y/M')) . "/";

$config['path_video'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/video/" . strtoupper(date('Y/M')) . "/";
$config['save_video'] = $config['HOST'] . $config['B2C'] . "assets/file/video/" . strtoupper(date('Y/M')) . "/";

$config['path_social_media'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/social_media/" . strtoupper(date('Y/M')) . "/";
$config['save_social_media'] = $config['HOST'] . $config['B2C'] . "assets/file/social_media/" . strtoupper(date('Y/M')) . "/";

$config['path_report'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/report/" . strtoupper(date('Y/M')) . "/";
$config['save_report'] = $config['HOST'] . $config['B2C'] . "assets/file/report/" . strtoupper(date('Y/M')) . "/";

$config['path_report_rups'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/report_rups/" . strtoupper(date('Y/M')) . "/";
$config['save_report_rups'] = $config['HOST'] . $config['B2C'] . "assets/file/report_rups/" . strtoupper(date('Y/M')) . "/";

$config['path_kesra_winner'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/winner_kesra/" . strtoupper(date('Y/M')) . "/";
$config['save_kesra_winner'] = $config['HOST'] . $config['B2C'] . "assets/file/winner_kesra/" . strtoupper(date('Y/M')) . "/";

$config['path_prospectus'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/prospektus/" . strtoupper(date('Y/M')) . "/";
$config['save_prospectus'] = $config['HOST'] . $config['B2C'] . "assets/file/prospektus/" . strtoupper(date('Y/M')) . "/";
$config['max_size_prospectus'] = '10000';

$config['path_report_gcg'] = $_SERVER['DOCUMENT_ROOT'] . "/" . $config['B2C'] . "assets/file/gcg/" . strtoupper(date('Y/M')) . "/";
$config['save_report_gcg'] = $config['HOST'] . $config['B2C'] . "assets/file/gcg/" . strtoupper(date('Y/M')) . "/";