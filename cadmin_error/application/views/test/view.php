<?php
$data = isset($data) ? $data : "";
$Menu = isset($data['menu']) ? $data['menu'] : array();
$Controllers = $this->uri->segment(1);
$paramadd = array();
$paramadd['menu'] = isset($data['datamenu']) ? $data['datamenu'] : array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('menu/add');

// $is_create = $authorize['is_create'];
// $is_update = $authorize['is_update'];
// $is_delete = $authorize['is_delete'];
?>
<!-- page headder -->
<script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/fixed_header.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/scroller.min.js"></script>


<style>
.ui-datepicker {
    margin-left: 100px;
    z-index: 1000;
}

.dataTables_scrollBody::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
    border-radius: 10px;
}

.dataTables_scrollBody::-webkit-scrollbar {
    width: 6px;
    background-color: #F5F5F5;
}

.dataTables_scrollBody::-webkit-scrollbar-thumb {
    background-color: #777;
    border-radius: 10px;
}
</style>
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Master Menu</a>
                <!-- <span class="breadcrumb-item \ active">Dashboard</span> -->
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <div class="card-body">
            <a href="<?php echo base_url("menu/add_menu/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add Menu
                </button>
            </a>
        </div>

        <table id="tableData" class="table" style="display: block;width: 100%;overflow-x: auto;">
            <thead>
                <tr>
                    <th width="300">Name Indonesian</th>
                    <th>Name English</th>
                    <th data-orderable="false">Menu Level</th>
                    <th data-orderable="false">Menu Position</th>
                    <th data-orderable="false">Sequence Header</th>
                    <th data-orderable="false">Sequence Footer</th>
                    <th>Created By</th>
                    <th data-orderable="false">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th style="display:none;">sts</th>
                    <th style="display:none;">position</th>
                    <th data-orderable="false">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Menu as $menu) {
    $ID = isset($menu['ID']) ? $menu['ID'] : "";
    $NAME_ID = isset($menu['NAME_ID']) ? $menu['NAME_ID'] : "";
    $NAME_EN = isset($menu['NAME_EN']) ? $menu['NAME_EN'] : "";
    $POSITION = isset($menu['POSITION']) ? $menu['POSITION'] : "";
    $SEQUENCE_HEADER = isset($menu['SEQUENCE_HEADER']) ? $menu['SEQUENCE_HEADER'] : "";
    $SEQUENCE_FOOTER = isset($menu['SEQUENCE_FOOTER']) ? $menu['SEQUENCE_FOOTER'] : "";
    $MENU_PARENT = isset($menu['MENU_PARENT']) ? $menu['MENU_PARENT'] : "";
    $IS_CONTENT = isset($menu['IS_CONTENT']) ? $menu['IS_CONTENT'] : "";
    $STATUS = isset($menu['STATUS']) ? $menu['STATUS'] : "";
    $CREATED_BY = isset($menu['CREATED_BY']) ? $menu['CREATED_BY'] : "";
    $CREATED_DATE = isset($menu['CREATED_DATE']) ? date('d M Y', strtotime($menu['CREATED_DATE'])) : "";
    $USER_LOG = isset($menu['USER_LOG']) ? $menu['USER_LOG'] : "";
    $DATE_LOG = isset($menu['DATE_LOG']) ? date('d M Y', strtotime($menu['DATE_LOG'])) : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    if ($POSITION == "1") {
        $PSTN = "Header";
    } elseif ($POSITION == "2") {
        $PSTN = "Footer";
    } elseif ($POSITION == "3") {
        $PSTN = "Header & Footer";
    } else {
        $PSTN = "";
    }
    if ($MENU_PARENT == "0") {
        $MENU_LEVEL = "Menu Parent";
    } else {
        $MENU_LEVEL = "Menu Child";
    }

    ?>
                <tr>
                    <td>
                        <?php echo $NAME_ID ?>
                    </td>
                    <td>
                        <?php echo $NAME_EN ?>
                    </td>
                    <td>
                        <?php echo $MENU_LEVEL ?>
                    </td>
                    <td>
                        <?php echo $PSTN ?>
                    </td>
                    <td>
                        <?php echo $SEQUENCE_HEADER ?>
                    </td>
                    <td>
                        <?php echo $SEQUENCE_FOOTER ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo $CREATED_DATE ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td style="display:none;">
                        <?php echo $STATUS ?>
                    </td>
                    <td>
                        <?php echo $POSITION ?>
                    </td>
                    <td style="text-align: center;">
                        <?php
$paramaction['Controllers'] = $Controllers;
    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $menu;
    $paramaction['Data']['menu'] = $data['datamenu'];
    $paramaction['Modal'] = "modal-lg";
    $enc = base64_encode(json_encode($paramaction));
    ?>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo base_url("menu/detail_menu/") . $ID ?>" class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                    <a href="<?php echo base_url("menu/edit_menu/") . $ID ?>" class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>

                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i>
                                        Activate</a>

                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i>
                                        Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?}elseif($STATUS == "99"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<!-- <script src="<?php echo base_url(); ?>assets/js/table.js"></script> -->
<script type="text/javascript">
function init() {
    // Image lightbox
    $('[data-popup="lightbox"]').fancybox({
        padding: 3,
    });
}

$("#tableData thead th").each(function() {
    var title = $(this).text();
    if (title == "Action" || title == "Image" || title == "Video") {} else if (title == "Type Log") {
        $(this).append(
            '<br><select onchange="changeSelectType(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="UPDATE">UPDATE</option><option value="INSERT">INSERT</option></select>'
        );
    } else if (title == "Language") {
        $(this).append(
            '<br><select onchange="changeSelectLang(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="Indonesia">Indonesia</option><option value="English">English</option></select>'
        );
    } else if (title == "Article") {
        $(this).append(
            '<br><select onchange="changeSelectArticle(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
        );
    } else if (title == "Highlight") {
        $(this).append(
            '<br><select onchange="changeSelectHighlight(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
        );
    } else if (title == "Footer") {
        $(this).append(
            '<br><select onchange="changeSelectFooter(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
        );
    } else if (title == "Status") {
        $(this).append(
            '<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option><option value="99">Publish</option></select>'
        );
    } else if (title == "Status Approve") {
        $(this).append(
            '<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Not Approved</option><option value="99">Approved</></select>'
        );
    } else if (title == "Menu Level") {
        $(this).append(
            '<br><select onchange="changeSelectLevel(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="Menu Parent">Menu Parent</option><option value="Menu Child">Menu Child</></select>'
        );
    } else if (title == "Menu Position") {
        $(this).append(
            '<br><select onchange="changeSelectPosition(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="1">Header</option><option value="2">Footer</option><option value="3">Header & Footer</option></select>'
        );
    } else if (title == "Created Date") {
        $(this).append(
            '<br> <div class="input-group search-date" style="color:#333">' +
            '<span class="input-group-prepend">' +
            '<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
            "</span>" +
            '<input type="text" onchange="changeSelectDate(this)" name="created_date" id="created_date" class="form-control pickadate-selectors" placeholder="Select Date..">' +
            "</div>"
        );
        $(".pickadate-selectors").pickadate({
            selectYears: true,
            selectMonths: true,
            format: "dd mmm yyyy",
            formatSubmit: "dd mmm yyyy",
            // container: "body",
            hiddenName: true,
        });


    } else if (title == "Approved Date") {
        $(this).append(
            '<br> <div class="input-group search-date" style="color:#333">' +
            '<span class="input-group-prepend">' +
            '<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
            "</span>" +
            '<input type="text" onchange="changeSelectApprovedDate(this)" name="effective_date" id="approved_date" class="form-control approved-date" placeholder="Select Date..">' +
            "</div>"
        );
        $(".approved-date").pickadate({
            selectYears: true,
            selectMonths: true,
            format: "dd mmm yyyy",
            formatSubmit: "dd mmm yyyy",
            hiddenName: true,
        });
    } else if (title == "Effective Date" || title == "Date") {
        $(this).append(
            '<br> <div class="input-group search-date" style="color:#333">' +
            '<span class="input-group-prepend">' +
            '<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
            "</span>" +
            '<input type="text" onchange="changeSelectEffectiveDate(this)" name="effective_date" id="effective_date" class="form-control effective-date" placeholder="Select Date..">' +
            "</div>"
        );
        $(".effective-date").pickadate({
            selectYears: true,
            selectMonths: true,
            format: "dd mmm yyyy",
            formatSubmit: "dd mmm yyyy",
            hiddenName: true,
        });
    } else {

        $(this).append(
            '<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
        );
    }
});

var t = $("#tableData").DataTable({
    initComplete: function(settings, json) {
        init();
    },
    serverSide: false,
    autoWidth: true,
    keys: !0,
    order: [],
    autofill: true,
    select: true,
    // responsive: true,
    // scrollX: true,
    // scrollCollapse: true,
    // fixedHeader: true,
    // fixedColumns: {
    // leftColumns: 0,
    // rightColumns: 2,
    // },
    buttons: true,
    dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',
});

t.columns().every(function() {
    var that = this;
    $("input", this.header()).on("keyup", function(e) {
        if (event.keyCode === 8) {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        } else {
            if (this.value.length >= 1) {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            }
        }
    });
});

$("#tableData thead input").on("click", function(e) {
    e.stopPropagation();
});

t.column(9).visible(false);
t.column(10).visible(false);


function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(9)
            .search(dis.value)
            .draw();
    } else {
        t.column(9)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}

function changeSelectLevel(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}

function changeSelectPosition(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(10)
            .search(dis.value)
            .draw();
    } else {
        t.column(10)
            .search(dis.value)
            .draw();
    }

}
</script>