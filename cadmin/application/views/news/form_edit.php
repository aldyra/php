<?php
$param = isset($param) ? $param : "";
$ID = isset($param['ID']) ? $param['ID'] : "";
$ID_CONTENT = isset($param['ID_CONTENT']) ? $param['ID_CONTENT'] : "";
$TITLE = isset($param['TITLE']) ? $param['TITLE'] : "";
$LINK = isset($param['LINK']) ? $param['LINK'] : "";
$LANG = isset($param['LANGUAGE']) ? $param['LANGUAGE'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
$DESCRIPTION = isset($param['DESCRIPTION']) ? $param['DESCRIPTION'] : "";
$Content = isset($param['content']) ? $param['content'] : array();

?>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        LANGUAGE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="hidden" name="id_news" class="form-control" id="id" value="<?php echo $ID ?>">
        <select class="form-control select2modal" name="language" id="language" required="">
            <?php
$LANGUAGE = $this->config->item('LANGUAGE');
$ID_LANGUAGE = $this->config->item('ID_LANGUAGE');
$Exp_ID = explode(":", $ID_LANGUAGE);
$Exp_DESC = explode(":", $LANGUAGE);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    ?>
            <option value="<?php echo $ID ?>" <?php if ($LANG == $ID) {echo 'selected';}?>><?php echo $DESC ?></option>
            <?php
}
?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        CONTENT<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <select id="id_content" class="form-control select2modal" name="id_content" required>
            <option selected disabled>Choose Content</option>
            <?php foreach ($Content as $content) {
    $ID = isset($content['ID']) ? $content['ID'] : "";
    $TITLE_ID = isset($content['TITLE_ID']) ? $content['TITLE_ID'] : "";
    $TITLE_EN = isset($content['TITLE_EN']) ? $content['TITLE_EN'] : "";
    ?>
            <option value="<?php echo $ID ?>" <?php if ($ID == $ID_CONTENT) {echo 'selected';}?>>
                <?php echo $TITLE_ID . ' / ' . $TITLE_EN ?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        TITLE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" name="title" class="form-control" id="title" value="<?php echo $TITLE ?>" required="">
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        DESCRIPTION<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea type="text" name="description" class="form-control" id="description"
            required=""><?php echo $DESCRIPTION ?></textarea>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        SAVED IMAGE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
            data-default-file="<?php echo $LINK ?>" />
        <a href="#" onclick="openTnC('<?php echo base_url('news/tncupload') ?>')">Terms and Conditions Upload</a>

    </div>
</div>

<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();

    $(".select2modal").select2({
        dropdownParent: $('#modal')
    });

    changeformfile();



});
</script>