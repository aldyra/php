$(function(){
	var w_screen = screen.width;
	var h_screen = screen.height;
	var h_navbar = $('div.header-fixed').innerHeight();
	var h_module_right = $('div#module-right').innerHeight();
    var header = $('header').innerHeight();

	if(w_screen < 992){
		$('section#section1').css({'padding-top':h_navbar});
        // $('div.card-module-detail').css({'padding-top':header});
        // $('div.card-module-detail span.times').css({'padding-top':header});
	}else{
		setTimeout(function(){
			$('section#section1').css({'padding-top':h_navbar});
		},500);
	}

	$('#icon-search').click(function(){
		$('#group-search').fadeIn(500);
		$(this).css({'display':'none'});
		$('#input-search').val('');
	});

	$('#icon-close').click(function(){
		$('#group-search').css({'display':'none'});
		$('#icon-search').fadeIn();
		$('#input-search').val('');
	});
});

$(function(){
    setTimeout(function(){
        $('span').css({'font-family':"Nunito-Light"});
    },100);

    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('.scrollup').fadeIn('slow');
        } else {
            $('.scrollup').fadeOut('slow');
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
        return false;
    });
});

function changelang(lang, base, url){
	var data = {
		language : lang,
		origin : url
	}

	$.ajax({
		url : base+'LanguageSwitcher/switchLang/'+lang,
		type : 'post',
		data : data,
		dataType : 'json',
		success : function(res){
			// console.log(res);return;
			window.location.href = res.Url;
		}
	});
	
}

function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)){
    	if(iKeyCode == 188 || iKeyCode == 190){
    		return true;
    	}
        return false;
    }else{
    	return true;
    }

}    

function loader(type){
	$.LoadingOverlay(type, {
	    image       : "",
	    fontawesome : "fa fa-spinner fa-spin"
	});
}

function formatMoney(amount, decimalCount = 0, decimal = ".", thousands = ",") {
	try {
	decimalCount = Math.abs(decimalCount);
	decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

	const negativeSign = amount < 0 ? "-" : "";

	let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
	let j = (i.length > 3) ? i.length % 3 : 0;

	return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	} catch (e) {
	console.log(e)
	}
};

function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)){
        if(iKeyCode == 188 || iKeyCode == 190){
            return true;
        }
        return false;
    }else{
        return true;
    }
}

function showhidesimulation(id){
    var getclass = $('#'+id).attr('class');
    var split = getclass.split(' ');
    var replace = id.replace('-','');
    $('.box-simulation').attr({'class':'t-default box-simulation'});
    $('.box-form').css('display','none');
    $('.result-simulasi').css('display','none');

    if(split.includes("show")){
        $('span#'+id).attr({'class':'t-default box-simulation'});
        $('#'+replace).fadeOut();
    }else{
        $('span#'+id).attr({'class':'t-default box-simulation show'});
        $('#'+replace).fadeIn();
    }
}

function tandaPemisahTitik(b){
    var _minus = false;
    if (b<0) _minus = true;
    b = b.toString();
    b=b.replace(".","");
    b=b.replace("-","");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--){
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)){
            c = b.substr(i-1,1) + "." + c;
        } else {
            c = b.substr(i-1,1) + c;
        }
    }
    if (_minus) c = "-" + c ;
    return c;
}

function numbersonly(ini, e){
    if (e.keyCode>=49){
        if(e.keyCode<=57){
            a = ini.value.toString().replace(".","");
            b = a.replace(/[^\d]/g,"");
            b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
            ini.value = tandaPemisahTitik(b);
            return false;
        }else if(e.keyCode<=105){
            if(e.keyCode>=96){
                a = ini.value.toString().replace(".","");
                b = a.replace(/[^\d]/g,"");
                b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
                ini.value = tandaPemisahTitik(b);
                return false;
            }
            else {
                return false;
            }
        }
        else {
            return false; 
        }
    }else if (e.keyCode==48){
        a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
        b = a.replace(/[^\d]/g,"");
        if (parseFloat(b)!=0){
            ini.value = tandaPemisahTitik(b);
            return false;
        } else {
            return false;
        }
    }else if (e.keyCode==95){
        a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
        b = a.replace(/[^\d]/g,"");
        if (parseFloat(b)!=0){
            ini.value = tandaPemisahTitik(b);
            return false;
        } else {
            return false;
        }
    }else if (e.keyCode==8 || e.keycode==46){
        a = ini.value.replace(".","");
        b = a.replace(/[^\d]/g,"");
        b = b.substr(0,b.length -1);
        if (tandaPemisahTitik(b)!=""){
            ini.value = tandaPemisahTitik(b);
        } else {
            ini.value = "";
        }
        return false;
    } else if (e.keyCode==9){
        return true;
    } else if (e.keyCode==17){
        return true;
    } else {
        return false;
    }
}