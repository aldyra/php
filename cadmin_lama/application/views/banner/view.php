<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('banner/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Banner = isset($Data['banner']) ? $Data['banner'] : array();
?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Content</a>
                <a href="#" class="breadcrumb-item active">Banner</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', 'modal-lg', 'Add Banner', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Banner
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Image</th>
                    <th width="200">Title</th>
                    <th data-orderable="false">Language</th>
                    <th>Created By</th>
                    <th data-orderable="false"  width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Banner as $banner) {
    $ID = isset($banner['ID']) ? $banner['ID'] : "";
    $TITLE = isset($banner['TITLE']) ? $banner['TITLE'] : "";
    $LANGUAGE = isset($banner['LANGUAGE']) ? $banner['LANGUAGE'] : "";
    $LINK = isset($banner['LINK']) ? $banner['LINK'] : "";
    $STATUS = isset($banner['STATUS']) ? $banner['STATUS'] : "";
    $CREATED_BY = isset($banner['CREATED_BY']) ? $banner['CREATED_BY'] : "";
    $CREATED_DATE = isset($banner['CREATED_DATE']) ? $banner['CREATED_DATE'] : "";
    $USER_LOG = isset($banner['USER_LOG']) ? $banner['USER_LOG'] : "";
    $DATE_LOG = isset($banner['DATE_LOG']) ? $banner['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    if ($LANGUAGE == "EN") {
        $LANGUAGE = "English";
    } elseif ($LANGUAGE == "ID") {
        $LANGUAGE = "Indonesia";
    }

    ?>
                <tr>
                    <td>
                        <a href='<?php echo $LINK ?>' data-popup='lightbox' data-fancybox><img src='<?php echo $LINK ?>'
                                class='img-thumbnail' width='80px' heigh='80px'></a>
                    </td>
                    <td>
                        <?php echo $TITLE ?>
                    </td>
                    <td>
                        <?php echo $LANGUAGE ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $banner;
        $paramaction['Modal'] = "modal-lg";
        $this->load->view('is_action', $paramaction);
        ?>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(6).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function changeSelectLang(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}
</script>