<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Giro_ir extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('giro_ir_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "giro_ir/view";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['giro_ir'] = $this->giro_ir_model->view();

        $this->load->view('templates/view', $data);
    }
    public function add_giro_ir()
    {
        $data = array();
        $data['content'] = "giro_ir/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'giro_ir';

        $this->load->view('templates/view', $data);
    }
    public function edit_giro_ir($id)
    {
        $data = array();
        $data['content'] = "giro_ir/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['giro_ir'] = $this->giro_ir_model->get_edit($id);
        $data['data']['detail'] = $this->giro_ir_model->get_detail($id);
        $data['data']['back'] = base_url() . 'giro_ir';
        $this->load->view('templates/view', $data);
    }
    public function detail_giro_ir($id)
    {
        $data = array();
        $data['content'] = "giro_ir/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['giro_ir'] = $this->giro_ir_model->get_edit($id);
        $data['data']['detail'] = $this->giro_ir_model->get_detail_giro_ir($id);
        $data['data']['back'] = base_url() . 'giro_ir';
        $this->load->view('templates/view', $data);
    }
    public function add()
    {
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d', strtotime($_POST['effective_date'])) : "";

        //detail
        $RATE = isset($_POST['rate']) ? $_POST['rate'] : "";
        $BALANCE_ID = isset($_POST['balance_id']) ? $_POST['balance_id'] : "";
        $BALANCE_EN = isset($_POST['balance_en']) ? $_POST['balance_en'] : "";

        $this->db->trans_begin();

        $dataheader['CATEGORY_GIRO'] = $CATEGORY;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;

        $dataheader['CREATED_BY'] = $_SESSION['username'];
        $insert_header = $this->db->insert('MS_GIRO_IR_HEADER', $dataheader);
        $ID_GIRO_IR_HEADER = $this->db->insert_id();

        if (!$insert_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        } else {
            $cpt = count($RATE);

            for ($i = 0; $i < $cpt; $i++) {
                $datadetail['ID_GIRO_IR_HEADER'] = $ID_GIRO_IR_HEADER;
                $datadetail['BALANCE_ID'] = htmlentities($BALANCE_ID[$i]);
                $datadetail['BALANCE_EN'] = htmlentities($BALANCE_EN[$i]);

                $datadetail['RATE'] = $RATE[$i];
                $datadetail['CREATED_BY'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('MS_GIRO_IR_DETAIL', $datadetail);
                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }

            }
            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }
    public function update()
    {
        // var_dump($_POST);exit();
        $ID_GIRO_IR_HEADER = isset($_POST['id_giro_ir_header']) ? $_POST['id_giro_ir_header'] : "";
        $LIST_DELETE = isset($_POST['listDelete']) ? explode(",", $_POST['listDelete']) : "";
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d', strtotime($_POST['effective_date'])) : "";

        $BALANCE_ID = isset($_POST['balance_id']) ? $_POST['balance_id'] : "";
        $BALANCE_EN = isset($_POST['balance_en']) ? $_POST['balance_en'] : "";
        $RATE = isset($_POST['rate']) ? $_POST['rate'] : "";
        $ID_GIRO_IR_DETAIL = isset($_POST['id_detail']) ? $_POST['id_detail'] : "";

        $this->db->trans_begin();
        //UPDATE HEADER
        $dataheader['CATEGORY_GIRO'] = $CATEGORY;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['USER_LOG'] = $_SESSION['username'];
        $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
        $where['ID_GIRO_IR_HEADER'] = $ID_GIRO_IR_HEADER;
        $update_header = $this->db->where($where)->update('MS_GIRO_IR_HEADER', $dataheader);

        if (!$update_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        } else {
            //UPDATE DETAIL

            $cpt = count($RATE);
            for ($i = 0; $i < $cpt; $i++) {

                if ($ID_GIRO_IR_DETAIL[$i] != 'NEW') {
                    $where['ID_GIRO_IR_DETAIL'] = $ID_GIRO_IR_DETAIL[$i];
                    $datadetail['ID_GIRO_IR_HEADER'] = $ID_GIRO_IR_HEADER;
                    $datadetail['BALANCE_ID'] = htmlentities($BALANCE_ID[$i]);
                    $datadetail['BALANCE_EN'] = htmlentities($BALANCE_EN[$i]);

                    $datadetail['RATE'] = $RATE[$i];
                    $datadetail['DATE_LOG'] = date('Y-m-d H:i:s');
                    $datadetail['USER_LOG'] = $_SESSION['username'];
                    $update_detail = $this->db->where($where)->update('MS_GIRO_IR_DETAIL', $datadetail);
                    if (!$update_detail) {
                        $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }
                } else {
                    $datadetail['ID_GIRO_IR_HEADER'] = $ID_GIRO_IR_HEADER;
                    $datadetail['BALANCE_ID'] = htmlentities($BALANCE_ID[$i]);
                    $datadetail['BALANCE_EN'] = htmlentities($BALANCE_EN[$i]);
                    $datadetail['RATE'] = $RATE[$i];
                    $datadetail['CREATED_BY'] = $_SESSION['username'];
                    $insert_detail = $this->db->insert('MS_GIRO_IR_DETAIL', $datadetail);
                    if (!$insert_detail) {
                        $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                }

            }

            $del = count($LIST_DELETE);
            for ($j = 0; $j < $del; $j++) {
                $run_delete = $this->db->delete('MS_GIRO_IR_DETAIL', array('ID_GIRO_IR_DETAIL' => $LIST_DELETE[$j]));
                if (!$run_delete) {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    return $JSON;
                }
            }

            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_GIRO_IR_HEADER = isset($Data['ID_GIRO_IR_HEADER']) ? $Data['ID_GIRO_IR_HEADER'] : "";
        $param['ID_GIRO_IR_HEADER'] = $ID_GIRO_IR_HEADER;
        $Delete = $this->giro_ir_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_GIRO_IR_HEADER']) ? $Data['ID_GIRO_IR_HEADER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_GIRO_IR_HEADER' => $id);
        $Active = $this->giro_ir_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_GIRO_IR_HEADER']) ? $Data['ID_GIRO_IR_HEADER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_GIRO_IR_HEADER' => $id);
        $Active = $this->giro_ir_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_GIRO_IR_HEADER']) ? $Data['ID_GIRO_IR_HEADER'] : "";

        $data['STATUS'] = "99";
        $data['APPROVED_DATE'] = date('Y-m-d H:i:s');
        $data['APPROVED_BY'] = $_SESSION['username'];
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_GIRO_IR_HEADER' => $id);
        $Active = $this->giro_ir_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }

        $dataTransaction = array(
            array(
                'ID' => $id,
                'MODULE' => 'GIRO',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => date('Y-m-d H:i:s'),
                'COLUMN_DATA' => 'APPROVED_DATE',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
            array(
                'ID' => $id,
                'MODULE' => 'GIRO',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => $_SESSION['username'],
                'COLUMN_DATA' => 'APPROVED_BY',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
        );
        $InsertTransaction = $this->db->insert_batch('LOG_TRANSACTION', $dataTransaction);
        if (!$InsertTransaction) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }

        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Approved");
        die(json_encode($JSON));
    }
}