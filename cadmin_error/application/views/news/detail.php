<?php
$data = isset($data) ? $data : "";
$ID = isset($data['news']->ID) ? $data['news']->ID : "";
$TITLE = isset($data['news']->TITLE) ? $data['news']->TITLE : "";
$DESCRIPTION = isset($data['news']->DESCRIPTION) ? $data['news']->DESCRIPTION : "";
$LANGUAGE = isset($data['news']->LANGUAGE) ? $data['news']->LANGUAGE : "";
$LINK = isset($data['news']->LINK) ? $data['news']->LINK : "";
$STATUS = isset($data['news']->STATUS) ? $data['news']->STATUS : "";
$CREATED_BY = isset($data['news']->CREATED_BY) ? $data['news']->CREATED_BY : "";
$CREATED_DATE = isset($data['news']->CREATED_DATE) ? $data['news']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}
$TITLE_EN = isset($data['content']->TITLE_EN) ? $data['content']->TITLE_EN : "";
$TITLE_ID = isset($data['content']->TITLE_ID) ? $data['content']->TITLE_ID : "";

$back = isset($data['back']) ? $data['back'] : '';
$is_update = $authorize['is_update'];
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Content</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Highlight</a>
                <a href="#" class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Highlight</span> -
                Detail
                <small><?php echo $TITLE ?></small>


            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <div class="row">
        <div class="col-md-4">

            <!-- Zooming -->
            <div class="card text-center">
                <div class="card-body">
                    <a href="<?php echo $LINK ?>" data-popup="lightbox">
                        <img class="" src="<?php echo $LINK ?>" alt="" width="200px">
                    </a>
                    <h6><b><?php echo $TITLE_EN . ' / ' . $TITLE_ID ?></b></h6>

                    <h5 class="card-title"><?php echo $TITLE ?></h5>
                    <p class="card-text"><?php echo $DESCRIPTION ?></p>
                </div>

                <div class="card-footer d-flex justify-content-between text-center">
                    <span class="text-muted"><?php echo convertDate($CREATED_DATE) ?>
                    </span>
                    <span>
                        <?php echo $STS ?>

                    </span>
                </div>
            </div>
            <!-- /zooming -->

        </div>
    </div>

</div>