<?php
$param = isset($param) ? $param : "";
$ID = isset($param['ID']) ? $param['ID'] : "";
$TITLE = isset($param['TITLE']) ? $param['TITLE'] : "";
$LINK = isset($param['LINK']) ? $param['LINK'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Title</label><span style="color: orange">*</span><br>
            <input type="text" name="title" class="form-control" id="title" placeholder="" value="<?php echo $TITLE ?>">
            <input type="hidden" name="id_gallery" class="form-control" id="id" value="<?php echo $ID ?>">
        </div>
        <div class="form-group">
            <label>Upload Gallery</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
                data-default-file="<?php echo $LINK ?>" />
            <a href="#" onclick="openTnC('<?php echo base_url('gallery/tncupload') ?>')">Terms and Conditions Upload</a>

        </div>
        <div class="form-group">
            <label>Status</label><br>
            <select class="form-control" name="status" id="status">
                <?php $STATUS = $this->config->item('STATUS');
$ID_STATUS = $this->config->item('ID_STATUS');
$Exp_ID = explode(":", $ID_STATUS);
$Exp_DESC = explode(":", $STATUS);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    if ($ID == $STATUS_DATA) {
        $selected = "selected";
    } else {
        $selected = "";
    }?>
                <option value="<?php echo $ID ?>" <?php echo $selected; ?>><?php echo $DESC ?></option>
                <?php }?>
            </select>

        </div>
    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>


<script type="text/javascript">
$('.dropify').dropify();
changeformfile();
</script>