<?php
$param = isset($param) ? $param : "";
$ID = isset($param['ID']) ? $param['ID'] : "";
$TITLE = isset($param['TITLE']) ? $param['TITLE'] : "";
$LINK = isset($param['LINK']) ? $param['LINK'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
$DESCRIPTION = isset($param['DESCRIPTION']) ? $param['DESCRIPTION'] : "";

$ExpVideo = explode("/", $LINK);
$MaxVideo = sizeof($ExpVideo);
$VideoName = $ExpVideo[$MaxVideo - 1];
$ExpExt = explode(".", $VideoName);
$MaxExt = sizeof($ExpExt);
$Ext = $ExpExt[$MaxExt - 1];
?>

<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        TITLE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" name="title" class="form-control" id="title" value="<?php echo $TITLE ?>" required="">
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        SAVED VIDEO
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
            data-default-file="<?php echo $LINK ?>" />
        <a href="#" onclick="openTnC('<?php echo base_url('video/tncupload') ?>')">Terms and Conditions Upload</a>

    </div>
</div>

<!-- <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        DESCRIPTION
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea type="text" name="description" class="form-control ckeditor"
            id="description"><?php echo $DESCRIPTION ?></textarea>
    </div>
</div> -->
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        STATUS
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $ID ?>">
        <select class="form-control" name="status" id="status">
            <?php
$STATUS = $this->config->item('STATUS');
$ID_STATUS = $this->config->item('ID_STATUS');
$Exp_ID = explode(":", $ID_STATUS);
$Exp_DESC = explode(":", $STATUS);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    if ($ID == $STATUS_DATA) {
        $selected = "selected";
    } else {
        $selected = "";
    }
    ?>
            <option value="<?php echo $ID ?>" <?php echo $selected; ?>><?php echo $DESC ?></option>
            <?php
}
?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    changeformfile();
    $('.dropify').dropify();


    // CKEDITOR.replace('description');
});
</script>