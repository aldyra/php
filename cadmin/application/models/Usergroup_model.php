<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Usergroup_model extends MY_Model
{

    protected $_table_name = 'MS_USER_GROUP';
    protected $_primary_key = 'ID';
    protected $_order_by = 'ID';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function cekDuplicate($name, $id = null)
    {
        if ($id == null) {
            $this->db->where('USER_GROUP_NAME', $name);
            $num = $this->db->get($this->_table_name)->num_rows();
            if ($num >= 1) {
                return 0;
            } else {
                return 1;
            }

        } else {
            $this->db->where('USER_GROUP_NAME', $name);
            $this->db->where_not_in('ID', $id);
            $num = $this->db->get($this->_table_name)->num_rows();
            if ($num >= 1) {
                return 0;
            } else {
                return 1;
            }

        }
    }

    public function select_col($col, $getAction)
    {
        $this->datatables->select($col);
        $this->datatables->from($this->_table_name);

        $edtdelete = '';
        if ($getAction['is_update'] == 1) {
            $edtdelete .= '<a onclick="statusChg(\'$1\')" class="dropdown-item"><i class="$icon"></i> $status</a>';
            $edtdelete .= '<a onclick="openEdit(\'$1\')" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        }

        // if ($getAction['is_delete'] == 1) {
        //     $edtdelete .= '<a onclick="confirmDelete(\'$1\')" class="dropdown-item"><i class="icon-trash"></i> Delete</a>';
        // }

        $this->datatables->add_column('ACTION',
            '<div class="list-icons">
                <div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        ' . $edtdelete . '
                    </div>
                </div>
            </div>', 'ID'
        );

        return $sql = $this->datatables->generate();
    }

    public function get_edit($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->_table_name)->row_array();
    }

    public function delete_row($id)
    {
        $this->db->where($this->_primary_key, $id);
        $this->db->delete($this->_table_name);

        return $this->db->affected_rows() . " Affected Rows";
    }

}