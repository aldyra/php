<?php 
$banner = isset($data['banner']) ? $data['banner']:array();
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
?>
<div class="owl-carousel owl-carousel-banner owl-theme" style="z-index: 0;">
	<?php 
	for($i=0; $i<sizeof($banner); $i++){
		if($i == 0){
			$active = "active";
		}else{
			$active = "";
		}
		$Link = isset($banner[$i]['LINK']) ? $banner[$i]['LINK']:"";
	?>
		<div class="item">
			<img class="d-block w-100 img-carousel img-fluid" src="<?php echo $Link ?>" alt="Slide banner">
		</div>
	<?php 
	} 
	?>
</div>

<section id="section2">
	<div class="container-fluid container-news">
		<div class="row">
			<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<p class="title-section2 t-default" style="font-family: 'Nunito-SemiBold' !important;">
					<?php echo strtoupper($this->lang->line('home1')) ?>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
				<div class="owl-carousel owl-carousel-news owl-theme" style="z-index: 0; ">
					<?php 
					foreach($news as $row){
						$ID = isset($row['ID']) ? $row['ID']:"";
						$TITLE = isset($row['TITLE']) ? $row['TITLE']:"";
						$DESCRIPTION = isset($row['DESCRIPTION']) ? $row['DESCRIPTION']:"";
						$LINK = isset($row['LINK']) ? $row['LINK']:"";
						$ID_CONTENT = isset($row['ID_CONTENT']) ? $row['ID_CONTENT']:"";

						$COUNTDESC = str_word_count($DESCRIPTION);
						$COUNTSTRING = strlen($DESCRIPTION);
						if($COUNTSTRING > 100){
							$DESCRIPTION  = substr($DESCRIPTION, 0, 100)." ...";
						}else{
							$DESCRIPTION  = $DESCRIPTION;
						}

					?>
						<div class="item" style="margin-top: 40px; margin-bottom: 40px;">
							<div class="card" style="cursor: pointer;box-shadow: 0px 2px 10px #716868;" 
							onclick="window.location.href='<?php echo base_url(). $SiteLang ?>/home/redirecturl/<?php echo $ID_CONTENT ?>'">
								<div class="card-header card-image-news d-padding">
									<figure class="figure" style="margin: 0;">
										<img src="<?php echo $LINK ?>" class="figure-img img-fluid rounded img-news" alt="<?php echo $TITLE ?>">
										<div class="title-dreams"><?php echo $TITLE ?></div>
									</figure>
								</div>
								<div class="card-header card-description-news" style="background-color: #e5e5e5;height: 100px; line-height: 75px;">
									<!-- <figure class="figure" style="margin: 0;">
										<figcaption class="figure-caption" style="color: black; font-weight: bold; font-size: 14px; "></figcaption>
									</figure> -->
									<span style="display: inline-block;vertical-align: middle;line-height: normal;font-weight: bold;"><?php echo $DESCRIPTION ?>
									</span>
								</div>
							</div>
						</div>
					<?php
					}
					?>
				</div>
				<button class="btn btn-prev-news" onclick="prev('news')">
					<i class="fa fa-chevron-left icon-prev-news"></i>
				</button>
				<button class="btn btn-next-news" onclick="next('news')">
					<i class="fa fa-chevron-right icon-next-news"></i>
				</button>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function prev(id){
		$('div.owl-carousel-'+id+' button.owl-prev').trigger('click');
	}

	function next(id){
		$('div.owl-carousel-'+id+' button.owl-next').trigger('click');
	}

	$(function(){
		$('.owl-carousel-news').owlCarousel({
		    loop:true,
		    margin:10,
		    responsiveClass:true,
		    autoplay:true,
		    autoplayHoverPause:false,
		    responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:2,
		            nav:false
		        },
		        1000:{
		            items:3,
		            nav:true,
		            loop:false
		        }
		    }
		});
	});
</script>

<style type="text/css">
	section#section2 {
		background: #F2F2F2;
	}

	.img-carousel{
		object-fit: fill;
		object-position: center;
		height: 100% !important;
		width: 100% !important;
	}

	@media all and (max-width: 992px) {
		img.img-news{
			/*height: 270px; */
			height: auto; 
			width: 100%; 
			object-fit: contain; 
			object-position: center; 
			margin: 0;
		}

		div.container-news{
			padding-left: 0;
			padding-right: 0;
		}

		button.btn-prev-news .svg-inline--fa{
			color: #403e3e; 
			font-size: 23px !important;
			line-height: 44px;
			display: -webkit-inline-box;
			vertical-align: middle;
		}

		button.btn-next-news .svg-inline--fa{
			color: #403e3e; 
			font-size: 23px !important;
			line-height: 44px;
			display: -webkit-inline-box;
			vertical-align: middle;
		}

		button.btn-prev-news{
			border-radius: 25px; 
			width: 50px; 
			height: 50px; 
			background-color: rgba(255, 255, 255, 0.7);
			position: absolute;
			left: 3px;
			top: 33%;
		}

		button.btn-prev-news:hover{
			background-color: #eae5e5;
		}

		button.btn-next-news{
			border-radius: 25px; 
			width: 50px; 
			height: 50px; 
			background-color: rgba(255, 255, 255, 0.7);
			position: absolute;
			right: 3px;
			top: 33%;
		}

		button.btn-next-news:hover{
			background-color: #eae5e5;
		}

		.title-dreams{
			background-color: #f24940;
			color: white;
			font-weight: bold;
			position: absolute;
			padding: 10px;
			left: 0;
			width: 70%;
			bottom: 35%;
			font-size: 17px;
			text-align: left;
			opacity: 80%;
		}

		div.owl-carousel-banner.owl-theme .owl-dots .owl-dot span{
			width: 8px !important;
			height: 8px !important;
		}

		div.card-image-news{
			background-color: #e5e5e5; 
			border-bottom: none; 
			height: 210px;
		}
	}

	@media all and (min-width: 992px) {
		img.img-news{
			height: 270px; 
			width: 100%; 
			object-fit: cover; 
			object-position: center; 
			margin: 0;
		}

		button.btn-prev-news{
			display: none;
		}
		button.btn-next-news{
			display: none;
		}
		.title-dreams{
			background-color: #ff0000;
			color: white;
			font-weight: bold;
			position: absolute;
			padding: 10px;
			left: 0;
			width: 70%;
			bottom: 35%;
			font-size:30px;
			text-align: left;
			opacity: 80%;
		}

		div.card-image-news{
			background-color: #e5e5e5; 
			border-bottom: none; 
			height: 270px;
		}
	}

	div.owl-carousel-news div.owl-item{
		/*margin-right: 10px;*/
		/*margin-left: 10px;*/
	}

	div.owl-carousel-news div.owl-item.active{
		/*margin-right: 10px;*/
		/*margin-left: 10px;*/
	}

	div.owl-carousel-news div.owl-item.active.center{
		/*width: 50% !important;*/
	}

	div.owl-carousel-banner div.owl-dots{
		margin-top: -30px;
		z-index: 2 !important;
		position: absolute;
		right: 0;
		left: 0;
	}

	.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span{
		background-color: #213E8B;
	}

	div.owl-carousel-banner div.owl-nav{
		display: none;
	}

	div.owl-carousel-news div.owl-nav{
		display: none;
	}

	div.row-footer-bg{
        background-color: white !important;
    }

	div.row-footer{
		background-color: white !important;
	}

	@media only screen and (max-width: 576px){
		.container-news .title-section2.t-default{
			font-size: 20px;
    		margin-bottom: 0rem;
		}
	}
</style>