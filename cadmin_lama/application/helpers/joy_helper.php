<?php
/*clean url*/
function title_url($str, $replace = array(), $delimiter = '-')
{
    setlocale(LC_ALL, 'en_US.UTF8');

    if (!empty($replace)) {
        $str = str_replace((array) $replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}

function pushArray($data, $module_name)
{
    $res = array();
    foreach ($data as $dt) {
        array_push($res, strtolower($dt->$module_name));
    }
    return $res;
}

/*hash string*/
function hash_string($string = '')
{
    return hash('sha1', $string . config_item('encryption_key'));
}

function auth()
{
    !empty($_SESSION['iduser']) || redirect(base_url("authentication"));

}

function authorize($privilege, $controller)
{
    $haveRead = pushArray($privilege, 'CONTROLLER');
    $controller = strtolower($controller);
    if (!in_array($controller, $haveRead)) {
        die('forbidden page');
        // redirect(base_url("authentication"));
    }

    $isCreate = 0;
    foreach ($privilege as $modul) {
        if (strtolower($modul->CONTROLLER) == $controller) {
            if ($modul->IS_READ == 0) {
                $isRead = 0;
            } else {
                $isRead = 1;
            }
            if ($modul->IS_CREATE == 0) {
                $isCreate = 0;
            } else {
                $isCreate = 1;
            }

            if ($modul->IS_UPDATE == 0) {
                $isUpdate = 0;
            } else {
                $isUpdate = 1;
            }

            if ($modul->IS_DELETE == 0) {
                $isDelete = 0;
            } else {
                $isDelete = 1;
            }

            if ($modul->IS_EXPORT == 0) {
                $isExport = 0;
            } else {
                $isExport = 1;
            }

            if ($modul->IS_IMPORT == 0) {
                $isImport = 0;
            } else {
                $isImport = 1;
            }

            break;
        }
    }

    $res['is_create'] = $isCreate;
    $res['is_read'] = $isRead;
    $res['is_update'] = $isUpdate;
    $res['is_delete'] = $isDelete;
    $res['is_export'] = $isExport;
    $res['is_import'] = $isImport;

    return $res;
}

function hash_link_encode($value)
{
    $_this = &get_instance();
    $_this->load->library('encryption');
    $encode = $_this->encryption->encrypt($value);
    $result = str_replace(array('+', '/', '='), array('-', '_', '~'), $encode);

    return $result;
}

function hash_link_decode($value)
{
    $_this = &get_instance();
    $_this->load->library('encryption');
    $decode = str_replace(array('-', '_', '~'), array('+', '/', '='), $value);
    $result = $_this->encryption->decrypt($decode);

    return $result;
}

/*active menu*/
function active_parent($src, $array = array(), $block = false)
{
    if (in_array($src, $array)) {
        if ($block) {
            return 'style="display:block"';
        } else {
            return 'nav-item-open';
        }

    }
}

function active_menu($uri, $link)
{
    if ($uri == $link) {
        return 'active';
    }
}

function time_elapsed_string($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) {
        $string = array_slice($string, 0, 1);
    }

    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function limitKalimat($kalimat, $limit = 40)
{
    $synopsis = '';
    if (strpos($kalimat, "</p>")) {
        if (strlen($kalimat) > $limit) {
            $synopsis = explode("</p>", $kalimat);
            $synopsis = reset($synopsis);
            $synopsis = substr($synopsis, 0, $limit) . '...</p>';
        } else {
            $synopsis = $kalimat;
            $synopsis = explode("</p>", $synopsis);
            $synopsis = reset($synopsis) . '</p>';
        }
    } else {
        if (strlen($kalimat) > $limit) {
            $synopsis = substr($kalimat, 0, $limit) . '...';
        } else {
            $synopsis = $kalimat;
        }
    }
    return $synopsis;
}

function convertDate($source, $cond = 'default')
{
    $date = new DateTime($source);
    switch ($cond) {
        case 'tgl':
            $r = $date->format('d');
            break;

        case 'bln':
            $r = $date->format('F');
            break;

        case 'thn':
            $r = $date->format('Y');
            break;

        case 'db':
            $r = $date->format('Y-m-d');
            break;

        case 'datepick':
            $r = $date->format('d-M-Y');
            break;

        case 'timestamp':
            $r = $date->format('d M Y H:i');
            break;

        default:
            $r = $date->format('d M Y');
            break;
    }

    return $r;
}

function _linkRegex($link)
{
    if (!empty($link)) {
        if (preg_match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/', $link)) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function _noSpecChar($string)
{
    if (preg_match('/^([a-zA-Z0-9 ])+$/', $string)) {
        return true;
    } else {
        return false;
    }
}

function _routeRegex($string)
{
    if (preg_match('/^([a-zA-Z]{3}) - ([a-zA-Z]{3}) - ([a-zA-Z]{3})+$/', $string)) {
        return true;
    } else {
        return false;
    }
}

function _passRules($string)
{
    // $charLength = ((strlen($string) >= 5)) ? true : false;
    $lowercase = preg_match('/^(?=.*[a-z]).+$/', $string);
    $uppercase = preg_match('/^(?=.*[A-Z]).+$/', $string);
    $numeric = preg_match('/^(?=.*[0-9]).+$/', $string);
    $special = preg_match('/^(?=.*[!@#\$%\^&\*]).+$/', $string);

    if ($lowercase && $uppercase && $numeric && $special) {
        return true;
    } else {
        return false;
    }

}

function validateEmail($email)
{
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}