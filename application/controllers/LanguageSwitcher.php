<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct();    
    }

    function switchLang($lang = "") {
    	$origin = isset($_POST['origin']) ? $_POST['origin']:"";
    	$language = isset($_POST['language']) ? $_POST['language']:"";

        $this->session->set_userdata('site_lang', $lang);

        $exp = explode("/", $origin);
        $explanguage = isset($exp[4]) ? $exp[4]:"";
        
        $_SESSION['site_lang'] = $lang;
        $_SESSION['lang'] = $lang;

        if(isset($_POST['default']))
        {
        	redirect(base_url().$language);
        	die();
        }

        if(in_array($explanguage, array("id","en"))){
            if($language == "id"){
                $url = str_replace("/en", "/id", $origin);
            }elseif($language == "en"){
                $url = str_replace("/id", "/en", $origin);
            }
        }else{
            $url = $origin.$language."/";
        }
        $url = base_url().$_SESSION['site_lang'];

    	die(json_encode(array("ErrorCode"=>"EC:0000", "Url"=>$url)));
    }
}