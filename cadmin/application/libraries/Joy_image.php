<?php

/**
* Library Joy_image diciptakan untuk mengolah data gambar/image (Upload, Delete dan membuat Thumbnail).
*
*/
class Joy_image
{
	private $image_path;
	private $path = '../assets/uploads/images/';
	const DS = DIRECTORY_SEPARATOR;

	/*
	| DIRECTORY_SEPARATOR digunakan untuk memisahkan directory sesuai sistem operasi yang digunakan
	*/

	/*
	* upload_image digunakan untuk mengupload 1 file gambar,
	* parameter yang dibutuhkan, yaitu :
		1. $img_path 		-> lokasi tujuan file yang diupload
		2. $field_name 		-> nilai dari attribut "name" pada tag input bertipe file
		3. $alt 			-> merupakan alt untuk gambar yang juga digunakan untuk mengganti nama gambar yang diupload
	*/
	// buat group folder
	function create_dir($modul)
    {
    	$doc_root = $_SERVER['DOCUMENT_ROOT'].'/admin';
        $date = date('Y-m');
        $path = $doc_root . '/assets/uploads/images/'.$modul.'/' . $date;
        if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
        }

        return $modul.'/'.$date;
    }

	function upload_image($img_path, $field_name, $alt = NULL, $wt, $ht)
	{
		$_this =& get_instance();
		$_this->load->library('upload');

		/*
		| get_instace, mendefinisikan "$_this" sebagai pengganti "$this" agar bisa menggunakan fasilitas dari CI
		| me-load library yang akan digunakan
		*/

		$img_path 		  = $this->create_dir($img_path);
		$this->image_path = realpath(APPPATH . $this->path.$img_path);
		$files 			  = $_FILES[$field_name];
		$rand 			  = rand(11, 10000);

		/*
		| deklarasi lokasi file (path) ke dalam "$this->image_path"
		| deklarasi Predefined Variables ke dalam variable "$files"
		| $rand -> nilai random yang digunakan sebagai nama file yang diupload
		*/

		$config['upload_path']		= $this->image_path;
		$config['allowed_types']	= 'jpg|jpeg|png';
		$config['max_size']			= '10000';
		$ext						= pathinfo($files['name'], PATHINFO_EXTENSION);

		/*
		| konfigurasi lokasi upload
		| konfigurasi tipe gambar (ekstensi) yang diizinkan
		| mendapatkan array nama dan ekstensi file
		*/

		$_FILES[$field_name]['name'] = $alt.'-'.$rand.'.'.$ext;

		/*
		| perubahan nama file
		*/

		$_this->upload->initialize($config);
		
		// validasi upload CI, allowed types and max size
		if (!$_this->upload->do_upload($field_name)) {
			$err['err_message'] = strip_tags($_this->upload->display_errors());
			return $err;
			exit();
		}

		/*
		| upload file
		*/

		$image = $_this->upload->data();

		$_this->image_moo
					->load($this->image_path.self::DS.$image['file_name'])
					->resize($wt,$ht)
					->save_pa('thumb_','');


		/*
		| memasukan data file yang diupload kedalam "$image"
		*/

		$data['image'] 		= $image;
		$data['image_path'] = $this->image_path;

		return $data;

	}

	/*
	* upload_images digunakan untuk mengupload lebih dari 1 file gambar,
	* parameter yang dibutuhkan, yaitu :
		1. $img_path 		-> lokasi tujuan file yang diupload
		2. $field_name 	-> nilai dari attribut "name" pada tag input bertipe file
		3. $alt 				-> merupakan alt untuk gambar yang juga digunakan untuk mengganti nama gambar yang diupload
		4. $thumb_pre		-> prefix yang digunakan untuk nama thumbnail
		5. $wt 					-> (width thumbnail) nilai lebar untuk thumbnail
		6. $ht 					-> (height thumbnail) nilai tinggi untuk thumbnail
	*/
	public function upload_images($img_path, $field_name, $alt=NULL, $wt, $ht)
	{
		$_this =& get_instance();
		$_this->load->library('upload');

		$img_path 		  = $this->create_dir($img_path);
		$this->image_path = realpath(APPPATH . $this->path.$img_path);

		$files 			= $_FILES[$field_name];
		$num_rand		= rand(11, 10000);
		$count_files	= count($files['name']);

		$config['upload_path']		= $this->image_path;
		$config['allowed_types']	= 'jpg|jpeg|png';
		$config['max_size']			= '10000';

		// validasi upload CI, allowed types and max size
		for ($i=0; $i < $count_files; $i++) { 
			if (!empty($files['name'][$i]))
			{
				$ext						 	 = pathinfo($files['name'][$i], PATHINFO_EXTENSION);
				$_FILES[$field_name]['name'] 	 = $alt.'-'.$num_rand.'.'.$ext;
				$_FILES[$field_name]['type'] 	 = $files['type'][$i];
				$_FILES[$field_name]['tmp_name'] = $files['tmp_name'][$i];
				$_FILES[$field_name]['error'] 	 = $files['error'][$i];
				$_FILES[$field_name]['size'] 	 = $files['size'][$i];
				$_this->upload->initialize($config);

				if (!$_this->upload->do_upload($field_name)) {
					$err['err_message'] = strip_tags($_this->upload->display_errors());
					return $err;
					exit();
				}
				else{
					$image = $_this->upload->data();
					unlink($image['full_path']);
				}
			}
		}

		for ($i=0; $i < $count_files; $i++) {

			if (!empty($files['name'][$i])) {
				$unique_rand 					 = $num_rand + $i;
				$ext						 	 = pathinfo($files['name'][$i], PATHINFO_EXTENSION);
				$_FILES[$field_name]['name'] 	 = $alt.'-'.$unique_rand.'.'.$ext;
				$_FILES[$field_name]['type'] 	 = $files['type'][$i];
				$_FILES[$field_name]['tmp_name'] = $files['tmp_name'][$i];
				$_FILES[$field_name]['error'] 	 = $files['error'][$i];
				$_FILES[$field_name]['size'] 	 = $files['size'][$i];


				$_this->upload->initialize($config);
				$_this->upload->do_upload($field_name);

				$image = $_this->upload->data();					
				$_this->image_moo
					->load($this->image_path.self::DS.$image['file_name'])
					->resize($wt,$ht)
					->save_pa('thumb_','');

				$new_name[] = $image['file_name'];
			}

			else {
				$new_name[] = 'no-image.png';
			}

		}

		return $new_name;
	}

	/*
	* delete_image digunakan untuk menghapus 1 file gambar,
	* parameter yang dibutuhkan, yaitu :
		1. $img_path 		-> lokasi tujuan file yang diupload
		2. $image_name 	-> nilai (berupa nama) dari file yang akan dihapus
		3. $thumb 			-> bernilai prefix dari thumbnail gambar yang akan dihapus
	*/
	public function delete_image($img_path, $image_name, $thumb = NULL)
	{
		$_this =& get_instance();

		$this->image_path = realpath(APPPATH . $this->path.$img_path);

		if (!empty($image_name)) {
			unlink($this->image_path.self::DS.$image_name);

			/*
			| gambar dihapus jika parameter pertama tidak kosong (empty)
			*/

			if ($thumb){
				$thumb_img = str_replace('/', '/'.$thumb, $image_name);
				unlink($this->image_path.self::DS.$thumb_img);

				/*
				| thumbnail gambar akan dihapus jika parameter terakhir sesuai dengan perifix thumbnail
				*/

			}
		}
	}
}
