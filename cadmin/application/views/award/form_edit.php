<?php
$param = isset($param) ? $param : "";
$ID_AWARD = isset($param['ID_AWARD']) ? $param['ID_AWARD'] : "";
$TITLE_EN = isset($param['TITLE_EN']) ? $param['TITLE_EN'] : "";
$TITLE_ID = isset($param['TITLE_ID']) ? $param['TITLE_ID'] : "";
$DESCRIPTION_EN = isset($param['DESCRIPTION_EN']) ? $param['DESCRIPTION_EN'] : "";
$DESCRIPTION_ID = isset($param['DESCRIPTION_ID']) ? $param['DESCRIPTION_ID'] : "";
$LINK = isset($param['LINK']) ? $param['LINK'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <input type="hidden" name="id_award" class="form-control" id="id" value="<?php echo $ID_AWARD ?>">
        <div class="form-group">
            <label>Title Indonesian</label><span style="color: orange">*</span><br>
            <input type="text" name="title_id" class="form-control" id="title_id" placeholder=""
                value="<?php echo $TITLE_ID ?>">

        </div>
        <div class="form-group">
            <label>Title English</label><span style="color: orange">*</span><br>
            <input type="text" name="title_en" class="form-control" id="title_en" placeholder=""
                value="<?php echo $TITLE_EN ?>">

        </div>
        <div class="form-group">
            <label>Description Indonesian</label><span style="color: orange">*</span><br>
            <textarea class="form-control" name="description_id" id="DESCRIPTION_ID" rows="4" required=""
                parsley-trigger="change"><?php echo $DESCRIPTION_ID ?></textarea>
        </div>
        <div class="form-group">
            <label>Description English</label><span style="color: orange">*</span><br>
            <textarea class="form-control" name="description_en" id="DESCRIPTION_EN" rows="4" required=""
                parsley-trigger="change"><?php echo $DESCRIPTION_EN ?></textarea>
        </div>
        <div class="form-group">
            <label>Upload Gallery</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
                data-default-file="<?php echo $LINK ?>" />
        </div>
        <a href="#" onclick="openTnC('<?php echo base_url('award/tncupload') ?>')">Terms and Conditions Upload</a>

    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>


<script type="text/javascript">
$('.dropify').dropify();
changeformfile();
</script>