<?php
$lang['home'] = 'Home';
$lang['home1'] = 'Make Your Dreams';

$lang['nav_search'] = 'Search here';
$lang['menu1'] = "Investor Relations";
$lang['share'] = "Share";
$lang['result'] = "Result";
$lang['opening'] = "Openings";

$lang['mod_right1'] = 'Network';
$lang['mod_right2'] = 'SBDK';
$lang['mod_right3'] = 'Exchange Rate';
$lang['mod_right4'] = 'Simulation';

$lang['lowker-0'] = 'position';
$lang['lowker-1'] = 'Find Your Passion';
$lang['lowker-2'] = 'Position Available';
$lang['lowker-3'] = 'Career';
$lang['lowker-4'] = 'Job Vacancy';
$lang['lowker-5'] = 'Position';
$lang['lowker-6'] = 'Registration period';
$lang['lowker-7'] = 'Details';
$lang['lowker-8'] = 'Vacancy Code';
$lang['lowker-9'] = 'Posting Date';
$lang['lowker-10'] = 'Job Description';
$lang['lowker-11'] = 'Requirements';
$lang['lowker-12'] = 'Place the position code at the email`s subject or at the upper left of your envelope. Send your Application Letter, Curriculum Vitae, Academic Degree, Transcript of Record, and 2 pcs of 4 x 6 Photo to: ';
$lang['lowker-13'] = "Let's join the BBA";
$lang['lowker-14'] = 'Develop your career with us';

$lang['kurs-0'] = 'Currency';
$lang['kurs-1'] = 'Buy';
$lang['kurs-2'] = 'Sell';
$lang['kurs-3'] = 'Exchange Rate';
$lang['kurs-4'] = "Other Exchange Rates";
$lang['kurs-5'] = "TT Counter Exchange Rates";
$lang['kurs-6'] = "Disclaimer";
$lang['kurs-7'] = "Exchange rates are subject to change at anytime without prior notice";
$lang['kurs-8'] = "Please contact our nearest BBA Branch Office to get special exchange rate information for transactions with a certain nominal amount.";
$lang['kurs-9'] = "Last Updated";

$lang['sbdk-0'] = 'Other SBDK';
$lang['sbdk-1'] = 'Product';
$lang['sbdk-2'] = 'Interest Rate';
$lang['sbdk-3'] = 'Corporate Credit';
$lang['sbdk-4'] = 'Retail Credit';
$lang['sbdk-5'] = 'Micro Credit';
$lang['sbdk-6'] = "(effective % per year)";
$lang['sbdk-7'] = "PRIME LENDING RATE";
$lang['sbdk-8'] = "Prime Lending Rate";

$lang['keterangan_sbdk'] = '<p><strong>Information :</strong></p>
							<ol>
							<li> The Prime Lending Rate (SBDK) is used as the basis for determining the loan interest rate that the Bank will charge to customers. Prime Lending Rate does not take into account the risk premium estimation component, the amount of which depends on the Bank\'s assessment of the risk of each debtor or group of debtors. Thus, the amount of credit interest rate charged to debtors is not necessarily the same as the prime lending rate </li>
							<li> Non-mortgage consumption credit does not include disbursement of funds via credit cards and unsecured credit (KTA). </li>
							<li> Information on prime lending rates that are valid at any time can be seen in publications at each Bank office and / or on the Bank\'s website. </li>
							</ol>

							<p> * Interest rates are subject to change without prior notice </p>';
$lang['efektif_pertahun'] = '(effective % per year)';
$lang['berdasarkan_segmen'] = 'Based on the Credit Segment';
$lang['kredit_korporasi'] = 'Corporate Credit';
$lang['kredit_retail'] = 'Retail Credit';
$lang['kredit_mikro'] = 'Micro Credit';
$lang['kredit_konsumsi'] = 'Consumption Credit';
$lang['kpr'] = 'MORTGAGE';
$lang['non_kpr'] = 'Non-MORTGAGE';

$lang['tabungan'] = 'Savings';
$lang['giro'] = 'Giro';
$lang['deposito'] = 'Deposito';
$lang['suku_bunga_ket'] = '*Interest rates can change at any time without prior notice';

$lang['suku_bunga_0'] = 'Month';
$lang['suku_bunga_1'] = 'Balance';
$lang['suku_bunga_2'] = 'Rates';
$lang['suku_bunga_3'] = 'Interest Rates';
$lang['suku_bunga_4'] = 'Interest Rates Before';
$lang['suku_bunga_5'] = 'Interest Rates Next';

$lang['penghargaan_0'] = 'Awards';

$lang['cabang-0'] = "branch";
$lang['cabang-1'] = "location";
$lang['cabang-2'] = "Please Select Province";
$lang['cabang-3'] = "Please Select District / City ";
$lang['cabang-4'] = "Branch Office";
$lang['cabang-5'] = "Sub Branch Office";
$lang['cabang-6'] = "Cash Office";
$lang['cabang-7'] = "Payment Point";
$lang['cabang-8'] = "Branch, Sub Branch, Cash Credit Office and Payment Point Location at ";
$lang['cabang-9'] = "More details";

$lang['manajemen_0'] = 'More Info';
$lang['manajemen_1'] = 'Work History';
$lang['manajemen_2'] = 'Year';
$lang['manajemen_3'] = 'Position';

$lang['search-0'] = "search";
$lang['search-1'] = "keywords";
$lang['search-2'] = "Keywords";
$lang['search-3'] = "Post date";
$lang['search-4'] = "Data not found";
$lang['search-5'] = "Search result";
$lang['search-6'] = "Recent Video";

$lang['simulation-0'] = "Simulation";
$lang['simulation-1'] = "Home Prices";
$lang['simulation-2'] = "Interest";
$lang['simulation-3'] = "Length of Credit (Years)";
$lang['simulation-4'] = "Choose Year";
$lang['simulation-5'] = "Calculation";
$lang['simulation-6'] = "Calculate Simulation";
$lang['simulation-7'] = "PPR Simulation";
$lang['simulation-8'] = "PPM Simulation";
$lang['simulation-9'] = "Start";
$lang['simulation-10'] = "PPR Information";
$lang['simulation-11'] = "PPM Information";
$lang['simulation-12'] = "Loan Amount";
$lang['simulation-13'] = "Length of Credit (Month)";
$lang['simulation-14'] = "Year";
$lang['simulation-15'] = "Month";
$lang['simulation-16'] = "Installment";
$lang['simulation-17'] = "This calculation is only a simulation. For more information, please contact the bank.";
$lang['simulation-18'] = "Car Prices";

$lang['footer-0'] = "Head Office Bank Bumi Arta";
$lang['footer-1'] = "24 Hours Service";

$lang['pemenang-0'] = "Announcement of Tabungan Kesra Grand Prize Winner";
$lang['pemenang-1'] = "Period";
$lang['pemenang-2'] = "Date";
$lang['pemenang-3'] = "Winner List";
$lang['pemenang-4'] = "Click here to download";
$lang['pemenang-5'] = "Data not found";
$lang['pemenang-6'] = "";
$lang['pemenang-7'] = "";