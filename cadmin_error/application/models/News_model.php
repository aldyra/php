<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class News_model extends MY_Model
{

    protected $_table_name = 'MS_NEWS';
    protected $_primary_key = 'ID';
    protected $_order_by = 'ID';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get('MS_NEWS');
        return $view->result_array();
    }
    public function get_edit($id)
    {
        // $this->db->select('A.*,B.NAME_ID, B.NAME_EN, B.LINK_ID,B.LINK_EN');
        // $this->db->from('MS_NEWS A');
        // $this->db->join('MS_MENU B ', 'A.ID_MENU = B.ID', 'left');
        // $this->db->where('A.ID', $id);
        // $query = $this->db->get();

        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }

    public function updateaction($param)
    {
        $LINK = isset($param['LINK']) ? $param['LINK'] : "";
        if (!empty($LINK)) {
            $DATA['LINK'] = isset($param['LINK']) ? $param['LINK'] : "";
        }

        $ID = isset($param['ID']) ? $param['ID'] : "";
        $WHERE['ID'] = $ID;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID'] = isset($param['ID']) ? $param['ID'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }
}