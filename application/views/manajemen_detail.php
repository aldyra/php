<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>

<style>
	.content-manajemen{
		max-width: 95%;
	}
	.item-manajemen{
		padding: 5px 0;
	}
	.image-title img{
		max-width: 100px;
		width: 173px;
	    height: 240px;
	    object-fit: cover;
	}

	.item-manajemen .title{
		font-weight: bold;
		color: #00326c;
		font-size: 15px;
	}
	table.tbl-riwayat {
	    width: 100%;
	}
	.tbl-riwayat th {
	    text-align: center;
	    background: #243E8B;
	    color: white;
	    font-weight: normal;
	}
	.tbl-riwayat td {
	    text-align: center;
	    color: #3a3a3a;
	}
	.tbl-riwayat th, .tbl-riwayat td {
	    font-size: 14px;
	    padding: 8px 0;
	    /*width: 50%;*/
	    border: 1px solid #999999;
	}
	.riwayat-text{
		font-weight: bold;
	    /*color: #00326c;*/
	    font-size: 15px;
	}

	.image-title{
		text-align: center;
	    flex-grow: 0;
	    flex-shrink: 0;
	    flex-basis: 20%;
	}
	.image-title p{
		font-size: 14px;
    	font-weight: bold;
	}

	.row-socialize{
		padding-bottom: 30px;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-manajemen {
		    max-width: 100%;
		}
		.content-manajemen > h3{
			margin-top: 1.5rem; 
			font-size: 20px;
		}
		p.riwayat-text{
			text-align: center;
			font-size: 18px;
		}
		.image-title p{
			margin-bottom: 1rem !important;
		}
		.tbl-riwayat tr > th:first-child{
			width: 25% !important;
		}
		.tbl-riwayat th, .tbl-riwayat td {
		    font-size: 14px;
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}
	}
</style>
<div class="row row-header" style="padding-left: 0; padding-right: 0;">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="padding-left: 0; padding-right: 0;">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"><?php echo $MENU_PARENT?></a> / -->
		<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<?php } ?> 
		
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
			<?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-8">
			<div class="content-manajemen" style="text-align: justify;">
				<h3 class="mb-4"><strong><?php echo $contentData['provideData']['HEADER']['NAME']; ?></strong></h3>
				<div class="desc-image mb-4 d-lg-flex d-block d-md-block align-items-center">
					<div class="image-title mr-3">
						<img style="max-width:100%;text-align:center;" class="mb-2" src="<?php echo $contentData['provideData']['HEADER']['LINK']; ?>" alt="">
						<p class="mb-0 text-center"><?php echo $contentData['provideData']['HEADER']['POSITION']; ?></p>
					</div>
					<p class="mb-0" style="font-size: 14px;"><?php echo $contentData['provideData']['HEADER']['DESCRIPTION']; ?></p>
				</div>

				<p class="mb-2 riwayat-text"><?php echo $this->lang->line('manajemen_1') ?></p>
				<table class="tbl-riwayat">
					<tr>
						<th style="width: 200px"><?php echo $this->lang->line('manajemen_2') ?></th>
						<th><?php echo $this->lang->line('manajemen_3') ?></th>
					</tr>
					<?php foreach($contentData['provideData']['DETAIL'] as $val){ ?>
					<tr>
						<td>
							<?php 
								if(isset($_SESSION['site_lang']) && $_SESSION['site_lang'] == "id"){
									$ValueYear = $val['YEAR_ID'];
								}elseif(isset($_SESSION['site_lang']) && $_SESSION['site_lang'] == "en"){
									$ValueYear = $val['YEAR_EN'];
								}
								echo $ValueYear;
							?>
						</td>
						<td style="text-align: left !important; padding-left: 10px; padding-right: 10px;"><?php echo $val['DESCRIPTION'] ?> </td>
					</tr>
					<?php } ?>
				</table>

				<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
				
			</div>
		</div>
	</div>
</div>