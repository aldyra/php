<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Category</label><span style="color: orange">*</span><br>
            <select class="form-control select2modal" name="category" id="category" required="">
                <option selected disabled>Choose Category</option>
                <?php
$REPORT_ID = $this->config->item('REPORT_ID');
$REPORT_EN = $this->config->item('REPORT_EN');
$ID_CATEG_REPORT = $this->config->item('ID_CATEG_REPORT');
$Exp_REPORT_ID = explode(":", $REPORT_ID);
$Exp_REPORT_EN = explode(":", $REPORT_EN);
$Exp_ID_CATEG_REPORT = explode(":", $ID_CATEG_REPORT);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_REPORT); $i++) {
    $ID = $Exp_ID_CATEG_REPORT[$i];
    $DESC_EN = $Exp_REPORT_EN[$i];
    $DESC_ID = $Exp_REPORT_ID[$i];
    ?>
                <option value="<?php echo $ID ?>"><?php echo $DESC_EN . ' / ' . $DESC_ID ?></option>
                <?php
}
?>
            </select>
        </div>
        <div class="form-group">
            <label>Report Name Indonesia</label><span style="color: orange">*</span><br>
            <input type="text" name="report_name_id" class="form-control" id="report_name_id" placeholder=""
                required="">
        </div>
        <div class="form-group">
            <label>Report Name English</label><span style="color: orange">*</span><br>
            <input type="text" name="report_name_en" class="form-control" id="report_name_en" placeholder=""
                required="">
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="form-group">
                    <label>Upload File Indonesia</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_id" name="file_id" class="dropify" required="" />
                    <a href="#" onclick="openTnC('<?php echo base_url('report/tncupload') ?>')">Terms and Conditions
                        Upload</a>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="form-group">
                    <label>Upload File English</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_en" name="file_en" class="dropify" required="" />
                    <a href="#" onclick="openTnC('<?php echo base_url('report/tncupload') ?>')">Terms and Conditions
                        Upload</a>

                </div>
            </div>
        </div>


    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    $('.select2modal').select2({
        dropdownParent: $('#modal')
    });
    changeformfile();
});
</script>