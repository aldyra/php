$(function(){
	var w_window = screen.width;
	var h_banner = $('#module-right').innerHeight();
	var h_navbar = $('.header-fixed').innerHeight();
	if(w_window > 992){
		$('img.img-carousel').css({'height':h_banner});
		$('#section1').css({'padding-top':h_navbar});
	}

	$('.owl-carousel-banner').owlCarousel({
	    loop:true,
	    margin:10,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	        600:{
	            items:1,
	            nav:false
	        },
	        1000:{
	            items:1,
	            nav:true,
	            loop:false
	        }
	    }
	})
});