<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('prospectus/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Prospectus = isset($Data['prospectus']) ? $Data['prospectus'] : array();
?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master Reporting</a>
                <a href="#" class="breadcrumb-item active">Prospectus</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Prospectus', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Prospectus
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Document ID</th>
                    <th width="200">Document EN</th>
                    <th>Created By</th>
                    <th data-orderable="false"  width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php
foreach ($Prospectus as $prospectus) {
    $ID = isset($prospectus['ID']) ? $prospectus['ID'] : "";
    $LINK_ID = isset($prospectus['LINK_ID']) ? $prospectus['LINK_ID'] : "";
    $LINK_EN = isset($prospectus['LINK_EN']) ? $prospectus['LINK_EN'] : "";
    $STATUS = isset($prospectus['STATUS']) ? $prospectus['STATUS'] : "";
    $CREATED_BY = isset($prospectus['CREATED_BY']) ? $prospectus['CREATED_BY'] : "";
    $CREATED_DATE = isset($prospectus['CREATED_DATE']) ? convertDate($prospectus['CREATED_DATE'], 'timestamp') : "";
    $USER_LOG = isset($prospectus['USER_LOG']) ? $prospectus['USER_LOG'] : "";
    $DATE_LOG = isset($prospectus['DATE_LOG']) ? $prospectus['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <?php if (strpos($LINK_ID, '.pdf') == false) {?>
                        <a href='<?php echo $LINK_ID ?>' target="_blank"><?php echo basename($LINK_ID) ?></a>
                        <?php } else {?>
                        <a href='<?php echo $LINK_ID ?>' data-popup='lightbox'
                            data-fancybox><?php echo basename($LINK_ID) ?>
                        </a>
                        <?php }?>
                    </td>
                    <td>
                        <?php if (strpos($LINK_EN, '.pdf') == false) {?>
                        <a href='<?php echo $LINK_EN ?>' target="_blank"><?php echo basename($LINK_EN) ?></a>
                        <?php } else {?>
                        <a href='<?php echo $LINK_EN ?>' data-popup='lightbox'
                            data-fancybox><?php echo basename($LINK_EN) ?>
                        </a>
                        <?php }?>

                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo $CREATED_DATE ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
        ?>
                    <td style="text-align: center;">
                        <?php
$paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $prospectus;
        $paramaction['Modal'] = "";
        $this->load->view('is_action', $paramaction);
        ?>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(5).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(3)
            .search(dis.value)
            .draw();
    } else {
        t.column(3)
            .search(dis.value)
            .draw();
    }

}
</script>