<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Branch_model extends MY_Model
{

    protected $_table_name = 'MS_BRANCH';
    protected $_primary_key = 'ID_BRANCH';
    protected $_order_by = 'ID_BRANCH';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }
    public function get_edit_branch($id)
    {
        $result = $this->db->query('SELECT A.*, B.PROVINCE_NAME, C.CITY_KAB_NAME, D.KECAMATAN_NAME, E.KELURAHAN_NAME, E.POSTAL_CODE
            FROM MS_BRANCH A
            LEFT JOIN MS_PROVINCE B ON A.ID_PROVINCE = B.PROVINCE_ID
            LEFT JOIN MS_CITY_KAB C ON A.ID_CITY = C.CITY_KAB_ID
            LEFT JOIN MS_KECAMATAN D ON A.ID_KECAMATAN = D.KECAMATAN_ID
            LEFT JOIN MS_KELURAHAN E ON A.ID_KELURAHAN = E.KELURAHAN_ID
            WHERE A.ID_BRANCH ="' . $id . '"')->row();
        return $result;
    }
    public function province()
    {
        $this->db->where("STATUS", '99');

        $province = $this->db->get('MS_PROVINCE')->result();
        return $province;
    }
    public function city($id = null)
    {

        $city = $this->db->get('MS_CITY_KAB')->result();
        return $city;
    }
    public function kecamatan()
    {
        $kecamatan = $this->db->get('MS_KECAMATAN')->result();
        return $kecamatan;
    }
    public function kelurahan()
    {
        $kelurahan = $this->db->get('MS_KELURAHAN')->result();
        return $kelurahan;
    }
    public function updateaction($param)
    {
        $ID = isset($param['ID_BRANCH']) ? $param['ID_BRANCH'] : "";
        $WHERE['ID_BRANCH'] = $ID;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_BRANCH'] = isset($param['ID_BRANCH']) ? $param['ID_BRANCH'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }
}