<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('video/add');

$Data = isset($Data) ? $Data : "";
$Video = isset($Data['video']) ? $Data['video'] : array();

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];
?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Content</a>
                <a href="#" class="breadcrumb-item active">Video</a>
                <!-- <span class="breadcrumb-item \ active">Dashboard</span> -->
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', 'modal-lg', 'Add Video', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Video
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Video</th>
                    <th>Title</th>
                    <th width="200">Copy Link</th>
                    <th>Created By</th>
                    <th data-orderable="false" width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
    ?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php
foreach ($Video as $video) {
    $ID = isset($video['ID']) ? $video['ID'] : "";
    $TITLE = isset($video['TITLE']) ? $video['TITLE'] : "";
    $LANGUAGE = isset($video['LANGUAGE']) ? $video['LANGUAGE'] : "";
    $LINK = isset($video['LINK']) ? $video['LINK'] : "";
    $STATUS = isset($video['STATUS']) ? $video['STATUS'] : "";
    $CREATED_BY = isset($video['CREATED_BY']) ? $video['CREATED_BY'] : "";
    $CREATED_DATE = isset($video['CREATED_DATE']) ? $video['CREATED_DATE'] : "";
    $USER_LOG = isset($video['USER_LOG']) ? $video['USER_LOG'] : "";
    $DATE_LOG = isset($video['DATE_LOG']) ? $video['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <a href='<?php echo $LINK ?>' data-popup='lightbox'>
                            <video width='150' autoplay='' class='img-thumbnail' muted>

                                <source src='<?php echo $LINK ?>' type='video/mp4'>

                            </video>
                        </a>
                    </td>
                    <td>
                        <?php echo $TITLE ?>
                    </td>
                    <td>
                        <a data-toggle="tooltip" data-placement="top" title="Copy Link" href="#"
                            onclick="copy_text('<?php echo $LINK ?>')">COPY LINK</a>
                    </td>

                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
        ?>
                    <td style="text-align: center;">
                        <?php
$paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $video;
        $paramaction['Modal'] = "modal-lg";
        $enc = base64_encode(json_encode($paramaction));

        ?>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_update)) {?>
                                    <a onclick="updateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-pencil"></i> Edit</a>

                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i>
                                        Activate</a>

                                    <?}elseif($STATUS == "88"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?}elseif($STATUS == "99"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?php }?>
                                    <?php }?>
                                    <?php if (!empty($is_delete)) {?>
                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(6).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function copy_text(text) {

    var input = document.createElement('input');
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    var result = document.execCommand('copy');
    document.body.removeChild(input);
    Swal.fire({
        type: 'success',
        title: 'Link Video Copied',
        showConfirmButton: false,
        timer: 1000
    });
}
</script>