<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin | Bank Bumi Arta</title>
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo-bumi-artha.png">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/sweetalert.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/joy-style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/libs/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/libs/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/libs/jquery-ui/jquery-ui.structure.min.css">
    <link href="<?php echo base_url(); ?>assets/libs/select2/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/Custom.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/lib/jstree/themes/proton/style.min.css" />


    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Treeview css -->
    <link href="<?php echo base_url(); ?>assets/treeview/css/style.css" rel="stylesheet" type="text/css" />
    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/parsleyjs/parsley.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/parsleyjs/init_parsley.js"></script>
    <!-- CKEDITOR -->
    <script src="<?php echo base_url(); ?>assets/libs/ckeditor4/ckeditor.js"></script>
    <!-- DATEPICKER -->
    <script type="text/javascript" src="<?=base_url()?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.js"></script>
    <!-- Select2 js -->
    <script src="<?php echo base_url(); ?>assets/libs/select2/select2.min.js"></script>

    <script src="<?php echo base_url() ?>assets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/libs/datatables/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url() ?>assets/libs/datatables/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/libs/datatables/responsive.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <script src="<?php echo base_url() ?>assets/js/lib/jstree/jstree.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/JavaScript/public.js"></script>
</head>

<?php
$HOST = $_SERVER['REQUEST_URI'];
$Exp = explode("/", $HOST);
$Max = sizeof($Exp) - 1;
$Controllers = $Exp[$Max];

$menu = isset($_SESSION['menu']) ? $_SESSION['menu'] : "";
$MODULE_NAME_ACTIVE = "";
foreach ($menu as $submenu) {
    $ID = isset($submenu['ID']) ? $submenu['ID'] : "";
    $MODULE_NAME = isset($submenu['MODULE_NAME']) ? $submenu['MODULE_NAME'] : "";
    $CONTROLLER = isset($submenu['CONTROLLER']) ? $submenu['CONTROLLER'] : "";
    $CONTROLLER = str_replace(" ", "", $CONTROLLER);
    $FUNCTION = isset($submenu['FUNCTION']) ? $submenu['FUNCTION'] : "";
    $FUNCTION = str_replace(" ", "", $FUNCTION);
    $ICON = isset($submenu['ICON']) ? $submenu['ICON'] : "";
    $SUBMENU = isset($submenu['SUBMENU']) ? $submenu['SUBMENU'] : "";

    if (empty(sizeof($SUBMENU))) {
        if (strtolower($CONTROLLER) == $Controllers) {
            $MODULE_NAME_ACTIVE = $MODULE_NAME;
        }
    } else {
        foreach ($SUBMENU as $SUB_MENU) {
            $Url = strtolower(base_url() . $SUB_MENU['CONTROLLER']);
            if (strtolower($SUB_MENU['CONTROLLER']) == $Controllers) {
                $MODULE_NAME_ACTIVE = $SUB_MENU['MODULE_NAME'];
            }
        }
    }
}

$paramheader['MODULE_NAME_ACTIVE'] = $MODULE_NAME_ACTIVE;
$paramcontent['MODULE_NAME_ACTIVE'] = $MODULE_NAME_ACTIVE;
?>
<style type="text/css">

</style>

<body>
    <!-- Main navbar -->
    <?php $this->load->view('navbar')?>
    <!-- /Main navbar -->
    <div class="page-content">
        <!-- Main Module -->
        <?php $this->load->view('module')?>
        <!-- /Main Module -->
        <div class="content-wrapper">
            <!-- Page header -->
            <?php $this->load->view('header', $paramheader)?>
            <!-- /page header -->

            <div class="content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
                        <?php $this->load->view($content, $paramcontent);?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true"></div>
</body>

</html>