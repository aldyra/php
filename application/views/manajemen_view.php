<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>

<style>
	.content-manajemen{
		max-width: 95%;
	}
	.item-manajemen{
		padding: 15px 20px 55px;
		text-align: center;
		box-shadow: 0px 2px 15px #e5e5e5;
		min-height: 100%;
		position: relative;
	}
	.item-manajemen img{
		max-width: 100%;
		/*width: 190px;*/
		width: 100%;
		height: 270px;
		object-fit: cover;
		object-position: center;
	}

	.item-manajemen .title{
		font-weight: bold;
		color: #2A2C2F;
		font-size: 15px;
	}
	.item-manajemen .position{
		font-weight: bold;
		color: #2A2C2F;
		font-size: 14px;
	}

	.detail-singkat{
		font-size: 14px;
	}

	.btn-more-info{
		background: #243E8B;
    	color: #fff !important;
    	position: absolute;
	    bottom: 30px;
	    left: 0;
	    right: 0;
	    margin: auto;
	}

	.box-manajemen.desktop{
		display: block;
	}
	.box-manajemen.mobile{
		display: none;
	}

	
	.row-page-module{
		font-size: 14px;
	}

	.row-socialize{
		padding-bottom: 30px;
	}

	@media all and (max-width: 992px) {
		.owl-stage-outer{
			box-shadow: 0px 2px 15px #e5e5e5;
		}

		.row-header{
			text-align: center;
		}

		.row-header h4{

		}

		.row-header h4{
			position: absolute;
			bottom: 0;
			left: 0;
			right: 0;
		}
	}

	@media all and (min-width: 992px) {
		.row-header{
			text-align: left;
		}

		.row-header h4{
			position: absolute;
			bottom: 0;
		}
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-manajemen {
		    max-width: 100%;
		}
		.content-manajemen > h3{
			margin-top: 1.5rem; 
			font-size: 20px;
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}

		.box-manajemen.desktop{
			display: none;
		}
		.box-manajemen.mobile{
			display: block;
		}
		.row.header-fixed{
			z-index: 999;
		}
		.item-manajemen p.desc{
			height: 190px;
		    overflow: hidden;
		    text-overflow: ellipsis;
		}
		.item-manajemen img {
		    max-width: 100%;
		    width: 190px !important;
		    height: 270px;
		    object-fit: cover;
		    margin: 0 auto 1rem !important;
		}

		.owl-carousel .owl-nav button.owl-prev, .owl-carousel .owl-nav .owl-next{
			background: #EAEAEA !important;
		    height: 40px;
		    width: 40px;
		    border-radius: 50%;
		    position: absolute;
		    top: 50%;
		    transform: translateY(-50%);
		}
		.owl-carousel .owl-nav button.owl-prev{
			left: 0;
		}
		.owl-carousel .owl-nav .owl-next{
			right: 0;
		}
		.owl-carousel .owl-nav button.owl-prev span, .owl-carousel .owl-nav button.owl-next span{
		    font-size: 40px;
		    font-weight: bold;
		    position: absolute;
		    top: -13px;
		    left: 15px;
		    color: #B9B9B9;
		}
	}
</style>
<div class="row row-header" style="padding-left: 0; padding-right: 0;">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="padding-left: 0; padding-right: 0;">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>">
			<?php echo $MENU_PARENT?>
		</a> --> 
		<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
			<?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-8">
			<div class="content-manajemen">
				<h3 class="mb-4" style="font-size: 18px;"><strong><?php echo $MENU_CHILD; ?></strong></h3>
				<div class="footer-note mb-4" style="font-size: 14px;text-align: justify;">
					<?php echo $contentData['provideData']['menuDetail']['FOOTER_NOTE'] ?>
				</div>

				<div class="box-manajemen desktop">
					<!-- <table style="width: 100%:"> -->
					<div class="row">
					<?php foreach($contentData['provideData']['data']['HEADER'] as $val){ ?>
						<div class="col-lg-4 d-padtop-10">
							<div class="item-manajemen">
								<img class="mr-2 mb-3" src="<?=$val['LINK']?>" alt="<?php echo 'gambar_'.$val['NAME'] ?>">
								<p class="title mb-1"><?php echo $val['NAME'] ?></p>
								<p class="position mb-3"><?php echo $val['POSITION'] ?></p>
								<p class="desc" style="margin-bottom: 2rem !important;text-align: justify;font-size: 14px;"><?php echo $val['DESCRIPTION'] ?></p>
								<a style="font-size: 14px;font-weight: bold;color: #00326c;"
								 href="<?php echo base_url($_SESSION['lang'].'/'.$this->uri->segment(2).'/detail/'.slugify($val['NAME'])); ?>">
								 <button class="btn btn-more-info"><?php echo $this->lang->line('manajemen_0') ?></button>
								</a>
							</div>
						</div>
					<?php } ?>
					</div>
					<!-- </table> -->
				</div>

				<div id="owlManajemen" class="box-manajemen mobile owl-carousel">
					<!-- <div class="row"> -->
						<?php foreach($contentData['provideData']['data']['HEADER'] as $val){ ?>
							<div class="item-manajemen">
								<img class="mr-2 mb-3" src="<?=$val['LINK']?>" alt="<?php echo 'gambar_'.$val['NAME'] ?>">
								<p class="title mb-1"><?php echo $val['NAME'] ?></p>
								<p class="position mb-3"><?php echo $val['POSITION'] ?></p>
								<p class="desc" style="margin-bottom: 2rem !important;text-align: justify;font-size: 14px;"><?php echo $val['DESCRIPTION'] ?></p>
								<a style="font-size: 14px;font-weight: bold;color: #00326c;"
								 href="<?php echo base_url($_SESSION['lang'].'/'.$this->uri->segment(2).'/detail/'.slugify($val['NAME'])); ?>">
								 <button class="btn btn-more-info"><?php echo $this->lang->line('manajemen_0') ?></button>
								</a>
							</div>
						<?php } ?>
					<!-- </div> -->
				</div>

				<?php require_once(APPPATH.'views/share_sosmed.php'); ?>

			</div>

			

		</div>
	</div>
</div>

<script>
	$(document).ready(function(){

		$("#owlManajemen").owlCarousel({
		    nav:true,
		    loop:true,
		    margin:10,
		    responsiveClass:true,
		    autoplay:true,
		    autoplayHoverPause:false,
			responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:1,
		            nav:false
		        },
		        1000:{
		            items:3,
		            nav:true,
		            loop:false
		        }
		    }
		});

	});
</script>