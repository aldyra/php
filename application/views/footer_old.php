<style type="text/css">
	img.img-footer-bisnis{
		height: 40px;
		width: auto;
	}

	img.img-logo-referal{
		height: 50px;
		width: auto;
	}

	img.icon-socialize{
		margin-top: 15px;
		margin-left: 5px;
		margin-right: 5px;
		height: 25px;
		width: 25px;
	}

	div.footer-location h4.txt_kantor_pusat{
		margin-top: 1rem;
	}

	@media only screen and (max-width: 576px){
        div.footer-location h4.txt_kantor_pusat{
        	font-size: 18px !important;
        }
        div.footer-location .txt_alamat{
        	font-size: 14px;
        }
        div.footer-location .txt_alamat .txt_nama_pt{
        	font-size: 16px;
        	margin-bottom: .5rem;
    		display: inline-block;
        }
        .telp_atm{
        	font-size: 14px;
        }
        div.row-footer-module {
		    /*padding-top: 20px;*/
		    text-align: center;
		    font-size: 12px;
		    margin-bottom: .5rem;
		}
		div.row-copyright span{
			font-size: 12px !important;
			color: #243E8B;
		}
    }
</style>

<div class="footer" style="display: contents !important;">
	<div class="row">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 d-martop-10 footer-location">
			<h4 class="txt_kantor_pusat"><b class="t-default" style="font-family: 'Nunito-Bold';font-weight: 500;">Kantor Pusat Bank Bumi Arta</b></h4><br>
			<h6 class="f-black txt_alamat">
				<b class="f-bold txt_nama_pt" style="font-family: 'Nunito-Bold';font-weight: 500;">P.T. Bank Bumi Arta Tbk.</b><br>
				<span>Jl. Wahid Hasyim No. 234</span><br>
				<span>Jakarta Pusat 10250</span><br>
				<span>Indonesia</span><br><br>
				<span>Phone &nbsp;: (021) 2300893 / (021) 2300455</span><br>
				<span>Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: (021)2303623</span>
			</h6>
			<p>
				<?php 
				foreach($social_media as $row){
					$LINK = isset($row['LINK']) ? $row['LINK']:"";
					$URL_SOCIAL_MEDIA = isset($row['URL_SOCIAL_MEDIA']) ? $row['URL_SOCIAL_MEDIA']:"";
				?>
					<a href="<?php echo $URL_SOCIAL_MEDIA?>" style="text-decoration: none; cursor: pointer;">
						<img class="icon-socialize" src="<?php echo $LINK ?>">
					</a>
				<?php
				}
				?>
			</p>
		</div>
		<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 d-martop-10" align="center">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-martop-10 row-otoritas" style="padding-left: 0; padding-right: 0;">
					<img src="<?php echo base_url('assets/image/logo-otoritas-new.png')?>" style="width: 85%; height: auto;">
				</div>
			</div>
			<!-- <div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-martop-10" style="text-align: right;">
					<img class="img-fluid img-logo-referal" src="<?php echo base_url('assets/image/ojk.png')?>">
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-martop-10" style="text-align: left;">
					Bank Bumi Arta terdaftar dan diawasi oleh OJK
				</div>
			</div>
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-martop-10" style="text-align: right;">
					<img class="img-fluid img-logo-referal" src="<?php echo base_url('assets/image/lps.png')?>">
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-martop-10" style="text-align: left;">
					Bank Bumi Arta merupakan peserta penjaminan LPS
				</div>
			</div> -->
			<!-- <div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-martop-10" style="text-align: right;">
					<img class="img-fluid img-logo-referal" src="<?php echo base_url('assets/image/bank-sahabat-konsumen.png')?>">
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-martop-10" style="text-align: left;">
					<img class="img-fluid img-logo-referal" src="<?php echo base_url('assets/image/ayo-ke-bank.png')?>">
				</div>
			</div> -->
		</div>
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 d-martop-10">
			<b class="t-default" style="line-height: 3">Download Mobile Banking</b><br>
			<a href="https://bit.ly/2K1XMOw">
				<img class="img-fluid img-footer-bisnis " src="<?php echo base_url('assets/image/googleplay.png')?>">
			</a>
			&nbsp;
			<a href="https://apple.co/2rs80kU">
				<img class="img-fluid img-footer-bisnis" src="<?php echo base_url('assets/image/appsstore.png')?>">
			</a>
			<br><br>
			<div style="text-align: left; color: black;">
				<span style="line-height: 3;" class="t-default"><b style="font-family: 'Nunito-Bold';font-weight: 500;">Layanan 24 Jam</b></span><br>
				<span>
					<img src="<?php echo base_url('assets/image/atm-machine.png') ?>" style="width: 17px; height: 17px;">
					&nbsp;<b>ATM</b>
				</span><br>
				<span class="telp_atm" style="padding-left: 25px;">021 314 2121 (Jakarta)</span><br>
				<span class="telp_atm" style="padding-left: 25px;">0804 140 1221 (Luar Jakarta)</span><br><br>
				<span>
					<img src="<?php echo base_url('assets/image/briefcase.png') ?>" style="width: 17px; height: 17px;">
					&nbsp;<b><a href="https://ebanking.bankbba.co.id" style="color: black;">Internet Banking</a></b>
				</span><br><br>
				<span>
					<img src="<?php echo base_url('assets/image/user.png') ?>" style="width: 17px; height: 17px;">
					&nbsp;<b><a href="https://epersonal.bankbba.co.id" style="color: black;">e-Personal BBA</a></b>
				</span><br><br>
			</div>
		</div>
	</div>
	<div class="row row-footer-module d-padtop-10">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<?php 
			$LanguageActive = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
			$i = 0;
			$max = sizeof($nav_menu_footer)-1;
			foreach($nav_menu_footer as $row){
				$NAME = isset($row['NAME']) ? $row['NAME']:"";
				$LINK = isset($row['LINK']) ? $row['LINK']:"";
				$PARENT = isset($row['PARENT']) ? $row['PARENT']:"";
				$Url = base_url().$LanguageActive."/".$LINK;
			?>
				<a style="color: #707070;" href="<?php echo $Url ?>"><?php echo $NAME ?></a>
				<?php 
				if($i != $max){
				?>
					<span class="seperator"></span>
			<?php	
				}
			$i++;
			}
			?>
		</div>
	</div>
</div>