    <link rel="stylesheet" href="<?php echo base_url() ?>assets/lib/jstree/themes/proton/style.css" />
    <link rel="stylesheet" href="assets/lib/jstree/libs/bootstrap/css/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
    <script src="<?php echo base_url() ?>assets/lib/jstree/jstree.min.js"></script>
    <?php
$is_create = $authorize['is_create'];
$data = isset($data) ? $data : "";
$back = isset($data['back']) ? $data['back'] : base_url();
$menu = isset($data['menu']) ? $data['menu'] : array();

$folders_arr = array();
foreach ($menu as $row) {
    $parentid = $row['MENU_PARENT'];
    $type = 'child';
    if ($parentid == '0' || $parentid == null || $parentid == '') {
        $parentid = '#';
        $type = 'default';
    }

    $selected = false;
    $opened = false;
// if ($row['ID'] == 56) {
    // $selected = true;
    // $opened = true;
    // }
    $folders_arr[] = array(
        "id" => $row['ID'],
        "parent" => $parentid,
        "type" => $type,
        "text" => $row['NAME_ID'] . '/' . $row['NAME_EN'],
        "state" => array("selected" => $selected, "opened" => $opened),
        "data" => $row['MENU_LEVEL'],
    );
}
?>
    <!-- page headder -->
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="#" class="breadcrumb-item">Master</a>
                    <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Menu</a>
                    <a href="#" class="breadcrumb-item active">Add</a>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <!-- Content area -->
    <div class="content">
        <?php if ($is_create): ?>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Add Menu</h5>
            </div>
            <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveMenu">

                <div class="card-body">

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                Menu Name in Indonesian<span style="color:orange">*</span>
                            </label>
                            <input type="text" name="name_id" class="form-control" id="name_id" placeholder=""
                                data-parsley-trigger="focusin focusout" required="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                Menu Name in English<span style="color:orange">*</span>
                            </label>
                            <input type="text" name="name_en" class="form-control" id="name_en" placeholder=""
                                required="" data-parsley-trigger="focusin focusout">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>
                                Link Indonesian<span style="color:orange">*</span>
                            </label>
                            <input type="text" name="link_id" class="form-control" id="link_id" placeholder=""
                                required="" data-parsley-trigger="focusin focusout" data-parsley-length="[3, 100]"
                                data-parsley-pattern="^[a-zA-Z0-9_-]*$" onkeyup="validatelink(this);">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>
                                Link English<span style="color:orange">*</span>
                            </label>
                            <input type="text" name="link_en" class="form-control" id="link_en" placeholder=""
                                required="" data-parsley-trigger="focusin focusout" data-parsley-length="[3, 100]"
                                data-parsley-pattern="^[a-zA-Z0-9_-]*$" onkeyup="validatelink(this);">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="is_content"
                                        id="is_content">
                                    MENU CONTENT
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <label>Position<span style="color:orange">*</span></label>
                            <select id="position" class="form-control select2" name="position" required>
                                <option selected disabled>Choose Position</option>
                                <option value="1">HEADER</option>
                                <option value="2">FOOTER</option>
                                <option value="3">FOOTER & HEADER</option>

                            </select>
                        </div>
                    </div>
                    <div class="row" style="display:none;" id="sequence_headerdiv">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                Sequence for Header<span style="color:orange">*</span>
                            </label>
                            <input type="text" name="sequence_header" class="form-control" id="sequence_header"
                                onkeyup="validate(this);" data-parsley-minlength="1" data-parsley-maxlength="2"
                                placeholder="" required="">
                        </div>
                    </div>
                    <div class="row" style="display:none;" id="sequence_footerdiv">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                Sequence for Footer<span style="color:orange">*</span>
                            </label>
                            <input type="text" name="sequence_footer" class="form-control" id="sequence_footer"
                                onkeyup="validate(this);" data-parsley-minlength="1" data-parsley-maxlength="2"
                                placeholder="" required="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                Footer Note Indonesian
                            </label>
                            <textarea type="text" name="footer_note_id" class="form-control" id="footer_note_id"
                                rows="4"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                Footer Note English
                            </label>
                            <textarea type="text" name="footer_note_en" class="form-control" id="footer_note_en"
                                rows="4"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <label>Menu Level<span style="color:orange">*</span></label>
                            <select id="parent" class="form-control select2" name="parent" required>
                                <option selected disabled>Choose Menu</option>
                                <option value="menu_parent">MENU PARENT</option>
                                <option value="menu_child">MENU CHILD</option>

                            </select>
                        </div>
                    </div>

                    <div class="row" style="display:none;" id="menuParent">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                CHOOSE MENU PARENT
                            </label>
                            <div id="folder_jstree"></div>
                            <input type="hidden" id="menu_parent" name="menu_parent" />
                            <input type="hidden" id="menu_level" name="menu_level" />
                        </div>
                        <textarea id='txt_folderjsondata'
                            style="display:none;"><?=json_encode($folders_arr)?></textarea>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="1" name="header_banner"
                                        id="header_banner">
                                    UPLOAD HEADER BANNER
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="upload" style="display:none;">
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Upload Header Banner Indonesia</label><span style="color: orange">*</span><br>
                                <input type="file" id="file_id" name="file_id" class="dropify" />
                                <a href="#" onclick="openTnC('<?php echo base_url('menu/tncupload') ?>')">Terms and
                                    Conditions Upload</a>

                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Upload Header Banner English</label><span style="color: orange">*</span><br>
                                <input type="file" id="file_en" name="file_en" class="dropify" />
                                <a href="#" onclick="openTnC('<?php echo base_url('menu/tncupload') ?>')">Terms and
                                    Conditions Upload</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="progress" style="display:none">
                                <div id="progressBar" class="progress-bar progress-bar-striped active"
                                    role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 0%">
                                    <span class="sr-only">0%</span>
                                </div>
                            </div>
                            <div class="msg alert alert-info text-left" style="display:none"></div>
                        </div>
                    </div>
                </div>
                <div id="jstree-proton-3" style="margin-top:20px;" class="proton-demo"></div>
                <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0 float-right">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i
                                class="icon-paperplane ml-2"></i></button>
                        <a href="<?php echo $back ?>"><button type="button"
                                class="btn btn-secondary waves-effect waves-light">
                                Cancel</button></a>
                    </div>
                </div>
            </form>

        </div>
        <?php endif;?>
    </div>
    <script type="text/javascript">
$(function() {
    $('.dropify').dropify();

    var folder_jsondata = JSON.parse($('#txt_folderjsondata').val());

    $('#folder_jstree').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            },
            'data': folder_jsondata,
            'multiple': false,

        },
        'checkbox': {
            'three_state': false,
        },
        'plugins': ['checkbox', 'wholerow'],
        // 'types': {
        //     'default': {
        //         'icon': 'glyphicon glyphicon-leaf'
        //     },
        //     'child': {
        //         'icon': 'glyphicon glyphicon-leaf'
        //     }
        // },
    });

    $("#folder_jstree").on(
        "select_node.jstree",
        function(evt, data) {
            $menu_parent = data.node.id;
            $menu_level = data.node.data;

            $('#menu_parent').val($menu_parent);
            $('#menu_level').val($menu_level);
        }
    );

    $('[name="parent"]').change(function() {
        var menu = $(this).val();
        if (menu == 'menu_child') {
            $('#menuParent').show();

        } else {
            $('#menuParent').hide();

        }
    });
    $('#position').change(function() {
        var position = $(this).val();
        if (position == '1') {
            document.getElementById("sequence_header").required = true;
            document.getElementById("sequence_footer").required = false;
            $('#sequence_footer').val('');
            $('#sequence_headerdiv').show();
            $('#sequence_footerdiv').hide();
        } else if (position == '2') {
            document.getElementById("sequence_header").required = false;
            document.getElementById("sequence_footer").required = true;
            $('#sequence_header').val('');
            $('#sequence_headerdiv').hide();
            $('#sequence_footerdiv').show();
        } else if (position == '3') {
            document.getElementById("sequence_header").required = true;
            document.getElementById("sequence_footer").required = true;
            $('#sequence_header').val('');
            $('#sequence_footer').val('');
            $('#sequence_headerdiv').show();
            $('#sequence_footerdiv').show();

        }
    });

    $("#header_banner").change(function() {
        if (this.checked) {
            $("#upload").show();
            $("#file_id").prop("required", true);
            $("#file_en").prop("required", true);
        } else {
            $("#upload").hide();
            $("#file_id").prop("required", false);
            $("#file_en").prop("required", false);

        }
    });


    $(".select2").select2();
    CKEDITOR.editorConfig = function(config) {
        config.line_height = "1em;1.1em;1.2em;1.3em;1.4em;1.5em";
    };
    CKEDITOR.plugins.addExternal('videoembed',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/videoembed/plugin.js', '');
    CKEDITOR.plugins.addExternal('html5video',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/html5video/plugin.js', '');
    CKEDITOR.plugins.addExternal('lineheight',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/lineheight/plugin.js', '');
    CKEDITOR.plugins.addExternal('spicingsliders',
        '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/spacingsliders/plugin.js', '');

    CKEDITOR.replace('footer_note_id', {
        extraPlugins: 'videoembed, html5video, lineheight,spacingsliders',
    });

    CKEDITOR.replace('footer_note_en', {
        extraPlugins: 'videoembed, html5video, lineheight, spacingsliders'
    });




    CKEDITOR.on('instanceReady', function() {
        $.each(CKEDITOR.instances, function(instance) {
            CKEDITOR.instances[instance].on("change", function(e) {
                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();
            });
        });
    });
    $('#frmsaveMenu').on('submit', function(e) {
        e.preventDefault();
        $(".progress").show();
        $(".msg").hide();

        if ($(this).parsley().isValid()) {
            var form = $(this)[0];
            var formData = new FormData(form);
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(e) {
                        if (e.lengthComputable) {
                            var percent = Math.round((e.loaded / e.total) * 100);

                            $("#progressBar")
                                .attr("aria-valuenow", percent)
                                .css("width", percent + "%")
                                .text(percent + "%");
                        }
                    });
                    return xhr;
                },
                url: "<?php echo base_url('menu/add'); ?>",
                data: formData,
                dataType: "JSON",
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            var uri = "<?php echo $back; ?>";
                            window.location.href = uri;
                        }, 1500);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire(textStatus).then(function() {
                        Swal.fire({
                            type: "error",
                            title: textStatus,
                        });
                    });
                },
                beforeSend: function() {
                    $('#loadingBox').show();
                },
                complete: function() {
                    $('#loadingBox').fadeOut();
                }
            });
        } else {
            ;
        }
    });

});
    </script>