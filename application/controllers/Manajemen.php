<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manajemen extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{
		$getMenuLeft = $this->getMenuLeft();
		
		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "suku_bunga";
		$this->data['data'] = array();

		// $this->data['contentData'] = array(
		// 	'menuLeft' => $getMenuLeft,
		// );

		// echo "<pre>";
		// print_r($provideDataDeposito);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function struktur_organisasi()
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$getMenuLeft = $this->getMenuLeft();

		$GetModule = $this->Module_model->getmodule($linkingg);
		$ID_MENU = isset($GetModule['ID']) ? $GetModule['ID']:"";

		$getContent = $this->Content_model->getcontent($ID_MENU);

		// echo "<pre>";
		// print_r($getContent);
		// echo "</pre>";exit;

		$getMenuDetail = $this->db->query("SELECT * FROM MS_MENU WHERE LINK_ID = '".$linkingg."' OR LINK_EN = '".$linkingg."'")->row_array();
		$getMenuDetail['FOOTER_NOTE'] = $getMenuDetail[$this->cekLang('FOOTER_NOTE')];

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "struktur_organisasi";
		$this->data['data'] = array();
		$this->data['contentData'] = array('menuLeft' => $getMenuLeft, 'getContent' => $getContent, 'menuDetail' => $getMenuDetail);
        $this->data['navbar_link'] = $this->getnavbarlink();

		$this->load->view('template', $this->data);
	}

	public function dewan_komisaris()
	{	
		$provideData = $this->provideData(1);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_view";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);
        $this->data['navbar_link'] = $this->getnavbarlink();

  //       echo "<pre>";
		// print_r($provideData);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function dewan_direksi()
	{	
		$provideData = $this->provideData(2);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_view";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);

		$this->load->view('template', $this->data);
	}

	public function sekretaris_perusahaan()
	{	
		$provideData = $this->provideData(3);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_view";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);

		$this->load->view('template', $this->data);
	}

	public function komite_audit()
	{	
		$provideData = $this->provideData(4);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_view";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);

		$this->load->view('template', $this->data);
	}

	public function pemantau_resiko()
	{	
		$provideData = $this->provideData(5);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_view";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);

		$this->load->view('template', $this->data);
	}

	public function komite_remunerasi()
	{
		$provideData = $this->provideData(6);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_view";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);

		$this->load->view('template', $this->data);
	}

	public function detail($link){
		$rows = $this->db->query("
					SELECT * FROM MS_MANAGEMENT_HEADER
					WHERE STATUS = '99'
				")->result();

        $id = $this->getIdFromSlug($link, 'NAME', $rows);

        $provideData = $this->provideDetail($id);
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "manajemen_detail";
		$this->data['data'] = array();
		$this->data['contentData'] = array('provideData' => $provideData, 'menuLeft' => $getMenuLeft);	
		$this->data['navbar_link'] = $this->getnavbarlink();

		// echo "<pre>";
		// print_r($provideData);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function provideDetail($id)
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";
		
		$getHeader = $this->db->query("
			SELECT * FROM MS_MANAGEMENT_HEADER
			WHERE STATUS = '99' AND ID_MANAGEMENT_HEADER = ".$id."
		")->row_array();

		if (!empty($getHeader['ID_MANAGEMENT_HEADER'])) {
			$getHeader['POSITION'] = $getHeader[$this->cekLang('POSITION')];
			$getHeader['DESCRIPTION'] = $getHeader[$this->cekLang('DESCRIPTION')];

			$getDetail = $this->db->query("
				SELECT * FROM MS_MANAGEMENT_DETAIL
				WHERE ID_MANAGEMENT_HEADER = ".$getHeader['ID_MANAGEMENT_HEADER']
			)->result_array();

			foreach ($getDetail as $key2 => $value2) {
				$getDetail[$key2]['DESCRIPTION'] = $value2[$this->cekLang('DESCRIPTION')];
			}
		}else{
			$getDetail = array();
		}

		$getMenuDetail = $this->db->query("SELECT * FROM MS_MENU WHERE LINK_ID = '".$linkingg."' OR LINK_EN = '".$linkingg."'")->row_array();
		$getMenuDetail['FOOTER_NOTE'] = $getMenuDetail[$this->cekLang('FOOTER_NOTE')];
		$getMenuDetail['FILE_BANNER'] = $getMenuDetail[$this->cekLang('FILE_BANNER')];

		return array('HEADER' => $getHeader, 'DETAIL' => $getDetail, 'menuDetail' => $getMenuDetail);
	}

	public function provideData($id_categ_act)
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$conf['POSITION_ID'] = "Board of Commisioners:Board of Directors:Corporate Secretary:Audit Commitee:Risk Monitoring Commitee:Remuneration and Nomination Committee";
		$conf['POSITION_EN'] = "Dewan Komisaris:Dewan Direksi:Sekertaris Perusahaan:Komite Audit:Komite Pemantau Resiko:Komite Remunerasi dan Nominasi";
		$conf['ID_CATEG_POSITION'] = "1:2:3:4:5:6";

		$exp = explode(':', $conf[$this->cekLang('POSITION')]);
		$expIdCateg = explode(':', $conf['ID_CATEG_POSITION']);

		$data = array();
		foreach ($expIdCateg as $key => $value) {

			if ($value == $id_categ_act) {
				$getHeader = $this->db->query("
					SELECT * FROM MS_MANAGEMENT_HEADER
					WHERE STATUS = '99' AND POSITION_CATEGORY = ".$value."
				")->result_array();

				if (count($getHeader) > 0) {

					foreach ($getHeader as $key => $valH) {

						$getHeader[$key]['POSITION'] = $getHeader[$key][$this->cekLang('POSITION')];
						$getHeader[$key]['DESCRIPTION'] = $getHeader[$key][$this->cekLang('DESCRIPTION')];

						$getDetail = $this->db->query("
							SELECT * FROM MS_MANAGEMENT_DETAIL
							WHERE ID_MANAGEMENT_HEADER = ".$getHeader[$key]['ID_MANAGEMENT_HEADER']
						)->result_array();

						foreach ($getDetail as $key2 => $value2) {
							$getDetail[$key2]['DESCRIPTION'] = $value2[$this->cekLang('DESCRIPTION')];
						}

						$getHeader[$key]['DETAIL'] = $getDetail;

					}
				}else{
					$getDetail = array();
				}

				$data['ID_CATEG_POSITION'] = $value;
				$data['HEADER'] = $getHeader;
				// $data[$key]['DETAIL'] = $getDetail;
				$data['POSITION_NAME'] = $exp[$key];
				
				break;
			}

		}

		$getMenuDetail = $this->db->query("SELECT * FROM MS_MENU WHERE LINK_ID = '".$linkingg."' OR LINK_EN = '".$linkingg."'")->row_array();
		$getMenuDetail['FOOTER_NOTE'] = $getMenuDetail[$this->cekLang('FOOTER_NOTE')];
		$getMenuDetail['FILE_BANNER'] = $getMenuDetail[$this->cekLang('FILE_BANNER')];

		return array('data' => $data, 'menuDetail' => $getMenuDetail);
	}

	public function getnavbarlink(){
        $JSON = array();
        $link = $this->uri->segment(2);
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";

        $GetModule = $this->Module_model->getmodule($link);
        $PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
        $MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";
        $IS_CONTENT = isset($GetModule['IS_CONTENT']) ? $GetModule['IS_CONTENT']:"";

        if(sizeof($GetModule) > 0){
            $ID_MENU = isset($GetModule['ID']) ? $GetModule['ID']:"";

            $MENU_PARENT = isset($GetModule['PARENT']['NAME']) ? $GetModule['PARENT']['NAME']:"";
            $MENU_PARENT_LINK = isset($GetModule['PARENT']['LINK']) ? $GetModule['PARENT']['LINK']:"";
            $MENU_CHILD = isset($GetModule['NAME']) ? $GetModule['NAME']:"";
            $MENU_CHILD_LINK = isset($GetModule['LINK']) ? $GetModule['LINK']:"";

            $JSON['MENU_PARENT'] = $MENU_PARENT;
            $JSON['MENU_PARENT_LINK'] = $MENU_PARENT_LINK;
            $JSON['MENU_CHILD'] = $MENU_CHILD;
            $JSON['MENU_CHILD_LINK'] = $MENU_CHILD_LINK;
            $JSON['IS_CONTENT'] = $IS_CONTENT;
            $JSON['FILE_CONTENT'] = $link;
        }

        return $JSON;
    }

	public function getMenuLeft($value='')
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$GetModule = $this->Module_model->getmodule($linkingg);
		$PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
		$MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";

		// MENU PARENT -1
		$Q_MP = $this->db->query("SELECT * FROM MS_MENU WHERE ID = '$MENU_PARENT'")->row_array();
		if ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) {
			// $MENU_PARENT = ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) ? $Q_MP['MENU_PARENT'] : "";
			$MENU_PARENT = $Q_MP['MENU_PARENT'];
		}

		$ID_MENU_PARENT = $MENU_PARENT;
		
		$getMenuLeft = array();
		$getMenu1 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->result_array();

		foreach ($getMenu1 as $key => $value) {
			$getMenuLeft[$key]['menu_1']['NAME'] = $value[$this->cekLang('NAME')];
			$getMenuLeft[$key]['menu_1']['LINK'] = $value[$this->cekLang('LINK')];

			// cek aktif ga menu 1 single
			$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = ( $value[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

			$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value['ID'])->result_array();

			if (count($getMenu2) > 0) {
				// deklarasi aktif ga menu 1 multiple
				$stakMenu2 = array();
				$linkMenu1 = $value[$this->cekLang('LINK')];

				foreach ($getMenu2 as $key2 => $value2) {
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['NAME'] = $value2[$this->cekLang('NAME')];
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['LINK'] = $value2[$this->cekLang('LINK')];

					// cek aktif ga menu single
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = ( $value2[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

					$getMenu3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value2['ID'])->result_array();

					if (count($getMenu3) > 0) {
						// deklarasi aktif ga menu 2 multiple
						$stakMenu3 = array();
						$linkMenu2 = $value2[$this->cekLang('LINK')];

						foreach ($getMenu3 as $key3 => $value3) {
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['NAME'] =
							 	$value3[$this->cekLang('NAME')];
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['LINK'] =
							 	$value3[$this->cekLang('LINK')];

							 $getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['ACTIVE_URL'] = ( $value3[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

							$stakMenu3[] = $value3[$this->cekLang('LINK')];
						}

						// cek aktif ga menu 2 multiple
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = (in_array($linkingg, $stakMenu3)) ? TRUE : FALSE;

					}else{
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'] = array();
					}

					$stakMenu2[] = $value2[$this->cekLang('LINK')];
				}

				// cek aktif ga menu 1 multiple
				$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = (in_array($linkingg, $stakMenu2)) ? TRUE : FALSE;

			}else{
				$getMenuLeft[$key]['menu_1']['menu_2'] = array();
			}
		}

		return $getMenuLeft;
	}

	function cekLang($name)
	{
		if ($_SESSION['lang'] == 'id') {
			$nameLang = $name.'_ID';
		}else{
			$nameLang = $name.'_EN';
		}

		return $nameLang;
	}

	function getIdFromSlug($link, $col, $rows){
	    $fIndex = NULL;

	    foreach ($rows as $key => $value) {
	        if ($this->slugify($value->$col) == $link) {
	            $fIndex = $key;
	            break;
	        }
	    }

	    if(isset($fIndex))
	        return $rows[$fIndex]->ID_MANAGEMENT_HEADER;
	    else
	        return FALSE;
	}

	function slugify($str, $replace=array('rsquo'), $delimiter='-')
	{
	    setlocale(LC_ALL, 'en_US.UTF8');

	    if( !empty($replace) )
	    {
	        $str = str_replace((array)$replace, ' ', $str);
	    }

	    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	    $clean = strtolower(trim($clean, '-'));
	    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	    return $clean;
	}

}
