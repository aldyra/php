<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login | Bank Bumi Arta</title>
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo-bumi-artha.png">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- Sweet Alert css -->
    <link href="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/joy-style.css">
    <!-- /global stylesheets -->
    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Sweet Alert -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->

    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <!-- /theme JS files -->

    <style type="text/css">
    .form-control.parsley-error {
        border-color: #ea3a3a;
    }

    ul.parsley-errors-list {
        margin: 5px 0 0;
        padding-left: 15px;
    }

    ul.parsley-errors-list li {
        color: #fd0d0d;
        list-style: none;
    }

    .swal2-icon.swal2-warning {
        font-size: 1rem;
        line-height: normal;
    }
    </style>
</head>

<body>
    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">
                <!-- Login form -->
                <form id="auth_form" class="login-form" method="POST"
                    action="<?php echo base_url('Authentication/doLogin'); ?>">
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <img class="logo-login"
                                    src="<?php echo base_url('assets/images/logo-bumi-artha.png') ?>" alt="">
                                <h5 class="mb-0">Login to your account</h5>
                                <span class="d-block text-muted">Enter your credentials below</span>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control" name="uname" placeholder="Username" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" class="form-control" name="pass" placeholder="Password" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign in <i
                                        class="icon-circle-right2 ml-2"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /login form -->
            </div>
            <!-- /content area -->
            <!-- OPEN FOOTER -->
            <?php require_once APPPATH . 'views/templates/footer.php';?>
            <!-- END FOOTER -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

    <!-- Vendor js -->
    <script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>
    <!-- App js-->
    <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
    <!-- Validation js (Parsleyjs) -->
    <script src="<?php echo base_url(); ?>assets/libs/parsleyjs/parsley.min.js"></script>
    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.js"></script>
    <!-- Sweet alert init js-->
    <script src="<?php echo base_url(); ?>assets/js/pages/sweet-alerts.init.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#auth_form").parsley();
        $('#auth_form').on('submit', function(e) {
            e.preventDefault();
            if ($(this).parsley().isValid()) {
                $.ajax({
                    url: "<?php echo base_url('Authentication/dologin'); ?>",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    type: 'POST',
                    success: function(data) {
                        if (data == false) {
                            Swal.fire({
                                type: 'error',
                                title: 'Wrong Username or Password'
                            });
                        } else if (data[0].ID) {
                            window.location.href = "<?php echo base_url(); ?>";
                        } else if ((data.indexOf("Warning") != -1)) {
                            Swal.fire({
                                type: 'warning',
                                title: data
                            }).then(function() {
                                window.location.href =
                                    "<?php echo base_url('authentication/changePass') ?>"
                            });
                        } else if ((data.indexOf("Failed") != -1)) {
                            Swal.fire({
                                type: 'error',
                                title: data
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Swal.fire(textStatus);
                    }
                });
            }
        });
    });
    </script>
</body>

</html>