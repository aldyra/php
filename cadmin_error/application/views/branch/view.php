<?php
$Branch = isset($data['branch']) ? $data['branch'] : array();

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Master Branch</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">
            <a href="<?php echo base_url("branch/add_branch/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add Branch
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Name</th>
                    <th width="">Category</th>
                    <th width="">Address</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Branch as $branch) {
    $ID_BRANCH = isset($branch['ID_BRANCH']) ? $branch['ID_BRANCH'] : "";
    $BRANCH_NAME = isset($branch['BRANCH_NAME']) ? $branch['BRANCH_NAME'] : "";
    $CATEGORY = isset($branch['CATEGORY']) ? $branch['CATEGORY'] : "";
    $ADDRESS = isset($branch['ADDRESS']) ? $branch['ADDRESS'] : "";
    $LONGITUDE = isset($branch['LONGITUDE']) ? $branch['LONGITUDE'] : "";
    $LATITUDE = isset($branch['LATITUDE']) ? $branch['LATITUDE'] : "";
    $ID_PROVINCE = isset($branch['ID_PROVINCE']) ? $branch['ID_PROVINCE'] : "";
    $ID_CITY = isset($branch['ID_CITY']) ? $branch['ID_CITY'] : "";
    $ID_KECAMATAN = isset($branch['ID_KECAMATAN']) ? $branch['ID_KECAMATAN'] : "";
    $ID_KELURAHAN = isset($branch['ID_KELURAHAN']) ? $branch['ID_KELURAHAN'] : "";
    $POSTAL_CODE = isset($branch['POSTAL_CODE']) ? $branch['POSTAL_CODE'] : "";
    $PHONE_NUMBER = isset($branch['PHONE_NUMBER']) ? $branch['PHONE_NUMBER'] : "";
    $FAX_NUMBER = isset($branch['FAX_NUMBER']) ? $branch['FAX_NUMBER'] : "";
    $EMAIL = isset($branch['EMAIL']) ? $branch['EMAIL'] : "";
    $WEBSITE = isset($branch['WEBSITE']) ? $branch['WEBSITE'] : "";
    $STATUS = isset($branch['STATUS']) ? $branch['STATUS'] : "";
    $CREATED_BY = isset($branch['CREATED_BY']) ? $branch['CREATED_BY'] : "";
    $CREATED_DATE = isset($branch['CREATED_DATE']) ? date('d M Y', strtotime($branch['CREATED_DATE'])) : "";
    $USER_LOG = isset($branch['USER_LOG']) ? $branch['USER_LOG'] : "";
    $DATE_LOG = isset($branch['DATE_LOG']) ? date('d M Y', strtotime($branch['DATE_LOG'])) : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $branch;
    $enc = base64_encode(json_encode($paramaction));

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    if ($CATEGORY == "1") {
        $CATEG = "Kantor Pusat/Head Office";
    } elseif ($CATEGORY == "2") {
        $CATEG = "Kantor Cabang/Branch Office";
    } elseif ($CATEGORY == "3") {
        $CATEG = "Kantor Capem/Sub Branch Office";
    } elseif ($CATEGORY == "4") {
        $CATEG = "Kantor Kas/Cash Offices";
    } elseif ($CATEGORY == "5") {
        $CATEG = "Payment Point";
    } else {
        $CATEG = "";
    }

    ?>
                <tr>
                    <td>
                        <?php echo $BRANCH_NAME ?>
                    </td>
                    <td>
                        <?php echo $CATEG ?>
                    </td>
                    <td>
                        <?php echo $ADDRESS ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo base_url("branch/detail_branch/") . $ID_BRANCH ?>"
                                        class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("branch/edit_branch/") . $ID_BRANCH ?>"
                                        class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <?php if ($STATUS == "0") {?>
                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i> Activate</a>
                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i> Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>
                                    <?}elseif($STATUS == "99"){?>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i> Deactive</a>
                                    <?php }?>
                                    <?php }?>
                                    <?php if (!empty($is_delete)) {?>
                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?>

                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(4).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(7)
            .search(dis.value)
            .draw();
    } else {
        t.column(7)
            .search(dis.value)
            .draw();
    }

}
</script>