<!-- Theme JS files -->
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>
<!-- <script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script> -->
<!-- <script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script> -->
<!-- <script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script> -->
<!-- /theme JS files -->
<?php
$is_create = $authorize['is_create'];
$back = isset($data['back']) ? $data['back'] : '';

$sbdk = isset($data['sbdk']) ? $data['sbdk'] : '';
$ID_SBDK = isset($sbdk->ID_SBDK) ? $sbdk->ID_SBDK : "";
$EFFECTIVE_DATE = isset($sbdk->EFFECTIVE_DATE) ? $sbdk->EFFECTIVE_DATE : "";
$CORPORATE_LOAN = isset($sbdk->CORPORATE_LOAN) ? $sbdk->CORPORATE_LOAN : "";
$RETAIL_LOAN = isset($sbdk->RETAIL_LOAN) ? $sbdk->RETAIL_LOAN : "";
$MICRO_LOAN = isset($sbdk->MICRO_LOAN) ? $sbdk->MICRO_LOAN : "";
$MORTGAGE = isset($sbdk->MORTGAGE) ? $sbdk->MORTGAGE : "";
$NONMORTGAGE = isset($sbdk->NONMORTGAGE) ? $sbdk->NONMORTGAGE : "";

$str = $EFFECTIVE_DATE;
$arr = explode(" ", $str);

$EFFECTIVE_DATE = $arr[0];
$EFFECTIVE_TIME = $arr[1];

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master SBDK</a>
                <a class="breadcrumb-item active">Edit</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master SBDK</span> - Edit
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_create): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveSBDK">
            <div class="card-header bg-blue-800 d-flex justify-content-between">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Effective Date<span style="color:orange">*</span></label>

                        <div class="input-group" style="color:#333">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar5"></i></span>
                            </span>
                            <input type="text" name="effective_date" id="effective_date"
                                class="form-control pickadate-selectors" placeholder="Select Date.." required=""
                                data-value="<?php echo $EFFECTIVE_DATE ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Time<span style="color:orange">*</span></label>

                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-watch2"></i></span>
                        </span>
                        <input type="text" class="form-control" name="time" id="anytime-time"
                            value="<?php echo $EFFECTIVE_TIME ?>">
                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">SBDK Detail</h6>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="workhistory">
                        <thead>
                            <tr>
                                <th>Business Segment</th>
                                <th>Prime Lending Rate</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Corporate Loan/Kredit Korporasi</td>
                                <td>
                                    <input type="hidden" name="id_sbdk" id="id_sbdk" value="<?php echo $ID_SBDK ?>">
                                    <input type="text" class="form-control" name="corporate_loan" id="CORPORATE_LOAN"
                                        placeholder="Corporate Loan" required="" value="<?php echo $CORPORATE_LOAN ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Retail Loan/Kredit Retail</td>
                                <td>
                                    <input type="text" class="form-control" name="retail_loan" id="RETAIL_LOAN"
                                        placeholder="Retail Loan" required="" value="<?php echo $RETAIL_LOAN ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Micro Loan/Kredit Mikro</td>
                                <td>
                                    <input type="text" class="form-control" name="micro_loan" id="MICRO_LOAN"
                                        placeholder="Micro Loan" required="" value="<?php echo $MICRO_LOAN ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Mortgage/KPR</td>
                                <td>
                                    <input type="text" class="form-control" name="mortgage" id="MORTGAGE"
                                        placeholder="Mortgage" required="" value="<?php echo $MORTGAGE ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Non-Mortgage/Non-KPR</td>
                                <td>
                                    <input type="text" class="form-control" name="nonmortgage" id="NONMORTGAGE"
                                        placeholder="Non Mortgage" required="" value="<?php echo $NONMORTGAGE ?>">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Cancel</button></a>
                </div>
            </div>
        </form>
    </div>
    <?php endif;?>
</div>

<script type="text/javascript">
$(function() {
    $('.select2').select2();
    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });
    // Time picker
    $('#anytime-time').AnyTime_picker({
        format: '%H:%i:%s'
    });
});
var room = 0;

$('#frmsaveSBDK').on('submit', function(e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        $("#frmsaveSBDK").parsley().validate();
        var form = $("#frmsaveSBDK");
        var data = form.serialize();

        if ($("#frmsaveSBDK").parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('sbdk/update') ?>",
                type: "POST",
                data: data,
                cache: false,
                dataType: "json",
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            var uri = "<?php echo $back; ?>";
                            window.location.href = uri;
                        }, 1500);
                    }
                },
            });
        }
    } else {

    }
});
</script>