<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getcontent($param)
    {
        $row = array();
        $where['STATUS'] = "99";
        $where['ID_MENU'] = $param;
    	$Sitelang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $Getmodule = $this->db->where($where)->get('MS_CONTENT')->row_array();
        if($Sitelang == "id")
        {
            $row['ID'] = $Getmodule['ID_MENU'];
            $row['TITLE'] = $Getmodule['TITLE_ID'];
            $row['DESCRIPTION'] = $Getmodule['DESCRIPTION_ID'];
        }elseif($Sitelang == "en")
        {
            $row['ID'] = $Getmodule['ID_MENU'];
            $row['TITLE'] = $Getmodule['TITLE_EN'];
            $row['DESCRIPTION'] = $Getmodule['DESCRIPTION_EN'];
        }

    	return $row;
    }

    public function getcontenthighlight($param)
    {
        $row = array();
        $where['STATUS'] = "99";
        $where['ID'] = $param;
        $Sitelang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $Getmodule = $this->db->where($where)->get('MS_CONTENT')->row_array();
        if($Sitelang == "id")
        {
            $row['ID'] = $Getmodule['ID_MENU'];
            $row['TITLE'] = $Getmodule['TITLE_ID'];
            $row['DESCRIPTION'] = $Getmodule['DESCRIPTION_ID'];
        }elseif($Sitelang == "en")
        {
            $row['ID'] = $Getmodule['ID_MENU'];
            $row['TITLE'] = $Getmodule['TITLE_EN'];
            $row['DESCRIPTION'] = $Getmodule['DESCRIPTION_EN'];
        }

        return $row;
    }
}