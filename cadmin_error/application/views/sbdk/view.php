<?php
$SBDK = isset($Data['sbdk']) ? $Data['sbdk'] : array();

$is_create = $authorize['is_create'];
$is_read = $authorize['is_read'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Master SBDK</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">
            <a href="<?php echo base_url("sbdk/add_sbdk/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add SBDK
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th data-orderable="false" style="width:150px">Effective Date</th>
                    <th>Corporate Loan</th>
                    <th>Retail Loan</th>
                    <th>Micro Loan</th>
                    <th>Mortgage</th>
                    <th>Non-Mortgage</th>
                    <th data-orderable="false" style="width:150px">Approved Date</th>
                    <th>Approved By</th>
                    <th data-orderable="false">Status Approve</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($SBDK as $sbdk) {
    $ID_SBDK = isset($sbdk['ID_SBDK']) ? $sbdk['ID_SBDK'] : "";
    $DESCRIPTION = isset($sbdk['DESCRIPTION']) ? $sbdk['DESCRIPTION'] : "";
    $EFFECTIVE_DATE = isset($sbdk['EFFECTIVE_DATE']) ? date('d M Y', strtotime($sbdk['EFFECTIVE_DATE'])) : "";
    $CORPORATE_LOAN = isset($sbdk['CORPORATE_LOAN']) ? $sbdk['CORPORATE_LOAN'] : "";
    $RETAIL_LOAN = isset($sbdk['RETAIL_LOAN']) ? $sbdk['RETAIL_LOAN'] : "";
    $MICRO_LOAN = isset($sbdk['MICRO_LOAN']) ? $sbdk['MICRO_LOAN'] : "";
    $MORTGAGE = isset($sbdk['MORTGAGE']) ? $sbdk['MORTGAGE'] : "";
    $NONMORTGAGE = isset($sbdk['NONMORTGAGE']) ? $sbdk['NONMORTGAGE'] : "";
    $STATUS = isset($sbdk['STATUS']) ? $sbdk['STATUS'] : "";
    $CREATED_BY = isset($sbdk['CREATED_BY']) ? $sbdk['CREATED_BY'] : "";
    $CREATED_DATE = isset($sbdk['CREATED_DATE']) ? date('d M Y', strtotime($sbdk['CREATED_DATE'])) : "";
    $USER_LOG = isset($sbdk['USER_LOG']) ? $sbdk['USER_LOG'] : "";
    $DATE_LOG = isset($sbdk['DATE_LOG']) ? date('d M Y', strtotime($sbdk['DATE_LOG'])) : "";
    $APPROVED_DATE = isset($sbdk['APPROVED_DATE']) ? date('d M Y', strtotime($sbdk['APPROVED_DATE'])) : "";
    $APPROVED_BY = isset($sbdk['APPROVED_BY']) ? $sbdk['APPROVED_BY'] : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $sbdk;
    $enc = base64_encode(json_encode($paramaction));

    if ($STATUS == "88") {
        $STS = "<span class='badge badge-danger'>Not Approved</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Approved</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <?php echo $EFFECTIVE_DATE ?>
                    </td>
                    <td>
                        <?php echo $CORPORATE_LOAN ?>
                    </td>
                    <td>
                        <?php echo $RETAIL_LOAN ?>
                    </td>
                    <td>
                        <?php echo $MICRO_LOAN ?>
                    </td>
                    <td>
                        <?php echo $MORTGAGE ?>
                    </td>
                    <td>
                        <?php echo $NONMORTGAGE ?>
                    </td>
                    <td>
                        <?php echo $APPROVED_DATE ?>
                    </td>
                    <td>
                        <?php echo $APPROVED_BY ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_read)) {?>
                                    <a href="<?php echo base_url("sbdk/detail_sbdk/") . $ID_SBDK ?>"
                                        class="dropdown-item">
                                        <i class="icon-eye"></i> Detail
                                    </a>
                                    <?php }?>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("sbdk/edit_sbdk/") . $ID_SBDK ?>"
                                        class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <?php $approval = $this->config->item("approval_sbdk");if (in_array($_SESSION['level'], $approval)) {?>
                                    <?php if ($STATUS == "88") {?>
                                    <a onclick="approvedaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-check"></i> Approved</a>
                                    <?php }?>
                                    <?php }}?>


                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(9).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(9)
            .search(dis.value)
            .draw();
    } else {
        t.column(9)
            .search(dis.value)
            .draw();
    }

}

function changeSelectApprovedDate(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectEffectiveDate(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(0)
            .search(dis.value)
            .draw();
    } else {
        t.column(0)
            .search(dis.value)
            .draw();
    }

}
//Open Change Status active
function approvedaction(param) {
    var decrypt = JSON.parse(atob(param));
    var Url = decrypt.Url;
    Swal.fire({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, Approved!",
        cancelButtonText: "No, cancel!",
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: Url + "/publish",
                dataType: "JSON",
                data: {
                    Data: decrypt.Data,
                },
                type: "POST",
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 1500);
                    }
                },
            });
        }
    });
}
//END Change Status active
</script>