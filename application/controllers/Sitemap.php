<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{
		$this->data['cssName'] = "Css/public.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "sitemap";
		$this->data['data'] = array();

		// echo "<pre>";
		// print_r($this->data['nav_menu']);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}
}
