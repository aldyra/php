<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>

<style>
	.content-suku-bunga{
		max-width: 95%;
	}
	.content-suku-bunga li.nav-item a.nav-link {
	    font-size: 16px;
	    color: #999999 !important;
	    background-color: transparent;
	    border-radius: 20px;
	    padding: 10px 0;
	    min-width: 115px;
	    text-align: center;
	    border: 1px solid #999999;
	    margin-right: 10px;
	}
	.content-suku-bunga .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
	    color: #fff !important;
	    background-color: #243E8B;
	    border-radius: 20px;
	    padding: 10px 0;
	    min-width: 115px;
	    text-align: center;
	}
	.last-update{
		font-size: 17px;
	}

	table.tbl-detail-item{
		width: 100%;
	}
	.tbl-detail-item th, .tbl-detail-item td{
		font-size: 14px;
		padding: 8px 0;
		width: 50%;
		border: 1px solid #999999;
	}
	.tbl-detail-item.deposito th, .tbl-detail-item.deposito td{
		width: auto;
	}
	.tbl-detail-item th{
		text-align: center;
		background: #243E8B;
		color: #d9d9d9;
		font-weight: normal;
	}
	.tbl-detail-item td{
		text-align: center;
		color: #3a3a3a;
	}

	.item-suku h5{
		font-size: 16px !important;
	}

	.title-last-update{
		font-size: 14px !important;
	}

	.desc-last-update{
		font-size: 14px !important;
	}

	.row-socialize{
		padding-bottom: 30px;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-suku-bunga {
		    max-width: 100%;
		    margin-top: 1.5rem;
		}
		.content-suku-bunga > h3{
			/*display: none; */
			font-size: 20px;
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}

		.item-suku h5{
			text-align: center;
			font-size: 16px !important;
		}
		.tbl-detail-item tr th, .tbl-detail-item tr td {
			font-size: 14px;
		}
		.content-suku-bunga .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
		    color: #fff !important;
		    background-color: #243E8B;
		    border-radius: 20px;
		    padding: 5px 0;
		    min-width: 105px;
		    text-align: center;
		}
		.content-suku-bunga li.nav-item a.nav-link {
		    font-size: 15px;
		    color: #999999 !important;
		    background-color: transparent;
		    border-radius: 20px;
		    padding: 5px 0;
		    min-width: 105px;
		    text-align: center;
		    border: 1px solid #999999;
		    margin-right: 5px;
		}
	}
</style>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color: #707070;"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"> -->
			<label style="color: #707070;"><?php echo $MENU_PARENT?></label> /
		<!-- </a> / -->
		<?php } ?> 
		<label style="color: #707070;"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
            <?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-8">
			<div class="content-suku-bunga">
				<h3 class="mb-4" style="font-size: 18px;"><strong><?php echo $this->lang->line('sbdk-2') ?></strong></h3>

				<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				  <li class="nav-item" style="padding-top: 10px;">
				    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-tabungan" role="tab" aria-controls="pills-home" aria-selected="true"><?php echo $this->lang->line('tabungan') ?></a>
				  </li>
				  <li class="nav-item" style="padding-top: 10px;">
				    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-giro" role="tab" aria-controls="pills-profile" aria-selected="false"><?php echo $this->lang->line('giro') ?></a>
				  </li>
				  <li class="nav-item" style="padding-top: 10px;">
				    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-deposito" role="tab" aria-controls="pills-contact" aria-selected="false"><?php echo $this->lang->line('deposito') ?></a>
				  </li>
				</ul>
				<div class="tab-content" id="pills-tabContent">
				  <div class="tab-pane fade show active" id="pills-tabungan" role="tabpanel" aria-labelledby="pills-home-tab">
				  	<div class="last-update mb-3">
				  		<p class="mb-1 title-last-update">Last Updated : <strong><?php echo convertDateLang($contentData['lastUpdateSaving'], 'dmy') ?></strong></p>
				  		<p class="mb-0 desc-last-update"><?php echo $this->lang->line('suku_bunga_ket') ?></p>
				  	</div>

				  	<div class="box-suku-bunga">
				  		<?php foreach($contentData['dataSaving'] as $key => $val){ ?>
				  		<div class="item-suku mb-4">
				  			<h5 class="mb-3"><strong><?php echo $val['SAVING_NAME']; ?></strong></h5>
				  			<table class="tbl-detail-item">
				  				<tr>
				  					<th><?php echo $this->lang->line('suku_bunga_1') ?></th>
				  					<th><?php echo $this->lang->line('suku_bunga_2') ?></th>
				  				</tr>
				  				<?php foreach($val['DETAIL'] as $key2 => $val2){ ?>
				  				<tr>
				  					<td><?php echo $val2['BALANCE'] ?></td>
				  					<td><?php echo $val2['RATE'] ?></td>
				  				</tr>
				  				<?php } ?>
				  			</table>
				  		</div>
				  		<?php } ?>
				  	</div>
				  </div>
				  <div class="tab-pane fade" id="pills-giro" role="tabpanel" aria-labelledby="pills-profile-tab">
				  	<div class="last-update mb-3">
				  		<p class="mb-1 title-last-update">Last Updated : <strong><?php echo convertDateLang($contentData['lastUpdateGiro'], 'dmy'); ?></strong></p>
				  		<p class="mb-0 desc-last-update"><?php echo $this->lang->line('suku_bunga_ket') ?></p>
				  	</div>

				  	<div class="box-suku-bunga">
				  		<?php foreach($contentData['dataGiro'] as $key => $val){ ?>
				  		<div class="item-suku mb-4">
				  			<h5 class="mb-3"><strong><?php echo $val['NAME']; ?></strong></h5>
				  			<table class="tbl-detail-item">
				  				<tr>
				  					<th><?php echo $this->lang->line('suku_bunga_1') ?></th>
				  					<th><?php echo $this->lang->line('suku_bunga_2') ?></th>
				  				</tr>
				  				<?php foreach($val['DETAIL'] as $key2 => $val2){ ?>
				  				<tr>
				  					<td><?php echo $val2['BALANCE'] ?></td>
				  					<td><?php echo $val2['RATE'] ?></td>
				  				</tr>
				  				<?php } ?>
				  			</table>
				  		</div>
				  		<?php } ?>
				  	</div>
				  </div>
				  <div class="tab-pane fade" id="pills-deposito" role="tabpanel" aria-labelledby="pills-deposito-tab">
				  	<div class="last-update mb-3">
				  		<p class="mb-1 title-last-update">Last Updated : <strong><?php echo convertDateLang($contentData['lastUpdateDeposito'], 'dmy') ?></strong></p>
				  		<p class="mb-0 desc-last-update"><?php echo $this->lang->line('suku_bunga_ket') ?></p>
				  	</div>

				  	<div class="box-suku-bunga">
				  		<?php foreach($contentData['dataDeposito'] as $key => $val){ ?>
					  		<div class="item-suku mb-4">
					  			<h5 class="mb-3"><strong><?php echo $val['NAME']; ?></strong></h5>

					  			<!-- TBL DEPOSITO UMUM -->
					  			<div class="table-responsive">
					  			<?php if($val['ID_CATEG'] == 1){ ?>
					  				<table class="tbl-detail-item deposito">
						  				<thead>
						  					<tr>
						  						<th><?php echo $this->lang->line('suku_bunga_1') ?></th>
						  						<th><?php echo $this->lang->line('suku_bunga_0') ?></th>
						  						<th><?php echo $this->lang->line('suku_bunga_2') ?></th>
						  					</tr>
						  				</thead>
						  				<tbody>
						  					<?php 
						  					foreach($val['DETAIL'] as $key2 => $val2){
						  					?>
							  					<tr>
							  						<td><?php echo $val2['BALANCE'] ?></td>
							  						<td><?php echo $val2['MONTH'] ?> <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['INTEREST_RATE'] ?></td>
							  					</tr>
							  					<!-- <tr>
							  						<td>3 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr>
							  					<tr>
							  						<td>4 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr>
							  					<tr>
							  						<td>6 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr>
							  					<tr>
							  						<td>12 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr> -->
						  					<?php } ?>
						  				</tbody>
					  				</table>
					  			<?php } ?>
					  			</div>

					  			<!-- TBL DEPOSITO PLUS -->
					  			<?php if($val['ID_CATEG'] == 2){ ?>
					  			<table class="tbl-detail-item deposito">
					  				<tr>
					  					<th><?php echo $this->lang->line('suku_bunga_1') ?></th>
					  					<th> </th>
					  					<th>Min. Tabungan Kesra</th>
					  					<th><?php echo $this->lang->line('suku_bunga_3') ?></th>
					  				</tr>
					  				<?php foreach($val['DETAIL'] as $key2 => $val2){ ?>
					  				<tr>
					  					<td><?php echo $val2['BALANCE'] ?></td>
					  					<td><?php echo $val2['OPERATION'] ?></td>
					  					<td><?php echo $val2['MIN_TABUNGAN_KESRA'] ?></td>
					  					<td><?php echo $val2['INTEREST'] ?></td>
					  				</tr>
					  				<?php } ?>
					  			</table>
					  			<?php } ?>

					  			<!-- TBL DEPOSITO PREMIUM -->
					  			<div class="table-responsive">
						  			<?php if($val['ID_CATEG'] == 3){ ?>
						  			<table class="tbl-detail-item deposito">
						  				<thead>
						  					<tr>
						  						<th><?php echo $this->lang->line('suku_bunga_1') ?></th>
						  						<th><?php echo $this->lang->line('suku_bunga_0') ?></th>
						  						<th><?php echo $this->lang->line('suku_bunga_2') ?></th>
						  					</tr>
						  				</thead>
						  				<tbody>
						  					<?php 
						  					foreach($val['DETAIL'] as $key2 => $val2){
						  					?>
							  					<tr>
							  						<td><?php echo $val2['BALANCE'] ?></td>
							  						<td><?php echo $val2['MONTH'] ?> <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['INTEREST_RATE'] ?></td>
							  					</tr>
							  					<!-- <tr>
							  						<td>3 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr>
							  					<tr>
							  						<td>4 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr>
							  					<tr>
							  						<td>6 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr>
							  					<tr>
							  						<td>12 <?php echo $this->lang->line('suku_bunga_0') ?></td>
							  						<td><?php echo $val2['MONTH'] ?></td>
							  					</tr> -->
						  					<?php } ?>
						  				</tbody>
					  				</table>
						  			<?php } ?>
						  		</div>

					  			<!-- TBL DEPOSITO PERPANJANGAN -->
					  			<?php if($val['ID_CATEG'] == 4){ ?>
					  			<table class="tbl-detail-item deposito">
					  				<tr>
					  					<th><?php echo $this->lang->line('suku_bunga_0') ?></th>
					  					<th><?php echo $this->lang->line('suku_bunga_4') ?></th>
					  					<th><?php echo $this->lang->line('suku_bunga_5') ?></th>
					  				</tr>
					  				<?php foreach($val['DETAIL'] as $key2 => $val2){ ?>
					  				<tr>
					  					<td><?php echo $val2['MONTH_DETAIL'] ?> <?php echo $this->lang->line('suku_bunga_0') ?></td>
					  					<td><?php echo $val2['INTEREST_RATE_BEFORE'] ?></td>
					  					<td><?php echo $val2['INTEREST_RATE_NEXT'] ?></td>
					  				</tr>
					  				<?php } ?>
					  			</table>
					  			<?php } ?>

					  			<!-- TBL DEPOSITO USD -->
					  			<?php if($val['ID_CATEG'] == 5){ ?>
					  			<table class="tbl-detail-item deposito">
					  				<tr>
					  					<th><?php echo $this->lang->line('suku_bunga_1') ?></th>
					  					<th><?php echo $this->lang->line('suku_bunga_2') ?></th>
					  				</tr>
					  				<?php foreach($val['DETAIL'] as $key2 => $val2){ ?>
					  				<tr>
					  					<td><?php echo $val2['BALANCE'] ?></td>
					  					<td><?php echo $val2['INTEREST'] ?></td>
					  				</tr>
					  				<?php } ?>
					  			</table>
					  			<?php } ?>

					  		</div>
				  		<?php } ?>
				  	</div>
				  </div>
				</div>

				<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
			</div>
		</div>
	</div>
</div>