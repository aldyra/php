<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Province extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('province_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "province/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['province'] = $this->province_model->view();

        $this->load->view('templates/view', $data);
    }

    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['PROVINCE_ID']) ? $Data['PROVINCE_ID'] : "";

        $data['STATUS'] = "0";
        $where = array('PROVINCE_ID' => $id);
        $Active = $this->province_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['PROVINCE_ID']) ? $Data['PROVINCE_ID'] : "";

        $data['STATUS'] = "88";
        $where = array('PROVINCE_ID' => $id);
        $Active = $this->province_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['PROVINCE_ID']) ? $Data['PROVINCE_ID'] : "";

        $data['STATUS'] = "99";
        $where = array('PROVINCE_ID' => $id);
        $Active = $this->province_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}