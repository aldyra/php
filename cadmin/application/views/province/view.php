<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('province/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Province = isset($Data['province']) ? $Data['province'] : array();
?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Content</a>
                <a href="#" class="breadcrumb-item active">Province</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Province', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Province
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th>Province Name</th>
                    <th width="200" data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Province as $province) {
    $PROVINCE_ID = isset($province['PROVINCE_ID']) ? $province['PROVINCE_ID'] : "";
    $PROVINCE_NAME = isset($province['PROVINCE_NAME']) ? $province['PROVINCE_NAME'] : "";
    $STATUS = isset($province['STATUS']) ? $province['STATUS'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>

                    <td>
                        <?php echo $PROVINCE_NAME ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $param['Controllers'] = $Controllers;
        $param['Url'] = base_url() . $Controllers;
        $param['Data'] = $province;
        $param['Modal'] = "";
        $enc = base64_encode(json_encode($param));
        ?>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_update)) {?>

                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i>
                                        Activate</a>

                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i> Publish</a>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?}elseif($STATUS == "99"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(2).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(3)
            .search(dis.value)
            .draw();
    } else {
        t.column(3)
            .search(dis.value)
            .draw();
    }

}
</script>