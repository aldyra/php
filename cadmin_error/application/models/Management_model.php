<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Management_model extends MY_Model
{

    protected $_table_name = 'MS_MANAGEMENT_HEADER';
    protected $_detail_table_name = 'MS_MANAGEMENT_DETAIL';
    protected $_primary_key = 'ID_MANAGEMENT_HEADER';
    protected $_detail_primary_key = 'ID_MANAGEMENT_DETAIL';
    protected $_order_by = 'ID_MANAGEMENT_HEADER';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }
    public function get_management_detail($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_detail_table_name);
        return $view->result();

    }
    public function get_detail($id)
    {
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result();
        $html = '';
        foreach ($detail as $row) {
            $html .= '<tr id="edit' . $row->ID_MANAGEMENT_DETAIL . '" data-id="' . $row->ID_MANAGEMENT_DETAIL . '">
                        <td><input type="hidden" value="' . $row->ID_MANAGEMENT_DETAIL . '" name="id_detail[]" id="id_detail"><input type="text" class="form-control" id="year_id" name="year_id[]"
                                placeholder="Year" required="" parsley-trigger="change" value="' . $row->YEAR_ID . '"></td>
                        <td><input type="text" class="form-control" id="year_en" name="year_en[]"
                                placeholder="Year" required="" parsley-trigger="change" value="' . $row->YEAR_EN . '"></td>
                        <td><input type="text" class="form-control" id="desc_id" name="desc_id[]"
                                placeholder="Description ID" required="" parsley-trigger="change" value="' . htmlentities($row->DESCRIPTION_ID) . '"></td>
                        <td><input type="text" class="form-control" id="desc_en" name="desc_en[]"
                                placeholder="Description EN" required="" parsley-trigger="change" value="' . htmlentities($row->DESCRIPTION_EN) . '"></td>
                        <td> <button type="button" class="btn btn-danger btn-icon rounded-round"><i
                                    class="fas fa-minus" onclick="remove(\'edit' . $row->ID_MANAGEMENT_DETAIL . '\');"></i></button>
                        </td>
                    </tr>';
        }
        return $html;

    }

    public function updateaction($param)
    {
        $ID_MANAGEMENT_HEADER = isset($param['ID_MANAGEMENT_HEADER']) ? $param['ID_MANAGEMENT_HEADER'] : "";

        $WHERE['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_MANAGEMENT_HEADER'] = isset($param['ID_MANAGEMENT_HEADER']) ? $param['ID_MANAGEMENT_HEADER'] : "";
        $run = $this->db->where($WHERE)->delete($this->_detail_table_name);
        $run = $this->db->where($WHERE)->delete($this->_table_name);

        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

}