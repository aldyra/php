// (function(){
// 	var password = document.querySelector('.passwordvalidator');

// 	var helperText = {
// 		charLength: document.querySelector('.helper-text .length'),
// 		lowercase: document.querySelector('.helper-text .lowercase'),
// 		uppercase: document.querySelector('.helper-text .uppercase'),
// 		numeric: document.querySelector('.helper-text .numeric'),
// 		special: document.querySelector('.helper-text .special')
// 	};

// 	var pattern = {
// 		charLength: function() {
// 			if((password.value.length >= 5 ) && (password.value.length <=12)) {
// 				return true;
// 			}
// 		},
// 		lowercase: function() {
// 			var regex = /^(?=.*[a-z]).+$/; // Lowercase character pattern

// 			if( regex.test(password.value) ) {
// 				return true;
// 			}
// 		},
// 		uppercase: function() {
// 			var regex = /^(?=.*[A-Z]).+$/; // Uppercase character pattern

// 			if( regex.test(password.value) ) {
// 				return true;
// 			}
// 		},
// 		numeric: function(){
// 			var regex = /^(?=.*[0-9]).+$/; // Special character or number pattern

// 			if( regex.test(password.value) ) {
// 				return true;
// 			}
// 		},
// 		special: function() {
// 			var regex = /^(?=.*[!@#\$%\^&]).+$/; // Special character or number pattern

// 			if( regex.test(password.value) ) {
// 				return true;
// 			}
// 		}
// 	};

//  	 password.addEventListener('keyup', function (){
// 		patternTest( pattern.charLength(), helperText.charLength );

// 		patternTest( pattern.lowercase(), helperText.lowercase );

// 		patternTest( pattern.uppercase(), helperText.uppercase );

// 		patternTest( pattern.numeric(), helperText.numeric );

// 		patternTest( pattern.special(), helperText.special );

// 	    if( hasClass(helperText.charLength, 'invalid') && hasClass(helperText.lowercase, 'invalid') && hasClass(helperText.uppercase, 'invalid') && hasClass(helperText.numeric, 'invalid') && hasClass(helperText.special, 'invalid')) {
// 			password.style.border='1px solid #ddd';
// 	    }else {
// 	     	password.style.border='1px solid red';
// 	    }
// 	});

// 	function patternTest(pattern, response) {
// 		if(pattern) {
// 	      addClass(response, 'invalid');
// 	    }
// 	    else {
// 	      removeClass(response, 'invalid');
// 	    }
// 	}

// 	function addClass(el, className) {
// 		if (el.classList) {
// 			el.classList.add(className);
// 		}
// 		else {
// 			el.className += ' ' + className;
// 		}
// 	}

// 	function removeClass(el, className) {
// 		if (el.classList)
// 				el.classList.remove(className);
// 			else
// 				el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
// 	}

// 	function hasClass(el, className) {
// 		if (el.classList) {
// 			// console.log(el.classList);
// 			return el.classList.contains(className);
// 		}
// 		else {
// 			new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
// 		}
// 	}

// })();
$(".passwordvalidator").on("keyup", function () {
	$(this).parsley().validate();
});

//has uppercase
window.ParsleyValidator.addValidator(
	"uppercase",
	function (value, requirement) {
		var uppercases = value.match(/[A-Z]/g) || [];
		return uppercases.length >= requirement;
	},
	32
).addMessage(
	"en",
	"uppercase",
	"Your password must contain at least (%s) uppercase letter."
);

//has lowercase
window.ParsleyValidator.addValidator(
	"lowercase",
	function (value, requirement) {
		var lowecases = value.match(/[a-z]/g) || [];
		return lowecases.length >= requirement;
	},
	32
).addMessage(
	"en",
	"lowercase",
	"Your password must contain at least (%s) lowercase letter."
);

//has number
window.ParsleyValidator.addValidator(
	"number",
	function (value, requirement) {
		var numbers = value.match(/[0-9]/g) || [];
		return numbers.length >= requirement;
	},
	32
).addMessage(
	"en",
	"number",
	"Your password must contain at least (%s) number."
);

//has special char
window.ParsleyValidator.addValidator(
	"special",
	function (value, requirement) {
		var specials = value.match(/[!@#\$%\^&]/g) || [];
		return specials.length >= requirement;
	},
	32
).addMessage(
	"en",
	"special",
	"Your password must contain at least (%s) special characters."
);
