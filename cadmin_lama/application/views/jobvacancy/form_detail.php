<?php
$data = isset($data) ? $data : "";
// var_dump($data);exit();
$jobvacancy = isset($data['jobvacancy']) ? $data['jobvacancy'] : array();
$back = isset($data['back']) ? $data['back'] : '';
$is_update = $authorize['is_update'];

$ID_JOBVACANCY = isset($jobvacancy->ID_JOBVACANCY) ? $jobvacancy->ID_JOBVACANCY : "";
$ID_GROUP_JOBVACANCY = isset($jobvacancy->ID_GROUP_JOBVACANCY) ? $jobvacancy->ID_GROUP_JOBVACANCY : "";
$CODE = isset($jobvacancy->CODE) ? $jobvacancy->CODE : "";
$POSITION = isset($jobvacancy->POSITION) ? $jobvacancy->POSITION : "";
$JOB_DESCRIPTION = isset($jobvacancy->JOB_DESCRIPTION) ? $jobvacancy->JOB_DESCRIPTION : "";
$STATUS_JOB = isset($jobvacancy->STATUS_JOB) ? $jobvacancy->STATUS_JOB : "";
$LANGUAGE = isset($jobvacancy->LANGUAGE) ? $jobvacancy->LANGUAGE : "";
$REQUIREMENT = isset($jobvacancy->REQUIREMENT) ? $jobvacancy->REQUIREMENT : "";
$DETAIL = isset($jobvacancy->DETAIL) ? $jobvacancy->DETAIL : "";
$START_DATE = isset($jobvacancy->START_DATE) ? convertDate($jobvacancy->START_DATE) : "";
$END_DATE = isset($jobvacancy->END_DATE) ? convertDate($jobvacancy->END_DATE) : "";
$NAME_GROUP = isset($jobvacancy->NAME) ? $jobvacancy->NAME : "";
$STATUS = isset($jobvacancy->STATUS) ? $jobvacancy->STATUS : "";
$CREATED_BY = isset($jobvacancy->CREATED_BY) ? $jobvacancy->CREATED_BY : "";
$CREATED_DATE = isset($jobvacancy->CREATED_DATE) ? $jobvacancy->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}
if ($LANGUAGE == "ID") {
    $LANGUAGE = "Indonesia";

} elseif ($LANGUAGE == "EN") {
    $LANGUAGE = "English";
}

?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Career</a>
                <a href="#" class="breadcrumb-item ">Master Career</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item ">Master Job Vacancy</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-jobvacancy header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Job Vacancy</span> -
                Detail

            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="mb-4">
                <h4 class="font-weight-semibold mb-1">
                    <a class="text-default">Job Vacancy</a>
                </h4>
                <ul class="list-inline list-inline-dotted text-muted mb-3">
                    <li class="list-inline-item">By <?php echo $CREATED_BY ?></li>
                    <li class="list-inline-item"><?php echo $LANGUAGE ?></li>
                    <li class="list-inline-item"><?php echo convertDate($CREATED_DATE) ?></li>
                    <li class="list-inline-item"><?php echo $STS ?></li>
                </ul>
                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Date
                    </h6>
                    <?php echo $START_DATE . ' - ' . $END_DATE ?>
                </div>
                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Name Group
                    </h6>
                    <?php echo $NAME_GROUP ?>
                </div>
                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Code
                    </h6>
                    <?php echo $CODE ?>
                </div>
                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Position
                    </h6>
                    <?php echo $STATUS_JOB . ' - ' . $POSITION ?>
                </div>

                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Job Description
                    </h6>
                    <?php echo $JOB_DESCRIPTION ?>
                </div>
                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Required
                    </h6>
                    <?php echo $REQUIREMENT ?>
                </div>
                <div class="mb-3">
                    <h6 class="font-weight-semibold mb-1">
                        Detail
                    </h6>
                    <?php echo $DETAIL ?>
                </div>
            </div>
        </div>
    </div>

</div>