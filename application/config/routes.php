<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['struktur-organisasi'] = 'manajemen/struktur_organisasi';
$route['organization-structure'] = 'manajemen/struktur_organisasi';

$route['default_controller'] = 'home';
$route['404_override'] = '';

$route['(:any)'] = 'main/content/$i';

$route['lowongan-kerja/index'] = 'jobvacancy/index';
$route['lowongan-kerja/posisi/(:any)'] = 'jobvacancy/position/$1';
$route['lowongan-kerja/posisi/(:any)/(:num)'] = 'jobvacancy/position/$1/$2';
$route['lowongan-kerja/detail/(:any)'] = 'jobvacancy/detail/$1';

$route['job-vacancy/index'] = 'jobvacancy/index';
$route['job-vacancy/position/(:any)'] = 'jobvacancy/position/$1';
$route['job-vacancy/position/(:any)/(:num)'] = 'jobvacancy/position/$1/$2';
$route['job-vacancy/detail/(:any)'] = 'jobvacancy/detail/$1';

$route['careers/index'] = 'karir/index';
$route['karir/index'] = 'karir/index';

$route['simulasi-ppr/index'] = 'simulasi/ppr';
$route['ppr-simulation/index'] = 'simulasi/ppr';

$route['simulasi-ppm/index'] = 'simulasi/ppm';
$route['ppm-simulation/index'] = 'simulasi/ppm';

$route['good-corporate-governance/index'] = 'gcg/index';

$route['laporan-tahunan/index'] = 'laporan/tahunan';
$route['annual-financial-reports/index'] = 'laporan/tahunan';

$route['laporan-bulanan/index'] = 'laporan/bulanan';
$route['monthly-financial-reports/index'] = 'laporan/bulanan';

$route['laporan-keuangan-publikasi/index'] = 'laporan/keuangan_publikasi';
$route['published-financial-statement/index'] = 'laporan/keuangan_publikasi';

$route['laporan-keuangan-interim/index'] = 'laporan/keuangan_interim';
$route['interim-financial-statement/index'] = 'laporan/keuangan_interim';

$route['laporan-keuangan-konsolidasi/index'] = 'laporan/keuangan_konsolidasi';
$route['consolidated-financial-statement/index'] = 'laporan/keuangan_konsolidasi';

$route['laporan-pengungkapan-informasi-kuantitatif-eksposur-risiko/index'] = 'laporan/pengungkapan_informasi_kuantitatif';
$route['disclosure-of-quantitative-information-risk-exposure/index'] = 'laporan/pengungkapan_informasi_kuantitatif';

$route['laporan-total-eksposur/index'] = 'laporan/total_eksposur';
$route['total-exposure-report/index'] = 'laporan/total_eksposur';

$route['laporan-perhitungan-rasio-pengungkit/index'] = 'laporan/perhitungan_rasio_pengungkit';
$route['everage-ratio-calculation-report/index'] = 'laporan/perhitungan_rasio_pengungkit';

$route['peta-situs/index'] = 'sitemap/index';
$route['site-map/index'] = 'sitemap/index';

$route['sbdk/index'] = 'sukubunga/sbdk';
$route['prime-lending-rate/index'] = 'sukubunga/sbdk';

$route['kurs/index'] = 'kurs/index';
$route['exchange-rate/index'] = 'kurs/index';

$route['suku-bunga-suku-bunga/index'] = 'sukubunga/index';
$route['rates-rates/index'] = 'sukubunga/index';

$route['pemenang-undian-tabungan-kesra/index'] = 'pemenang/index';
$route['tabungan-kesra-grand-prize-winner/index'] = 'pemenang/index';

$route['penghargaan/index'] = 'penghargaan/index';
$route['awards/index'] = 'penghargaan/index';

$route['cabang/index'] = 'cabang/index';
$route['cabang/kota/(:any)'] = 'cabang/city/$1';
$route['cabang/lokasi/(:any)/(:any)'] = 'cabang/location/$1/$2';
$route['cabang/view'] = 'cabang/view';

$route['branch/index'] = 'cabang/index';
$route['branch/city/(:any)'] = 'cabang/city/$1';
$route['branch/location/(:any)/(:any)'] = 'cabang/location/$1/$2';
$route['branch/view'] = 'cabang/view';

$route['dewan-komisaris/index'] = 'manajemen/dewan_komisaris';
$route['board-of-commisioners/index'] = 'manajemen/dewan_komisaris';
$route['dewan-komisaris/detail/(:any)'] = 'manajemen/detail/$1';
$route['board-of-commisioners/detail/(:any)'] = 'manajemen/detail/$1';

$route['dewan-direksi/index'] = 'manajemen/dewan_direksi';
$route['board-of-director/index'] = 'manajemen/dewan_direksi';
$route['dewan-direksi/detail/(:any)'] = 'manajemen/detail/$1';
$route['board-of-director/detail/(:any)'] = 'manajemen/detail/$1';

$route['sekretaris-perusahaan/index'] = 'manajemen/sekretaris_perusahaan';
$route['corporate-secretary/index'] = 'manajemen/sekretaris_perusahaan';
$route['sekretaris-perusahaan/detail/(:any)'] = 'manajemen/detail/$1';
$route['corporate-secretary/detail/(:any)'] = 'manajemen/detail/$1';

$route['komite-audit/index'] = 'manajemen/komite_audit';
$route['audit-commitee/index'] = 'manajemen/komite_audit';
$route['komite-audit/detail/(:any)'] = 'manajemen/detail/$1';
$route['audit-commitee/detail/(:any)'] = 'manajemen/detail/$1';

$route['komite-pemantau-resiko/index'] = 'manajemen/pemantau_resiko';
$route['risk-monitoring-commitee/index'] = 'manajemen/pemantau_resiko';
$route['komite-pemantau-resiko/detail/(:any)'] = 'manajemen/detail/$1';
$route['risk-monitoring-commitee/detail/(:any)'] = 'manajemen/detail/$1';

$route['komite-remunerasi-dan-nominasi/index'] = 'manajemen/komite_remunerasi';
$route['remuneration-and-nomination-commitee/index'] = 'manajemen/komite_remunerasi';
$route['komite-remunerasi-dan-nominasi/detail/(:any)'] = 'manajemen/detail/$1';
$route['remuneration-and-nomination-commitee/detail/(:any)'] = 'manajemen/detail/$1';

$route['search/index'] = 'search/index';
$route['search/keywords/(:any)'] = 'search/keywords/$1';
$route['search/keywords/(:any)/(:num)'] = 'search/keywords/$1/$2';

$route['cari/index'] = 'search/index';
$route['cari/katakunci/(:any)'] = 'search/keywords/$1';
$route['cari/katakunci/(:any)/(:num)'] = 'search/keywords/$1/$2';

$route['rups/index'] = 'rups/index';
$route['general-meeting-of-shareholders/index'] = 'rups/index';

$route['ringkasan-prospektus/index'] = 'prospektus/index';
$route['summary-of-prospektus/index'] = 'prospektus/index';
