<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();

$HEADER_BANNER = $contentData['menuDetail']['HEADER_BANNER'];
$FILE_BANNER = isset($contentData['menuDetail']['FILE_BANNER']) ? $contentData['menuDetail']['FILE_BANNER']:"";
$stylebanner = "";
if(!empty($FILE_BANNER) && $HEADER_BANNER == 1){
	$stylebanner = "background-image:url('".$FILE_BANNER."'); background-size:100% 100%;height:250px;";
}
?>
<div class="row row-header" style="<?php echo $stylebanner; ?>">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex align-items-center">
		<div class="box-header">	
			<h4 style="margin-bottom: 0 !important;width: 100%;"><?php echo $MENU_PARENT?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"> -->
			<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<!-- </a> / -->
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<style type="text/css">
	#myInput {
	    background-image: url(<?=base_url('assets/image/searchicon.png')?>);
	    background-position: 10px 12px;
	    background-repeat: no-repeat;
	    width: 100%;
	    font-size: 14px;
	    padding: 12px 20px 12px 40px;
	    border: 1px solid #ddd;
	    margin-bottom: 12px;
	    max-width: 90%;
	}
	/*.box-laporan{
		padding-left: 1.5rem;
	}*/
	.item-laporan{
		box-shadow: 2px 0px 4px #e5e5e5;
	    max-width: 90%;
	    padding: 12px 15px;
	    display: flex;
	    align-items: center;
	}
	.item-laporan .ext_file{
		text-transform: uppercase;
		padding: 5px;
	    margin-right: .5rem;
	    font-size: 10px;
	    width: 35px;
    	text-align: center;
		color: #fff;
	}
	.item-laporan .ext_file.pdf{
		background: #233F8A;
	}
	.item-laporan .ext_file.other{
		background: #FFD35D;
	}
	div.card-loker{
		box-shadow: 0px 1px 3px 1px #dfdfde;
		cursor: pointer;
	}

	div.card-loker:hover{
		background-color: #28448f;
		color: white;
	}
/*#243E8B*/
	/*.paginationjs-pages .disabled{
	    visibility: hidden;
	}*/
	.paginationjs-pages .current a{
	    cursor: none;
	}

	
	.pagination-wrap{
		max-width: 90%;
	}
	.pagination ul li {
	  display: inline-block;
	  width: 30px;
	  height: 30px;
	  line-height: 30px;
	  text-align: center;
	  background: #f1f1f1;
	  border-radius: 3px;
	  margin-left: 3px;
	}
	.pagination ul li:first-child {
	  margin-left: 0;
	  margin-right: 10px;
	}
	.pagination ul li:last-child {
	    margin-right: 0;
	  	margin-left: 10px;
	}
	.pagination ul li a {
	  display: block;
	  border-radius: 3px;
	}
	.pagination ul li a:hover {
	  background: #233F8A;
	  color: #ffffff;
	}
	.pagination ul li.current a {
	  background: #233F8A;
	  color: #ffffff !important;
	}
	.pagination ul li.next {
	  width: 40px;
	}

	.paginationjs-prev, .paginationjs-next{
		background: #cccccc !important;
    	border-radius: 50% !important;
    	margin: 0 10px;
	}
	.paginationjs-prev a, .paginationjs-next a{
		color: #fff !important;
    	border-radius: 50% !important;
	}

	.pagination ul li.paginationjs-page.J-paginationjs-page, .pagination ul li.paginationjs-page.J-paginationjs-page a {
	    width: 25px;
	    height: 25px;
	    line-height: 25px;
	    border-radius: 8px;
	    color: #9D9D9D;
	    text-decoration: none;
	}

	.result-num{
		max-width: 90%;
		color: #9d9d9d;
    	font-size: 15px;
	}

	section#section1{
		padding-top: 60px !important;
	}

	.box-laporan{
		font-size: 14px;
	}

	.row-page-module{
		font-size: 14px;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-sbdk {
		    max-width: 100%;
		}
		.title-laporan{
			margin-top: 1rem;
			font-size: 20px; 
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}
		#myInput, .item-laporan{
			max-width: 100%;
		}
	}

	@media all and (min-width: 992px) {
		.audit-ket{
			text-align: left;
		}

		.justify-content-end{
			justify-content: flex-end !important;
		}
	}

	@media all and (max-width: 992px) {
		.audit-ket{
			text-align: center;
		}

		.justify-content-end{
			justify-content: center !important;
		}
	}
</style>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
            <?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-9" style="margin-bottom: 30px;">
			<h4 class="mr-3 title-laporan" style="color: #242424;font-weight: bold;margin-bottom: 1.5rem; font-size: 18px;"><?php echo $MENU_CHILD; ?></h4>
			<input type="text" id="myInput" onkeyup="" placeholder="<?php echo ucwords($this->lang->line('search-0')) ?>">
			<div id="myUL" class="box-laporan"></div>

			<div class="audit-ket mt-4">
				<?php echo $contentData['menuDetail']['FOOTER_NOTE'] ?>
			</div>

			<div class="d-flex justify-content-end pagination-wrap mt-3">
            	<div class="pagination" id="productPaging"></div>
            </div>
            <div class="result-num d-flex justify-content-end align-items-center pagination-wrap mt-2">
            	<select style="width:80px;height:40px" id="optPerPage" class="mr-2 form-control">
            		<option selected="" value="10">10</option>
            		<option value="25">25</option>
            		<option value="50">50</option>
            		<option value="100">100</option>
            	</select>
            	<p class="mb-0"><?php echo ucwords($this->lang->line('result')) ?>&nbsp;:&nbsp;<span id="showingProduct"></span>&nbsp;of&nbsp;<span id="totalProduct"></span></p>
        	</div>

		</div>
	</div>
</div>

<script>
	var globalPage = 1;
	var globalPerPage = 10;
	var globalCategory = <?php echo $categoryLaporan; ?>;
	var globalSearch = "";

	$(document).ready(function(){

		initProduct();

		$('#myInput').keypress(function (e) {
		  if (e.which == 13) {
		    searchNew();
		  }
		});

		$("#optPerPage").on('change', function(){
			var _val = $(this).val();
			customPerPage(_val);
		});

	});

	function initPagination(page, perPage, totalPage){
		// console.log(page);
		// console.log(perPage);
		// console.log(totalPage);

	    $('#productPaging').pagination({
	        dataSource: function(done){
	            var result = [];
	            for (var i = 1; i <= totalPage; i++) {
	                result.push(i);
	            }
	            done(result);
	        },
	        prevText: '<i class="fa fa-arrow-left"></i>',
	        nextText: '<i class="fa fa-arrow-right"></i>',
	        autoHidePrevious: false,
    		autoHideNext: false,
	        pageNumber: page,
	        pageSize: perPage,
	        activeClassName: 'current',
	        callback: function(data, pagination){
	            $('.J-paginationjs-page').on('click', function(e){
	                if(!$(this).hasClass('current')){
	                    // console.log($(this).attr('data-num'));
	                    var tmpPage = $(this).attr('data-num');
	                    globalPage = parseInt(tmpPage);
	                    initProduct();
	                }
	            });

	            $('.paginationjs-prev').on('click', function(e){
	                if(!$(this).hasClass('disabled')){
	                    // console.log($(this).attr('data-num'));
	                    // var tmpPage = $(this).attr('data-num');
	                    globalPage -= 1;
	                    initProduct();
	                }
	            });

	            $('.paginationjs-next').on('click', function(e){
	                if(!$(this).hasClass('disabled')){
	                    // console.log($(this).attr('data-num'));
	                    // var tmpPage = $(this).attr('data-num');
	                    globalPage += 1;
	                    initProduct();
	                }
	            });

	        }
	    });
	}

	function initProduct(){
	    const paramArray = {
	    	paramCategory: globalCategory,
	        paramPerPage: globalPerPage,
	        paramPage: globalPage,
	        paramSearch: globalSearch
	    }
	    const param = { param: paramArray };
	    // const param = JSON.stringify(paramArray);
	    getListData(param);
	}

	function getListData(param){
	    $('#myUL').html('');
	    $.ajax({
	        type: "POST",
	        url: "<?php echo base_url('Laporan/list_lapor');?>",
	        data: param,
	        dataType: 'json',
	        success: function(response) {
	            console.log(response);

	            if(response.getLaporan.length > 0){
	            	$('#myUL').html("");
	                $.each(response.getLaporan, function(index, item){
	                    // var html = addListData(item, response.myStore);
	                    var html = addList(item);
	                    $('#myUL').append(html);
	                });
	                // isNoData = false;
	            }else{
	                var html = '<div class="col-12"><center>Data yang anda cari tidak ditemukan..</center></div>';
	                $('#myUL').append(html);
	                // isNoData = true;
	            }

	            $('#showingProduct').text(response.record);
            	$('#totalProduct').text(response.totalLaporan);

	            initPagination(param.param.paramPage, param.param.paramPerPage, response.totalLaporan);
	        },
	        error: function() {
	            alert('Connection timeout.');
	        }
	    });
	}

	function addList(param) {
		var html = '<div class="item-laporan">'+
						'<span class="ext_file '+( (param.FILE_EXT == 'pdf') ? "pdf" : "other" )+' ">'+param.FILE_EXT+'</span>'+
						'<a target="_blank" href="'+param.LINK+'">'+param.REPORT_NAME+'</a>'+
					'</div>';

		return html;
	}

	function searchNew(){
	    var input, filter, ul, li, a, i, txtValue;

	    input = document.getElementById("myInput");
	    globalSearch = input.value;
        globalPage = 1;

	    initProduct();
		// console.log(filter);
	}

	function customPerPage(perPage) {
        globalPage = 1;
		globalPerPage = parseInt(perPage);
		
		initProduct();
	}

	function mySearch() {
	    var input, filter, ul, li, a, i, txtValue;
	    input = document.getElementById("myInput");
	    filter = input.value.toUpperCase();
	    ul = document.getElementById("myUL");
	    li = ul.getElementsByTagName("div");
	    for (i = 0; i < li.length; i++) {
	        a = li[i].getElementsByTagName("a")[0];
	        txtValue = a.textContent || a.innerText;
	        if (txtValue.toUpperCase().indexOf(filter) > -1) {
	            li[i].style.display = "";
	        } else {
	            li[i].style.display = "none";
	        }
	    }
	}
</script>