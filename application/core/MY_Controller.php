<?php
class MY_Controller extends CI_Controller
{
	function __construct()
	{
        parent::__construct();		
        $REQUEST_URI = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']:"";
        if (strpos($REQUEST_URI, '/en/') !== false) {
            $_SESSION['site_lang'] = "en";
        }else if(strpos($REQUEST_URI, '/id/') !== false){
            $_SESSION['site_lang'] = "id";
        }

        $this->data['nav_menu'] = $this->Module_model->getallmodule();
        $this->data['nav_menu_footer'] = $this->Module_model->getfootermodule();
        $this->data['social_media'] = $this->Module_model->socialmedia();
        $this->data['navbar_link'] = $this->Module_model->getnavbarlink();
        $this->data['kurs'] = $this->Main_model->kurs();
        $this->data['sbdk'] = $this->Main_model->sbdk();
        $this->data['province'] = $this->Main_model->province();
    }
}