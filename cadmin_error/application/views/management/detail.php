<?php
$data = isset($data) ? $data : "";
$ID_MANAGEMENT_HEADER = isset($data['management']->ID_MANAGEMENT_HEADER) ? $data['management']->ID_MANAGEMENT_HEADER : "";
$LINK = isset($data['management']->LINK) ? $data['management']->LINK : "";
$NAME = isset($data['management']->NAME) ? $data['management']->NAME : "";
$POSITION_CATEGORY = isset($data['management']->POSITION_CATEGORY) ? $data['management']->POSITION_CATEGORY : "";
$POSITION_ID = isset($data['management']->POSITION_ID) ? $data['management']->POSITION_ID : "";
$POSITION_EN = isset($data['management']->POSITION_EN) ? $data['management']->POSITION_EN : "";
$DESCRIPTION_ID = isset($data['management']->DESCRIPTION_ID) ? $data['management']->DESCRIPTION_ID : "";
$DESCRIPTION_EN = isset($data['management']->DESCRIPTION_EN) ? $data['management']->DESCRIPTION_EN : "";

$STATUS = isset($data['management']->STATUS) ? $data['management']->STATUS : "";
$CREATED_BY = isset($data['management']->CREATED_BY) ? $data['management']->CREATED_BY : "";
$CREATED_DATE = isset($data['management']->CREATED_DATE) ? $data['management']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}

$POSITION_ID = $this->config->item('POSITION_ID');
$POSITION_EN = $this->config->item('POSITION_EN');
$ID_CATEG_POSITION = $this->config->item('ID_CATEG_POSITION');
$Exp_POSITION_ID = explode(":", $POSITION_ID);
$Exp_POSITION_EN = explode(":", $POSITION_EN);
$Exp_ID_CATEG_POSITION = explode(":", $ID_CATEG_POSITION);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_POSITION); $i++) {
    $ID = $Exp_ID_CATEG_POSITION[$i];
    $DESC_EN = $Exp_POSITION_EN[$i];
    $DESC_ID = $Exp_POSITION_ID[$i];
    if ($POSITION_CATEGORY == $ID) {
        $POSITION_CATEGORY = $DESC_EN . ' / ' . $DESC_ID;
    }
}

$back = isset($data['back']) ? $data['back'] : '';
$detail = isset($data['detail']) ? $data['detail'] : array();
$is_read = $authorize['is_read'];
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Management</a>
                <a href="#" class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Management</span> -
                Detail

            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Combined table styles -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <a href='<?php echo $LINK ?>' data-popup='lightbox' data-fancybox><img src='<?php echo $LINK ?>'
                            class='img-thumbnail' width='200px'></a>
                </div>
                <div class="col-md-10">
                    <dl class="mb-0">
                        <dt>Name</dt>
                        <dd><?php echo $NAME ?></dd>
                        <dt>Position Category</dt>
                        <dd>
                            <?php echo $POSITION_CATEGORY ?>
                        </dd>
                        <dt>Position</dt>
                        <dd><?php echo $POSITION_ID . ' ' . $POSITION_EN ?></dd>
                        <dt>Description IDN</dt>
                        <dd><?php echo $DESCRIPTION_ID ?></dd>
                        <dt>Description ENG</dt>
                        <dd><?php echo $DESCRIPTION_EN ?></dd>
                    </dl>
                </div>
            </div>

        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Year ID</th>
                        <th>Year EN</th>
                        <th>Description Indonesian</th>
                        <th>Description English</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0;foreach ($detail as $row) {?>
                    <tr>
                        <td><?php $no = $no + 1;
    echo $no;?></td>
                        <td><?php echo $row->YEAR_ID ?></td>
                        <td><?php echo $row->YEAR_EN ?></td>
                        <td><?php echo $row->DESCRIPTION_ID ?></td>
                        <td><?php echo $row->DESCRIPTION_EN ?></td>
                    </tr>
                    <?php }?>


                </tbody>
            </table>
        </div>

        <div class="card-footer bg-white d-flex justify-content-between">
            <div>
                <div class="list-icons">

                </div>
            </div>

            <ul class="list-inline list-inline-dotted mb-0 mt-1 mt-sm-0">
                <li class="list-inline-item">By <?php echo $CREATED_BY ?></li>
                <li class="list-inline-item"><?php echo convertDate($CREATED_DATE) ?></li>
                <li class="list-inline-item"><?php echo $STS ?></li>
            </ul>
        </div>
    </div>
    <!-- /combined table styles -->

</div>