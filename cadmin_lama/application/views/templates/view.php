<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Web Admin | Bank Bumi Arta</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <link href="<?php echo base_url(); ?>assets/lib/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <!-- Treeview css -->
    <link href="<?php echo base_url(); ?>assets/lib/treeview/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/lib//dropify/dropify.min.css">
    <link href="<?php echo base_url(); ?>assets/lib/fancybox/jquery.fancybox.min.css" rel="stylesheet"
        type="text/css" />

    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->
    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
    <!-- /theme JS files -->
    <!-- Validation js (Parsleyjs) -->
    <script src="<?php echo base_url(); ?>assets/lib/parsleyjs/parsley.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/parsleyjs/init_parsley.js"></script>
    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url(); ?>assets/lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- Sweet alert init js-->
    <script src="<?php echo base_url(); ?>assets/lib/sweetalert2/sweet-alerts.init.js"></script>
    <!-- JS Tree js-->
    <script src="<?php echo base_url(); ?>assets/lib/treeview/js/jstree.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/treeview/js/jstreegrid.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/treeview/js/treebase.js"></script>
    <!-- Select2 js-->
    <script src="<?php echo base_url(); ?>assets/lib/select2/dist/js/select2.full.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/lib/ckeditor4/ckeditor.js"></script>
    <!-- Theme JS files -->
    <script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/demo_pages/datatables_responsive.js"></script>
    <!-- /theme JS files -->

    <script src="<?php echo base_url(); ?>assets/lib/fancybox/jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url() ?>assets/lib/dropify/dropify.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>


    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>

    <script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script>

    <script src="<?php echo base_url() ?>assets/js/app.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/public.js"></script>



</head>
<style>

</style>

<body>

    <?php $this->load->view('templates/navbar')?>
    <!-- Page content -->
    <div class="page-content">
        <?php $this->load->view('templates/sidebar')?>

        <!-- Main content -->
        <div class="content-wrapper">
            <?php $this->load->view($content);?>
            <?php $this->load->view('templates/footer');?>

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <div id="modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true"></div>

    <div class="modal fade" id="modalTnCUpload" role="dialog" aria-labelledby="TnCUploadLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TnCUploadLabel">Terms for upload file</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="tncupload">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $("li.active").removeClass("active");
        $('a[href="' + window.location.href + '"]')
            .addClass("active");
        $('a[href="' + window.location.href + '"]').parent().parent().parent()
            .addClass("nav-item-expanded")
            .addClass("nav-item-open");
        $('a[href="' + window.location.href + '"]').parent().parent().parent().parent().parent()
            .addClass("nav-item-expanded")
            .addClass("nav-item-open");
    });
    </script>
</body>

</html>