function showhidetoggle(){
	var toggle = document.getElementById('chevron-toggle').getAttribute('data');
	if(toggle == "0"){
		$('#chevron-toggle').attr({'data':'1'});
		$('#chevron-toggle').removeClass().addClass('fas fa-chevron-down');
		$('.list-group-module').fadeOut('1000');
	}else{
		$('#chevron-toggle').attr({'data':'0'});
		$('#chevron-toggle').removeClass().addClass('fas fa-chevron-up');
		$('.list-group-module').fadeIn('1000');
	}
}