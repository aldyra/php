<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simulasi extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function ppr()
	{
		$post = array();
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$link = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$provideData = $this->provideData();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "simulasi_ppr";
		$this->data['contentData'] = $provideData;
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
	}

	public function ppm()
	{
		$post = array();
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$link = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$provideData = $this->provideData();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "simulasi_ppm";
		$this->data['contentData'] = $provideData;
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
	}

	function provideData()
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$GetModule = $this->Module_model->getmodule($linkingg);
		$PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
		$MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";

		// MENU PARENT -1
		$Q_MP = $this->db->query("SELECT * FROM MS_MENU WHERE ID = '$MENU_PARENT'")->row_array();
		if ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) {
			// $MENU_PARENT = ( isset($Q_MP['MENU_PARENT']) && !empty($Q_MP['MENU_PARENT']) ) ? $Q_MP['MENU_PARENT'] : "";
			$MENU_PARENT = $Q_MP['MENU_PARENT'];
		}

		$ID_MENU_PARENT = $MENU_PARENT;
		$getMenuLeft = array();
		$footerContentEn = "";
		$footerContentId = "";
		$getMenu1 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->result_array();
		$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->row_array();

		if(($getMenu2['FOOTER_NOTE_EN'] != '') && ($getMenu2['FOOTER_NOTE_ID'] != '')){
			$footerContentEn = $getMenu2['FOOTER_NOTE_EN'];
			$footerContentId = $getMenu2['FOOTER_NOTE_ID'];
		}

		foreach ($getMenu1 as $key => $value) {
			$getMenuLeft[$key]['menu_1']['NAME'] = $value[$this->cekLang('NAME')];
			$getMenuLeft[$key]['menu_1']['LINK'] = $value[$this->cekLang('LINK')];

			// cek aktif ga menu 1 single
			$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = ( $value[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

			$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value['ID'])->result_array();

			if (count($getMenu2) > 0) {
				// deklarasi aktif ga menu 1 multiple
				$stakMenu2 = array();
				$linkMenu1 = $value[$this->cekLang('LINK')];

				foreach ($getMenu2 as $key2 => $value2) {
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['NAME'] = $value2[$this->cekLang('NAME')];
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['LINK'] = $value2[$this->cekLang('LINK')];

					// cek aktif ga menu single
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = ( $value2[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

					$getMenu3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value2['ID'])->result_array();

					if (count($getMenu3) > 0) {
						// deklarasi aktif ga menu 2 multiple
						$stakMenu3 = array();
						$linkMenu2 = $value2[$this->cekLang('LINK')];

						foreach ($getMenu3 as $key3 => $value3) {
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['NAME'] =
							 	$value3[$this->cekLang('NAME')];
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['LINK'] =
							 	$value3[$this->cekLang('LINK')];

							 $getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['ACTIVE_URL'] = ( $value3[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

							$stakMenu3[] = $value3[$this->cekLang('LINK')];
						}

						// cek aktif ga menu 2 multiple
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = (in_array($linkingg, $stakMenu3)) ? TRUE : FALSE;

					}else{
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'] = array();
					}

					$stakMenu2[] = $value2[$this->cekLang('LINK')];
				}

				// cek aktif ga menu 1 multiple
				$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = (in_array($linkingg, $stakMenu2)) ? TRUE : FALSE;

			}else{
				$getMenuLeft[$key]['menu_1']['menu_2'] = array();
			}
		}

		$getMenuDetail = $this->db->query("SELECT * FROM MS_MENU WHERE LINK_ID = '".$linkingg."' OR LINK_EN = '".$linkingg."'")->row_array();
		$getMenuDetail['FOOTER_NOTE'] = $getMenuDetail[$this->cekLang('FOOTER_NOTE')];
		$getMenuDetail['FILE_BANNER'] = $getMenuDetail[$this->cekLang('FILE_BANNER')];

		// return array('totalLaporan' => $total_laporan, 'laporan' => $getLaporan, 'menuLeft' => $getMenuLeft, 'menuDetail' => $getMenuDetail, 'footerContentId' => $footerContentId, 'footerContentEn'=>$footerContentEn);
		return array('menuLeft' => $getMenuLeft, 'menuDetail' => $getMenuDetail, 'footerContentId' => $footerContentId, 'footerContentEn'=>$footerContentEn);
		// return array('laporan' => $getLaporan, 'menuLeft' => $getMenuLeft);
	}

	function cekLang($name)
	{
		if ($_SESSION['lang'] == 'id') {
			$nameLang = $name.'_ID';
		}else{
			$nameLang = $name.'_EN';
		}

		return $nameLang;
	}
}