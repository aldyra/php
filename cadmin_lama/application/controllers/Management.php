<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Management extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('management_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "management/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['management'] = $this->management_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Image max pixel ' . $this->config->item('max_height_management') . 'x' . $this->config->item('max_width_management') . 'px <br>
                    Allowed images are ' . str_replace("|", ", ", $this->config->item('allowed_types_image')) . ' <br>
                    Max image size ' . $this->config->item('max_size_management') . 'KB<br>';
        echo json_encode($data);
    }
    public function detail_management($id)
    {
        $data = array();
        $data['content'] = "management/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['management'] = $this->management_model->get_edit($id);
        $data['data']['detail'] = $this->management_model->get_management_detail($id);
        $data['data']['back'] = base_url() . 'management';
        $this->load->view('templates/view', $data);
    }
    public function add_management()
    {
        $data = array();
        $data['content'] = "management/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'management';

        $this->load->view('templates/view', $data);
    }
    public function edit_management($id)
    {
        $data = array();
        $data['content'] = "management/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['management'] = $this->management_model->get_edit($id);
        $data['data']['detail'] = $this->management_model->get_detail($id);
        $data['data']['back'] = base_url() . 'management';
        $this->load->view('templates/view', $data);
    }

    public function add()
    {
        // var_dump($_POST);
        // var_dump($_FILES);exit();
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $POSITION_ID = isset($_POST['position_id']) ? $_POST['position_id'] : "";
        $POSITION_EN = isset($_POST['position_en']) ? $_POST['position_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";
        $POSITION_CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        //detail
        $DESCRIPTION_POSITION_ID = isset($_POST['desc_id']) ? $_POST['desc_id'] : "";
        $DESCRIPTION_POSITION_EN = isset($_POST['desc_en']) ? $_POST['desc_en'] : "";
        $YEAR_EN = isset($_POST['year_en']) ? $_POST['year_en'] : "";
        $YEAR_ID = isset($_POST['year_id']) ? $_POST['year_id'] : "";

        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
            die(json_encode($JSON));
        }

        $pathmanagement = $this->config->item('save_management');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size_management');
        $config['max_width'] = $this->config->item('max_width_management');
        $config['min_width'] = $this->config->item('min_width_management');
        $config['max_height'] = $this->config->item('max_height_management');
        $config['min_height'] = $this->config->item('min_height_management');
        $config['file_name'] = 'MANAGEMENT_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_management');
        $path_parts = pathinfo($_FILES["file"]["name"]);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_image');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $this->db->trans_begin();

                $dataheader['NAME'] = $NAME;
                $dataheader['POSITION_ID'] = $POSITION_ID;
                $dataheader['POSITION_EN'] = $POSITION_EN;
                $dataheader['POSITION_CATEGORY'] = $POSITION_CATEGORY;
                $dataheader['DESCRIPTION_ID'] = $DESCRIPTION_ID;
                $dataheader['DESCRIPTION_EN'] = $DESCRIPTION_EN;
                $dataheader['LINK'] = $pathmanagement . $config['file_name'] . "." . $forUpload['extension'];
                $dataheader['CREATED_BY'] = $_SESSION['username'];
                $insert_header = $this->db->insert('MS_MANAGEMENT_HEADER', $dataheader);
                $ID_MANAGEMENT_HEADER = $this->db->insert_id();

                if (!$insert_header) {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));

                } else {
                    $detail = count($YEAR_ID);
                    for ($i = 0; $i < $detail; $i++) {
                        $datadetail['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
                        $datadetail['YEAR_ID'] = $YEAR_ID[$i];
                        $datadetail['YEAR_EN'] = $YEAR_EN[$i];
                        $datadetail['DESCRIPTION_ID'] = $DESCRIPTION_POSITION_ID[$i];
                        $datadetail['DESCRIPTION_EN'] = $DESCRIPTION_POSITION_EN[$i];
                        $datadetail['CREATED_BY'] = $_SESSION['username'];
                        $insert_detail = $this->db->insert('MS_MANAGEMENT_DETAIL', $datadetail);
                        if (!$insert_detail) {
                            $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                            $this->db->trans_rollback();
                            die(json_encode($JSON));
                        }

                    }

                    if ($this->db->trans_status() === false) {
                        $final_res = 'Failed: Failed to submit data';
                        $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                        $this->db->trans_rollback();
                        die(json_encode($JSON));

                    } else {
                        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                        $this->db->trans_commit();
                        die(json_encode($JSON));

                    }

                }

            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->management_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        // var_dump($_POST);
        // var_dump($_FILES);exit();

        $LIST_DELETE = isset($_POST['listDelete']) ? explode(",", $_POST['listDelete']) : "";
        $ID_MANAGEMENT_HEADER = isset($_POST['id_management_header']) ? $_POST['id_management_header'] : "";
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $POSITION_CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $POSITION_ID = isset($_POST['position_id']) ? $_POST['position_id'] : "";
        $POSITION_EN = isset($_POST['position_en']) ? $_POST['position_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";
        $POSITION_CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        //detail
        $ID_MANAGEMENT_DETAIL = isset($_POST['id_detail']) ? $_POST['id_detail'] : "";
        $DESCRIPTION_POSITION_ID = isset($_POST['desc_id']) ? $_POST['desc_id'] : "";
        $DESCRIPTION_POSITION_EN = isset($_POST['desc_en']) ? $_POST['desc_en'] : "";
        $YEAR_EN = isset($_POST['year_en']) ? $_POST['year_en'] : "";
        $YEAR_ID = isset($_POST['year_id']) ? $_POST['year_id'] : "";

        $this->db->trans_begin();

        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }

            $pathmanagement = $this->config->item('save_management');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size_management');
            $config['max_width'] = $this->config->item('max_width_management');
            $config['min_width'] = $this->config->item('min_width_management');
            $config['max_height'] = $this->config->item('max_height_management');
            $config['min_height'] = $this->config->item('min_height_management');
            $config['file_name'] = 'MANAGEMENT_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_management');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_image');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $dataheader['NAME'] = $NAME;
                    $dataheader['POSITION_ID'] = $POSITION_ID;
                    $dataheader['POSITION_EN'] = $POSITION_EN;
                    $dataheader['POSITION_CATEGORY'] = $POSITION_CATEGORY;
                    $dataheader['DESCRIPTION_ID'] = $DESCRIPTION_ID;
                    $dataheader['DESCRIPTION_EN'] = $DESCRIPTION_EN;
                    $dataheader['LINK'] = $pathmanagement . $config['file_name'] . "." . $forUpload['extension'];
                    $dataheader['USER_LOG'] = $_SESSION['username'];
                    $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
                    $where['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
                    $update_header = $this->db->where($where)->update('MS_MANAGEMENT_HEADER', $dataheader);

                    if (!$update_header) {
                        $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $dataheader['NAME'] = $NAME;
            $dataheader['POSITION_ID'] = $POSITION_ID;
            $dataheader['POSITION_EN'] = $POSITION_EN;
            $dataheader['POSITION_CATEGORY'] = $POSITION_CATEGORY;
            $dataheader['DESCRIPTION_ID'] = $DESCRIPTION_ID;
            $dataheader['DESCRIPTION_EN'] = $DESCRIPTION_EN;
            $dataheader['USER_LOG'] = $_SESSION['username'];
            $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
            $where['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
            $update_header = $this->db->where($where)->update('MS_MANAGEMENT_HEADER', $dataheader);

            if (!$update_header) {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                $this->db->trans_rollback();
                die(json_encode($JSON));
            }

        }
        $cpt = count($ID_MANAGEMENT_DETAIL);
        for ($i = 0; $i < $cpt; $i++) {
            if ($ID_MANAGEMENT_DETAIL[$i] != 'NEW') {
                $where['ID_MANAGEMENT_DETAIL'] = $ID_MANAGEMENT_DETAIL[$i];
                $datadetail['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
                $datadetail['YEAR_ID'] = $YEAR_ID[$i];
                $datadetail['YEAR_EN'] = $YEAR_EN[$i];
                $datadetail['DESCRIPTION_ID'] = $DESCRIPTION_POSITION_ID[$i];
                $datadetail['DESCRIPTION_EN'] = $DESCRIPTION_POSITION_EN[$i];
                $datadetail['DATE_LOG'] = date('Y-m-d H:i:s');
                $datadetail['USER_LOG'] = $_SESSION['username'];
                $update_detail = $this->db->where($where)->update('MS_MANAGEMENT_DETAIL', $datadetail);
                if (!$update_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }
            } else {
                $datadetail['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
                $datadetail['YEAR_ID'] = $YEAR_ID[$i];
                $datadetail['YEAR_EN'] = $YEAR_EN[$i];
                $datadetail['DESCRIPTION_ID'] = $DESCRIPTION_POSITION_ID[$i];
                $datadetail['DESCRIPTION_EN'] = $DESCRIPTION_POSITION_EN[$i];
                $datadetail['DATE_LOG'] = date('Y-m-d H:i:s');
                $datadetail['USER_LOG'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('MS_MANAGEMENT_DETAIL', $datadetail);
                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }

            }
        }
        $del = count($LIST_DELETE);
        for ($j = 0; $j < $del; $j++) {
            $run_delete = $this->db->delete('MS_MANAGEMENT_DETAIL', array('ID_MANAGEMENT_DETAIL' => $LIST_DELETE[$j]));
            if (!$run_delete) {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                $this->db->trans_rollback();
                return $JSON;
            }
        }

        if ($this->db->trans_status() === false) {
            $final_res = 'Failed: Failed to submit data';
            $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
            $this->db->trans_rollback();
            die(json_encode($JSON));

        } else {
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
            $this->db->trans_commit();
            die(json_encode($JSON));

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_MANAGEMENT_HEADER = isset($Data['ID_MANAGEMENT_HEADER']) ? $Data['ID_MANAGEMENT_HEADER'] : "";
        $param['ID_MANAGEMENT_HEADER'] = $ID_MANAGEMENT_HEADER;
        $Delete = $this->management_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_MANAGEMENT_HEADER']) ? $Data['ID_MANAGEMENT_HEADER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_MANAGEMENT_HEADER' => $id);
        $Active = $this->management_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_MANAGEMENT_HEADER']) ? $Data['ID_MANAGEMENT_HEADER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_MANAGEMENT_HEADER' => $id);
        $Active = $this->management_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_MANAGEMENT_HEADER']) ? $Data['ID_MANAGEMENT_HEADER'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_MANAGEMENT_HEADER' => $id);
        $Active = $this->management_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}