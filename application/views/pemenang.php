<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>

<style>
	ul.menu_side_left{
		list-style: none;
	}
	ul.menu_side_left li{
		padding-top: .5rem;
		padding-bottom: .2rem;
	}
	ul.menu_side_left li:hover > a{
		color: #233F8A;
		font-weight: bold;
	}
	ul.menu_side_left li a, .btn.btn-link{
		color: #414141;
		text-decoration: none;
	}
	.content-sbdk{
		max-width: 100%;
		text-align: center;
	}
	.title-sbdk{
		text-align: center;
		color: #212529;
		font-weight: bold;
		font-size: 18px;
	}
	.efektif-pertahun{
		text-align: center;
	}
	.row-page-module {
	    background-color: #f7f7f7;
	    padding: 10px;
	}
	.tbl-sbdk{
		width: 100%;
	}
	.tbl-sbdk tr td{
		text-align: center;
		background: #253D8C; 
		color: #C3D3DF;
		border: 1px solid #C3D3DF;
    	padding: 8px 0;
    	font-size: 14px;
    	/*font-weight: bold;*/
	}
	.tbl-sbdk tr.value-sbdk td{
		color: #333;
		background: #fff;
		/*font-weight: bold;*/
	}

	.garis-putus{
		border-top: 4px dashed #84B4DF;
	}

	.keterangan-sbdk{
		text-align: left;
		color: #535353;
	}

	.row-socialize{
		padding-bottom: 30px;
	}
</style>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14PX;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"> -->
			<label style="color:#707070"><?php echo $MENU_PARENT?> /</label>
		<!-- </a> / -->
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			<?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
			<div class="content-sbdk">
				<p class="title-sbdk" style="padding-top: 20px;">
					<?php echo $this->lang->line('pemenang-0') ?>
				</p>
				<table class="tbl-sbdk">
					<tr>
						<td><?php echo $this->lang->line('pemenang-1') ?></td>
						<td><?php echo $this->lang->line('pemenang-2') ?></td>
						<td><?php echo $this->lang->line('pemenang-3') ?></td>
					</tr>
					<?php
						$q = $this->db->query("SELECT * FROM MS_KESRA_WINNER WHERE STATUS = '99' ORDER BY CREATED_DATE DESC")->result();
						if(!empty($q)){
							$lang = '';
							foreach($q as $val):
					?>
								<tr class="value-sbdk">
									<td><?php echo $val->PERIODE ?></td>
									<td><?php $date = date_create($val->DATE); echo date_format($date,'d M Y'); ?></td>
									<td>
										<a href="<?php echo $val->LINK; ?>" target="_blank"><?php echo $this->lang->line('pemenang-4') ?></a>
									</td>
								</tr>
					<?php
							endforeach;
						}else{
					?>
						<tr class="value-sbdk">
							<td colspan="3"><?php echo $this->lang->line('pemenang-5') ?></td>
						</tr>
					<?php
						}
					?>
				</table>

				<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
			</div>
		</div>
	</div>
</div>