<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">User Management</a>
                <a href="#" class="breadcrumb-item active">User Group Privilege</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th>User Group Name</th>
                    <th data-orderable="false" width="200">Created Date</th>
                    <th data-orderable="false">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!-- MODAL EDIT USER GROUP PRIVILEGE -->
<div id="modalGroupPrivilegeEdit" class="modal fade" role="dialog" aria-labelledby="groupPrivilegeEditLbl2"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-dark">
            <div class="modal-header">
                <h4 class="modal-title" id="groupPrivilegeEditLbl2">Edit User Group Privilege</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

                <form id="frmsaveUGroupPrivilege">
                    <input type="hidden" name="uGroupCBID" id="uGroupCBID">
                    <!-- TREE VIEW -->
                    <div id="containerbasicTree">
                        <div id="basicTree"></div>
                    </div>
                    <!-- /TREE VIEW -->
                    <div class="form-group mb-0 mt-3" id="btnformUGroupPrivilege" style="text-align: right;">
                        <button class="btn btn-success waves-effect waves-light mr-1" type="button" id="checkallCB"
                            style="float: left"> Check/Uncheck All</button>
                        <button class="btn btn-primary waves-effect waves-light mr-1" type="submit"> Submit</button>
                        <button type="reset" class="btn btn-secondary waves-effect waves-light" data-dismiss="modal">
                            Cancel</button>
                    </div>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END MODAL EDIT USER GROUP PRIVILEGE -->

<style type="text/css">
.jstree-grid-header-regular {
    background-color: #324148 !important;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
    $.fn.modal.Constructor.prototype._enforceFocus = function() {
        modal_this = this;
        modal_this._element.focus();
    };
});

<?php if (!$authorize['is_update']) {?>
var colDef = [{
    "targets": [2, 3],
    "visible": false,
}];
<?php } else {?>
var colDef = [{
    "targets": [3],
    "visible": false,
}];
<?php }?>
var arr = ["USER_GROUP_NAME", "STATUS", "USER_GROUP_ID", "CREATED_DATE"];
var t = $("#tableData").DataTable({
    serverSide: false,
    autoWidth: false,
    ajax: {
        "url": "<?php echo base_url('user_group_p/get_data') ?>",
        "type": "POST",
        "data": {
            "arr": arr,
            "tb": "v_tblGroupPrivilege",
            "modName": "User Group Privilege"
        },
    },
    columns: [{
            "data": "USER_GROUP_NAME"
        },
        {
            "data": "CREATED_DATE"
        },
        {
            "data": "ACTION",
            "width": "10px",
            "orderable": false
        },
        {
            "data": "USER_GROUP_ID"
        }
    ],
    columnDefs: colDef,
    lengthMenu: [10, 25, 50, 100, 500],
    keys: !0,
    order: [
        [3, "desc"]
    ],
    autofill: true,
    select: true,
    responsive: true,
    buttons: true,
    length: 4,
    processing: true,
    dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',

});
$('#tableData thead th').each(function() {
    var title = $(this).text();
    if (title == "Action" || title == "Image") {

    } else if (title == "Status") {
        $(this).append(
            '<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option></select>'
        );
    } else if (title == "Created Date") {
        $(this).append(
            '<br> <div class="input-group" style="color:#333">' +
            '<span class="input-group-prepend">' +
            '<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
            "</span>" +
            '<input type="text" onchange="changeSelectDate(this)" name="created_date" id="created_date" class="form-control pickadate-selectors" placeholder="Select Date..">' +
            "</div>"
        );
        $(".pickadate-selectors").pickadate({
            selectYears: true,
            selectMonths: true,
            format: "dd mmm yyyy",
            formatSubmit: "dd mmm yyyy",
            hiddenName: true,
        });
    } else {
        $(this).append(
            '<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
        );
    }
});


t.columns().every(function() {
    var that = this;
    $('input', this.header()).on('keyup', function(e) {
        if (event.keyCode === 8) {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        } else {
            if (this.value.length >= 3) {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            }
        }
    });
});

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(8)
            .search(dis.value)
            .draw();
    } else {
        t.column(8)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(1)
            .search(dis.value)
            .draw();
    } else {
        t.column(1)
            .search(dis.value)
            .draw();
    }

}
$('.datatable-complex-header thead input').on('click', function(e) {
    e.stopPropagation();
});

//Open Edit Form2
function openEdit(id) {
    $.ajax({
        url: "<?php echo base_url('user_group_p/get_PrivilegeCb/'); ?>" + id,
        dataType: "JSON",
        type: 'GET',
        success: function(data) {

            $("#uGroupCBID").val(id);
            $("#containerbasicTree").html("<div id='basicTree'></div>");
            $("#basicTree").jstree({
                plugins: ["core", "ui", "grid", "types"],
                grid: {
                    columns: [{
                            width: 280,
                            header: "Module Menu",
                            value: "ModuleMenu",
                            title: "Module Menu"
                        },
                        {
                            width: 75,
                            header: "View",
                            value: "Read",
                            title: "Read"
                        },
                        {
                            width: 75,
                            header: "Edit",
                            value: "Edit",
                            title: "Edit"
                        },
                        {
                            width: 75,
                            header: "Add",
                            value: "Add",
                            title: "Add"
                        },
                        {
                            width: 75,
                            header: "Delete",
                            value: "Delete",
                            title: "Delete"
                        },
                        {
                            width: 75,
                            header: "Export",
                            value: "Export",
                            title: "Export"
                        },
                        {
                            width: 65,
                            header: "Import",
                            value: "Import",
                            title: "Import"
                        },
                        {
                            width: 95,
                            header: "Btn",
                            value: "Btn",
                            title: "Btn"
                        }
                    ],
                    resizable: true,
                    contextmenu: true
                },
                core: {
                    themes: {
                        responsive: !1
                    },
                    data: data.dataTree

                },
                types: {
                    default: {
                        icon: "mdi mdi-folder-star"
                    },
                    file: {
                        icon: "mdi mdi-file"
                    }
                }
            });

            $('#modalGroupPrivilegeEdit').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal(textStatus);
            document.getElementById("frmeditGroupPrivilege").reset();
        }
    });
}

// SAVE USER GROUP PRIIVILEGE MODAL
$('#frmsaveUGroupPrivilege').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        url: "<?php echo base_url('user_group_p/saveCb'); ?>",
        data: $(this).serialize(),
        dataType: "JSON",
        type: 'POST',
        success: function(data) {
            if (!(data.indexOf("Failed") != -1)) {
                swal({
                    type: 'success',
                    title: data
                });
                $('#modalGroupPrivilegeEdit').modal('hide');
            } else {
                swal({
                    type: 'error',
                    title: data
                });
                $('#modalGroupPrivilegeEdit').modal('hide');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal(textStatus);
        }
    });
});
// END SAVE USER GROUP PRIVILEGE MODAL
</script>