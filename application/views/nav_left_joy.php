<style>
	ul.menu_side_left{
		list-style: none;
		margin-bottom: 0;
	}
	ul.menu_side_left li{
		padding-top: 5px !important;
		padding-bottom: 5px !important;
	}
	ul.menu_side_left li:hover > a, ul.menu_side_left li.active > a, span.active{
		color: #233F8A;
		font-weight: bold;
	}
	ul.menu_side_left li a, .btn.btn-link{
		color: #414141;
		text-decoration: none;
	}
	#accordion .card{
		border-radius: unset;
		border: none;
	}

	.btn.mobile-parent-accordion{
		display: none !important;
	}
	button.btn.btn-link{
		text-align: left;
	}

	#accordion{
		border: 1px solid rgba(0, 0, 0, 0.125);
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		#accordion{
			display: none;
		}
		.btn.mobile-parent-accordion{
			display: flex !important;
			background: #243E8B;
			color: #fff;
			padding: 10px 20px;
			border-radius: unset;
		}
	}

	.txt-left-menu{
		font-size: 14px;
	}
</style>

<button id="btnMobParent" class="btn mobile-parent-accordion btn-block text-left d-flex align-items-center">
	<span class="mr-auto"><?php echo $MENU_CHILD; ?></span>
	<i class="fa fa-angle-down"></i>
</button>

<div id="accordion">

	<?php $idAccord = 1; ?>
	<?php foreach($contentData['menuLeft'] as $key => $value){ ?>

	  <?php if(count($value['menu_1']['menu_2']) > 0){ ?>
	  	<!-- MULTI -->
		  <div class="card <?php echo $value['menu_1']['ACTIVE_URL'] ? 'active' : '' ?>">
		    <div class="card-header p-0" id="<?php echo 'heading_'.$idAccord ?>">
		      <h5 class="mb-0">
		        <button style="width:100%;padding:5px 10px" class="btn btn-link d-flex align-items-center" data-toggle="collapse" data-target="#<?php echo 'collapse_'.$idAccord ?>" aria-expanded="true" aria-controls="<?php echo 'collapse_'.$idAccord ?>">
		          <span class="txt-left-menu mr-auto <?php echo $value['menu_1']['ACTIVE_URL'] ? 'active' : '' ?>"><?php echo $value['menu_1']['NAME'] ?></span>
		          <i class="fa fa-angle-down"></i>
		        </button>
		      </h5>
		    </div>

		    <div style="background-color: rgba(0, 0, 0, 0.03);" id="<?php echo 'collapse_'.$idAccord ?>" class="collapse <?php echo $value['menu_1']['ACTIVE_URL'] ? 'show' : '' ?>" aria-labelledby="<?php echo 'heading_'.$idAccord ?>" data-parent="#accordion">
		      <div class="card-body pt-2 pb-2">
		      	<div id="accordion2">

				<?php 
					$idAccordInc = 1;
					$idAccord2 = "ex_".$idAccord."_".$idAccordInc;
				?>
			      	<ul class="menu_side_left pl-3">
			      		<?php foreach ($value['menu_1']['menu_2'] as $key2 => $value2) { ?>

			      			<?php if(count($value2['menu_3']) > 0){ ?>
			      			<li>
			      				<div style="border:none;" class="card <?php echo $value2['ACTIVE_URL'] ? 'active' : '' ?>">
								    <div style="border:none;" class="card-header pt-0 pb-0 pl-0 pr-0" id="<?php echo 'heading_'.$idAccord2 ?>">
								      <h5 class="mb-0">
								        <button style="width:100%;text-align: left;padding:0 ;" class="btn btn-link d-flex align-items-center" data-toggle="collapse" data-target="#<?php echo 'collapse_'.$idAccord2 ?>" aria-expanded="true" aria-controls="<?php echo 'collapse_'.$idAccord2 ?>">
								          <span class="txt-left-menu mr-auto <?php echo $value2['ACTIVE_URL'] ? 'active' : '' ?>"><?php echo $value2['NAME'] ?></span>
								          <i class="fa fa-angle-down"></i>
								        </button>
								      </h5>
								    </div>

								    <div style="background-color: rgba(0, 0, 0, 0.03);" id="<?php echo 'collapse_'.$idAccord2 ?>" class="collapse <?php echo $value2['ACTIVE_URL'] ? 'show' : '' ?>" aria-labelledby="<?php echo 'heading_'.$idAccord2 ?>" data-parent="#accordion2">
								      <div class="card-body pt-0 pb-0">
								      	<ul class="menu_side_left pl-0">
								      		<?php foreach ($value2['menu_3']as $key3 => $value3) { ?>
								      				<li class="<?php echo $value3['ACTIVE_URL'] ? 'active' : '' ?>"><a class="txt-left-menu" href="<?php echo base_url($_SESSION['lang'].'/'.$value3['LINK']) ?>"><?php echo $value3['NAME'] ?></a></li>
								      		<?php } ?>
								      	</ul>
								      </div>
								    </div>
								</div>
			      			</li>
			      			<?php }else{ ?>
			      				<li class="<?php echo $value2['ACTIVE_URL'] ? 'active' : '' ?>"><a class="txt-left-menu" href="<?php echo base_url($_SESSION['lang'].'/'.$value2['LINK']) ?>"><?php echo $value2['NAME'] ?></a></li>
			      			<?php } ?>

			      		<?php } ?>
			      	</ul>
		      	</div>
		      </div>
		    </div>
		  </div>
	  <?php }else{ ?>
	  	<!-- SINGLE -->
		  <div class="card <?php echo $value['menu_1']['ACTIVE_URL'] ? 'active' : '' ?>">
		    <div class="card-header p-0" id="<?php echo 'heading_'.$idAccord ?>">
		      <h5 class="mb-0">
		        <a style="text-decoration: none;" href="<?php echo base_url($_SESSION['lang'].'/'.$value['menu_1']['LINK']) ?>">
		        	<button style="width:100%;padding:5px 10px" class="btn btn-link d-flex align-items-center">
			          <span class="txt-left-menu mr-auto <?php echo $value['menu_1']['ACTIVE_URL'] ? 'active' : '' ?>"><?php echo $value['menu_1']['NAME'] ?></span>
			        </button>
		        </a>
		      </h5>
		    </div>
		  </div>
	  <?php } ?>

	<?php $idAccord++; } ?>

	</div>

	<script>
		$("#btnMobParent").on('click', function(){
			$("#accordion").slideToggle('fast');
		});

		$(document).ready(function(){
			// $("#accordion > .card.active").addClass('to-remove');
			// $("#accordion").prepend($("#accordion > .card.active").html());
			$("#accordion").prepend($("#accordion > .card.active"));
			$("#accordion2 > ul.menu_side_left").prepend($("ul.menu_side_left > li.active"));

			// $("#accordion > .card.active").before($("#accordion > .card:nth-child(2)"));
		});
	</script>