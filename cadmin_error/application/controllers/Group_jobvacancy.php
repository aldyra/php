<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group_jobvacancy extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('group_jobvacancy_model');
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "group_jobvacancy/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['group_jobvacancy'] = $this->group_jobvacancy_model->view();
        $this->load->view('templates/view', $data);
    }
    public function add_group_jobvacancy()
    {
        $data = array();
        $data['content'] = "group_jobvacancy/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'group_jobvacancy';

        $this->load->view('templates/view', $data);
    }
    public function edit_group_jobvacancy($id)
    {
        $data = array();
        $data['content'] = "group_jobvacancy/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['group_jobvacancy'] = $this->group_jobvacancy_model->get_edit($id);
        $data['data']['back'] = base_url() . 'group_jobvacancy';

        $this->load->view('templates/view', $data);
    }
    public function get_group()
    {
        $lang = $this->input->post('lang', true);
        $this->db->order_by('NAME', 'ASC');
        $this->db->where('LANGUAGE', $lang);
        $data = $this->db->get('MS_GROUP_JOBVACANCY')->result();
        echo json_encode($data);
    }

    public function add()
    {
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";

        $param['NAME'] = $NAME;
        $param['LANGUAGE'] = $LANGUAGE;
        $param['CREATED_BY'] = $_SESSION['username'];
        $param['CREATED_DATE'] = date('Y-m-d H:i:s');
        $Insert = $this->group_jobvacancy_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));
    }
    public function update()
    {
        $ID = isset($_POST['id_group_jobvacancy']) ? $_POST['id_group_jobvacancy'] : "";
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";

        $param['ID'] = $ID;
        $param['NAME'] = $NAME;
        $param['LANGUAGE'] = $LANGUAGE;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $Update = $this->group_jobvacancy_model->updateaction($param);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->group_jobvacancy_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }

    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "0";
        $where = array('ID' => $id);
        $Active = $this->group_jobvacancy_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "88";
        $where = array('ID' => $id);
        $Active = $this->group_jobvacancy_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "99";
        $where = array('ID' => $id);
        $Active = $this->group_jobvacancy_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }

}