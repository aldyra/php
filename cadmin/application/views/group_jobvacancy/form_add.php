<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>
                LANGUAGE<span style="color:orange">*</span>
            </label>
            <select class="form-control select2modal" name="language" id="language" required="">
                <?php
$LANGUAGE = $this->config->item('LANGUAGE');
$ID_LANGUAGE = $this->config->item('ID_LANGUAGE');
$Exp_ID = explode(":", $ID_LANGUAGE);
$Exp_DESC = explode(":", $LANGUAGE);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    ?>
                <option value="<?php echo $ID ?>"><?php echo $DESC ?></option>
                <?php
}
?>
            </select>
        </div>
        <div class="form-group">
            <label>Name</label><span style="color: orange">*</span><br>
            <input type="text" name="name" class="form-control" id="name" placeholder="">

        </div>
    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>