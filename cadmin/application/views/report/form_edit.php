<?php
$param = isset($param) ? $param : "";
$ID_REPORT = isset($param['ID_REPORT']) ? $param['ID_REPORT'] : "";
$REPORT_NAME_ID = isset($param['REPORT_NAME_ID']) ? $param['REPORT_NAME_ID'] : "";
$REPORT_NAME_EN = isset($param['REPORT_NAME_EN']) ? $param['REPORT_NAME_EN'] : "";
$CATEGORY = isset($param['CATEGORY']) ? $param['CATEGORY'] : "";
$LINK_ID = isset($param['LINK_ID']) ? $param['LINK_ID'] : "";
$LINK_EN = isset($param['LINK_EN']) ? $param['LINK_EN'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <input type="hidden" id="id_report" name="id_report" value="<?php echo $ID_REPORT ?>">
        <div class="form-group">
            <label>Category</label><span style="color: orange">*</span><br>
            <select class="form-control select2modal" name="category" id="category" required="">
                <option selected disabled>Choose Category</option>
                <?php
$REPORT_ID = $this->config->item('REPORT_ID');
$REPORT_EN = $this->config->item('REPORT_EN');
$ID_CATEG_REPORT = $this->config->item('ID_CATEG_REPORT');
$Exp_REPORT_ID = explode(":", $REPORT_ID);
$Exp_REPORT_EN = explode(":", $REPORT_EN);
$Exp_ID_CATEG_REPORT = explode(":", $ID_CATEG_REPORT);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_REPORT); $i++) {
    $ID = $Exp_ID_CATEG_REPORT[$i];
    $DESC_EN = $Exp_REPORT_EN[$i];
    $DESC_ID = $Exp_REPORT_ID[$i];
    ?>
                <option value="<?php echo $ID ?>" <?php if ($ID == $CATEGORY) {echo 'selected';}?>>
                    <?php echo $DESC_EN . ' / ' . $DESC_ID ?></option>
                <?php
}
?>
            </select>
        </div>
        <div class="form-group">
            <label>Report Name</label><span style="color: orange">*</span><br>
            <input type="text" name="report_name_id" class="form-control" id="report_name_id" placeholder="" required=""
                value="<?php echo $REPORT_NAME_ID ?>">
        </div>
        <div class="form-group">
            <label>Report Name English</label><span style="color: orange">*</span><br>
            <input type="text" name="report_name_en" class="form-control" id="report_name_en" placeholder="" required=""
                value="<?php echo $REPORT_NAME_EN ?>">
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="form-group">
                    <label>Upload File Indonesia</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_id" name="file_id" class="dropify" value="<?php echo $LINK_ID ?>"
                        data-default-file="<?php echo $LINK_ID ?>" />
                    <input type="hidden" name="file_id_url" id="file_id_url" value="<?php echo $LINK_ID ?>">
                    <a href="#" onclick="openTnC('<?php echo base_url('report/tncupload') ?>')">Terms and Conditions
                        Upload</a>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="form-group">
                    <label>Upload File English</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_en" name="file_en" class="dropify" value="<?php echo $LINK_EN ?>"
                        data-default-file="<?php echo $LINK_EN ?>" />
                    <input type="hidden" name="file_en_url" id="file_en_url" value="<?php echo $LINK_EN ?>">
                    <a href="#" onclick="openTnC('<?php echo base_url('report/tncupload') ?>')">Terms and Conditions
                        Upload</a>


                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    $('.select2modal').select2({
        dropdownParent: $('#modal')
    });
    changeformfile();
});
</script>