<?php
$Log_sbdk = isset($data['log_sbdk']) ? $data['log_sbdk'] : array();
$is_read = $authorize['is_read'];
$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Log History</a>
                <a href="#" class="breadcrumb-item active">Log SBDK</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">

        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200" data-orderable="false">Type Log</th>

                    <th>Old Data</th>
                    <th>New Data</th>
                    <th>Column Data</th>
                    <th data-orderable="false" width="200">Created Date</th>
                    <th>Created By</th>
                    <?php if (!empty($is_read)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Log_sbdk as $log_sbdk) {
    $ID_LOG = isset($log_sbdk['ID_LOG']) ? $log_sbdk['ID_LOG'] : "";
    $ID_SBDK_HEADER = isset($log_sbdk['ID']) ? $log_sbdk['ID'] : "";
    $TYPE = isset($log_sbdk['TYPE']) ? $log_sbdk['TYPE'] : "";
    $OLD_DATA = isset($log_sbdk['OLD_DATA']) ? $log_sbdk['OLD_DATA'] : "";
    $NEW_DATA = isset($log_sbdk['NEW_DATA']) ? $log_sbdk['NEW_DATA'] : "";
    $COLUMN_DATA = isset($log_sbdk['COLUMN_DATA']) ? $log_sbdk['COLUMN_DATA'] : "";
    $CREATED_BY = isset($log_sbdk['CREATED_BY']) ? $log_sbdk['CREATED_BY'] : "";
    $CREATED_DATE = isset($log_sbdk['CREATED_DATE']) ? date('d M Y', strtotime($log_sbdk['CREATED_DATE'])) : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $log_sbdk;
    $enc = base64_encode(json_encode($paramaction));

    ?>
                <tr>
                    <td>
                        <?php echo $TYPE ?>
                    </td>
                    <td>
                        <?php echo $OLD_DATA ?>
                    </td>
                    <td>
                        <?php echo $NEW_DATA ?>
                    </td>
                    <td>
                        <?php echo $COLUMN_DATA ?>
                    </td>
                    <td>
                        <?php echo $CREATED_DATE ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <?php if (!empty($is_read)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo base_url("log_history/detail_sbdk/") . $ID_SBDK_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function changeSelectType(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(0)
            .search(dis.value)
            .draw();
    } else {
        t.column(0)
            .search(dis.value)
            .draw();
    }

}
</script>