<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$Url = base_url().$SiteLang."/kurs";
?>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important;"><?php echo $this->lang->line('kurs-3') ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color: #707070;"><?php echo $this->lang->line('home') ?></a> / 
		<label style="color:#707070"><?php echo $this->lang->line('kurs-3') ?></label>
	</div>
</div>

<div class="container-fluid d-padding" style="padding-bottom: 50px;">
	<div class="row d-padtop-10">
		<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			<div class="accordion" id="accordionmobile">
				<div class="card">
					<div class="card-body" id="heading" style="padding-left: 10px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #dadada;background-color: #f7f7f7;">
						<a href="<?php echo $Url ?>" style='color: #243E8B !important; font-weight: bold;'><?php echo $this->lang->line('kurs-3') ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10">
					<h4 style="font-size: 18px;"><b><?php echo $this->lang->line('kurs-5')?></b></h4>
				</div>
				<div class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12 d-padtop-10">
					<?php 
			        $i=0;
			        foreach($kurs as $row){
			            $DETAIL = $kurs[$i]['DETAIL'];
			            $EFFECTIVE_DATE = date("d F Y", strtotime($kurs[$i]['EFFECTIVE_DATE']));
			        ?>
			        	<span style="font-size: 16px;"><?php echo $this->lang->line('kurs-3') ?> <br> PT. BANK BUMI ARTA</span>
			        	<div class="row d-padtop-10">
			        		<!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12"></div> -->
			        		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
			        			<div class=" table-responsive ">
					        		<table class="table table-bordered table-striped table-kurs-page">
					        			<thead>
					        				<tr align="center">
					        					<th><?php echo $this->lang->line('kurs-0') ?></th>
					        					<th><?php echo $this->lang->line('kurs-1') ?></th>
					        					<th><?php echo $this->lang->line('kurs-2') ?></th>
					        				</tr>
					        			</thead>
					        			<tbody>
					        				<?php
					        				foreach ($DETAIL as $row2) {
								            	$CURRENCY_CODE = isset($row2['CURRENCY_CODE']) ? $row2['CURRENCY_CODE']:"";
							                    $BUY = isset($row2['BUY']) ? $row2['BUY']:"";
							                    $SELL = isset($row2['SELL']) ? $row2['SELL']:"";
							                    $LINK = isset($row2['LINK']) ? $row2['LINK']:"";
							                ?>
							                	<tr>
							                		<td align="left">
							                			<img src="<?php echo $LINK?>" style="width: 35px; height: 20px; margin-right: 20px; object-fit: fill; object-position: center; border:1px solid #ddcfcf; margin-left: 10px;"><?php echo $CURRENCY_CODE ?>
							                		</td>
					                                <td align="center"><?php echo $BUY ?></td>
					                                <td align="center"><?php echo $SELL ?></td>
							                	</tr>
							                <?php
							                }
							                ?>
					        			</tbody>
					        		</table>
				        		</div>
			        		</div>
			        	</div>
			        	<div class="row-effective">
			        		<?php echo $this->lang->line('kurs-9') ?> <?php echo convertDateLang($EFFECTIVE_DATE, 'dmy') ?>
			        	</div>
			    	<?php
			    	$i++;
			    	} 
			    	?>
				</div>
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10" style="text-align: justify;">
					<b><?php echo $this->lang->line('kurs-6')?> :</b><br>
					<ul>
						<li><?php echo $this->lang->line('kurs-7')?></li>
						<li><?php echo $this->lang->line('kurs-8')?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	table.table-kurs-page thead tr th{
		font-size: 14px;
		background-color: #253D8C;
		color: white;
	}

	table.table-kurs-page tbody tr td{
		font-size: 14px;
		background-color: white;
	}

	.row{
		font-size: 14px;
	}

	@media all and (min-width: 992px) {
		#accordionmobile{
			display: block;
		}

		.row-effective{
			text-align: right;
		}
	}

	@media all and (max-width: 992px) {
		#accordionmobile{
			display: none;
		}

		.row-effective{
			text-align: center;
		}
	}
</style>