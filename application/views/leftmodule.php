<div class="accordion" id="accordionparent">
	<div class="card">
		<div class="card-body" style="padding: 0px; background-color: #28448f; color: white;">
			<?php 
			if(!empty($data['content']['MENU_PARENT'])){
			?>
			<button id="btnMobParent" class="btn mobile-parent-accordion btn-block text-left d-flex align-items-center">
				<span class="mr-auto"><?php echo $data['content']['MENU_PARENT'] ?></span>
				<i class="fa fa-angle-down"></i>
			</button>
			<?php } ?>

			<div id="accordion_parent">
				<div class="accordion" id="accordion0">
					<div class="card" id="cardPrepend">
						<?php
						$i = 0;
						foreach($data['leftmodule'] as $CHILD1){
							if(empty(sizeof($CHILD1['CHILD']))){
								$UrlChild = base_url().$_SESSION['site_lang']."/".$CHILD1['LINK'];
							}else{
								$UrlChild = "javascript:void(0)";
							}

							if($data['controller'] == $CHILD1['LINK']){
								$active = "style='color: #243E8B !important; font-weight: bold;'";
							}else{
								$active = "style='color: #707070;'";
							}
						?>
							<div class="card-body" id="heading<?php echo $i?>" style="padding-left: 10px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #dadada;	background-color: #f7f7f7;">
								<a href="<?php echo $UrlChild ?>" <?php echo $active ?>><?php echo $CHILD1['NAME'];?></a>
								<?php 
								if(!empty(sizeof($CHILD1['CHILD']))){
								?>
								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>" style="position: absolute; right: 0; padding-top: 0;">
									<i class="fas fa-chevron-down" style="font-size: 14px; color: #707070;"></i>
								</button>
								<?php } ?>
							</div>

							<div id="collapse<?php echo $i ?>" class="collapse" aria-labelledby="heading<?php echo $i?>" data-parent="#accordion0">
								<div class="accordion" id="accordion1">
									<div class="card" style="border: none">
										<?php
										$j = 0;
										foreach($CHILD1['CHILD'] AS $CHILD2){
				                    		if(empty(sizeof($CHILD2['CHILD']))){
												$UrlChild2 = base_url().$_SESSION['site_lang']."/".$CHILD2['LINK'];
											}else{
												$UrlChild2 = "javascript:void(0)";
											}
											
											$active = "style='margin-bottom: 0; font-size: 14px;'";
				                    	?>
											<div class="card-body" id="heading<?php echo $i.$j ?>" style="padding-left: 30px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px;border: none; background-color: #f7f7f7;">
												<a href="<?php echo $UrlChild2 ?>" style="color: #707070; "><?php echo $CHILD2['NAME'];?></a>
												<?php 
												if(!empty(sizeof($CHILD2['CHILD']))){
												?>
												<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i.$j ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j ?>" style="position: absolute; right: 0; padding-top: 0;">
													<i class="fas fa-chevron-down" style="font-size: 14px;color: #707070;"></i>
												</button>
												<?php } ?>
											</div>
											<div id="collapse<?php echo $i.$j ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j ?>" data-parent="#accordion1">
												<div class="accordion" id="accordion2">
													<div class="card" style="border: none">
														<?php
														$k = 0;
														foreach($CHILD2['CHILD'] AS $CHILD3){
								                    		if(empty(sizeof($CHILD3['CHILD']))){
																$UrlChild3 = base_url().$_SESSION['site_lang']."/".$CHILD3['LINK'];
															}else{
																$UrlChild3 = "javascript:void(0)";
															}
															
															$active = "style='margin-bottom: 0; font-size: 14px;'";
								                    	?>
								                    		<div class="card-body" id="heading<?php echo $i.$j.$k ?>" style="padding-left: 50px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; border: none; background-color: #f7f7f7;">
																<a  href="<?php echo $UrlChild3 ?>" style="color: #707070; "><?php echo $CHILD3['NAME'];?></a>
																<?php 
																if(!empty(sizeof($CHILD3['CHILD']))){
																?>
																<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i.$j.$k ?>" aria-expanded="true" aria-controls="collapse<?php echo $i.$j.$k ?>" style="position: absolute; right: 0; padding-top: 0;">
																	<i class="fas fa-chevron-down" style="font-size: 14px; color: #707070;"></i>
																</button>
																<?php } ?>
															</div>
															<div id="collapse<?php echo $i.$j.$k ?>" class="collapse" aria-labelledby="heading<?php echo $i.$j.$k ?>" data-parent="#accordionq">
																tes
															</div>
								                    	<?php 
								                    	$k++;
									                    }
									                    ?>
													</div>
												</div>
											</div>
										<?php 
										$j++;
										} 
										?>
									</div>
								</div>
							</div>
						<?php 
						$i++;
						} 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.btn.mobile-parent-accordion{
		display: flex !important;
		background: #243E8B;
		color: #fff;
		padding: 10px 20px;
		border-radius: unset;
	}

	

	@media all and (min-width: 992px) {
		#btnMobParent{
			display: none !important;
		}
	}

	@media all and (max-width: 992px) {
		#accordion_parent{
			display: none;
		}
	}
</style>

<script type="text/javascript">
	$("#cardPrepend").prepend($("#cardPrepend > .card-body.active"));
	$("#cardPrepend > .card-body.active > .collapse.show > .accordion > .card").prepend($("#cardPrepend > .card-body.active .card > .card-body.active"));

	$("#cardPrepend > .card-body.active > a").css("color", "#233F8A");
	$("#cardPrepend > .card-body.active > a").css("font-weight", "bold");

	$("#btnMobParent").on('click', function(){
		$("#accordion_parent").slideToggle('fast');
	});
</script>