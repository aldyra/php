<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{	
		$where = array();
		$post['branch'] = $this->Main_model->branch($where);

		$post['branch_head'] = $this->Main_model->branchhead('1');
		$post['branchall'] = $this->Main_model->branchhead();

		$CATEGORY1 = array();
		$CATEGORY2 = array();
		$CATEGORY3 = array();
		$CATEGORY4 = array();
		$CATEGORY5 = array();

		foreach($post['branchall'] as $row){
			$CATEGORY = isset($row['CATEGORY']) ? $row['CATEGORY']:"";
			if($CATEGORY == "1"){
				$CATEGORY1[] = $row;
			}elseif($CATEGORY == "2"){
				$CATEGORY2[] = $row;
			}elseif($CATEGORY == "3"){
				$CATEGORY3[] = $row;
			}elseif($CATEGORY == "4"){
				$CATEGORY4[] = $row;
			}elseif($CATEGORY == "5"){
				$CATEGORY5[] = $row;
			}
		}

		$CTG['CATEGORY1'] = sizeof($CATEGORY1);
		$CTG['CATEGORY2'] = sizeof($CATEGORY2);
		$CTG['CATEGORY3'] = sizeof($CATEGORY3);
		$CTG['CATEGORY4'] = sizeof($CATEGORY4);
		$CTG['CATEGORY5'] = sizeof($CATEGORY5);
		$post['category'] = $CTG;

		$getprovide = $this->providedata();
		$post['content'] = isset($getprovide['content']) ? $getprovide['content']:array();
		$post['leftmodule'] = isset($getprovide['leftmodule']) ? $getprovide['leftmodule']:array();
		$post['controller'] = $this->lang->line('cabang-0');

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['Content'] = "cabang";
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
	}

	public function view()
	{
		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		$PROVINCE = isset($_POST['province']) ? $_POST['province']:"";
		$CITYKAB = isset($_POST['citykab']) ? $_POST['citykab']:"";

		$ExpProvince = explode("-", $PROVINCE);
		$ExpCity = explode("-", $CITYKAB);
		$ID_PROVINCE = $ExpProvince[0];
		$ID_CITY = $ExpCity[0];

		$NAME_PROVINCE = isset($ExpProvince[1]) ? $ExpProvince[1]:"";
		$NAME_CITY = isset($ExpCity[1]) ? $ExpCity[1]:"";

		$where['PROVINCE'] = $ID_PROVINCE;
		$where['CITYKAB'] = $ID_CITY;
		$branch = $this->Main_model->branch($where);

		if(empty(sizeof($branch))){
			$Url = base_url().$SiteLang."/".$this->lang->line('cabang-0');
			die(json_encode(array("ErrorCode"=>"EC:001A", "ErrorMsg"=>"Data not available!", "Url"=>strtolower($Url))));
		}

		$Url = base_url().$SiteLang."/".$this->lang->line('cabang-0')."/".$this->lang->line('cabang-1')."/".slugify($NAME_PROVINCE)."/".slugify($NAME_CITY);
		die(json_encode(array("ErrorCode"=>"EC:0000", "ErrorMsg"=>"Success", "Url"=>strtolower($Url))));
	}

	public function location($province, $city)
    {
    	$getprovince = $this->Main_model->province();
		$idprovince = getIdFromSlug($province, 'PROVINCE_NAME', $getprovince);

		$getcity = $this->Main_model->city();
		$idcity = getIdFromSlug($city, 'CITY_KAB_NAME', $getcity);

		$where['PROVINCE'] = $idprovince;
		$where['CITYKAB'] = $idcity;
		$post['branch'] = $this->Main_model->branch($where);

		$post['branch_head'] = $this->Main_model->branchhead('1');
		$post['branchall'] = $this->Main_model->branchhead();

		$getprovide = $this->providedata();
		$post['content'] = isset($getprovide['content']) ? $getprovide['content']:array();
		$post['leftmodule'] = isset($getprovide['leftmodule']) ? $getprovide['leftmodule']:array();
		$post['controller'] = $this->lang->line('cabang-0');

		$CATEGORY1 = array();
		$CATEGORY2 = array();
		$CATEGORY3 = array();
		$CATEGORY4 = array();
		$CATEGORY5 = array();

		foreach($post['branchall'] as $row){
			$CATEGORY = isset($row['CATEGORY']) ? $row['CATEGORY']:"";
			if($CATEGORY == "1"){
				$CATEGORY1[] = $row;
			}elseif($CATEGORY == "2"){
				$CATEGORY2[] = $row;
			}elseif($CATEGORY == "3"){
				$CATEGORY3[] = $row;
			}elseif($CATEGORY == "4"){
				$CATEGORY4[] = $row;
			}elseif($CATEGORY == "5"){
				$CATEGORY5[] = $row;
			}
		}

		$CTG['CATEGORY1'] = sizeof($CATEGORY1);
		$CTG['CATEGORY2'] = sizeof($CATEGORY2);
		$CTG['CATEGORY3'] = sizeof($CATEGORY3);
		$CTG['CATEGORY4'] = sizeof($CATEGORY4);
		$CTG['CATEGORY5'] = sizeof($CATEGORY5);
		$post['category'] = $CTG;

		$params['PROVINCE'] = $province;
		$params['CITYKAB'] = $city;
		$params['ID_PROVINCE'] = $idprovince;
		$params['ID_CITY'] = $idcity;
		$post['param'] = $params;

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['Content'] = "cabang";
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
    }

    public function providedata()
    {
    	$controller = $this->lang->line('cabang-0');
		$GetModule = $this->Module_model->getmodule($controller);
		$PARENT = isset($GetModule['PARENT']) ? $GetModule['PARENT']:"";
		$MENU_PARENT = isset($GetModule['MENU_PARENT']) ? $GetModule['MENU_PARENT']:"";

		if(sizeof($GetModule) > 0){
			$ID_MENU = isset($GetModule['ID']) ? $GetModule['ID']:"";
			$Getleftmodule = $this->Module_model->getleftmodule($MENU_PARENT);
			$post['leftmodule'] = $Getleftmodule;

			$MENU_PARENT = isset($GetModule['PARENT']['NAME']) ? $GetModule['PARENT']['NAME']:"";
			$MENU_PARENT_LINK = isset($GetModule['PARENT']['LINK']) ? $GetModule['PARENT']['LINK']:"";
			$MENU_CHILD = isset($GetModule['NAME']) ? $GetModule['NAME']:"";
			$MENU_CHILD_LINK = isset($GetModule['LINK']) ? $GetModule['LINK']:"";

			$content['MENU_PARENT'] = $MENU_PARENT;
			$content['MENU_PARENT_LINK'] = $MENU_PARENT_LINK;
			$content['MENU_CHILD'] = $MENU_CHILD;
			$content['MENU_CHILD_LINK'] = $MENU_CHILD_LINK;
			$post['content'] = $content;

			return $post;
		}
    }
}