<?php
$data = isset($data) ? $data : "";
$ID = isset($data['content']->ID) ? $data['content']->ID : "";
$ID_MN = isset($data['content']->ID_MENU) ? $data['content']->ID_MENU : "";
$TITLE_ID = isset($data['content']->TITLE_ID) ? $data['content']->TITLE_ID : "";
$TITLE_EN = isset($data['content']->TITLE_EN) ? $data['content']->TITLE_EN : "";
$DESCRIPTION_ID = isset($data['content']->DESCRIPTION_ID) ? $data['content']->DESCRIPTION_ID : "";
$DESCRIPTION_EN = isset($data['content']->DESCRIPTION_EN) ? $data['content']->DESCRIPTION_EN : "";
$STATUS = isset($data['content']->STATUS) ? $data['content']->STATUS : "";
$CREATED_BY = isset($data['content']->CREATED_BY) ? $data['content']->CREATED_BY : "";
$CREATED_DATE = isset($data['content']->CREATED_DATE) ? $data['content']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}

$menu = isset($data['menu']) ? $data['menu'] : array();
$back = isset($data['back']) ? $data['back'] : '';
$is_update = $authorize['is_update'];
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Content</a>
                <a href="#" class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Content</span> - Detail
                <small><?php echo $TITLE_ID . ' - ' . $TITLE_EN ?></small>

            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="mb-4">
                <h4 class="font-weight-semibold mb-1">
                    <a class="text-default"><?php echo $TITLE_ID ?></a>
                </h4>

                <ul class="list-inline list-inline-dotted text-muted mb-3">
                    <li class="list-inline-item">By <?php echo $CREATED_BY ?></li>
                    <li class="list-inline-item"><?php echo convertDate($CREATED_DATE) ?></li>
                    <li class="list-inline-item"><?php echo $STS ?></li>

                </ul>

                <div class="mb-3" style="overflow-x:auto">
                    <?php echo $DESCRIPTION_ID ?>
                </div>

            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="mb-4">
                <h4 class="font-weight-semibold mb-1">
                    <a class="text-default"><?php echo $TITLE_EN ?></a>
                </h4>

                <ul class="list-inline list-inline-dotted text-muted mb-3">
                    <li class="list-inline-item">By <a class="text-muted"><?php echo $CREATED_BY ?></a></li>
                    <li class="list-inline-item"><?php echo convertDate($CREATED_DATE) ?></li>
                    <li class="list-inline-item"><?php echo $STS ?></li>
                </ul>

                <div class="mb-3" style="overflow-x:auto">
                    <?php echo $DESCRIPTION_EN ?>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
$('video').attr({
    'controls': 'controls'
});
$('video').css({
    'width': '100%'
});
</script>