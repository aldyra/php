<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kurs extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('kurs_model');
        $this->load->model('currency_model');

    }

    public function index()
    {
        $data = array();
        $data['content'] = "kurs/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['kurs'] = $this->kurs_model->view();

        $this->load->view('templates/view', $data);
    }
    public function add_kurs()
    {
        $data = array();
        $data['content'] = "kurs/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'kurs';
        $data['data']['currency'] = $this->currency_model->get_currency();

        $this->load->view('templates/view', $data);
    }
    public function get_kurs_detail($id)
    {
        $this->db->select('A.*, C.CURRENCY_NAME, C.CURRENCY_CODE');
        $this->db->from('TR_KURS_DETAIL A');
        $this->db->join('TR_KURS_HEADER B ', 'A.ID_KURS_HEADER = B.ID_KURS_HEADER', 'left');
        $this->db->join('MS_CURRENCY C', ' A.ID_CURRENCY = C.ID_CURRENCY', 'left');
        $this->db->where('B.ID_KURS_HEADER', $id);
        $this->db->order_by('C.ID_CURRENCY', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }
    public function edit_kurs($id)
    {
        $data = array();
        $data['content'] = "kurs/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['kurs'] = $this->kurs_model->get_edit($id);
        $data['data']['detail'] = $this->get_kurs_detail($id);
        $data['data']['back'] = base_url() . 'kurs';
        $this->load->view('templates/view', $data);
    }
    public function detail_kurs($id)
    {
        $data = array();
        $data['content'] = "kurs/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['kurs'] = $this->kurs_model->get_edit($id);
        $data['data']['detail'] = $this->get_kurs_detail($id);
        $data['data']['back'] = base_url() . 'kurs';
        $this->load->view('templates/view', $data);
    }

    public function add()
    {
        // var_dump($_POST);exit();
        // var_dump($_FILES);exit();
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d H:i:s', strtotime($_POST['effective_date'] . $_POST['time'])) : "";
        $DESCRIPTION = "Exchange Rates | Last Updated " . date("Y-m-d H:i:s");
        //detail
        $ID_CURRENCY = isset($_POST['id_currency']) ? $_POST['id_currency'] : "";
        $BUY = isset($_POST['buy']) ? $_POST['buyV'] : "";
        $SELL = isset($_POST['sell']) ? $_POST['sellV'] : "";

        $this->db->trans_begin();

        $dataheader['DESCRIPTION'] = $DESCRIPTION;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['CREATED_BY'] = $_SESSION['username'];
        $insert_header = $this->db->insert('TR_KURS_HEADER', $dataheader);
        $ID_KURS_HEADER = $this->db->insert_id();

        if (!$insert_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));

        } else {
            $detail = count($ID_CURRENCY);
            for ($i = 0; $i < $detail; $i++) {
                $datadetail['ID_KURS_HEADER'] = $ID_KURS_HEADER;
                $datadetail['ID_CURRENCY'] = $ID_CURRENCY[$i];
                $datadetail['BUY'] = $BUY[$i];
                $datadetail['SELL'] = $SELL[$i];
                $datadetail['CREATED_BY'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('TR_KURS_DETAIL', $datadetail);
                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }

            }

            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }

    public function view()
    {
        $GetData = $this->kurs_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        // var_dump($_POST);
        // var_dump($_FILES);exit();

        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d H:i:s', strtotime($_POST['effective_date'] . $_POST['time'])) : "";
        $ID_KURS_HEADER = isset($_POST['id_kurs_header']) ? $_POST['id_kurs_header'] : "";

        $DESCRIPTION = "Exchange Rates | Last Updated " . date("Y-m-d H:i:s");
        //detail
        $ID_KURS_DETAIL = isset($_POST['id_kurs_detail']) ? $_POST['id_kurs_detail'] : "";
        $ID_CURRENCY = isset($_POST['id_currency']) ? $_POST['id_currency'] : "";
        $BUY = isset($_POST['buyV']) ? $_POST['buyV'] : "";
        $SELL = isset($_POST['sellV']) ? $_POST['sellV'] : "";

        $this->db->trans_begin();

        $dataheader['DESCRIPTION'] = $DESCRIPTION;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['USER_LOG'] = $_SESSION['username'];
        $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
        $where['ID_KURS_HEADER'] = $ID_KURS_HEADER;
        $update_header = $this->db->where($where)->update('TR_KURS_HEADER', $dataheader);

        if (!$update_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        }

        $cpt = count($ID_KURS_DETAIL);
        for ($i = 0; $i < $cpt; $i++) {
            $where['ID_KURS_DETAIL'] = $ID_KURS_DETAIL[$i];
            $datadetail['ID_KURS_HEADER'] = $ID_KURS_HEADER;
            $datadetail['ID_CURRENCY'] = $ID_CURRENCY[$i];
            $datadetail['BUY'] = $BUY[$i];
            $datadetail['SELL'] = $SELL[$i];

            $datadetail['DATE_LOG'] = date('Y-m-d H:i:s');
            $datadetail['USER_LOG'] = $_SESSION['username'];
            $update_detail = $this->db->where($where)->update('TR_KURS_DETAIL', $datadetail);
            if (!$update_detail) {
                $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                $this->db->trans_rollback();
                die(json_encode($JSON));
            }
        }

        if ($this->db->trans_status() === false) {
            $final_res = 'Failed: Failed to submit data';
            $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
            $this->db->trans_rollback();
            die(json_encode($JSON));

        } else {
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
            $this->db->trans_commit();
            die(json_encode($JSON));

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_KURS_HEADER = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";
        $param['ID_KURS_HEADER'] = $ID_KURS_HEADER;
        $Delete = $this->kurs_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_KURS_HEADER' => $id);
        $Active = $this->kurs_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_KURS_HEADER' => $id);
        $Active = $this->kurs_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";

        $data['STATUS'] = "99";
        $data['APPROVED_BY'] = $_SESSION['username'];
        $data['APPROVED_DATE'] = date('Y-m-d H:i:s');
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_KURS_HEADER' => $id);
        $Active = $this->kurs_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $dataTransaction = array(
            array(
                'ID' => $id,
                'MODULE' => 'KURS',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => date('Y-m-d H:i:s'),
                'COLUMN_DATA' => 'APPROVED_DATE',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
            array(
                'ID' => $id,
                'MODULE' => 'KURS',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => $_SESSION['username'],
                'COLUMN_DATA' => 'APPROVED_BY',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
        );
        $InsertTransaction = $this->db->insert_batch('LOG_TRANSACTION', $dataTransaction);
        if (!$InsertTransaction) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }

        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Approved");
        die(json_encode($JSON));
    }
}