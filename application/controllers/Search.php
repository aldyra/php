<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Search_model');
    }

	public function index()
	{	
		$REQUEST_URI = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']:"";
		if (strpos($REQUEST_URI, '/en/') !== false) {
		    $_SESSION['site_lang'] = "en";
		}elseif(strpos($REQUEST_URI, '/id/') !== false){
			$_SESSION['site_lang'] = "id";
		}
		
		$Search = isset($_GET['search']) ? $_GET['search']:"";
		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";

		$Url = base_url().$SiteLang."/".$this->lang->line('search-0')."/".$this->lang->line('search-1')."/".slugify($Search);

		redirect($Url);
	}

	public function keywords($keywords)
	{
		$REQUEST_URI = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']:"";
		if (strpos($REQUEST_URI, '/en/') !== false) {
		    $_SESSION['site_lang'] = "en";
		}else if(strpos($REQUEST_URI, '/id/') !== false){
			$_SESSION['site_lang'] = "id";
		}

		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";

		$keywords = str_replace("-", " ", $keywords);
		$post['news'] = $this->Search_model->getmodule($keywords);
		// $post['news'] = $this->Search_model->getcontent($keywords);
		$post['video'] = $this->Search_model->getvideo($keywords);
		$post['keywords'] = $keywords;

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['Content'] = "search";
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
	}
}