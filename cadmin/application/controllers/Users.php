<?php
defined('BASEPATH') or exit('No direct script access allowed');
include APPPATH . 'third_party/ssp.php';

class Users extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('usergroup_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "users/view";
        $data['authorize'] = $this->data['authorize'];
        $data['group'] = $this->get_group();

        $this->load->view('templates/view', $data);
    }

    public function get_data()
    {
        header('Content-Type: application/json');
        $post = $this->input->post(null, true);
        $col = implode(',', $post['arr']);
        $getAction = $this->data['authorize'];

        if (isset($post['where'])) {
            $where = $post['where'];
        } else {
            $where = null;
        }

        $res = $this->user_model->select_col($col, $getAction, $where);
        $tes = json_decode($res);

        foreach ($tes->data as $row) {
            // convert date
            $row->CREATED_DATE = convertDate($row->CREATED_DATE);
            // convert date
            switch ($row->STATUS) {
                case '0':
                    $row->STS = '0';
                    $row->STATUS = "<span class='badge badge-danger'>Inactive</span>";
                    $row->ACTION = str_replace(array('$icon', '$status'), array('icon-check', 'Active'), $row->ACTION);
                    break;
                case '88':
                    $row->STS = '88';
                    $row->STATUS = "<span class='badge badge-success'>Active</span>";
                    $row->ACTION = str_replace(array('$icon', '$status'), array('icon-cross', 'Inactive'), $row->ACTION);
                    break;
                case '99':
                    $row->STS = '99';
                    $row->STATUS = "<span class='badge badge-primary'>Publish</span>";
                    $row->ACTION = str_replace(array('$icon', '$status'), array('icon-cross', 'Inactive'), $row->ACTION);
                    break;
            }

        }

        echo json_encode($tes);
    }
    public function get_data_user()
    {
        $table = "(SELECT *, CONCAT('','') as ACTION  FROM MS_USERS ORDER BY CREATED_DATE DESC)temp";

        // Primary key of table
        $primaryKey = $col[0];

        for ($i = 0; $i < sizeof($col); $i++) {
            $columns[$i]['db'] = $col[$i];
            $columns[$i]['dt'] = $i;
        }

        $columns[$i]['db'] = 'ACTION';
        $columns[$i]['dt'] = $i;

        echo json_encode(
            SSP::simple($_GET, $this->config->item('dbss'), $table, $primaryKey, $columns)
        );
    }

    public function get_group()
    {
        // $data_group = $this->usergroup_model->get_by(
        //     array('STATUS' => '88'), null, null, false, null
        // );
        $data_group = $this->usergroup_model->get();
        $result = array();
        foreach ($data_group as $data) {
            $param['USER_GROUP_NAME'] = $data->USER_GROUP_NAME;
            $param['ID'] = $data->ID;
            $result[] = $param;
        }
        return $result;
    }

    public function get_edit($id)
    {
        $res['profile'] = $this->user_model->get($id);

        $data_group = $this->usergroup_model->get();
        $res['group'] = '';
        foreach ($data_group as $data) {
            if ($res['profile']->GROUP_ID == $data->ID) {
                $res['group'] .= '<option selected value="' . $data->ID . '">' . $data->USER_GROUP_NAME . '</option>';
            } else {
                $res['group'] .= '<option value="' . $data->ID . '">' . $data->USER_GROUP_NAME . '</option>';
            }

        }

        echo json_encode($res);
    }

    public function create_action()
    {
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Form not found!");
        if ($this->data['authorize']['is_create']) {
            $cek = $this->user_model->cekDuplicate($this->input->post('username', true), $this->input->post('email', true));
            $post = $this->input->post(null, true);

            $this->form_validation->set_rules($this->user_model->rules);

            if ($this->form_validation->run() == false) {
                $ErrorMessage = "Failed: " . validation_errors();
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else if ((!_noSpecChar($post['username'])) || (!_noSpecChar($post['name']))) {
                $ErrorMessage = "Failed: Username or Name cannot contain any special characters.";
                $JSON = array("ErrorCode" => "EC:001B", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else if (!validateEmail($post['email'])) {
                $ErrorMessage = "Failed: Email should be a valid email.";
                $JSON = array("ErrorCode" => "EC:001C", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else if (!_passRules($post['password'])) {
                $ErrorMessage = "Failed: Password not valid.";
                $JSON = array("ErrorCode" => "EC:001D", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else {
                $hash = generateRandomString();
                $password = md5($this->input->post('password', true) . $hash);

                if ($cek) {
                    $datainsert = array(
                        'USERNAME' => $this->input->post('username', true),
                        'NAME' => $this->input->post('name', true),
                        'EMAIL' => $this->input->post('email', true),
                        'POSITION' => $this->input->post('position', true),
                        'GROUP_ID' => $this->input->post('group_user', true),
                        'PASSWORD' => $password,
                        'HASH' => $hash,
                        'CREATED_BY' => $_SESSION['username'],
                        'CREATED_DATE' => date('Y-m-d H:i:s'),
                        'LAST_UPDATE_PASSWORD' => date('Y-m-d H:i:s'),
                    );
                    // insert user table
                    $id_group = $this->user_model->insert($datainsert);

                    $id_group ? $ErrorMessage = "Success" : $ErrorMessage = "Failed";
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => $ErrorMessage);
                    die(json_encode($JSON));
                } else {
                    $ErrorMessage = "Failed: Duplicate Username or Email";
                    $JSON = array("ErrorCode" => "EC:001E", "ErrorMessage" => $ErrorMessage);
                    die(json_encode($JSON));
                }
            }
        } else {
            $ErrorMessage = "Failed: You do not have access to data creation.";
            $JSON = array("ErrorCode" => "EC:001F", "ErrorMessage" => $ErrorMessage);
            die(json_encode($JSON));
        }

        die(json_encode($JSON));
    }

    public function update_action()
    {
        if ($this->data['authorize']['is_update']) {
            $id = $this->input->post('IDUser', true);
            $data_user = $this->user_model->get_by(array('ID' => $id), 1, null, true);
            $post = $this->input->post(null, true);

            $this->form_validation->set_rules($this->user_model->rules);
            if ($this->form_validation->run() == false) {
                $ErrorMessage = "Failed: " . validation_errors();
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else if ((!_noSpecChar($post['username'])) || (!_noSpecChar($post['name']))) {
                $ErrorMessage = "Failed: Username or Name cannot contain any special characters.";
                $JSON = array("ErrorCode" => "EC:001B", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else if (!validateEmail($post['email'])) {
                $ErrorMessage = "Failed: Email should be a valid email.";
                $JSON = array("ErrorCode" => "EC:001C", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else {
                if ($this->input->post('password', true) == "") {
                    $dataupdate = array(
                        'USERNAME' => $this->input->post('username', true),
                        'NAME' => $this->input->post('name', true),
                        'EMAIL' => $this->input->post('email', true),
                        'POSITION' => $this->input->post('position'),
                        'GROUP_ID' => $this->input->post('group_user'),
                        'DATE_LOG' => date('Y-m-d H:i:s'),
                        'USER_LOG' => $_SESSION['username'],
                        'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                    );
                } else {
                    $hash = generateRandomString();
                    $password = md5($this->input->post('password', true) . $hash);
                    $dataupdate = array(
                        'USERNAME' => $this->input->post('username', true),
                        'NAME' => $this->input->post('name', true),
                        'EMAIL' => $this->input->post('email', true),
                        'POSITION' => $this->input->post('position'),
                        'GROUP_ID' => $this->input->post('group_user'),
                        'PASSWORD' => $password,
                        'HASH' => $hash,
                        'LAST_UPDATE_PASSWORD' => date('Y-m-d H:i:s'),
                        'DATE_LOG' => date('Y-m-d H:i:s'),
                        'USER_LOG' => $_SESSION['username'],
                        'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                    );
                }

                if (isset($dataupdate['PASSWORD'])) {
                    $new_pass = $post['password'];
                    if (md5($new_pass . $data_user->HASH) == $data_user->PASSWORD) {
                        $ErrorMessage = 'Failed: The new password cannot be the same as the old password.';
                        $JSON = array("ErrorCode" => "EC:001D", "ErrorMessage" => $ErrorMessage);
                        die(json_encode($JSON));
                    } else if (!_passRules($post['password'])) {
                        $ErrorMessage = "Failed: Password not valid.";
                        $JSON = array("ErrorCode" => "EC:001E", "ErrorMessage" => $ErrorMessage);
                        die(json_encode($JSON));
                    } else {
                        $cek = $this->user_model->cekDuplicate($this->input->post('username', true), $this->input->post('email', true), $id);
                        if ($cek) {
                            $this->user_model->update($dataupdate, array('ID' => $id));
                            $ErrorMessage = "Success";
                            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => $ErrorMessage);
                            die(json_encode($JSON));
                        } else {
                            $ErrorMessage = "Failed: Duplicate Username or Email";
                            $JSON = array("ErrorCode" => "EC:001F", "ErrorMessage" => $ErrorMessage);
                            die(json_encode($JSON));
                        }
                    }
                } else {
                    $cek = $this->user_model->cekDuplicate($this->input->post('username', true), $this->input->post('email', true), $id);
                    if ($cek) {
                        // die(json_encode($id));
                        $this->user_model->update($dataupdate, array('ID' => $id));
                        $ErrorMessage = "Success";
                        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => $ErrorMessage);
                        die(json_encode($JSON));
                    } else {
                        $ErrorMessage = "Failed: Duplicate Username or Email";
                        $JSON = array("ErrorCode" => "EC:001H", "ErrorMessage" => $ErrorMessage);
                        die(json_encode($JSON));
                    }
                }
            }

        } else {
            $ErrorMessage = "Failed: You do not have access to data updates.";
            $JSON = array("ErrorCode" => "EC:001J", "ErrorMessage" => $ErrorMessage);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Process not found!");
        die(json_encode($JSON));

    }

    public function statusChg($id)
    {
        if ($this->data['authorize']['is_update']) {
            $array_id = array('ID' => $id);
            $get_data = $this->user_model->get($id);

            if ($get_data->STATUS == '88') {
                $array_data = array(
                    'STATUS' => '0',
                    'DATE_LOG' => date('Y-m-d H:i:s'),
                    'USER_LOG' => $_SESSION['username'],
                    'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                );
                $res = 'Success: Status deactivated';
            } else {
                $array_data = array(
                    'STATUS' => '88',
                    'DATE_LOG' => date('Y-m-d H:i:s'),
                    'USER_LOG' => $_SESSION['username'],
                    'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                );
                $res = 'Success: Status activated!';
            }

            $this->user_model->update($array_data, $array_id);
        } else {
            $res = "Failed: You do not have access to update data.";
        }

        echo json_encode($res);
    }

    public function delete_action($id)
    {
        if ($this->data['authorize']['is_delete']) {
            $del = $this->user_model->delete_row($id);
            if ($del) {
                $res = "Success delete data!";
            } else {
                $res = "Failed.";
            }

        } else {
            $res = 'Failed: You do not have access to delete data.';
        }

        echo json_encode($res);
    }
    public function update_profile()
    {
        if ($this->data['authorize']['is_update']) {

            $id = $this->input->post('IDchProfile', true);
            $cekduplicate = $this->user_model->cekDuplicate($_SESSION['username'], $this->input->post('chProfileemailAddress', true), $id);
            if ($cekduplicate) {
                $dataupdate = array(
                    'NAME' => strtoupper($this->input->post('chProfilenamee', true)),
                    'EMAIL' => strtoupper($this->input->post('chProfileemailAddress', true)),
                    'POSITION' => strtoupper($this->input->post('chProfilepositionUser', true)),
                    'DATE_LOG' => date('Y-m-d H:i:s'),
                    'USER_LOG' => $_SESSION['username'],
                );

                $res = $this->user_model->update_action($dataupdate, $id);
                if ($res == "1 Affected Rows") {
                    $_SESSION['name'] = $dataupdate['NAME'];
                    $_SESSION['email'] = $dataupdate['EMAIL'];
                    $_SESSION['position'] = $dataupdate['POSITION'];
                }
            } else {
                $res = "Failed: Duplicate Email";
            }

            echo json_encode($res);

        }
    }

    public function changePass()
    {
        if ($this->data['authorize']['is_update']) {

            if (empty($_SESSION['iduser'])) {
                $id = $this->input->post('idUser', true);
            } else {
                $id = $_SESSION['iduser'];
            }
            if ($this->input->post('NewchPass', true) == $this->input->post('RechPass', true)) {
                $data_user = $this->user_model->get_by(array('ID' => $id), 1, null, true);
                $cekoldpass = $this->user_model->cekoldpass(md5($this->input->post('chPass', true) . $data_user->HASH), $id);
                if ($cekoldpass) {
                    if ($this->input->post('chPass', true) == $this->input->post('NewchPass', true)) {
                        $res = "Failed: Old Password and New Password are same";
                    } else {
                        $hash = generateRandomString();
                        $password = md5($this->input->post('NewchPass', true) . $hash);

                        $dataupdate = array(
                            'PASSWORD' => $password,
                            'HASH' => $hash,
                            'DATE_LOG' => date('Y-m-d H:i:s'),
                            'USER_LOG' => $id,
                        );

                        $res = $this->user_model->changePass($dataupdate, $id);
                    }
                } else {
                    $res = "Failed: Wrong old password";
                }
            } else {
                $res = "Failed: Password not match";
            }

            echo json_encode($res);

        }
    }

}
