<?php
$param = isset($param) ? $param : "";
$ID_SOCIAL_MEDIA = isset($param['ID_SOCIAL_MEDIA']) ? $param['ID_SOCIAL_MEDIA'] : "";
$NAME = isset($param['NAME']) ? $param['NAME'] : "";
$LINK = isset($param['LINK']) ? $param['LINK'] : "";
$URL_SOCIAL_MEDIA = isset($param['URL_SOCIAL_MEDIA']) ? $param['URL_SOCIAL_MEDIA'] : "";
$IS_SHARING = isset($param['IS_SHARING']) ? $param['IS_SHARING'] : "";
$IS_HIDDEN = isset($param['IS_HIDDEN']) ? $param['IS_HIDDEN'] : "";

$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Title</label><span style="color: orange">*</span><br>
            <input type="text" name="name" class="form-control" id="name" placeholder="" value="<?php echo $NAME ?>">
            <input type="hidden" name="id_social_media" class="form-control" id="id"
                value="<?php echo $ID_SOCIAL_MEDIA ?>">
        </div>
        <div class="form-group">
            <label>Social Media URL</label><span style="color: orange">*</span><br>
            <input type="url" name="url_social_media" class="form-control" id="url_social_media" placeholder=""
                required="" value="<?php echo $URL_SOCIAL_MEDIA ?>">

        </div>
        <!-- <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" value="1" name="is_sharing" id="is_sharing"
                            <?php if ($IS_SHARING == 1) {echo 'checked';}?>>
                        Is Article
                    </label>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" value="1" name="is_hidden" id="is_hidden"
                            <?php if ($IS_HIDDEN == 1) {echo 'checked';}?>>
                        Is Footer
                    </label>
                </div>
            </div>
        </div> -->


        <div class="form-group">
            <label>Upload Social Media</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
                data-default-file="<?php echo $LINK ?>" />
        </div>
        <div class="form-group">
            <label>Status</label><br>
            <select class="form-control" name="status" id="status">
                <?php $STATUS = $this->config->item('STATUS');
$ID_STATUS = $this->config->item('ID_STATUS');
$Exp_ID = explode(":", $ID_STATUS);
$Exp_DESC = explode(":", $STATUS);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    if ($ID == $STATUS_DATA) {
        $selected = "selected";
    } else {
        $selected = "";
    }?>
                <option value="<?php echo $ID ?>" <?php echo $selected; ?>><?php echo $DESC ?></option>
                <?php }?>
            </select>

        </div>
        <a href="#" onclick="openTnC('<?php echo base_url('socialmedia/tncupload') ?>')">Terms and Conditions Upload</a>

    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>


<script type="text/javascript">
$('.dropify').dropify();
changeformfile();
</script>