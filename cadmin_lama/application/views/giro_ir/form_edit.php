<?php
$is_update = $authorize['is_update'];
$back = isset($data['back']) ? $data['back'] : '';
$giro_ir = isset($data['giro_ir']) ? $data['giro_ir'] : array();
$DETAIL = isset($data['detail']) ? $data['detail'] : array();

$ID_GIRO_IR_HEADER = isset($giro_ir->ID_GIRO_IR_HEADER) ? $giro_ir->ID_GIRO_IR_HEADER : "";
$EFFECTIVE_DATE = isset($giro_ir->EFFECTIVE_DATE) ? $giro_ir->EFFECTIVE_DATE : "";
$CATEGORY_GIRO = isset($giro_ir->CATEGORY_GIRO) ? $giro_ir->CATEGORY_GIRO : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dropify-wrapper {
    height: 100%;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Interest Rate</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Giro Interest Rate</a>
                <a class="breadcrumb-item active">Edit</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Giro Interest Rate</span> -
                Edit
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveGiroIR">
            <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
                <input type="hidden" name="id_giro_ir_header" id="id_giro_ir_header"
                    value="<?php echo $ID_GIRO_IR_HEADER ?>">
                <input type="hidden" name="listDelete" id="listDelete" value="">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Category</label><span style="color: orange">*</span><br>
                        <select class="form-control select2" name="category" id="category" required="">
                            <option selected disabled>Choose Category</option>
                            <?php
$GIRO_ID = $this->config->item('GIRO_ID');
$GIRO_EN = $this->config->item('GIRO_EN');
$ID_CATEG_GIRO = $this->config->item('ID_CATEG_GIRO');
$Exp_GIRO_ID = explode(":", $GIRO_ID);
$Exp_GIRO_EN = explode(":", $GIRO_EN);
$Exp_ID_CATEG_GIRO = explode(":", $ID_CATEG_GIRO);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_GIRO); $i++) {
    $ID = $Exp_ID_CATEG_GIRO[$i];
    $DESC_EN = $Exp_GIRO_EN[$i];
    $DESC_ID = $Exp_GIRO_ID[$i];
    ?>
                            <option value="<?php echo $ID ?>">
                                <?php echo $DESC_EN . ' / ' . $DESC_ID ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Effective Date<span style="color:orange">*</span></label>

                        <div class="input-group" style="color:#333">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar5"></i></span>
                            </span>
                            <input type="text" name="effective_date" id="effective_date"
                                class="form-control pickadate-selectors" placeholder="Select Date.." required=""
                                data-value="<?php echo $EFFECTIVE_DATE ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">Giro Interest Rate Detail</h6>

                    </div>
                    <div class="col-md-6">
                        <button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" style="float:right;"
                            type="button" onclick="add_detail();"><b><i class="fas fa-plus"></i></b>
                            Add
                        </button>
                    </div>
                </div>

                <hr>
                <?php if (!empty($DETAIL)) {?>
                <div id="form_detail">
                    <?php echo $DETAIL; ?>

                </div>
                <?php }?>
            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Cancel</button></a>
                </div>
            </div>
        </form>
    </div>


    <?php endif;?>


</div>

<script type="text/javascript">
var room = 0;
$(document).ready(function() {
    $('#category').val("<?php echo $CATEGORY_GIRO ?>").trigger('change');
});
var room = 0;
$(function() {
    $('.select2').select2();
    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });
});

function add_detail() {
    room++;
    var objTo = document.getElementById('form_detail');
    html = '';
    html =
        ' <div class="card border-left-info border-right-info rounded-0" id="removeclass' +
        room + '"><div class="card-body"><div class="row" >' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
        ' <div class="row">' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">' +
        '<div class="form-group">' +
        '<label>Balance Indonesian<span style="color:orange">*</span></label><input type="hidden" value="NEW" name="id_detail[]" id="id_detail">' +
        '<input type="text" class="form-control" id="balance_id" name = "balance_id[]" placeholder = "Balance in Indonesian" required = ""  parsley-trigger = "change" > ' +
        '</div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">' +
        '<div class="form-group">' +
        '<label>Balance English<span style="color:orange">*</span></label>' +
        '<input type="text" class="form-control" id="balance_en" name = "balance_en[]" placeholder = "Balance in English" required = "" parsley-trigger = "change" > ' +
        '</div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
        '<div class="form-group">' +
        '<label>Rate<span style="color:orange">*</span></label>' +
        '<input type="text" class="form-control" id="rate" name="rate[]" placeholder = "Rate" required = "" parsley-trigger = "change" > ' +
        '</div>' +
        '</div>' +
        ' </div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
        '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type="button" onclick="remove(' +
        room + ');"><b><i class="fas fa-minus"></i></b>' +
        'Remove</button>' +
        '</div>' +
        '</div></div></div>';
    $("#form_detail").append(html);
    $('.form-control-uniform').uniform();
}

function remove(rid) {
    Swal.fire({
        title: "Are you sure?",
        type: "warning",
        text: "Your will not be able to recover this data!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes",
    }).then((result) => {
        if (result.value) {
            var deleteid = $('#removeclass' + rid).data('id');
            var listDelete = $('#listDelete').val();
            if (deleteid != 'NEW') {
                $('#listDelete').val((listDelete == '' ? '' : (listDelete + ',')) + deleteid);
            }

            $('#removeclass' + rid).remove();

        }
    });

}

$('#frmsaveGiroIR').on('submit', function(e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        $("#frmsaveGiroIR").parsley().validate();
        var form = $("#frmsaveGiroIR");
        var data = form.serialize();

        if ($("#frmsaveGiroIR").parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('giro_ir/update') ?>",
                type: "POST",
                data: data,
                cache: false,
                dataType: "json",
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            var uri = "<?php echo $back; ?>";
                            window.location.href = uri;
                        }, 1500);
                    }
                },
            });
        }
    } else {

    }
});
</script>