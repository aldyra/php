<?php
$data = isset($data) ? $data : "";
$ID_GCG_HEADER = isset($data['gcg']->ID_GCG_HEADER) ? $data['gcg']->ID_GCG_HEADER : "";
$DESCRIPTION_ID = isset($data['gcg']->DESCRIPTION_ID) ? $data['gcg']->DESCRIPTION_ID : "";
$DESCRIPTION_EN = isset($data['gcg']->DESCRIPTION_EN) ? $data['gcg']->DESCRIPTION_EN : "";
$STATUS = isset($data['gcg']->STATUS) ? $data['gcg']->STATUS : "";
$CREATED_BY = isset($data['gcg']->CREATED_BY) ? $data['gcg']->CREATED_BY : "";
$CREATED_DATE = isset($data['gcg']->CREATED_DATE) ? $data['gcg']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}

$gcg_detail = isset($data['detail']) ? $data['detail'] : array();
$back = isset($data['back']) ? $data['back'] : '';
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master Reporting</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master GCG</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master GCG</span> - Detail
                <small><?php echo $DESCRIPTION_ID . ' - ' . $DESCRIPTION_EN ?></small>
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title"><?php echo $DESCRIPTION_ID . ' - ' . $DESCRIPTION_EN ?>
            </h6>

        </div>

        <ul class="media-list media-list-bordered">
            <?php foreach ($gcg_detail as $detail) {?>
            <li class="media">
                <div class="mr-3">
                    <a class="btn bg-transparent border-danger text-danger rounded-round border-2 btn-icon"><i
                            class="fas fa-file-pdf"></i></a>
                </div>

                <div class="media-body">
                    <h6 class="media-title"><?php echo $detail->NAME_ID . ' - ' ?>
                        <a href='<?php echo $detail->LINK_ID ?>' download>
                            Show File</a>
                    </h6>
                    <h6 class="media-title"><?php echo $detail->NAME_EN . ' - ' ?>
                        <a href='<?php echo $detail->LINK_EN ?>' download>
                            Show File</a>
                    </h6>
                </div>
            </li>
            <?php }?>

        </ul>
    </div>
</div>