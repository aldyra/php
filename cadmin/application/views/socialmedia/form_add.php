<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Name</label><span style="color: orange">*</span><br>
            <input type="text" name="name" class="form-control" id="name" placeholder="" required="">

        </div>
        <div class="form-group">
            <label>Social Media URL</label><span style="color: orange">*</span><br>
            <input type="url" name="url_social_media" class="form-control" id="url_social_media" placeholder=""
                required="">

        </div>
        <!-- <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" value="1" name="is_sharing" id="is_sharing">
                        Is Article
                    </label>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" value="1" name="is_hidden" id="is_hidden">
                        Is Footer
                    </label>
                </div>
            </div>
        </div> -->
        <div class="form-group">
            <label>Upload Icon</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" required="" />
        </div>
        <a href="#" onclick="openTnC('<?php echo base_url('socialmedia/tncupload') ?>')">Terms and Conditions Upload</a>
    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    changeformfile();
});
</script>