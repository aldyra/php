<!-- Theme JS files -->
<script src="<?php echo base_url() ?>/assets/js/plugins/pickers/anytime.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
<!-- /theme JS files -->
<?php
$data = isset($data) ? $data : "";
$ID_JOBVACANCY = isset($data['jobvacancy']->ID_JOBVACANCY) ? $data['jobvacancy']->ID_JOBVACANCY : "";
$CODE = isset($data['jobvacancy']->CODE) ? $data['jobvacancy']->CODE : "";
$POSITION = isset($data['jobvacancy']->POSITION) ? $data['jobvacancy']->POSITION : "";
$JOB_DESCRIPTION = isset($data['jobvacancy']->JOB_DESCRIPTION) ? $data['jobvacancy']->JOB_DESCRIPTION : "";
$REQUIREMENT = isset($data['jobvacancy']->REQUIREMENT) ? $data['jobvacancy']->REQUIREMENT : "";
$STATUS_JOB = isset($data['jobvacancy']->STATUS_JOB) ? $data['jobvacancy']->STATUS_JOB : "";
$LANG = isset($data['jobvacancy']->LANGUAGE) ? $data['jobvacancy']->LANGUAGE : "";
$ID_GROUP_JOBVACANCY = isset($data['jobvacancy']->ID_GROUP_JOBVACANCY) ? $data['jobvacancy']->ID_GROUP_JOBVACANCY : "";
$DETAIL = isset($data['jobvacancy']->DETAIL) ? $data['jobvacancy']->DETAIL : "";
$START_DATE = isset($data['jobvacancy']->START_DATE) ? date("d-M-Y", strtotime($data['jobvacancy']->START_DATE)) : "";
$END_DATE = isset($data['jobvacancy']->END_DATE) ? date("d-M-Y", strtotime($data['jobvacancy']->END_DATE)) : "";
$STATUS_DATA = isset($data['jobvacancy']->STATUS) ? $data['jobvacancy']->STATUS : "";
$menu = isset($data['menu']) ? $data['menu'] : array();
$back = isset($data['back']) ? $data['back'] : '';
$is_update = $authorize['is_update'];

?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Career</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Job Vacancy</a>
                <a href="#" class="breadcrumb-item active">Edit</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Edit Job Vacancy</h5>
        </div>

        <div class="card-body">
            <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveJobvacancy">
                <input type="hidden" name="id_jobvacancy" id="id_jobvacancy" value="<?php echo $ID_JOBVACANCY ?>">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="form-group">
                            <label>
                                Language<span style="color:orange">*</span>
                            </label>
                            <select class="form-control select2" name="language" id="language" required="">
                                <option selected disabled>Choose Language</option>

                                <?php
$LANGUAGE = $this->config->item('LANGUAGE');
$ID_LANGUAGE = $this->config->item('ID_LANGUAGE');
$Exp_ID = explode(":", $ID_LANGUAGE);
$Exp_DESC = explode(":", $LANGUAGE);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    ?>
                                <option value="<?php echo $ID ?>"><?php echo $DESC ?></option>
                                <?php
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group">
                            <label>
                                Group Job Vacancy<span style="color:orange">*</span>
                            </label>
                            <select class="form-control select2" name="group_jobvacancy" id="group_jobvacancy"
                                required="">
                                <option selected disabled>Choose Menu</option>
                                <?php foreach ($group_jobvacancy as $group) {
    $ID = isset($group['ID']) ? $group['ID'] : "";
    $NAME = isset($group['NAME']) ? $group['NAME'] : "";
    ?>
                                <option value="<?php echo $ID ?>"><?php echo $NAME ?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                STATUS JOB<span style="color:orange">*</span>
                            </label>
                            <select class="form-control select2" name="status_job" id="status_job" required="">
                                <option selected disabled>Choose Status</option>
                                <option value="Full Time">Full Time</option>
                                <option value="Part Time">Part Time</option>
                                <option value="Internship">Internship</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group">
                            <label>Job Vacancy Code<span style="color:orange">*</span></label>
                            <input type="text" class="form-control" id="CODE" name="code" placeholder="Job Vacancy Code"
                                required="" parsley-trigger="change" value="<?php echo $CODE ?>">
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="form-group">
                            <label for="position">Position<span style="color: orange">*</span></label>
                            <input type="text" class="form-control" id="POSITION" name="position" placeholder="Position"
                                required="" parsley-trigger="change" value="<?php echo $POSITION ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="form-group">
                            <label for="job_description">Job Description<span style="color: orange">*</span></label>
                            <textarea class="form-control" name="job_description" id="JOB_DESCRIPTION" rows="4"
                                required="" parsley-trigger="change"><?php echo $JOB_DESCRIPTION ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="form-group">
                            <label for="requirement">Requirement<span style="color: orange">*</span></label>
                            <textarea class="form-control" name="requirement" id="REQUIREMENT" rows="4" required=""
                                parsley-trigger="change"><?php echo $REQUIREMENT ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="form-group">
                            <label for="detail">Detail<span style="color: orange">*</span></label>
                            <textarea class="form-control" name="detail" id="DETAIL" rows="4" required=""
                                parsley-trigger="change"><?php echo $DETAIL ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label>Start Date<span style="color:orange">*</span></label>

                        <p><input type="text" class="form-control" id="start_date" name="start_date"
                                placeholder="Start date" required=""></p>
                    </div>

                    <div class="col-md-6">
                        <label>End Date<span style="color:orange">*</span></label>

                        <p><input type="text" class="form-control" id="end_date" name="end_date"
                                placeholder="Finish date" required="" disabled></p>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light"> Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
    <?php endif;?>

</div>
<!-- end Content area -->


<script type="text/javascript">
$('.select2').select2();
CKEDITOR.editorConfig = function(config) {
    config.line_height = "1em;1.1em;1.2em;1.3em;1.4em;1.5em";
};
CKEDITOR.plugins.addExternal('videoembed',
    '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/videoembed/plugin.js', '');
CKEDITOR.plugins.addExternal('html5video',
    '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/html5video/plugin.js', '');
CKEDITOR.plugins.addExternal('lineheight',
    '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/lineheight/plugin.js', '');
CKEDITOR.plugins.addExternal('spicingsliders',
    '<?php echo base_url() ?>assets/lib/ckeditor4/plugins/spacingsliders/plugin.js', '');




CKEDITOR.replace('job_description', {
    extraPlugins: 'videoembed, html5video, lineheight,spacingsliders',

});

CKEDITOR.replace('requirement', {
    extraPlugins: 'videoembed, html5video, lineheight,spacingsliders',


});
CKEDITOR.replace('detail', {
    extraPlugins: 'videoembed, html5video, lineheight,spacingsliders',


});



CKEDITOR.on('instanceReady', function() {
    $.each(CKEDITOR.instances, function(instance) {
        CKEDITOR.instances[instance].on("change", function(e) {
            for (instance in CKEDITOR.instances)
                CKEDITOR.instances[instance].updateElement();
        });
    });
});

$('#group_jobvacancy').prop('disabled', true);
$('#language').change(function() {
    var lang = $(this).val();
    $.ajax({
        url: "<?php echo base_url('group_jobvacancy/get_group'); ?>",
        method: "POST",
        data: {
            lang: lang
        },
        async: false,
        dataType: 'json',
        success: function(data) {
            var html = '';
            var i;
            html += '<option disabled selected> Select </option>';
            for (i = 0; i < data.length; i++) {

                html += '<option value="' + data[i].ID + '">' + data[i].NAME + '</option>';
            }
            $('#group_jobvacancy').html(html);
            $('#group_jobvacancy').prop('disabled', false);
        },
        beforeSend: function() {
            $('.loadingBox').show();
        },
        complete: function() {
            $('.loadingBox').fadeOut();
        }
    });
});
$(function() {
    // Options
    var oneDay = 24 * 60 * 60 * 1000;
    var rangeDemoFormat = '%e-%b-%Y';
    var rangeDemoConv = new AnyTime.Converter({
        format: rangeDemoFormat
    });

    // Set today's date
    $('#rangeDemoToday').on('click', function(e) {
        $('#start_date').val(rangeDemoConv.format(new Date())).trigger('change');
    });

    // Clear dates
    $('#rangeDemoClear').on('click', function(e) {
        $('#start_date').val('').trigger('change');
    });

    // Start date
    $('#start_date').AnyTime_picker({
        format: rangeDemoFormat
    });

    // On value change
    $('#start_date').on('change', function(e) {
        try {
            var fromDay = rangeDemoConv.parse($('#start_date').val()).getTime();

            var dayLater = new Date(fromDay + oneDay);
            dayLater.setHours(0, 0, 0, 0);

            var ninetyDaysLater = new Date(fromDay + (90 * oneDay));
            ninetyDaysLater.setHours(23, 59, 59, 999);

            // End date
            $('#end_date')
                .AnyTime_noPicker()
                .removeAttr('disabled')
                // .val(rangeDemoConv.format(dayLater))
                .AnyTime_picker({
                    earliest: dayLater,
                    format: rangeDemoFormat,
                });
        } catch (e) {

            // Disable End date field
            // $('#end_date').val('').attr('disabled', 'disabled');
        }
    });
});

$('#frmsaveJobvacancy').on('submit', function(e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        $.ajax({
            url: "<?php echo base_url('jobvacancy/update'); ?>",
            data: $(this).serialize(),
            dataType: "JSON",
            type: 'POST',
            cache: false,
            success: function(res) {
                var ErrorMessage = res.ErrorMessage;
                var ErrorCode = res.ErrorCode;
                if (ErrorCode != "EC:0000") {
                    Swal.fire({
                        type: "error",
                        html: ErrorMessage,
                        confirmButton: true,
                        confirmButtonColor: "#1FB3E5",
                        confirmButtonText: "Close",
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        type: "success",
                        text: ErrorMessage,
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    setTimeout(function() {
                        var uri = "<?php echo $back; ?>";
                        window.location.href = uri;
                    }, 1500);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire(textStatus).then(function() {
                    Swal.fire({
                        type: "error",
                        title: textStatus,
                    });
                });
            },
            beforeSend: function() {
                $('#loadingBox').show();
            },
            complete: function() {
                $('#loadingBox').fadeOut();
            }
        });
    } else {
        ;
    }
});
$(document).ready(function() {
    $('#language').val("<?php echo $LANG ?>").trigger('change');
    $('#group_jobvacancy').val("<?php echo $ID_GROUP_JOBVACANCY ?>").trigger('change');
    $('#status_job').val("<?php echo $STATUS_JOB ?>").trigger('change');
    $('#start_date').val("<?php echo $START_DATE ?>").trigger('change');
    $('#end_date').val("<?php echo $END_DATE ?>").trigger('change');
});
</script>