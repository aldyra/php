<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Video extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('video_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "video/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['video'] = $this->video_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Allowed videos are ' . str_replace("|", ", ", $this->config->item('allowed_types_video')) . ' <br>
                    Max video size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }

    public function add()
    {
        $TITLE = isset($_POST['title']) ? $_POST['title'] : "";

        // $DESCRIPTION = isset($_POST['description']) ? $_POST['description'] : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($TITLE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        } elseif (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File video is required!");
            die(json_encode($JSON));
        }

        $path = $this->config->item('save_video');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['file_name'] = 'VIDEO_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_video');
        $path_parts = pathinfo($FILE);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_video');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $param['TITLE'] = $TITLE;
                $param['LINK'] = $path . $config['file_name'] . "." . $forUpload['extension'];
                $param['CREATED_BY'] = $_SESSION['username'];
                $Insert = $this->video_model->add($param);
                if ($Insert['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                    die(json_encode($JSON));
                }
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->video_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        $ID = isset($_POST['id']) ? $_POST['id'] : "";
        $TITLE = isset($_POST['title']) ? $_POST['title'] : "";
        $STATUS = isset($_POST['status']) ? $_POST['status'] : "";
        if (empty($TITLE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        }

        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000C", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }

            $path = $this->config->item('save_video');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['file_name'] = 'VIDEO_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_video');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_video');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $param['ID'] = $ID;
                    $param['TITLE'] = $TITLE;
                    $param['LINK'] = $path . $config['file_name'] . "." . $forUpload['extension'];
                    $param['USER_LOG'] = $_SESSION['username'];
                    $param['DATE_LOG'] = date('Y-m-d H:i:s');
                    $param['STATUS'] = $STATUS;
                    $Update = $this->video_model->updateaction($param);
                    if ($Update['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
                    die(json_encode($JSON));
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $param['ID'] = $ID;
            $param['TITLE'] = $TITLE;
            $param['USER_LOG'] = $_SESSION['username'];
            $param['DATE_LOG'] = date('Y-m-d H:i:s');
            $param['STATUS'] = $STATUS;
            $Update = $this->video_model->updateaction($param);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        }
    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->video_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "0";
        $where = array('ID' => $id);
        $Active = $this->video_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "88";
        $where = array('ID' => $id);
        $Active = $this->video_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "99";
        $where = array('ID' => $id);
        $Active = $this->video_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}