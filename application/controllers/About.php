<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {

	public function index()
	{
		$this->data['cssName'] = "Css/public.css:Css/about.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/about.js";
		$this->data['Content'] = "overview";
		$this->data['data'] = array();
		$this->load->view('template', $this->data);
	}
}
