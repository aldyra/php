<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Web Admin | Bank Bumi Arta</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <script src="<?php echo base_url() ?>assets/js/app.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/public.js"></script>

    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_pages/dashboard.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/streamgraph.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/sparklines.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/lines.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/areas.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/donuts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/bars.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/progress.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/heatmaps.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/pies.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_charts/pages/dashboard/light/bullets.js"></script>
    <!-- /theme JS files -->

</head>

<body>

    <?php $this->load->view('templates/navbar')?>
    <!-- Page content -->
    <div class="page-content">
        <?php $this->load->view('templates/sidebar')?>

        <!-- Main content -->
        <div class="content-wrapper">
            <?php $this->load->view($content, $paramcontent);?>
            <?php $this->load->view('templates/footer');?>

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script>
    var url = <?php echo base_url() ?>;
    </script>

</body>

</html>