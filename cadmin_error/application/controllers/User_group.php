<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_group extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('usergroup_model');
        $this->load->model('usergroupp_model');
        $this->load->model('modulep_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "user_group/view";
        $data['authorize'] = $this->data['authorize'];

        $this->load->view('templates/view', $data);
    }

    public function get_data()
    {
        header('Content-Type: application/json');

        $col = implode(',', $this->input->post('arr', true));
        $getAction = $this->data['authorize'];
        $res = $this->usergroup_model->select_col($col, $getAction);
        $tes = json_decode($res);

        foreach ($tes->data as $row) {
            $row->CREATED_DATE = convertDate($row->CREATED_DATE);
            ($row->DATE_LOG != null) ? $row->DATE_LOG = convertDate($row->DATE_LOG) : $row->DATE_LOG = '';
            switch ($row->STATUS) {
                case '0':
                    $row->STS = '0';
                    $row->STATUS = "<span class='badge badge-danger'>Inactive</span>";
                    $row->ACTION = str_replace(array('$icon', '$status'), array('icon-check', 'Active'), $row->ACTION);
                    break;
                case '88':
                    $row->STS = '88';
                    $row->STATUS = "<span class='badge badge-success'>Active</span>";
                    $row->ACTION = str_replace(array('$icon', '$status'), array('icon-cross', 'Inactive'), $row->ACTION);
                    break;
                case '99':
                    $row->STS = '99';
                    $row->STATUS = "<span class='badge badge-primary'>Publish</span>";
                    $row->ACTION = str_replace(array('$icon', '$status'), array('icon-cross', 'Inactive'), $row->ACTION);
                    break;
            }
        }

        echo json_encode($tes);
    }

    public function get_edit($id)
    {
        $res = $this->usergroup_model->get($id);
        echo json_encode($res);
    }

    public function create_action()
    {

        if ($this->data['authorize']['is_create']) {
            $cek = $this->usergroup_model->cekDuplicate($this->input->post('user_group_name', true));

            $this->form_validation->set_rules('user_group_name', 'User Group Name', 'required|max_length[25]|min_length[2]');
            if ($this->form_validation->run() == false) {
                $ErrorMessage = "Failed: " . validation_errors();
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else if (!_noSpecChar($this->input->post('user_group_name', true))) {
                $ErrorMessage = "Failed: User Group Name cannot contain any special characters.";
                $JSON = array("ErrorCode" => "EC:001B", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else {
                if ($cek) {
                    $datainsert = array(
                        'USER_GROUP_NAME' => $this->input->post('user_group_name', true),
                        'CREATED_DATE' => date('Y-m-d H:i:s'),
                        'CREATED_BY' => $_SESSION['username'],
                    );
                    // insert user group table
                    $id_group = $this->usergroup_model->insert($datainsert);

                    $main_module = $this->modulep_model->get_by(
                        array('MENU_LEVEL' => 0, 'ACTIVE' => 1), null, null, false, array('ID')
                    );
                    // group privilege
                    // foreach ($main_module as $module) {
                    //     $groupins[] = array(
                    //         'USER_GROUP_ID' => $id_group,
                    //         'MODULE_PREVILEGE_ID' => $module->ID,
                    //         'IS_READ' => 1,
                    //         'CREATED_BY' => $_SESSION['username'],
                    //         'CREATED_DATE' => date('Y-m-d H:i:s'),
                    //         'DATE_LOG' => date('Y-m-d H:i:s'),
                    //         'USER_LOG' => $_SESSION['username'],
                    //     );
                    // }

                    // $this->usergroupp_model->insert($groupins, true);
                    $ErrorMessage = "success";
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => $ErrorMessage);
                    die(json_encode($JSON));
                } else {
                    $ErrorMessage = "Failed: Duplicate Group Name";
                    $JSON = array("ErrorCode" => "EC:001D", "ErrorMessage" => $ErrorMessage);
                    die(json_encode($JSON));
                }
            }
        } else {
            $ErrorMessage = "Failed: You do not have access to data creation.";
            $JSON = array("ErrorCode" => "EC:001E", "ErrorMessage" => $ErrorMessage);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Process not found!");
        die(json_encode($JSON));
    }

    public function update_action()
    {
        if ($this->data['authorize']['is_update']) {
            $id = $this->input->post('IDUserGroup', true);

            $dataupdate = array(
                'USER_GROUP_NAME' => $this->input->post('user_group_name', true),
                'USER_LOG' => $_SESSION['username'],
                'DATE_LOG' => date('Y-m-d H:i:s'),
                'IP_LOG' => $_SERVER['REMOTE_ADDR'],
            );

            $cek = $this->usergroup_model->cekDuplicate($this->input->post('user_group_name', true), $id);
            if ($cek) {
                $this->usergroup_model->update($dataupdate, array('ID' => $id));
                $ErrorMessage = "Success!";
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            } else {
                $ErrorMessage = "Failed: Duplicate Name";
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            }
        } else {
            $ErrorMessage = "Failed: You do not have access to data update.";
            $JSON = array("ErrorCode" => "EC:001B", "ErrorMessage" => $ErrorMessage);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Process not found!");
        die(json_encode($JSON));

    }

    public function statusChg($id)
    {
        if ($this->data['authorize']['is_update']) {
            $array_id = array('ID' => $id);
            $get_data = $this->usergroup_model->get($id);

            if ($get_data->STATUS == '88') {
                $array_data = array(
                    'STATUS' => '0',
                    'DATE_LOG' => date('Y-m-d H:i:s'),
                    'USER_LOG' => $_SESSION['username'],
                    'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                );
                $res = 'Success: Status deactivated';
            } else {
                $array_data = array(
                    'STATUS' => '88',
                    'DATE_LOG' => date('Y-m-d H:i:s'),
                    'USER_LOG' => $_SESSION['username'],
                    'IP_LOG' => $_SERVER['REMOTE_ADDR'],
                );
                $res = 'Success: Status activated!';
            }

            $this->usergroup_model->update($array_data, $array_id);
        } else {
            $res = "Failed: You do not have access to update data.";
        }

        echo json_encode($res);
    }

    public function delete_action($id)
    {
        if ($this->data['authorize']['is_delete']) {
            $del = $this->usergroup_model->delete_row($id);
            $this->usergroupp_model->delete_by(array('USER_GROUP_ID' => $id));
            if ($del) {
                $res = "Success delete data!";
            } else {
                $res = "Failed.";
            }

        } else {
            $res = "Failed: You do not have access to delete data.";
        }

        echo json_encode($res);
    }

}