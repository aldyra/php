<?php
class Custom
{
	
	function pagination($url = NULL, $num_rows = NULL, $per_page = NULL, $num_links = NULL, $uri_segment = NULL)
	{
		$_this =& get_instance();

		$config['base_url'] = base_url($url);
		$config['total_rows'] = $num_rows;
        $config["uri_segment"] = $uri_segment;  // uri parameter

		/*banyaknya record yang ditampilkan dalam 1 halaman*/
		if ($per_page) {
			$config['per_page'] = $per_page;
		}

		else {
			$config['per_page'] = 10;
		}

		/*jumlah link sebelum dan sesudah link aktif*/
		if ($num_links) {
			$config['num_links'] = $num_links;
		}

		else {
			$config['num_links'] = 2;
		}

		$config['use_page_numbers'] = TRUE;

		/* Configurasi Style. Default library ini menggunakan bootstrap */
		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<ul class="pagination justify-content-center mt-3">';
        $config['full_tag_close']   = '</ul>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';


		/*inisialisasi*/
		$_this->pagination->initialize($config);
	}
}