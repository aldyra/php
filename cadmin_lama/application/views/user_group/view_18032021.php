<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>User Group - NAM Air</title>

    <!-- INCLUDE SCRIPT TOP -->
        <?php require_once APPPATH . 'views/templates/script_top.php';?>
    <!-- END INCLUDE SCRIPT TOP -->

    <style type="text/css">
    </style>

</head>

<body>

    <!-- OPEN NAV MENU -->
    <?php require_once APPPATH . 'views/templates/topbar.php';?>
    <!-- END NAV MENU -->

    <!-- Page content -->
    <div class="page-content">

        <!-- OPEN LEFT NAV BAR -->
        <?php require_once APPPATH . 'views/templates/nav_menu.php';?>
        <!-- END LEFT NAV BAR -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- OPEN MODAL -->
            <?php require_once APPPATH . 'views/templates/modals.php';?>
            <!-- END MODAL -->

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><span class="font-weight-semibold">User Group</span></h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active">User Group</span>
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Order direction sequence control -->
                <div class="card">
                    <div class="card-body">
                        <?php if ($authorize['is_create']): ?>
                        <button type="button" class="btn btn-info" id="btnAddUserGroup" data-toggle="modal" data-target="#modalAddUserGroup">Add Group</button>
                    <?php endif;?>
                    </div>

                    <table id="tableData" class="table datatable-complex-header">
                        <thead>
                            <tr>
                                <th>User Group Name</th>
                                <th>Created By</th>
                                <th data-orderable="false"  width="200">Created Date</th>
                                <th>User Log</th>
                                <th>Date Log</th>
                                <th data-orderable="false">Status</th>
                                <th  data-orderable="false">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /order direction sequence control -->

            </div>
            <!-- /content area -->


            <!-- OPEN FOOTER -->
            <?php require_once APPPATH . 'views/templates/footer.php';?>
            <!-- END FOOTER -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script type="text/javascript">
        <?php if (!$authorize['is_update']) {?>
            var colDef = [{
                            "targets": [6,7],
                            "visible": false,
                            "searchable": false
                        }];
        <?php } else {?>
            var colDef = [{
                            "targets": [7],
                            "visible": false,
                            "searchable": false
                        }];
        <?php }?>
        var arr = ["ID", "USER_GROUP_NAME", "CREATED_BY", "CREATED_DATE", "USER_LOG", "DATE_LOG", "STATUS"];
        var t = $("#tableData").DataTable({
            serverSide: true,
            autoWidth: false,
            ajax: {
                "url": "<?php echo base_url('User_group/get_data') ?>",
                "type": "POST",
                "data": {"arr": arr},
            },
            columns: [
                { "data": "USER_GROUP_NAME" },
                { "data": "CREATED_BY" },
                { "data": "CREATED_DATE" },
                { "data": "USER_LOG" },
                { "data": "DATE_LOG" },
                { "data": "STATUS" },
                {
                    "className": 'text-center',
                    "orderable": false,
                    "data": "ACTION"
                },
                { "data": "ID" }
            ],
            columnDefs: colDef,
            keys: !0,
            order: [[ 7, "desc" ]],
            autofill: true,
            select: true,
            responsive: true,
            buttons: true,
            length: 5
        });

        var frmAdd = 'frmsaveUserGroup';
        var frmEdit = 'frmeditUserGroup';
        var $formSave = $("#"+frmAdd);
        var $formEdit = $("#"+frmEdit);
        var $submitAdd = $("#submitAddUserGroup");
        var $submitEdit = $("#submitEditUserGroup");

        var checkValid = function(act){
            switch(act){
                case 'add':
                if( $formSave.parsley().isValid() )
                  $submitAdd.removeAttr("disabled");
                else
                  $submitAdd.attr("disabled", "disabled");
                break;

                case 'edit':
                if( $formEdit.parsley().isValid() )
                  $submitEdit.removeAttr("disabled");
                else
                  $submitEdit.attr("disabled", "disabled");
                break;
            }
        }

        $("#"+frmAdd+" :input").keyup(function(){
            $(this).parsley().validate();
            checkValid('add');
        });
        $("#"+frmAdd+" :input").change(function(){
            $(this).parsley().validate();
            checkValid('add');
        });
        $("#"+frmEdit+" :input").keyup(function(){
            $(this).parsley().validate();
            checkValid('edit');
        });
        $("#"+frmEdit).on('change', ':input', function(){
            $(this).parsley().validate();
            checkValid('edit');
        });

        $('#frmsaveUserGroup').parsley();
        $('#frmsaveUserGroup').on('submit',function(e) {
                e.preventDefault();
                if ( $(this).parsley().isValid() ) {
                    $.ajax({
                        url:"<?php echo base_url('user_group/create_action'); ?>",
                        data:$(this).serialize(),
                        dataType : "JSON",
                        type:'POST',
                        success:function(data){
                            if(!(data.indexOf("Failed") != -1)){
                            swal({type: 'success', title: 'Add User Success!'}, function(){ t.ajax.reload(null, false);});
                            }else{
                                swal({type: 'error', title: data}, function(){ t.ajax.reload( null, false ); });
                            }
                            document.getElementById("frmsaveUserGroup").reset();
                            $('#modalAddUserGroup').modal('hide');
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal({type: 'error', title: 'Failed: '+textStatus}, function(){ t.ajax.reload( null, false ); });
                            document.getElementById("frmsaveUserGroup").reset();
                            $('#modalAddUserGroup').modal('hide');
                        }
                    });
                }
            });

        //EDIT USER GROUP
        $('#frmeditUserGroup').parsley();
        $('#frmeditUserGroup').on('submit',function(e) {
            e.preventDefault();
            if ( $(this).parsley().isValid() ) {
                $.ajax({
                    url:"<?php echo base_url('user_group/update_action'); ?>",
                    data:$(this).serialize(),
                    dataType : "JSON",
                    type:'POST',
                    success:function(data){
                        if(!(data.indexOf("Failed") != -1)){
                            swal({type: 'success', title: 'Update User Group Success!'}, function(){ t.ajax.reload(null, false);});
                        }else{
                            swal({type: 'error', title: data}, function(){ t.ajax.reload( null, false ); });
                        }
                        document.getElementById("frmeditUserGroup").reset();
                        $('#modalEditUserGroup').modal('hide');
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        Swal.fire(textStatus).then(function(){ t.ajax.reload( null, false ); });
                        document.getElementById("frmeditUserGroup").reset();
                        $('#modalEditUserGroup').modal('hide');
                    }
                });
            }
        });
        //END EDIT USER GROUP

        function confirmDelete(id){
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this Data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:"<?php echo base_url('user_group/delete_action/'); ?>"+id,
                        dataType : "JSON",
                        type:'POST',
                        success:function(data){
                            if(!(data.indexOf("Failed") != -1)){
                                swal({type: 'success', title: 'Delete Data Success'}, function(){
                                 t.ajax.reload(null, false);
                                 });
                            }else{
                                swal({type: 'error', title: data}, function(){
                                 t.ajax.reload( null, false );
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal({type: 'error', title: 'Failed: '+textStatus}, function(){ t.ajax.reload( null, false );tp.ajax.reload( null, false ); });
                        }
                    });
                } else {
                    swal("Cancelled", "Your data is safe :)", "error");
                }
            });
        }

        $(function(){
            $("#btnAddUserGroup").click(function(){
                document.getElementById("frmsaveUserGroup").reset();
                $submitAdd.attr("disabled", "disabled");

                setTimeout(function(){
                    $('#frmsaveUserGroup').parsley().reset();
                }, 100);
            });
        });

        //Open Edit Form
        function openEdit(id){
            $('#frmeditUserGroup').parsley().reset();
            $.ajax({
                url:"<?php echo base_url('user_group/get_edit/'); ?>"+id,
                dataType : "JSON",
                type:'GET',
                success:function(data){
                    $("#userGroupNameEdit").val(data.USER_GROUP_NAME);
                    $("#IDUserGroup").val(data.ID);
                    $('#modalEditUserGroup').modal('show');
                },
                error:function(jqXHR, textStatus, errorThrown){
                    swal(textStatus);
                    document.getElementById("frmeditUserGroup").reset();
                }
            });
        }
        //END Open Edit Form

        // CHANGE STATUS
        function statusChg(id){
            swal({
                    title: "Are you sure?",
                    // text: "Your will not be able to recover this Data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, change it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url:"<?php echo base_url('user_group/statusChg/'); ?>"+id,
                            dataType : "JSON",
                            type:'GET',
                            success:function(data){
                                if(data.indexOf("Success") != -1){
                                    swal({type: 'success', title: data}, function(){
                                     t.ajax.reload(null, false);
                                 });
                                }else{
                                    swal({type: 'error', title: data}, function(){
                                     t.ajax.reload( null, false );
                                 });
                                }
                            },
                            error:function(jqXHR, textStatus, errorThrown){
                                swal(textStatus, function(){
                                    t.ajax.reload(null, false);
                                });
                            }
                        });
                    } else {
                        swal("Cancelled", "Status not change :)", "error");
                    }
                });
        }
        // CHANGE STATUS
    </script>
</body>
</html>