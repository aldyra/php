<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('kesra_winner/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Kesra_Winner = isset($Data['kesra_winner']) ? $Data['kesra_winner'] : array();
?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master Reporting</a>
                <a href="#" class="breadcrumb-item active">Kesra Winner</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', 'modal-lg', 'Add Kesra Winner', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Kesra Winner
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Document</th>
                    <th width="200">Periode</th>
                    <th data-orderable="false">Date</th>
                    <th>Created By</th>
                    <th data-orderable="false"  width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
    ?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php
foreach ($Kesra_Winner as $kesra_winner) {
    $ID = isset($kesra_winner['ID']) ? $kesra_winner['ID'] : "";
    $PERIODE = isset($kesra_winner['PERIODE']) ? $kesra_winner['PERIODE'] : "";
    $DATE = isset($kesra_winner['DATE']) ? convertDate($kesra_winner['DATE']) : "";
    $LINK = isset($kesra_winner['LINK']) ? $kesra_winner['LINK'] : "";
    $STATUS = isset($kesra_winner['STATUS']) ? $kesra_winner['STATUS'] : "";
    $CREATED_BY = isset($kesra_winner['CREATED_BY']) ? $kesra_winner['CREATED_BY'] : "";
    $CREATED_DATE = isset($kesra_winner['CREATED_DATE']) ? convertDate($kesra_winner['CREATED_DATE'], 'timestamp') : "";
    $USER_LOG = isset($kesra_winner['USER_LOG']) ? $kesra_winner['USER_LOG'] : "";
    $DATE_LOG = isset($kesra_winner['DATE_LOG']) ? $kesra_winner['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <?php if (strpos($LINK, '.pdf') == false) {?>
                        <a href='<?php echo $LINK ?>' target="_blank"><?php echo basename($LINK) ?></a>
                        <?php } else {?>
                        <a href='<?php echo $LINK ?>' data-popup='lightbox' data-fancybox><?php echo basename($LINK) ?>
                        </a>
                        <?php }?>

                    </td>
                    <td>
                        <?php echo $PERIODE ?>
                    </td>
                    <td>
                        <?php echo $DATE ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo $CREATED_DATE ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
        ?>
                    <td style="text-align: center;">
                        <?php
$paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $kesra_winner;
        $paramaction['Modal'] = "modal-lg";
        $this->load->view('is_action', $paramaction);
        ?>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(6).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function changeSelectEffectiveDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}
</script>