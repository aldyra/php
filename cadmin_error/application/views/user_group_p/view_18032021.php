<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bank Bumi Arta</title>

    <!-- INCLUDE SCRIPT TOP -->
        <?php require_once APPPATH . 'views/templates/script_top.php';?>
    <!-- END INCLUDE SCRIPT TOP -->

    <link href="<?php echo base_url(); ?>assets/treeview/css/style.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .jstree-grid-header-regular {
            background: #263237 !important;
        }

        .jstree-grid-separator-regular {
            border: none !important;
        }

        .jstree-default .jstree-hovered, .jstree-default .jstree-clicked{
            background: #333;
        }
      .jstree-grid-column {
          text-align: center;
      }
      .jstree-grid-column:first-child {
          text-align: unset;
      }
      .jstree-grid-header-last {
          color: transparent;
      }
      .jstree-grid-cell-last{
          padding: 0px !important;
      }
    </style>

    <script src="<?php echo base_url(); ?>assets/treeview/js/jstree.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/treeview/js/jstreegrid.js"></script>
    <script src="<?php echo base_url(); ?>assets/treeview/js/treebase.js"></script>

</head>

<body>

    <!-- OPEN NAV MENU -->
    <?php require_once APPPATH . 'views/templates/topbar.php';?>
    <!-- END NAV MENU -->

    <!-- Page content -->
    <div class="page-content">

        <!-- OPEN LEFT NAV BAR -->
        <?php require_once APPPATH . 'views/templates/nav_menu.php';?>
        <!-- END LEFT NAV BAR -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- OPEN MODAL -->
            <?php require_once APPPATH . 'views/templates/modals.php';?>
            <!-- END MODAL -->

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><span class="font-weight-semibold">User Group Privilege</span></h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active">User Group Privilege</span>
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">

                <!-- Order direction sequence control -->
                <div class="card">

                    <table id="key-table" class="table datatable-complex-header">
                        <thead>
                            <tr>
                                <th>User Group Name</th>
                                <th data-orderable="false"  width="200">Created Date</th>
                                <th  data-orderable="false">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /order direction sequence control -->

            </div>
            <!-- /content area -->


            <!-- OPEN FOOTER -->
            <?php require_once APPPATH . 'views/templates/footer.php';?>
            <!-- END FOOTER -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script type="text/javascript">
            <?php if (!$authorize['is_update']) {?>
                var colDef = [{
                                "targets": [2,3],
                                "visible": false,
                                "searchable": false
                            }];
            <?php } else {?>
                var colDef = [{
                                "targets": [3],
                                "visible": false,
                                "searchable": false
                            }];
            <?php }?>
            var arr = ["ID", "USER_GROUP_NAME", "STATUS", "USER_GROUP_ID", "MODULE_NAME", "CREATED_DATE"];
            var t = $("#key-table").DataTable({
                serverSide: true,
                autoWidth: false,
                ajax: {
                    "url": "<?php echo base_url('user_group_p/get_data') ?>",
                    "type": "POST",
                    "data": {"arr": arr, "tb": "v_tbl_GroupPrivilege3", "modName": "User Group Privilege"},
                },
                columns: [
                    {
                        "data": "USER_GROUP_NAME"
                    },
                    {
                        "data": "CREATED_DATE"
                    },
                    {
                        "data": "ACTION",
                        "width": "10px",
                        "orderable": false
                    },
                    {
                        "data": "ID"
                    }
                ],
                columnDefs: colDef,
                lengthMenu: [10, 25, 50, 100, 500],
                keys: !0,
                order: [[ 3, "desc" ]],
                autofill: true,
                select: true,
                responsive: true,
                buttons: true,
                length: 4,
                processing: true
            });

        //Open Edit Form2
            function openEdit(id){
                $.ajax({
                    url:"<?php echo base_url('user_group_p/get_PrivilegeCb/'); ?>"+id,
                    dataType : "JSON",
                    type:'GET',
                    success:function(data){

                        $("#uGroupCBID").val(id);
                        $("#containerbasicTree").html("<div id='basicTree'></div>");
                        $("#basicTree").jstree( {
                            plugins: ["core", "ui", "grid", "types"],
                            grid: {
                                columns: [
                                    {width: 280, header: "Module Menu", value: "ModuleMenu", title: "Module Menu"},
                                    {width: 75, header: "View", value: "Read", title: "Read"},
                                    {width: 75, header: "Edit", value: "Edit", title: "Edit"},
                                    {width: 75, header: "Add", value: "Add", title: "Add"},
                                    {width: 75, header: "Delete", value: "Delete", title: "Delete"},
                                    {width: 75, header: "Export", value: "Export", title: "Export"},
                                    {width: 65, header: "Import", value: "Import", title: "Import"},
                                    {width: 95, header: "Btn", value: "Btn", title: "Btn"}
                                ],
                                resizable: true,
                                contextmenu: true
                            },
                            core: {
                                themes: {
                                    responsive: !1
                                },
                                data: data.dataTree

                            },
                            types: {
                                default: {
                                    icon: "mdi mdi-folder-star"
                                }
                                , file: {
                                    icon: "mdi mdi-file"
                                }
                            }
                        });

                        $('#modalGroupPrivilegeEdit').modal('show');
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        swal(textStatus);
                        document.getElementById("frmeditGroupPrivilege").reset();
                    }
                });
            }

        // SAVE USER GROUP PRIIVILEGE MODAL
                $('#frmsaveUGroupPrivilege').on('submit',function(e) {
                    e.preventDefault();
                    $.ajax({
                        url:"<?php echo base_url('user_group_p/saveCb'); ?>",
                        data:$(this).serialize(),
                        dataType : "JSON",
                        type:'POST',
                        success:function(data){
                            if(!(data.indexOf("Failed") != -1)){
                                swal({type: 'success', title: data});
                                $('#modalGroupPrivilegeEdit').modal('hide');
                            }else{
                                swal({type: 'error', title: data});
                                $('#modalGroupPrivilegeEdit').modal('hide');
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal(textStatus);
                        }
                    });
                });
        // END SAVE USER GROUP PRIVILEGE MODAL
    </script>
</body>
</html>