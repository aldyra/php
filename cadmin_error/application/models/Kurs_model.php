<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kurs_model extends MY_Model
{

    protected $_table_name = 'TR_KURS_HEADER';
    protected $_detail_table_name = 'TR_KURS_DETAIL';
    protected $_primary_key = 'ID_KURS_HEADER';
    protected $_detail_primary_key = 'ID_KURS_DETAIL';
    protected $_order_by = 'ID_KURS_HEADER';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }

    public function updateaction($param)
    {
        $ID_KURS_HEADER = isset($param['ID_KURS_HEADER']) ? $param['ID_KURS_HEADER'] : "";

        $WHERE['ID_KURS_HEADER'] = $ID_KURS_HEADER;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_KURS_HEADER'] = isset($param['ID_KURS_HEADER']) ? $param['ID_KURS_HEADER'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

}