<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Submenu extends MY_Controller
{
    /*
    =========================================
    |                                       |
    |               SUBMENU                 |
    |                                       |
    =========================================
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model');
        $this->load->model('submenu_model');
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "submenu/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['submenu'] = $this->submenu_model->view();
        $data['menu'] = $this->get_menu();

        $this->load->view('template', $data);
    }
    public function get_menu()
    {
        $data_menu = $this->menu_model->get();
        $result = array();
        foreach ($data_menu as $data) {
            $param['NAME'] = $data->NAME;
            $param['ID'] = $data->ID;
            $result[] = $param;
        }
        return $result;
    }

    public function add()
    {
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $ICON = isset($_POST['icon']) ? $_POST['icon'] : "";
        $LINK = isset($_POST['link']) ? $_POST['link'] : "";
        $ID_PARENT = isset($_POST['id_parent']) ? $_POST['id_parent'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";

        if (empty($NAME)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        } elseif (empty($LINK)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "Link is required!");
            die(json_encode($JSON));
        } elseif (empty($LANGUAGE)) {
            $JSON = array("ErrorCode" => "EC:000C", "ErrorMessage" => "Language is required!");
            die(json_encode($JSON));
        }

        $param['NAME'] = $NAME;
        $param['LANGUAGE'] = $LANGUAGE;
        $param['LINK'] = $LINK;
        $param['ICON'] = $ICON;
        $param['ID_PARENT'] = $ID_PARENT;
        $param['CREATED_BY'] = $_SESSION['username'];
        $Insert = $this->submenu_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));
    }

    public function view()
    {
        $GetData = $this->submenu_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID = isset($_POST['id_menu']) ? $_POST['id_menu'] : "";
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $LINK = isset($_POST['link']) ? $_POST['link'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";
        $ID_PARENT = isset($_POST['id_parent']) ? $_POST['id_parent'] : "";
        $STATUS = isset($_POST['status']) ? $_POST['status'] : "";
        $ICON = isset($_POST['icon']) ? $_POST['icon'] : "";

        if (empty($NAME)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Name is required!");
            die(json_encode($JSON));
        } elseif (empty($LINK)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "Link is required!");
            die(json_encode($JSON));
        }

        $param['ID'] = $ID;
        $param['NAME'] = $NAME;
        $param['LINK'] = $LINK;
        $param['LANGUAGE'] = $LANGUAGE;
        $param['ICON'] = $ICON;
        $param['ID_PARENT'] = $ID_PARENT;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $param['STATUS'] = $STATUS;
        $Update = $this->submenu_model->updateaction($param);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->submenu_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }

}