<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Log_history extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('log_history_model');
        $this->load->model('kurs_model');
        $this->load->model('deposit_ir_model');
        $this->load->model('saving_ir_model');
        $this->load->model('giro_ir_model');
        $this->load->model('sbdk_model');

    }

    public function kurs()
    {
        $data = array();
        $data['content'] = "log_history/log_kurs";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_kurs'] = $this->log_history_model->view('KURS');

        $this->load->view('templates/view', $data);
    }
    public function detail_kurs($id)
    {
        $data = array();
        $data['content'] = "log_history/detail_kurs";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['kurs'] = $this->kurs_model->get_edit($id);
        $data['data']['detail'] = $this->get_kurs_detail($id);
        $data['data']['back'] = base_url() . 'log_history/kurs';
        $this->load->view('templates/view', $data);
    }
    public function get_kurs_detail($id)
    {
        $this->db->select('A.*, C.CURRENCY_NAME, C.CURRENCY_CODE');
        $this->db->from('TR_KURS_DETAIL A');
        $this->db->join('TR_KURS_HEADER B ', 'A.ID_KURS_HEADER = B.ID_KURS_HEADER', 'left');
        $this->db->join('MS_CURRENCY C', ' A.ID_CURRENCY = C.ID_CURRENCY', 'left');
        $this->db->where('B.ID_KURS_HEADER', $id);
        $this->db->order_by('C.ID_CURRENCY', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }
    public function sbdk()
    {
        $data = array();
        $data['content'] = "log_history/log_sbdk";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_sbdk'] = $this->log_history_model->view('SBDK');

        $this->load->view('templates/view', $data);
    }
    public function detail_sbdk($id)
    {
        $data = array();
        $data['content'] = "log_history/detail_sbdk";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['sbdk'] = $this->sbdk_model->get_edit($id);
        $data['data']['back'] = base_url() . 'log_history/sbdk';
        $this->load->view('templates/view', $data);

    }
    public function saving_ir()
    {
        $data = array();
        $data['content'] = "log_history/log_saving";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_saving'] = $this->log_history_model->view('SAVING');

        $this->load->view('templates/view', $data);
    }
    public function detail_saving_ir($id)
    {
        $data = array();
        $data['content'] = "log_history/detail_saving";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['saving_ir'] = $this->saving_ir_model->get_edit($id);
        $data['data']['detail'] = $this->saving_ir_model->get_detail_saving_ir($id);
        $data['data']['back'] = base_url() . 'log_history/saving_ir';
        $this->load->view('templates/view', $data);
    }
    public function giro_ir()
    {
        $data = array();
        $data['content'] = "log_history/log_giro";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_giro'] = $this->log_history_model->view('GIRO');

        $this->load->view('templates/view', $data);
    }
    public function detail_giro_ir($id)
    {
        $data = array();
        $data['content'] = "log_history/detail_giro";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['giro_ir'] = $this->giro_ir_model->get_edit($id);
        $data['data']['detail'] = $this->giro_ir_model->get_detail_giro_ir($id);
        $data['data']['back'] = base_url() . 'log_history/giro_ir';
        $this->load->view('templates/view', $data);
    }
    public function deposit_ir()
    {
        $data = array();
        $data['content'] = "log_history/log_deposito";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_deposit'] = $this->log_history_model->view('DEPOSITO');

        $this->load->view('templates/view', $data);
    }
    public function detail_deposit_ir($id)
    {
        $data = array();
        $data['content'] = "log_history/detail_deposito";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['deposit_ir'] = $this->deposit_ir_model->get_edit($id);
        $data['data']['detail'] = $this->deposit_ir_model->get_detail_deposit_ir($id);
        $data['data']['back'] = base_url() . 'log_history/deposit_ir';
        $this->load->view('templates/view', $data);
    }
    public function add_log_history()
    {
        $data = array();
        $data['content'] = "log_history/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'log_history';
        $data['data']['currency'] = $this->currency_model->get_currency();

        $this->load->view('templates/view', $data);
    }
    public function get_log_history_detail($id)
    {
        $this->db->select('A.*, C.CURRENCY_NAME, C.CURRENCY_CODE');
        $this->db->from('TR_KURS_DETAIL A');
        $this->db->join('TR_KURS_HEADER B ', 'A.ID_KURS_HEADER = B.ID_KURS_HEADER', 'left');
        $this->db->join('MS_CURRENCY C', ' A.ID_CURRENCY = C.ID_CURRENCY', 'left');
        $this->db->where('B.ID_KURS_HEADER', $id);
        $this->db->order_by('C.ID_CURRENCY', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }
    public function edit_log_history($id)
    {
        $data = array();
        $data['content'] = "log_history/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_history'] = $this->log_history_model->get_edit($id);
        $data['data']['detail'] = $this->get_log_history_detail($id);
        $data['data']['back'] = base_url() . 'log_history';
        $this->load->view('templates/view', $data);
    }
    public function detail_log_history($id)
    {
        $data = array();
        $data['content'] = "log_history/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['log_history'] = $this->log_history_model->get_edit($id);
        $data['data']['detail'] = $this->get_log_history_detail($id);
        $data['data']['back'] = base_url() . 'log_history';
        $this->load->view('templates/view', $data);
    }

    public function add()
    {
        // var_dump($_POST);exit();
        // var_dump($_FILES);exit();
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d H:i:s', strtotime($_POST['effective_date'])) : "";
        $DESCRIPTION = "Exchange Rates | Last Updated " . date("Y-m-d H:i:s");
        //detail
        $ID_CURRENCY = isset($_POST['id_currency']) ? $_POST['id_currency'] : "";
        $BUY = isset($_POST['buy']) ? $_POST['buyV'] : "";
        $SELL = isset($_POST['sell']) ? $_POST['sellV'] : "";

        $this->db->trans_begin();

        $dataheader['DESCRIPTION'] = $DESCRIPTION;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['CREATED_BY'] = $_SESSION['username'];
        $insert_header = $this->db->insert('TR_KURS_HEADER', $dataheader);
        $ID_KURS_HEADER = $this->db->insert_id();

        if (!$insert_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));

        } else {
            $detail = count($ID_CURRENCY);
            for ($i = 0; $i < $detail; $i++) {
                $datadetail['ID_KURS_HEADER'] = $ID_KURS_HEADER;
                $datadetail['ID_CURRENCY'] = $ID_CURRENCY[$i];
                $datadetail['BUY'] = $BUY[$i];
                $datadetail['SELL'] = $SELL[$i];
                $datadetail['CREATED_BY'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('TR_KURS_DETAIL', $datadetail);
                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }

            }

            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }

    public function view()
    {
        $GetData = $this->log_history_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        // var_dump($_POST);
        // var_dump($_FILES);exit();

        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d H:i:s', strtotime($_POST['effective_date'])) : "";
        $ID_KURS_HEADER = isset($_POST['id_log_history_header']) ? $_POST['id_log_history_header'] : "";

        $DESCRIPTION = "Exchange Rates | Last Updated " . date("Y-m-d H:i:s");
        //detail
        $ID_KURS_DETAIL = isset($_POST['id_log_history_detail']) ? $_POST['id_log_history_detail'] : "";
        $ID_CURRENCY = isset($_POST['id_currency']) ? $_POST['id_currency'] : "";
        $BUY = isset($_POST['buyV']) ? $_POST['buyV'] : "";
        $SELL = isset($_POST['sellV']) ? $_POST['sellV'] : "";

        $this->db->trans_begin();

        $dataheader['DESCRIPTION'] = $DESCRIPTION;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['USER_LOG'] = $_SESSION['username'];
        $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
        $where['ID_KURS_HEADER'] = $ID_KURS_HEADER;
        $update_header = $this->db->where($where)->update('TR_KURS_HEADER', $dataheader);

        if (!$update_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        }

        $cpt = count($ID_KURS_DETAIL);
        for ($i = 0; $i < $cpt; $i++) {
            $where['ID_KURS_DETAIL'] = $ID_KURS_DETAIL[$i];
            $datadetail['ID_KURS_HEADER'] = $ID_KURS_HEADER;
            $datadetail['ID_CURRENCY'] = $ID_CURRENCY[$i];
            $datadetail['BUY'] = $BUY[$i];
            $datadetail['SELL'] = $SELL[$i];

            $datadetail['DATE_LOG'] = date('Y-m-d H:i:s');
            $datadetail['USER_LOG'] = $_SESSION['username'];
            $update_detail = $this->db->where($where)->update('TR_KURS_DETAIL', $datadetail);
            if (!$update_detail) {
                $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                $this->db->trans_rollback();
                die(json_encode($JSON));
            }
        }

        if ($this->db->trans_status() === false) {
            $final_res = 'Failed: Failed to submit data';
            $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
            $this->db->trans_rollback();
            die(json_encode($JSON));

        } else {
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
            $this->db->trans_commit();
            die(json_encode($JSON));

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_KURS_HEADER = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";
        $param['ID_KURS_HEADER'] = $ID_KURS_HEADER;
        $Delete = $this->log_history_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_KURS_HEADER' => $id);
        $Active = $this->log_history_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_KURS_HEADER' => $id);
        $Active = $this->log_history_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KURS_HEADER']) ? $Data['ID_KURS_HEADER'] : "";

        $data['STATUS'] = "99";
        $data['APPROVED_BY'] = $_SESSION['username'];
        $data['APPROVED_DATE'] = date('Y-m-d H:i:s');
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_KURS_HEADER' => $id);
        $Active = $this->log_history_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Approved");
        die(json_encode($JSON));
    }
}
