<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Currency extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('currency_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "currency/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['currency'] = $this->currency_model->view();

        $this->load->view('templates/view', $data);
    }

    public function add()
    {
        $CURRENCY_NAME = isset($_POST['currency_name']) ? $_POST['currency_name'] : "";
        $CURRENCY_CODE = isset($_POST['currency_code']) ? $_POST['currency_code'] : "";
        $IS_HIGHLIGHT = isset($_POST['is_highlight']) ? $_POST['is_highlight'] : 0;
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($CURRENCY_NAME)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Name is required!");
            die(json_encode($JSON));
        }
        if (empty($CURRENCY_CODE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Code is required!");
            die(json_encode($JSON));
        }
        if (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
            die(json_encode($JSON));
        }
        $pathgallery = $this->config->item('save_currency');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size');
        $config['max_width'] = $this->config->item('max_width');
        $config['min_width'] = $this->config->item('min_width');
        $config['max_height'] = $this->config->item('max_height');
        $config['min_height'] = $this->config->item('min_height');
        $config['file_name'] = $CURRENCY_CODE . '_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_currency');
        $path_parts = pathinfo($_FILES["file"]["name"]);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_image');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {

                $check_duplicate = $this->currency_model->checkDuplicate('CURRENCY_CODE', $CURRENCY_CODE);
                if ($check_duplicate) {
                    $param['CURRENCY_NAME'] = $CURRENCY_NAME;
                    $param['CURRENCY_CODE'] = $CURRENCY_CODE;
                    $param['IS_HIGHLIGHT'] = $IS_HIGHLIGHT;
                    $param['LINK'] = $pathgallery . $config['file_name'] . "." . $forUpload['extension'];
                    $param['CREATED_BY'] = $_SESSION['username'];
                    $Insert = $this->currency_model->add($param);
                    if ($Insert['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                    die(json_encode($JSON));

                } else {
                    $JSON = array("ErrorCode" => "EC:000D", "ErrorMessage" => "Failed: Duplicate Currency Code");
                    die(json_encode($JSON));
                }

            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }

    }

    public function view()
    {
        $GetData = $this->currency_model->view();
        die(json_encode($GetData));
    }
    public function tncupload()
    {
        $data = 'Image max pixel ' . $this->config->item('max_height') . 'x' . $this->config->item('max_width') . 'px <br>
                    Allowed images are ' . str_replace("|", ", ", $this->config->item('allowed_types_image')) . ' <br>
                    Max image size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }

    public function update()
    {
        $CURRENCY_NAME = isset($_POST['currency_name']) ? $_POST['currency_name'] : "";
        $CURRENCY_CODE = isset($_POST['currency_code']) ? $_POST['currency_code'] : "";
        $ID_CURRENCY = isset($_POST['id_currency']) ? $_POST['id_currency'] : "";
        $IS_HIGHLIGHT = isset($_POST['is_highlight']) ? $_POST['is_highlight'] : 0;
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($CURRENCY_NAME)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Name is required!");
            die(json_encode($JSON));
        }
        if (empty($CURRENCY_CODE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Code is required!");
            die(json_encode($JSON));
        }
        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }
            $pathgallery = $this->config->item('save_currency');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size');
            $config['max_width'] = $this->config->item('max_width');
            $config['min_width'] = $this->config->item('min_width');
            $config['max_height'] = $this->config->item('max_height');
            $config['min_height'] = $this->config->item('min_height');
            $config['file_name'] = $CURRENCY_CODE . '_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_currency');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_image');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {

                    $check_duplicate = $this->currency_model->checkDuplicate('CURRENCY_CODE', $CURRENCY_CODE, 'ID_CURRENCY', $ID_CURRENCY);
                    if ($check_duplicate) {
                        $param['CURRENCY_NAME'] = $CURRENCY_NAME;
                        $param['CURRENCY_CODE'] = $CURRENCY_CODE;
                        $param['IS_HIGHLIGHT'] = $IS_HIGHLIGHT;
                        $param['LINK'] = $pathgallery . $config['file_name'] . "." . $forUpload['extension'];
                        $param['CREATED_BY'] = $_SESSION['username'];
                        $Update = $this->currency_model->updateaction($param, $ID_CURRENCY);
                        if ($Update['ErrorCode'] != "EC:0000") {
                            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                            die(json_encode($JSON));
                        }

                        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                        die(json_encode($JSON));

                    } else {
                        $JSON = array("ErrorCode" => "EC:000D", "ErrorMessage" => "Failed: Duplicate Currency Code");
                        die(json_encode($JSON));
                    }
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $check_duplicate = $this->currency_model->checkDuplicate('CURRENCY_CODE', $CURRENCY_CODE, 'ID_CURRENCY', $ID_CURRENCY);
            if ($check_duplicate) {
                $param['CURRENCY_NAME'] = $CURRENCY_NAME;
                $param['CURRENCY_CODE'] = $CURRENCY_CODE;
                $param['IS_HIGHLIGHT'] = $IS_HIGHLIGHT;
                $param['CREATED_BY'] = $_SESSION['username'];
                $Update = $this->currency_model->updateaction($param, $ID_CURRENCY);
                if ($Update['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                    die(json_encode($JSON));
                }

                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:000D", "ErrorMessage" => "Failed: Duplicate Currency Code");
                die(json_encode($JSON));
            }

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_CURRENCY = isset($Data['ID_CURRENCY']) ? $Data['ID_CURRENCY'] : "";
        $param['ID_CURRENCY'] = $ID_CURRENCY;
        $Delete = $this->currency_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_CURRENCY']) ? $Data['ID_CURRENCY'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_CURRENCY' => $id);
        $Active = $this->currency_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_CURRENCY']) ? $Data['ID_CURRENCY'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_CURRENCY' => $id);
        $Active = $this->currency_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_CURRENCY']) ? $Data['ID_CURRENCY'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_CURRENCY' => $id);
        $Active = $this->currency_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}