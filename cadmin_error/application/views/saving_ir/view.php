<?php
$SavingIR = isset($data['saving_ir']) ? $data['saving_ir'] : array();

$is_read = $authorize['is_read'];
$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Interest Rate</a>
                <a href="#" class="breadcrumb-item active">Saving Interest Rate</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">
            <a href="<?php echo base_url("saving_ir/add_saving_ir/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add Saving Interest Rate
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th>Category ID</th>
                    <th>Category EN</th>
                    <th data-orderable="false" style="width:150px">Effective Date</th>
                    <th data-orderable="false" style="width:150px">Approved Date</th>
                    <th>Approved By</th>
                    <th data-orderable="false">Status Approve</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($SavingIR as $saving_ir) {
    $ID_SAVING_IR_HEADER = isset($saving_ir['ID_SAVING_IR_HEADER']) ? $saving_ir['ID_SAVING_IR_HEADER'] : "";
    $EFFECTIVE_DATE = isset($saving_ir['EFFECTIVE_DATE']) ? date('d M Y', strtotime($saving_ir['EFFECTIVE_DATE'])) : "";
    $CATEGORY_SAVING = isset($saving_ir['CATEGORY_SAVING']) ? $saving_ir['CATEGORY_SAVING'] : "";
    $STATUS = isset($saving_ir['STATUS']) ? $saving_ir['STATUS'] : "";
    $CREATED_BY = isset($saving_ir['CREATED_BY']) ? $saving_ir['CREATED_BY'] : "";
    $CREATED_DATE = isset($saving_ir['CREATED_DATE']) ? date('d M Y', strtotime($saving_ir['CREATED_DATE'])) : "";
    $USER_LOG = isset($saving_ir['USER_LOG']) ? $saving_ir['USER_LOG'] : "";
    $DATE_LOG = isset($saving_ir['DATE_LOG']) ? date('d M Y', strtotime($saving_ir['DATE_LOG'])) : "";
    $APPROVED_DATE = isset($saving_ir['APPROVED_DATE']) ? date('d M Y', strtotime($saving_ir['APPROVED_DATE'])) : "";
    $APPROVED_BY = isset($saving_ir['APPROVED_BY']) ? $saving_ir['APPROVED_BY'] : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $saving_ir;
    $enc = base64_encode(json_encode($paramaction));

    if ($STATUS == "88") {
        $STS = "<span class='badge badge-danger'>Not Approved</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Approved</span>";
    } else {
        $STS = "";
    }
    $SAVING_ID = $this->config->item('SAVING_ID');
    $SAVING_EN = $this->config->item('SAVING_EN');
    $ID_CATEG_SAVING = $this->config->item('ID_CATEG_SAVING');
    $Exp_SAVING_ID = explode(":", $SAVING_ID);
    $Exp_SAVING_EN = explode(":", $SAVING_EN);
    $Exp_ID_CATEG_SAVING = explode(":", $ID_CATEG_SAVING);
    for ($i = 0; $i < sizeof($Exp_ID_CATEG_SAVING); $i++) {
        $ID = $Exp_ID_CATEG_SAVING[$i];

        if ($CATEGORY_SAVING == $ID) {
            $DESC_EN = $Exp_SAVING_EN[$i];
            $DESC_ID = $Exp_SAVING_ID[$i];
        }
    }

    ?>
                <tr>
                    <td>
                        <?php echo $DESC_ID ?>
                    </td>
                    <td>
                        <?php echo $DESC_EN ?>
                    </td>
                    <td>
                        <?php echo $EFFECTIVE_DATE ?>
                    </td>
                    <td>
                        <?php echo $APPROVED_DATE ?>
                    </td>
                    <td>
                        <?php echo $APPROVED_BY ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_read)) {?>
                                    <a href="<?php echo base_url("saving_ir/detail_saving_ir/") . $ID_SAVING_IR_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="icon-eye"></i> Detail
                                    </a>
                                    <?php }?>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("saving_ir/edit_saving_ir/") . $ID_SAVING_IR_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <?php $approval = $this->config->item("approval_saving_ir");if (in_array($_SESSION['level'], $approval)) {?>
                                    <?php if ($STATUS == "88") {?>
                                    <a onclick="approvedaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-check"></i> Approved</a>
                                    <?php }?>
                                    <?php }}?>


                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(6).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectApprovedDate(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(3)
            .search(dis.value)
            .draw();
    } else {
        t.column(3)
            .search(dis.value)
            .draw();
    }

}

function changeSelectEffectiveDate(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}
//Open Change Status active
function approvedaction(param) {
    var decrypt = JSON.parse(atob(param));
    var Url = decrypt.Url;
    Swal.fire({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, Approved!",
        cancelButtonText: "No, cancel!",
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: Url + "/publish",
                dataType: "JSON",
                data: {
                    Data: decrypt.Data,
                },
                type: "POST",
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 1500);
                    }
                },
            });
        }
    });
}
//END Change Status active
</script>