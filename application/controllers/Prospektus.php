<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prospektus extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{
		// echo 'test';exit;
		$lang = $_SESSION['lang'];
		$data = $this->db->query("SELECT * FROM MS_PROSPECTUS WHERE STATUS = '99' LIMIT 1")->row_array();
		$url = base_url();
		if(!empty($data)){
			if($lang == 'id'){
				$url = $data['LINK_ID'];
			}else{
				$url = $data['LINK_EN'];
			}
		}
		// echo $lang;exit;
		redirect($url);
		// header('Location : https://demo.sriwijayaair.co.id/bumi_artha/assets/file/prospektus/2021/APR/PROSPEKTUS_ID_210416121229.pdf');
	}
}

?>