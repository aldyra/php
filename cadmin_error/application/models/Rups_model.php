<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rups_model extends MY_Model
{

    protected $_table_name = 'MS_RUPS_HEADER';
    protected $_detail_table_name = 'MS_RUPS_DETAIL';
    protected $_primary_key = 'ID_RUPS_HEADER';
    protected $_detail_primary_key = 'ID_RUPS_DETAIL';
    protected $_order_by = 'ID_RUPS_HEADER';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }
    public function get_detail_rups($id)
    {
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result();
        return $detail;

    }
    public function get_detail($id)
    {
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result();
        $html = '';
        foreach ($detail as $row) {
            $html .= '<div class="card border-left-info border-right-info rounded-0" id="removeclassedit' . $row->ID_RUPS_DETAIL . '" data-id="' . $row->ID_RUPS_DETAIL . '">
                        <div class="card-body">
                        <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                    <div class="row">
                                <input type="hidden" value="' . $row->ID_RUPS_DETAIL . '" name="id_detail[]" id="id_detail">

                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>Name Indonesian<span style="color:orange">*</span></label>
                                                <input type="text" class="form-control" id="name_id" name="name_id[]"
                                                    placeholder="Description in Indonesian" required=""
                                                    parsley-trigger="change" value="' . $row->NAME_ID . '">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>Name English<span style="color:orange">*</span></label>
                                                <input type="text" class="form-control" id="name_en" name="name_en[]"
                                                    placeholder="Description in English" required=""
                                                    parsley-trigger="change" value="' . $row->NAME_EN . '">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>File Upload Indonesia<span style="color:orange">*</span></label>
                                                <input type="file" name="file_id[]" id="file_id"
                                                    class="form-control-uniform" data-fouc
                                                    data-parsley-errors-container="#validation-error-block-id' . $row->ID_RUPS_DETAIL . ' ">
                                                <div class="" id="validation-error-block-id' . $row->ID_RUPS_DETAIL . '"></div>
                                                <a href="' . $row->LINK_ID . '" data-popup="lightbox" data-fancybox>Show File</a>
                                                <input type="hidden" name="FILE_URL_ID[]" id="FILE_URL_ID" value="' . $row->LINK_ID . '">

                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>File Upload English<span style="color:orange">*</span></label>
                                                <input type="file" name="file_en[]" id="file_en"
                                                    class="form-control-uniform" data-fouc
                                                    data-parsley-errors-container="#validation-error-block-en' . $row->ID_RUPS_DETAIL . '">
                                                <div class="" id="validation-error-block-en' . $row->ID_RUPS_DETAIL . '"></div>
                                                <a href="' . $row->LINK_EN . '" data-popup="lightbox" data-fancybox>Show File</a>
                                                <input type="hidden" name="FILE_URL_EN[]" id="FILE_URL_EN" value="' . $row->LINK_EN . '">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                    <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                        type="button" onclick="remove(\'edit' . $row->ID_RUPS_DETAIL . '\');"><b><i class="fas fa-minus"></i></b>
                                        Remove
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>';
        }
        return $html;
    }
    public function updateaction($param)
    {
        $ID_RUPS_HEADER = isset($param['ID_RUPS_HEADER']) ? $param['ID_RUPS_HEADER'] : "";

        $WHERE['ID_RUPS_HEADER'] = $ID_RUPS_HEADER;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_RUPS_HEADER'] = isset($param['ID_RUPS_HEADER']) ? $param['ID_RUPS_HEADER'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

}