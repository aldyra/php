<?php
$Management = isset($Data['management']) ? $Data['management'] : array();

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Master Management</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">
            <a href="<?php echo base_url("management/add_management/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add Management
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Image</th>
                    <th width="200">Name</th>
                    <th width="">Position</th>
                    <th width="">Position Indonesia</th>
                    <th width="">Position English</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Management as $management) {
    $ID_MANAGEMENT_HEADER = isset($management['ID_MANAGEMENT_HEADER']) ? $management['ID_MANAGEMENT_HEADER'] : "";
    $LINK = isset($management['LINK']) ? $management['LINK'] : "";
    $NAME = isset($management['NAME']) ? $management['NAME'] : "";
    $POSITION_CATEGORY = isset($management['POSITION_CATEGORY']) ? $management['POSITION_CATEGORY'] : "";
    $POSITION_ID = isset($management['POSITION_ID']) ? $management['POSITION_ID'] : "";
    $POSITION_EN = isset($management['POSITION_EN']) ? $management['POSITION_EN'] : "";
    $STATUS = isset($management['STATUS']) ? $management['STATUS'] : "";
    $CREATED_BY = isset($management['CREATED_BY']) ? $management['CREATED_BY'] : "";
    $CREATED_DATE = isset($management['CREATED_DATE']) ? date('d M Y', strtotime($management['CREATED_DATE'])) : "";
    $USER_LOG = isset($management['USER_LOG']) ? $management['USER_LOG'] : "";
    $DATE_LOG = isset($management['DATE_LOG']) ? date('d M Y', strtotime($management['DATE_LOG'])) : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $management;
    $enc = base64_encode(json_encode($paramaction));

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    $POSITION_CATEG_ID = $this->config->item('POSITION_ID');
    $POSITION_CATEG_EN = $this->config->item('POSITION_EN');
    $ID_CATEG_POSITION = $this->config->item('ID_CATEG_POSITION');
    $Exp_POSITION_ID = explode(":", $POSITION_CATEG_ID);
    $Exp_POSITION_EN = explode(":", $POSITION_CATEG_EN);
    $Exp_ID_CATEG_POSITION = explode(":", $ID_CATEG_POSITION);
    for ($i = 0; $i < sizeof($Exp_ID_CATEG_POSITION); $i++) {
        $ID = $Exp_ID_CATEG_POSITION[$i];
        $DESC_EN = $Exp_POSITION_EN[$i];
        $DESC_ID = $Exp_POSITION_ID[$i];

        if ($POSITION_CATEGORY == $ID) {
            $POSITION_CATEGORY = $DESC_ID . '/' . $DESC_EN;
        }
    }

    ?>
                <tr>
                    <td>
                        <a href='<?php echo $LINK ?>' data-popup='lightbox' data-fancybox><img src='<?php echo $LINK ?>'
                                class='img-thumbnail' width='80px' heigh='80px'></a>
                    </td>
                    <td>
                        <?php echo $NAME ?>
                    </td>
                    <td>
                        <?php echo $POSITION_CATEGORY ?>
                    </td>
                    <td>
                        <?php echo $POSITION_ID ?>
                    </td>
                    <td>
                        <?php echo $POSITION_EN ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo base_url("management/detail_management/") . $ID_MANAGEMENT_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("management/edit_management/") . $ID_MANAGEMENT_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <?php if ($STATUS == "0") {?>
                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i> Activate</a>
                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i> Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>
                                    <?}elseif($STATUS == "99"){?>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i> Deactive</a>
                                    <?php }?>
                                    <?php }?>
                                    <?php if (!empty($is_delete)) {?>
                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?>

                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(6).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(6)
            .search(dis.value)
            .draw();
    } else {
        t.column(6)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}
</script>