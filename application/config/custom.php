<?php
date_default_timezone_set('Asia/Jakarta');

$config['HOST'] = "https://".$_SERVER['HTTP_HOST']."/";
$config['LANGUAGE'] = "English:Bahasa Indonesia";
$config['ID_LANGUAGE'] = "EN:ID";
$config['STATUS'] = "Inactive:Active:Publish";
$config['ID_STATUS'] = "0:88:99";

$config['allowed_types'] = "*";
$config['allowed_types_file'] = 'JPEG|JPG|PNG|GIF';
$config['allowed_types_video'] = 'MPEG|MJPEG|MP4|WMV|AVI|M4V';
$config['allowed_types_file_video'] = 'JPEG|JPG|PNG|GIF|MPEG|MJPEG|MP4|WMV|AVI|M4V';

$config['path_banner'] = $_SERVER['DOCUMENT_ROOT']."assets/file/banner/".strtoupper(date('Y/M'))."/";
$config['save_banner'] = $config['HOST']."assets/file/banner/".strtoupper(date('Y/M'))."/";

$config['path_news'] = $_SERVER['DOCUMENT_ROOT']."assets/file/news/".strtoupper(date('Y/M'))."/";
$config['save_news'] = $config['HOST']."assets/file/news/".strtoupper(date('Y/M'))."/";

$config['path_video'] = $_SERVER['DOCUMENT_ROOT']."assets/file/video/".strtoupper(date('Y/M'))."/";
$config['save_video'] = $config['HOST']."assets/file/video/".strtoupper(date('Y/M'))."/";

// Url Job Vacancy
$config['url_jobvacancy_id'] = "lowongan-kerja";
$config['url_jobvacancy_en'] = "job-vacancy";