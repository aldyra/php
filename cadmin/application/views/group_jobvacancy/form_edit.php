<?php
$param = isset($param) ? $param : "";
$ID = isset($param['ID']) ? $param['ID'] : "";
$NAME = isset($param['NAME']) ? $param['NAME'] : "";
$LANG = isset($param['LANGUAGE']) ? $param['LANGUAGE'] : "";
?>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        LANGUAGE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="hidden" name="id_group_jobvacancy" class="form-control" id="id" value="<?php echo $ID ?>">
        <select class="form-control" name="language" id="language" required="">
            <?php
$LANGUAGE = $this->config->item('LANGUAGE');
$ID_LANGUAGE = $this->config->item('ID_LANGUAGE');
$Exp_ID = explode(":", $ID_LANGUAGE);
$Exp_DESC = explode(":", $LANGUAGE);
for ($i = 0; $i < sizeof($Exp_ID); $i++) {
    $ID = $Exp_ID[$i];
    $DESC = $Exp_DESC[$i];
    ?>
            <option value="<?php echo $ID ?>" <?php if ($LANG == $ID) {echo 'selected';}?>><?php echo $DESC ?>
            </option>
            <?php
}
?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        NAME<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" name="name" class="form-control" id="name" value="<?php echo $NAME ?>">
    </div>
</div>

<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>