<style>
	.row-socialize{
		float: right;
		text-align: left;
		margin-top: 1rem;
	}
	.row-socialize .bagikan_text{
			font-size: 14px;
		}
	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.row-socialize{
			float: unset;
			text-align: center;
		}

	}
</style>

<div class="row-socialize">
	<p class="bagikan_text" style="color: #243E8B"><?php echo $this->lang->line('share') ?></p>

	<a href="<?php echo 'https://twitter.com/share?url='.base_url(uri_string()) ?>" target="_blank">
		<img src="<?php echo base_url('assets/image/socialize/SOCIAL_MEDIA_210511140517.png'); ?>" style="width: 25px; height: 25px; ">
	</a>&nbsp;
	<a href="<?php echo 'http://www.facebook.com/sharer.php?u='.base_url(uri_string()) ?>" target="_blank">
		<img src="<?php echo base_url('assets/image/socialize/SOCIAL_MEDIA_210511133132.png'); ?>" style="width: 25px; height: 25px; ">
	</a>&nbsp;
	<a href="<?php echo 'http://www.linkedin.com/shareArticle?mini=true&amp;url='.base_url(uri_string()) ?>" target="_blank">
		<img src="<?php echo base_url('assets/image/socialize/SOCIAL_MEDIA_210511133201.png'); ?>" style="width: 25px; height: 25px; ">
	</a>&nbsp;
	<!-- <a href="<?php echo 'https://www.instagram.com/share?'.base_url(uri_string()) ?>" target="_blank">
		<img src="<?php echo base_url('assets/image/socialize/instagram.png'); ?>" style="width: 25px; height: 25px; ">
	</a>&nbsp; -->
	<a href="<?php echo 'https://api.whatsapp.com/send?text='.base_url(uri_string()) ?>" target="_blank">
		<img src="<?php echo base_url('assets/image/socialize/whatsapp.png'); ?>" style="width: 25px; height: 25px; ">
	</a>&nbsp;
	<!-- <a href="<?php echo 'https://plus.google.com/share?url=='.base_url(uri_string()) ?>" target="_blank">
		<img src="<?php echo base_url('assets/image/socialize/SOCIAL_MEDIA_210511140447.png'); ?>" style="width: 25px; height: 25px; ">
	</a>&nbsp; -->
</div>