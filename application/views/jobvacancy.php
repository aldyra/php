<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$MENU_CHILD_LINK = isset($navbar_link['MENU_CHILD_LINK']) ? $navbar_link['MENU_CHILD_LINK']:"";
$Url = base_url().$SiteLang."/".$MENU_CHILD_LINK."/".$this->lang->line('lowker-0');

$FILE_BANNER = isset($data['content']['MENU_CHILD_FILE_BANNER']) ? $data['content']['MENU_CHILD_FILE_BANNER']:"";
$stylebanner = "height:250px";
if(!empty($FILE_BANNER)){
	$stylebanner = "background-image:url('".$FILE_BANNER."'); background-size:cover;height:250px;";
}
?>
<div class="row row-header" style="<?php echo $stylebanner ?>">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important;"><?php echo $this->lang->line('lowker-13') ?></h4>
			<?php echo $this->lang->line('lowker-14') ?>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color: #707070;"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>" style="color: #707070;"> -->
			<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<!-- </a> / -->
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<div class="container" style="padding-top: 20px; padding-bottom: 50px;"> 
	<div class="row d-padtop-10">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 t-center d-padtop-10" style="padding-bottom: 30px;">
			<h3 class="t-default"><b><?php echo $this->lang->line('lowker-1') ?></b></h3>
		</div>
		<?php 
		foreach($jobvacancy as $row){
			$ID = isset($row['ID']) ? $row['ID']:"";
			$NAME_HEADER = isset($row['NAME']) ? $row['NAME']:"";
			$DETAIL = isset($row['DETAIL']) ? $row['DETAIL']:"";
			$total = sizeof($DETAIL);
			$UrlDetail = $Url."/".slugify($NAME_HEADER);
		?>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 t-center d-pad-10" style="padding-left: 30px; padding-right: 30px;">
				<div class="card card-loker" onclick="window.location.href='<?php echo $UrlDetail ?>'">
					<div class="card-body" style="margin-top: 20px; margin-bottom: 20px;">
						<span class='t-default' style="font-size: 18px;font-weight: 700;"><?php echo $NAME_HEADER ?></span><br><br>
						<span style="font-size: 14px;'"><?php echo $total ?> <?php echo ucwords($this->lang->line('opening')) ?></span>
					</div>
				</div>
			</div>
		<?php 
		} 
		?>
	</div>

	<div style="max-width: 100%" class="mb-3 mt-3">
		<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
		<div style="clear:both"></div>
	</div>
</div>

<style type="text/css">
	div.card-loker{
		box-shadow: 0px 1px 3px 1px #dfdfde;
		cursor: pointer;
		background-color: #F5F5F5;
		border:none;
	}

	div.card-loker:hover{
		background-color: #28448f;
	}

	div.card-loker:hover span{
		color: white !important;
	}
</style>