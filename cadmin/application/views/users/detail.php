<?php
$profile = isset($param['profile']) ? $param['profile'] : array();
$group = isset($param['group']) ? $param['group'] : array();
$ID = isset($profile['ID']) ? $profile['ID'] : "";
$USERNAME = isset($profile['USERNAME']) ? $profile['USERNAME'] : "";
$GROUP_ID = isset($profile['GROUP_ID']) ? $profile['GROUP_ID'] : "";
$NAME = isset($profile['NAME']) ? $profile['NAME'] : "";
$EMAIL = isset($profile['EMAIL']) ? $profile['EMAIL'] : "";
$POSITION = isset($profile['POSITION']) ? $profile['POSITION'] : "";

$group = isset($rows['group']) ? $rows['group'] : array();
$decrypt = json_decode(base64_decode($group));
$group = $decrypt->group;
?>
<input type="hidden" name="IDUser" id="IDUser" value="<?php echo $ID ?>">
<div class="form-group row">
    <label for="username" class="col-sm-3 col-form-label">Username</label>
    <div class="col-sm-9">
        <input type="text" name="username" required="" pattern="/^([a-zA-Z0-9 ])+$/" data-parsley-maxlength="15"
            data-parsley-minlength="2" data-parsley-required-message="Username is required."
            data-parsley-pattern-message="Username cannot contain any special characters."
            data-parsley-maxlength-message="Username is too long. It should have 15 characters or fewer."
            data-parsley-maxlength-message="Username is too short. It should have 2 characters or more."
            placeholder="Enter Username" class="form-control" id="usernameEdit" value="<?php echo $USERNAME ?>"
            readonly>
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        <input type="text" name="name" required="" data-parsley-maxlength="50" data-parsley-minlength="2"
            data-parsley-required-message="Name is required."
            data-parsley-maxlength-message="Name is too long. It should have 50 characters or fewer."
            data-parsley-maxlength-message="Name is too short. It should have 2 characters or more."
            placeholder="Enter Name" class="form-control" id="nameEdit" value="<?php echo $NAME ?>" readonly>
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-sm-3 col-form-label">Email</label>
    <div class="col-sm-9">
        <input type="email" name="email" required="" data-parsley-required-message="Email is required."
            data-parsley-type-message="Email should be a valid email." placeholder="Enter Email" class="form-control"
            id="emailEdit" value="<?php echo $EMAIL ?>" readonly>
    </div>
</div>

<div class="form-group row">
    <label for="position" class="col-sm-3 col-form-label">User Group</label>
    <div class="col-sm-9">
        <?php foreach ($group as $groupuser) {
    $ID = isset($groupuser->ID) ? $groupuser->ID : "";
    $USER_GROUP_NAME = isset($groupuser->USER_GROUP_NAME) ? $groupuser->USER_GROUP_NAME : "";
    ?>
        <?php if ($ID == $GROUP_ID) {?>
        <input class="form-control" value="<?php echo $USER_GROUP_NAME ?>" readonly>
        <?php }}?>
    </div>
</div>
<div class="form-group row">
    <label for="position" class="col-sm-3 col-form-label">Position</label>
    <div class="col-sm-9">
        <input type="text" name="position" required="" data-parsley-required-message="Position is required."
            placeholder="Enter Position" class="form-control" id="position" value="<?php echo $POSITION ?>" readonly>
    </div>
</div>