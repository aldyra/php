<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kesra_winner extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('kesra_winner_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "kesra_winner/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['kesra_winner'] = $this->kesra_winner_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Allowed files are ' . str_replace("|", ", ", $this->config->item('allowed_types_file')) . ' <br>
                    Max file size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }
    public function add()
    {
        $PERIODE = isset($_POST['periode']) ? $_POST['periode'] : "";
        $DATE = isset($_POST['date']) ? date('Y-m-d', strtotime($_POST['date'])) : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File is required!");
            die(json_encode($JSON));
        }

        $pathkesra_winner = $this->config->item('save_kesra_winner');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size');
        $config['max_width'] = $this->config->item('max_width');
        $config['min_width'] = $this->config->item('min_width');
        $config['max_height'] = $this->config->item('max_height');
        $config['min_height'] = $this->config->item('min_height');
        $config['file_name'] = 'KESRA_WINNER_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_kesra_winner');
        $path_parts = pathinfo($_FILES["file"]["name"]);
        $forUpload['extension'] = $path_parts['extension'];
        $Extension = $this->config->item('allowed_types_file');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $param['PERIODE'] = $PERIODE;
                $param['DATE'] = $DATE;
                $param['LINK'] = $pathkesra_winner . $config['file_name'] . "." . $forUpload['extension'];
                $param['CREATED_BY'] = $_SESSION['username'];
                $Insert = $this->kesra_winner_model->add($param);
                if ($Insert['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                    die(json_encode($JSON));
                }
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->kesra_winner_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID = isset($_POST['id']) ? $_POST['id'] : "";
        $PERIODE = isset($_POST['periode']) ? $_POST['periode'] : "";
        $DATE = isset($_POST['date']) ? date('Y-m-d', strtotime($_POST['date'])) : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File is required!");
                die(json_encode($JSON));
            }

            $pathkesra_winner = $this->config->item('save_kesra_winner');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size');
            $config['max_width'] = $this->config->item('max_width');
            $config['min_width'] = $this->config->item('min_width');
            $config['max_height'] = $this->config->item('max_height');
            $config['min_height'] = $this->config->item('min_height');
            $config['file_name'] = 'KESRA_WINNER_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_kesra_winner');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_file');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $param['PERIODE'] = $PERIODE;
                    $param['DATE'] = $DATE;
                    $param['LINK'] = $pathkesra_winner . $config['file_name'] . "." . $forUpload['extension'];
                    $param['USER_LOG'] = $_SESSION['username'];
                    $param['DATE_LOG'] = date('Y-m-d H:i:s');
                    $Update = $this->kesra_winner_model->updateaction($param, $ID);
                    if ($Update['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
                    die(json_encode($JSON));
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $param['PERIODE'] = $PERIODE;
            $param['DATE'] = $DATE;
            $param['USER_LOG'] = $_SESSION['username'];
            $param['DATE_LOG'] = date('Y-m-d H:i:s');
            $Update = $this->kesra_winner_model->updateaction($param, $ID);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        }
    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_KESRA_WINNER = isset($Data['ID_KESRA_WINNER']) ? $Data['ID_KESRA_WINNER'] : "";
        $param['ID_KESRA_WINNER'] = $ID_KESRA_WINNER;
        $Delete = $this->kesra_winner_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KESRA_WINNER']) ? $Data['ID_KESRA_WINNER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_KESRA_WINNER' => $id);
        $Active = $this->kesra_winner_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KESRA_WINNER']) ? $Data['ID_KESRA_WINNER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_KESRA_WINNER' => $id);
        $Active = $this->kesra_winner_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_KESRA_WINNER']) ? $Data['ID_KESRA_WINNER'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_KESRA_WINNER' => $id);
        $Active = $this->kesra_winner_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}