<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    protected $_table_name;
    protected $_order_by;
    protected $_order_by_type;
    protected $_primary_filter = 'intval';
    protected $_primary_key;
    protected $_type;
    public $rules;

    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $batch = false)
    {
        if ($batch == true) {
            $this->db->insert_batch($this->_table_name, $data);
        } else {
            $this->db->set($data);
            $this->db->insert($this->_table_name);
            $id = $this->db->insert_id();
            return $id;
        }
    }

    public function update($data, $where = array())
    {
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($this->_table_name);
    }

    public function get($id = null, $single = false)
    {
        if ($id != null) {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        } elseif ($single == true) {
            $method = 'row';
        } else {
            $method = 'result';

        }

        if ($this->_order_by_type) {
            $this->db->order_by($this->_order_by, $this->_order_by_type);
        } else {
            $this->db->order_by($this->_order_by);
        }

        // return $this->db->get($this->_table_name)->$method();
        return $this->db->get($this->_table_name)->$method();

    }

    public function get_by($where = null, $limit = null, $offset = null, $single = false, $select = null)
    {
        if ($select != null) {
            $this->db->select($select);
        }

        if ($where != null) {
            $this->db->where($where);
        }

        if (($limit) && ($offset)) {
            $this->db->limit($limit, $offset);
        } elseif (($limit)) {
            $this->db->limit($limit);
        }

        return $this->get(null, $single);
    }

    public function delete($id)
    {
        $filter = $this->_primary_filter;
        $id = $filter($id);

        if (!$id) {
            return false;
        }

        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $delete = $this->db->delete($this->_table_name);
        if ($delete) {
            return true;
        }
    }

    public function delete_by($where = null)
    {
        if ($where) {
            $this->db->where($where);
        }

        $this->db->delete($this->_table_name);
    }

    public function count($where = null)
    {
        if (!empty($this->_type)) {
            $where['post_type'] = $this->_type;
        }

        if ($where) {
            $this->db->where($where);
        }

        $this->db->from($this->_table_name);
        return $this->db->count_all_results();
    }

    public function unique_update($value, $id, $field)
    {
        $get_data = $this->get($id);
        $value = strtolower($value);
        $get_field = strtolower($get_data->$field);

        if ($value == $get_field) {
            $require = '';
        } else {
            $require = '|is_unique[' . $this->_table_name . '.' . $field . ']';
        }

        return $require;
    }
    public function updatestatus($arr_data, $arr_where)
    {
        $run = $this->db->update($this->_table_name, $arr_data, $arr_where);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;

    }
    public function checkDuplicate($field_where, $name, $field_id = null, $id = null)
    {
        if ($id == null) {
            $this->db->where($field_where, $name);
            $num = $this->db->get($this->_table_name)->num_rows();
            if ($num >= 1) {
                return false;
            } else {
                return true;
            }

        } else {
            $this->db->where($field_where, $name);
            $this->db->where_not_in($field_id, $id);
            $num = $this->db->get($this->_table_name)->num_rows();
            if ($num >= 1) {
                return false;
            } else {
                return true;
            }

        }

        $this->db->close();
    }

}