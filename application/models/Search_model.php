<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getcontent($keywords)
    {
    	$SiteLang = strtoupper(isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"");
    	$TITLE = $this->cekLang('TITLE');
    	$DESCRIPTION = $this->cekLang('DESCRIPTION');

    	$Query = "SELECT ID, ID_MENU, $TITLE AS TITLE, $DESCRIPTION AS DESCRIPTION, CREATED_DATE, CREATED_BY FROM MS_CONTENT 
    	WHERE STATUS = '99' AND (".$TITLE." LIKE '%".$keywords."%' OR ".$DESCRIPTION." LIKE '%".$keywords."%')";
    	$Run = $this->db->query($Query);
    	$Row = $Run->result();
    	return $Row;
    }

    public function getvideo($keywords)
    {
    	$SiteLang = strtoupper(isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"");
    	$Query = "SELECT * FROM MS_VIDEO
    	WHERE STATUS = '99' AND LANGUAGE = '".$SiteLang."' AND (TITLE LIKE '%".$keywords."%' OR DESCRIPTION LIKE '%".$keywords."%') LIMIT 1";
    	$Run = $this->db->query($Query);
    	$Row = $Run->result();
    	return $Row;
    }

    public function getcountcontent($keywords)
    {
    	$SiteLang = strtoupper(isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"");
    	$TITLE = $this->cekLang('TITLE');
    	$DESCRIPTION = $this->cekLang('DESCRIPTION');

    	$Query = "SELECT ID, ID_MENU, $TITLE AS TITLE, $DESCRIPTION AS DESCRIPTION, CREATED_DATE, CREATED_BY FROM MS_CONTENT 
    	WHERE STATUS = '99' AND (".$TITLE." LIKE '%".$keywords."%' OR ".$DESCRIPTION." LIKE '%".$keywords."%')";
    	$Run = $this->db->query($Query);
    	$Row = $Run->result();
    	return sizeof($Row);
    }

    public function getmodule($keywords)
    {
        $SiteLang = strtoupper(isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"");
        $NAME = $this->cekLang('NAME');
        $LINK = $this->cekLang('LINK');

        $Query = "SELECT ID, ".$NAME." AS NAME, ".$LINK." AS LINK, CREATED_DATE, CREATED_BY 
        FROM MS_MENU 
        WHERE STATUS = '99' AND (".$NAME." LIKE '%".$keywords."%' OR ".$LINK." LIKE '%".$keywords."%')";
        $Run = $this->db->query($Query);
        $Row = $Run->result();
        return $Row;
    }

    public function cekLang($name)
	{
		if ($_SESSION['site_lang'] == 'id') {
			$nameLang = $name.'_ID';
		}elseif ($_SESSION['site_lang'] == 'en'){
			$nameLang = $name.'_EN';
		}

		return $nameLang;
	}
}