<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login - Web Admin Bumi Arta</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <link href="<?php echo base_url(); ?>assets/lib/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <style>
    .form-control.parsley-error {
        border-color: #ea3a3a;
    }

    ul.parsley-errors-list {
        margin: 5px 0 0;
        padding-left: 15px;
    }

    ul.parsley-errors-list li {
        color: #fd0d0d;
        list-style: none;
    }

    .mouse-cursor-gradient-tracking {
        position: relative;
        /* background: #accbee; */
        /* padding: 0.5rem 1rem; */
        /* font-size: 1.2rem; */
        border: none;
        color: #e7f0fd;
        cursor: pointer;
        outline: none;
        overflow: hidden;
    }

    .mouse-cursor-gradient-tracking span {
        position: relative;
    }

    .mouse-cursor-gradient-tracking:before {
        --size: 0;
        content: '';
        position: absolute;
        left: var(--x);
        top: var(--y);
        width: var(--size);
        height: var(--size);
        background: radial-gradient(circle closest-side, #accbee, transparent);
        transform: translate(-50%, -50%);
        transition: width 0.2s ease, height 0.2s ease;
    }

    .mouse-cursor-gradient-tracking:hover:before {
        --size: 200px;
    }
    </style>
</head>

<body class="bg-gradient">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login card -->
                <form id="auth_form" class="login-form" method="POST"
                    action="<?php echo base_url('Authentication/doLogin'); ?>">
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <img class="logo-login"
                                    src="<?php echo base_url('assets/images/logo-bumi-artha.png') ?>" alt=""
                                    width="100px">
                                <h5 class="mb-0">Login to your account</h5>
                                <span class="d-block text-muted">Your credentials</span>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control" placeholder="Username" name="uname" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" class="form-control" placeholder="Password" name="pass" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group d-flex align-items-center">
                                <div class="form-check mb-0">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="remember" class="form-input-styled" checked
                                            data-fouc>
                                        Remember
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit"
                                    class="btn btn-primary btn-block mouse-cursor-gradient-tracking">Sign in <i
                                        class="icon-circle-right2 ml-2"></i></button>
                            </div>
                            <div class="text-center">
                                <a href="<?php echo base_url('forgotpassword') ?>">Forgot password?</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /login card -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo_pages/login.js"></script>
    <!-- /theme JS files -->
    <!-- Validation js (Parsleyjs) -->
    <script src="<?php echo base_url(); ?>assets/lib/parsleyjs/parsley.min.js"></script>
    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url(); ?>assets/lib/sweetalert2/sweetalert2.min.js"></script>
    <!-- Sweet alert init js-->
    <script src="<?php echo base_url(); ?>assets/lib/sweetalert2/sweet-alerts.init.js"></script>
    <script type="text/javascript">
    let btn = document.querySelector('.mouse-cursor-gradient-tracking');
    btn.addEventListener('mousemove', e => {
        let rect = e.target.getBoundingClientRect();
        let x = e.clientX - rect.left;
        let y = e.clientY - rect.top;
        btn.style.setProperty('--x', x + 'px');
        btn.style.setProperty('--y', y + 'px');
    });
    $(document).ready(function() {
        $("#auth_form").parsley();
        $('#auth_form').on('submit', function(e) {
            e.preventDefault();
            if ($(this).parsley().isValid()) {
                $.ajax({
                    url: "<?php echo base_url('Authentication/dologin'); ?>",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    type: 'POST',
                    success: function(data) {
                        if (data == false) {
                            Swal.fire({
                                type: 'error',
                                title: 'Wrong Username or Password'
                            });
                        } else if (data[0].ID) {
                            window.location.href = "<?php echo base_url(); ?>";
                        } else if ((data.indexOf("Warning") != -1)) {
                            Swal.fire({
                                type: 'warning',
                                title: data
                            }).then(function() {
                                window.location.href =
                                    "<?php echo base_url('authentication/changePass') ?>"
                            });
                        } else if ((data.indexOf("Failed") != -1)) {
                            Swal.fire({
                                type: 'error',
                                title: data
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Swal.fire(textStatus);
                    }
                });
            }
        });
    });
    </script>
</body>

</html>