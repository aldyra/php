<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>


<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<!-- <a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"> -->
			<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<!-- </a> / -->
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<style>
	.row-socialize{
		margin-bottom: 50px;
	}

	.box-penghargaan{
		/*width: 400px;*/
	}
	.item-penghargaan{
		padding: 5px 0;
	}
	.item-penghargaan img{
		max-width: 100px;
	}

	.item-penghargaan .title{
		font-weight: bold;
		color: #00326c;
		font-size: 15px;
	}

	.item-penghargaan .desc{
		font-size: 14px;
		/*max-width: 400px;*/
		max-width: 100%;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-sbdk {
		    max-width: 100%;
		}
		.title-laporan{
			margin-top: 1rem;
			font-size: 20px; 
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}
	}
</style>

<div class="container-fluid d-padding" style="padding-top: 20px; padding-bottom: 50px;">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
			<?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-8">
			<h3 class="mb-4 title-laporan" style="font-size: 18px;"><strong><?php echo $this->lang->line('penghargaan_0') ?></strong></h3>

			<div class="box-penghargaan">
				<?php foreach($contentData['getAward'] as $key => $val){ ?>
				<div class="item-penghargaan d-flex">
					<a target="_blank" href="<?=$val['LINK']?>"><img class="mr-2" src="<?=$val['LINK']?>" alt="<?php echo 'gambar_'.$val['TITLE'] ?>"></a>
					<div class="title-desc" style="text-align: justify;">
						<p class="title mb-2"><?php echo $val['TITLE'] ?></p>
						<p class="desc mb-0"><?php echo $val['DESCRIPTION'] ?></p>
					</div>
				</div>
					<?php if( $key != (count($contentData['getAward']) - 1) ) { ?>
					<hr class="garis-batas">
					<?php } ?>
				<?php } ?>
			</div>
			
			<div style="max-width: 95%;"><?php require_once(APPPATH.'views/share_sosmed.php'); ?></div>
		</div>
	</div>
</div>