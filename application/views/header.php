<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>::: Bank Bumi Arta - Teman Anda Dalam Usaha :::</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>

    <link rel="shortcut icon" href="<?php echo base_url('assets/image/logo-bumi-artha.png')?>">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fontawesome5/css/all.css">
    <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/fontawesome4/css/font-awesome.css"> -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/Css/navbar.css?>">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/nunito/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/DataTables/css/dataTables.bootstrap4.min.css" type="text/css" />

    <?php
        if(!empty($cssName)){ 
            $AddtionalCss = explode(":",$cssName);
            for($i=0;$i<sizeof($AddtionalCss);$i++){
            ?>
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/<?= $AddtionalCss[$i] ?>?v=1">
            <?php
            }
        }
    ?>
    <script src="<?php echo base_url() ?>assets/js/jquery-3.4.0.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/fontawesome5/js/all.js"></script>
    <script src="<?php echo base_url() ?>assets/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/OwlCarousel2/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>assets/DataTables/js/jquery.dataTables.min.js"  type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/DataTables/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

    <?php
        if(!empty($jsName)){ 
            $AddtionalJs = explode(":",$jsName);
            for($i=0;$i<sizeof($AddtionalJs);$i++){
            ?>
                <script src="<?php echo base_url() ?>assets/custom/<?= $AddtionalJs[$i] ?>?v=1"></script>
            <?php
            }
        }
    ?>
</head>