<?php
$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];
$param['Controllers'] = $Controllers;
$param['Url'] = $Url;
$param['Modal'] = $Modal;
$param['Data'] = $Data;
$enc = base64_encode(json_encode($param));
?>
<div class="list-icons">
    <div class="dropdown">
        <a href="#" class="list-icons-item" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <?php if (!empty($is_update)) {?>
            <a onclick="updateaction('<?php echo $enc ?>')" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>

            <?php if ($Data['STATUS'] == "0") {?>

            <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i class="fas fa-toggle-on"></i>
                Activate</a>

            <?}elseif($Data['STATUS'] == "88"){?>

            <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i class="icon-move-up"></i>
                Publish</a>

            <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i class="fas fa-toggle-off"></i>
                Deactive</a>

            <?}elseif($Data['STATUS'] == "99"){?>

            <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i class="fas fa-toggle-off"></i>
                Deactive</a>

            <?php }?>
            <?php }?>
            <?php if (!empty($is_delete)) {?>
            <?php if ($Data['STATUS'] == "0") {?>

            <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
            <?php }}?>
        </div>
    </div>
</div>