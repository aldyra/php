<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Content extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('content_model');
        $this->load->model('menu_model');
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "content/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['content'] = $this->content_model->view();
        $data['Data']['menu'] = $this->menu_model->view();
        $this->load->view('templates/view', $data);
    }
    public function add_content()
    {
        $data = array();
        $data['content'] = "content/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'content';
        $data['data']['menu'] = $this->menu_model->menu_content();
        $this->load->view('templates/view', $data);

    }
    public function edit_content($id)
    {
        $data = array();
        $data['content'] = "content/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'content';
        $data['data']['content'] = $this->content_model->get_edit($id);
        $data['data']['menu'] = $this->menu_model->menu_content();
        $this->load->view('templates/view', $data);

    }
    public function detail_content($id)
    {
        $data = array();
        $data['content'] = "content/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'content';
        $data['data']['content'] = $this->content_model->get_edit($id);
        $data['data']['menu'] = $this->menu_model->menu_content();
        $this->load->view('templates/view', $data);

    }

    public function add()
    {
        // $a = strtolower(str_replace(' ', '', $this->slugify("doakafosaiofd-=0-=   -32+")));

        // var_dump($a);exit();
        $TITLE_ID = isset($_POST['title_id']) ? $_POST['title_id'] : "";
        $TITLE_EN = isset($_POST['title_en']) ? $_POST['title_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";
        $ID_MENU = isset($_POST['id_menu']) ? $_POST['id_menu'] : "";

        if (empty($ID_MENU)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Menu Parent is required!");
            die(json_encode($JSON));
        }
        if (empty($DESCRIPTION_ID)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Content Indonesia is required!");
            die(json_encode($JSON));
        }
        if (empty($DESCRIPTION_EN)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Content English is required!");
            die(json_encode($JSON));
        }

        $param['TITLE_ID'] = $TITLE_ID;
        $param['TITLE_EN'] = $TITLE_EN;
        $param['DESCRIPTION_ID'] = $DESCRIPTION_ID;
        $param['DESCRIPTION_EN'] = $DESCRIPTION_EN;
        $param['ID_MENU'] = $ID_MENU;
        $param['CREATED_BY'] = $_SESSION['username'];
        $param['CREATED_DATE'] = date('Y-m-d H:i:s');
        $Insert = $this->content_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));
    }

    public function view()
    {
        $GetData = $this->content_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID = isset($_POST['id']) ? $_POST['id'] : "";
        $TITLE_ID = isset($_POST['title_id']) ? $_POST['title_id'] : "";
        $TITLE_EN = isset($_POST['title_en']) ? $_POST['title_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";
        $ID_MENU = isset($_POST['id_menu']) ? $_POST['id_menu'] : "";

        if (empty($ID_MENU)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Menu Parent is required!");
            die(json_encode($JSON));
        }
        if (empty($DESCRIPTION_ID)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Content Indonesia is required!");
            die(json_encode($JSON));
        }
        if (empty($DESCRIPTION_EN)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Content English is required!");
            die(json_encode($JSON));
        }

        $param['ID'] = $ID;
        $param['TITLE_ID'] = $TITLE_ID;
        $param['TITLE_EN'] = $TITLE_EN;
        $param['DESCRIPTION_ID'] = $DESCRIPTION_ID;
        $param['DESCRIPTION_EN'] = $DESCRIPTION_EN;
        $param['ID_MENU'] = $ID_MENU;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $Update = $this->content_model->updateaction($param);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->content_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "0";
        $where = array('ID' => $id);
        $Active = $this->content_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "88";
        $where = array('ID' => $id);
        $Active = $this->content_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "99";
        $where = array('ID' => $id);
        $Active = $this->content_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }

}