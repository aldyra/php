<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>

<style>
	.content-sitemap{
		display: flex;
		flex-wrap: wrap;
		padding-top: 1rem;
		padding-bottom: 1rem;
		max-width: 95%;
	}
	.row-page-module {
	    background-color: #f7f7f7;
	    padding: 10px;
	}
	ul.ul-sitemap ul{
		list-style: inherit;
	}
	ul.ul-sitemap li::marker {
	  color: #9F9F9F;
	}
	.item-sitemap{
		flex: auto;
		margin-bottom: 1.5rem;
	}
	p.title-sitemap a{
		color: #333;
		font-weight: bold;
		font-size: 23px;
	}

	.item-sitemap.custom-product{
		flex: 100%;
	}
	.item-sitemap.custom-biaya{
		flex: 80%;
	}
	.item-sitemap.custom-product ul.ul-sitemap{
		column-count: 4;
	}
	.item-sitemap.custom-biaya ul.ul-sitemap{
		column-count: 3;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.item-sitemap{
			flex: 100%;
		}
		.item-sitemap ul.ul-sitemap{
			column-count: auto !important;
		}
		.row-page-module{
			display: none;
		}
	}

	.content-sitemap p.title-sitemap a{
		font-size: 16px;
	}
	ul.ul-sitemap li a{
		font-size: 14px;
	}
	.row-page-module {
		/*margin-top: 15px;*/
		font-size: 14px;
	}
</style>
<!-- <div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_CHILD; ?></h4>
	</div>
</div> -->
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<a href="<?php echo base_url().$SiteLang.'/'.$MENU_PARENT_LINK?>"><?php echo $MENU_PARENT?></a> /
		<?php } ?> 
		<?php echo $MENU_CHILD; ?>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="content-sitemap">
				<?php $ij = 0; ?>
				<?php foreach($nav_menu as $menu){ 
					$NAME = isset($menu['NAME']) ? $menu['NAME']:"";
                    $LINK = isset($menu['LINK']) ? $menu['LINK']:"";
                    $CHILD = isset($menu['CHILD']) ? $menu['CHILD']:array();
                    $MENU_PARENT = isset($menu['MENU_PARENT']) ? $menu['MENU_PARENT']:array();

                    $UrlController = base_url().$SiteLang."/".$LINK;
                    
                    if(!empty($CHILD)){
                        $UrlActive = "javascript:void(0)";
                    }else{
                        $UrlActive = $UrlController;
                    }
                    ?>
					<div class="item-sitemap <?php echo ($menu['ID'] == 39) ? 'custom-product' : '' ?> <?php echo ($menu['ID'] == 98) ? 'custom-biaya' : '' ?>">
						<p class="title-sitemap"><strong><a href="<?php echo $UrlActive;?>"><?php echo $NAME ?></a></strong></p>
						<ul class="ul-sitemap">
							<?php foreach($CHILD as $submenu){ 
								$NAME_SUB = isset($submenu['NAME']) ? $submenu['NAME']:"";
                                $LINK_SUB = isset($submenu['LINK']) ? $submenu['LINK']:"";
                                $CHILD_SUB = isset($submenu['CHILD']) ? $submenu['CHILD']:array();
                                $UrlControllerSub = base_url().$SiteLang."/".$LINK_SUB;

                                if(!empty($CHILD_SUB)){
                                    $UrlControllerSub = "javascript:void(0)";
                                }else{
                                    $UrlControllerSub = $UrlControllerSub;
                                }
								?>
								<li><a href="<?php echo $UrlControllerSub?>"><?php echo $NAME_SUB ?></a></li>
								<?php if(!empty($CHILD_SUB)){ ?>
									<ul class="sub-sitemap">
										<?php foreach($CHILD_SUB as $submenu_child){ 
											$NAME_SUB_CHILD = isset($submenu_child['NAME']) ? $submenu_child['NAME']:"";
                                            $LINK_SUB_CHILD = isset($submenu_child['LINK']) ? $submenu_child['LINK']:"";
                                            $CHILD_SUB_CHILD = isset($submenu_child['CHILD']) ? $submenu_child['CHILD']:array();
                                            $UrlControllerSubChild = base_url().$SiteLang."/".$LINK_SUB_CHILD;
                                            if(!empty(sizeof($CHILD_SUB_CHILD))){
                                                $UrlActiveSubChild = "javascript:void(0)";
                                            }else{
                                                $UrlActiveSubChild = $UrlControllerSubChild;
                                            }
										?>
											<li><a href="<?php echo $UrlActiveSubChild?>"><?php echo $NAME_SUB_CHILD ?></a></li>
											<?php if(!empty($CHILD_SUB_CHILD)){ ?>
												<?php 
                                                foreach($CHILD_SUB_CHILD as $submenu_child_2){
                                                    $NAME_SUB_CHILD_2 = isset($submenu_child_2['NAME']) ? $submenu_child_2['NAME']:"";
                                                    $LINK_SUB_CHILD_2 = isset($submenu_child_2['LINK']) ? $submenu_child_2['LINK']:"";
                                                    $CHILD_SUB_CHILD_2 = isset($submenu_child_2['CHILD']) ? $submenu_child_2['CHILD']:array();
                                                    $UrlControllerSubChild_2 = base_url().$SiteLang."/".$LINK_SUB_CHILD_2;
                                                    if(!empty($CHILD_SUB_CHILD_2)){
                                                        $UrlActiveSubChild_2 = "javascript:void(0)";
                                                    }else{
                                                        $UrlActiveSubChild_2 = $UrlControllerSubChild_2;
                                                    }
                                                ?>
	                                                <ul class="sub-sitemap-2">
														<li><a href="<?php echo $UrlActiveSubChild_2?>"><?php echo $NAME_SUB_CHILD_2 ?></a></li>
													</ul>
                                            	<?php } ?>
											<?php } ?>
										<?php } ?>
									</ul>
								<?php } ?>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
			</div>

			<!-- <style>
				.row-socialize{
					float: right;
					text-align: left;
					margin-top: 1rem;
					margin-right: 4rem;
    				margin-bottom: 1rem;
				}
				@media only screen and (min-width: 200px) and (max-width: 1024px) {
					.row-socialize{
						float: unset;
						text-align: center;
						margin-right: auto;
					}
				}
			</style>

			<div class="row-socialize">
				<p style="color: #243E8B"><?php echo $this->lang->line('share') ?></p>
				<?php
				// echo '<pre>'; print_r($data); echo '</pre>';
					$q = $this->db->query("SELECT * FROM MS_SOCIAL_MEDIA WHERE STATUS = '99'")->result();
					if(!empty($q)){
						$url = '';
						foreach($q as $val):
							if($val->NAME == 'Twitter'){
								$url = 'https://twitter.com/share?url='.base_url(uri_string());
							}else if($val->NAME == 'Facebook'){
								$url = 'http://www.facebook.com/sharer.php?u='.base_url(uri_string());
							}else if($val->NAME == 'LinkedIn'){
								$url = 'http://www.linkedin.com/shareArticle?mini=true&amp;url='.base_url(uri_string());
							}else if($val->NAME == 'Google'){
								$url = 'https://plus.google.com/share?url='.base_url(uri_string());
							}else{
								$url = '';
							}
				?>
					<a href="<?php echo $url; ?>" target="_blank"><img src="<?php echo $val->LINK; ?>" style="width: 25px; height: 25px; "></a>&nbsp;
				<?php
						endforeach;
					}

					// echo $url;
				?>
			</div> -->
		</div>
	</div>
</div>