<style>
.field-icon {
    position: absolute;
    top: 0;
    color: #333;
    padding-left: .875rem;
    padding-right: .875rem;
    line-height: calc(1.5385em + .875rem + 2px);
    min-width: 1rem;
    right: 0;
}

/* .container {
    padding-top: 50px;
    margin: auto;
} */
</style>
<?php
// die(json_encode($param));
$profile = isset($param['profile']) ? $param['profile'] : array();
$group = isset($param['group']) ? $param['group'] : array();
$ID = isset($profile['ID']) ? $profile['ID'] : "";
$USERNAME = isset($profile['USERNAME']) ? $profile['USERNAME'] : "";
$GROUP_ID = isset($profile['GROUP_ID']) ? $profile['GROUP_ID'] : "";
$NAME = isset($profile['NAME']) ? $profile['NAME'] : "";
$EMAIL = isset($profile['EMAIL']) ? $profile['EMAIL'] : "";
$POSITION = isset($profile['POSITION']) ? $profile['POSITION'] : "";

$group = isset($rows['group']) ? $rows['group'] : array();
$decrypt = json_decode(base64_decode($group));
$group = $decrypt->group;
?>
<input type="hidden" name="IDUser" id="IDUser" value="<?php echo $ID ?>">
<div class="form-group row">
    <label for="username" class="col-sm-3 col-form-label">Username<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="username" required="" pattern="/^([a-zA-Z0-9 ])+$/" data-parsley-maxlength="15"
            data-parsley-minlength="2" data-parsley-required-message="Username is required."
            data-parsley-pattern-message="Username cannot contain any special characters."
            data-parsley-maxlength-message="Username is too long. It should have 15 characters or fewer."
            data-parsley-maxlength-message="Username is too short. It should have 2 characters or more."
            placeholder="Enter Username" class="form-control" id="usernameEdit" value="<?php echo $USERNAME ?>">
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-3 col-form-label">Name<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="name" required="" data-parsley-maxlength="50" data-parsley-minlength="2"
            data-parsley-required-message="Name is required."
            data-parsley-maxlength-message="Name is too long. It should have 50 characters or fewer."
            data-parsley-maxlength-message="Name is too short. It should have 2 characters or more."
            placeholder="Enter Name" class="form-control" id="nameEdit" value="<?php echo $NAME ?>">
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-sm-3 col-form-label">Email<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="email" name="email" required="" data-parsley-required-message="Email is required."
            data-parsley-type-message="Email should be a valid email." placeholder="Enter Email" class="form-control"
            id="emailEdit" value="<?php echo $EMAIL ?>">
    </div>
</div>

<div class="form-group row">
    <label for="position" class="col-sm-3 col-form-label">User Group<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <select id="group_userEdit" class="form-control select2modal" name="group_user" required=""
            data-parsley-required-message="User Group is required.">
            <option selected disabled>Choose Group</option>
            <?php
foreach ($group as $groupuser) {
    $ID = isset($groupuser->ID) ? $groupuser->ID : "";
    $USER_GROUP_NAME = isset($groupuser->USER_GROUP_NAME) ? $groupuser->USER_GROUP_NAME : "";
    ?>
            <option value="<?php echo $ID ?>" <?php if ($ID == $GROUP_ID) {echo 'selected';}?>>
                <?php echo $USER_GROUP_NAME ?></option>
            <?php
}
?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="position" class="col-sm-3 col-form-label">Position<span style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="position" required="" data-parsley-required-message="Position is required."
            placeholder="Enter Position" class="form-control" id="position" value="<?php echo $POSITION ?>">
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword32" class="col-sm-3 col-form-label">Password</label>
    <div class="col-sm-9">
        <input type="password" name="password" parsley-trigger="change" class="form-control passwordValidator"
            id="inputPassword88" placeholder="Password">
        <span toggle="#inputPassword88" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
        <span class="text-muted font-size-sm"> Must contain at
            least an uppercase letter, a number, and
            a special characters.</span>
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword52" class="col-sm-3 col-form-label">Re Password</label>
    <div class="col-sm-9">
        <input type="password" data-parsley-equalto="#inputPassword88" name="retypepassword" class="form-control"
            id="inputPassword99" placeholder="Retype Password"
            data-parsley-equalto-message="Password should be the same.">
        <span toggle="#inputPassword99" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
        <span class="text-muted font-size-sm"> Must contain at
            least an uppercase letter, a number, and
            a special characters.</span>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/passwordValidator.js"></script>
<script type="text/javascript">
$(function() {
    $(".select2modal").select2({
        dropdownParent: $('#modal')
    });
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});
</script>