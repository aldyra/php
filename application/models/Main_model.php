<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function kurs()
    {
    	$JSON = array();
    	$JSON2 = array();
    	$DateNow = date('Y-m-d');

		$where['STATUS'] = '99';
		$where['EFFECTIVE_DATE'] = $DateNow;
		$Query = "SELECT ID_KURS_HEADER, DESCRIPTION, EFFECTIVE_DATE FROM TR_KURS_HEADER WHERE STATUS = '99' ORDER BY CREATED_DATE DESC LIMIT 1";
		$Run = $this->db->query($Query);
    	$Row = $Run->result();
    	
    	foreach($Row as $row){
    		$ID_KURS_HEADER = isset($row->ID_KURS_HEADER) ? $row->ID_KURS_HEADER:"";
    		$DESCRIPTION = isset($row->DESCRIPTION) ? $row->DESCRIPTION:"";
    		$EFFECTIVE_DATE	 = isset($row->EFFECTIVE_DATE) ? $row->EFFECTIVE_DATE:"";
    		$ROW['ID_KURS_HEADER'] = $ID_KURS_HEADER;
    		$ROW['DESCRIPTION'] = $DESCRIPTION;
    		$ROW['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;

    		$Query2 = "SELECT A.ID_KURS_DETAIL, B.CURRENCY_NAME, B.LINK, B.CURRENCY_CODE, B.IS_HIGHLIGHT, A.BUY, A.SELL, A.CREATED_DATE
				FROM TR_KURS_DETAIL AS A
				LEFT JOIN MS_CURRENCY AS B ON A.ID_CURRENCY = B.ID_CURRENCY WHERE A.ID_KURS_HEADER = '".$ID_KURS_HEADER."'";
			$Run2 = $this->db->query($Query2);
			$Row2 = $Run2->result();
			foreach($Row2 as $row2){
				$ROW2['ID_KURS_DETAIL'] = isset($row2->ID_KURS_DETAIL) ? $row2->ID_KURS_DETAIL:"";
				$ROW2['CURRENCY_NAME'] = isset($row2->CURRENCY_NAME) ? $row2->CURRENCY_NAME:"";
                $ROW2['CURRENCY_CODE'] = isset($row2->CURRENCY_CODE) ? $row2->CURRENCY_CODE:"";
				$ROW2['IS_HIGHLIGHT'] = isset($row2->IS_HIGHLIGHT) ? $row2->IS_HIGHLIGHT:"";
				$ROW2['BUY'] = isset($row2->BUY) ? $row2->BUY:"";
                $ROW2['SELL'] = isset($row2->SELL) ? $row2->SELL:"";
				$ROW2['LINK'] = isset($row2->LINK) ? $row2->LINK:"";
				$JSON2[] = $ROW2;
			}
			$ROW['DETAIL'] = $JSON2;
			$JSON[] = $ROW;
    	}
    	return $JSON;
    }

    public function sbdk()
    {
    	$Query = "SELECT ID_SBDK, CORPORATE_LOAN, RETAIL_LOAN, MICRO_LOAN, MORTGAGE, NONMORTGAGE, DESCRIPTION, EFFECTIVE_DATE, APPROVED_DATE FROM MS_SBDK WHERE STATUS = '99' ORDER BY EFFECTIVE_DATE DESC LIMIT 1";
		$Run = $this->db->query($Query);
    	$Row = $Run->result_array();
    	return $Row;
    }

    public function province()
    {
        $where['STATUS'] = '99';
        $Query2 = "SELECT DISTINCT ID_PROVINCE FROM MS_BRANCH";
        $Run2 = $this->db->query($Query2);
        $Row2 = $Run2->result_array();

        $PROVINCE = array();
        foreach($Row2 as $row2){
            $PROVINCE[] = isset($row2['ID_PROVINCE']) ? $row2['ID_PROVINCE']:"";
        }
        $ID_PROVINCE = join(',', $PROVINCE);

        // $Query = $this->db->select("PROVINCE_ID AS ID, PROVINCE_ID, PROVINCE_NAME")->where($where)->get("MS_PROVINCE");
        $Query = "SELECT PROVINCE_ID AS ID, PROVINCE_ID, PROVINCE_NAME FROM MS_PROVINCE WHERE PROVINCE_ID IN (".$ID_PROVINCE.") AND STATUS = '99'";
        $Run = $this->db->query($Query);
        $Row = $Run->result();
        return $Row;
    }

    public function city()
    {
        $Query = $this->db->select("CITY_KAB_ID AS ID, CITY_KAB_ID, CITY_KAB_NAME")->get("MS_CITY_KAB");
        $Row = $Query->result();
        return $Row;
    }

    public function citykab($param)
    {
        $where['PROVINCE_ID'] = $param;
        $Query = $this->db->where($where)->get("MS_CITY_KAB");
        $Row = $Query->result_array();
        return $Row;
    }

    public function branch($param = '')
    {
        $where['ID_PROVINCE'] = isset($param['PROVINCE']) ? $param['PROVINCE']:"";
        $where['ID_CITY'] = isset($param['CITYKAB']) ? $param['CITYKAB']:"";
        $where['STATUS'] = "99";

        if(empty($where['ID_PROVINCE']) && empty($where['ID_CITY'])){
            $params = "";
        }else{
            $params = " AND ID_PROVINCE = '".$where['ID_PROVINCE']."' AND ID_CITY = '".$where['ID_CITY']."' ";
        }

        $Query = "SELECT A.ID_BRANCH, A.BRANCH_NAME, A.CATEGORY, A.ADDRESS, A.LONGITUDE, A.LATITUDE, 
        A.ID_PROVINCE, B.PROVINCE_NAME, A.ID_KECAMATAN, C.CITY_KAB_NAME,
        A.POSTAL_CODE, A.PHONE_NUMBER, A.FAX_NUMBER, A.EMAIL, A.WEBSITE
        FROM MS_BRANCH AS A
        LEFT JOIN MS_PROVINCE AS B ON A.ID_PROVINCE = B.PROVINCE_ID
        LEFT JOIN MS_CITY_KAB AS C ON A.ID_CITY = C.CITY_KAB_ID
        WHERE A.STATUS = '99'".$params;
        $Run = $this->db->query($Query);
        $Row = $Run->result_array();
        return $Row;
    }

    public function branchhead($param = '')
    {
        $params = "";
        if(!empty($param)){
            $params = " AND CATEGORY = '".$param."' ";
        }

        $Query = "SELECT A.ID_BRANCH, A.BRANCH_NAME, A.CATEGORY, A.ADDRESS, A.LONGITUDE, A.LATITUDE, 
        A.ID_PROVINCE, B.PROVINCE_NAME, A.ID_KECAMATAN, C.CITY_KAB_NAME,
        A.POSTAL_CODE, A.PHONE_NUMBER, A.FAX_NUMBER, A.EMAIL, A.WEBSITE
        FROM MS_BRANCH AS A
        LEFT JOIN MS_PROVINCE AS B ON A.ID_PROVINCE = B.PROVINCE_ID
        LEFT JOIN MS_CITY_KAB AS C ON A.ID_CITY = C.CITY_KAB_ID
        WHERE A.STATUS = '99'".$params;
        $Run = $this->db->query($Query);
        $Row = $Run->result_array();
        return $Row;
    }
}