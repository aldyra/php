<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('socialmedia/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$SocialMedia = isset($Data['socialmedia']) ? $Data['socialmedia'] : array();
?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Social Media</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Social Media', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Social Media
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Image</th>
                    <th width="200">Name</th>
                    <th width="200">Link</th>
                    <th width="150" ata-orderable="false">Article</th>
                    <th width="150" ata-orderable="false">Footer</th>
                    <th>Created By</th>
                    <th data-orderable="false" width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($SocialMedia as $socialmedia) {
    $ID = isset($socialmedia['ID_SOCIAL_MEDIA']) ? $socialmedia['ID_SOCIAL_MEDIA'] : "";
    $NAME = isset($socialmedia['NAME']) ? $socialmedia['NAME'] : "";
    $LINK = isset($socialmedia['LINK']) ? $socialmedia['LINK'] : "";
    $IS_SHARING = isset($socialmedia['IS_SHARING']) ? $socialmedia['IS_SHARING'] : "";
    $IS_HIDDEN = isset($socialmedia['IS_HIDDEN']) ? $socialmedia['IS_HIDDEN'] : "";
    $URL_SOCIAL_MEDIA = isset($socialmedia['URL_SOCIAL_MEDIA']) ? $socialmedia['URL_SOCIAL_MEDIA'] : "";
    $STATUS = isset($socialmedia['STATUS']) ? $socialmedia['STATUS'] : "";
    $CREATED_BY = isset($socialmedia['CREATED_BY']) ? $socialmedia['CREATED_BY'] : "";
    $CREATED_DATE = isset($socialmedia['CREATED_DATE']) ? $socialmedia['CREATED_DATE'] : "";
    $USER_LOG = isset($socialmedia['USER_LOG']) ? $socialmedia['USER_LOG'] : "";
    $DATE_LOG = isset($socialmedia['DATE_LOG']) ? $socialmedia['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    if ($IS_SHARING == 1) {
        $ARTICLE = 'YES';

    } else {
        $ARTICLE = 'NO';
    }

    if ($IS_HIDDEN == 1) {
        $FOOTER = 'YES';

    } else {
        $FOOTER = 'NO';
    }

    ?>
                <tr>
                    <td>
                        <a href='<?php echo $LINK ?>' data-popup='lightbox' data-fancybox><img src='<?php echo $LINK ?>'
                                class='img-thumbnail' width='80px' heigh='80px'></a>
                    </td>
                    <td>
                        <?php echo $NAME ?>
                    </td>
                    <td>
                        <a href="<?php echo $URL_SOCIAL_MEDIA ?>" target="_blank"><?php echo $URL_SOCIAL_MEDIA ?>
                        </a>

                    </td>

                    <td>
                        <?php echo $ARTICLE ?>
                    </td>
                    <td>
                        <?php echo $FOOTER ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $socialmedia;
        $paramaction['Modal'] = "";
        $this->load->view('is_action', $paramaction);
        ?>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(3).visible(false);
t.column(4).visible(false);
t.column(8).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(8)
            .search(dis.value)
            .draw();
    } else {
        t.column(8)
            .search(dis.value)
            .draw();
    }

}

function changeSelectArticle(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(3)
            .search(dis.value)
            .draw();
    } else {
        t.column(3)
            .search(dis.value)
            .draw();
    }

}

function changeSelectFooter(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}
</script>