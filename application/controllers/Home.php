<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Content_model');
    }

	public function index()
	{
		$GetNews = $this->Home_model->getnews();
		// die(json_encode($GetNews));
		$GetBanner = $this->Home_model->getbanner();
		$post['banner'] = $GetBanner;
		$post['news'] = $GetNews;

		$this->data['cssName'] = "Css/public.css:Css/home.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/home.js";
		$this->data['Content'] = "home";
		$this->data['data'] = $post;
		$this->load->view('template', $this->data);
	}

	public function redirecturl($param)
	{
		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		$GetContent = $this->Content_model->getcontenthighlight($param);
	
	        $ID_MENU = isset($GetContent['ID']) ? $GetContent['ID']:"";

		$GetModule = $this->Module_model->redirectmodule($ID_MENU);
		// die(json_encode($param));
		$LINK = isset($GetModule['LINK']) ? $GetModule['LINK']:"";
		$LINK = base_url().$SiteLang."/".$LINK;
		redirect($LINK);
	}
}
