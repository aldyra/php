<?php
$is_create = $authorize['is_create'];
$back = isset($data['back']) ? $data['back'] : '';

$sbdk = isset($data['sbdk']) ? $data['sbdk'] : '';
$ID_SBDK = isset($sbdk->ID_SBDK) ? $sbdk->ID_SBDK : "";
$CORPORATE_LOAN = isset($sbdk->CORPORATE_LOAN) ? $sbdk->CORPORATE_LOAN : "";
$RETAIL_LOAN = isset($sbdk->RETAIL_LOAN) ? $sbdk->RETAIL_LOAN : "";
$MICRO_LOAN = isset($sbdk->MICRO_LOAN) ? $sbdk->MICRO_LOAN : "";
$MORTGAGE = isset($sbdk->MORTGAGE) ? $sbdk->MORTGAGE : "";
$NONMORTGAGE = isset($sbdk->NONMORTGAGE) ? $sbdk->NONMORTGAGE : "";
$EFFECTIVE_DATE = isset($sbdk->EFFECTIVE_DATE) ? date('d M Y H:i:s', strtotime($sbdk->EFFECTIVE_DATE)) : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master SBDK</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master SBDK</span> - Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_create): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveSBDK">
            <div class="card-header bg-blue-800 d-flex justify-content-between">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Effective Date<span style="color:orange">*</span></label>

                        <h5><?php echo $EFFECTIVE_DATE ?></h5>

                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">SBDK Detail</h6>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="workhistory">
                        <thead>
                            <tr>
                                <th>Business Segment</th>
                                <th>Prime Lending Rate</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Corporate Loan/Kredit Korporasi</td>
                                <td>
                                    <?php echo $CORPORATE_LOAN ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Retail Loan/Kredit Retail</td>
                                <td>
                                    <?php echo $RETAIL_LOAN ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Micro Loan/Kredit Mikro</td>
                                <td>
                                    <?php echo $MICRO_LOAN ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Mortgage/KPR</td>
                                <td>
                                    <?php echo $MORTGAGE ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Non-Mortgage/Non-KPR</td>
                                <td>
                                    <?php echo $NONMORTGAGE ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Back</button></a>
                </div>
            </div>
        </form>
    </div>
    <?php endif;?>
</div>

<script type="text/javascript">
$(function() {
    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });
});
</script>