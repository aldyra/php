$(function () {
	var height;
	height = $(window).height() - parseInt(100);
	$(".modal-set-height").find(".modal-body").css({ height: height });
});
$("#formData").parsley();

function validFileSize(input, maxSize) {
	var input, file;

	if (!window.FileReader) {
		alert("Please update your browser!");
		return;
	}

	input = document.getElementById(input);
	file = input.files[0];
	console.log(file);

	if (input.files[0]) {
		if (file.size >= maxSize) {
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}
}
(function (w, d) {
	function LetterAvatar(name, size) {
		name = name || "";
		size = size || 60;

		var colours = [
				"#1abc9c",
				"#2ecc71",
				"#3498db",
				"#9b59b6",
				"#34495e",
				"#16a085",
				"#27ae60",
				"#2980b9",
				"#8e44ad",
				"#2c3e50",
				"#f1c40f",
				"#e67e22",
				"#e74c3c",
				"#95a5a6",
				"#f39c12",
				"#d35400",
				"#c0392b",
				"#bdc3c7",
				"#7f8c8d",
			],
			nameSplit = String(name).toUpperCase().split(" "),
			initials,
			charIndex,
			colourIndex,
			canvas,
			context,
			dataURI;

		if (nameSplit.length == 1) {
			initials = nameSplit[0] ? nameSplit[0].charAt(0) : "?";
		} else {
			initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
		}

		if (w.devicePixelRatio) {
			size = size * w.devicePixelRatio;
		}

		charIndex = (initials == "?" ? 72 : initials.charCodeAt(0)) - 64;
		colourIndex = charIndex % 20;
		canvas = d.createElement("canvas");
		canvas.width = size;
		canvas.height = size;
		context = canvas.getContext("2d");

		context.fillStyle = colours[colourIndex - 1];
		context.fillRect(0, 0, canvas.width, canvas.height);
		context.font = Math.round(canvas.width / 2) + "px Arial";
		context.textAlign = "center";
		context.fillStyle = "#FFF";
		context.fillText(initials, size / 2, size / 1.5);

		dataURI = canvas.toDataURL();
		canvas = null;

		return dataURI;
	}

	LetterAvatar.transform = function () {
		Array.prototype.forEach.call(
			d.querySelectorAll("img[avatar]"),
			function (img, name) {
				name = img.getAttribute("avatar");
				img.src = LetterAvatar(name, img.getAttribute("width"));
				img.removeAttribute("avatar");
				img.setAttribute("alt", name);
			}
		);
	};

	// AMD support
	if (typeof define === "function" && define.amd) {
		define(function () {
			return LetterAvatar;
		});

		// CommonJS and Node.js module support.
	} else if (typeof exports !== "undefined") {
		// Support Node.js specific `module.exports` (which can be a function)
		if (typeof module != "undefined" && module.exports) {
			exports = module.exports = LetterAvatar;
		}

		// But always support CommonJS module 1.1.1 spec (`exports` cannot be a function)
		exports.LetterAvatar = LetterAvatar;
	} else {
		window.LetterAvatar = LetterAvatar;

		d.addEventListener("DOMContentLoaded", function (event) {
			LetterAvatar.transform();
		});
	}
})(window, document);
$(".menu a").each(function () {
	var t = window.location.href.split(/[?#]/)[0];
	this.href == t &&
		($(this).addClass("active"),
		// $(this).attr("aria-expanded", "true"),
		// $(this).attr("data-active", "true"),
		$(this).parent().addClass("active"),
		$(this).parent().parent().addClass("show"),
		$(this).parent().parent().prev().addClass("active"),
		// $(this).parent().parent().prev().attr("aria-expanded", "true"),
		// $(this).parent().parent().prev().attr("data-active", "true"),
		$(this).parent().parent().parent().addClass("active"),
		$(this).parent().parent().parent().parent().addClass("show"),
		$(this).parent().parent().parent().parent().parent().addClass("active"));
});
$(".modal").on("hidden.bs.modal", function (e) {
	$(this)
		.find("input[type=text],textarea,select,input[type=password]")
		.val("")
		.end();
	var Selectlength = $(".parsleyy").length;
	for (var i = 0; i < Selectlength; i++) {
		$(".parsleyy").parsley()[i].reset();
	}
});

function noSymbol(evt) {
	evt.value = evt.value.replace(
		/\`|\~|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\+|\=|\[|\{|\]|\}|\||\\|\'|\<|\,|\.|\>|\?|\/|\"\"|\;|\:|\s/g,
		""
	);
	evt.value = evt.value.replace('"', "");
}

function validate(evt) {
	evt.value = evt.value.replace(/[^0-9]/g, "");
}

function minzero(evt) {
	evt.value = evt.value.replace(/[^0-9][-]/g, "");
}

function commaasd(dis) {
	dis.value = dis.value.replace(/^[^,]+,[^,]*$/g, "");
}

function upper(input) {
	input.value = input.value.toUpperCase();
}

function formatNumber(num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
function validate_number(evt) {
	evt.value = evt.value.replace(/[^0-9.]/g, "");
}
function isDatev(dis, selector) {
	if (isNaN(Date.parse(dis.value))) {
		var idh = "#" + selector;
		var idhV = "#" + selector + "V";
		$(idhV).datepicker("setDate", new Date($(idh).val()));
	}
}
function validatelink(dis) {
	dis.value = dis.value.replace(/[^a-zA-Z0-9_-]/g, "");
}

function changePrice(dis, selector) {
	if (dis.value != "" && dis.value != null) {
		var ids = "#" + selector;
		valuee = dis.value.replace(/,/g, "");
		$(ids).val(valuee).trigger("change");
		dis.value = numberWithCommas(valuee);
	}
}

function currencyFormat(num) {
	var p = num.toFixed(2).split(".");
	return (
		p[0]
			.split("")
			.reverse()
			.reduce(function (acc, num, i, orig) {
				return num + (i && !(i % 3) ? "," : "") + acc;
			}, "") +
		"." +
		p[1]
	);
}
function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function number_format(number, decimals, decPoint, thousandsSep) {
	number = (number + "").replace(/[^0-9+\-Ee.]/g, "");
	var n = !isFinite(+number) ? 0 : +number;
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
	var sep = typeof thousandsSep === "undefined" ? "," : thousandsSep;
	var dec = typeof decPoint === "undefined" ? "." : decPoint;
	var s = "";

	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return "" + (Math.round(n * k) / k).toFixed(prec);
	};

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split(".");
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || "").length < prec) {
		s[1] = s[1] || "";
		s[1] += new Array(prec - s[1].length + 1).join("0");
	}

	return s.join(dec);
}

function modal(type, modal, title, file, row, action) {
	var rows = JSON.parse(atob(row));
	var data = {
		type: type,
		title: title,
		file: file,
		rows: rows,
		modal: modal,
		action: action,
	};
	$.ajax({
		type: "post",
		url: url + "modal",
		data: data,
		cache: false,
		beforeSend: function () {},
		success: function (res) {
			$("#modal").html(res);
			$("#modal").modal("show");
		},
	});
}

function openTnC(url) {
	$.ajax({
		url: url,
		dataType: "JSON",
		type: "GET",
		success: function (data) {
			$("#tncupload").html(data);
			$("#modalTnCUpload").modal("show");
		},
	});
}

function submitform() {
	$("#formData").parsley().validate();
	var form = $("#formData");
	var data = form.serialize();
	var Url = form.data("url");

	var ckeditor = form.find(".ckeditor").length;
	if (ckeditor > 0) {
		// CKEDITOR.instances.description.updateElement();
	}
	if ($("#formData").parsley().isValid()) {
		$.ajax({
			url: Url,
			type: "POST",
			data: data,
			cache: false,
			dataType: "json",
			beforeSend: function () {
				document.getElementById("btnform").disabled = true;
			},
			success: function (res) {
				document.getElementById("btnform").disabled = false;
				var ErrorMessage = res.ErrorMessage;
				var ErrorCode = res.ErrorCode;
				if (ErrorCode != "EC:0000") {
					Swal.fire({
						type: "error",
						html: ErrorMessage,
						confirmButton: true,
						confirmButtonColor: "#1FB3E5",
						confirmButtonText: "Close",
					});
				} else {
					Swal.fire({
						position: "center",
						type: "success",
						text: ErrorMessage,
						showConfirmButton: false,
						timer: 1500,
					});
					setTimeout(function () {
						window.location.reload();
					}, 1500);
				}
			},
		});
	}
}

function submitformfile() {
	$("#formData").parsley().validate();
	var form = $("#formData");
	var formElement = $("#formData")[0];
	var data = new FormData(formElement);
	var Url = form.data("url");

	var ckeditor = form.find(".ckeditor").length;

	data.append("file", $("input[type=file]")[0].files[0]);
	if (ckeditor > 0) {
		data.append("description", CKEDITOR.instances["description"].getData());
	}

	$(".msg").hide();
	$(".progress").show();
	if ($("#formData").parsley().isValid()) {
		$.ajax({
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (e) {
					if (e.lengthComputable) {
						var percent = Math.round((e.loaded / e.total) * 100);

						$("#progressBar")
							.attr("aria-valuenow", percent)
							.css("width", percent + "%")
							.text(percent + "%");
					}
				});
				return xhr;
			},
			url: Url,
			type: "POST",
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			dataType: "json",
			beforeSend: function () {
				document.getElementById("btnform").disabled = true;
			},
			success: function (res) {
				document.getElementById("btnform").disabled = false;
				var ErrorMessage = res.ErrorMessage;
				var ErrorCode = res.ErrorCode;
				if (ErrorCode != "EC:0000") {
					Swal.fire({
						type: "error",
						html: ErrorMessage,
						confirmButton: true,
						confirmButtonColor: "#1FB3E5",
						confirmButtonText: "Close",
					});
				} else {
					$("#progressBar").hide();
					Swal.fire({
						position: "center",
						type: "success",
						text: ErrorMessage,
						showConfirmButton: false,
						timer: 1500,
					});
					setTimeout(function () {
						window.location.reload();
					}, 1500);
				}
			},
		});
	}
}

function changeformfile() {
	$("#btnform").attr({ onclick: "submitformfile()" });
}

function removemodalfooter() {
	$(".modal-footer").remove();
}

function ambilId(file) {
	return document.getElementById(file);
}

function progressHandler(event) {
	ambilId("loaded_n_total").innerHTML =
		"Uploaded " + event.loaded + " bytes of " + event.total;
	var percent = (event.loaded / event.total) * 100;
	ambilId("progressBar").value = Math.round(percent);
	ambilId("status").innerHTML =
		Math.round(percent) + "% uploaded... please wait";
}

function completeHandler(event) {
	ambilId("status").innerHTML = event.target.responseText;
	ambilId("progressBar").value = 0;
}

function errorHandler(event) {
	ambilId("status").innerHTML = "Upload Failed";
}

function abortHandler(event) {
	ambilId("status").innerHTML = "Upload Aborted";
}

function updateaction(param) {
	var decrypt = JSON.parse(atob(param));
	var Url = decrypt.Url;
	var Controller = decrypt.Controllers;
	var Data = decrypt.Data;
	var Modal = decrypt.Modal;
	if (Modal == undefined || Modal == "") {
		var Modal = "";
	}

	var params = {
		param: Data,
	};

	var param = btoa(JSON.stringify(params));
	modal(
		"update",
		Modal,
		"Update",
		Controller + "/form_edit",
		param,
		Url + "/update"
	);
}

function deleteaction(param) {
	var decrypt = JSON.parse(atob(param));
	var Url = decrypt.Url;

	Swal.fire({
		title: "Are you sure?",
		type: "warning",
		text: "Your will not be able to recover this Data!",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes",
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: Url + "/remove",
				dataType: "JSON",
				data: {
					Data: decrypt.Data,
				},
				type: "POST",
				success: function (res) {
					var ErrorMessage = res.ErrorMessage;
					var ErrorCode = res.ErrorCode;
					if (ErrorCode != "EC:0000") {
						Swal.fire({
							type: "error",
							html: ErrorMessage,
							confirmButton: true,
							confirmButtonColor: "#1FB3E5",
							confirmButtonText: "Close",
						});
					} else {
						Swal.fire({
							position: "center",
							type: "success",
							text: ErrorMessage,
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					}
				},
			});
		}
	});
}

//Open Change Status deactive
function deactiveaction(param) {
	var decrypt = JSON.parse(atob(param));
	var Url = decrypt.Url;
	Swal.fire({
		title: "Are you sure?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, deactivate it!",
		cancelButtonText: "No, cancel!",
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: Url + "/deactive",
				dataType: "JSON",
				data: {
					Data: decrypt.Data,
				},
				type: "POST",
				success: function (res) {
					var ErrorMessage = res.ErrorMessage;
					var ErrorCode = res.ErrorCode;
					if (ErrorCode != "EC:0000") {
						Swal.fire({
							type: "error",
							html: ErrorMessage,
							confirmButton: true,
							confirmButtonColor: "#1FB3E5",
							confirmButtonText: "Close",
						});
					} else {
						Swal.fire({
							position: "center",
							type: "success",
							text: ErrorMessage,
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					}
				},
			});
		}
	});
}
//END Change Status deactive

//Open Change Status active
function activateaction(param) {
	var decrypt = JSON.parse(atob(param));
	var Url = decrypt.Url;
	Swal.fire({
		title: "Are you sure?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, activate it!",
		cancelButtonText: "No, cancel!",
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: Url + "/active",
				dataType: "JSON",
				data: {
					Data: decrypt.Data,
				},
				type: "POST",
				success: function (res) {
					var ErrorMessage = res.ErrorMessage;
					var ErrorCode = res.ErrorCode;
					if (ErrorCode != "EC:0000") {
						Swal.fire({
							type: "error",
							html: ErrorMessage,
							confirmButton: true,
							confirmButtonColor: "#1FB3E5",
							confirmButtonText: "Close",
						});
					} else {
						Swal.fire({
							position: "center",
							type: "success",
							text: ErrorMessage,
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					}
				},
			});
		}
	});
}
//END Change Status active
//Open Change Status active
function publishaction(param) {
	var decrypt = JSON.parse(atob(param));
	var Url = decrypt.Url;
	Swal.fire({
		title: "Are you sure?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Yes, publish it!",
		cancelButtonText: "No, cancel!",
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: Url + "/publish",
				dataType: "JSON",
				data: {
					Data: decrypt.Data,
				},
				type: "POST",
				success: function (res) {
					var ErrorMessage = res.ErrorMessage;
					var ErrorCode = res.ErrorCode;
					if (ErrorCode != "EC:0000") {
						Swal.fire({
							type: "error",
							html: ErrorMessage,
							confirmButton: true,
							confirmButtonColor: "#1FB3E5",
							confirmButtonText: "Close",
						});
					} else {
						Swal.fire({
							position: "center",
							type: "success",
							text: ErrorMessage,
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					}
				},
			});
		}
	});
}
//END Change Status active
