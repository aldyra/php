<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$paramadd['group'] = $group;
$Sendparam = base64_encode(json_encode($paramadd));
$action_add = base_url('users/create_action');
$action_update = base_url('users/update_action');
?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">User Management</a>
                <a href="#" class="breadcrumb-item active">Manage User</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <?php if ($authorize['is_create']): ?>
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Users', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action_add ?>')">
                Add User
            </button>
            <?php endif;?>
        </div>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created By</th>
                    <th data-orderable="false" width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th data-orderable="false">Action</th>
                    <th data-orderable="false">sts</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>

</script>
<script type="text/javascript">
<?php if (!$authorize['is_update']) {?>
var colDef = [{
    "targets": [6, 7, 8],
    "visible": false,
}];
<?php } else {?>
var colDef = [{
    "targets": [7, 8],
    "visible": false,
}];
<?php }?>

var arr = ["ID", "USERNAME", "NAME", "EMAIL", "CREATED_BY", "CREATED_DATE", "STATUS"];
var t = $("#tableData").DataTable({
    serverSide: false,
    autoWidth: false,
    deferRender: true,
    ajax: {
        "url": "<?php echo base_url('users/get_data') ?>",
        "type": "POST",
        "data": {
            "arr": arr
        },
    },
    columns: [{
            "data": "USERNAME"
        },
        {
            "data": "NAME"
        },
        {
            "data": "EMAIL"
        },
        {
            "data": "CREATED_BY"
        },
        {
            "data": "CREATED_DATE"
        },
        {
            "data": "STATUS"
        },
        {
            "className": 'text-center',
            "orderable": false,
            "data": "ACTION"
        },
        {
            "data": "ID"
        },
        {
            "data": "STS"
        },
    ],
    columnDefs: colDef,
    keys: !0,
    order: [
        [7, "desc"]
    ],
    autofill: true,
    select: true,
    responsive: true,
    buttons: true,
    searchable: true,
    dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',

});


$('#tableData thead th').each(function() {
    var title = $(this).text();
    if (title == "Action" || title == "Image") {

    } else if (title == "Status") {
        $(this).append(
            '<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option></select>'
        );
    } else if (title == "Created Date") {
        $(this).append(
            '<br> <div class="input-group" style="color:#333">' +
            '<span class="input-group-prepend">' +
            '<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
            "</span>" +
            '<input type="text" onchange="changeSelectDate(this)" name="created_date" id="created_date" class="form-control pickadate-selectors" placeholder="Select Date..">' +
            "</div>"
        );
        $(".pickadate-selectors").pickadate({
            selectYears: true,
            selectMonths: true,
            format: "dd mmm yyyy",
            formatSubmit: "dd mmm yyyy",
            hiddenName: true,
        });
    } else {
        $(this).append(
            '<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
        );
    }
});

t.columns().every(function() {
    var that = this;
    $("input", this.header()).on("keyup", function(e) {
        if (event.keyCode === 8) {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        } else {
            if (this.value.length >= 1) {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            }
        }
    });
});

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(8)
            .search(dis.value)
            .draw();
    } else {
        t.column(8)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

$("#tableData thead input").on("click", function(e) {
    e.stopPropagation();
});

function confirmDelete(id) {
    Swal.fire({
        title: 'Are you sure?',
        type: 'warning',
        text: "Your will not be able to recover this Data!",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?php echo base_url('users/delete_action/'); ?>" + id,
                dataType: "JSON",
                type: 'POST',
                success: function(data) {
                    if (!(data.indexOf("Failed") != -1)) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Delete data success',
                            showConfirmButton: false,
                            timer: 1500
                        });

                        setTimeout(function() {
                            window.location.reload();
                        }, 1500);
                    } else {
                        swal({
                            type: 'error',
                            title: data
                        }, function() {
                            t.ajax.reload(null, false);
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        type: 'error',
                        title: 'Failed: ' + textStatus
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                }
            });
        }
    })
}
//Open Edit Form
function openEdit(id) {
    var Url_update = '<?php echo $action_update ?>';
    var Sendparam = '<?php echo $Sendparam ?>';

    $.ajax({
        url: "<?php echo base_url('users/get_edit/'); ?>" + id,
        dataType: "JSON",
        type: 'GET',
        success: function(data) {
            var params = {
                param: data,
                group: Sendparam
            }
            var param = btoa(JSON.stringify(params));
            modal('update', '', 'Update Users', 'users/form_edit', param, Url_update);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal(textStatus);
        }
    });
}
//END Open Edit Form
//Open Edit Form
function openDetail(id) {
    var Url_update = '<?php echo $action_update ?>';
    var Sendparam = '<?php echo $Sendparam ?>';

    $.ajax({
        url: "<?php echo base_url('users/get_edit/'); ?>" + id,
        dataType: "JSON",
        type: 'GET',
        success: function(data) {
            var params = {
                param: data,
                group: Sendparam
            }
            var param = btoa(JSON.stringify(params));
            modal('', '', 'Detail Users', 'users/detail', param, Url_update);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal(textStatus);
        }
    });
}
//END Open Edit Form

// CHANGE STATUS
function statusChg(id) {
    Swal.fire({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?php echo base_url('Users/statusChg/'); ?>" + id,
                dataType: "JSON",
                type: 'GET',
                success: function(data) {
                    if (data.indexOf("Success") != -1) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: data,
                            showConfirmButton: false,
                            timer: 1500
                        });

                        setTimeout(function() {
                            window.location.reload();
                        }, 1500);
                    } else {
                        swal({
                            type: 'error',
                            title: data
                        }, function() {
                            window.location.reload();
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal(textStatus, function() {
                        window.location.reload();
                    });
                }
            });
        }
    })
}
// CHANGE STATUS
</script>