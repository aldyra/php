<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurs extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{
		$post = array();
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$link = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['Content'] = "kurs";
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
	}
}