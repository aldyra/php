<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Deposit_ir_model extends MY_Model
{
    protected $_table_name = 'MS_DEPOSIT_IR_HEADER';
    protected $_detail_table_name = 'MS_DEPOSIT_IR_DETAIL';
    protected $_primary_key = 'ID_DEPOSIT_IR_HEADER';
    protected $_detail_primary_key = 'ID_DEPOSIT_IR_DETAIL';
    protected $_order_by = 'ID_DEPOSIT_IR_HEADER';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }
    public function get_detail_deposit_ir($id)
    {
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result_array();
        if (!empty($detail)) {
            for ($i = 0; $i < count($detail); $i++) {
                $this->db->where($this->_detail_primary_key, $detail[$i]['ID_DEPOSIT_IR_DETAIL']);
                $detail_month = $this->db->get('MS_DEPOSIT_IR_MONTH')->result_array();
                if (!empty($detail_month)) {
                    for ($j = 0; $j < count($detail_month); $j++) {
                        $detail[$i]['detail_month'][$j] = $detail_month[$j];
                    }
                }
            }
        }

        return $detail;

    }
    public function get_detail($id)
    {
        //detail
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result();
        //header
        $this->db->where($this->_primary_key, $id);
        $header = $this->db->get($this->_table_name)->row();
        $category = $header->CATEGORY_DEPOSIT;

        $html = '';
        $no = 0;
        foreach ($detail as $row) {
            if ($category == '1' || $category == '3') {
                $html .= '<div class="card border-left-info border-right-info rounded-0" id="edit' . $row->ID_DEPOSIT_IR_DETAIL . '" data-id="' . $row->ID_DEPOSIT_IR_DETAIL . '">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                        <div class="row">
                                            <input type="hidden" value="' . $row->ID_DEPOSIT_IR_DETAIL . '" name="detail_id[]">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label>Balance Indonesian<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="balance_id"
                                                        name="balance_id[]" placeholder="Balance in Indonesian"
                                                        parsley-trigger="change" required="" value="' . $row->BALANCE_ID . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label>Balance English<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="balance_en"
                                                        name="balance_en[]" placeholder="Balance in English"
                                                        parsley-trigger="change" required="" value="' . $row->BALANCE_EN . '">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                        <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                            type="button" onclick="remove(\'edit' . $row->ID_DEPOSIT_IR_DETAIL . '\');"><b><i
                                                    class="fas fa-minus"></i></b>
                                            Remove
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <h6 class="card-title">Detail Month</h6>
                                    </div>
                                    <div class="col-md-10">
                                        <button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"
                                            style="float:left;" type="button"
                                            onclick="add_detail_month(\'detail_ir_edit' . $row->ID_DEPOSIT_IR_DETAIL . '\', \'' . $no . '\');"><b><i
                                                    class="fas fa-plus"></i></b>
                                            Add Detail Month
                                        </button>
                                    </div>
                                </div>
                                <div class="row"><div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="detail_ir_edit' . $row->ID_DEPOSIT_IR_DETAIL . '">';
                $this->db->where('ID_DEPOSIT_IR_DETAIL', $row->ID_DEPOSIT_IR_DETAIL);
                $detail_month = $this->db->get('MS_DEPOSIT_IR_MONTH')->result();
                foreach ($detail_month as $dtl) {
                    $html .= '<div class="row" id="umum_detail_edit' . $dtl->ID_DEPOSIT_IR_MONTH . '">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Month<span style="color:orange">*</span></label>
                                                    <input type="hidden" name="id_deposit_ir_month" value="' . $dtl->ID_DEPOSIT_IR_MONTH . '">
                                                    <input type="text" class="form-control" id="month" onkeyup="validate(this);" name="month[' . $no . '][]"
                                                        placeholder="Month" parsley-trigger="change" required="" value="' . $dtl->MONTH . '">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Interest Rate<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="interest_rate_month"
                                                        name="interest_rate_month[' . $no . '][]" placeholder="Interest Rate"
                                                        parsley-trigger="change" required="" value="' . $dtl->INTEREST_RATE . '">
                                                </div>
                                            </div>
                                            <div class="col-4 center-right">
                                                <button type="button" class="btn btn-outline-danger"><i
                                                        class="fa fa-minus"
                                                        onclick="remove(\'umum_detail_edit' . $dtl->ID_DEPOSIT_IR_MONTH . '\')"></i></button>
                                            </div>
                                        </div>
                                    ';
                }

                $html .= '</div></div></div>
                        </div>';
            } elseif ($category == '2') {
                $html .= '<div class="card border-left-info border-right-info rounded-0" id="edit' . $row->ID_DEPOSIT_IR_DETAIL . '" data-id="' . $row->ID_DEPOSIT_IR_DETAIL . '">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                        <div class="row">
                                            <input type="hidden" value="' . $row->ID_DEPOSIT_IR_DETAIL . '" name="detail_id[]">

                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label>Balance Indonesian<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="balance_id"
                                                        name="balance_id[]" placeholder="Balance in Indonesian"
                                                        parsley-trigger="change" required="" value="' . $row->BALANCE_ID . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label>Balance English<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="balance_en"
                                                        name="balance_en[]" placeholder="Balance in English"
                                                        parsley-trigger="change" required="" value="' . $row->BALANCE_EN . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Operation<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="operation"
                                                        name="operation[]" placeholder="Operation"
                                                        parsley-trigger="change" required="" value="' . $row->OPERATION . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Min Tabungan Kesra<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="min_tabungan_kesra"
                                                        name="min_tabungan_kesra[]" placeholder="Min Tabungan Kesra"
                                                        parsley-trigger="change" required="" value="' . $row->MIN_TABUNGAN_KESRA . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Interest Rates<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="interest_rate"
                                                        name="interest_rate[]" placeholder="Interest Rates"
                                                        parsley-trigger="change" required="" value="' . $row->INTEREST_RATE . '">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                        <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                            type="button" onclick="remove(\'edit' . $row->ID_DEPOSIT_IR_DETAIL . '\');"><b><i
                                                    class="fas fa-minus"></i></b>
                                            Remove
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>';

            } elseif ($category == '4') {

                $html .= '<div class="card border-left-info border-right-info rounded-0" id="edit' . $row->ID_DEPOSIT_IR_DETAIL . '" data-id="' . $row->ID_DEPOSIT_IR_DETAIL . '">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                        <div class="row">
                                            <input type="hidden" value="' . $row->ID_DEPOSIT_IR_DETAIL . '" name="detail_id[]">

                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                               <div class="form-group">
                                                   <label>Month<span style="color:orange">*</span></label>
                                                   <input type="text" class="form-control" id="month_perpanjang"
                                                       name="month_perpanjang[]" placeholder="Month" onkeyup="validate(this);"
                                                       parsley-trigger="change" required="" value="' . $row->MONTH . '">
                                               </div>
                                           </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Interest Rate Before<span
                                                            style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="interest_rate_before"
                                                        name="interest_rate_before[]" placeholder="Interest Rate Before"
                                                        parsley-trigger="change" required="" value="' . $row->INTEREST_RATE_BEFORE . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Interest Rate Next<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="interest_rate_next"
                                                        name="interest_rate_next[]" placeholder="Interest Rate Next"
                                                        parsley-trigger="change" required="" value="' . $row->INTEREST_RATE_NEXT . '">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                        <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                            type="button" onclick="remove(\'edit' . $row->ID_DEPOSIT_IR_DETAIL . '\');"><b><i class="fas fa-minus"></i></b>
                                            Remove
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>';

            } elseif ($category == '5') {
                $html .= '<div class="card border-left-info border-right-info rounded-0" id="edit' . $row->ID_DEPOSIT_IR_DETAIL . '" data-id="' . $row->ID_DEPOSIT_IR_DETAIL . '">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                        <div class="row">
                                            <input type="hidden" value="' . $row->ID_DEPOSIT_IR_DETAIL . '" name="detail_id[]">

                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Balance Indonesian<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="balance_id"
                                                        name="balance_id[]" placeholder="Balance in Indonesian"
                                                        parsley-trigger="change" required="" value="' . $row->BALANCE_ID . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Balance English<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="balance_en"
                                                        name="balance_en[]" placeholder="Balance in English"
                                                        parsley-trigger="change" required="" value="' . $row->BALANCE_EN . '">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label>Interest Rate<span style="color:orange">*</span></label>
                                                    <input type="text" class="form-control" id="interest_rate"
                                                        name="interest_rate[]" placeholder="Interest Rate"
                                                        parsley-trigger="change" required="" value="' . $row->INTEREST_RATE . '">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                        <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                            type="button" onclick="remove(\'edit' . $row->ID_DEPOSIT_IR_DETAIL . '\');"><b><i
                                                    class="fas fa-minus"></i></b>
                                            Remove
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>';
            }

            $no++;
        }
        $result = array("html" => $html, "no" => $no);
        return $result;
    }

    public function updateaction($param)
    {
        $ID_DEPOSIT_IR_HEADER = isset($param['ID_DEPOSIT_IR_HEADER']) ? $param['ID_DEPOSIT_IR_HEADER'] : "";

        $WHERE['ID_DEPOSIT_IR_HEADER'] = $ID_DEPOSIT_IR_HEADER;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_DEPOSIT_IR_HEADER'] = isset($param['ID_DEPOSIT_IR_HEADER']) ? $param['ID_DEPOSIT_IR_HEADER'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

}