<!-- Theme JS files -->
<script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script>
<!-- /theme JS files -->
<?php
$is_create = $authorize['is_create'];
$back = isset($data['back']) ? $data['back'] : '';
$DETAIL = isset($data['detail']) ? $data['detail'] : '';
$management = isset($data['management']) ? $data['management'] : '';
$ID_MANAGEMENT_HEADER = isset($management->ID_MANAGEMENT_HEADER) ? $management->ID_MANAGEMENT_HEADER : "";
$NAME = isset($management->NAME) ? $management->NAME : "";
$LINK = isset($management->LINK) ? $management->LINK : "";
$POSITION_CATEGORY = isset($management->POSITION_CATEGORY) ? $management->POSITION_CATEGORY : "";
$POSITION_ID = isset($management->POSITION_ID) ? $management->POSITION_ID : "";
$POSITION_EN = isset($management->POSITION_EN) ? $management->POSITION_EN : "";
$DESCRIPTION_ID = isset($management->DESCRIPTION_ID) ? $management->DESCRIPTION_ID : "";
$DESCRIPTION_EN = isset($management->DESCRIPTION_EN) ? $management->DESCRIPTION_EN : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dropify-wrapper {
    height: 90%;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master Management</a>
                <a class="breadcrumb-item active">Edit</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Management</span> -
                Edit
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_create): ?>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Add Management</h5>
        </div>

        <div class="card-body">
            <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveManagement">
                <input type="hidden" name="id_management_header" id="id_management_header"
                    value="<?php echo $ID_MANAGEMENT_HEADER ?>">
                <input type="hidden" name="listDelete" id="listDelete" value="">
                <fieldset>
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-file-text2 mr-2"></i>
                        Enter your information
                        <a href="#" class="float-right text-default" data-toggle="collapse" data-target="#demo1">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </legend>

                    <div class="collapse show" id="demo1">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
                                    data-default-file="<?php echo $LINK; ?>" />
                                <a href="#" onclick="openTnC('<?php echo base_url('management/tncupload') ?>')">Terms
                                    and Conditions Upload</a>

                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                        <div class="form-group">
                                            <label>Name<span style="color:orange">*</span></label>
                                            <input type="text" class="form-control" id="name" name="name"
                                                placeholder="Name" required="" parsley-trigger="change"
                                                value="<?php echo $NAME; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Category<span style="color:orange">*</span></label>
                                            <select class="form-control select2" name="category" id="category"
                                                required="">
                                                <option selected disabled>Choose Category</option>
                                                <?php
$CATEG_POSITION_ID = $this->config->item('POSITION_ID');
$CATEG_POSITION_EN = $this->config->item('POSITION_EN');
$ID_CATEG_POSITION = $this->config->item('ID_CATEG_POSITION');
$Exp_POSITION_ID = explode(":", $CATEG_POSITION_ID);
$Exp_POSITION_EN = explode(":", $CATEG_POSITION_EN);
$Exp_ID_CATEG_POSITION = explode(":", $ID_CATEG_POSITION);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_POSITION); $i++) {
    $ID = $Exp_ID_CATEG_POSITION[$i];
    $DESC_EN = $Exp_POSITION_EN[$i];
    $DESC_ID = $Exp_POSITION_ID[$i];
    ?>
                                                <option value="<?php echo $ID ?>"
                                                    <?php if ($ID == $POSITION_CATEGORY) {echo 'selected';}?>>
                                                    <?php echo $DESC_EN . ' / ' . $DESC_ID ?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Position Indonesian<span style="color:orange">*</span></label>
                                            <input type="text" class="form-control" id="position_id" name="position_id"
                                                placeholder="Position in Indonesian" required=""
                                                parsley-trigger="change" value="<?php echo $POSITION_ID; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Position English<span style="color:orange">*</span></label>
                                            <input type="text" class="form-control" id="position_en" name="position_en"
                                                placeholder="Position in English" required="" parsley-trigger="change"
                                                value="<?php echo $POSITION_EN; ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description_id">Description Indonesia<span
                                    style="color: orange">*</span></label>
                            <textarea class="form-control" name="description_id" id="description_id" rows="4"
                                required="" parsley-trigger="change"><?php echo $DESCRIPTION_ID; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description_en">Description English<span style="color: orange">*</span></label>
                            <textarea class="form-control" name="description_en" id="description_en" rows="4"
                                required="" parsley-trigger="change"><?php echo $DESCRIPTION_EN; ?></textarea>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-reading mr-2"></i>
                        Add Work History
                        <button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" type="button"
                            onclick="add_detail();"><b><i class="fas fa-plus"></i></b>
                            Add
                        </button>
                        <a class="float-right text-default" data-toggle="collapse" data-target="#demo2">
                            <i class="icon-circle-down2"></i>
                        </a>
                    </legend>

                    <div class="collapse show" id="demo2">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="workhistory">
                                <thead>
                                    <tr>
                                        <th width="200px">Year ID</th>
                                        <th width="200px">Year EN</th>
                                        <th>Description Indonesia</th>
                                        <th>Description English</th>
                                        <th width="100px" data-orderable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($DETAIL)) {echo $DETAIL;}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>

                <div class="text-right mt-4">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light"> Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
    <?php endif;?>


</div>

<script type="text/javascript">
$(function() {
    $('.select2').select2();
    $('.dropify').dropify();
    changeformfile();
});
var room = 0;

function add_detail() {
    room++;
    $('#workhistory tr:last').after('<tr id="' + room + '" data-id="NEW">' +
        '<td><input type="hidden" name="id_detail[]" id="id_detail" value="NEW"><input type="text" class="form-control" id="year_id" name="year_id[]" placeholder = "Year ID" required = "" parsley-trigger= "change" > </td>' +
        '<td><input type="text" class="form-control" id="year_en" name="year_en[]" placeholder = "Year EN" required = "" parsley-trigger= "change" > </td>' +
        '<td><input type="text" class="form-control" id="desc_id" name="desc_id[]" placeholder = "Description ID" required = "" parsley-trigger = "change" > </td>' +
        '<td><input type="text" class="form-control" id="desc_en" name="desc_en[]" placeholder = "Description EN" required = "" parsley-trigger = "change" > </td>' +
        '<td> <button type="button" class="btn btn-danger btn-icon rounded-round"><i class = "fas fa-minus" onclick="remove(' +
        room + ');" > </i></button > ' +
        '</td>' +
        '</tr>');
}

function remove(rid) {
    Swal.fire({
        title: "Are you sure?",
        type: "warning",
        text: "Your will not be able to recover this data!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes",
    }).then((result) => {
        if (result.value) {
            var deleteid = $('table#workhistory tr#' + rid).data('id');
            var listDelete = $('#listDelete').val();
            if (deleteid != 'NEW') {
                $('table#workhistory tr#' + rid).remove();

                $('#listDelete').val((listDelete == '' ? '' : (listDelete + ',')) + deleteid);
            }
            console.log($('#listDelete').val());

            $('table#workhistory tr#' + rid).remove();
        }
    });

}
$('#frmsaveManagement').on('submit', function(e) {
    e.preventDefault();
    $(".progress").show();
    $(".msg").hide();

    if ($(this).parsley().isValid()) {
        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e) {
                    if (e.lengthComputable) {
                        var percent = Math.round((e.loaded / e.total) * 100);

                        $("#progressBar")
                            .attr("aria-valuenow", percent)
                            .css("width", percent + "%")
                            .text(percent + "%");
                    }
                });
                return xhr;
            },
            url: "<?php echo base_url('management/update'); ?>",
            data: formData,
            dataType: "JSON",
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            success: function(res) {
                var ErrorMessage = res.ErrorMessage;
                var ErrorCode = res.ErrorCode;
                if (ErrorCode != "EC:0000") {
                    Swal.fire({
                        type: "error",
                        html: ErrorMessage,
                        confirmButton: true,
                        confirmButtonColor: "#1FB3E5",
                        confirmButtonText: "Close",
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        type: "success",
                        text: ErrorMessage,
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    setTimeout(function() {
                        var uri = "<?php echo $back; ?>";
                        window.location.href = uri;
                    }, 1500);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire(textStatus).then(function() {
                    Swal.fire({
                        type: "error",
                        title: textStatus,
                    });
                });
            },
            beforeSend: function() {
                $('#loadingBox').show();
            },
            complete: function() {
                $('#loadingBox').fadeOut();
            }
        });
    } else {
        ;
    }
});
</script>