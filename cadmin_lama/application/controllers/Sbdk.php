<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sbdk extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('sbdk_model');
        $this->load->model('currency_model');

    }

    public function index()
    {
        $data = array();
        $data['content'] = "sbdk/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['sbdk'] = $this->sbdk_model->view();

        $this->load->view('templates/view', $data);
    }
    public function add_sbdk()
    {
        $data = array();
        $data['content'] = "sbdk/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'sbdk';
        $data['data']['currency'] = $this->currency_model->get_currency();

        $this->load->view('templates/view', $data);
    }
    public function edit_sbdk($id)
    {
        $data = array();
        $data['content'] = "sbdk/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['sbdk'] = $this->sbdk_model->get_edit($id);
        $data['data']['back'] = base_url() . 'sbdk';
        $this->load->view('templates/view', $data);
    }
    public function detail_sbdk($id)
    {
        $data = array();
        $data['content'] = "sbdk/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['sbdk'] = $this->sbdk_model->get_edit($id);
        $data['data']['back'] = base_url() . 'sbdk';
        $this->load->view('templates/view', $data);
    }

    public function add()
    {
        // var_dump($_POST);exit();
        // var_dump($_FILES);exit();
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d H:i:s', strtotime($_POST['effective_date'] . $_POST['time'])) : "";
        $DESCRIPTION = "SUKU BUNGA DASAR KREDIT RUPIAH (PRIME LENDING RATE) | Last Updated " . date("Y-m-d H:i:s");
        //detail
        $CORPORATE_LOAN = isset($_POST['corporate_loan']) ? $_POST['corporate_loan'] : "";
        $RETAIL_LOAN = isset($_POST['retail_loan']) ? $_POST['retail_loan'] : "";
        $MICRO_LOAN = isset($_POST['micro_loan']) ? $_POST['micro_loan'] : "";
        $MORTGAGE = isset($_POST['mortgage']) ? $_POST['mortgage'] : "";
        $NONMORTGAGE = isset($_POST['nonmortgage']) ? $_POST['nonmortgage'] : "";

        $param['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $param['DESCRIPTION'] = $DESCRIPTION;
        $param['CORPORATE_LOAN'] = $CORPORATE_LOAN;
        $param['RETAIL_LOAN'] = $RETAIL_LOAN;
        $param['MICRO_LOAN'] = $MICRO_LOAN;
        $param['MORTGAGE'] = $MORTGAGE;
        $param['NONMORTGAGE'] = $NONMORTGAGE;
        $param['CREATED_BY'] = $_SESSION['username'];
        $param['CREATED_DATE'] = date('Y-m-d H:i:s');
        $Insert = $this->sbdk_model->add($param);
        if ($Insert['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
        die(json_encode($JSON));

    }

    public function update()
    {
        $ID_SBDK = isset($_POST['id_sbdk']) ? $_POST['id_sbdk'] : "";
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d H:i:s', strtotime($_POST['effective_date'] . $_POST['time'])) : "";
        $DESCRIPTION = "SUKU BUNGA DASAR KREDIT RUPIAH (PRIME LENDING RATE) | Last Updated " . date("Y-m-d H:i:s");
        //detail
        $CORPORATE_LOAN = isset($_POST['corporate_loan']) ? $_POST['corporate_loan'] : "";
        $RETAIL_LOAN = isset($_POST['retail_loan']) ? $_POST['retail_loan'] : "";
        $MICRO_LOAN = isset($_POST['micro_loan']) ? $_POST['micro_loan'] : "";
        $MORTGAGE = isset($_POST['mortgage']) ? $_POST['mortgage'] : "";
        $NONMORTGAGE = isset($_POST['nonmortgage']) ? $_POST['nonmortgage'] : "";

        $param['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $param['DESCRIPTION'] = $DESCRIPTION;
        $param['CORPORATE_LOAN'] = $CORPORATE_LOAN;
        $param['RETAIL_LOAN'] = $RETAIL_LOAN;
        $param['MICRO_LOAN'] = $MICRO_LOAN;
        $param['MORTGAGE'] = $MORTGAGE;
        $param['NONMORTGAGE'] = $NONMORTGAGE;
        $param['USER_LOG'] = $_SESSION['username'];
        $param['DATE_LOG'] = date('Y-m-d H:i:s');
        $Update = $this->sbdk_model->updateaction($param, $ID_SBDK);
        if ($Update['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
        die(json_encode($JSON));

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_SBDK = isset($Data['ID_SBDK']) ? $Data['ID_SBDK'] : "";
        $param['ID_SBDK'] = $ID_SBDK;
        $Delete = $this->sbdk_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_SBDK']) ? $Data['ID_SBDK'] : "";

        $data['STATUS'] = "0";
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_SBDK' => $id);
        $Active = $this->sbdk_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_SBDK']) ? $Data['ID_SBDK'] : "";

        $data['STATUS'] = "88";
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_SBDK' => $id);
        $Active = $this->sbdk_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_SBDK']) ? $Data['ID_SBDK'] : "";

        $data['STATUS'] = "99";
        $data['APPROVED_BY'] = $_SESSION['username'];
        $data['APPROVED_DATE'] = date('Y-m-d H:i:s');
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_SBDK' => $id);
        $Active = $this->sbdk_model->updatestatus($data, $where);

        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $dataTransaction = array(
            array(
                'ID' => $id,
                'MODULE' => 'SBDK',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => date('Y-m-d H:i:s'),
                'COLUMN_DATA' => 'APPROVED_DATE',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
            array(
                'ID' => $id,
                'MODULE' => 'SBDK',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => $_SESSION['username'],
                'COLUMN_DATA' => 'APPROVED_BY',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
        );
        $InsertTransaction = $this->db->insert_batch('LOG_TRANSACTION', $dataTransaction);
        if (!$InsertTransaction) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }

        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Approved");
        die(json_encode($JSON));
    }
}