<?php
$data = isset($data) ? $data : "";
$ID_AWARD = isset($data['award']->ID_AWARD) ? $data['award']->ID_AWARD : "";
$TITLE_EN = isset($data['award']->TITLE_EN) ? $data['award']->TITLE_EN : "";
$TITLE_ID = isset($data['award']->TITLE_ID) ? $data['award']->TITLE_ID : "";
$DESCRIPTION_EN = isset($data['award']->DESCRIPTION_EN) ? $data['award']->DESCRIPTION_EN : "";
$DESCRIPTION_ID = isset($data['award']->DESCRIPTION_ID) ? $data['award']->DESCRIPTION_ID : "";
$LINK = isset($data['award']->LINK) ? $data['award']->LINK : "";
$STATUS = isset($data['award']->STATUS) ? $data['award']->STATUS : "";
$CREATED_BY = isset($data['award']->CREATED_BY) ? $data['award']->CREATED_BY : "";
$CREATED_DATE = isset($data['award']->CREATED_DATE) ? $data['award']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}

$menu = isset($data['menu']) ? $data['menu'] : array();
$back = isset($data['back']) ? $data['back'] : '';
$is_update = $authorize['is_update'];
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Award</a>
                <a href="#" class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Award</span> - Detail
                <small><?php echo $TITLE_ID . '-' . $TITLE_EN ?></small>

            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <div class="row">
        <div class="col-md-6">

            <!-- Zooming -->
            <div class="card text-center">
                <div class="card-img-actions">
                    <a href="<?php echo $LINK ?>" data-popup="lightbox">
                        <img class="card-img-top img-fluid" src="<?php echo $LINK ?>" alt="">
                        <span class="card-img-actions-overlay card-img-top">
                            <i class="icon-plus3 icon-2x"></i>
                        </span>
                    </a>
                </div>

                <div class="card-body">
                    <h5 class="card-title"><?php echo $TITLE_ID ?></h5>
                    <p class="card-text"><?php echo $DESCRIPTION_ID ?></p>
                </div>

                <div class="card-footer d-flex justify-content-between text-center">
                    <span class="text-muted"><?php echo convertDate($CREATED_DATE) ?>
                    </span>
                    <span>
                        <?php echo $STS ?>

                    </span>
                </div>
            </div>
            <!-- /zooming -->

        </div>
        <div class="col-md-6">

            <!-- Zooming -->
            <div class="card text-center">
                <div class="card-img-actions">
                    <a href="<?php echo $LINK ?>" data-popup="lightbox">
                        <img class="card-img-top img-fluid" src="<?php echo $LINK ?>" alt="">
                        <span class="card-img-actions-overlay card-img-top">
                            <i class="icon-plus3 icon-2x"></i>
                        </span>
                    </a>
                </div>

                <div class="card-body">
                    <h5 class="card-title"><?php echo $TITLE_EN ?></h5>
                    <p class="card-text"><?php echo $DESCRIPTION_EN ?></p>
                </div>

                <div class="card-footer d-flex justify-content-between text-center">
                    <span class="text-muted"><?php echo convertDate($CREATED_DATE) ?>
                    </span>
                    <span>
                        <?php echo $STS ?>

                    </span>
                </div>
            </div>
            <!-- /zooming -->

        </div>
    </div>

</div>