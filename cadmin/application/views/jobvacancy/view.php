<?php
$Jobvacancy = isset($data['jobvacancy']) ? $data['jobvacancy'] : array();

$is_create = $authorize['is_create'];
$is_read = $authorize['is_read'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Career</a>
                <a href="#" class="breadcrumb-item active">Master Job Vacancy</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">
            <a href="<?php echo base_url("jobvacancy/add_jobvacancy/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add Job Vacancy
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Code</th>
                    <th width="">Position</th>
                    <th width="" data-orderable="false">Date</th>
                    <th width="" data-orderable="false">Language</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Jobvacancy as $jobvacancy) {
    $ID_JOBVACANCY = isset($jobvacancy['ID_JOBVACANCY']) ? $jobvacancy['ID_JOBVACANCY'] : "";
    $CODE = isset($jobvacancy['CODE']) ? $jobvacancy['CODE'] : "";
    $POSITION = isset($jobvacancy['POSITION']) ? $jobvacancy['POSITION'] : "";
    $JOB_DESCRIPTION = isset($jobvacancy['JOB_DESCRIPTION']) ? $jobvacancy['JOB_DESCRIPTION'] : "";
    $REQUIREMENT = isset($jobvacancy['REQUIREMENT']) ? $jobvacancy['REQUIREMENT'] : "";
    $LANGUAGE = isset($jobvacancy['LANGUAGE']) ? $jobvacancy['LANGUAGE'] : "";
    $DETAIL = isset($jobvacancy['DETAIL']) ? $jobvacancy['DETAIL'] : "";
    $START_DATE = isset($jobvacancy['START_DATE']) ? $jobvacancy['START_DATE'] : "";
    $END_DATE = isset($jobvacancy['END_DATE']) ? $jobvacancy['END_DATE'] : "";
    $STATUS = isset($jobvacancy['STATUS']) ? $jobvacancy['STATUS'] : "";
    $CREATED_BY = isset($jobvacancy['CREATED_BY']) ? $jobvacancy['CREATED_BY'] : "";
    $CREATED_DATE = isset($jobvacancy['CREATED_DATE']) ? date('d M Y', strtotime($jobvacancy['CREATED_DATE'])) : "";
    $USER_LOG = isset($jobvacancy['USER_LOG']) ? $jobvacancy['USER_LOG'] : "";
    $DATE_LOG = isset($jobvacancy['DATE_LOG']) ? date('d M Y', strtotime($jobvacancy['DATE_LOG'])) : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $jobvacancy;
    $enc = base64_encode(json_encode($paramaction));

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    if ($LANGUAGE == "EN") {
        $LANGUAGE = "English";
    } elseif ($LANGUAGE == "ID") {
        $LANGUAGE = "Indonesia";
    }

    ?>
                <tr>
                    <td>
                        <?php echo $CODE ?>
                    </td>
                    <td>
                        <?php echo $POSITION ?>
                    </td>
                    <td>
                        <?php echo convertDate($START_DATE) . ' - ' . convertDate($END_DATE) ?>
                    </td>
                    <td>
                        <?php echo $LANGUAGE ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_read)) {?>
                                    <a href="<?php echo base_url("jobvacancy/detail_jobvacancy/") . $ID_JOBVACANCY ?>"
                                        class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                    <?php }?>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("jobvacancy/edit_jobvacancy/") . $ID_JOBVACANCY ?>"
                                        class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <?php if ($STATUS == "0") {?>
                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i> Activate</a>
                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i> Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>
                                    <?}elseif($STATUS == "99"){?>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i> Deactive</a>
                                    <?php }?>
                                    <?php }?>
                                    <?php if (!empty($is_delete)) {?>
                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?>

                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(5).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}

function changeSelectLang(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(3)
            .search(dis.value)
            .draw();
    } else {
        t.column(3)
            .search(dis.value)
            .draw();
    }

}
</script>