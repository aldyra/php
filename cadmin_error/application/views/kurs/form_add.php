<?php
$is_create = $authorize['is_create'];
$back = isset($data['back']) ? $data['back'] : '';
$currency = isset($data['currency']) ? $data['currency'] : array();
?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dropify-wrapper {
    height: 100%;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master Kurs</a>
                <a class="breadcrumb-item active">Add</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Kurs</span> - Add
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_create): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveKurs">
            <div class="card-header bg-blue-800 d-flex justify-content-between">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Effective Date<span style="color:orange">*</span></label>

                        <div class="input-group" style="color:#333">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar5"></i></span>
                            </span>
                            <input type="text" name="effective_date" id="effective_date"
                                class="form-control pickadate-selectors" placeholder="Select Date.." required="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Time<span style="color:orange">*</span></label>

                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-watch2"></i></span>
                        </span>
                        <input type="text" class="form-control" name="time" id="anytime-time">
                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">Kurs Detail</h6>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="workhistory">
                        <thead>
                            <tr>
                                <th width="200px">Currency Code</th>
                                <th>Currency Name</th>
                                <th>Buy</th>
                                <th>Sell</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0;foreach ($currency as $curr) {$no++;?>
                            <tr>
                                <td><?php echo $curr['CURRENCY_CODE'] ?></td>
                                <td><?php echo $curr['CURRENCY_NAME'] ?></td>
                                <td>
                                    <input type="hidden" id="id_currency<?php echo $no; ?>" name="id_currency[]"
                                        value="<?php echo $curr['ID_CURRENCY'] ?>">
                                    <input type="text" class="form-control" name="buyV[]" id="BUYV<?php echo $no; ?>"
                                        placeholder="Value Buy" required=""
                                        onkeyup="validate_number(this);changePrice(this, 'BUY<?php echo $no; ?>')">
                                    <input type="hidden" class="form-control" name="buy[]" id="BUY<?php echo $no; ?>"
                                        placeholder="Value Buy" required="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="sellV[]" id="SELLV<?php echo $no; ?>"
                                        placeholder="Value Buy" required=""
                                        onkeyup="validate_number(this);changePrice(this, 'SELL<?php echo $no; ?>')">
                                    <input type="hidden" class="form-control" name="sell[]" id="SELL<?php echo $no; ?>"
                                        placeholder="Value Buy" required="">
                                </td>
                            </tr>
                            <?php }?>

                        </tbody>
                    </table>
                </div>




            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

    <?php endif;?>


</div>

<script type="text/javascript">
$(function() {
    $('.select2').select2();
    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });
    // Time picker
    $('#anytime-time').AnyTime_picker({
        format: '%H:%i:%s'
    });
});
var room = 0;

$('#frmsaveKurs').on('submit', function(e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        $("#frmsaveKurs").parsley().validate();
        var form = $("#frmsaveKurs");
        var data = form.serialize();

        if ($("#frmsaveKurs").parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('kurs/add') ?>",
                type: "POST",
                data: data,
                cache: false,
                dataType: "json",
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            var uri = "<?php echo $back; ?>";
                            window.location.href = uri;
                        }, 1500);
                    }
                },
            });
        }
    } else {

    }
});
</script>