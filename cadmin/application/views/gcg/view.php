<?php
$GCG = isset($data['gcg']) ? $data['gcg'] : array();
$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Controllers = $this->uri->segment(1);

?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item active">Master Reporting</a>
                <a href="#" class="breadcrumb-item active">Report GCG</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>

        <div class="card-body">
            <a href="<?php echo base_url("gcg/add_gcg/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add GCG
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Description Indonesian</th>
                    <th>Description English</th>
                    <th data-orderable="false"  width="200">Created Date</th>
                    <th>Created By</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th  data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($GCG as $gcg) {
    $ID_GCG_HEADER = isset($gcg['ID_GCG_HEADER']) ? $gcg['ID_GCG_HEADER'] : "";
    $DESCRIPTION_ID = isset($gcg['DESCRIPTION_ID']) ? $gcg['DESCRIPTION_ID'] : "";
    $DESCRIPTION_EN = isset($gcg['DESCRIPTION_EN']) ? $gcg['DESCRIPTION_EN'] : "";
    $STATUS = isset($gcg['STATUS']) ? $gcg['STATUS'] : "";
    $CREATED_BY = isset($gcg['CREATED_BY']) ? $gcg['CREATED_BY'] : "";
    $CREATED_DATE = isset($gcg['CREATED_DATE']) ? date('d M Y', strtotime($gcg['CREATED_DATE'])) : "";
    $USER_LOG = isset($gcg['USER_LOG']) ? $gcg['USER_LOG'] : "";
    $DATE_LOG = isset($gcg['DATE_LOG']) ? date('d M Y', strtotime($gcg['DATE_LOG'])) : "";

    $paramaction['Url'] = base_url() . $Controllers;
    $paramaction['Data'] = $gcg;
    $enc = base64_encode(json_encode($paramaction));

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <?php echo $DESCRIPTION_ID ?>
                    </td>
                    <td>
                        <?php echo $DESCRIPTION_EN ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo base_url("gcg/detail_gcg/") . $ID_GCG_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("gcg/edit_gcg/") . $ID_GCG_HEADER ?>"
                                        class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>
                                    <?php if ($STATUS == "0") {?>
                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i> Activate</a>
                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i> Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>
                                    <?}elseif($STATUS == "99"){?>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i> Deactive</a>
                                    <?php }?>
                                    <?php }?>
                                    <?php if (!empty($is_delete)) {?>
                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?>

                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(5).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}
</script>