<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
if($SiteLang == "id"){
	$Url = base_url().$SiteLang."/".$this->config->item('url_jobvacancy_id');
}elseif($SiteLang == "en"){
	$Url = base_url().$SiteLang."/".$this->config->item('url_jobvacancy_en');
}

$FILE_BANNER = $MENU_CHILD_FILE_BANNER;
$stylebanner = "height:250px";
if(!empty($FILE_BANNER)){
	$stylebanner = "background-image:url('".$FILE_BANNER."'); background-size:cover;height:250px;";
}
?>
<div class="row row-header" style="<?php echo $stylebanner ?>">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important;"><?php echo $this->lang->line('lowker-4') ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color: #707070;"><?php echo $this->lang->line('home') ?></a> / 
		<label style="color: #707070;"><?php echo $this->lang->line('lowker-4') ?></label> /
		<label style="color:#707070"><?php echo $this->lang->line('lowker-5') ?></label>
	</div>
</div>

<div class="container" style="padding-top: 20px; padding-bottom: 50px;">
	<div class="row d-padtop-10">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 t-center d-padtop-10">
			<h3 class="t-default"><b><?php echo $this->lang->line('lowker-2') ?></b></h3>
			<br>
		</div>
	</div>
	<div class="row" align="center" style="text-align: center;">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 t-center d-padtop-10">
			<?php 
			$i = 0;
			foreach($data as $row){
				$NAME_HEADER = isset($row['NAME']) ? $row['NAME']:"";
				$ID = isset($row['ID']) ? $row['ID']:"";
				$DETAIL = isset($row['DETAIL']) ? $row['DETAIL']:"";
				if($ID == $ID_GROUP_JOBVACANCY){
					$show = "active";
				}else{
					$show = "";
				}
				$UrlDetail = $Url."/".$this->lang->line('lowker-0')."/".slugify($NAME_HEADER);
			?>
				<div class="row-position-jobvacancy <?php echo $show ?>" style="color: #666666;" onclick="window.location.href='<?php echo $UrlDetail ?>'">
					<?php echo $NAME_HEADER ?>
				</div>
			<?php 
			$i++;
			} 
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 t-center d-padtop-10">
			<!-- <?php echo $pagination ?> -->
		</div>
	</div>
	
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 t-center d-padding">
			<div  style="margin-top: 35px;">
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
						<?php
						foreach ($jobvacancy as $row2) {
							$CODE = isset($row2['CODE']) ? $row2['CODE']:"";
			    			$POSITION = isset($row2['POSITION']) ? $row2['POSITION']:"";
			    			$JOB_DESCIRPTION = isset($row2['JOB_DESCIRPTION']) ? $row2['JOB_DESCIRPTION']:"";
			    			$REQUIREMENT = isset($row2['REQUIREMENT']) ? $row2['REQUIREMENT']:"";
			    			$DETAIL = isset($row2['DETAIL']) ? $row2['DETAIL']:"";
			    			$START_DATE = isset($row2['START_DATE']) ? $row2['START_DATE']:"";
			    			$END_DATE = isset($row2['END_DATE']) ? $row2['END_DATE']:"";
						?>
							<div class="card" style="margin-bottom: 10px;">
								<div class="card-body" style="padding: 20px;">
									<div class="row">
										<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-9 d-padding" style="text-align: left; line-height: 35px;">
											<span style="display: inline-block;vertical-align: middle; color: #243E8B; font-weight: 700; line-height: 3;">
												<?php echo $POSITION ?>
											</span>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-9 d-padding col-register" style="font-size: 14px;">
											<span style="color: #213E8B;font-weight: 400;"><?php echo $this->lang->line('lowker-6')?></span><br>
											<span>
												<?php echo date('d M Y', strtotime($START_DATE))?>
											</span>
											<span>
												s/d
											</span>
											<span>
												<?php echo date('d M Y', strtotime($END_DATE))?>
											</span>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-3 d-padding" style="text-align: right;">
											<a href="<?php echo $Url.'/detail/'.slugify($CODE) ?>">
												<button type="button" style="background-color: #28448f;border: none; padding: 15px; color: white; padding-top: 7px; padding-bottom: 7px; cursor: pointer;margin-top: 10px;">
													<?php echo $this->lang->line('lowker-7')?>
												</button>
											</a>
										</div>
									</div>
								</div>
							</div>
						<?php
						}
						?>
					</div>									
				</div>
			</div>
		</div>
	</div>

	<div style="max-width: 100%" class="mb-3 mt-3">
		<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
		<div style="clear:both"></div>
	</div>

	<div class="row mt-5">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 t-mob-center d-padtop-10">
			<?php 
			$urlpage = $pages_active;
			if($pages_active > 1){
				$prevpage = $pages_active-1;
				$urlpageprev = $urlpaging.$prevpage;
				$active = " active";
			}else{
				$urlpageprev = "javascript:void(0)";
				$active = "";
			}
			?>
			<div class="page-prev<?php echo $active?>" onclick="window.location.href = '<?php echo $urlpageprev ?>'">
				<i class="fa fa-arrow-left"></i>
			</div>
			<?php 
			for($i=1; $i<=$pages; $i++)
			{
				if($i == $pages_active){
					$active = " active";
				}else{
					$active = "";
				}
			?>
				<div class="pagination<?php echo $active ?>" onclick="window.location.href='<?php echo $urlpaging.$i ?>'">
					<a href="javascript:void(0)"><?php echo "&nbsp;".$i."&nbsp;";?></a>
				</div>
			<?php
			}

			if($pages_active >= $pages){
				$active = "";
				$urlpagenext = "javascript:void(0)";
			}else{
				$active = " active";
				$nextpage = $pages_active+1;
				$urlpagenext = $urlpaging.$nextpage;
			}
			?>
			<div class="page-next<?php echo $active?>" onclick="window.location.href = '<?php echo $urlpagenext ?>'">
				<i class="fa fa-arrow-right"></i>
			</div>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10 t-mob-center" style="padding-top: 20px; color: #9d9d9d;">
			<?php 
			$SHOW = isset($_SESSION['pagination']['jobvacancy']) ? $_SESSION['pagination']['jobvacancy']:10;
			// $pagerow = array(10, 20, 30, 40, 50, 100); 
			$pagerow = array(10, 20, 30, 40, 50, 100); 
			?>
			<select name="pagerow" id="pagerow" class="form-control" data-url="jobvacancy" style="font-size: 14px; margin-right: 5px; background-color: white; width: 80px; display: initial !important;">
				<?php 
				for($i=0; $i<sizeof($pagerow); $i++){
					if($SHOW == $pagerow[$i]){
						$selected = "selected";
					}else{
						$selected = "";
					}
				?>
					<option value="<?php echo $pagerow[$i]?>" <?php echo $selected ?>><?php echo $pagerow[$i]?></option>
				<?php
				}
				?>
			</select>
			<?php echo ucwords($this->lang->line('result')) ?> : <?php echo $start + 1; ?> - <?php echo $SHOW * $pages_active ?> of <?php echo $total ?>
		</div>
	</div>
</div>

<style type="text/css">
	div.pagination{
		padding:0;
		/*background-color: transparent;*/
		background-color: #f1f1f1; 
		display: inline-table;
		text-align: center;
		cursor: pointer;
		/*margin-left: 2px;
		margin-right: 2px;
		padding-left: 5px;
		padding-right: 5px;*/
		width: 25px;
	    height: 25px;
	    line-height: 25px;
	    border-radius: 8px;
	    color: #9D9D9D;
	}

	div.pagination:hover{
		background-color: #28448f;
	}

	div.pagination:hover a{
		color: white !important;
	}

	div.pagination.active{
		background-color: #28448f;
	}

	div.pagination a{
		color: #707070 !important;
		text-decoration: none;
		font-size: 13px;
	}

	div.pagination.active a{
		color: white !important;
		text-decoration: none;
	}

	div.page-prev {
		margin-right: 10px; 
		cursor: pointer;
		display: inline-table;
		vertical-align: middle;
		width: 30px;
	    height: 30px;
	    line-height: 30px;
	    text-align: center;
	    background: #cccccc !important;
    	border-radius: 50% !important;
	}

	div.page-prev svg{
		color: #fff !important;
		/*font-size: 20px;*/
	}

	div.page-prev.active svg{
		color: #fff !important;
	}

	div.page-next {
		margin-left: 10px; 
		cursor: pointer;
		display: inline-table;
		vertical-align: middle;
		width: 30px;
	    height: 30px;
	    line-height: 30px;
	    text-align: center;
	    background: #cccccc !important;
    	border-radius: 50% !important;
	}

	div.page-next svg{
		color: #fff !important;
	}

	div.page-next.active svg{
		color: #fff !important;
	}

	div.page-prev:hover, div.page-next:hover{
		background-color: #28448f !important; 
	}

	div.row-position-jobvacancy{
		padding-top: 10px; 
		padding-left: 30px; 
		padding-right: 30px; 
		padding-bottom: 10px;
		border-radius: 25px; 
		border: 1px solid rgba(0, 0, 0, 0.125); 
		margin: 5px;
		cursor: pointer;
		display: inline-block;
	}

	div.card-detail{
		display: none;
	}

	div.card-detail.show{
		display: block !important;
		background-color: color;
	}

	div.row-position-jobvacancy.active{
		background-color: #28448f;
		color: white !important;
	}

	@media all and (min-width: 992px) {
		.col-register{
			text-align: center;
		}

		.t-mob-center{
			text-align: right;
		}
	}

	@media all and (max-width: 992px) {
		.col-register{
			text-align: left;
		}

		.t-mob-center{
			text-align: center;
		}
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('#pagerow').change(function(){
			var show = $(this).val();
			var data = $(this).data('url');
			var SiteLang = "<?php echo $_SESSION['site_lang'] ?>";
			var url = '<?php echo base_url() ?>'+SiteLang+'/main/show_row';
			$.ajax({
				url : url,
				type : 'post',
				data : {
					show:show,
					id:data
				},
				cache : false,
				dataType : 'json',
				beforeSend : function(){
					$('.preloader').fadeIn();
				},
				success : function(res){
					window.location.reload();
				}
			});
		});
	});
</script>
