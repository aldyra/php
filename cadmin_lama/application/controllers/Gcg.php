<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gcg extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('gcg_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "gcg/view";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['gcg'] = $this->gcg_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Allowed files are ' . str_replace("|", ", ", $this->config->item('allowed_types_file')) . ' <br>
                    Max file size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }
    public function add_gcg()
    {
        $data = array();
        $data['content'] = "gcg/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'gcg';

        $this->load->view('templates/view', $data);
    }
    public function detail_gcg($id)
    {
        $data = array();
        $data['content'] = "gcg/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['gcg'] = $this->gcg_model->get_edit($id);
        $data['data']['detail'] = $this->gcg_model->get_detail_gcg($id);
        $data['data']['back'] = base_url() . 'gcg';
        $this->load->view('templates/view', $data);
    }
    public function edit_gcg($id)
    {
        $data = array();
        $data['content'] = "gcg/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['gcg'] = $this->gcg_model->get_edit($id);
        $data['data']['detail'] = $this->gcg_model->get_detail($id);
        $data['data']['back'] = base_url() . 'gcg';
        $this->load->view('templates/view', $data);
    }
    public function add()
    {
        $NAME_ID = isset($_POST['name_id']) ? $_POST['name_id'] : "";
        $NAME_EN = isset($_POST['name_en']) ? $_POST['name_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";
        // $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        $files = $_FILES;
        $FILE_ID = isset($_FILES["file_id"]['name']) ? $_FILES["file_id"]['name'] : "";
        $FILE_EN = isset($_FILES["file_en"]['name']) ? $_FILES["file_en"]['name'] : "";
        // var_dump($files);exit();
        if (empty($FILE_ID) && empty($FILE_EN)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File is required!");
            die(json_encode($JSON));
        }
        if (empty($NAME_EN) || empty($NAME_ID)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "Detail is required!");
            die(json_encode($JSON));
        }
        $this->db->trans_begin();

        $dataheader['DESCRIPTION_ID'] = $DESCRIPTION_ID;
        $dataheader['DESCRIPTION_EN'] = $DESCRIPTION_EN;
        $dataheader['CREATED_BY'] = $_SESSION['username'];
        $insert_header = $this->db->insert('MS_GCG_HEADER', $dataheader);
        $ID_GCG_HEADER = $this->db->insert_id();

        if (!$insert_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        } else {
            $pathgcg = $this->config->item('save_report_gcg');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size');
            $config['max_width'] = $this->config->item('max_width');
            $config['min_width'] = $this->config->item('min_width');
            $config['max_height'] = $this->config->item('max_height');
            $config['min_height'] = $this->config->item('min_height');
            $config['upload_path'] = $this->config->item('path_report_gcg');
            $Extension = $this->config->item('allowed_types_file');
            $Expfile = explode("|", $Extension);

            $cpt = count($NAME_ID);

            for ($i = 0; $i < $cpt; $i++) {
                if (!empty($files["file_id"]["name"][$i])) {
                    $path_parts = pathinfo($files["file_id"]["name"][$i]);
                    $forUpload['extension'] = $path_parts['extension'];
                    $config['file_name'] = 'REPORT_ID' . date('ymdHis') . $i . '.' . $forUpload['extension'];
                    $_FILES['file_id']['name'] = $config['file_name'];
                    $_FILES['file_id']['type'] = $files['file_id']['type'][$i];
                    $_FILES['file_id']['tmp_name'] = $files['file_id']['tmp_name'][$i];
                    $_FILES['file_id']['error'] = $files['file_id']['error'][$i];
                    $_FILES['file_id']['size'] = $files['file_id']['size'][$i];
                    $filename = $_FILES['file_id']['name'];
                    $forUpload['file_name_id'] = $config['file_name'];

                    if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                        $this->upload->initialize($config);
                        if (!is_dir($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }

                        if (!$this->upload->do_upload('file_id')) {
                            $error = array('error' => $this->upload->display_errors());
                            $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                            $this->db->trans_rollback();
                            die(json_encode($JSON));

                        } else {
                            $LINK_ID[$i] = $pathgcg . $forUpload['file_name_id'];
                        }
                    } else {
                        $JSON = array("ErrorCode" => "EC:002AB", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                }
                if (!empty($files["file_en"]["name"][$i])) {
                    $path_parts = pathinfo($files["file_en"]["name"][$i]);
                    $forUpload['extension'] = $path_parts['extension'];
                    $config['file_name'] = 'REPORT_EN' . date('ymdHis') . $i . '.' . $forUpload['extension'];
                    $_FILES['file_en']['name'] = $config['file_name'];
                    $_FILES['file_en']['type'] = $files['file_en']['type'][$i];
                    $_FILES['file_en']['tmp_name'] = $files['file_en']['tmp_name'][$i];
                    $_FILES['file_en']['error'] = $files['file_en']['error'][$i];
                    $_FILES['file_en']['size'] = $files['file_en']['size'][$i];
                    $filename = $_FILES['file_en']['name'];
                    $forUpload['file_name_en'] = $config['file_name'];

                    if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                        $this->upload->initialize($config);
                        if (!is_dir($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }

                        if (!$this->upload->do_upload('file_en')) {
                            $error = array('error' => $this->upload->display_errors());
                            $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                            $this->db->trans_rollback();
                            die(json_encode($JSON));

                        } else {
                            $LINK_EN[$i] = $pathgcg . $forUpload['file_name_en'];
                        }
                    } else {
                        $JSON = array("ErrorCode" => "EC:002AB", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                }

                $datadetail['ID_GCG_HEADER'] = $ID_GCG_HEADER;
                $datadetail['NAME_ID'] = $NAME_ID[$i];
                $datadetail['NAME_EN'] = $NAME_EN[$i];
                $datadetail['LINK_ID'] = $LINK_ID[$i];
                $datadetail['LINK_EN'] = $LINK_EN[$i];
                $datadetail['CREATED_BY'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('MS_GCG_DETAIL', $datadetail);
                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }

            }
            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }

    public function view()
    {
        $GetData = $this->gcg_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID_GCG_HEADER = isset($_POST['id_gcg_header']) ? $_POST['id_gcg_header'] : "";
        $LIST_DELETE = isset($_POST['listDelete']) ? explode(",", $_POST['listDelete']) : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";

        $NAME_ID = isset($_POST['name_id']) ? $_POST['name_id'] : "";
        $NAME_EN = isset($_POST['name_en']) ? $_POST['name_en'] : "";
        $ID_GCG_DETAIL = isset($_POST['id_detail']) ? $_POST['id_detail'] : "";
        $FILE_URL_ID = isset($_POST['FILE_URL_ID']) ? $_POST['FILE_URL_ID'] : "";
        $FILE_URL_EN = isset($_POST['FILE_URL_EN']) ? $_POST['FILE_URL_EN'] : "";

        $files = $_FILES;

        if (empty($NAME_EN) || empty($NAME_ID)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "Detail is required!");
            die(json_encode($JSON));
        }
        $this->db->trans_begin();
        //UPDATE HEADER
        $dataheader['DESCRIPTION_ID'] = $DESCRIPTION_ID;
        $dataheader['DESCRIPTION_EN'] = $DESCRIPTION_EN;
        $dataheader['USER_LOG'] = $_SESSION['username'];
        $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
        $where['ID_GCG_HEADER'] = $ID_GCG_HEADER;
        $update_header = $this->db->where($where)->update('MS_GCG_HEADER', $dataheader);

        if (!$update_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        } else {
            //UPDATE DETAIL
            $pathgcg = $this->config->item('save_report_gcg');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size');
            $config['max_width'] = $this->config->item('max_width');
            $config['min_width'] = $this->config->item('min_width');
            $config['max_height'] = $this->config->item('max_height');
            $config['min_height'] = $this->config->item('min_height');
            $config['upload_path'] = $this->config->item('path_report_gcg');
            $Extension = $this->config->item('allowed_types_file');
            $Expfile = explode("|", $Extension);

            $cpt = count($NAME_ID);
            for ($i = 0; $i < $cpt; $i++) {
                if (!empty($files["file_id"]["name"][$i])) {
                    $path_parts = pathinfo($files["file_id"]["name"][$i]);
                    $forUpload['extension'] = $path_parts['extension'];
                    $config['file_name'] = 'REPORT_ID' . date('ymdHis') . $i . '.' . $forUpload['extension'];
                    $_FILES['file_id']['name'] = $config['file_name'];
                    $_FILES['file_id']['type'] = $files['file_id']['type'][$i];
                    $_FILES['file_id']['tmp_name'] = $files['file_id']['tmp_name'][$i];
                    $_FILES['file_id']['error'] = $files['file_id']['error'][$i];
                    $_FILES['file_id']['size'] = $files['file_id']['size'][$i];
                    $filename = $_FILES['file_id']['name'];
                    $forUpload['file_name_id'] = $config['file_name'];

                    if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                        $this->upload->initialize($config);
                        if (!is_dir($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }

                        if (!$this->upload->do_upload('file_id')) {
                            $error = array('error' => $this->upload->display_errors());
                            $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                            $this->db->trans_rollback();
                            die(json_encode($JSON));

                        } else {
                            $LINK_ID[$i] = $pathgcg . $forUpload['file_name_id'];
                        }
                    } else {
                        $JSON = array("ErrorCode" => "EC:002AB", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                } else {
                    $LINK_ID[$i] = $FILE_URL_ID[$i];
                }
                if (!empty($files["file_en"]["name"][$i])) {
                    $path_parts = pathinfo($files["file_en"]["name"][$i]);
                    $forUpload['extension'] = $path_parts['extension'];
                    $config['file_name'] = 'REPORT_EN' . date('ymdHis') . $i . '.' . $forUpload['extension'];
                    $_FILES['file_en']['name'] = $config['file_name'];
                    $_FILES['file_en']['type'] = $files['file_en']['type'][$i];
                    $_FILES['file_en']['tmp_name'] = $files['file_en']['tmp_name'][$i];
                    $_FILES['file_en']['error'] = $files['file_en']['error'][$i];
                    $_FILES['file_en']['size'] = $files['file_en']['size'][$i];
                    $filename = $_FILES['file_en']['name'];
                    $forUpload['file_name_en'] = $config['file_name'];

                    if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                        $this->upload->initialize($config);
                        if (!is_dir($config['upload_path'])) {
                            mkdir($config['upload_path'], 0777, true);
                        }

                        if (!$this->upload->do_upload('file_en')) {
                            $error = array('error' => $this->upload->display_errors());
                            $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                            $this->db->trans_rollback();
                            die(json_encode($JSON));

                        } else {
                            $LINK_EN[$i] = $pathgcg . $forUpload['file_name_en'];
                        }
                    } else {
                        $JSON = array("ErrorCode" => "EC:002AB", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                } else {
                    $LINK_EN[$i] = $FILE_URL_EN[$i];
                }

                if ($ID_GCG_DETAIL[$i] != 'NEW') {
                    $where['ID_GCG_DETAIL'] = $ID_GCG_DETAIL[$i];
                    $datadetail['ID_GCG_HEADER'] = $ID_GCG_HEADER;
                    $datadetail['NAME_ID'] = $NAME_ID[$i];
                    $datadetail['NAME_EN'] = $NAME_EN[$i];
                    $datadetail['LINK_ID'] = $LINK_ID[$i];
                    $datadetail['LINK_EN'] = $LINK_EN[$i];
                    $datadetail['DATE_LOG'] = date('Y-m-d H:i:s');
                    $datadetail['USER_LOG'] = $_SESSION['username'];
                    $update_detail = $this->db->where($where)->update('MS_GCG_DETAIL', $datadetail);
                    if (!$update_detail) {
                        $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }
                } else {
                    $datadetail['ID_GCG_HEADER'] = $ID_GCG_HEADER;
                    $datadetail['NAME_ID'] = $NAME_ID[$i];
                    $datadetail['NAME_EN'] = $NAME_EN[$i];
                    $datadetail['LINK_ID'] = $LINK_ID[$i];
                    $datadetail['LINK_EN'] = $LINK_EN[$i];
                    $datadetail['CREATED_BY'] = $_SESSION['username'];
                    $insert_detail = $this->db->insert('MS_GCG_DETAIL', $datadetail);
                    if (!$insert_detail) {
                        $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                        $this->db->trans_rollback();
                        die(json_encode($JSON));
                    }

                }

            }

            $del = count($LIST_DELETE);
            for ($j = 0; $j < $del; $j++) {
                $run_delete = $this->db->delete('MS_GCG_DETAIL', array('ID_GCG_DETAIL' => $LIST_DELETE[$j]));
                if (!$run_delete) {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    return $JSON;
                }
            }

            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_GCG_HEADER = isset($Data['ID_GCG_HEADER']) ? $Data['ID_GCG_HEADER'] : "";
        $param['ID_GCG_HEADER'] = $ID_GCG_HEADER;
        $Delete = $this->gcg_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_GCG_HEADER']) ? $Data['ID_GCG_HEADER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_GCG_HEADER' => $id);
        $Active = $this->gcg_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_GCG_HEADER']) ? $Data['ID_GCG_HEADER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_GCG_HEADER' => $id);
        $Active = $this->gcg_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_GCG_HEADER']) ? $Data['ID_GCG_HEADER'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_GCG_HEADER' => $id);
        $Active = $this->gcg_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}