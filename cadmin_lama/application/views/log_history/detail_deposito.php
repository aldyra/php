<!-- Theme JS files -->
<script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>

<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script>
<!-- /theme JS files -->
<?php
$is_update = $authorize['is_update'];
$back = isset($data['back']) ? $data['back'] : '';

$deposit_ir = isset($data['deposit_ir']) ? $data['deposit_ir'] : array();
$DETAIL = isset($data['detail']) ? $data['detail'] : array();
$ID_DEPOSIT_IR_HEADER = isset($deposit_ir->ID_DEPOSIT_IR_HEADER) ? $deposit_ir->ID_DEPOSIT_IR_HEADER : "";
$CATEGORY_DEPOSIT = isset($deposit_ir->CATEGORY_DEPOSIT) ? $deposit_ir->CATEGORY_DEPOSIT : "";
?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Log History</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Log Deposit</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Deposit Interest
                    Rate</span> - Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
            <div class="col-md-12">
                <input type="hidden" name="id_deposit_ir_header" id="id_deposit_ir_header"
                    value="<?php echo $ID_DEPOSIT_IR_HEADER ?>">
                <div class="form-group">
                    <label>Category</label><br>
                    <?php
$DEPOSIT_ID = $this->config->item('DEPOSIT_ID');
$DEPOSIT_EN = $this->config->item('DEPOSIT_EN');
$ID_CATEG_DEPOSIT = $this->config->item('ID_CATEG_DEPOSIT');
$Exp_DEPOSIT_ID = explode(":", $DEPOSIT_ID);
$Exp_DEPOSIT_EN = explode(":", $DEPOSIT_EN);
$Exp_ID_CATEG_DEPOSIT = explode(":", $ID_CATEG_DEPOSIT);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_DEPOSIT); $i++) {
    $ID = $Exp_ID_CATEG_DEPOSIT[$i];
    $DESC_EN = $Exp_DEPOSIT_EN[$i];
    $DESC_ID = $Exp_DEPOSIT_ID[$i];
    if ($CATEGORY_DEPOSIT == $ID) {
        echo '<h5>' . $DESC_EN . '-' . $DESC_ID . '</h5>';
    }
}
?>
                </div>
            </div>
        </div>
        <div class="card-body" id="detail">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="card-title">Deposit Interest Rate Detail</h6>

                </div>
            </div>

            <hr>
            <?php if (!empty($DETAIL)) {?>
            <div id="form_detail">
                <?php if ($CATEGORY_DEPOSIT == '1' || $CATEGORY_DEPOSIT == '3') {?>
                <div id="deposit_umum">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">Balance ID</th>
                                    <th rowspan="2">Balance EN</th>
                                    <th colspan="5">Rates</th>
                                </tr>
                                <tr>
                                    <th>1 Month</th>
                                    <th>3 Month</th>
                                    <th>4 Month</th>
                                    <th>6 Month</th>
                                    <th>12 Month</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $no = $no + 1; ?></td>
                                    <td><?php echo $row->BALANCE_ID ?></td>
                                    <td><?php echo $row->BALANCE_EN ?></td>
                                    <td><?php echo $row->MONTH_1 ?></td>
                                    <td><?php echo $row->MONTH_3 ?></td>
                                    <td><?php echo $row->MONTH_4 ?></td>
                                    <td><?php echo $row->MONTH_6 ?></td>
                                    <td><?php echo $row->MONTH_12 ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <?php if ($CATEGORY_DEPOSIT == '2') {?>
                <div id="deposit_plus">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Balance ID</th>
                                    <th>Balance EN</th>
                                    <th width="10px"></th>
                                    <th>Min Tabungan Kesra</th>
                                    <th>Interest Rates</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $no = $no + 1; ?></td>
                                    <td><?php echo $row->BALANCE_ID ?></td>
                                    <td><?php echo $row->BALANCE_EN ?></td>
                                    <td><?php echo $row->OPERATION ?></td>
                                    <td><?php echo $row->MIN_TABUNGAN_KESRA ?></td>
                                    <td><?php echo $row->INTEREST_RATE ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <?php if ($CATEGORY_DEPOSIT == '4') {?>
                <div id="deposit_perpanjangan">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th>Interest Rates Before</th>
                                    <th>Interest Rates Next</th>
                                </tr>
                                <tr>
                                    <th>1,3,4,6, and 12 month</th>
                                    <th>1,3,4,6, and 12 month</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $no = $no + 1; ?></td>
                                    <td><?php echo $row->INTEREST_RATE_BEFORE ?></td>
                                    <td><?php echo $row->INTEREST_RATE_NEXT ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <?php if ($CATEGORY_DEPOSIT == '5') {?>
                <div id="deposit_usd">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Balance ID</th>
                                    <th>Balance EN</th>
                                    <th>Rates</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $no = $no + 1; ?></td>
                                    <td><?php echo $row->BALANCE_ID ?></td>
                                    <td><?php echo $row->BALANCE_EN ?></td>
                                    <td><?php echo $row->INTEREST_RATE ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>

            </div>
            <?php }?>
        </div>
        <div class="card-footer bg-transparent d-flex justify-content-between mt-2 border-top-0 pt-0">
            <div class="text-right">
                <a href="<?php echo $back ?>"><button type="button" class="btn btn-secondary waves-effect waves-light">
                        Back</button></a>
            </div>
        </div>

    </div>

    <?php endif;?>

</div>