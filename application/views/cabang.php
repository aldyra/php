<?php 
$LanguageActive = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$ID_PROVINCE = isset($data['param']['ID_PROVINCE']) ? $data['param']['ID_PROVINCE']:"";
$ID_CITY = isset($data['param']['ID_CITY']) ? $data['param']['ID_CITY']:"";
$NAME_PROVINCE = isset($data['param']['PROVINCE']) ? $data['param']['PROVINCE']:"";
$NAME_CITYKAB = isset($data['param']['CITYKAB']) ? $data['param']['CITYKAB']:"";
$MENU_PARENT = isset($data['content']['MENU_PARENT']) ? $data['content']['MENU_PARENT']:"";
$MENU_CHILD = isset($data['content']['MENU_CHILD']) ? $data['content']['MENU_CHILD']:"";
if(empty($MENU_CHILD)){
	redirect(base_url());
}
?>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $MENU_PARENT; ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color:#707070"><?php echo $this->lang->line('home') ?></a> / 
		<?php 
		if(!empty($MENU_PARENT)){
		?>
		<label style="color:#707070"><?php echo $MENU_PARENT?></label> /
		<?php } ?> 
		<label style="color:#707070"><?php echo $MENU_CHILD; ?></label>
	</div>
</div>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" style="font-size: 14px;">
			<?php 
			$this->load->view('leftmodule', $data);
			?>
		</div>
		<div class="col-xl-8 col-lg-8 col-md-9 col-sm-12 col-12" style="padding-top: 1rem;">
			<span style="font-size: 18px;"><strong><?php echo $MENU_CHILD; ?></strong></span><br><br>
			<form id="formDataSearchBranch" name="formDataSearchBranch" action="javascript:void(0)" method="post" data-url="<?php echo base_url().$LanguageActive.'/'.$this->lang->line('cabang-0').'/view' ?>">
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-top: 10px !important;padding: 5px;">
						<select name="province" id="province_branch" class="form-control font-13">
		                    <option value=""><?php echo $this->lang->line('cabang-2') ?></option>
		                    <?php 
		                    foreach ($province as $row) {
		                        $PROVINCE_ID = isset($row->PROVINCE_ID) ? $row->PROVINCE_ID:"";
		                        $PROVINCE_NAME = isset($row->PROVINCE_NAME) ? $row->PROVINCE_NAME:"";

		                        if($ID_PROVINCE == $PROVINCE_ID){
		                        	$Selected = "selected";
		                        }else{
		                        	$Selected = "";
		                        }
		                    ?>
		                        <option value="<?php echo $PROVINCE_ID.'-'.$PROVINCE_NAME ?>" <?php echo $Selected ?>><?php echo $PROVINCE_NAME ?></option>
		                    <?php
		                    }
		                    ?>
		                </select>
					</div>
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-top: 10px !important;padding: 5px;">
						<select name="citykab" id="citykab_branch" class="form-control font-13">
		                    <option value="0"><?php echo $this->lang->line('cabang-3') ?></option>
		                </select>
					</div>
				</div>
			</form>
			<div class="row">
	            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 d-padding" style="padding-top: 10px !important;">
	                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d126920.3746158166!2d106.8072958!3d-6.2291876!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f682cab14665%3A0x69fd7708f2aac3f8!2sBank%20Bumi%20Arta!5e0!3m2!1sid!2sid!4v1617062173346!5m2!1sid!2sid" style="border:0;width: 100%; height: 350px;" allowfullscreen="" loading="lazy"></iframe>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding t-mob-center" style="padding-top: 10px !important;">
	    			<div class="row">
	    				<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 d-padding d-padtop-10">
	    					<?php
			            	foreach($branch_head as $row){
			            		$BRANCH_NAME = isset($row['BRANCH_NAME']) ? $row['BRANCH_NAME']:"";
			            		$CATEGORY = isset($row['CATEGORY']) ? $row['CATEGORY']:"";
			            		$ADDRESS = isset($row['ADDRESS']) ? $row['ADDRESS']:"";
			            		$LONGTITUDE = isset($row['LONGTITUDE']) ? $row['LONGTITUDE']:"";
			            		$LATITUDE = isset($row['LATITUDE']) ? $row['LATITUDE']:"";
			            		$POSTAL_CODE = isset($row['POSTAL_CODE']) ? $row['POSTAL_CODE']:"";
			            		$PHONE_NUMBER = isset($row['PHONE_NUMBER']) ? $row['PHONE_NUMBER']:"";
			            		$FAX_NUMBER = isset($row['FAX_NUMBER']) ? $row['FAX_NUMBER']:"";
			            		$EMAIL = isset($row['EMAIL']) ? $row['EMAIL']:"";
			            		$WEBSITE = isset($row['WEBSITE']) ? $row['WEBSITE']:"";
			            		$PROVINCE_NAME = isset($row['PROVINCE_NAME']) ? $row['PROVINCE_NAME']:"";
			            		$CITY_KAB_NAME = isset($row['CITY_KAB_NAME']) ? $row['CITY_KAB_NAME']:"";
			            		?>
		    					<p><b><?php echo $BRANCH_NAME ?></b></p>
		            			<p style="padding-bottom: 15px;">
		            				<?php 
		            				echo $ADDRESS."<br>";
		            				echo $CITY_KAB_NAME." ".$POSTAL_CODE."<br>";
		            				echo "Phone : ".$PHONE_NUMBER."<br>";
		            				echo "Fax : ".$FAX_NUMBER."<br>";
		            				echo $WEBSITE;
		            				?>
		            			</p>
		            		<?php 
		            		} 
		            		?>
	    				</div>
	    				<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 d-padding d-padtop-10">
	    					<p>
	    						<b><?php echo $MENU_CHILD; ?></b>
	    					</p>
	    					<div class="table-responsive">
	    						<table class="table table-branch table-bordered">
	    							<thead>
	    								<tr>
	    									<td>Outlet</td>
	    									<td>Jumlah</td>
	    								</tr>
	    							</thead>
	    							<tbody>
	    								<tr>
	    									<td><?php echo $this->lang->line('cabang-4') ?></td>
	    									<td><?php echo $category['CATEGORY2'] ?></td>
	    								</tr>
	    								<tr>
	    									<td><?php echo $this->lang->line('cabang-5') ?></td>
	    									<td><?php echo $category['CATEGORY3'] ?></td>
	    								</tr>
	    								<tr>
	    									<td><?php echo $this->lang->line('cabang-6') ?></td>
	    									<td><?php echo $category['CATEGORY4'] ?></td>
	    								</tr>
	    								<tr>
	    									<td><?php echo $this->lang->line('cabang-7') ?></td>
	    									<td><?php echo $category['CATEGORY5'] ?></td>
	    								</tr>
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    			</div>
	            </div>
	        </div>
	        <?php 
	        if(!empty($data['param'])){
	        ?>
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding d-padtop-10 t-mob-center">
	            	<p><b>
	            		<?php echo $this->lang->line('cabang-8') ?> <?php echo ucwords(str_replace("-", " ", $NAME_CITYKAB)); ?>
	            	</b></p>
	            	<div class="row">
		            	<?php
		            	foreach($branch as $row){
		            		$BRANCH_NAME = isset($row['BRANCH_NAME']) ? $row['BRANCH_NAME']:"";
		            		$CATEGORY = isset($row['CATEGORY']) ? $row['CATEGORY']:"";
		            		$ADDRESS = isset($row['ADDRESS']) ? $row['ADDRESS']:"";
		            		$LONGTITUDE = isset($row['LONGTITUDE']) ? $row['LONGTITUDE']:"";
		            		$LATITUDE = isset($row['LATITUDE']) ? $row['LATITUDE']:"";
		            		$POSTAL_CODE = isset($row['POSTAL_CODE']) ? $row['POSTAL_CODE']:"";
		            		$PHONE_NUMBER = isset($row['PHONE_NUMBER']) ? $row['PHONE_NUMBER']:"";
		            		$FAX_NUMBER = isset($row['FAX_NUMBER']) ? $row['FAX_NUMBER']:"";
		            		$EMAIL = isset($row['EMAIL']) ? $row['EMAIL']:"";
		            		$WEBSITE = isset($row['WEBSITE']) ? $row['WEBSITE']:"";
		            		$PROVINCE_NAME = isset($row['PROVINCE_NAME']) ? $row['PROVINCE_NAME']:"";
		            		$CITY_KAB_NAME = isset($row['CITY_KAB_NAME']) ? $row['CITY_KAB_NAME']:"";
		            		if($CATEGORY != "1"){
		            	?>
		            		
		            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-padding d-padtop-10 t-mob-center">
		            				<p style="padding-top: 20px;"><strong><?php echo $BRANCH_NAME ?></strong></p>
			            			<p>
			            				<?php 
			            				echo $ADDRESS."<br>";
			            				echo $CITY_KAB_NAME." ".$POSTAL_CODE."<br>";
			            				echo "Phone : ".$PHONE_NUMBER."<br>";
			            				echo "Fax : ".$FAX_NUMBER."<br>";
			            				echo $WEBSITE;
			            				?>
			            			</p>
		            			</div>
		            	<?php
		            		}
		            	}
		            	?>
	            	</div>
	            </div>
	        </div>
	        <?php 
	    	} 
	    	?>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-12">
			<div style="max-width: 100%" class="mb-3 mt-3">
				<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
				<div style="clear:both"></div>
			</div>	
		</div>
	</div>
</div>

<?php 
if(!empty($ID_PROVINCE) && !empty($ID_CITY)){
?>
	<script type="text/javascript">
		$(function(){
			var LanguageActive = "<?php echo $LanguageActive ?>";
        	var base_url = '<?php echo base_url() ?>';

        	setTimeout(function(){
        		$('#province_branch').trigger('change');
        	}, 1000);

        	setTimeout(function(){
        		$('#province_branch').change(function(){
		            var id = $(this).val();
		            var id_city = '<?php echo $ID_CITY ?>';
		            $.ajax({
		                url : base_url+LanguageActive+'/main/citykab',
		                data : {'id' : id, 'id_city' : id_city},
		                type : 'post',
		                cache : false,
		                success : function(res){
		                    $('#citykab_branch').html(res);
		                }
		            });
		        });
        	}, 500);
		});
	</script>
<?php
}
?>


<style type="text/css">
	table.table-branch thead tr td{
		background-color: #28448f;
		color: white;
		padding: 5px;
		text-align: center;
		font-size: 14px;
	}

	table.table-branch tbody tr td{
		padding: 5px;
		text-align: center;
		font-size: 14px;
	}

	@media only screen and (min-width: 992px) {
		.t-mob-center{
			text-align: left;
		}
	}
	@media only screen and (max-width: 992px) {
		.t-mob-center{
			text-align: center;
		}
	}
</style>

<script type="text/javascript">
	$(function(){
		var LanguageActive = "<?php echo $LanguageActive ?>";
        var base_url = '<?php echo base_url() ?>';

		$('#province_branch').change(function(){
            var id = $(this).val();
            var id_city = '<?php echo $ID_CITY ?>';
            $.ajax({
                url : base_url+LanguageActive+'/main/citykab',
                data : {'id' : id, 'id_city' : id_city},
                type : 'post',
                cache : false,
                success : function(res){
                    $('#citykab_branch').html(res);
                }
            });
        });

		$('#citykab_branch').change(function(){
			var idprovince = document.getElementById('province_branch').value;
			var idcity = document.getElementById('citykab_branch').value;
			var data = {
				idprovince : idprovince,
				idcity : idcity
			}

			if(idprovince == ""){
				Swal.fire({
                    type: 'error',
                    html: 'Province is required!',
                    confirmButton: true,
                    confirmButtonColor : '#1FB3E5',
                    confirmButtonText : 'Close'
                });
			}else if(idcity == ""){
				Swal.fire({
                    type: 'error',
                    html: 'City is required!',
                    confirmButton: true,
                    confirmButtonColor : '#1FB3E5',
                    confirmButtonText : 'Close'
                });
			}

			searchbrand();
		});
	});

	function searchbrand(){
        var form = $('#formDataSearchBranch');
        var data = form.serialize();
        var url = form.data('url');
        $.ajax({
            url : url,
            data : data,
            cache : false,
            dataType : 'json',
            type : 'post',
            beforeSend : function(){
                $('.preloader').fadeIn();
            },
            success : function(res){
                $('.preloader').fadeOut();
                var ErrorMessage = res.ErrorMsg;
                var ErrorCode = res.ErrorCode;
                var nextpage = res.Url;
                if(ErrorCode != "EC:0000"){
                    Swal.fire({
                        type: 'error',
                        html: ErrorMessage,
                        showConfirmButton: false,
  						timer: 1500
                    });
                    setTimeout(function(){
                        window.location.href=nextpage;
                    },1500);
                }else{
                    setTimeout(function(){
                        window.location.href=nextpage;
                    },500);
                }     
            }
        });
    }
</script>