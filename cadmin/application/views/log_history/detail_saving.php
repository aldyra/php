<?php
$is_update = $authorize['is_update'];
$back = isset($data['back']) ? $data['back'] : '';
$saving_ir = isset($data['saving_ir']) ? $data['saving_ir'] : array();
$DETAIL = isset($data['detail']) ? $data['detail'] : array();

$ID_SAVING_IR_HEADER = isset($saving_ir->ID_SAVING_IR_HEADER) ? $saving_ir->ID_SAVING_IR_HEADER : "";
$CATEGORY_SAVING = isset($saving_ir->CATEGORY_SAVING) ? $saving_ir->CATEGORY_SAVING : "";
$EFFECTIVE_DATE = isset($saving_ir->EFFECTIVE_DATE) ? date('d M Y', strtotime($saving_ir->EFFECTIVE_DATE)) : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dropify-wrapper {
    height: 100%;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Log History</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Log History Saving</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Saving Interest Rate</span> -
                Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveSavingIR">
            <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
                <input type="hidden" name="id_saving_ir_header" id="id_saving_ir_header"
                    value="<?php echo $ID_SAVING_IR_HEADER ?>">
                <input type="hidden" name="listDelete" id="listDelete" value="">
                <?php $SAVING_ID = $this->config->item('SAVING_ID');
$SAVING_EN = $this->config->item('SAVING_EN');
$ID_CATEG_SAVING = $this->config->item('ID_CATEG_SAVING');
$Exp_SAVING_ID = explode(":", $SAVING_ID);
$Exp_SAVING_EN = explode(":", $SAVING_EN);
$Exp_ID_CATEG_SAVING = explode(":", $ID_CATEG_SAVING);
$DESC_EN = '';
$DESC_ID = '';

for ($i = 0; $i < sizeof($Exp_ID_CATEG_SAVING); $i++) {
    $ID = $Exp_ID_CATEG_SAVING[$i];

    if ($CATEGORY_SAVING == $ID) {
        $DESC_EN = $Exp_SAVING_EN[$i];
        $DESC_ID = $Exp_SAVING_ID[$i];
    }
}?>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Description Indonesian</label>
                        <h5><?php echo $DESC_ID ?></h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Description English</label>
                        <h5><?php echo $DESC_EN ?></h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Effective Date</label>
                        <h5><?php echo $EFFECTIVE_DATE ?></h5>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <?php if (!empty($DETAIL)) {?>
                <div id="form_detail">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="workhistory">
                            <thead>
                                <tr>
                                    <th>Balance Indonesia</th>
                                    <th>Balance English</th>
                                    <th>Rate</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($DETAIL as $row) {?>

                                <tr>
                                    <td><?php echo $row->BALANCE_ID ?></td>
                                    <td><?php echo $row->BALANCE_EN ?></td>
                                    <td><?php echo $row->RATE ?></td>
                                </tr>
                                <?php }?>


                            </tbody>
                        </table>
                    </div>

                </div>
                <?php }?>
            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Back</button></a>
                </div>
            </div>
        </form>
    </div>


    <?php endif;?>


</div>