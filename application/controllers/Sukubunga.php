<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sukubunga extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{
		$getMenuLeft = $this->getMenuLeft();

		$provideDataSaving = $this->provideDataSaving();
		$provideDataGiro = $this->provideDataGiro();
		$provideDataDeposito = $this->provideDataDeposito();

		// die(json_encode($provideDataDeposito));
		
		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "suku_bunga";
		$this->data['data'] = array();

		$this->data['contentData'] = array(
			'menuLeft' => $getMenuLeft,
			'dataSaving' => $provideDataSaving['dataSaving'],
			'lastUpdateSaving' => date('d F Y', strtotime($provideDataSaving['lastUpdateSaving'])),
			'dataGiro' => $provideDataGiro['data'],
			'lastUpdateGiro' => date('d F Y', strtotime($provideDataGiro['lastUpdate'])),
			'dataDeposito' => $provideDataDeposito['data'],
			'lastUpdateDeposito' => date('d F Y', strtotime($provideDataDeposito['lastUpdate'])),
		);

		// echo "<pre>";
		// print_r($provideDataDeposito);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function sbdk()
	{	
		$msSbdk = $this->db->query("SELECT * FROM MS_SBDK WHERE STATUS = '99' ORDER BY EFFECTIVE_DATE DESC")->row_array();
		$getMenuLeft = $this->getMenuLeft();

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js";
		$this->data['Content'] = "sbdk";
		$this->data['data'] = array();
		$this->data['contentData'] = array('msSbdk' => $msSbdk, 'menuLeft' => $getMenuLeft);

		// echo "<pre>";
		// print_r($this->data);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function provideDataDeposito()
	{
		$conf['DEPOSIT_ID'] = "Deposito Umum:Deposito Plus:Deposito Premium:Deposito Perpanjangan:Deposito USD";
		$conf['DEPOSIT_EN'] = "Deposit Umum:Deposit Plus:Deposit Premium:Deposit Perpanjangan:Deposit USD";
		$conf['ID_CATEG_DEPOSIT'] = "1:2:3:4:5";

		$expSaving = explode(':', $conf[$this->cekLang('DEPOSIT')]);
		$expIdCateg = explode(':', $conf['ID_CATEG_DEPOSIT']);

		$dataSaving = array();
		foreach ($expIdCateg as $key => $value) {

			$getHeader = $this->db->query("
				SELECT * FROM MS_DEPOSIT_IR_HEADER
				WHERE STATUS = '99' AND CATEGORY_DEPOSIT = ".$value." ORDER BY EFFECTIVE_DATE DESC
			")->row_array();

			if (!empty($getHeader['ID_DEPOSIT_IR_HEADER'])) {
				// $getDetail = $this->db->query("
				// 	SELECT * FROM MS_DEPOSIT_IR_DETAIL
				// 	WHERE ID_DEPOSIT_IR_HEADER = ".$getHeader['ID_DEPOSIT_IR_HEADER']
				// )->result_array();
				$getDetail = $this->db->query("SELECT A.BALANCE_EN, A.BALANCE_ID, A.MIN_TABUNGAN_KESRA, A.OPERATION, A.INTEREST_RATE AS INTEREST, A.INTEREST_RATE_BEFORE, A.INTEREST_RATE_NEXT, B.MONTH, B.INTEREST_RATE, A.MONTH AS MONTH_DETAIL
					FROM MS_DEPOSIT_IR_DETAIL AS A LEFT JOIN MS_DEPOSIT_IR_MONTH AS B ON B.ID_DEPOSIT_IR_DETAIL = A.ID_DEPOSIT_IR_DETAIL WHERE ID_DEPOSIT_IR_HEADER = '".$getHeader['ID_DEPOSIT_IR_HEADER']."'")->result_array();

				foreach ($getDetail as $key2 => $value2) {
					$getDetail[$key2]['BALANCE'] = $value2[$this->cekLang('BALANCE')];
				}
			}else{
				$getDetail = array();
			}

			$dataSaving[$key]['ID_CATEG'] = $value;
			$dataSaving[$key]['HEADER'] = $getHeader;
			$dataSaving[$key]['DETAIL'] = $getDetail;
			$dataSaving[$key]['NAME'] = $expSaving[$key];

		}

		$lastUpdateHeader = $this->db->query("SELECT DATE_LOG FROM MS_DEPOSIT_IR_HEADER WHERE STATUS = '99' ORDER BY DATE_LOG DESC")->row_array();
		$lastUpdateDetail = $this->db->query("SELECT DATE_LOG FROM MS_DEPOSIT_IR_DETAIL ORDER BY DATE_LOG DESC")->row_array();
		$dateLogHeader = date('Y-m-d', strtotime($lastUpdateHeader['DATE_LOG']));
		$dateLogDetail = date('Y-m-d', strtotime($lastUpdateDetail['DATE_LOG']));

		// compare dates
		if ($dateLogHeader > $dateLogDetail)
		    $lastUpdate = $dateLogHeader;
		else
		    $lastUpdate = $dateLogDetail;

		return array('data' => $dataSaving, 'lastUpdate' => $lastUpdate);
	}

	public function provideDataSaving()
	{
		$conf['SAVING_ID'] = "Tabungan BBA:Tabungan Kesra:Tabungan BBA USD:Tabungan Pensiun:TabunganKu:Tabungan Berjangka Super BBA(TangKas BBA):Tabungan MultiGuna(TaMu)";
		$conf['SAVING_EN'] = "Tabungan BBA:Tabungan Kesra:Tabungan BBA USD:Tabungan Pensiun:TabunganKu:Tabungan Berjangka Super BBA(TangKas BBA):Tabungan MultiGuna(TaMu)";
		$conf['ID_CATEG_SAVING'] = "1:2:3:4:5:6:7";

		$expSaving = explode(':', $conf[$this->cekLang('SAVING')]);
		$expIdCategSaving = explode(':', $conf['ID_CATEG_SAVING']);

		$dataSaving = array();
		foreach ($expIdCategSaving as $key => $value) {

			$getHeader = $this->db->query("
				SELECT * FROM MS_SAVING_IR_HEADER
				WHERE STATUS = '99' AND CATEGORY_SAVING = ".$value." ORDER BY EFFECTIVE_DATE DESC
			")->row_array();

			if (!empty($getHeader['ID_SAVING_IR_HEADER'])) {
				$getDetail = $this->db->query("
					SELECT * FROM MS_SAVING_IR_DETAIL
					WHERE ID_SAVING_IR_HEADER = ".$getHeader['ID_SAVING_IR_HEADER']
				)->result_array();

				foreach ($getDetail as $key2 => $value2) {
					$getDetail[$key2]['BALANCE'] = $value2[$this->cekLang('BALANCE')];
				}
			}else{
				$getDetail = array();
			}

			$dataSaving[$key]['ID_CATEG_SAVING'] = $value;
			$dataSaving[$key]['HEADER'] = $getHeader;
			$dataSaving[$key]['DETAIL'] = $getDetail;
			$dataSaving[$key]['SAVING_NAME'] = $expSaving[$key];

		}

		$lastUpdateSavingHeader = $this->db->query("SELECT DATE_LOG FROM MS_SAVING_IR_HEADER WHERE STATUS = '99' ORDER BY DATE_LOG DESC")->row_array();
		$lastUpdateSavingDetail = $this->db->query("SELECT DATE_LOG FROM MS_SAVING_IR_DETAIL ORDER BY DATE_LOG DESC")->row_array();
		$dateLogSavingHeader = date('Y-m-d', strtotime($lastUpdateSavingHeader['DATE_LOG']));
		$dateLogSavingDetail = date('Y-m-d', strtotime($lastUpdateSavingDetail['DATE_LOG']));

		// compare dates
		if ($dateLogSavingHeader > $dateLogSavingDetail)
		    $lastUpdateSaving = $dateLogSavingHeader;
		else
		    $lastUpdateSaving = $dateLogSavingHeader;

		return array('dataSaving' => $dataSaving, 'lastUpdateSaving' => $lastUpdateSaving);
	}

	public function provideDataGiro()
	{
		$conf['GIRO_ID'] = "Giro Rupiah:Giro USD";
		$conf['GIRO_EN'] = "Giro Rupiah:Giro USD";
		$conf['ID_CATEG_GIRO'] = "1:2";

		$expSaving = explode(':', $conf[$this->cekLang('GIRO')]);
		$expIdCateg = explode(':', $conf['ID_CATEG_GIRO']);

		$dataSaving = array();
		foreach ($expIdCateg as $key => $value) {

			$getHeader = $this->db->query("
				SELECT * FROM MS_GIRO_IR_HEADER
				WHERE STATUS = '99' AND CATEGORY_GIRO = ".$value." ORDER BY EFFECTIVE_DATE DESC
			")->row_array();

			if (!empty($getHeader['ID_GIRO_IR_HEADER'])) {
				$getDetail = $this->db->query("
					SELECT * FROM MS_GIRO_IR_DETAIL
					WHERE ID_GIRO_IR_HEADER = ".$getHeader['ID_GIRO_IR_HEADER']
				)->result_array();

				foreach ($getDetail as $key2 => $value2) {
					$getDetail[$key2]['BALANCE'] = $value2[$this->cekLang('BALANCE')];
				}
			}else{
				$getDetail = array();
			}

			$dataSaving[$key]['ID_CATEG'] = $value;
			$dataSaving[$key]['HEADER'] = $getHeader;
			$dataSaving[$key]['DETAIL'] = $getDetail;
			$dataSaving[$key]['NAME'] = $expSaving[$key];

		}

		$lastUpdateHeader = $this->db->query("SELECT DATE_LOG FROM MS_GIRO_IR_HEADER WHERE STATUS = '99' ORDER BY DATE_LOG DESC")->row_array();
		$lastUpdateDetail = $this->db->query("SELECT DATE_LOG FROM MS_GIRO_IR_DETAIL ORDER BY DATE_LOG DESC")->row_array();
		$dateLogHeader = date('Y-m-d', strtotime($lastUpdateHeader['DATE_LOG']));
		$dateLogDetail = date('Y-m-d', strtotime($lastUpdateDetail['DATE_LOG']));

		// compare dates
		if ($dateLogHeader > $dateLogDetail)
		    $lastUpdate = $dateLogHeader;
		else
		    $lastUpdate = $dateLogDetail;

		return array('data' => $dataSaving, 'lastUpdate' => $lastUpdate);
	}

	public function getMenuLeft($value='')
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$ID_MENU_PARENT = 94;
		$getMenuLeft = array();
		$getMenu1 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->result_array();

		foreach ($getMenu1 as $key => $value) {
			$getMenuLeft[$key]['menu_1']['NAME'] = $value[$this->cekLang('NAME')];
			$getMenuLeft[$key]['menu_1']['LINK'] = $value[$this->cekLang('LINK')];

			// cek aktif ga menu 1 single
			$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = ( $value[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

			$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value['ID'])->result_array();

			if (count($getMenu2) > 0) {
				// deklarasi aktif ga menu 1 multiple
				$stakMenu2 = array();
				$linkMenu1 = $value[$this->cekLang('LINK')];

				foreach ($getMenu2 as $key2 => $value2) {
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['NAME'] = $value2[$this->cekLang('NAME')];
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['LINK'] = $value2[$this->cekLang('LINK')];

					// cek aktif ga menu single
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = ( $value2[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

					$getMenu3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value2['ID'])->result_array();

					if (count($getMenu3) > 0) {
						// deklarasi aktif ga menu 2 multiple
						$stakMenu3 = array();
						$linkMenu2 = $value2[$this->cekLang('LINK')];

						foreach ($getMenu3 as $key3 => $value3) {
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['NAME'] =
							 	$value3[$this->cekLang('NAME')];
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['LINK'] =
							 	$value3[$this->cekLang('LINK')];

							 $getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['ACTIVE_URL'] = ( $value3[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

							$stakMenu3[] = $value3[$this->cekLang('LINK')];
						}

						// cek aktif ga menu 2 multiple
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = (in_array($linkingg, $stakMenu3)) ? TRUE : FALSE;

					}else{
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'] = array();
					}

					$stakMenu2[] = $value2[$this->cekLang('LINK')];
				}

				// cek aktif ga menu 1 multiple
				$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = (in_array($linkingg, $stakMenu2)) ? TRUE : FALSE;

			}else{
				$getMenuLeft[$key]['menu_1']['menu_2'] = array();
			}
		}

		return $getMenuLeft;
	}

	function cekLang($name)
	{
		if ($_SESSION['lang'] == 'id') {
			$nameLang = $name.'_ID';
		}else{
			$nameLang = $name.'_EN';
		}

		return $nameLang;
	}

}
