<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Title</label><span style="color: orange">*</span><br>
            <input type="text" name="title" class="form-control" id="title" placeholder="">

        </div>
        <div class="form-group">
            <label>Upload Document</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" required="" />
            <a href="#" onclick="openTnC('<?php echo base_url('document/tncupload') ?>')">Terms and Conditions
                Upload</a>

        </div>
    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    changeformfile();

    // document.querySelector('.custom-file-input').addEventListener('change', function(e) {
    //     var fileName = document.getElementById("file").files[0].name;
    //     var nextSibling = e.target.nextElementSibling
    //     nextSibling.innerText = fileName;
    // });
});
</script>