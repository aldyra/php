<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jobvacancy_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function jobvacancy()
    {
    	$DATA = array();
    	$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
    	$where['STATUS'] = "99";
    	$where['LANGUAGE'] = $SiteLang;
    	// $Query = $this->db->where($where)->get("MS_GROUP_JOBVACANCY");
        $Query = $this->db->query("SELECT * FROM MS_GROUP_JOBVACANCY WHERE STATUS = '99' AnD LANGUAGE='".$SiteLang."' ORDER BY NAME ASC");
    	$Row = $Query->result_array();
    	foreach($Row as $row){
    		$ROW = array();
    		$JSON['ID'] = isset($row['ID']) ? $row['ID']:"";
    		$JSON['NAME'] = isset($row['NAME']) ? $row['NAME']:"";
    		$where['ID_GROUP_JOBVACANCY'] = $JSON['ID'];
    		$QueryDetail = $this->db->where($where)->get("MS_JOBVACANCY");
    		$RowDetail = $QueryDetail->result_array();
    		foreach ($RowDetail as $rowdetail) {
    			$DETAIL['CODE'] = isset($rowdetail['CODE']) ? $rowdetail['CODE']:"";
    			$DETAIL['POSITION'] = isset($rowdetail['POSITION']) ? $rowdetail['POSITION']:"";
    			$DETAIL['JOB_DESCRIPTION'] = isset($rowdetail['JOB_DESCRIPTION']) ? $rowdetail['JOB_DESCRIPTION']:"";
    			$DETAIL['REQUIREMENT'] = isset($rowdetail['REQUIREMENT']) ? $rowdetail['REQUIREMENT']:"";
    			$DETAIL['DETAIL'] = isset($rowdetail['DETAIL']) ? $rowdetail['DETAIL']:"";
    			$DETAIL['START_DATE'] = isset($rowdetail['START_DATE']) ? $rowdetail['START_DATE']:"";
                $DETAIL['END_DATE'] = isset($rowdetail['END_DATE']) ? $rowdetail['END_DATE']:"";
    			$DETAIL['STATUS_JOB'] = isset($rowdetail['STATUS_JOB']) ? $rowdetail['STATUS_JOB']:"";
    			$ROW[] = $DETAIL;
    		}
    		$JSON['DETAIL'] = $ROW;
    		$DATA[] = $JSON;
    	}
    	return $DATA;
    }

    public function gettotal($param)
    {   
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $ID = isset($param['ID']) ? $param['ID']:"";

        $QueryDetail = "SELECT * FROM MS_JOBVACANCY 
        WHERE 
        ID_GROUP_JOBVACANCY = '".$ID."'
        AND LANGUAGE = '".$SiteLang."'
        AND STATUS = '99'";

        $RunDetail = $this->db->query($QueryDetail);
        $RowDetail = $RunDetail->result_array();
        return $RowDetail;
    }

    public function joblistvacancy($param){
        $ROW = array();
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $ID = isset($param['ID']) ? $param['ID']:"";
        $START = isset($param['START']) ? $param['START']:"";
        $PERPAGE = isset($param['PERPAGE']) ? $param['PERPAGE']:"";

        $LIMIT = "";
        if(!empty($PERPAGE)){
            $LIMIT = " LIMIT ".$START.", ".$PERPAGE;
        }

        $QueryDetail = "SELECT * FROM MS_JOBVACANCY 
        WHERE 
        ID_GROUP_JOBVACANCY = '".$ID."'
        AND LANGUAGE = '".$SiteLang."'
        AND STATUS = '99' ORDER BY CREATED_DATE DESC  ".$LIMIT;
        $RunDetail = $this->db->query($QueryDetail);
        $RowDetail = $RunDetail->result_array();
        foreach ($RowDetail as $rowdetail) {
            $DETAIL['CODE'] = isset($rowdetail['CODE']) ? $rowdetail['CODE']:"";
            $DETAIL['POSITION'] = isset($rowdetail['POSITION']) ? $rowdetail['POSITION']:"";
            $DETAIL['JOB_DESCRIPTION'] = isset($rowdetail['JOB_DESCRIPTION']) ? $rowdetail['JOB_DESCRIPTION']:"";
            $DETAIL['REQUIREMENT'] = isset($rowdetail['REQUIREMENT']) ? $rowdetail['REQUIREMENT']:"";
            $DETAIL['DETAIL'] = isset($rowdetail['DETAIL']) ? $rowdetail['DETAIL']:"";
            $DETAIL['START_DATE'] = isset($rowdetail['START_DATE']) ? $rowdetail['START_DATE']:"";
            $DETAIL['END_DATE'] = isset($rowdetail['END_DATE']) ? $rowdetail['END_DATE']:"";
            $DETAIL['STATUS_JOB'] = isset($rowdetail['STATUS_JOB']) ? $rowdetail['STATUS_JOB']:"";
            $ROW[] = $DETAIL;
        }
        return $ROW;
    }

    public function getgroup(){
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $where['STATUS'] = "99";
        $where['LANGUAGE'] = $SiteLang;

        $Query = $this->db->where($where)->get("MS_GROUP_JOBVACANCY");
        $Row = $Query->result();
        return $Row;
    }

    public function getjobvacancy($param = ''){
        $where = "";
        if(!empty($param)){
            $where = " AND ID_JOBVACANCY = ".$param;
        }

        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $Query = "SELECT A.ID_JOBVACANCY AS ID, A.CODE, A.POSITION, A.JOB_DESCRIPTION, A.REQUIREMENT, A.DETAIL, A.START_DATE, A.END_DATE, A.LANGUAGE, A.CREATED_DATE, B.NAME AS GROUP_NAME, A.STATUS_JOB
            FROM MS_JOBVACANCY AS A
            LEFT JOIN MS_GROUP_JOBVACANCY AS B ON A.ID_GROUP_JOBVACANCY = B.ID
            WHERE A.STATUS = '99' AND A.LANGUAGE = '".$SiteLang."'".$where;
        $Run = $this->db->query($Query);
        if(!empty($param)){
            $Row = $Run->row_array();
            return $Row;
        }

        $Row = $Run->result();
        return $Row;
    }
}