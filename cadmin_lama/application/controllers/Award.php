<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Award extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('award_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "award/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['award'] = $this->award_model->view();

        $this->load->view('templates/view', $data);
    }
    public function detail_award($id)
    {
        $data = array();
        $data['content'] = "award/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['award'] = $this->award_model->get_edit($id);
        $data['data']['back'] = base_url() . 'award';
        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Image max pixel ' . $this->config->item('max_height_award') . 'x' . $this->config->item('max_width_award') . 'px <br>
                    Allowed images are ' . str_replace("|", ", ", $this->config->item('allowed_types_image')) . ' <br>
                    Max image size ' . $this->config->item('max_size_award') . 'KB<br>';
        echo json_encode($data);
    }

    public function add()
    {
        $TITLE_ID = isset($_POST['title_id']) ? $_POST['title_id'] : "";
        $TITLE_EN = isset($_POST['title_en']) ? $_POST['title_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
            die(json_encode($JSON));
        }

        $pathaward = $this->config->item('save_award');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size_award');
        $config['max_width'] = $this->config->item('max_width_award');
        $config['min_width'] = $this->config->item('min_width_award');
        $config['max_height'] = $this->config->item('max_height_award');
        $config['min_height'] = $this->config->item('min_height_award');
        $config['file_name'] = 'AWARD_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_award');
        $path_parts = pathinfo($_FILES["file"]["name"]);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_image');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $param['TITLE_ID'] = $TITLE_ID;
                $param['TITLE_EN'] = $TITLE_EN;
                $param['DESCRIPTION_ID'] = $DESCRIPTION_ID;
                $param['DESCRIPTION_EN'] = $DESCRIPTION_EN;
                $param['LINK'] = $pathaward . $config['file_name'] . "." . $forUpload['extension'];
                $param['CREATED_BY'] = $_SESSION['username'];
                $Insert = $this->award_model->add($param);
                if ($Insert['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                    die(json_encode($JSON));
                }
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->award_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        $ID_AWARD = isset($_POST['id_award']) ? $_POST['id_award'] : "";
        $TITLE_ID = isset($_POST['title_id']) ? $_POST['title_id'] : "";
        $TITLE_EN = isset($_POST['title_en']) ? $_POST['title_en'] : "";
        $DESCRIPTION_ID = isset($_POST['description_id']) ? $_POST['description_id'] : "";
        $DESCRIPTION_EN = isset($_POST['description_en']) ? $_POST['description_en'] : "";

        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }

            $pathaward = $this->config->item('save_award');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size_award');
            $config['max_width'] = $this->config->item('max_width_award');
            $config['min_width'] = $this->config->item('min_width_award');
            $config['max_height'] = $this->config->item('max_height_award');
            $config['min_height'] = $this->config->item('min_height_award');
            $config['file_name'] = 'AWARD_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_award');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_image');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $param['TITLE_ID'] = $TITLE_ID;
                    $param['TITLE_EN'] = $TITLE_EN;
                    $param['DESCRIPTION_ID'] = $DESCRIPTION_ID;
                    $param['DESCRIPTION_EN'] = $DESCRIPTION_EN;
                    $param['LINK'] = $pathaward . $config['file_name'] . "." . $forUpload['extension'];
                    $param['USER_LOG'] = $_SESSION['username'];
                    $param['DATE_LOG'] = date('Y-m-d H:i:s');
                    $Update = $this->award_model->updateaction($param, $ID_AWARD);
                    if ($Update['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
                    die(json_encode($JSON));
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $param['TITLE_ID'] = $TITLE_ID;
            $param['TITLE_EN'] = $TITLE_EN;
            $param['DESCRIPTION_ID'] = $DESCRIPTION_ID;
            $param['DESCRIPTION_EN'] = $DESCRIPTION_EN;
            $param['USER_LOG'] = $_SESSION['username'];
            $param['DATE_LOG'] = date('Y-m-d H:i:s');
            $Update = $this->award_model->updateaction($param, $ID_AWARD);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        }
    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_AWARD = isset($Data['ID_AWARD']) ? $Data['ID_AWARD'] : "";
        $param['ID_AWARD'] = $ID_AWARD;
        $Delete = $this->award_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_AWARD']) ? $Data['ID_AWARD'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_AWARD' => $id);
        $Active = $this->award_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_AWARD']) ? $Data['ID_AWARD'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_AWARD' => $id);
        $Active = $this->award_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_AWARD']) ? $Data['ID_AWARD'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_AWARD' => $id);
        $Active = $this->award_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}