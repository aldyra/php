<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('report/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Report = isset($Data['report']) ? $Data['report'] : array();
?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master Reporting</a>
                <a href="#" class="breadcrumb-item active">Report</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Report', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Report
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="100">File IND</th>
                    <th width="100">File ENG</th>
                    <th>Name Indonesia</th>
                    <th>Name English</th>
                    <th data-orderable="false" width="150">Created Date</th>
                    <th>Created By</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Report as $report) {
    $ID = isset($report['ID']) ? $report['ID'] : "";
    $REPORT_NAME_ID = isset($report['REPORT_NAME_ID']) ? $report['REPORT_NAME_ID'] : "";
    $REPORT_NAME_EN = isset($report['REPORT_NAME_EN']) ? $report['REPORT_NAME_EN'] : "";
    $LINK_ID = isset($report['LINK_ID']) ? $report['LINK_ID'] : "";
    $LINK_EN = isset($report['LINK_EN']) ? $report['LINK_EN'] : "";
    $CATEGORY = isset($report['CATEGORY']) ? $report['CATEGORY'] : "";
    $STATUS = isset($report['STATUS']) ? $report['STATUS'] : "";
    $CREATED_BY = isset($report['CREATED_BY']) ? $report['CREATED_BY'] : "";
    $CREATED_DATE = isset($report['CREATED_DATE']) ? $report['CREATED_DATE'] : "";
    $USER_LOG = isset($report['USER_LOG']) ? $report['USER_LOG'] : "";
    $DATE_LOG = isset($report['DATE_LOG']) ? $report['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    $CATEG = "";
    $REPORT_ID = $this->config->item('REPORT_ID');
    $REPORT_EN = $this->config->item('REPORT_EN');
    $ID_CATEG_REPORT = $this->config->item('ID_CATEG_REPORT');
    $Exp_REPORT_ID = explode(":", $REPORT_ID);
    $Exp_REPORT_EN = explode(":", $REPORT_EN);
    $Exp_ID_CATEG_REPORT = explode(":", $ID_CATEG_REPORT);
    for ($i = 0; $i < sizeof($Exp_ID_CATEG_REPORT); $i++) {
        $ID = $Exp_ID_CATEG_REPORT[$i];
        $DESC_EN = $Exp_REPORT_EN[$i];
        $DESC_ID = $Exp_REPORT_ID[$i];
        if ($ID == $CATEGORY) {
            $CATEG = $DESC_ID . '/' . $DESC_EN;
        }
    }
    ?>
                <tr>
                    <td>
                        <?php if (strpos($LINK_ID, '.pdf') == false) {?>
                        <a href='<?php echo $LINK_ID ?>' download><?php echo basename($LINK_ID) ?></a>
                        <?php } else {?>
                        <a href='<?php echo $LINK_ID ?>' data-popup='lightbox'
                            data-fancybox><?php echo basename($LINK_ID) ?>
                        </a>
                        <?php }?>
                    </td>
                    <td>
                        <?php if (strpos($LINK_EN, '.pdf') == false) {?>
                        <a href='<?php echo $LINK_EN ?>' download><?php echo basename($LINK_EN) ?></a>
                        <?php } else {?>
                        <a href='<?php echo $LINK_EN ?>' data-popup='lightbox'
                            data-fancybox><?php echo basename($LINK_EN) ?>
                        </a>
                        <?php }?>

                    </td>
                    <td>
                        <?php echo $REPORT_NAME_ID ?>
                    </td>
                    <td>
                        <?php echo $REPORT_NAME_EN ?>
                    </td>

                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $report;
        $paramaction['Modal'] = "";
        $this->load->view('is_action', $paramaction);
        ?>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(7).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(7)
            .search(dis.value)
            .draw();
    } else {
        t.column(7)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}
</script>