<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/lib//dropify/dropify.min.css">
<link href="<?php echo base_url(); ?>assets/lib/fancybox/jquery.fancybox.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/lib/fancybox/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url() ?>assets/lib/dropify/dropify.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('prospektus/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$Kesra_Winner = isset($Data['prospektus']) ? $Data['prospektus'] : array();
?>
<!-- page headder -->

<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Master Reporting</a>
                <a href="#" class="breadcrumb-item active">Prospektus</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Prospektus', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Prospektus
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th width="200">Document ID</th>
                    <th width="200">Document EN</th>
                    <th>Created By</th>
                    <th>Created Date</th>
                    <th>Status</th>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
    ?>
                    <th>Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php
foreach ($Kesra_Winner as $prospektus) {
    $ID = isset($prospektus['ID']) ? $prospektus['ID'] : "";
    $LINK_ID = isset($prospektus['LINK_ID']) ? $prospektus['LINK_ID'] : "";
    $LINK_EN = isset($prospektus['LINK_EN']) ? $prospektus['LINK_EN'] : "";
    $STATUS = isset($prospektus['STATUS']) ? $prospektus['STATUS'] : "";
    $CREATED_BY = isset($prospektus['CREATED_BY']) ? $prospektus['CREATED_BY'] : "";
    $CREATED_DATE = isset($prospektus['CREATED_DATE']) ? convertDate($prospektus['CREATED_DATE'], 'timestamp') : "";
    $USER_LOG = isset($prospektus['USER_LOG']) ? $prospektus['USER_LOG'] : "";
    $DATE_LOG = isset($prospektus['DATE_LOG']) ? $prospektus['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }

    ?>
                <tr>
                    <td>
                        <a href='<?php echo $LINK_ID ?>' data-popup='lightbox' data-fancybox><i
                                class="fas fa-file"></i></a>
                    </td>
                    <td>
                        <a href='<?php echo $LINK_EN ?>' data-popup='lightbox' data-fancybox><i
                                class="fas fa-file"></i></a>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo $CREATED_DATE ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <?php
if (!empty($is_update) || !empty($is_delete)) {
        ?>
                    <td style="text-align: center;">
                        <?php
$paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $prospektus;
        $paramaction['Modal'] = "";
        $this->load->view('is_action', $paramaction);
        ?>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
function init() {
    // Image lightbox
    $('[data-popup="lightbox"]').fancybox({
        padding: 3
    });
}
var t = $("#tableData").DataTable({
    initComplete: function(settings, json) {
        init()
    },
    serverSide: false,
    autoWidth: false,
    keys: !0,
    order: [],
    autofill: true,
    select: true,
    responsive: true,
    buttons: true,
    dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',
});

$('#tableData thead th').each(function() {
    var title = $(this).text();
    if (title == 'Action' || title == 'Image') {} else {
        $(this).append(
            '<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
        );
    }
});

t.columns().every(function() {
    var that = this;
    $('input', this.header()).on('keyup', function(e) {
        if (event.keyCode === 8) {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        } else {
            if (this.value.length >= 3) {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            }
        }
    });
});

$('#tableData thead input').on('click', function(e) {
    e.stopPropagation();
});
</script>