<?php
use PHPMailer\PHPMailer\PHPMailer;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/PHPMailer/src/Exception.php';
require APPPATH . 'libraries/PHPMailer/src/PHPMailer.php';
require APPPATH . 'libraries/PHPMailer/src/SMTP.php';
require APPPATH . 'libraries/class.phpmailer.php';
require APPPATH . 'libraries/class.smtp.php';

class forgotpassword extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('forgotpassword_model');
        // $this->load->library('email');
    }

    public function index()
    {
        $this->load->view('auth/forgotpassword');
    }

    public function request()
    {
        $JSON = array();
        $params['EMAIL'] = strtolower(isset($_POST['email']) ? $_POST['email'] : "");

        $VALID_EMAIL = "/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/";
        if (empty($params['EMAIL'])) {
            $ERROR_MESSAGE = 'Email is required!';
            $ERROR_CODE = "EC:002A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        } elseif (!preg_match($VALID_EMAIL, $params['EMAIL'])) {
            $ERROR_MESSAGE = 'Email is not registered. Please Try Again';
            $ERROR_CODE = "EC:002A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        }

        $params['CODE_VERIFICATION'] = strtoupper(substr(md5(date('siHdmY')), 0, 10));
        $params['IP_LOG'] = $this->get_client_ip();

        $insertLogs = $this->forgotpassword_model->Notificationpassword($params);
        // var_dump($insertLogs);exit();
        $ERROR_CODE = $insertLogs['ErrorCode'];
        $ERROR_MESSAGE = $insertLogs['ErrorMessage'];
        if ($ERROR_CODE != "EC:0000") {
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        }

        $CODE_VERIFICATION = base64_encode(json_encode($params['CODE_VERIFICATION']));

        if ($ERROR_CODE == "EC:0000") {
            $paramMail['EMAIL_TO'] = $params['EMAIL'];
            $paramMail['FROM'] = $this->config->item('EMAIL');
            $paramMail['SUBJECT'] = "[Web Admin - Bank Bumi Arta]Request Change Password";
            $MESSAGE = '<html>';
            $MESSAGE .= '<body>';
            $MESSAGE .= "Hey, <br><br>";
            $MESSAGE .= "To change password your account, please use the following url: <br><br>";
            $MESSAGE .= "<a href='" . base_url() . "forgotpassword/verification/" . $CODE_VERIFICATION . "'>" . "Change password</a><br><br>";
            $MESSAGE .= "Thank you for confirm, we hope to see you again soon.";
            $MESSAGE .= '</body></html>';

            $paramMail['MESSAGE'] = $MESSAGE;
            $paramMail['EMAIL_CC'] = "";

            $this->_sendEmail($paramMail);
            $RetrieveInsertMail = $this->forgotpassword_model->insertMail($paramMail);
            $ErrorCode = $RetrieveInsertMail['ErrorCode'];
            $ErrorMessage = $RetrieveInsertMail['ErrorMessage'];
            if ($ErrorCode != "EC:0000") {
                $JSON = array("ErrorCode" => "EC:003A", "ErrorMessage" => $ErrorMessage);
                die(json_encode($JSON));
            }
        }

        $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => 'Please check your email, we have sent a confirmation email!');
        die(json_encode($JSON));
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public function verification($param)
    {
        // var_dump($param);
        $enc = base64_decode($param);
        // var_dump($enc);exit();
        $CODE_VERIFICATION = json_decode($enc);
        $params['CODE_VERIFICATION'] = $CODE_VERIFICATION;
        $verificationUser = $this->forgotpassword_model->verificationUser($params);
        $data = array();
        $data['DATA']['result'] = $verificationUser;

        $this->load->view('auth/verification', $data);
    }

    public function process()
    {
        // $VALID_PASSWORD = "/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/";
        $VALID_PASSWORD = "/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,20}$/";

        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $code_verification = isset($_POST['code_verification']) ? $_POST['code_verification'] : "";
        $password = isset($_POST['password']) ? $_POST['password'] : "";
        $password_1 = isset($_POST['password_1']) ? $_POST['password_1'] : "";
        $password_2 = isset($_POST['password_2']) ? $_POST['password_2'] : "";

        if (empty($password_1)) {
            $ERROR_MESSAGE = 'New password is required!';
            $ERROR_CODE = "EC:001A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        } elseif (empty($password_2)) {
            $ERROR_MESSAGE = 'Repeat new password is required!';
            $ERROR_CODE = "EC:001A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        } elseif ($password_1 != $password_2) {
            $ERROR_MESSAGE = 'Password not matching!';
            $ERROR_CODE = "EC:001A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        }

        if (!preg_match($VALID_PASSWORD, $password_1)) {
            $ERROR_MESSAGE = 'Password invalid, please use :<br>At least one lowercase char<br>At least one uppercase char<br>At least one digit number<br>At have to be 8-20 characters<br>At least one special sign of #?!@$%^&*-';

            $ERROR_CODE = "EC:001A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        } elseif (!preg_match($VALID_PASSWORD, $password_2)) {
            $ERROR_MESSAGE = 'Repeat password invalid, please use :<br>At least one lowercase char<br>At least one uppercase char<br>At least one digit number<br>At have to be 8-20 characters<br>At least one special sign of #?!@$%^&*-';
            $ERROR_CODE = "EC:001A";
            $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
            die(json_encode($JSON));
        }

        $params['EMAIL'] = $email;
        $params['CODE_VERIFICATION'] = $code_verification;
        $params['PASSWORD'] = $password;
        $params['NEW_PASSWORD'] = $password_1;

        $changepassword = $this->forgotpassword_model->changepassword($params);
        $ERROR_CODE = $changepassword['ErrorCode'];
        $ERROR_MESSAGE = $changepassword['ErrorMessage'];
        $JSON = array("ErrorCode" => $ERROR_CODE, "ErrorMessage" => $ERROR_MESSAGE);
        die(json_encode($JSON));
    }
    public function _sendEmail($param)
    {
        $mail = new PHPMailer();
        // $mail->SMTPDebug = 1;
        $mail->IsSMTP();
        $mail->Host = $this->config->item('mail_host');
        $mail->SMTPAuth = true;
        $mail->Username = $this->config->item('mail_username');
        $mail->Password = $this->config->item('mail_password');
        $mail->SMTPKeepAlive = true;
        $mail->Mailer = '“smtp”';
        // $mail->SMTPSecure = "tls";
        // $mail->Port = "587";

        $mail->From = $mail->Username;
        $mail->FromName = $this->config->item('mail_fromname');

        // $mail->setFrom('webadmin@bankbba.co.id', 'Web Admin Bank Bumi Arta');

        $mail->AddAddress($param['EMAIL_TO'], "User");

        $mail->WordWrap = 50;
        $mail->IsHTML(true);
        $mail->Subject = $param['SUBJECT'];
        $mail->Body = $param['MESSAGE'];

        if (!$mail->Send()) {
            $ErrorCode = "EC:003B";
            $ErrorMessage = "Mail could not be sent : " . $mail->ErrorInfo;

            $JSON = array("ErrorCode" => $ErrorCode, "ErrorMessage" => $ErrorMessage);
            die(json_encode($JSON));

        }

        // $message = $param['MESSAGE'];

        // // Email header
        // $subject = $param['SUBJECT'];
        // $from = $param['FROM'];
        // $Bcc = $param['EMAIL_CC'];
        // $to = $param['EMAIL_TO'];
        // $eol = "\r\n";
        // $header = "From: " . $from . $eol;
        // $header .= "Bcc: " . $Bcc . $eol;
        // $header .= "MIME-Version: 1.0" . $eol;
        // $header .= "Content-Type: text/html; charset=UTF-8" . $eol;

        // mail($to, $subject, $message, $header);
    }
}