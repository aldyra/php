 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

 <?php
$menu = isset($rows['menu']) ? $rows['menu'] : array();
$folders_arr = array();
foreach ($menu as $row) {
    $parentid = $row['MENU_PARENT'];
    $type = 'child';
    if ($parentid == '0' || $parentid == null || $parentid == '') {
        $parentid = '#';
        $type = 'default';
    }

    $selected = false;
    $opened = false;
    // if ($row['ID'] == 56) {
    //     $selected = true;
    //     $opened = true;
    // }
    $folders_arr[] = array(
        "id" => $row['ID'],
        "parent" => $parentid,
        "type" => $type,
        "text" => $row['NAME_ID'] . '/' . $row['NAME_EN'],
        "state" => array("selected" => $selected, "opened" => $opened),
        "data" => $row['MENU_LEVEL'],
    );
}
?>
 <div class="row">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             Menu Name in Indonesian<span style="color:orange">*</span>
         </label>
         <input type="text" name="name_id" class="form-control" id="name_id" placeholder=""
             data-parsley-trigger="focusin focusout" required="">
     </div>
 </div>
 <div class="row">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             Menu Name in English<span style="color:orange">*</span>
         </label>
         <input type="text" name="name_en" class="form-control" id="name_en" placeholder="" required=""
             data-parsley-trigger="focusin focusout">
     </div>
 </div>
 <div class="row">
     <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
         <label>
             Link Indonesian<span style="color:orange">*</span>
         </label>
         <input type="text" name="link_id" class="form-control" id="link_id" placeholder="" required=""
             data-parsley-trigger="focusin focusout" data-parsley-length="[3, 100]"
             data-parsley-pattern="^[a-z0-9]+(?:-[a-z0-9]+)*$">
     </div>
     <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
         <label>
             Link English<span style="color:orange">*</span>
         </label>
         <input type="text" name="link_en" class="form-control" id="link_en" placeholder="" required=""
             data-parsley-trigger="focusin focusout" data-parsley-length="[3, 100]"
             data-parsley-pattern="^[a-z0-9]+(?:-[a-z0-9]+)*$">
     </div>
 </div>
 <div class="row">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="form-check">
             <label class="form-check-label">
                 <input type="checkbox" class="form-check-input" checked="" value="1" name="is_content" id="is_content">
                 MENU CONTENT
             </label>
         </div>
     </div>
 </div>

 <div class="row">
     <div class="col-xl-12 col-md-12">
         <label>Position<span style="color:orange">*</span></label>
         <select id="position" class="form-control select2modal" name="position" required>
             <option selected disabled>Choose Option</option>
             <option value="1">HEADER</option>
             <option value="2">FOOTER</option>
             <option value="3">FOOTER & HEADER</option>

         </select>
     </div>
 </div>
 <div class="row" style="display:none;" id="sequence_headerdiv">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             Sequence for Header<span style="color:orange">*</span>
         </label>
         <input type="text" name="sequence_header" class="form-control" id="sequence_header" onkeyup="validate(this);"
             data-parsley-minlength="1" data-parsley-maxlength="2" placeholder="" required="">
     </div>
 </div>
 <div class="row" style="display:none;" id="sequence_footerdiv">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             Sequence for Footer<span style="color:orange">*</span>
         </label>
         <input type="text" name="sequence_footer" class="form-control" id="sequence_footer" onkeyup="validate(this);"
             data-parsley-minlength="1" data-parsley-maxlength="2" placeholder="" required="">
     </div>
 </div>
 <div class="row">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             Footer Note Indonesian
         </label>
         <textarea type="text" name="footer_note_id" class="form-control" id="footer_note_id" rows="4"></textarea>
     </div>
 </div>
 <div class="row">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             Footer Note English
         </label>
         <textarea type="text" name="footer_note_en" class="form-control" id="footer_note_en" rows="4"></textarea>
     </div>
 </div>
 <div class="row">
     <div class="col-xl-12 col-md-12">
         <label>Menu Level<span style="color:orange">*</span></label>
         <select id="parent" class="form-control select2modal" name="parent" required>
             <option selected disabled>Choose Option</option>
             <option value="menu_parent">MENU PARENT</option>
             <option value="menu_child">MENU CHILD</option>

         </select>
     </div>
 </div>

 <div class="row" style="display:none;" id="menuParent">
     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <label>
             CHOOSE MENU PARENT
         </label>
         <div id="folder_jstree"></div>
         <input type="hidden" id="menu_parent" name="menu_parent" />
         <input type="hidden" id="menu_level" name="menu_level" />
     </div>
     <textarea id='txt_folderjsondata' style="display:none;"><?=json_encode($folders_arr)?></textarea>
 </div>
 <div class="row">
     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
     <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
         <div class="progress" style="display:none">
             <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                 aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                 <span class="sr-only">0%</span>
             </div>
         </div>
         <div class="msg alert alert-info text-left" style="display:none"></div>
     </div>
 </div>

 <script type="text/javascript">
$(function() {
    var folder_jsondata = JSON.parse($('#txt_folderjsondata').val());
    $(".select2modal").select2();

    $('#folder_jstree').jstree({
        'core': {
            'data': folder_jsondata,
            'multiple': false
        },
        'checkbox': {
            'three_state': false,
        },
        'plugins': ['checkbox', 'types'],
        'types': {
            'default': {
                'icon': 'fa fa-angle-double-down'
            },
            'child': {
                'icon': 'fa fa-angle-double-right'
            }
        },
    });

    $("#folder_jstree").on(
        "select_node.jstree",
        function(evt, data) {
            $menu_parent = data.node.id;
            $menu_level = data.node.data;

            $('#menu_parent').val($menu_parent);
            $('#menu_level').val($menu_level);
        }
    );

    $('[name="parent"]').change(function() {
        var menu = $(this).val();
        if (menu == 'menu_child') {
            $('#menuParent').show();
        } else {
            $('#menuParent').hide();
        }
    });
    $('#position').change(function() {
        var position = $(this).val();
        if (position == '1') {
            document.getElementById("sequence_header").required = true;
            document.getElementById("sequence_footer").required = false;
            $('#sequence_footer').val('');
            $('#sequence_headerdiv').show();
            $('#sequence_footerdiv').hide();
        } else if (position == '2') {
            document.getElementById("sequence_header").required = false;
            document.getElementById("sequence_footer").required = true;
            $('#sequence_header').val('');
            $('#sequence_headerdiv').hide();
            $('#sequence_footerdiv').show();
        } else if (position == '3') {
            document.getElementById("sequence_header").required = true;
            document.getElementById("sequence_footer").required = true;
            $('#sequence_header').val('');
            $('#sequence_footer').val('');
            $('#sequence_headerdiv').show();
            $('#sequence_footerdiv').show();

        }
    });


});
 </script>