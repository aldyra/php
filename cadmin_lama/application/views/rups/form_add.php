<!-- Theme JS files -->
<script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>

<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script>
<!-- /theme JS files -->
<?php
$is_create = $authorize['is_create'];
$back = isset($data['back']) ? $data['back'] : '';

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master Reporting</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master RUPS</a>
                <a class="breadcrumb-item active">Add</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master RUPS</span> - Add
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_create): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveRUPS">
            <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>RUPS Description Indonesian<span style="color:orange">*</span></label>
                        <input type="text" class="form-control" id="description_id" name="description_id"
                            placeholder="Description in Indonesian" required="" parsley-trigger="change">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>RUPS Description English<span style="color:orange">*</span></label>
                        <input type="text" class="form-control" id="description_en" name="description_en"
                            placeholder="Description in English" required="" parsley-trigger="change">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">RUPS Detail</h6>
                        <a href="#" onclick="openTnC('<?php echo base_url('rups/tncupload') ?>')">Terms and Conditions
                            Upload</a>
                    </div>
                    <div class="col-md-6">
                        <button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" style="float:right;"
                            type="button" onclick="add_detail();"><b><i class="fas fa-plus"></i></b>
                            Add
                        </button>
                    </div>
                </div>

                <hr>
                <div id="form_detail">
                    <div class="card border-left-info border-right-info rounded-0" id="removeclassfirst">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>Name Indonesian<span style="color:orange">*</span></label>
                                                <input type="text" class="form-control" id="name_id" name="name_id[]"
                                                    placeholder="Description in Indonesian" required=""
                                                    parsley-trigger="change">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>Name English<span style="color:orange">*</span></label>
                                                <input type="text" class="form-control" id="name_en" name="name_en[]"
                                                    placeholder="Description in English" required=""
                                                    parsley-trigger="change">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>File Upload Indonesia<span style="color:orange">*</span></label>
                                                <input type="file" name="file_id[]" id="file_id"
                                                    class="form-control-uniform" required="" data-fouc
                                                    data-parsley-errors-container="#validation-error-block-id">
                                                <div class="" id="validation-error-block-id"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="form-group">
                                                <label>File Upload English<span style="color:orange">*</span></label>
                                                <input type="file" name="file_en[]" id="file_en"
                                                    class="form-control-uniform" required="" data-fouc
                                                    data-parsley-errors-container="#validation-error-block-en">
                                                <div class="" id="validation-error-block-en"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                    <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                        type="button" onclick="remove('first');"><b><i class="fas fa-minus"></i></b>
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="progress" style="display:none">
                            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar"
                                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                <span class="sr-only">0%</span>
                            </div>
                        </div>
                        <div class="msg alert alert-info text-left" style="display:none"></div>
                    </div>
                </div>


            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

    <?php endif;?>

</div>

<script type="text/javascript">
var room = 0;

function add_detail() {
    room++;
    var objTo = document.getElementById('form_detail');
    html = '';
    html =
        ' <div class="card border-left-info border-right-info rounded-0" id="removeclass' +
        room + '"><div class="card-body"><div class="row" >' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
        '<div class="row">' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
        '<div class="form-group">' +
        '<label>Name Indonesian<span style="color:orange">*</span></label>' +
        '<input type="text" class="form-control" id="name_id" name="name_id[]" placeholder="Description in Indonesian" required="" parsley-trigger="change">' +
        '</div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
        '<div class="form-group">' +
        '<label>Name English<span style="color:orange">*</span></label>' +
        '<input type="text" class="form-control" id="name_en" name="name_en[]" placeholder="Description in English" required=""parsley-trigger="change">' +
        '</div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
        '<div class="form-group">' +
        '<label>File Upload Indonesia<span style="color:orange">*</span></label>' +
        '<input type="file" name="file_id[]" id="file" class="form-control-uniform" required="" data-fouc data-parsley-errors-container="#validation-error-block-id' +
        room + '">' +
        '<div class="" id="validation-error-block-id' + room + '"></div>' +
        ' </div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
        '<div class="form-group">' +
        '<label>File Upload English<span style="color:orange">*</span></label>' +
        '<input type="file" name="file_en[]" id="file_en" class="form-control-uniform" required="" data-fouc data-parsley-errors-container="#validation-error-block-en' +
        room + '">' +
        '<div class="" id="validation-error-block-en' + room + '"></div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
        '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type="button" onclick="remove(' +
        room + ');"><b><i class="fas fa-minus"></i></b>' +
        'Remove</button>' +
        '</div>' +
        '</div></div></div>';
    $("#form_detail").append(html);
    $('.form-control-uniform').uniform();
}

function remove(rid) {
    Swal.fire({
        title: "Are you sure?",
        type: "warning",
        text: "Your will not be able to recover this data!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes",
    }).then((result) => {
        if (result.value) {
            $('#removeclass' + rid).remove();

        }
    });

}
$('#frmsaveRUPS').on('submit', function(e) {
    e.preventDefault();
    $(".progress").show();
    $(".msg").hide();

    if ($(this).parsley().isValid()) {
        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e) {
                    if (e.lengthComputable) {
                        var percent = Math.round((e.loaded / e.total) * 100);

                        $("#progressBar")
                            .attr("aria-valuenow", percent)
                            .css("width", percent + "%")
                            .text(percent + "%");
                    }
                });
                return xhr;
            },
            url: "<?php echo base_url('rups/add'); ?>",
            data: formData,
            dataType: "JSON",
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            success: function(res) {
                var ErrorMessage = res.ErrorMessage;
                var ErrorCode = res.ErrorCode;
                if (ErrorCode != "EC:0000") {
                    Swal.fire({
                        type: "error",
                        html: ErrorMessage,
                        confirmButton: true,
                        confirmButtonColor: "#1FB3E5",
                        confirmButtonText: "Close",
                    });
                } else {
                    Swal.fire({
                        position: "center",
                        type: "success",
                        text: ErrorMessage,
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    setTimeout(function() {
                        var uri = "<?php echo $back; ?>";
                        window.location.href = uri;
                    }, 1500);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire(textStatus).then(function() {
                    Swal.fire({
                        type: "error",
                        title: textStatus,
                    });
                });
            },
            beforeSend: function() {
                $('#loadingBox').show();
            },
            complete: function() {
                $('#loadingBox').fadeOut();
            }
        });
    } else {
        ;
    }
});
</script>