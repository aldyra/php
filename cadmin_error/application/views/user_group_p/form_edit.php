<?php
$dataTree = isset($param['dataTree']) ? $param['dataTree']:array();
?>
<form id="frmsaveUGroupPrivilege" action="javascript:void(0)">
	<input type="hidden" name="uGroupCBID" id="uGroupCBID">
	<!-- TREE VIEW -->
	<div id="containerbasicTree"><div id="basicTree"></div></div>
	<!-- /TREE VIEW -->
	<div class="form-group mb-0 mt-3" id="btnformUGroupPrivilege" style="text-align: right;">
	    <button class="btn btn-success waves-effect waves-light mr-1" type="button" id="checkallCB" style="float: left"> Check/Uncheck All</button>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		// $("#containerbasicTree").html("<div id='basicTree'></div>");
		// var dataTree = "<?php echo $dataTree ?>";
		var data = [];
		$("#basicTree").jstree( {
            plugins: ["core", "ui", "grid", "types"],
            grid: {
                columns: [
                    {width: 280, header: "Module Menu", value: "ModuleMenu", title: "Module Menu"},
                    {width: 75, header: "View", value: "Read", title: "Read"},
                    {width: 75, header: "Edit", value: "Edit", title: "Edit"},
                    {width: 75, header: "Add", value: "Add", title: "Add"},
                    {width: 75, header: "Delete", value: "Delete", title: "Delete"},
                    {width: 75, header: "Export", value: "Export", title: "Export"},
                    {width: 65, header: "Import", value: "Import", title: "Import"},
                    {width: 95, header: "Btn", value: "Btn", title: "Btn"}
                ],
                resizable: true,
                contextmenu: true
            },
            core: {
                themes: {
                    responsive: !1
                },
                data: data

            }, 
            types: {
                default: {
                    icon: "mdi mdi-folder-star"
                }
                , file: {
                    icon: "mdi mdi-file"
                }
            }
        });
		
	});
	
</script>