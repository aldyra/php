<?php
$data = isset($data) ? $data : "";
$ID = isset($data['menu']->ID) ? $data['menu']->ID : "";
$NAME_ID = isset($data['menu']->NAME_ID) ? $data['menu']->NAME_ID : "";
$NAME_EN = isset($data['menu']->NAME_EN) ? $data['menu']->NAME_EN : "";
$LINK_ID = isset($data['menu']->LINK_ID) ? $data['menu']->LINK_ID : "";
$LINK_EN = isset($data['menu']->LINK_EN) ? $data['menu']->LINK_EN : "";
$ICON = isset($data['menu']->ICON) ? $data['menu']->ICON : "";
$MENU_LEVEL = isset($data['menu']->MENU_LEVEL) ? $data['menu']->MENU_LEVEL : "";
$MENU_PARENT = isset($data['menu']->MENU_PARENT) ? $data['menu']->MENU_PARENT : "";
$IS_CONTENT = isset($data['menu']->IS_CONTENT) ? $data['menu']->IS_CONTENT : "";
$FOOTER_NOTE_ID = isset($data['menu']->FOOTER_NOTE_ID) ? $data['menu']->FOOTER_NOTE_ID : "-";
$FOOTER_NOTE_EN = isset($data['menu']->FOOTER_NOTE_EN) ? $data['menu']->FOOTER_NOTE_EN : "-";
$FILE_BANNER_ID = isset($data['menu']->FILE_BANNER_ID) ? $data['menu']->FILE_BANNER_ID : "";
$FILE_BANNER_EN = isset($data['menu']->FILE_BANNER_EN) ? $data['menu']->FILE_BANNER_EN : "";
$SEQUENCE_HEADER = isset($data['menu']->SEQUENCE_HEADER) ? $data['menu']->SEQUENCE_HEADER : "";
$SEQUENCE_FOOTER = isset($data['menu']->SEQUENCE_FOOTER) ? $data['menu']->SEQUENCE_FOOTER : "";
$POSITION = isset($data['menu']->POSITION) ? $data['menu']->POSITION : "";
$STATUS = isset($data['menu']->STATUS) ? $data['menu']->STATUS : "";
$CREATED_BY = isset($data['menu']->CREATED_BY) ? $data['menu']->CREATED_BY : "";
$CREATED_DATE = isset($data['menu']->CREATED_DATE) ? $data['menu']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}
if ($POSITION == "1") {
    $POSITION = "Header";
} elseif ($POSITION == "2") {
    $POSITION = "Footer";
} elseif ($POSITION == "3") {
    $POSITION = "Header & Footer";
} else {
    $POSITION = "";
}
if ($MENU_PARENT == "0") {
    $MENU_LEVEL = "Menu Parent";
} else {
    $MENU_LEVEL = "Menu Child";
}

if ($IS_CONTENT == "1") {
    $CONTENT = "<span class='badge badge-primary'>Content</span>";
} else {
    $CONTENT = "";
}

$back = isset($data['back']) ? $data['back'] : '';
$detail = isset($data['detail']) ? $data['detail'] : array();
$is_read = $authorize['is_read'];
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item active">Master Menu</a>
                <a href="#" class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Menu</span> -
                Detail

            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Combined table styles -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $CONTENT ?>

                    <dl class="mb-0 mt-2">
                        <dt>Header Banner ID</dt>
                        <dd><?php if ($FILE_BANNER_ID) {?>
                            <a href='<?php echo $FILE_BANNER_ID ?>' data-popup='lightbox' data-fancybox><img
                                    src='<?php echo $FILE_BANNER_ID ?>' class='img-thumbnail' width='120px'></a>
                            <?php } else {echo '-';}?>
                        </dd>
                        <dt>Header Banner EN</dt>
                        <dd><?php if ($FILE_BANNER_EN) {?>
                            <a href='<?php echo $FILE_BANNER_EN ?>' data-popup='lightbox' data-fancybox><img
                                    src='<?php echo $FILE_BANNER_EN ?>' class='img-thumbnail' width='120px'></a>
                            <?php } else {echo '-';}?>
                        </dd>
                        <dt>Name</dt>
                        <dd><?php echo $NAME_ID . ' - ' . $NAME_EN ?></dd>
                        <dt>Link</dt>
                        <dd>
                            <?php echo $LINK_ID . ' / ' . $LINK_EN ?>
                        </dd>
                        <dt>Position</dt>
                        <dd><?php echo $POSITION ?></dd>
                        <dt>Sequence Header/Footer</dt>
                        <dd><?php echo $SEQUENCE_HEADER . ' / ' . $SEQUENCE_FOOTER ?></dd>
                        <dt>Footer Note IDN</dt>
                        <dd><?php echo !empty($FOOTER_NOTE_ID) ? $FOOTER_NOTE_ID : "-" ?>
                        </dd>
                        <dt>Footer Note ENG</dt>
                        <dd><?php echo !empty($FOOTER_NOTE_EN) ? $FOOTER_NOTE_EN : "-" ?>
                        </dd>
                    </dl>
                </div>
            </div>

        </div>



        <div class="card-footer bg-white d-flex justify-content-between">
            <div>
                <div class="list-icons">

                </div>
            </div>

            <ul class="list-inline list-inline-dotted mb-0 mt-1 mt-sm-0">
                <li class="list-inline-item">By <?php echo $CREATED_BY ?></li>
                <li class="list-inline-item"><?php echo convertDate($CREATED_DATE) ?></li>
                <li class="list-inline-item"><?php echo $STS ?></li>
            </ul>
        </div>
    </div>
    <!-- /combined table styles -->

</div>