<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Modulep_model');
    }

    public function index()
    {   
        $Username = isset($_SESSION['username']) ? $_SESSION['username']:"";

        $menu0 = $this->Modulep_model->getModule($Username);
        $menu = array();
        foreach($menu0 as $menu_0){
            $ID = isset($menu_0->ID) ? $menu_0->ID:"";
            $CONTROLLER = isset($menu_0->CONTROLLER) ? $menu_0->CONTROLLER:"";
            $CONTROLLER = str_replace(" ", "", $CONTROLLER);
            $ICON = isset($menu_0->ICON) ? $menu_0->ICON:"";
            $ICON = str_replace(" ", "", $ICON);
            $FUNCTION = isset($menu_0->FUNCTION) ? $menu_0->FUNCTION:"";
            $FUNCTION = str_replace(" ", "", $FUNCTION);
            $MODULE_NAME = isset($menu_0->MODULE_NAME) ? $menu_0->MODULE_NAME:"";

            if($CONTROLLER != "" && $FUNCTION != "" && $ICON != "") {
                $parent['ID'] = $ID;
                $parent['MODULE_NAME'] = $MODULE_NAME;
                $parent['CONTROLLER'] = $CONTROLLER;
                $parent['FUNCTION'] = $FUNCTION;
                $parent['ICON'] = $ICON;
                $parent['SUBMENU'] = array();
                $menu[] = $parent;
            }else{
                $menu1 = $this->Modulep_model->getModule($_SESSION['username'], array('MENU_LEVEL' => 1, 'MENU_PARENT' => $menu_0->ID));
                
                $menu_child = array();
                if(!empty(sizeof($menu1))){
                    foreach($menu1 as $menu_1){
                        $child['ID'] = $menu_1->ID;
                        $child['MODULE_NAME'] = $menu_1->MODULE_NAME;
                        $child['CONTROLLER'] = $menu_1->CONTROLLER;
                        $child['FUNCTION'] = $menu_1->FUNCTION;
                        $child['ICON'] = $menu_1->ICON;
                        $menu_child[] = $child;
                    }
                    $parent['ID'] = $ID;
                    $parent['MODULE_NAME'] = $MODULE_NAME;
                    $parent['CONTROLLER'] = $CONTROLLER;
                    $parent['FUNCTION'] = $FUNCTION;
                    $parent['ICON'] = $ICON;
                    $parent['SUBMENU'] = $menu_child;
                    $menu[] = $parent;
                }
            }
        }
        $data['menu'] = $menu;
        $this->load->view('template', $data);
    }
}