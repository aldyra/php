<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
if($SiteLang == "id"){
	$Url = base_url().$SiteLang."/".$this->config->item('url_jobvacancy_id');
}elseif($SiteLang == "en"){
	$Url = base_url().$SiteLang."/".$this->config->item('url_jobvacancy_en');
}
$GROUP_NAME = isset($jobvacancy['GROUP_NAME']) ? $jobvacancy['GROUP_NAME']:"";
$CODE = isset($jobvacancy['CODE']) ? $jobvacancy['CODE']:"";
$POSITION = isset($jobvacancy['POSITION']) ? $jobvacancy['POSITION']:"";
$JOB_DESCRIPTION = isset($jobvacancy['JOB_DESCRIPTION']) ? $jobvacancy['JOB_DESCRIPTION']:"";
$REQUIREMENT = isset($jobvacancy['REQUIREMENT']) ? $jobvacancy['REQUIREMENT']:"";
$DETAIL = isset($jobvacancy['DETAIL']) ? $jobvacancy['DETAIL']:"";
$START_DATE = isset($jobvacancy['START_DATE']) ? $jobvacancy['START_DATE']:"";
$END_DATE = isset($jobvacancy['END_DATE']) ? $jobvacancy['END_DATE']:"";
$CREATED_DATE = isset($jobvacancy['CREATED_DATE']) ? $jobvacancy['CREATED_DATE']:"";
$STATUS_JOB = isset($jobvacancy['STATUS_JOB']) ? $jobvacancy['STATUS_JOB']:"";

$FILE_BANNER = $MENU_CHILD_FILE_BANNER;
$stylebanner = "height:250px";
if(!empty($FILE_BANNER)){
	$stylebanner = "background-image:url('".$FILE_BANNER."'); background-size:cover;height:250px;";
}
?>
<div class="row row-header" style="<?php echo $stylebanner ?>">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important;"><?php echo $GROUP_NAME ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color: #707070;"><?php echo $this->lang->line('home') ?></a> / 
		<label style="color: #707070;"><?php echo $this->lang->line('lowker-4') ?></label> /
		<a href="javascript:void(0)" onclick="window.history.back(-1)" style="color: #707070;"><?php echo $this->lang->line('lowker-5') ?></a> /
		<label style="color:#707070"><?php echo $POSITION ?></label>
	</div>
</div>

<div class="container" style="padding-bottom: 50px;">
	<div class="row d-padtop-10">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padtop-10">
			<h3><b><?php echo $POSITION ?></b></h3>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="padding-top: 0; font-size: 14px;">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 d-padding">
					<span><?php echo $this->lang->line('lowker-8') ?> :</span>&nbsp;<span class="t-default">
					[( <?php echo $CODE ?> )]</span>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 d-padding">
					<span>Status :</span>&nbsp;<span class="t-default"><?php echo $STATUS_JOB ?></span>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 d-padding">
					<span><?php echo $this->lang->line('lowker-9') ?> :</span>&nbsp;<span class="t-default"><?php echo date('d F Y', strtotime($CREATED_DATE)) ?></span>
				</div>
			</div>
			
			<br>
			<p><b><?php echo $this->lang->line('lowker-10') ?> :</b></p>
			<p><?php echo $JOB_DESCRIPTION ?></p>

			<p><b><?php echo $this->lang->line('lowker-11') ?> :</b></p>
			<p><?php echo $REQUIREMENT ?></p>
			<br>
			<span style="color: #213E8B !important;">
				<!-- <?php echo $this->lang->line('lowker-12') ?> -->
				<?php echo $DETAIL ?>
			</span>
			<p>
				<b>Recruitment & Training Division<br>
				P.T. Bank Bumi Arta Tbk.</b><br>
				Jln. K. H. Wahid Hasyim No. 234<br>
				Jakarta Pusat 10250<br>
				Atau<br>
				Via Email : hrd@bankbba.co.id
			</p>
		</div>
	</div>
	<div style="max-width: 100%" class="mb-3 mt-3">
		<?php require_once(APPPATH.'views/share_sosmed.php'); ?>
		<div style="clear:both"></div>
	</div>	
</div>