<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model');
    }

    function send(){
        // Load PHPMailer library
        // $this->load->library('phpmailer_lib');
        // $this->load->library('email');

        // SMTP configuration
        $subject = "This is a test";
        $message = "test";

        // Get full html:
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);

        // Attaching the logo first.
        // $file_logo = FCPATH.'apple-touch-icon-precomposed.png';  // Change the path accordingly.
        // The last additional parameter is set to true in order
        // the image (logo) to appear inline, within the message text:
        // $this->email->attach($file_logo, 'inline', null, '', true);
        // $cid_logo = $this->email->get_attachment_cid($file_logo);
        // $body = str_replace('cid:logo_src', 'cid:'.$cid_logo, $body);
        // End attaching the logo.
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.bankbba.co.id',
            'smtp_port' => 587,
            'smtp_user' => 'webadmin@bankbba.co.id',
            'smtp_pass' => 'Password12!@',
            'mailtype'  => 'html'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        // $result = $this->email->send();
        $to = "webadmin@bankbba.co.id";
        $this->email->initialize();
        $result = $this->email
            ->from("webadmin@bankbba.co.id")
            ->reply_to("webadmin@bankbba.co.id")    // Optional, an account where a human being reads.
            ->to($to)
            ->subject($subject)
            ->message($message)
            ->send();

        var_dump($result);
        echo '<br />';
        echo $this->email->print_debugger();

        exit;
    }

    public function index()
    {
        $data = array();
        $data['content'] = "test/view";
        // $data['authorize'] = $this->data['authorize'];
        $data['data']['menu'] = $this->menu_model->view();
        $data['data']['datamenu'] = $this->get_menu();
        $this->load->view('templates/view', $data);
    }
    public function get_menu()
    {
        $data_menu = $this->menu_model->get();
        $result = array();
        foreach ($data_menu as $data) {
            $param['MENU_PARENT'] = $data->MENU_PARENT;
            $param['ID'] = $data->ID;
            $param['NAME_ID'] = $data->NAME_ID;
            $param['NAME_EN'] = $data->NAME_EN;
            $param['MENU_LEVEL'] = $data->MENU_LEVEL;

            $result[] = $param;
        }
        return $result;
    }

}