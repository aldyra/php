<?php
$data = isset($data) ? $data : "";
$Menu = isset($data['menu']) ? $data['menu'] : array();
$Controllers = $this->uri->segment(1);
$paramadd = array();
$paramadd['menu'] = isset($data['datamenu']) ? $data['datamenu'] : array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('menu/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];
?>
<!-- page headder -->
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url() ?>assets/lib/date-time-picker/css/jquery.datetimepicker.css" />
<script src="<?php echo base_url() ?>assets/js/demo_pages/datatables_basic.js"></script>
<style>


</style>
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item active">Master Menu</a>
                <!-- <span class="breadcrumb-item \ active">Dashboard</span> -->
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <a href="<?php echo base_url("menu/add_menu/") ?>">
                <button data-toggle="tooltip" data-placement="top" title="Add" type="button" class="btn btn-info">
                    <i class="fas fa-plus-circle"></i> Add Menu
                </button>
            </a>
        </div>
        <?php endif;?>

        <table id="tableData" class="table" width="100%">
            <thead>
                <tr>
                    <th>Name Indonesian</th>
                    <th>Name English</th>
                    <th data-orderable="false">Menu Level</th>
                    <th data-orderable="false">Menu Position</th>
                    <th data-orderable="false">Sequence Header</th>
                    <th data-orderable="false">Sequence Footer</th>
                    <th>Created By</th>
                    <th data-orderable="false" width="150px" class="head">Created Date</th>
                    <th data-orderable="false" width="150px">Status</th>
                    <th style="display:none;">sts</th>
                    <th style="display:none;">position</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($Menu as $menu) {
    $ID = isset($menu['ID']) ? $menu['ID'] : "";
    $NAME_ID = isset($menu['NAME_ID']) ? $menu['NAME_ID'] : "";
    $NAME_EN = isset($menu['NAME_EN']) ? $menu['NAME_EN'] : "";
    $POSITION = isset($menu['POSITION']) ? $menu['POSITION'] : "";
    $SEQUENCE_HEADER = isset($menu['SEQUENCE_HEADER']) ? $menu['SEQUENCE_HEADER'] : "";
    $SEQUENCE_FOOTER = isset($menu['SEQUENCE_FOOTER']) ? $menu['SEQUENCE_FOOTER'] : "";
    $MENU_PARENT = isset($menu['MENU_PARENT']) ? $menu['MENU_PARENT'] : "";
    $IS_CONTENT = isset($menu['IS_CONTENT']) ? $menu['IS_CONTENT'] : "";
    $STATUS = isset($menu['STATUS']) ? $menu['STATUS'] : "";
    $CREATED_BY = isset($menu['CREATED_BY']) ? $menu['CREATED_BY'] : "";
    $CREATED_DATE = isset($menu['CREATED_DATE']) ? date('d M Y', strtotime($menu['CREATED_DATE'])) : "";
    $USER_LOG = isset($menu['USER_LOG']) ? $menu['USER_LOG'] : "";
    $DATE_LOG = isset($menu['DATE_LOG']) ? date('d M Y', strtotime($menu['DATE_LOG'])) : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    if ($POSITION == "1") {
        $PSTN = "Header";
    } elseif ($POSITION == "2") {
        $PSTN = "Footer";
    } elseif ($POSITION == "3") {
        $PSTN = "Header & Footer";
    } else {
        $PSTN = "";
    }
    if ($MENU_PARENT == "0") {
        $MENU_LEVEL = "Menu Parent";
    } else {
        $MENU_LEVEL = "Menu Child";
    }

    ?>
                <tr>
                    <td>
                        <?php echo $NAME_ID ?>
                    </td>
                    <td>
                        <?php echo $NAME_EN ?>
                    </td>
                    <td>
                        <?php echo $MENU_LEVEL ?>
                    </td>
                    <td>
                        <?php echo $PSTN ?>
                    </td>
                    <td>
                        <?php echo $SEQUENCE_HEADER ?>
                    </td>
                    <td>
                        <?php echo $SEQUENCE_FOOTER ?>
                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td style="display:none;">
                        <?php echo $STATUS ?>
                    </td>
                    <td>
                        <?php echo $POSITION ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $paramaction['Controllers'] = $Controllers;
        $paramaction['Url'] = base_url() . $Controllers;
        $paramaction['Data'] = $menu;
        $paramaction['Data']['menu'] = $data['datamenu'];
        $paramaction['Modal'] = "modal-lg";
        $enc = base64_encode(json_encode($paramaction));
        ?>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo base_url("menu/detail_menu/") . $ID ?>" class="dropdown-item">
                                        <i class="fa fa-eye"></i> Detail
                                    </a>
                                    <?php if (!empty($is_update)) {?>
                                    <a href="<?php echo base_url("menu/edit_menu/") . $ID ?>" class="dropdown-item">
                                        <i class="icon-pencil"></i> Edit
                                    </a>

                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i>
                                        Activate</a>

                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i>
                                        Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?}elseif($STATUS == "99"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<!-- <script src="<?php echo base_url(); ?>assets/js/table.js"></script> -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/lib/date-time-picker/src/jquery.datetimepicker.js">
</script>
<script script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script type="text/javascript">
function init() {
    $('[data-popup="lightbox"]').fancybox({
        padding: 3,
    });
}
$("#tableData thead th").each(function() {
    var title = $(this).text();
    if (title == "Action" || title == "Image" || title == "Video") {} else if (title == "Type Log") {
        $(this).append(
            '<br><select onchange="changeSelectType(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="UPDATE">UPDATE</option><option value="INSERT">INSERT</option></select>'
        );
    } else if (title == "Language") {
        $(this).append(
            '<br><select onchange="changeSelectLang(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="Indonesia">Indonesia</option><option value="English">English</option></select>'
        );
    } else if (title == "Article") {
        $(this).append(
            '<br><select onchange="changeSelectArticle(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
        );
    } else if (title == "Highlight") {
        $(this).append(
            '<br><select onchange="changeSelectHighlight(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
        );
    } else if (title == "Footer") {
        $(this).append(
            '<br><select onchange="changeSelectFooter(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="YES">YES</option><option value="NO">NO</option></select>'
        );
    } else if (title == "Status") {
        $(this).append(
            '<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option><option value="99">Publish</option></select>'
        );
    } else if (title == "Menu Level") {
        $(this).append(
            '<br><select onchange="changeSelectLevel(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="Menu Parent">Menu Parent</option><option value="Menu Child">Menu Child</></select>'
        );
    } else if (title == "Menu Position") {
        $(this).append(
            '<br><select onchange="changeSelectPosition(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="1">Header</option><option value="2">Footer</option><option value="3">Header & Footer</option></select>'
        );
    } else if (title == "Created Date") {
        $(this).append(
            '<br>' +
            '<input type="date" id="created_date" class="created_date" data-date="" data-date-format="DD MMM YYYY" value="" onchange="changeSelectDate(this)"/>' +
            '<button onclick="changeSelectDate(\'' + 'delete' + '\')">X</button>'
        );


    } else {
        $(this).append(
            '<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
        );
    }
});
var t = $("#tableData").DataTable({
    // serverSide: false,
    autoWidth: false,
    // keys: !0,
    // order: [],
    // autofill: true,
    // select: true,
    // responsive: true,
    scrollX: true,
    // scrollCollapse: true,
    // fixedColumns: {
    // leftColumns: 1,
    // rightColumns: 1,
    // },
    // buttons: true,
    orderCellsTop: true,
    dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',
});

$('#tableData').css('min-height', '200px');
t.columns().every(function() {
    var that = this;
    $("input", this.header()).on("keyup", function(e) {
        if (event.keyCode === 8) {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        } else {
            if (this.value.length >= 1) {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            }
        }
    });
});

$("#tableData thead input").on("click", function(e) {
    e.stopPropagation();
});

t.column(9).visible(false);
t.column(10).visible(false);


function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(9)
            .search(dis.value)
            .draw();
    } else {
        t.column(9)
            .search(dis.value)
            .draw();
    }

}


function changeSelectDate(dis) {
    if (dis == 'delete') {
        $("input[type=date]").val("");
        t.column(7)
            .search('')
            .draw();
    } else {

        dis.setAttribute(
            "data-date",
            moment(dis.value, "YYYY-MM-DD")
            .format(dis.getAttribute("data-date-format"))
        )
        date = moment(dis.value, "YYYY-MM-DD").format("DD MMM YYYY");
        console.log(date);
    }
    if (dis.value != "" && dis.value != null) {
        t.column(7)
            .search(date)
            .draw();
    } else {
        t.column(7)
            .search("")
            .draw();

    }

    // $("#created_date").val("");


}

function changeSelectLevel(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}

function changeSelectPosition(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(10)
            .search(dis.value)
            .draw();
    } else {
        t.column(10)
            .search(dis.value)
            .draw();
    }

}
</script>