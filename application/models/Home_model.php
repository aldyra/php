<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getbanner()
    {
    	$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
    	$where['LANGUAGE'] = $SiteLang;
    	$where['STATUS'] = "99";
    	$Get = $this->db->where($where)->get("MS_BANNER");
    	$row = $Get->result_array();
    	return $row;
    }

    public function getnews()
    {
        $SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
        $where['LANGUAGE'] = $SiteLang;
        $where['STATUS'] = "99";
        $Get = $this->db->where($where)->get("MS_NEWS");
        $row = $Get->result_array();
        return $row;
    }
}