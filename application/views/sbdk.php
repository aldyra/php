<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();
?>

<style>
	.content-sbdk{
		max-width: 95%;
		text-align: center;
	}
	.title-sbdk{
		text-align: center;
		color: #212529;
		font-weight: normal;
		font-size: 18px;
	}
	.efektif-pertahun{
		text-align: center;
	}
	.row-page-module {
	    background-color: #f7f7f7;
	    padding: 10px;
	}
	.tbl-sbdk{
		width: 100%;
	}
	.tbl-sbdk tr td{
		text-align: center;
		background: #253D8C; 
		color: #ffffff;
		border: 1px solid #C3D3DF;
    	padding: 8px 0;
	}
	.tbl-sbdk tr.value-sbdk td{
		color: #212529;
		background: #fff;
	}

	.garis-putus{
		border-top: 4px dashed #84B4DF;
	}

	.keterangan-sbdk{
		text-align: left;
		color: #535353;
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
		.content-sbdk {
		    max-width: 100%;
		}
		.content-sbdk .title-sbdk{
			margin-top: 1rem; 
		}
		.row-page-module{
			display: none;
		}
		.row.row-header{
			text-align: center;
		}
	}

	.row-socialize{
		padding-bottom: 30px;
	}
</style>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $this->lang->line('sbdk-2') ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<label style="color:#707070"><?php echo $this->lang->line('home') ?></label> /
		<label style="color:#707070"><?php echo $this->lang->line('sbdk-2') ?></label> /
		<label style="color:#707070">SBDK</label>
	</div>
</div>

<div class="container-fluid d-padding">
	<div class="row d-padtop-10">
		<div class="col-lg-3">
			<?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
		</div>
		<div class="col-lg-8">
			<div class="content-sbdk">
				<h3 class="mb-4" style="font-size: 18px;text-align: left;"><strong><?php echo $this->lang->line('sbdk-8') ?></strong></h3>

				<p class="title-sbdk"><?php echo $this->lang->line('sbdk-7') ?> <br> P.T. BANK BUMI ARTA, TBK.</p>
				<p class="efektif-pertahun">
					<?php echo $this->lang->line('sbdk-6') ?>
				</p>

				<table class="tbl-sbdk">
					<tr>
						<td colspan="5">SUKU BUNGA DASAR KREDIT RUPIAH (PRIME LENDING RATE)</td>
					</tr>
					<tr>
						<td colspan="5"><?php echo $this->lang->line('berdasarkan_segmen') ?></td>
					</tr>
					<tr>
						<td rowspan="2"><?php echo $this->lang->line('kredit_korporasi') ?></td>
						<td rowspan="2"><?php echo $this->lang->line('kredit_retail') ?></td>
						<td rowspan="2"><?php echo $this->lang->line('kredit_mikro') ?></td>
						<td colspan="2"><?php echo $this->lang->line('kredit_konsumsi') ?></td>
					</tr>
					<tr>
						<td><?php echo $this->lang->line('kpr') ?></td>
						<td><?php echo $this->lang->line('non_kpr') ?></td>
					</tr>
					<tr class="value-sbdk">
						<td><?php echo $contentData['msSbdk']['CORPORATE_LOAN'] ?></td>
						<td><?php echo $contentData['msSbdk']['RETAIL_LOAN'] ?></td>
						<td><?php echo $contentData['msSbdk']['MICRO_LOAN'] ?></td>
						<td><?php echo $contentData['msSbdk']['MORTGAGE'] ?></td>
						<td><?php echo $contentData['msSbdk']['NONMORTGAGE'] ?></td>
					</tr>
				</table>

				<p style="font-weight: bold;" class="effective-date mt-3">Per 
					<?php 
						// echo date('d F Y', strtotime($contentData['msSbdk']['EFFECTIVE_DATE']));
						echo convertDateLang($contentData['msSbdk']['EFFECTIVE_DATE'], 'dmy');

					?>
				</p>

				<div class="garis-putus"></div>

				<div class="keterangan-sbdk mt-4" style="color: #212529;text-align: justify;">
					<?php echo  $this->lang->line('keterangan_sbdk') ?>
				</div>

				<style>
					.row-socialize{
						float: right;
						text-align: left;
						margin-top: 1rem;
					}
					@media only screen and (min-width: 200px) and (max-width: 1024px) {
						.row-socialize{
							float: unset;
							text-align: center;
						}
					}
				</style>

				<div class="row-socialize">
					<p style="color: #243E8B"><?php echo $this->lang->line('share') ?></p>
					<?php
					// echo '<pre>'; print_r($data); echo '</pre>';
						$q = $this->db->query("SELECT * FROM MS_SOCIAL_MEDIA WHERE STATUS = '99'")->result();
						if(!empty($q)){
							$url = '';
							foreach($q as $val):
								if($val->NAME == 'Twitter'){
									$url = 'https://twitter.com/share?url='.base_url(uri_string());
								}else if($val->NAME == 'Facebook'){
									$url = 'http://www.facebook.com/sharer.php?u='.base_url(uri_string());
								}else if($val->NAME == 'LinkedIn'){
									$url = 'http://www.linkedin.com/shareArticle?mini=true&amp;url='.base_url(uri_string());
								}else if($val->NAME == 'Google'){
									$url = 'https://plus.google.com/share?url='.base_url(uri_string());
								}else{
									$url = '';
								}
					?>
						<a href="<?php echo $url; ?>" target="_blank"><img src="<?php echo $val->LINK; ?>" style="width: 25px; height: 25px; "></a>&nbsp;
					<?php
							endforeach;
						}

						// echo $url;
					?>
				</div>
				
			</div>
		</div>
	</div>
</div>