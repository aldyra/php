<style type="text/css">
	img.img-footer-bisnis{
		height: 40px;
		width: auto;
	}

	img.img-logo-referal{
		height: 50px;
		width: auto;
	}

	img.icon-socialize{
		margin-top: 15px;
		margin-left: 5px;
		margin-right: 5px;
		height: 30px;
		width: 30px;
	}

	.sosmed_desktop{
		display: block;
	}

	.sosmed_mobile{
		display: none;
	}

	@media all and (min-width: 992px) {
		.logo-otoritas {
			width: 85%;
			height: auto;
		}

		.title-footer-1{
			font-family: 'Nunito-Bold';
			font-weight: 500; 
			font-size: 20px;
		}
	}
	@media all and (max-width: 992px) {
		.logo-otoritas {
			width: 105%;
			height: auto;
		}

		.title-footer-1{
			font-family: 'Nunito-Bold';
			font-weight: 500; 
			font-size: 18px;
		}
	}

	@media only screen and (min-width: 200px) and (max-width: 1024px) {
        .txt_responsive_18{
        	font-size: 18px !important;
        }
        div.footer-location .txt_alamat{
        	font-size: 14px;
        }
        div.footer-location .txt_alamat .txt_nama_pt{
        	font-size: 16px;
        	margin-bottom: .5rem;
    		display: inline-block;
        }
        .telp_atm{
        	font-size: 14px;
        }
        div.row-footer-module {
		    padding-top: 2.5rem;
		    text-align: center;
		    font-size: 12px;
		    /*margin-top: .5rem;*/
		    margin-bottom: .5rem;
		}
		div.row-copyright{
			background-color: transparent !important;
		}
		div.row-copyright span{
			font-size: 12px !important;
			color: #243E8B !important;
		}

		.sosmed_desktop{
			display: none;
		}
		.sosmed_mobile{
			display: block;
		}
    }
</style>

<div class="footer" style="display: contents !important;">
	<div class="row">
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 d-martop-10 footer-location">
			<b class="t-default txt_responsive_18" style="line-height: 3; font-family: 'Nunito-Bold';font-weight: 500; font-size: 20px;">
				<?php echo $this->lang->line('footer-0'); ?>
			</b>
			<br>
			<h6 class="f-black txt_alamat">
				<b class="f-bold txt_nama_pt" style="font-family: 'Nunito-Bold';font-weight: 500;">P.T. Bank Bumi Arta Tbk.</b><br>
				<span>Jl. Wahid Hasyim No. 234</span><br>
				<span>Jakarta Pusat 10250</span><br>
				<span>Indonesia</span><br><br>
				<span>Phone &nbsp;: (021) 2300893 / (021) 2300455</span><br>
				<span>Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: (021)2303623</span>
			</h6>
			<p class="sosmed_desktop" style="padding-top: 20px;">
				<?php 
				foreach($social_media as $row){
					$LINK = isset($row['LINK']) ? $row['LINK']:"";
					$URL_SOCIAL_MEDIA = isset($row['URL_SOCIAL_MEDIA']) ? $row['URL_SOCIAL_MEDIA']:"";
				?>
					<a href="<?php echo $URL_SOCIAL_MEDIA?>" style="text-decoration: none; cursor: pointer; ">
						<img class="icon-socialize" src="<?php echo $LINK ?>">
					</a>
				<?php
				}
				?>
			</p>
		</div>
		<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 d-martop-10" align="center">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-martop-10 row-otoritas" style="padding-left: 0; padding-right: 0;">
					<?php 
					if($_SESSION['site_lang'] === "id"){
						$Url = base_url('assets/image/logo-otoritas-new.png');
					}elseif($_SESSION['site_lang'] === "en"){
						$Url = base_url('assets/image/logo-otoritas-en.png');
					}
					?>
					<img class="logo-otoritas" src="<?php echo $Url?>">
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 d-martop-10">
			<b class="t-default txt_responsive_18" style="line-height: 3; font-family: 'Nunito-Bold';font-weight: 500; font-size: 20px;">Download Mobile Banking</b><br>
			<a href="https://bit.ly/2K1XMOw">
				<img class="img-fluid img-footer-bisnis " src="<?php echo base_url('assets/image/googleplay.png')?>">
			</a>
			&nbsp;
			<a href="https://apple.co/2rs80kU">
				<img class="img-fluid img-footer-bisnis" src="<?php echo base_url('assets/image/appsstore.png')?>">
			</a>
			<br>
			<div style="text-align: left; color: black;">
				<span style="line-height: 3;" class="t-default txt_responsive_18">
					<b class="title-footer-1"><?php echo $this->lang->line('footer-1'); ?></b>
				</span><br>
				<span>
					<img src="<?php echo base_url('assets/image/atm-machine.png') ?>" style="width: 17px; height: 17px;">
					&nbsp;<b>ATM</b>
				</span><br>
				<span class="telp_atm" style="padding-left: 25px;">021 314 2121 (Jakarta)</span><br>
				<span class="telp_atm" style="padding-left: 25px;">0804 140 1221 (Luar Jakarta)</span><br>
				<span style="line-height: 3;">
					<img src="<?php echo base_url('assets/image/briefcase.png') ?>" style="width: 17px; height: 17px;">
					&nbsp;<b><a href="https://ebanking.bankbba.co.id" style="color: black;">Internet Banking Corporate</a></b>
				</span><br>
				<span>
					<img src="<?php echo base_url('assets/image/user.png') ?>" style="width: 17px; height: 17px;">
					&nbsp;<b><a href="https://epersonal.bankbba.co.id" style="color: black;">e-Personal BBA</a></b>
				</span><br>
			</div>
		</div>
	</div>
	<p class="sosmed_mobile">
		<?php 
		foreach($social_media as $row){
			$LINK = isset($row['LINK']) ? $row['LINK']:"";
			$URL_SOCIAL_MEDIA = isset($row['URL_SOCIAL_MEDIA']) ? $row['URL_SOCIAL_MEDIA']:"";
		?>
			<a href="<?php echo $URL_SOCIAL_MEDIA?>" style="text-decoration: none; cursor: pointer;">
				<img class="icon-socialize" src="<?php echo $LINK ?>">
			</a>
		<?php
		}
		?>
	</p>
	<div class="row row-footer-module d-padtop-10" style="margin-top: -10px;">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<?php 
			$LanguageActive = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
			$i = 0;
			$max = sizeof($nav_menu_footer)-1;
			foreach($nav_menu_footer as $row){
				$NAME = isset($row['NAME']) ? $row['NAME']:"";
				$LINK = isset($row['LINK']) ? $row['LINK']:"";
				$PARENT = isset($row['PARENT']) ? $row['PARENT']:"";
				$Url = base_url().$LanguageActive."/".$LINK;
			?>
				<a style="color: #707070;" href="<?php echo $Url ?>"><?php echo $NAME ?></a>
				<?php 
				if($i != $max){
				?>
					<span class="seperator"></span>
			<?php	
				}
			$i++;
			}
			?>
		</div>
	</div>
</div>