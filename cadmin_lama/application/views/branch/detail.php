<?php
$data = isset($data) ? $data : "";
$ID_BRANCH = isset($data['branch']->ID_BRANCH) ? $data['branch']->ID_BRANCH : '';
$BRANCH_NAME = isset($data['branch']->BRANCH_NAME) ? $data['branch']->BRANCH_NAME : '';
$PHONE_NUMBER = isset($data['branch']->PHONE_NUMBER) ? $data['branch']->PHONE_NUMBER : '';
$FAX_NUMBER = isset($data['branch']->FAX_NUMBER) ? $data['branch']->FAX_NUMBER : '';
$WEBSITE = isset($data['branch']->WEBSITE) ? $data['branch']->WEBSITE : '';
$EMAIL = isset($data['branch']->EMAIL) ? $data['branch']->EMAIL : '';
$ADDRESS = isset($data['branch']->ADDRESS) ? $data['branch']->ADDRESS : '';
$LONGITUDE = isset($data['branch']->LONGITUDE) ? $data['branch']->LONGITUDE : '';
$LATITUDE = isset($data['branch']->LATITUDE) ? $data['branch']->LATITUDE : '';
$CATEGORY = isset($data['branch']->CATEGORY) ? $data['branch']->CATEGORY : '';
$PROVINCE_NAME = isset($data['branch']->PROVINCE_NAME) ? $data['branch']->PROVINCE_NAME : '';
$CITY_KAB_NAME = isset($data['branch']->CITY_KAB_NAME) ? $data['branch']->CITY_KAB_NAME : '';
$KECAMATAN_NAME = isset($data['branch']->KECAMATAN_NAME) ? $data['branch']->KECAMATAN_NAME : '';
$KELURAHAN_NAME = isset($data['branch']->KELURAHAN_NAME) ? $data['branch']->KELURAHAN_NAME : '';
$POSTAL_CODE = isset($data['branch']->POSTAL_CODE) ? $data['branch']->POSTAL_CODE : '';
$STATUS = isset($data['branch']->STATUS) ? $data['branch']->STATUS : "";
$CREATED_BY = isset($data['branch']->CREATED_BY) ? $data['branch']->CREATED_BY : "";
$CREATED_DATE = isset($data['branch']->CREATED_DATE) ? $data['branch']->CREATED_DATE : "";
if ($STATUS == "0") {
    $STS = "<span class='badge badge-danger'>Inactive</span>";
} elseif ($STATUS == "88") {
    $STS = "<span class='badge badge-success'>Active</span>";
} elseif ($STATUS == "99") {
    $STS = "<span class='badge badge-primary'>Published</span>";
} else {
    $STS = "";
}
if ($CATEGORY == "1") {
    $CATEG = "Kantor Pusat/Head Office";
} elseif ($CATEGORY == "2") {
    $CATEG = "Kantor Cabang/Branch Office";
} elseif ($CATEGORY == "3") {
    $CATEG = "Kantor Capem/Sub Branch Office";
} elseif ($CATEGORY == "4") {
    $CATEG = "Kantor Kas/Cash Offices";
} elseif ($CATEGORY == "5") {
    $CATEG = "Payment Point";
} else {
    $CATEG = "";
}

$rups_detail = isset($data['detail']) ? $data['detail'] : array();
$back = isset($data['back']) ? $data['back'] : '';
?>
<script src="<?php echo base_url() ?>assets/js/demo_pages/blog_single.js"></script>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master Branch</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Branch</span> - Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <div class="card">
        <div class="card-body">
            <fieldset>
                <legend class="font-weight-semibold text-uppercase font-size-sm">
                    <i class="icon-file-text2 mr-2"></i>
                    Branch Information
                    <a href="#" class="float-right text-default" data-toggle="collapse" data-target="#demo1">
                        <i class="icon-circle-down2"></i>
                    </a>
                </legend>

                <div class="collapse show" id="demo1">
                    <div class="form-group">
                        <input type="hidden" name="id" id="id" value="<?php echo $ID_BRANCH ?>">
                        <label>Branch Name</label>
                        <input type="text" class="form-control" id="branch_name" name="branch_name"
                            placeholder="Branch Name" required="" parsley-trigger="change"
                            value="<?php echo $BRANCH_NAME ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Category Branch</label>
                        <input type="text" class="form-control" value="<?php echo $CATEG ?>" readonly>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" id="phone_number" name="phone_number"
                                    placeholder="Phone Number" onkeyup="validate(this);" required=""
                                    parsley-trigger="change" value="<?php echo $PHONE_NUMBER ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fax Number</label>
                                <input type="text" class="form-control" id="fax_number" name="fax_number"
                                    placeholder="Fax Number" onkeyup="validate(this);" required=""
                                    parsley-trigger="change" value="<?php echo $FAX_NUMBER ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Website</label>
                                <input type="url" class="form-control" id="website" name="website" placeholder="Website"
                                    parsley-trigger="change" value="<?php echo $WEBSITE ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                    parsley-trigger="change" value="<?php echo $EMAIL ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend class="font-weight-semibold text-uppercase font-size-sm">
                    <i class="icon-reading mr-2"></i>
                    Branch Details
                    <a class="float-right text-default" data-toggle="collapse" data-target="#demo2">
                        <i class="icon-circle-down2"></i>
                    </a>
                </legend>

                <div class="collapse show" id="demo2">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea class="form-control" name="address" id="ADDRESS" rows="3" required=""
                                    parsley-trigger="change" readonly><?php echo $ADDRESS ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Longitude</label>
                                <input type="text" class="form-control" name="longitude" id="LONGITUDE"
                                    data-parsley-pattern="/^[0-9_ .,-]*$/" data-parsley-trigger="focusin focusout"
                                    required="" placeholder="" value="<?php echo $LONGITUDE ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Latitude</label>
                                <input type="text" class="form-control" name="latitude" id="LATITUDE"
                                    data-parsley-pattern="/^[0-9_ .,-]*$/" data-parsley-trigger="focusin focusout"
                                    placeholder="" required value="<?php echo $LATITUDE ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Province</label>
                                <input type="text" class="form-control" id="PROVINCE_ID" name="province_id"
                                    value="<?php echo $PROVINCE_NAME ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" id="CITY_KAB_ID" name="city_kab_id"
                                    value="<?php echo $CITY_KAB_NAME ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <input type="text" class="form-control" id="KECAMATAN_ID" name="kecamatan_id"
                                    value="<?php echo $KECAMATAN_NAME ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Kelurahan</label>
                                <input type="text" class="form-control" id="KELURAHAN_ID" name="kelurahan_id"
                                    value="<?php echo $KELURAHAN_NAME ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Postal Code</label>
                                <input type="text" class="form-control" name="postal_code" id="POSTAL_CODE"
                                    placeholder="" readonly value="<?php echo $POSTAL_CODE ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="text-right">
                <a href="<?php echo $back ?>"><button type="button" class="btn btn-secondary waves-effect waves-light">
                        Back</button></a>

            </div>
        </div>
    </div>
</div>