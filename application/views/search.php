<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$Url = base_url().$SiteLang."/";
?>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important;"><?php echo ucwords($this->lang->line('search-0')) ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<a href="<?php echo base_url(); ?>" style="color: #707070;"><?php echo $this->lang->line('home') ?></a> / 
		<label style="color:#707070"><?php echo ucwords($this->lang->line('search-0')) ?></label>
	</div>
</div>

<div class="container-fluid" style="padding-bottom: 50px;">
	<div class="row d-padtop-10">
		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 d-padtop-10">
			<form action="<?php echo base_url().$SiteLang.'/'.$this->lang->line('search-0').'/index/' ?>" method="get" role="search" novalidate="novalidate">
				<div class="input-group">
					<input type="search" class="form-control rounded" placeholder="<?php echo $this->lang->line('search-2') ?>" aria-label="Search"
					aria-describedby="search-addon" style="border-radius: 0 !important;" value="<?php echo $keywords ?>" name="search"/>
					<button type="submit" class="btn btn-outline-primary" style="border-radius: 0 !important;" >
						<?php echo ucwords($this->lang->line('search-0')) ?>
					</button>
				</div>
			</form>
			<div class="row" style="padding-top: 25px;">
				<div class="table-responsive">
					<table class="table table-search">
						<thead>
							<tr>
								<th>
									<?php echo $this->lang->line('search-5')?>
								</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						foreach($news as $row){
							$ID = isset($row->ID) ? $row->ID:"";
							$NAME = isset($row->NAME) ? $row->NAME:"";
							$LINK = isset($row->NAME) ? $row->LINK:"";
							$CREATED_DATE = isset($row->CREATED_DATE) ? $row->CREATED_DATE:"";
							$CREATED_BY = isset($row->CREATED_BY) ? $row->CREATED_BY:"";
						?>
							<!-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding"> -->
							<tr>
								<td>
									<h5>
										<a href="<?php echo base_url(). $SiteLang ?>/<?php echo $LINK ?>" style="text-decoration: none;">
											<b><?php echo $NAME ?></b>
										</a>
									</h5>
									<label style="font-size: 12px;"><?php echo base_url(). $SiteLang ?>/<?php echo $LINK ?></label>
									<p style="font-size: 12px"><?php echo $this->lang->line('search-3')." : ".date("d F Y H:i:s", strtotime($CREATED_DATE)) ?></p>
									<hr width="100%">
								</td>
							</tr>
							<!-- </div> -->
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12 d-padtop-10">
			<p><h4><b><?php echo $this->lang->line('search-6')?></b></h4></p>
			<?php
			foreach($video as $row){
				$ID = isset($row->TITLE) ? $row->ID:"";
				$TITLE = isset($row->TITLE) ? $row->TITLE:"";
				$DESCRIPTION = isset($row->DESCRIPTION) ? $row->DESCRIPTION:"";
				$LINK = isset($row->LINK) ? $row->LINK:"";
			?>
			 	<video controls autoplay style="width: 100%; height: 200px;" src="<?php echo $LINK ?>"></video> 
			 	<p><?php echo $DESCRIPTION ?></p>
			 	<hr width="100%">
			<?php 
			} 
			?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    $('.table-search').DataTable({
	    	'ordering':false,
	    	'info':false,
	    	'searching':false,
	    	'lengthChange' : false,
	    });
	} );
</script>