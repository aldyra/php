<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model');
        $this->load->library('upload');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "menu/view";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['menu'] = $this->menu_model->view();
        $data['data']['datamenu'] = $this->get_menu();
        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Image max pixel ' . $this->config->item('max_height_menu') . 'x' . $this->config->item('max_width_menu') . 'px (Recomended size are 1280x240px)<br>
                    Allowed images are ' . str_replace("|", ", ", $this->config->item('allowed_types_image')) . ' <br>
                    Max image size ' . $this->config->item('max_size_menu') . 'KB<br>';
        echo json_encode($data);
    }
    public function detail_menu($id)
    {
        $data = array();
        $data['content'] = "menu/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['menu'] = $this->menu_model->get_edit($id);
        $data['data']['back'] = base_url() . 'menu';
        $this->load->view('templates/view', $data);
    }
    public function add_menu()
    {
        $data = array();
        $data['content'] = "menu/add_menu";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['menu'] = $this->menu_model->view();
        $data['data']['datamenu'] = $this->get_menu();
        $data['data']['back'] = base_url() . 'menu';
        $this->load->view('templates/view', $data);
    }
    public function edit_menu($id)
    {
        $data = array();
        $data['content'] = "menu/edit_menu";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['menu'] = $this->menu_model->view();
        $data['data']['dataedit'] = $this->menu_model->get_edit($id);
        $data['data']['back'] = base_url() . 'menu';
        $this->load->view('templates/view', $data);
    }

    public function get_menu()
    {
        $data_menu = $this->menu_model->get();
        $result = array();
        foreach ($data_menu as $data) {
            $param['MENU_PARENT'] = $data->MENU_PARENT;
            $param['ID'] = $data->ID;
            $param['NAME_ID'] = $data->NAME_ID;
            $param['NAME_EN'] = $data->NAME_EN;
            $param['MENU_LEVEL'] = $data->MENU_LEVEL;

            $result[] = $param;
        }
        return $result;
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return '#';
        }

        return $text;
    }

    public function add()
    {
        $NAME_ID = isset($_POST['name_id']) ? $_POST['name_id'] : "";
        $NAME_EN = isset($_POST['name_en']) ? $_POST['name_en'] : "";
        $MENU_PARENT = isset($_POST['menu_parent']) ? $_POST['menu_parent'] : "";
        $MENU_LEVEL = isset($_POST['menu_level']) ? $_POST['menu_level'] : "0";
        $PARENT = isset($_POST['parent']) ? $_POST['parent'] : "0";
        $POSITION = isset($_POST['position']) ? $_POST['position'] : "0";
        $SEQUENCE_HEADER = isset($_POST['sequence_header']) ? $_POST['sequence_header'] : "0";
        $SEQUENCE_FOOTER = isset($_POST['sequence_footer']) ? $_POST['sequence_footer'] : "0";
        $FOOTER_NOTE_EN = isset($_POST['footer_note_en']) ? $_POST['footer_note_en'] : "";
        $FOOTER_NOTE_ID = isset($_POST['footer_note_id']) ? $_POST['footer_note_id'] : "";

        $IS_CONTENT = isset($_POST['is_content']) ? $_POST['is_content'] : "0";
        $HEADER_BANNER = isset($_POST['header_banner']) ? $_POST['header_banner'] : "0";

        // $LINK_EN = strtolower(str_replace(' ', '', $this->slugify($NAME_EN)));
        // $LINK_ID = strtolower(str_replace(' ', '', $this->slugify($NAME_ID)));
        $LINK_EN = strtolower(isset($_POST['link_en']) ? str_replace(' ', '', $_POST['link_en']) : "");
        $LINK_ID = strtolower(isset($_POST['link_id']) ? str_replace(' ', '', $_POST['link_id']) : "");

        $FILE_ID = isset($_FILES["file_id"]['name']) ? $_FILES["file_id"]['name'] : "";
        $FILE_EN = isset($_FILES["file_en"]['name']) ? $_FILES["file_en"]['name'] : "";
        $FILE_BANNER_ID = '';
        $FILE_BANNER_EN = '';

        if ($PARENT == "menu_child") {
            if (empty($MENU_PARENT)) {
                $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Menu Parent is required!");
                die(json_encode($JSON));

            }
            $MENU_LEVEL = $MENU_LEVEL + 1;
        }
        $check_duplicate_id = $this->menu_model->checkDuplicate('LINK_ID', $LINK_ID);
        $check_duplicate_en = $this->menu_model->checkDuplicate('LINK_EN', $LINK_EN);

        if ($check_duplicate_id && $check_duplicate_en) {

            if (!empty($FILE_ID) && !empty($FILE_EN)) {
                if (empty($FILE_ID)) {
                    $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File is required!");
                    die(json_encode($JSON));
                }
                if (empty($FILE_EN)) {
                    $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File is required!");
                    die(json_encode($JSON));
                }

                $pathmenu = $this->config->item('save_menu');
                $config['allowed_types'] = $this->config->item('allowed_types');
                $config['max_size'] = $this->config->item('max_size_menu');
                $config['max_width'] = $this->config->item('max_width_menu');
                $config['min_width'] = $this->config->item('min_width_menu');
                $config['max_height'] = $this->config->item('max_height_menu');
                $config['min_height'] = $this->config->item('min_height_menu');
                $config['upload_path'] = $this->config->item('path_menu');

                $path_parts_en = pathinfo($_FILES["file_en"]["name"]);

                $path_parts_id = pathinfo($_FILES["file_id"]["name"]);

                $forUpload['extension_en'] = $path_parts_en['extension'];
                $forUpload['extension_id'] = $path_parts_id['extension'];

                $Extension = $this->config->item('allowed_types_image');
                $Expfile = explode("|", $Extension);

                if (in_array(strtoupper($forUpload['extension_id']), $Expfile)) {
                    $config['file_name'] = 'HEADER_BANNER_ID_' . date('ymdHis');

                    $this->upload->initialize($config);
                    $forUpload['file_name_id'] = $config['file_name'];
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }
                    if (!$this->upload->do_upload('file_id')) {
                        $error = array('error' => $this->upload->display_errors());
                        $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                        die(json_encode($JSON));
                    }
                } else {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                    die(json_encode($JSON));
                }
                if (in_array(strtoupper($forUpload['extension_en']), $Expfile)) {
                    $config['file_name'] = 'HEADER_BANNER_EN_' . date('ymdHis');

                    $this->upload->initialize($config);
                    $forUpload['file_name_en'] = $config['file_name'];
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }
                    if (!$this->upload->do_upload('file_en')) {
                        $error = array('error' => $this->upload->display_errors());
                        $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                        die(json_encode($JSON));
                    }
                } else {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                    die(json_encode($JSON));
                }
                $FILE_BANNER_ID = $pathmenu . $forUpload['file_name_id'] . "." . $forUpload['extension_id'];
                $FILE_BANNER_EN = $pathmenu . $forUpload['file_name_en'] . "." . $forUpload['extension_en'];

            }

            $param['FILE_BANNER_ID'] = $FILE_BANNER_ID;
            $param['FILE_BANNER_EN'] = $FILE_BANNER_EN;
            $param['HEADER_BANNER'] = $HEADER_BANNER;
            $param['NAME_ID'] = $NAME_ID;
            $param['NAME_EN'] = $NAME_EN;
            $param['LINK_ID'] = $LINK_ID;
            $param['LINK_EN'] = $LINK_EN;
            $param['IS_CONTENT'] = $IS_CONTENT;
            $param['MENU_LEVEL'] = $MENU_LEVEL;
            $param['MENU_PARENT'] = $MENU_PARENT;
            $param['POSITION'] = $POSITION;
            $param['FOOTER_NOTE_ID'] = $FOOTER_NOTE_ID;
            $param['FOOTER_NOTE_EN'] = $FOOTER_NOTE_EN;
            $param['SEQUENCE_HEADER'] = $SEQUENCE_HEADER;
            $param['SEQUENCE_FOOTER'] = $SEQUENCE_FOOTER;
            $param['CREATED_BY'] = $_SESSION['username'];
            $param['CREATED_DATE'] = date('Y-m-d H:i:s');
            $Insert = $this->menu_model->add($param);
            if ($Insert['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
            die(json_encode($JSON));
        } else {
            $JSON = array("ErrorCode" => "EC:000D", "ErrorMessage" => "Failed: Duplicate Link");
            die(json_encode($JSON));

        }
    }

    public function view()
    {
        $GetData = $this->menu_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID = isset($_POST['id']) ? $_POST['id'] : "";
        $NAME_ID = isset($_POST['name_id']) ? $_POST['name_id'] : "";
        $NAME_EN = isset($_POST['name_en']) ? $_POST['name_en'] : "";
        $MENU_PARENT = isset($_POST['menu_parent']) ? $_POST['menu_parent'] : "";
        $MENU_LEVEL = isset($_POST['menu_level']) ? $_POST['menu_level'] : "0";
        $PARENT = isset($_POST['parent']) ? $_POST['parent'] : "0";
        $IS_CONTENT = isset($_POST['is_content']) ? $_POST['is_content'] : "0";
        $POSITION = isset($_POST['position']) ? $_POST['position'] : "0";
        $SEQUENCE_HEADER = isset($_POST['sequence_header']) ? $_POST['sequence_header'] : "0";
        $SEQUENCE_FOOTER = isset($_POST['sequence_footer']) ? $_POST['sequence_footer'] : "0";
        $FOOTER_NOTE_EN = isset($_POST['footer_note_en']) ? $_POST['footer_note_en'] : "";
        $FOOTER_NOTE_ID = isset($_POST['footer_note_id']) ? $_POST['footer_note_id'] : "";
        $FILE_ID = isset($_FILES["file_id"]['name']) ? $_FILES["file_id"]['name'] : "";
        $FILE_EN = isset($_FILES["file_en"]['name']) ? $_FILES["file_en"]['name'] : "";
        $FILE_EN_URL = isset($_POST['file_en_url']) ? $_POST['file_en_url'] : "";
        $FILE_ID_URL = isset($_POST['file_id_url']) ? $_POST['file_id_url'] : "";

        $IS_CONTENT = isset($_POST['is_content']) ? $_POST['is_content'] : "0";
        $HEADER_BANNER = isset($_POST['header_banner']) ? $_POST['header_banner'] : "0";

        // $LINK_EN = strtolower(str_replace(' ', '', $this->slugify($NAME_EN)));
        // $LINK_ID = strtolower(str_replace(' ', '', $this->slugify($NAME_ID)));
        $LINK_EN = strtolower(isset($_POST['link_en']) ? str_replace(' ', '', $_POST['link_en']) : "");
        $LINK_ID = strtolower(isset($_POST['link_id']) ? str_replace(' ', '', $_POST['link_id']) : "");

        if ($PARENT == "menu_child") {
            if (empty($MENU_PARENT)) {
                $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Menu Parent is required!");
                die(json_encode($JSON));

            }
            $MENU_LEVEL = $MENU_LEVEL + 1;
        }
        $check_duplicate_id = $this->menu_model->checkDuplicate('LINK_ID', $LINK_ID, 'ID', $ID);
        $check_duplicate_en = $this->menu_model->checkDuplicate('LINK_ID', $LINK_ID, 'ID', $ID);

        if ($check_duplicate_id && $check_duplicate_en) {

            $pathmenu = $this->config->item('save_menu');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size_menu');
            $config['max_width'] = $this->config->item('max_width_menu');
            $config['min_width'] = $this->config->item('min_width_menu');
            $config['max_height'] = $this->config->item('max_height_menu');
            $config['min_height'] = $this->config->item('min_height_menu');
            $config['upload_path'] = $this->config->item('path_menu');
            $Extension = $this->config->item('allowed_types_image');
            $Expfile = explode("|", $Extension);

            if (!empty($FILE_ID)) {
                if (empty($FILE_ID)) {
                    $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File Indonesia is required!");
                    die(json_encode($JSON));
                }
                $config['file_name'] = 'HEADER_BANNER_ID_' . date('ymdHis');

                $path_parts_id = pathinfo($_FILES["file_id"]["name"]);
                $forUpload['extension_id'] = $path_parts_id['extension'];

                if (in_array(strtoupper($forUpload['extension_id']), $Expfile)) {
                    $this->upload->initialize($config);
                    $forUpload['file_name_id'] = $config['file_name'];
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }

                    if (!$this->upload->do_upload('file_id')) {
                        $error = array('error' => $this->upload->display_errors());
                        $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                        die(json_encode($JSON));
                    } else {
                        $FILE_ID_URL = $pathmenu . $forUpload['file_name_id'] . "." . $forUpload['extension_id'];
                    }
                } else {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                    die(json_encode($JSON));
                }

            }
            if (!empty($FILE_EN)) {
                if (empty($FILE_EN)) {
                    $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File English is required!");
                    die(json_encode($JSON));
                }
                $config['file_name'] = 'HEADER_BANNER_EN_' . date('ymdHis');

                $path_parts_en = pathinfo($_FILES["file_en"]["name"]);
                $forUpload['extension_en'] = $path_parts_en['extension'];

                if (in_array(strtoupper($forUpload['extension_en']), $Expfile)) {
                    $this->upload->initialize($config);
                    $forUpload['file_name_en'] = $config['file_name'];
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, true);
                    }

                    if (!$this->upload->do_upload('file_en')) {
                        $error = array('error' => $this->upload->display_errors());
                        $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                        die(json_encode($JSON));
                    } else {
                        $FILE_EN_URL = $pathmenu . $forUpload['file_name_en'] . "." . $forUpload['extension_en'];
                    }
                } else {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                    die(json_encode($JSON));
                }

            }

            $param['FILE_BANNER_ID'] = $FILE_ID_URL;
            $param['FILE_BANNER_EN'] = $FILE_EN_URL;
            $param['HEADER_BANNER'] = $HEADER_BANNER;
            $param['ID'] = $ID;
            $param['NAME_ID'] = $NAME_ID;
            $param['NAME_EN'] = $NAME_EN;
            $param['LINK_ID'] = $LINK_ID;
            $param['LINK_EN'] = $LINK_EN;
            $param['IS_CONTENT'] = $IS_CONTENT;
            $param['MENU_LEVEL'] = $MENU_LEVEL;
            $param['MENU_PARENT'] = $MENU_PARENT;
            $param['POSITION'] = $POSITION;
            $param['FOOTER_NOTE_ID'] = $FOOTER_NOTE_ID;
            $param['FOOTER_NOTE_EN'] = $FOOTER_NOTE_EN;
            $param['SEQUENCE_HEADER'] = $SEQUENCE_HEADER;
            $param['SEQUENCE_FOOTER'] = $SEQUENCE_FOOTER;
            $param['USER_LOG'] = $_SESSION['username'];
            $param['DATE_LOG'] = date('Y-m-d H:i:s');
            $Update = $this->menu_model->updateaction($param);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        } else {
            $JSON = array("ErrorCode" => "EC:000D", "ErrorMessage" => "Failed: Duplicate Link");
            die(json_encode($JSON));

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->menu_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "0";
        $where = array('ID' => $id);
        $Active = $this->menu_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "88";
        $where = array('ID' => $id);
        $Active = $this->menu_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "99";
        $where = array('ID' => $id);
        $Active = $this->menu_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }

}