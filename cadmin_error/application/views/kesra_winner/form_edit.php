<?php
$param = isset($param) ? $param : "";
$ID_KESRA_WINNER = isset($param['ID_KESRA_WINNER']) ? $param['ID_KESRA_WINNER'] : "";
$PERIODE = isset($param['PERIODE']) ? $param['PERIODE'] : "";
$DATE = isset($param['DATE']) ? $param['DATE'] : "";
$LINK = isset($param['LINK']) ? $param['LINK'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
?>
<div class="row">
    <input type="hidden" name="id" id="id" value="<?php echo $ID_KESRA_WINNER ?>">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        PERIODE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" name="periode" class="form-control" id="periode" value="<?php echo $PERIODE ?>">
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        DATE<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="input-group">
            <span class="input-group-prepend">
                <span class="input-group-text"><i class="icon-calendar5"></i></span>
            </span>
            <input type="text" name="date" id="date" class="form-control pickadate-selectors"
                placeholder="Select Date.." data-value="<?php echo $DATE ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        DOCUMENT<span style="color:orange">*</span>
    </div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="file" id="file" name="file" class="dropify" value="<?php echo $LINK ?>"
            data-default-file="<?php echo $LINK ?>" />
        <a href="#" onclick="openTnC('<?php echo base_url('kesra_winner/tncupload') ?>')">Terms and Conditions
            Upload</a>

    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();

    changeformfile();
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });

});
</script>