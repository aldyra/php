<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Document extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('document_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "document/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['document'] = $this->document_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Allowed files are ' . str_replace("|", ", ", $this->config->item('allowed_types_file')) . ' <br>
                    Max file size ' . $this->config->item('max_size_document') . 'KB<br>';
        echo json_encode($data);
    }

    public function add()
    {
        $TITLE = isset($_POST['title']) ? $_POST['title'] : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($TITLE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        } elseif (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
            die(json_encode($JSON));
        }

        $pathdocument = $this->config->item('save_document');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size_document');
        $config['max_width'] = $this->config->item('max_width_document');
        $config['min_width'] = $this->config->item('min_width_document');
        $config['max_height'] = $this->config->item('max_height_document');
        $config['min_height'] = $this->config->item('min_height_document');
        $config['file_name'] = 'DOCUMENT_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_document');
        $path_parts = pathinfo($_FILES["file"]["name"]);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_file');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $param['TITLE'] = $TITLE;
                $param['LINK'] = $pathdocument . $config['file_name'] . "." . $forUpload['extension'];
                $param['CREATED_BY'] = $_SESSION['username'];
                $Insert = $this->document_model->add($param);
                if ($Insert['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                    die(json_encode($JSON));
                }
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->document_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        $ID = isset($_POST['id_document']) ? $_POST['id_document'] : "";
        $TITLE = isset($_POST['title']) ? $_POST['title'] : "";
        $STATUS = isset($_POST['status']) ? $_POST['status'] : "";

        if (empty($TITLE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        }

        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }

            $pathdocument = $this->config->item('save_document');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size_document');
            $config['max_width'] = $this->config->item('max_width_document');
            $config['min_width'] = $this->config->item('min_width_document');
            $config['max_height'] = $this->config->item('max_height_document');
            $config['min_height'] = $this->config->item('min_height_document');
            $config['file_name'] = 'DOCUMENT_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_document');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_file');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $param['ID'] = $ID;
                    $param['TITLE'] = $TITLE;
                    $param['LINK'] = $pathdocument . $config['file_name'] . "." . $forUpload['extension'];
                    $param['USER_LOG'] = $_SESSION['username'];
                    $param['DATE_LOG'] = date('Y-m-d H:i:s');
                    $param['STATUS'] = $STATUS;
                    $Update = $this->document_model->updateaction($param);
                    if ($Update['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
                    die(json_encode($JSON));
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $param['ID'] = $ID;
            $param['TITLE'] = $TITLE;
            $param['USER_LOG'] = $_SESSION['username'];
            $param['DATE_LOG'] = date('Y-m-d H:i:s');
            $param['STATUS'] = $STATUS;
            $Update = $this->document_model->updateaction($param);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        }
    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->document_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "0";
        $where = array('ID' => $id);
        $Active = $this->document_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "88";
        $where = array('ID' => $id);
        $Active = $this->document_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "99";
        $where = array('ID' => $id);
        $Active = $this->document_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}