<div class="navbar navbar-expand-md navbar-dark bg-custom-gradient">
    <div class="navbar-brand" style="padding: 0px;">
        <a href="<?php echo base_url(); ?>" class="d-inline-block">
            <img style="margin-top:10px; height: 50px !important; object-position: center;object-fit: contain;" class="logo-topbar" src="<?php echo base_url('assets/images/logo-bumi-artha.png'); ?>" alt="Logo Bank Bumi Arta">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo base_url(); ?>assets/images/demo/users/face11.jpg" class="rounded-circle mr-2" height="34" alt="">
                    <span><?php echo $_SESSION['name']; ?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalMyProfile">
                        <i class="icon-user-plus"></i> My profile
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url('authentication/logout') ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
</div>