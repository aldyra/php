<?php
/**
 *
 */
class MY_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('joy_helper');
        $this->load->library('datatables');
        $this->load->model('modulep_model');
        $this->load->model('user_model');
        $this->load->model('authentication_model');

        $this->data['nav_menu0'] = $this->modulep_model->getModule(
            $_SESSION['username'],
            array('MENU_LEVEL' => 0));

        auth();
        $all_module = $this->modulep_model->getModule($_SESSION['username']);
        $this->data['profile'] = $this->user_model->getProfile(array('USERNAME' => $_SESSION['username']), 'v_tblUsers');
        $this->data['authorize'] = authorize($all_module, $this->uri->segment(1));
    }
}