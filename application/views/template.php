<?php 
$REQUEST_URI = $_SERVER['REQUEST_URI'];
$EXP = explode("/", $REQUEST_URI);
$LANG = isset($EXP[2]) ? $EXP[2]:"";
if(empty($LANG)){
    redirect(base_url('id/'));
}
$param['nav_menu'] = $nav_menu;
$param['nav_menu_footer'] = $nav_menu_footer;
$param['social_media'] = $social_media;
$param['kurs'] = $kurs;
?>
<!DOCTYPE html>
<html> 
    <?php 
    $this->load->view("header");
    ?>
    <body style="overflow-x: none;">
        <div class="preloader" style="display: none;background:black;text-align: center;padding-top: 200px;">
            <div class="loading">
                <div class="spinner-border text-info" role="status" style="width: 100px; height: 100px;">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>

        <div class="culmn">
            <div class="row header-fixed">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                    <?php 
                    $this->load->view("navbar", $param);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
                    <section id="section1">
                        <?php 
                        $this->load->view($Content, $data);
                        ?>
                    </section>
                </div>
            </div>
            <div class="row row-footer">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <?php 
                    $this->load->view("footer", $param);
                    ?>
                </div>
            </div>
            <div class="row row-footer-bg">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="padding-right: 0; padding-left: 0;">
                    <div class="row-copyright">
                        <span style="font-size: 14px;">Copyrighted &copy; 2021 P.T. Bank Bumi Arta, Tbk. All Right Reserved.</span>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function formatRupiah(angka, prefix){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

                if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
            }
        </script>
        <style>
            @media only screen and (min-width: 200px) and (max-width: 1024px) {
                .scrollup{
                    bottom: 110px;
                    right: 18px;
                    z-index: auto;
                    /*z-index: 3;*/
                }
            }

            div.row-copyright{
                /*background-color: #FF0000;*/
            }

             div.row-copyright span{
                /*color: white !important;*/
                margin-top: 15px;
                margin-bottom: 15px;
            }

            p, ol li, ul li{
                font-size: 14px;
            }

            @media all and (min-width: 992px) {
                .box-header{
                    padding: 20px;
                    position: absolute; 
                    bottom: 0;
                    -webkit-backdrop-filter: blur(10px);
                    backdrop-filter: blur(10px);
                    width: 100%;
                }
            }
            @media all and (max-width: 992px) {
                .box-header{
                    padding: 20px;
                    -webkit-backdrop-filter: blur(10px);
                    backdrop-filter: blur(10px);
                    position: absolute;
                    bottom: 0px;
                    left: 0;
                    right: 0;
                }
            }
        </style>

        <div class="scrollup">
            <a href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>
        </div>
        <div class="modal fade" id="modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"></div>
    </body>
</html>
