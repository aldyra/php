<?php
$TYPE = isset($_POST['type']) ? $_POST['type'] : "";
$TITLE = isset($_POST['title']) ? $_POST['title'] : "";
$FILE = isset($_POST['file']) ? $_POST['file'] : "";
$DATA = isset($_POST['rows']) ? $_POST['rows'] : "";
$MODAL = isset($_POST['modal']) ? $_POST['modal'] : "";
$ACTION = isset($_POST['action']) ? $_POST['action'] : "";
?>
<div class="modal-dialog <?php echo $MODAL ?>">
    <div class="modal-content">
        <form class="parsleyy" data-parsley-validate="" novalidate="" action="javascript:void(0)"
            data-url="<?php echo $ACTION ?>" id="formData" name="formData" enctype="multipart/form-data">
            <div class="modal-header">
                <h4 class="modal-title" id="title"><?php echo $TITLE ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <?php $this->load->view("$FILE", $DATA);?>
            </div>
            <div class="modal-footer">
                <?php if (!empty($TYPE)) {if ($TYPE == "add") {$Button = "Save";} elseif ($TYPE == "update") {$Button = "Update";}?>
                <button type="button" class="btn btn-primary" onclick="submitform()" id="btnform">
                    <?php echo $Button ?>
                </button>
                <?php }?>
                <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>