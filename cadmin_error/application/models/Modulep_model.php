<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Modulep_model extends MY_Model
{

    protected $_table_name = 'MS_MODULE_PREVILEGE';
    protected $_primary_key = 'ID';
    protected $_order_by = 'MENU_SEQ';
    protected $_order_by_type = 'ASC';

    public function __construct()
    {
        parent::__construct();
    }
    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view($where)
    {
        //menu b2c
        if ($where) {
            $this->db->where($where);

        }
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function getModule($username = null, $where = null)
    {
        $queryGet =
            "SELECT
        a.ID as ID,
        a.MODULE_NAME as MODULE_NAME,
        a.CONTROLLER as CONTROLLER,
        a.`FUNCTION` as `FUNCTION`,
        a.ICON as ICON,
        b.IS_READ as IS_READ,
        b.IS_CREATE as IS_CREATE,
        b.IS_UPDATE as IS_UPDATE,
        b.IS_DELETE as IS_DELETE,
        b.IS_EXPORT as IS_EXPORT,
        b.IS_IMPORT as IS_IMPORT
        FROM MS_MODULE_PREVILEGE a
        JOIN MS_USER_GROUP_PREVILEGE b
        ON a.ID = b.MODULE_PREVILEGE_ID
        JOIN MS_USER_GROUP c
        ON b.USER_GROUP_ID = c.ID
        JOIN MS_USERS d
        ON c.ID = d.GROUP_ID
        WHERE USERNAME='" . $username . "'
        AND IS_READ = 1
        AND ACTIVE = 1
        AND c.STATUS = '88'";

        if ($where) {
            foreach ($where as $key => $value) {
                $queryGet .= "
                    AND " . $key . " = '" . $value . "'
                ";
            }
        }

        $queryGet .= " ORDER BY a.MENU_SEQ ASC";

        return $this->db->query($queryGet)->result();
    }

    public function get_data($col)
    {
        $this->datatables->select($col);
        $this->datatables->from($this->_table_name);

        return $sql = $this->datatables->generate();
    }
    public function getMenu0()
    {
        $queryMainMenu = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=0 AND ACTIVE=1 AND IS_B2C = 0
							ORDER BY MENU_SEQ ASC";
        $listMainMenu = $this->db->query($queryMainMenu)->result();
        return $listMainMenu;
    }
    public function getMenu1()
    {
        $queryMainMenu = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=0 AND ACTIVE=1 AND IS_B2C = 0
							ORDER BY MENU_SEQ ASC";
        $listMainMenu = $this->db->query($queryMainMenu)->result();
        foreach ($listMainMenu as $row) {

            // CHECK MAIN SUB MENU
            $queryMSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=1 AND MENU_PARENT=" . $row->ID . " AND ACTIVE=1 AND IS_B2C = 0 ORDER BY MENU_SEQ ASC";
            $resMSM = $this->db->query($queryMSM);
        }
        return $listMainMenu;
    }
    public function getMenu()
    {
        $queryGet = "SELECT C.ID, MODULE_NAME, USERNAME, D.ID AS USER_ID, MENU_LEVEL, MENU_PARENT, MENU_SEQ,
                        A.IS_READ, A.IS_CREATE, A.IS_DELETE, A.IS_UPDATE, A.IS_EXPORT, A.IS_IMPORT,
                        C.CONTROLLER, C.FUNCTION, C.ICON
                    FROM MS_USER_GROUP_PREVILEGE A
                    JOIN MS_USER_GROUP B ON A.USER_GROUP_ID = B.ID
                    JOIN MS_MODULE_PREVILEGE C ON A.MODULE_PREVILEGE_ID = C.ID
                    JOIN MS_USERS D ON B.ID = D.GROUP_ID
                    WHERE IS_READ != '0'
                    -- AND IS_B2C = 0
                    ORDER BY MENU_SEQ ASC";

        $resMod = $this->db->query($queryGet)->result();

        $modName = array();
        foreach ($resMod as $listMenu) {
            array_push($modName, $listMenu->MODULE_NAME);
        }

        // var_dump($modName);

        $smodName = "'" . (implode("','", $modName)) . "'";

        $queryMainMenu = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=0 AND ACTIVE=1 AND IS_B2C = 0
							ORDER BY MENU_SEQ ASC";
        $listMainMenu = $this->db->query($queryMainMenu)->result();

        $data['NavMenu'] = array();

        foreach ($listMainMenu as $row) {

            // CHECK MAIN SUB MENU
            $queryMSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=1 AND MENU_PARENT=" . $row->ID . " AND ACTIVE=1 AND IS_B2C = 0 ORDER BY MENU_SEQ ASC";
            $resMSM = $this->db->query($queryMSM);
            // /CHECK MAIN SUB MENU

            if ($resMSM->num_rows() > 0) {

                $arraychildlvl1 = array();
                foreach ($resMSM->result() as $MSM) {

                    // CHECK SUB SUB MENU
                    $querySSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=2 AND MENU_PARENT=" . $MSM->ID . " AND ACTIVE=1 AND IS_B2C = 0 ORDER BY MENU_SEQ ASC";
                    $resSSM = $this->db->query($querySSM);
                    // /CHECK SUB SUB MENU

                    if ($resSSM->num_rows() > 0) {

                        $arraychildlvl2 = array();
                        foreach ($resSSM->result() as $SSM) {

                            // CHECK SUB SUB SUB MENU
                            $querySSSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=3 AND MENU_PARENT=" . $SSM->ID . " AND ACTIVE=1 AND IS_B2C = 0 ORDER BY MENU_SEQ ASC";
                            $resSSSM = $this->db->query($querySSSM);
                            // /CHECK SUB SUB SUB MENU

                            if ($resSSSM->num_rows() > 0) {

                                $arraychildlvl3 = array();
                                foreach ($resSSSM->result() as $SSSM) {
                                    // LEVEL 3
                                    $arraytmpchildlvl3 = array(
                                        'NAME' => $SSSM->MODULE_NAME,
                                        'CONTROLLER' => $SSSM->CONTROLLER,
                                        'LINK' => ($SSSM->CONTROLLER == null) ? '#' : $SSM->CONTROLLER . '/' . $SSM->FUNCTION,
                                        'ICON' => $SSSM->ICON,
                                    );
                                    if (in_array($SSM->MODULE_NAME, $modName)) {
                                        array_push($arraychildlvl3, $arraytmpchildlvl3);
                                    }
                                }

                                // LEVEL 2
                                $arraytmpchildlvl2 = array(
                                    'NAME' => $SSM->MODULE_NAME,
                                    'CONTROLLER' => $SSM->CONTROLLER,
                                    'LINK' => ($SSM->CONTROLLER == null) ? '#' : $SSM->CONTROLLER . '/' . $SSM->FUNCTION,
                                    'ICON' => $SSM->ICON,
                                    'CHILD' => $arraychildlvl3,
                                );
                                if (in_array($SSM->MODULE_NAME, $modName)) {
                                    array_push($arraychildlvl2, $arraytmpchildlvl2);
                                }
                            } else {
                                // LEVEL 2
                                $arraytmpchildlvl2 = array(
                                    'NAME' => $SSM->MODULE_NAME,
                                    'CONTROLLER' => $SSM->CONTROLLER,
                                    'LINK' => ($SSM->CONTROLLER == null) ? '#' : $SSM->CONTROLLER . '/' . $SSM->FUNCTION,
                                    'ICON' => $SSM->ICON,
                                    'CHILD' => null,
                                );
                                if (in_array($SSM->MODULE_NAME, $modName)) {
                                    array_push($arraychildlvl2, $arraytmpchildlvl2);
                                }
                            }

                        }
                        // LEVEL 1
                        $arraytmpchildlvl1 = array(
                            'NAME' => $MSM->MODULE_NAME,
                            'CONTROLLER' => $MSM->CONTROLLER,
                            'LINK' => ($MSM->CONTROLLER == null) ? '#' : $MSM->CONTROLLER . '/' . $MSM->FUNCTION,
                            'ICON' => $MSM->ICON,
                            'CHILD' => $arraychildlvl2,
                        );

                        if (!(empty($arraychildlvl2))) {
                            array_push($arraychildlvl1, $arraytmpchildlvl1);
                        }

                    } else {

                        // LEVEL 1 NO CHILD
                        $arraytmpchildlvl1 = array(
                            'NAME' => $MSM->MODULE_NAME,
                            'CONTROLLER' => $MSM->CONTROLLER,
                            'LINK' => ($MSM->CONTROLLER == null) ? '#' : $MSM->CONTROLLER . '/' . $MSM->FUNCTION,
                            'ICON' => $MSM->ICON,
                            'CHILD' => null,
                        );

                        if (in_array($MSM->MODULE_NAME, $modName)) {
                            array_push($arraychildlvl1, $arraytmpchildlvl1);
                        }

                    }
                }

                // LEVEL 0
                $arraytmp = array(
                    'NAME' => $row->MODULE_NAME,
                    'CONTROLLER' => $row->CONTROLLER,
                    'LINK' => ($row->CONTROLLER == null) ? '#' : $row->CONTROLLER . '/' . $row->FUNCTION,
                    'ICON' => $row->ICON,
                    'CHILD' => $arraychildlvl1,
                );

                if (!(empty($arraychildlvl1))) {
                    array_push($data['NavMenu'], $arraytmp);
                }

            } else {

                // LEVEL 0 NO CHILD
                $arraytmp = array(
                    'NAME' => $row->MODULE_NAME,
                    'CONTROLLER' => $row->CONTROLLER,
                    'LINK' => ($row->CONTROLLER == null) ? '#' : $row->CONTROLLER . '/' . $row->FUNCTION,
                    'ICON' => $row->ICON,
                    'CHILD' => null,
                );

                if (in_array($row->MODULE_NAME, $modName)) {
                    array_push($data['NavMenu'], $arraytmp);
                }

            }
        }

        // echo json_encode($data['NavMenu']);
        // var_dump($data['NavMenu']);

        return $data['NavMenu'];

    }
}