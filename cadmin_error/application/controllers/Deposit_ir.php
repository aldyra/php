<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Deposit_ir extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('deposit_ir_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "deposit_ir/view";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['deposit_ir'] = $this->deposit_ir_model->view();

        $this->load->view('templates/view', $data);
    }
    public function add_deposit_ir()
    {
        $data = array();
        $data['content'] = "deposit_ir/form_add";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['back'] = base_url() . 'deposit_ir';

        $this->load->view('templates/view', $data);
    }
    public function edit_deposit_ir($id)
    {
        $data = array();
        $data['content'] = "deposit_ir/form_edit";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['deposit_ir'] = $this->deposit_ir_model->get_edit($id);
        $detail = $this->deposit_ir_model->get_detail($id);
        $data['data']['detail'] = $detail['html'];
        $data['data']['no'] = $detail['no'];
        $data['data']['back'] = base_url() . 'deposit_ir';

        $this->load->view('templates/view', $data);
    }
    public function detail_deposit_ir($id)
    {
        $data = array();
        $data['content'] = "deposit_ir/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['deposit_ir'] = $this->deposit_ir_model->get_edit($id);
        $data['data']['detail'] = $this->deposit_ir_model->get_detail_deposit_ir($id);
        $data['data']['back'] = base_url() . 'deposit_ir';
        $this->load->view('templates/view', $data);
    }
    public function add()
    {
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d', strtotime($_POST['effective_date'])) : "";
        if ($CATEGORY == 1 || $CATEGORY == 3) {

            $month = isset($_POST['month']) ? $_POST['month'] : array();
            $interest_rate_month = isset($_POST['interest_rate_month']) ? $_POST['interest_rate_month'] : array();
            if (empty($month)) {
                $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Detail month is required!");
                die(json_encode($JSON));
            } elseif (empty($interest_rate_month)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "Detail interest rate is required!");
                die(json_encode($JSON));
            }

            $a = 0;
            foreach ($month as $mth) {
                $MONTH[$a] = $mth;
                $a++;
            }
            $b = 0;
            foreach ($interest_rate_month as $ratemth) {
                $INTEREST_RATE_MONTH[$b] = $ratemth;
                $b++;
            }
        } else {
            $MONTH = '';
            $INTEREST_RATE_MONTH = '';

        }

        //detail
        $ID_DETAIL = isset($_POST['detail_id']) ? $_POST['detail_id'] : "";
        $BALANCE_ID = isset($_POST['balance_id']) ? $_POST['balance_id'] : "";
        $BALANCE_EN = isset($_POST['balance_en']) ? $_POST['balance_en'] : "";
        $MONTH_PERPANJANG = isset($_POST['month_perpanjang']) ? $_POST['month_perpanjang'] : "";
        $MONTH_1 = isset($_POST['month_1']) ? $_POST['month_1'] : "";
        $MONTH_3 = isset($_POST['month_3']) ? $_POST['month_3'] : "";
        $MONTH_4 = isset($_POST['month_4']) ? $_POST['month_4'] : "";
        $MONTH_6 = isset($_POST['month_6']) ? $_POST['month_6'] : "";
        $MONTH_12 = isset($_POST['month_12']) ? $_POST['month_12'] : "";
        $MIN_TABUNGAN_KESRA = isset($_POST['min_tabungan_kesra']) ? $_POST['min_tabungan_kesra'] : "";
        $OPERATION = isset($_POST['operation']) ? $_POST['operation'] : "";
        $INTEREST_RATE = isset($_POST['interest_rate']) ? $_POST['interest_rate'] : "";
        $INTEREST_RATE_BEFORE = isset($_POST['interest_rate_before']) ? $_POST['interest_rate_before'] : "";
        $INTEREST_RATE_NEXT = isset($_POST['interest_rate_next']) ? $_POST['interest_rate_next'] : "";
        $count = count($ID_DETAIL);
        for ($i = 0; $i < $count; $i++) {
            $FIX_BALANCE_ID[$i] = ($BALANCE_ID == "") ? "" : $BALANCE_ID[$i];
            $FIX_BALANCE_EN[$i] = ($BALANCE_EN == "") ? "" : $BALANCE_EN[$i];
            $FIX_MONTH_PERPANJANG[$i] = ($MONTH_PERPANJANG == "") ? "" : $MONTH_PERPANJANG[$i];
            $FIX_MONTH_1[$i] = ($MONTH_1 == "") ? "" : $MONTH_1[$i];
            $FIX_MONTH_3[$i] = ($MONTH_3 == "") ? "" : $MONTH_3[$i];
            $FIX_MONTH_4[$i] = ($MONTH_4 == "") ? "" : $MONTH_4[$i];
            $FIX_MONTH_6[$i] = ($MONTH_6 == "") ? "" : $MONTH_6[$i];
            $FIX_MONTH_12[$i] = ($MONTH_12 == "") ? "" : $MONTH_12[$i];
            $FIX_MIN_TABUNGAN_KESRA[$i] = ($MIN_TABUNGAN_KESRA == "") ? "" : $MIN_TABUNGAN_KESRA[$i];
            $FIX_OPERATION[$i] = ($OPERATION == "") ? "" : $OPERATION[$i];
            $FIX_INTEREST_RATE[$i] = ($INTEREST_RATE == "") ? "" : $INTEREST_RATE[$i];
            $FIX_INTEREST_RATE_BEFORE[$i] = ($INTEREST_RATE_BEFORE == "") ? "" : $INTEREST_RATE_BEFORE[$i];
            $FIX_INTEREST_RATE_NEXT[$i] = ($INTEREST_RATE_NEXT == "") ? "" : $INTEREST_RATE_NEXT[$i];

        }

        $this->db->trans_begin();

        $dataheader['CATEGORY_DEPOSIT'] = $CATEGORY;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['CREATED_BY'] = $_SESSION['username'];
        $insert_header = $this->db->insert('MS_DEPOSIT_IR_HEADER', $dataheader);
        $ID_DEPOSIT_IR_HEADER = $this->db->insert_id();

        if (!$insert_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        } else {
            $cpt = count($ID_DETAIL);

            for ($j = 0; $j < $cpt; $j++) {
                $datadetail['ID_DEPOSIT_IR_HEADER'] = $ID_DEPOSIT_IR_HEADER;
                $datadetail['BALANCE_ID'] = htmlentities($FIX_BALANCE_ID[$j]);
                $datadetail['BALANCE_EN'] = htmlentities($FIX_BALANCE_EN[$j]);
                $datadetail['MONTH'] = $FIX_MONTH_PERPANJANG[$j];
                $datadetail['MONTH_1'] = $FIX_MONTH_1[$j];
                $datadetail['MONTH_3'] = $FIX_MONTH_3[$j];
                $datadetail['MONTH_4'] = $FIX_MONTH_4[$j];
                $datadetail['MONTH_6'] = $FIX_MONTH_6[$j];
                $datadetail['MONTH_12'] = $FIX_MONTH_12[$j];
                $datadetail['MIN_TABUNGAN_KESRA'] = $FIX_MIN_TABUNGAN_KESRA[$j];
                $datadetail['OPERATION'] = $FIX_OPERATION[$j];
                $datadetail['INTEREST_RATE'] = $FIX_INTEREST_RATE[$j];
                $datadetail['INTEREST_RATE_BEFORE'] = $FIX_INTEREST_RATE_BEFORE[$j];
                $datadetail['INTEREST_RATE_NEXT'] = $FIX_INTEREST_RATE_NEXT[$j];
                $datadetail['CREATED_BY'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('MS_DEPOSIT_IR_DETAIL', $datadetail);
                $ID_DEPOSIT_IR_DETAIL = $this->db->insert_id();

                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }
                if (!empty($MONTH)) {
                    $detail_month = count($MONTH[$j]);
                    for ($k = 0; $k < $detail_month; $k++) {
                        $datadetail_month = array(
                            'ID_DEPOSIT_IR_DETAIL' => $ID_DEPOSIT_IR_DETAIL,
                            'MONTH' => $MONTH[$j][$k],
                            'INTEREST_RATE' => $INTEREST_RATE_MONTH[$j][$k],
                            'CREATED_BY' => $_SESSION['username'],
                        );
                        $insert_detail_month = $this->db->insert('MS_DEPOSIT_IR_MONTH', $datadetail_month);
                        if (!$insert_detail_month) {
                            $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                            $this->db->trans_rollback();
                            die(json_encode($JSON));
                        }
                    }
                }

            }
            if ($this->db->trans_status() === false) {
                $final_res = 'Failed: Failed to submit data';
                $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
                $this->db->trans_rollback();
                die(json_encode($JSON));

            } else {
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added");
                $this->db->trans_commit();
                die(json_encode($JSON));

            }

        }

    }
    public function update()
    {
        // var_dump($_POST);exit();
        $ID_DEPOSIT_IR_HEADER = isset($_POST['id_deposit_ir_header']) ? $_POST['id_deposit_ir_header'] : "";
        $CATEGORY = isset($_POST['category']) ? $_POST['category'] : "";
        $EFFECTIVE_DATE = isset($_POST['effective_date']) ? date('Y-m-d', strtotime($_POST['effective_date'])) : "";

        if ($CATEGORY == 1 || $CATEGORY == 3) {

            $month = isset($_POST['month']) ? $_POST['month'] : array();
            $interest_rate_month = isset($_POST['interest_rate_month']) ? $_POST['interest_rate_month'] : array();
            if (empty($month)) {
                $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Detail month is required!");
                die(json_encode($JSON));
            } elseif (empty($interest_rate_month)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "Detail interest rate is required!");
                die(json_encode($JSON));
            }

            $a = 0;
            foreach ($month as $mth) {
                $MONTH[$a] = $mth;
                $a++;
            }
            $b = 0;
            foreach ($interest_rate_month as $ratemth) {
                $INTEREST_RATE_MONTH[$b] = $ratemth;
                $b++;
            }
        } else {
            $MONTH = '';
            $INTEREST_RATE_MONTH = '';

        }

        //detail
        $ID_DETAIL = isset($_POST['detail_id']) ? $_POST['detail_id'] : "";
        $BALANCE_ID = isset($_POST['balance_id']) ? $_POST['balance_id'] : "";
        $BALANCE_EN = isset($_POST['balance_en']) ? $_POST['balance_en'] : "";
        $MONTH_PERPANJANG = isset($_POST['month_perpanjang']) ? $_POST['month_perpanjang'] : "";
        $MONTH_1 = isset($_POST['month_1']) ? $_POST['month_1'] : "";
        $MONTH_3 = isset($_POST['month_3']) ? $_POST['month_3'] : "";
        $MONTH_4 = isset($_POST['month_4']) ? $_POST['month_4'] : "";
        $MONTH_6 = isset($_POST['month_6']) ? $_POST['month_6'] : "";
        $MONTH_12 = isset($_POST['month_12']) ? $_POST['month_12'] : "";
        $MIN_TABUNGAN_KESRA = isset($_POST['min_tabungan_kesra']) ? $_POST['min_tabungan_kesra'] : "";
        $OPERATION = isset($_POST['operation']) ? $_POST['operation'] : "";
        $INTEREST_RATE = isset($_POST['interest_rate']) ? $_POST['interest_rate'] : "";
        $INTEREST_RATE_BEFORE = isset($_POST['interest_rate_before']) ? $_POST['interest_rate_before'] : "";
        $INTEREST_RATE_NEXT = isset($_POST['interest_rate_next']) ? $_POST['interest_rate_next'] : "";
        $count = count($ID_DETAIL);
        for ($i = 0; $i < $count; $i++) {
            $FIX_BALANCE_ID[$i] = ($BALANCE_ID == "") ? "" : $BALANCE_ID[$i];
            $FIX_BALANCE_EN[$i] = ($BALANCE_EN == "") ? "" : $BALANCE_EN[$i];
            $FIX_MONTH_PERPANJANG[$i] = ($MONTH_PERPANJANG == "") ? "" : $MONTH_PERPANJANG[$i];
            $FIX_MONTH_1[$i] = ($MONTH_1 == "") ? "" : $MONTH_1[$i];
            $FIX_MONTH_3[$i] = ($MONTH_3 == "") ? "" : $MONTH_3[$i];
            $FIX_MONTH_4[$i] = ($MONTH_4 == "") ? "" : $MONTH_4[$i];
            $FIX_MONTH_6[$i] = ($MONTH_6 == "") ? "" : $MONTH_6[$i];
            $FIX_MONTH_12[$i] = ($MONTH_12 == "") ? "" : $MONTH_12[$i];
            $FIX_MIN_TABUNGAN_KESRA[$i] = ($MIN_TABUNGAN_KESRA == "") ? "" : $MIN_TABUNGAN_KESRA[$i];
            $FIX_OPERATION[$i] = ($OPERATION == "") ? "" : $OPERATION[$i];
            $FIX_INTEREST_RATE[$i] = ($INTEREST_RATE == "") ? "" : $INTEREST_RATE[$i];
            $FIX_INTEREST_RATE_BEFORE[$i] = ($INTEREST_RATE_BEFORE == "") ? "" : $INTEREST_RATE_BEFORE[$i];
            $FIX_INTEREST_RATE_NEXT[$i] = ($INTEREST_RATE_NEXT == "") ? "" : $INTEREST_RATE_NEXT[$i];
        }

        $this->db->trans_begin();
        //UPDATE HEADER
        $dataheader['CATEGORY_DEPOSIT'] = $CATEGORY;
        $dataheader['EFFECTIVE_DATE'] = $EFFECTIVE_DATE;
        $dataheader['USER_LOG'] = $_SESSION['username'];
        $dataheader['DATE_LOG'] = date('Y-m-d H:i:s');
        $where['ID_DEPOSIT_IR_HEADER'] = $ID_DEPOSIT_IR_HEADER;
        $update_header = $this->db->where($where)->update('MS_DEPOSIT_IR_HEADER', $dataheader);

        if (!$update_header) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            $this->db->trans_rollback();
            die(json_encode($JSON));
        } else {
            $detail = $this->db->query("SELECT * FROM MS_DEPOSIT_IR_DETAIL WHERE ID_DEPOSIT_IR_HEADER = '" . $ID_DEPOSIT_IR_HEADER . "'")->result();
            foreach ($detail as $dt) {

                $run_delete_detail = $this->db->delete('MS_DEPOSIT_IR_MONTH', array('ID_DEPOSIT_IR_DETAIL' => $dt->ID_DEPOSIT_IR_DETAIL));
                if (!$run_delete_detail) {
                    $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    return $JSON;
                }

            }

            $run_delete = $this->db->delete('MS_DEPOSIT_IR_DETAIL', array('ID_DEPOSIT_IR_HEADER' => $ID_DEPOSIT_IR_HEADER));
            if (!$run_delete) {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
                $this->db->trans_rollback();
                return $JSON;
            }

            //UPDATE DETAIL
            $cpt = count($ID_DETAIL);

            for ($j = 0; $j < $cpt; $j++) {
                $datadetail['ID_DEPOSIT_IR_HEADER'] = $ID_DEPOSIT_IR_HEADER;
                $datadetail['BALANCE_ID'] = htmlentities($FIX_BALANCE_ID[$j]);
                $datadetail['BALANCE_EN'] = htmlentities($FIX_BALANCE_EN[$j]);
                $datadetail['MONTH'] = $FIX_MONTH_PERPANJANG[$j];
                $datadetail['MONTH_1'] = $FIX_MONTH_1[$j];
                $datadetail['MONTH_3'] = $FIX_MONTH_3[$j];
                $datadetail['MONTH_4'] = $FIX_MONTH_4[$j];
                $datadetail['MONTH_6'] = $FIX_MONTH_6[$j];
                $datadetail['MONTH_12'] = $FIX_MONTH_12[$j];
                $datadetail['MIN_TABUNGAN_KESRA'] = $FIX_MIN_TABUNGAN_KESRA[$j];
                $datadetail['OPERATION'] = $FIX_OPERATION[$j];
                $datadetail['INTEREST_RATE'] = $FIX_INTEREST_RATE[$j];
                $datadetail['INTEREST_RATE_BEFORE'] = $FIX_INTEREST_RATE_BEFORE[$j];
                $datadetail['INTEREST_RATE_NEXT'] = $FIX_INTEREST_RATE_NEXT[$j];
                $datadetail['CREATED_BY'] = $_SESSION['username'];
                $insert_detail = $this->db->insert('MS_DEPOSIT_IR_DETAIL', $datadetail);
                $ID_DEPOSIT_IR_DETAIL = $this->db->insert_id();

                if (!$insert_detail) {
                    $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                    $this->db->trans_rollback();
                    die(json_encode($JSON));
                }
                if (!empty($MONTH)) {

                    $detail_month = count($MONTH[$j]);
                    for ($k = 0; $k < $detail_month; $k++) {
                        $datadetail_month = array(
                            'ID_DEPOSIT_IR_DETAIL' => $ID_DEPOSIT_IR_DETAIL,
                            'MONTH' => $MONTH[$j][$k],
                            'INTEREST_RATE' => $INTEREST_RATE_MONTH[$j][$k],
                            'CREATED_BY' => $_SESSION['username'],
                        );
                        $insert_detail_month = $this->db->insert('MS_DEPOSIT_IR_MONTH', $datadetail_month);
                        if (!$insert_detail_month) {
                            $JSON = array("ErrorCode" => "EC:002AA", "ErrorMessage" => json_encode($this->db->error()));
                            $this->db->trans_rollback();
                            die(json_encode($JSON));
                        }
                    }
                }

            }

        }
        if ($this->db->trans_status() === false) {
            $final_res = 'Failed: Failed to submit data';
            $JSON = array("ErrorCode" => "EC:000U", "ErrorMessage" => 'Failed to sumbit data');
            $this->db->trans_rollback();
            die(json_encode($JSON));

        } else {
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated");
            $this->db->trans_commit();
            die(json_encode($JSON));

        }

    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID_DEPOSIT_IR_HEADER = isset($Data['ID_DEPOSIT_IR_HEADER']) ? $Data['ID_DEPOSIT_IR_HEADER'] : "";
        $param['ID_DEPOSIT_IR_HEADER'] = $ID_DEPOSIT_IR_HEADER;
        $Delete = $this->deposit_ir_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_DEPOSIT_IR_HEADER']) ? $Data['ID_DEPOSIT_IR_HEADER'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_DEPOSIT_IR_HEADER' => $id);
        $Active = $this->deposit_ir_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_DEPOSIT_IR_HEADER']) ? $Data['ID_DEPOSIT_IR_HEADER'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_DEPOSIT_IR_HEADER' => $id);
        $Active = $this->deposit_ir_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_DEPOSIT_IR_HEADER']) ? $Data['ID_DEPOSIT_IR_HEADER'] : "";

        $data['STATUS'] = "99";
        $data['APPROVED_DATE'] = date('Y-m-d H:i:s');
        $data['APPROVED_BY'] = $_SESSION['username'];
        $data['USER_LOG'] = $_SESSION['username'];
        $data['DATE_LOG'] = date('Y-m-d H:i:s');

        $where = array('ID_DEPOSIT_IR_HEADER' => $id);
        $Active = $this->deposit_ir_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $dataTransaction = array(
            array(
                'ID' => $id,
                'MODULE' => 'DEPOSITO',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => date('Y-m-d H:i:s'),
                'COLUMN_DATA' => 'APPROVED_DATE',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
            array(
                'ID' => $id,
                'MODULE' => 'DEPOSITO',
                'TYPE' => 'UPDATE',
                'OLD_DATA' => '',
                'NEW_DATA' => $_SESSION['username'],
                'COLUMN_DATA' => 'APPROVED_BY',
                'CREATED_DATE' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $_SESSION['username'],
            ),
        );
        $InsertTransaction = $this->db->insert_batch('LOG_TRANSACTION', $dataTransaction);
        if (!$InsertTransaction) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }

        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Approved");
        die(json_encode($JSON));
    }
}