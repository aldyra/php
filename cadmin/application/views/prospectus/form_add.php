<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="form-group">
                    <label>Upload File Indonesia</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_id" name="file_id" class="dropify" required="" />
                    <a href="#" onclick="openTnC('<?php echo base_url('prospectus/tncupload') ?>')">Terms and Conditions
                        Upload</a>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="form-group">
                    <label>Upload File English</label><span style="color: orange">*</span><br>
                    <input type="file" id="file_en" name="file_en" class="dropify" required="" />
                    <a href="#" onclick="openTnC('<?php echo base_url('prospectus/tncupload') ?>')">Terms and Conditions
                        Upload</a>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    $('.select2modal').select2({
        dropdownParent: $('#modal')
    });
    changeformfile();
});
</script>