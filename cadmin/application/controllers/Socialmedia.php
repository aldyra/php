<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Socialmedia extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('socialmedia_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = "socialmedia/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['socialmedia'] = $this->socialmedia_model->view();

        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Image max pixel ' . $this->config->item('max_height') . 'x' . $this->config->item('max_width') . 'px <br>
                    Allowed images are ' . str_replace("|", ", ", $this->config->item('allowed_types_image')) . ' <br>
                    Max image size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }

    public function add()
    {
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $URL_SOCIAL_MEDIA = isset($_POST['url_social_media']) ? $_POST['url_social_media'] : "";
        // $IS_SHARING = isset($_POST['is_sharing']) ? $_POST['is_sharing'] : 0;
        // $IS_HIDDEN = isset($_POST['is_hidden']) ? $_POST['is_hidden'] : 0;
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($NAME)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        } elseif (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
            die(json_encode($JSON));
        }

        $pathsocialmedia = $this->config->item('save_social_media');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size');
        $config['max_width'] = $this->config->item('max_width');
        $config['min_width'] = $this->config->item('min_width');
        $config['max_height'] = $this->config->item('max_height');
        $config['min_height'] = $this->config->item('min_height');
        $config['file_name'] = 'SOCIAL_MEDIA_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_social_media');
        $path_parts = pathinfo($_FILES["file"]["name"]);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_image');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $param['NAME'] = $NAME;
                $param['LINK'] = $pathsocialmedia . $config['file_name'] . "." . $forUpload['extension'];
                $param['URL_SOCIAL_MEDIA'] = $URL_SOCIAL_MEDIA;
                // $param['IS_HIDDEN'] = $IS_HIDDEN;
                // $param['IS_SHARING'] = $IS_SHARING;

                $param['CREATED_BY'] = $_SESSION['username'];
                $Insert = $this->socialmedia_model->add($param);
                if ($Insert['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                    die(json_encode($JSON));
                }
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->socialmedia_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $ID_SOCIAL_MEDIA = isset($_POST['id_social_media']) ? $_POST['id_social_media'] : "";
        $NAME = isset($_POST['name']) ? $_POST['name'] : "";
        $URL_SOCIAL_MEDIA = isset($_POST['url_social_media']) ? $_POST['url_social_media'] : "";
        // $IS_SHARING = isset($_POST['is_sharing']) ? $_POST['is_sharing'] : 0;
        // $IS_HIDDEN = isset($_POST['is_hidden']) ? $_POST['is_hidden'] : 0;

        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        $STATUS = isset($_POST['status']) ? $_POST['status'] : "";
        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }

            $pathsocialmedia = $this->config->item('save_social_media');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size');
            $config['max_width'] = $this->config->item('max_width');
            $config['min_width'] = $this->config->item('min_width');
            $config['max_height'] = $this->config->item('max_height');
            $config['min_height'] = $this->config->item('min_height');
            $config['file_name'] = 'SOCIAL_MEDIA_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_social_media');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_image');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $param['ID_SOCIAL_MEDIA'] = $ID_SOCIAL_MEDIA;
                    $param['NAME'] = $NAME;
                    $param['URL_SOCIAL_MEDIA'] = $URL_SOCIAL_MEDIA;
                    $param['LINK'] = $pathsocialmedia . $config['file_name'] . "." . $forUpload['extension'];
                    // $param['IS_SHARING'] = $IS_SHARING;
                    // $param['IS_HIDDEN'] = $IS_HIDDEN;
                    $param['USER_LOG'] = $_SESSION['username'];
                    $param['DATE_LOG'] = date('Y-m-d H:i:s');
                    $param['STATUS'] = $STATUS;
                    $Update = $this->socialmedia_model->updateaction($param);
                    if ($Update['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
                    die(json_encode($JSON));
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $param['ID_SOCIAL_MEDIA'] = $ID_SOCIAL_MEDIA;
            $param['NAME'] = $NAME;
            $param['URL_SOCIAL_MEDIA'] = $URL_SOCIAL_MEDIA;
            $param['USER_LOG'] = $_SESSION['username'];
            // $param['IS_SHARING'] = $IS_SHARING;
            // $param['IS_HIDDEN'] = $IS_HIDDEN;
            $param['DATE_LOG'] = date('Y-m-d H:i:s');

            $param['STATUS'] = $STATUS;
            $Update = $this->socialmedia_model->updateaction($param);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        }
    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID_SOCIAL_MEDIA']) ? $Data['ID_SOCIAL_MEDIA'] : "";
        $param['ID_SOCIAL_MEDIA'] = $ID;
        $Delete = $this->socialmedia_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_SOCIAL_MEDIA']) ? $Data['ID_SOCIAL_MEDIA'] : "";

        $data['STATUS'] = "0";
        $where = array('ID_SOCIAL_MEDIA' => $id);
        $Active = $this->socialmedia_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_SOCIAL_MEDIA']) ? $Data['ID_SOCIAL_MEDIA'] : "";

        $data['STATUS'] = "88";
        $where = array('ID_SOCIAL_MEDIA' => $id);
        $Active = $this->socialmedia_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID_SOCIAL_MEDIA']) ? $Data['ID_SOCIAL_MEDIA'] : "";

        $data['STATUS'] = "99";
        $where = array('ID_SOCIAL_MEDIA' => $id);
        $Active = $this->socialmedia_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}