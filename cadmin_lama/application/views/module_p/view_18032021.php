<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bank Bumi Arta</title>

    <!-- INCLUDE SCRIPT TOP -->
        <?php require_once APPPATH . 'views/templates/script_top.php';?>
    <!-- END INCLUDE SCRIPT TOP -->

</head>

<body>

    <!-- OPEN NAV MENU -->
    <?php require_once APPPATH . 'views/templates/topbar.php';?>
    <!-- END NAV MENU -->

    <!-- Page content -->
    <div class="page-content">

        <!-- OPEN LEFT NAV BAR -->
        <?php require_once APPPATH . 'views/templates/nav_menu.php';?>
        <!-- END LEFT NAV BAR -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- OPEN MODAL -->
            <?php require_once APPPATH . 'views/templates/modals.php';?>
            <!-- END MODAL -->

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><span class="font-weight-semibold">Module Privilege</span></h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active">Module Privilege</span>
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Order direction sequence control -->
                <div class="card">

                    <table class="table datatable-complex-header">
                        <thead>
                            <tr>
                                <th>Module Name</th>
                                <th data-orderable="false">Status</th>
                                <th data-orderable="false"  width="200">Created Date</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /order direction sequence control -->

            </div>
            <!-- /content area -->


            <!-- OPEN FOOTER -->
            <?php require_once APPPATH . 'views/templates/footer.php';?>
            <!-- END FOOTER -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script type="text/javascript">
        var arr = ["MENU_SEQ", "MODULE_NAME", "ACTIVE", "CREATED_DATE"];
        var t = $(".datatable-complex-header").DataTable({
            serverSide: true,
            autoWidth: false,
            ajax: {
                "url": "<?php echo base_url('module_p/get_data') ?>",
                "type": "POST",
                "data": {"arr": arr},
            },
            columns: [
                { "data": "MODULE_NAME" },
                { "data": "ACTIVE" },
                { "data": "CREATED_DATE" },
                { "data": "MENU_SEQ" }
            ],
            columnDefs: [{
                "targets": [3],
                "visible": false,
                "searchable": false
            }],
            keys: !0,
            order: [[ 3, "asc" ]],
            autofill: true,
            select: true,
            responsive: true,
            buttons: true,
            length: 5
        });

            // $("#btnAddUserGroup").on('click', function(){
            //     $.ajax({
            //         type: "GET",
            //         url: "<?php echo site_url('user_group/get_privilege'); ?>",
            //         timeout : 3000,
            //         dataType: "JSON",
            //         error: function() {
            //             alert("ERROR!");
            //         },
            //         success: function(data) {
            //             $("#frmsaveUserGroup").find("#module_privilege").html(data.output);
            //         }
            //     });
            // });

        $('#frmsaveUserGroup').on('submit',function(e) {
                e.preventDefault();
                if ( $(this).valid() ) {
                    $.ajax({
                        url:"<?php echo base_url('user_group/create_action'); ?>",
                        data:$(this).serialize(),
                        dataType : "JSON",
                        type:'POST',
                        success:function(data){
                            if(data == "0: Duplicate Group Name"){
                                swal({type: 'error', title: 'Failed: Duplicate Group Name'}, function(){ t.ajax.reload( null, false ); });
                            }else if(data == "success"){
                                swal({type: 'success', title: 'Success!'}, function(){ t.ajax.reload( null, false ); });
                            }else{
                                swal({type: 'error', title: 'Failed Insert Data.'}, function(){ t.ajax.reload( null, false ); });
                            }
                            document.getElementById("frmsaveUserGroup").reset();
                            $('#modalAddUserGroup').modal('hide');
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal({type: 'error', title: 'Failed: '+textStatus}, function(){ t.ajax.reload( null, false ); });
                            document.getElementById("frmsaveUserGroup").reset();
                            $('#modalAddUserGroup').modal('hide');
                        }
                    });
                }
            });

        //EDIT USER GROUP
        $('#frmeditUserGroup').on('submit',function(e) {
            e.preventDefault();
            if ( $(this).valid() ) {
                $.ajax({
                    url:"<?php echo base_url('user_group/update_action'); ?>",
                    data:$(this).serialize(),
                    dataType : "JSON",
                    type:'POST',
                    success:function(data){
                        if(!(data.indexOf("Failed") != -1)){
                            swal({type: 'success', title: 'Update User Group Success!'}, function(){ t.ajax.reload(null, false);});
                        }else{
                            swal({type: 'error', title: data}, function(){ t.ajax.reload( null, false ); });
                        }
                        document.getElementById("frmeditUserGroup").reset();
                        $('#modalEditUserGroup').modal('hide');
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        Swal.fire(textStatus).then(function(){ t.ajax.reload( null, false ); });
                        document.getElementById("frmeditUserGroup").reset();
                        $('#modalEditUserGroup').modal('hide');
                    }
                });
            }
        });
        //END EDIT USER GROUP

        //Open Edit Form
        function openEdit(id){
            $.ajax({
                url:"<?php echo base_url('user_group/get_edit/'); ?>"+id,
                dataType : "JSON",
                type:'GET',
                success:function(data){
                    $("#userGroupNameEdit").val(data.USER_GROUP_NAME);
                    $("#IDUserGroup").val(data.ID);
                    $('#modalEditUserGroup').modal('show');
                },
                error:function(jqXHR, textStatus, errorThrown){
                    swal(textStatus);
                    document.getElementById("frmeditUserGroup").reset();
                }
            });
        }
        //END Open Edit Form

        //Open Confirm Delete
            function confirmDelete(id){
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }, function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url:"<?php echo base_url('user_group/delete_action/'); ?>"+id,
                            dataType : "JSON",
                            type:'POST',
                            success:function(data){
                                if(data){
                                    swal({type: 'success', title: 'Delete User Group Success'}, function(){
                                     t.ajax.reload( null, false ); });
                                }else{
                                    swal({type: 'error', title: 'Delete User Group Failed'}, function(){
                                    t.ajax.reload( null, false ); });
                                }
                            },
                            error:function(jqXHR, textStatus, errorThrown){
                                swal({type: 'error', title: textStatus}, function(){ t.ajax.reload( null, false ); });
                            }
                        });
                    }
                });
            }
            //END Confirm Delete
    </script>
</body>
</html>