<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karir extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

    public function index()
	{
		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		$Url = $this->config->item('url_jobvacancy_'.$SiteLang);
		redirect(base_url().$SiteLang."/".$Url);
	}
}