<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('news_model');
        $this->load->model('content_model');

    }

    public function index()
    {
        $data = array();
        $data['content'] = "news/view";
        $data['authorize'] = $this->data['authorize'];
        $data['Data']['news'] = $this->news_model->view();
        $data['Data']['content'] = $this->get_content();

        $this->load->view('templates/view', $data);
    }
    public function detail_news($id)
    {
        $data = array();
        $data['content'] = "news/detail";
        $data['authorize'] = $this->data['authorize'];
        $data['data']['news'] = $this->news_model->get_edit($id);
        $data['data']['content'] = $this->content_model->get_edit($data['data']['news']->ID_CONTENT);
        $data['data']['back'] = base_url() . 'news';
        $this->load->view('templates/view', $data);
    }
    public function tncupload()
    {
        $data = 'Image max pixel ' . $this->config->item('max_height') . 'x' . $this->config->item('max_width') . 'px <br>
                    Allowed images are ' . str_replace("|", ", ", $this->config->item('allowed_types_image')) . ' <br>
                    Max image size ' . $this->config->item('max_size') . 'KB<br>';
        echo json_encode($data);
    }
    public function get_content()
    {
        $data_content = $this->content_model->get();
        $result = array();
        foreach ($data_content as $data) {
            $param['ID'] = $data->ID;
            $param['TITLE_ID'] = $data->TITLE_ID;
            $param['TITLE_EN'] = $data->TITLE_EN;
            $result[] = $param;
        }
        return $result;
    }

    public function add()
    {
        $TITLE = isset($_POST['title']) ? $_POST['title'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";
        $DESCRIPTION = isset($_POST['description']) ? $_POST['description'] : "";
        $ID_CONTENT = isset($_POST['id_content']) ? $_POST['id_content'] : "";
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";

        if (empty($TITLE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        } elseif (empty($FILE)) {
            $JSON = array("ErrorCode" => "EC:000B", "ErrorMessage" => "File image is required!");
            die(json_encode($JSON));
        }

        $path = $this->config->item('save_news');
        $config['allowed_types'] = $this->config->item('allowed_types');
        $config['max_size'] = $this->config->item('max_size');
        $config['max_width'] = $this->config->item('max_width');
        $config['min_width'] = $this->config->item('min_width');
        $config['max_height'] = $this->config->item('max_height');
        $config['min_height'] = $this->config->item('min_height');
        $config['file_name'] = 'NEWS_' . date('ymdHis');
        $config['upload_path'] = $this->config->item('path_news');
        $path_parts = pathinfo($FILE);
        $forUpload['extension'] = $path_parts['extension'];

        $Extension = $this->config->item('allowed_types_image_video');
        $Expfile = explode("|", $Extension);

        if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
            $this->upload->initialize($config);
            $forUpload['file_name'] = $config['file_name'];
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                die(json_encode($JSON));
            } else {
                $param['TITLE'] = $TITLE;
                $param['LANGUAGE'] = $LANGUAGE;
                $param['LINK'] = $path . $config['file_name'] . "." . $forUpload['extension'];
                $param['DESCRIPTION'] = $DESCRIPTION;
                $param['ID_CONTENT'] = $ID_CONTENT;
                $param['CREATED_BY'] = $_SESSION['username'];
                $Insert = $this->news_model->add($param);
                if ($Insert['ErrorCode'] != "EC:0000") {
                    $JSON = array("ErrorCode" => $Insert['ErrorCode'], "ErrorMessage" => $Insert['ErrorMessage']);
                    die(json_encode($JSON));
                }
                $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Added", "Data" => $param);
                die(json_encode($JSON));
            }
        } else {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
            die(json_encode($JSON));
        }
    }

    public function view()
    {
        $GetData = $this->news_model->view();
        die(json_encode($GetData));
    }

    public function update()
    {
        $FILE = isset($_FILES["file"]['name']) ? $_FILES["file"]['name'] : "";
        $ID = isset($_POST['id_news']) ? $_POST['id_news'] : "";
        $TITLE = isset($_POST['title']) ? $_POST['title'] : "";
        $LANGUAGE = isset($_POST['language']) ? $_POST['language'] : "";
        $ID_CONTENT = isset($_POST['id_content']) ? $_POST['id_content'] : "";
        $DESCRIPTION = isset($_POST['description']) ? $_POST['description'] : "";

        if (empty($TITLE)) {
            $JSON = array("ErrorCode" => "EC:000A", "ErrorMessage" => "Title is required!");
            die(json_encode($JSON));
        }

        if (!empty($FILE)) {
            if (empty($FILE)) {
                $JSON = array("ErrorCode" => "EC:000C", "ErrorMessage" => "File image is required!");
                die(json_encode($JSON));
            }

            $path = $this->config->item('save_news');
            $config['allowed_types'] = $this->config->item('allowed_types');
            $config['max_size'] = $this->config->item('max_size');
            $config['max_width'] = $this->config->item('max_width');
            $config['min_width'] = $this->config->item('min_width');
            $config['max_height'] = $this->config->item('max_height');
            $config['min_height'] = $this->config->item('min_height');
            $config['file_name'] = 'NEWS_' . date('ymdHis');
            $config['upload_path'] = $this->config->item('path_news');
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $forUpload['extension'] = $path_parts['extension'];

            $Extension = $this->config->item('allowed_types_image_video');
            $Expfile = explode("|", $Extension);

            if (in_array(strtoupper($forUpload['extension']), $Expfile)) {
                $this->upload->initialize($config);
                $forUpload['file_name'] = $config['file_name'];
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, true);
                }

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    $JSON = array("ErrorCode" => "EC:001A", "ErrorMessage" => "Fail to add ! " . $error['error']);
                    die(json_encode($JSON));
                } else {
                    $param['ID'] = $ID;
                    $param['TITLE'] = $TITLE;
                    $param['LANGUAGE'] = $LANGUAGE;
                    $param['DESCRIPTION'] = $DESCRIPTION;
                    $param['LINK'] = $path . $config['file_name'] . "." . $forUpload['extension'];
                    $param['ID_CONTENT'] = $ID_CONTENT;
                    $param['USER_LOG'] = $_SESSION['username'];
                    $param['DATE_LOG'] = date('Y-m-d H:i:s');
                    $Update = $this->news_model->updateaction($param);
                    if ($Update['ErrorCode'] != "EC:0000") {
                        $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                        die(json_encode($JSON));
                    }
                    $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
                    die(json_encode($JSON));
                }
            } else {
                $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => "Fail to add ! Extension must " . $Extension);
                die(json_encode($JSON));
            }
        } else {
            $param['ID'] = $ID;
            $param['TITLE'] = $TITLE;
            $param['LANGUAGE'] = $LANGUAGE;
            $param['DESCRIPTION'] = $DESCRIPTION;
            $param['USER_LOG'] = $_SESSION['username'];
            $param['ID_CONTENT'] = $ID_CONTENT;
            $param['DATE_LOG'] = date('Y-m-d H:i:s');
            $Update = $this->news_model->updateaction($param);
            if ($Update['ErrorCode'] != "EC:0000") {
                $JSON = array("ErrorCode" => $Update['ErrorCode'], "ErrorMessage" => $Update['ErrorMessage']);
                die(json_encode($JSON));
            }
            $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Updated", "Data" => $param);
            die(json_encode($JSON));
        }
    }

    public function remove()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $ID = isset($Data['ID']) ? $Data['ID'] : "";
        $param['ID'] = $ID;
        $Delete = $this->news_model->removeaction($param);
        if ($Delete['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Delete['ErrorCode'], "ErrorMessage" => $Delete['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deleted", "Data" => $param);
        die(json_encode($JSON));
    }
    public function deactive()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "0";
        $where = array('ID' => $id);
        $Active = $this->news_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Deactivate");
        die(json_encode($JSON));
    }

    public function active()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "88";
        $where = array('ID' => $id);
        $Active = $this->news_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Activate");
        die(json_encode($JSON));
    }
    public function publish()
    {
        $Data = isset($_POST['Data']) ? $_POST['Data'] : "";
        $id = isset($Data['ID']) ? $Data['ID'] : "";

        $data['STATUS'] = "99";
        $where = array('ID' => $id);
        $Active = $this->news_model->updatestatus($data, $where);
        if ($Active['ErrorCode'] != "EC:0000") {
            $JSON = array("ErrorCode" => $Active['ErrorCode'], "ErrorMessage" => $Active['ErrorMessage']);
            die(json_encode($JSON));
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Successfully Published");
        die(json_encode($JSON));
    }
}