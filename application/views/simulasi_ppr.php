<style type="text/css">
	.sim-input{
		width: 150%;
	}
	.result tr{
		line-height: 30px;
	}
</style>
<style type="text/css">
	ul.menu_side_left{
		list-style: none;
	}
	ul.menu_side_left li{
		padding-top: .5rem;
		padding-bottom: .2rem;
	}
	ul.menu_side_left li:hover > a{
		color: #233F8A;
		font-weight: bold;
	}
	ul.menu_side_left li a, .btn.btn-link{
		color: #414141;
		text-decoration: none;
	}
	.box-laporan{
		padding-left: 1.5rem;
	}
	.item-laporan{
		box-shadow: 2px 0px 4px #e5e5e5;
	    max-width: 90%;
	    padding: 12px 15px;
	    display: flex;
	    align-items: center;
	}
	.item-laporan .ext_file{
		text-transform: uppercase;
		padding: 5px;
	    margin-right: .5rem;
	    font-size: 11px;
	    width: 35px;
    	text-align: center;
	}
	.item-laporan .ext_file.pdf{
		background: #233F8A;
		color: #fff;
	}
	div.card-loker{
		box-shadow: 0px 1px 3px 1px #dfdfde;
		cursor: pointer;
	}

	div.card-loker:hover{
		background-color: #28448f;
		color: white;
	}
</style>
<?php
$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
$MENU_PARENT = isset($navbar_link['MENU_PARENT']) ? $navbar_link['MENU_PARENT']:"";
$MENU_CHILD = isset($navbar_link['MENU_CHILD']) ? $navbar_link['MENU_CHILD']:"";
$MENU_PARENT_LINK = isset($navbar_link['MENU_PARENT_LINK']) ? $navbar_link['MENU_PARENT_LINK']:"";
$jobvacancy = array();

$Amout = isset($_POST['amount']) ? $_POST['amount']:0;
$Dp = isset($_POST['downpayment']) ? $_POST['downpayment']:0;
$Interest = isset($_POST['interest']) ? $_POST['interest']:0;
$Tenor = isset($_POST['tenor']) ? $_POST['tenor']:"";
$Anuitas = isset($_POST['anuitas']) ? $_POST['anuitas']:"";

?>
<div class="row row-header">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="box-header">
			<h4 style="margin-bottom: 0 !important; "><?php echo $this->lang->line('simulation-0'); ?></h4>
		</div>
	</div>
</div>
<div class="row row-page-module">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="font-size: 14px;">
		<a href="<?php echo base_url(); ?>" style="color: #707070 !important;"><?php echo $this->lang->line('home') ?></a> / 
		<label style="color: #707070;"><?php echo $this->lang->line('simulation-0'); ?></label> /
		<label style="color: #707070;"><?php echo $this->lang->line('simulation-7'); ?></label>
	</div>
</div>
<div class="row" style="padding-top: 2%; padding-bottom: 2%;"  id="contents">
	<!-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4"></div> -->
	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
        <?php require_once(APPPATH.'views/nav_left_joy.php'); ?>
	</div>
	<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="padding-bottom: 5px;">
				<label style="font-weight: bold;font-size: 18px !important;"><?php echo $this->lang->line('simulation-7'); ?></label><br>
				<label style="font-weight: bold;font-size: 16px !important;"><?php echo $this->lang->line('simulation-9'); ?> <?php echo $this->lang->line('simulation-5'); ?></label>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-bottom: 5px;">
				<label><?php echo $this->lang->line('simulation-1'); ?></label>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="padding-bottom: 5px;">
				<input type="text" id="amountppm" name="harga" class="form-control" required="" onkeydown="return numbersonly(this, event);" onkeypress="return isNumber2(event)" value="<?php echo $Amout ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-bottom: 5px;">
				<label>DP %</label>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="padding-bottom: 5px;">
				<input type="text" name="dp" placeholder="Contoh : 2.5" class="form-control" required="" onkeypress="return isNumber(event)" value="<?php echo $Dp ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-bottom: 5px;">
				<label><?php echo $this->lang->line('simulation-2'); ?></label> %
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="padding-bottom: 5px;">
				<input type="text" name="bunga" placeholder="Contoh : 2.5" class="form-control" required="" onkeypress="return isNumber(event)" value="<?php echo $Interest ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-bottom: 5px;">
				<label><?php echo $this->lang->line('simulation-3'); ?></label>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="padding-bottom: 5px;">
				<select name="jangka" id="jangka" class="form-control">
					<option>--<label><?php echo $this->lang->line('simulation-4'); ?></label>--</option>
					<?php 
					for($i=1; $i<=15; $i++){
						if($Tenor == $i){
							$selected = "selected";
						}else{
							$selected = "";
						}
					?>
						<option value="<?php echo $i?>" <?php echo $selected ?>><?php echo $i?></option>
					<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-bottom: 5px;">
				<label><?php echo $this->lang->line('simulation-5'); ?></label>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="padding-bottom: 5px;">
				<input type="text" class="form-control" value="Anuitas" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" style="padding-bottom: 5px;"></div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="padding-bottom: 5px;">
				<input type="button" class="btn btn-primary" id="calculate" value="<?php echo $this->lang->line('simulation-6')?>" style="background-color: #28448f;">
			</div>
		</div>
	</div>
	<!-- <h1>Simulasi Kredit PPR</h1> -->
</div>
<div class="row" id="contentSimulation" style="padding-top: 2%; padding-bottom: 5%; display: none;">
	<div class="col-xl-3 col-lg-3 col-md-3"></div>
	<div class="col-xl-7 col-lg-7 col-md-8 col-sm-12 col-12">
		<div class="card">
			<div class="card-body result" style="background-color: #e6e6e6; font-size: 14px; text-align: justify;">
				<label style="font-weight: bold;font-size: 16px !important;"><?php echo $this->lang->line('simulation-10')?></label>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 d-padding">
						<?php echo $this->lang->line('simulation-10')?>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
						: <span id="textHarga"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 d-padding">
						DP
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
						: <span id="textDp"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 d-padding">
						<?php echo $this->lang->line('simulation-12')?>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
						: <span id="textPlafond"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 d-padding">
						<?php echo $this->lang->line('simulation-13')?>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
						: <span id="textJangka"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 d-padding">
						<?php echo $this->lang->line('simulation-2')?>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
						: <span id="textBunga"></span>% / <?php  echo $this->lang->line('simulation-14') ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 d-padding">
						<?php  echo $this->lang->line('simulation-16') ?> / <?php  echo $this->lang->line('simulation-15') ?>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-padding">
						: <span id="textAngsuran"></span>
					</div>
				</div>
				<div class="row" style="padding-top: 20px;">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-padding">
						<label style="font-size: 12px !important; font-style: italic;">*<?php  echo $this->lang->line('simulation-17') ?></label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <h1>Simulasi Kredit PPR</h1> -->
</div>
<script src="https://code.jquery.com/jquery-3.6.0.js" inte
grity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<style type="text/css">
	label, input, select{
		font-size: 14px !important;
	}	
	table.result tr td{
		font-size: 14px;
	}
</style>

<script src="<?php echo base_url('assets/js/loadingoverlay.js'); ?>"></script>

<script type="text/javascript">
	$(function(){
		var ppm = document.getElementById('amountppm');
		ppm.addEventListener('keyup', function(e){
			ppm.value = formatRupiah(this.value, '');
		});
	});
	function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)){
        	if(iKeyCode == 188 || iKeyCode == 190){
        		return true;
        	}
            return false;
        }else{
        	return true;
        }
    }

    function isNumber2(evt) {
    	// alert(evt);
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        // alert(iKeyCode);
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57) || iKeyCode == 46){
        	// if(iKeyCode == 190){
        	// 	alert('tes');
        	// 	return false;
        	// }
            return false;
        }else{
        	return true;
        }

    }    

	function loader(type){
		$.LoadingOverlay(type, {
		    image       : "",
		    fontawesome : "fa fa-spinner fa-spin"
		});
	}

	function formatMoney(amount, decimalCount = 0, decimal = ".", thousands = ",") {
	  try {
	    decimalCount = Math.abs(decimalCount);
	    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

	    const negativeSign = amount < 0 ? "-" : "";

	    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
	    let j = (i.length > 3) ? i.length % 3 : 0;

	    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	  } catch (e) {
	    console.log(e)
	  }
	};

	$(document).ready(function(){
		<?php 
		if(isset($_POST)){
		?>
			setTimeout(function(){
				$('#calculate').trigger('click');
			}, 500);
		<?php
		}
		?>
		$('#calculate').click(function(){
			loader("show");
			var harga = $('input[name=harga]').val();
			harga = harga.replaceAll(".","");

			var dp = $('input[name=dp]').val();
			var bunga = $('input[name=bunga]').val();
			var jangkawaktu = document.getElementById('jangka').value;

			var url = '<?php echo base_url()?>main/ppr';
			var data = {
                'harga' : harga,
                'bunga' : bunga,
                'tenor' : jangkawaktu,
                'dp' : dp
            }

            $.ajax({
                url : url,
                type : 'post',
                cache : false,
                dataType : 'json',
                data : data,
                success : function(res){
                    $('#textHarga').html(formatMoney(res['harga']));
                    $('#textDp').html(formatMoney(res['dp']));
                    $('#textPlafond').html(formatMoney(res['plafond']));
                    $('#textJangka').html(res['tenor']);
                    $('#textBunga').html(res['bunga']);
                    $('#textAngsuran').html(formatMoney(res['anuitas']));
                }
            });

			// var dp_awal = (harga*dp)/100;
			// var pokok_pinjaman = harga - dp_awal;
			// var jangka_kredit = jangkawaktu * 12;
			// var jumlah_bunga = (bunga*pokok_pinjaman/100) * jangkawaktu;
			// var cicilan = (pokok_pinjaman + jumlah_bunga)/jangka_kredit;

			// // alert(cicilan);
			// // alert(Math.pow(4,3));
			// var pow = 1+jumlah_bunga/12/100;
			// console.log('jumlah bunga : '+Math.pow(pow,jangka_kredit));
			// // var 
			// // alert(pow);
			// var anuitas = (pokok_pinjaman * (jumlah_bunga/12/100))/(1-(1/Math.pow(pow,jangka_kredit)));
			// alert(anuitas);
			// if(cicilan == "Infinity" || cicilan == "NaN"){
			// 	cicilan = 0;
			// }

			// $('#textHarga').html(formatMoney(harga));
			// $('#textDp').html(formatMoney(dp_awal));
			// $('#textPlafond').html(formatMoney(pokok_pinjaman));
			// $('#textJangka').html(jangka_kredit);
			// $('#textBunga').html(bunga);
			// $('#textAngsuran').html(formatMoney(anuitas));

			$('#contentSimulation').css('display','');
			loader("hide");
		});
	});
</script>