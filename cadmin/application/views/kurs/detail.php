<?php
$is_read = $authorize['is_read'];
$back = isset($data['back']) ? $data['back'] : '';
$kurs = isset($data['kurs']) ? $data['kurs'] : array();
$kurs_detail = isset($data['detail']) ? $data['detail'] : array();

$ID_KURS_HEADER = isset($kurs->ID_KURS_HEADER) ? $kurs->ID_KURS_HEADER : "";
$DESCRIPTION = isset($kurs->DESCRIPTION) ? $kurs->DESCRIPTION : "";
$EFFECTIVE_DATE = isset($kurs->EFFECTIVE_DATE) ? date('d M Y H:i:s', strtotime($kurs->EFFECTIVE_DATE)) : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dropify-wrapper {
    height: 100%;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Master Kurs</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Master Kurs</span> - Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_read): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveKurs">
            <div class="card-header bg-blue-800 d-flex justify-content-between">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Effective Date<span style="color:orange">*</span></label>

                        <h5><?php echo $EFFECTIVE_DATE ?></h5>
                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">Kurs Detail</h6>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="workhistory">
                        <thead>
                            <tr>
                                <th width="200px">Currency Code</th>
                                <th>Currency Name</th>
                                <th>Buy</th>
                                <th>Sell</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0;foreach ($kurs_detail as $detail) {$no++;?>

                            <tr>
                                <td><?php echo $detail['CURRENCY_CODE'] ?></td>
                                <td><?php echo $detail['CURRENCY_NAME'] ?></td>
                                <td><?php echo $detail['BUY'] ?> </td>
                                <td><?php echo $detail['SELL'] ?> </td>
                            </tr>
                            <?php }?>

                        </tbody>
                    </table>
                </div>




            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Back</button></a>
                </div>
            </div>
        </form>
    </div>

    <?php endif;?>


</div>

<script type="text/javascript">
$(function() {
    $('.select2').select2();
    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });
});
var room = 0;
</script>