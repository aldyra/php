<?php
$param = isset($param) ? $param : "";
$ID = isset($param['ID']) ? $param['ID'] : "";
$NAME_ID = isset($param['NAME_ID']) ? $param['NAME_ID'] : "";
$NAME_EN = isset($param['NAME_EN']) ? $param['NAME_EN'] : "";
$LINK_ID = isset($param['LINK_ID']) ? $param['LINK_ID'] : "";
$LINK_EN = isset($param['LINK_EN']) ? $param['LINK_EN'] : "";
$IS_CONTENT = isset($param['IS_CONTENT']) ? $param['IS_CONTENT'] : "";
$MENU_PARENT = isset($param['MENU_PARENT']) ? $param['MENU_PARENT'] : "";
$MENU_LEVEL = isset($param['MENU_LEVEL']) ? $param['MENU_LEVEL'] : "";
$POSITION = isset($param['POSITION']) ? $param['POSITION'] : "";
$FOOTER_NOTE_ID = isset($param['FOOTER_NOTE_ID']) ? $param['FOOTER_NOTE_ID'] : "";
$FOOTER_NOTE_EN = isset($param['FOOTER_NOTE_EN']) ? $param['FOOTER_NOTE_EN'] : "";
$SEQUENCE_HEADER = isset($param['SEQUENCE_HEADER']) ? $param['SEQUENCE_HEADER'] : "";
$SEQUENCE_FOOTER = isset($param['SEQUENCE_FOOTER']) ? $param['SEQUENCE_FOOTER'] : "";
$STATUS_DATA = isset($param['STATUS']) ? $param['STATUS'] : "";
$menu = isset($param['menu']) ? $param['menu'] : array();
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

<?php
$folders_arr = array();
foreach ($menu as $row) {
    $parentid = $row['MENU_PARENT'];
    $type = 'child';
    if ($parentid == '0' || $parentid == null) {
        $parentid = '#';
        $type = 'default';
    }

    $selected = false;
    $opened = false;
    $checkbox_disabled = false;

    if ($row['ID'] == $MENU_PARENT) {
        $selected = true;
        $opened = true;
    }
    if ($row['ID'] == $ID) {
        $checkbox_disabled = true;

    }
    $folders_arr[] = array(
        "id" => $row['ID'],
        "parent" => $parentid,
        "data" => $row['MENU_LEVEL'],
        "type" => $type,
        "text" => $row['NAME_ID'] . '/' . $row['NAME_EN'],
        "state" => array("selected" => $selected, "opened" => $opened, "checkbox_disabled" => $checkbox_disabled),
    );

}
?>
<style>
.jstree-default .jstree-clicked {
    background: #00b1ff;
    border-radius: 2px;
    box-shadow: inset 0 0 1px #999;
}

jstree-default .jstree-hovered {
    background: #56a4ae;
    border-radius: 2px;
    box-shadow: inset 0 0 1px #999;
}
</style>


<div class="row">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            MENU NAME INDONESIAN<span style="color:orange">*</span>
        </label>
        <input type="hidden" id="id" name="id" value="<?php echo $ID; ?>" />

        <input type="text" name="name_id" class="form-control" id="name_id" placeholder=""
            data-parsley-trigger="focusin focusout" required="" value="<?php echo $NAME_ID; ?>">
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            MENU NAME ENGLISH<span style="color:orange">*</span>
        </label>
        <input type="text" name="name_en" class="form-control" id="name_en" placeholder=""
            value="<?php echo $NAME_EN; ?>" required="">
    </div>
</div>
<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <label>
            Link Indonesian<span style="color:orange">*</span>
        </label>
        <input type="text" name="link_id" class="form-control" id="link_id" placeholder="" required=""
            data-parsley-trigger="focusin focusout" data-parsley-length="[3, 100]"
            data-parsley-pattern="^[a-z0-9]+(?:-[a-z0-9]+)*$" value="<?php echo $LINK_ID; ?>">
    </div>
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <label>
            Link English<span style="color:orange">*</span>
        </label>
        <input type="text" name="link_en" class="form-control" id="link_en" placeholder="" required=""
            data-parsley-trigger="focusin focusout" data-parsley-length="[3, 100]"
            data-parsley-pattern="^[a-z0-9]+(?:-[a-z0-9]+)*$" value="<?php echo $LINK_EN; ?>">
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="1" name="is_content" id="is_content" <?php if ($IS_CONTENT == 1) {
    echo 'checked';
}
?>>
                MENU CONTENT
            </label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-md-12">
        <label>Position<span style="color:orange">*</span></label>
        <select id="position" class="form-control select2modal" name="position" required>
            <option selected disabled>Choose Option</option>
            <option value="1">HEADER</option>
            <option value="2">FOOTER</option>
            <option value="3">FOOTER & HEADER</option>

        </select>
    </div>
</div>
<div class="row" style="display:none;" id="sequence_headerdiv">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            Sequence for Header<span style="color:orange">*</span>
        </label>
        <input type="text" name="sequence_header" class="form-control" id="sequence_header" onkeyup="validate(this);"
            data-parsley-minlength="1" data-parsley-maxlength="2" data-parsley-trigger="focusin focusout" placeholder=""
            value="<?php echo $SEQUENCE_HEADER ?>">
    </div>
</div>
<div class="row" style="display:none;" id="sequence_footerdiv">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            Sequence for Footer<span style="color:orange">*</span>
        </label>
        <input type="text" name="sequence_footer" class="form-control" id="sequence_footer" onkeyup="validate(this);"
            data-parsley-minlength="1" data-parsley-maxlength="2" data-parsley-trigger="focusin focusout" placeholder=""
            value="<?php echo $SEQUENCE_FOOTER ?>">
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            Footer Note Indonesian
        </label>
        <textarea type="text" name="footer_note_id" class="form-control" id="footer_note_id"
            rows="4"><?php echo $FOOTER_NOTE_ID ?></textarea>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            Footer Note
        </label>
        <textarea type="text" name="footer_note_en" class="form-control" id="footer_note_en"
            rows="4"><?php echo $FOOTER_NOTE_EN ?></textarea>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-md-12">
        <label>MENU LEVEL<span style="color:orange">*</span></label>
        <select id="parent" class="form-control select2modal" name="parent" required>
            <option selected disabled>Choose Option</option>
            <option value="menu_parent">MENU PARENT</option>
            <option value="menu_child">MENU CHILD</option>
        </select>
    </div>
</div>
<div class="row" style="display:none;" id="menuParent">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>
            CHOOSE MENU PARENT
        </label>
        <div id="folder_jstree"></div>
        <input type="hidden" id="menu_parent" name="menu_parent" value="<?php echo $MENU_PARENT; ?>" />
        <input type="hidden" id="menu_level" name="menu_level" value="<?php echo $MENU_LEVEL; ?>" />
    </div>
    <textarea id='txt_folderjsondata' style="display:none;"><?=json_encode($folders_arr)?></textarea>
</div>

<script type="text/javascript">
$(document).ready(function() {


    var folder_jsondata = JSON.parse($('#txt_folderjsondata').val());

    $('#folder_jstree').jstree({
        'core': {
            'data': folder_jsondata,
            'multiple': false
        },
        'checkbox': {
            'three_state': false,
        },
        'plugins': ['checkbox', 'types'],
        'types': {
            'default': {
                'icon': 'fa fa-angle-double-down'
            },
            'child': {
                'icon': 'fa fa-angle-double-right'
            }
        },
    });

});

$(function() {
    $(".select2modal").select2();

    $("#folder_jstree").on(
        "select_node.jstree",
        function(evt, data) {
            $menu_parent = data.node.id;
            $menu_level = data.node.data;

            $('#menu_parent').val($menu_parent);
            $('#menu_level').val($menu_level);
        }
    );
    $('[name="parent"]').change(function() {
        var menu = $(this).val();
        console.log(menu);
        if (menu == 'menu_child') {
            $('#menuParent').show();
        } else {
            $('#menuParent').hide();
        }
    });

    if (<?php echo $MENU_PARENT; ?> != '' || <?php echo $MENU_PARENT; ?> != '0') {
        $("#parent").val("menu_child").trigger('change');

    } else {
        $("#parent").val("menu_parent").trigger('change');

    }
    $('#position').change(function() {
        var position = $(this).val();
        if (position == '1') {
            document.getElementById("sequence_header").required = true;
            document.getElementById("sequence_footer").required = false;
            $('#sequence_footer').val('');
            $('#sequence_headerdiv').show();
            $('#sequence_footerdiv').hide();
        } else if (position == '2') {
            document.getElementById("sequence_header").required = false;
            document.getElementById("sequence_footer").required = true;
            $('#sequence_header').val('');
            $('#sequence_headerdiv').hide();
            $('#sequence_footerdiv').show();
        } else if (position == '3') {
            document.getElementById("sequence_header").required = true;
            document.getElementById("sequence_footer").required = true;
            // $('#sequence_header').val('');
            // $('#sequence_footer').val('');
            $('#sequence_headerdiv').show();
            $('#sequence_footerdiv').show();

        }
    });
    $("#position").val("<?php echo $POSITION ?>").trigger('change');

});
</script>