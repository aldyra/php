<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bank Bumi Arta</title>

    <!-- INCLUDE SCRIPT TOP -->
        <?php require_once APPPATH . 'views/templates/script_top.php';?>
    <!-- END INCLUDE SCRIPT TOP -->

    <style type="text/css">
        label.error:before {
            content: "" !important;
            font-family: "icomoon" !important;
            position: absolute !important;
            left: 2px !important;
            top: -1px !important;
        }
        label.error {
            list-style: none !important;
            color: #ff5b5b !important;
            margin: 5px 0px 0px 0px !important;
            padding-left: 20px !important;
            position: relative !important;
            font-size: 14px !important;
            font-weight: 400 !important;
        }
        input:disabled {
            color: rgb(84, 84, 84) !important;
            cursor: default !important;
        }

        ul.helper-text {
            padding-left: 15px;
            display: block;
            font-size: 13px;
            color: #808080;
            list-style-type: none;
            margin-bottom: 0;
        }
        li.invalid{
            display: none;
        }
    </style>

</head>

<body>

    <!-- OPEN NAV MENU -->
    <?php require_once APPPATH . 'views/templates/topbar.php';?>
    <!-- END NAV MENU -->

    <!-- Page content -->
    <div class="page-content">

        <!-- OPEN LEFT NAV BAR -->
        <?php require_once APPPATH . 'views/templates/nav_menu.php';?>
        <!-- END LEFT NAV BAR -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- OPEN MODAL -->
            <?php require_once APPPATH . 'views/templates/modals.php';?>
            <!-- END MODAL -->

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4><span class="font-weight-semibold">Manage User</span></h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active">Manage User</span>
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Order direction sequence control -->
                <div class="card">
                    <div class="card-body">
                        <?php if ($authorize['is_create']): ?>
                        <button type="button" class="btn btn-info" id="btnAddUser" data-toggle="modal" data-target="#modalAddUser">Add User</button>
                        <?php endif;?>
                    </div>

                    <table id="tableData" class="table datatable-complex-header">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Created By</th>
                                <th data-orderable="false"  width="200">Created Date</th>
                                <th>User Log</th>
                                <th>Date Log</th>
                                <th data-orderable="false">Status</th>
                                <th  data-orderable="false">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /order direction sequence control -->

            </div>
            <!-- /content area -->


            <!-- OPEN FOOTER -->
            <?php require_once APPPATH . 'views/templates/footer.php';?>
            <!-- END FOOTER -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script src="<?php echo base_url(); ?>assets/js/passwordValidator.js"></script>

    <script type="text/javascript">
        <?php if (!$authorize['is_update']) {?>
            var colDef = [{
                            "targets": [6,7],
                            "visible": false,
                            "searchable": false
                        }];
        <?php } else {?>
            var colDef = [{
                            "targets": [7],
                            "visible": false,
                            "searchable": false
                        }];
        <?php }?>
        var arr = ["ID", "USERNAME", "CREATED_BY", "CREATED_DATE", "USER_LOG", "DATE_LOG", "STATUS"];
        var t = $("#tableData").DataTable({
            serverSide: true,
            autoWidth: false,
            ajax: {
                "url": "<?php echo base_url('users/get_data') ?>",
                "type": "POST",
                "data": {"arr": arr},
            },
            columns: [
                { "data": "USERNAME" },
                { "data": "CREATED_BY" },
                { "data": "CREATED_DATE" },
                { "data": "USER_LOG" },
                { "data": "DATE_LOG" },
                { "data": "STATUS" },
                {
                    "className": 'text-center',
                    "orderable": false,
                    "data": "ACTION"
                },
                { "data": "ID" }
            ],
            columnDefs: colDef,
            keys: !0,
            order: [[ 7, "desc" ]],
            autofill: true,
            select: true,
            responsive: true,
            buttons: true,
            length: 5
        });

        var frmAdd = 'frmsaveUser';
        var frmEdit = 'frmeditUser';
        var $formSave = $("#"+frmAdd);
        var $formEdit = $("#"+frmEdit);
        var $submitAdd = $("#submitAddUser");
        var $submitEdit = $("#submitEditUser");

        var checkValid = function(act){
            switch(act){
                case 'add':
                if( ($formSave.parsley().isValid()) && (validPass) )
                  $submitAdd.removeAttr("disabled");
                else
                  $submitAdd.attr("disabled", "disabled");
                break;

                case 'edit':
                if( ($formEdit.parsley().isValid()) && (validPass) )
                  $submitEdit.removeAttr("disabled");
                else
                  $submitEdit.attr("disabled", "disabled");
                break;
            }
        }

        $("#"+frmAdd+" :input").keyup(function(){
            $(this).parsley().validate();
            checkValid('add');
        });
        $("#"+frmAdd+" :input").change(function(){
            $(this).parsley().validate();
            checkValid('add');
        });
        $("#"+frmEdit+" :input").keyup(function(){
            $(this).parsley().validate();
            checkValid('edit');
        });
        $("#"+frmEdit).on('change', ':input', function(){
            $(this).parsley().validate();
            checkValid('edit');
        });

        $("#btnAddUser").on('click', function(){
            $.ajax({
                type: "GET",
                url: "<?php echo site_url('users/get_group'); ?>",
                timeout : 3000,
                dataType: "JSON",
                error: function() {
                    alert("ERROR!");
                },
                success: function(data) {
                    $("#frmsaveUser").find("#group_user").html(data);
                }
            });
        });

        $('#frmsaveUser').on('submit',function(e) {
                e.preventDefault();
                if ( ($(this).parsley().isValid()) && (validPass) ) {
                    $.ajax({
                        url:"<?php echo base_url('users/create_action'); ?>",
                        data:$(this).serialize(),
                        dataType : "JSON",
                        type:'POST',
                        success:function(data){
                            if(!(data.indexOf("Failed") != -1)){
                            swal({type: 'success', title: data}, function(){ t.ajax.reload(null, false);});
                            }else{
                                swal({type: 'error', title: data}, function(){ t.ajax.reload( null, false ); });
                            }
                            document.getElementById("frmsaveUser").reset();
                            $('#modalAddUser').modal('hide');
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal({type: 'error', title: 'Failed: '+textStatus}, function(){ t.ajax.reload( null, false ); });
                            document.getElementById("frmsaveUserGroup").reset();
                            $('#modalAddUser').modal('hide');
                        }
                    });
                }
            });

        //EDIT USER GROUP PRIVILEGE
        $('#frmeditUser').on('submit',function(e) {
            e.preventDefault();
            if ( ($(this).parsley().isValid()) && (validPass) ) {
                $.ajax({
                    url:"<?php echo base_url('users/update_action'); ?>",
                    data:$(this).serialize(),
                    dataType : "JSON",
                    type:'POST',
                    success:function(data){
                        if(!(data.indexOf("Failed") != -1)){
                            swal({type: 'success', title: data}, function(){ t.ajax.reload(null, false);});
                        }else{
                            swal({type: 'error', title: data}, function(){ t.ajax.reload( null, false ); });
                        }
                        document.getElementById("frmeditUser").reset();
                        $('#modalEditUser').modal('hide');
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        Swal.fire(textStatus).then(function(){ t.ajax.reload( null, false ); });
                        document.getElementById("frmeditUser").reset();
                        $('#modalEditUser').modal('hide');
                    }
                });
            }
        });
        //END EDIT USER GROUP

        function confirmDelete(id){
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this Data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:"<?php echo base_url('users/delete_action/'); ?>"+id,
                        dataType : "JSON",
                        type:'POST',
                        success:function(data){
                            if(!(data.indexOf("Failed") != -1)){
                                swal({type: 'success', title: 'Delete Data Success'}, function(){
                                 t.ajax.reload(null, false);
                                 });
                            }else{
                                swal({type: 'error', title: data}, function(){
                                 t.ajax.reload( null, false );
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal({type: 'error', title: 'Failed: '+textStatus}, function(){ t.ajax.reload( null, false ); });
                        }
                    });
                } else {
                    swal("Cancelled", "Your data is safe :)", "error");
                }
            });
        }

        $(function(){
            $("#btnAddUser").click(function(){
                document.getElementById("frmsaveUser").reset();
                $submitAdd.attr("disabled", "disabled");
                $('.helper-text li').addClass('invalid');
                $('.passwordValidator').css('border', '1px solid #ddd');

                setTimeout(function(){
                    $('#frmsaveUser').parsley().reset();
                }, 100);
            });
        });

        //Open Edit Form
        function openEdit(id){
            $('#frmeditUser').parsley().reset();
            $('.helper-text li').addClass('invalid');
            $('.passwordValidator').css('border', '1px solid #ddd');
            $('.passwordValidator').val('');
            validPass = true;
            $.ajax({
                url:"<?php echo base_url('users/get_edit/'); ?>"+id,
                dataType : "JSON",
                type:'GET',
                success:function(data){
                    $("#IDUser").val(data.profile.ID);
                    $("#usernameEdit").val(data.profile.USERNAME);
                    $("#nameEdit").val(data.profile.NAME);
                    $("#emailEdit").val(data.profile.EMAIL);
                    // $("#positionEdit").val(data.profile.POSITION);
                    $("#frmeditUser").find("#group_userEdit").html(data.group);
                    $('#modalEditUser').modal('show');
                },
                error:function(jqXHR, textStatus, errorThrown){
                    swal(textStatus);
                    document.getElementById("frmeditUser").reset();
                }
            });
        }
        //END Open Edit Form

        // CHANGE STATUS
        function statusChg(id){
            swal({
                title: "Are you sure?",
                // text: "Your will not be able to recover this Data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:"<?php echo base_url('Users/statusChg/'); ?>"+id,
                        dataType : "JSON",
                        type:'GET',
                        success:function(data){
                            if(data.indexOf("Success") != -1){
                                swal({type: 'success', title: data}, function(){
                                 t.ajax.reload(null, false);
                             });
                            }else{
                                swal({type: 'error', title: data}, function(){
                                 t.ajax.reload( null, false );
                             });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            swal(textStatus, function(){
                                t.ajax.reload(null, false);
                            });
                        }
                    });
                } else {
                    swal("Cancelled", "Status not change :)", "error");
                }
            });
        }
        // CHANGE STATUS

    </script>
</body>
</html>