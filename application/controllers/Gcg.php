<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gcg extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index()
	{
		$post = array();
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$link = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$category_id = 1;

		$provideData = $this->provideData();
		// $getMenuLeft = $this->getMenuLeft();
		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/pagination.min.js";
		$this->data['Content'] = "gcg";
		$this->data['data'] = array();
		$this->data['contentData'] = $provideData;


		// echo "<pre>";
		// print_r($this->data);
		// echo "</pre>";exit;

		$this->load->view('template', $this->data);
	}

	public function list_lapor()
	{
		// $post = $_POST;
		$param = $_POST['param'];

		// echo json_encode($post);
		// exit;

		// --------PAGING--------- //
		$page = 1;
		$perPage = 2;
		if(isset($param['paramPage'])) $page = $param['paramPage'];
		if(isset($param['paramPerPage'])) $perPage = $param['paramPerPage'];
		$offset = ($page - 1) * $perPage;
		// --------PAGING--------- //

		// ---- SEARCH ----- //
		$search = "";
		if(isset($param['paramSearch'])) $search = $param['paramSearch'];
		// ---- SEARCH ----- //

		$where['STATUS'] = '99';
		// $where['CATEGORY'] = $param['paramCategory'];

		$like = array();
		if(!empty($search)){
			$like[$this->cekLang('DESCRIPTION')] = $search;
		}

		// SELECT * FROM MS_RUPS_HEADER WHERE STATUS = '99' ORDER BY CREATED_DATE DESC
		$getLaporan = $this->db->select("*")
		->where($where)
		->like($like)
		->from('MS_GCG_HEADER')
		->order_by('CREATED_DATE', 'DESC')
		->limit($perPage, $offset)
		->get()
		->result_array();

		$total_laporan = $this->db->select("*")
		->where($where)
		->like($like)
		->from('MS_GCG_HEADER')
		->get()
		->num_rows();

		$totalPerPage = $perPage;
		if(sizeof($getLaporan) < $totalPerPage){
			$totalPerPage = sizeof($getLaporan);
		}
		
		$recFrom = $offset + 1;
		$recTo = ($recFrom - 1) + $totalPerPage;
		$record = $recFrom .'-'. $recTo;
		if($recFrom == $recTo){
			$record = $recFrom.'';
		}

		foreach ($getLaporan as $key => $value) {

				$getLaporan[$key]['DESCRIPTION'] = $value[$this->cekLang('DESCRIPTION')];

				$detail = $this->db->query("SELECT * FROM MS_GCG_DETAIL WHERE ID_GCG_HEADER = ".$value['ID_GCG_HEADER'])->result_array();

				foreach ($detail as $key2 => $value2) {
					$getLaporan[$key]['DETAIL'][$key2]['NAME'] = $value2[$this->cekLang('NAME')];
					$getLaporan[$key]['DETAIL'][$key2]['LINK'] = $value2[$this->cekLang('LINK')];

					$fileName = explode('/', $value2[$this->cekLang('LINK')]);
					$fileName = $fileName[sizeof($fileName) - 1];

					$fileExt = explode('.', $fileName);
					$fileExt = $fileExt[sizeof($fileExt) - 1];

					$getLaporan[$key]['DETAIL'][$key2]['FILE_NAME'] = $fileName;
					$getLaporan[$key]['DETAIL'][$key2]['FILE_EXT'] = $fileExt;
				}

		}

		$result = array(
			'getLaporan' => $getLaporan,
			'totalLaporan' => $total_laporan,
			'record' => $record
		);

		echo json_encode($result);
	}

	function provideData()
	{
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$ExpLing = explode("/", $REQUEST_URI);
		$linkingg = isset($ExpLing[3]) ? $ExpLing[3]:"";

		$ID_MENU_PARENT = 23;
		$getMenuLeft = array();
		$getMenu1 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$ID_MENU_PARENT)->result_array();

		foreach ($getMenu1 as $key => $value) {
			$getMenuLeft[$key]['menu_1']['NAME'] = $value[$this->cekLang('NAME')];
			$getMenuLeft[$key]['menu_1']['LINK'] = $value[$this->cekLang('LINK')];

			// cek aktif ga menu 1 single
			$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = ( $value[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

			$getMenu2 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value['ID'])->result_array();

			if (count($getMenu2) > 0) {
				// deklarasi aktif ga menu 1 multiple
				$stakMenu2 = array();
				$linkMenu1 = $value[$this->cekLang('LINK')];

				foreach ($getMenu2 as $key2 => $value2) {
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['NAME'] = $value2[$this->cekLang('NAME')];
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['LINK'] = $value2[$this->cekLang('LINK')];

					// cek aktif ga menu single
					$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = ( $value2[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

					$getMenu3 = $this->db->query("SELECT * FROM MS_MENU WHERE MENU_PARENT = ".$value2['ID'])->result_array();

					if (count($getMenu3) > 0) {
						// deklarasi aktif ga menu 2 multiple
						$stakMenu3 = array();
						$linkMenu2 = $value2[$this->cekLang('LINK')];

						foreach ($getMenu3 as $key3 => $value3) {
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['NAME'] =
							 	$value3[$this->cekLang('NAME')];
							$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['LINK'] =
							 	$value3[$this->cekLang('LINK')];

							 $getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'][$key3]['ACTIVE_URL'] = ( $value3[$this->cekLang('LINK')] == $linkingg ) ? TRUE : FALSE;

							$stakMenu3[] = $value3[$this->cekLang('LINK')];
						}

						// cek aktif ga menu 2 multiple
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['ACTIVE_URL'] = (in_array($linkingg, $stakMenu3)) ? TRUE : FALSE;

					}else{
						$getMenuLeft[$key]['menu_1']['menu_2'][$key2]['menu_3'] = array();
					}

					$stakMenu2[] = $value2[$this->cekLang('LINK')];
				}

				// cek aktif ga menu 1 multiple
				$getMenuLeft[$key]['menu_1']['ACTIVE_URL'] = (in_array($linkingg, $stakMenu2)) ? TRUE : FALSE;

			}else{
				$getMenuLeft[$key]['menu_1']['menu_2'] = array();
			}
		}

		$getMenuDetail = $this->db->query("SELECT * FROM MS_MENU WHERE LINK_ID = '".$linkingg."' OR LINK_EN = '".$linkingg."'")->row_array();
		$getMenuDetail['FOOTER_NOTE'] = $getMenuDetail[$this->cekLang('FOOTER_NOTE')];
		$getMenuDetail['FILE_BANNER'] = $getMenuDetail[$this->cekLang('FILE_BANNER')];

		return array('menuLeft' => $getMenuLeft, 'menuDetail' => $getMenuDetail);
	}

	function cekLang($name)
	{
		if ($_SESSION['lang'] == 'id') {
			$nameLang = $name.'_ID';
		}else{
			$nameLang = $name.'_EN';
		}

		return $nameLang;
	}
}