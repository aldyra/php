<?php
class LanguageLoader
{
   	function initialize() {
		$ci =& get_instance();
		$ci->load->helper('language');
		$siteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang'] : "";
		
		$REQUEST_URI = $_SERVER['REQUEST_URI'];
		$exp = explode("/", $REQUEST_URI);
		$language = isset($exp[2]) ? $exp[2]:"";
		
		if(in_array($language, array("id","en"))){
			$_SESSION['site_lang'] = $language;
			$_SESSION['lang'] = $language;
			$ci->session->set_userdata('site_lang', $_SESSION['site_lang']);
		}else{
			$_SESSION['site_lang'] = "id";
			$_SESSION['lang'] = "id";
			$ci->session->set_userdata('site_lang', $_SESSION['site_lang']);
		}

		$ci->lang->load('content',$_SESSION['site_lang']);
   	}
}