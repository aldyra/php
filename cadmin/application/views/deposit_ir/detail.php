<!-- Theme JS files -->
<script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>

<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script>
<script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script>
<!-- /theme JS files -->
<?php
$is_update = $authorize['is_update'];
$back = isset($data['back']) ? $data['back'] : '';

$deposit_ir = isset($data['deposit_ir']) ? $data['deposit_ir'] : array();
$DETAIL = isset($data['detail']) ? $data['detail'] : array();
// var_dump($DETAIL);
$ID_DEPOSIT_IR_HEADER = isset($deposit_ir->ID_DEPOSIT_IR_HEADER) ? $deposit_ir->ID_DEPOSIT_IR_HEADER : "";
$CATEGORY_DEPOSIT = isset($deposit_ir->CATEGORY_DEPOSIT) ? $deposit_ir->CATEGORY_DEPOSIT : "";
?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Interest Rate</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Deposito Interest Rate</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Deposito Interest
                    Rate</span> - Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
            <div class="col-md-12">
                <input type="hidden" name="id_deposit_ir_header" id="id_deposit_ir_header"
                    value="<?php echo $ID_DEPOSIT_IR_HEADER ?>">
                <div class="form-group">
                    <label>Category</label><br>
                    <?php
$DEPOSIT_ID = $this->config->item('DEPOSIT_ID');
$DEPOSIT_EN = $this->config->item('DEPOSIT_EN');
$ID_CATEG_DEPOSIT = $this->config->item('ID_CATEG_DEPOSIT');
$Exp_DEPOSIT_ID = explode(":", $DEPOSIT_ID);
$Exp_DEPOSIT_EN = explode(":", $DEPOSIT_EN);
$Exp_ID_CATEG_DEPOSIT = explode(":", $ID_CATEG_DEPOSIT);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_DEPOSIT); $i++) {
    $ID = $Exp_ID_CATEG_DEPOSIT[$i];
    $DESC_EN = $Exp_DEPOSIT_EN[$i];
    $DESC_ID = $Exp_DEPOSIT_ID[$i];
    if ($CATEGORY_DEPOSIT == $ID) {
        echo '<h5>' . $DESC_EN . '-' . $DESC_ID . '</h5>';
    }
}
?>
                </div>
            </div>
        </div>
        <div class="card-body" id="detail">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="card-title">Deposito Interest Rate Detail</h6>

                </div>
            </div>

            <hr>
            <?php if (!empty($DETAIL)) {?>
            <div id="form_detail">
                <?php if ($CATEGORY_DEPOSIT == '1' || $CATEGORY_DEPOSIT == '3') {?>
                <div id="deposit_umum">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th rowspan="2">Balance ID</th>
                                    <th rowspan="2">Balance EN</th>
                                    <th colspan="2">Rates</th>
                                </tr>
                                <tr>
                                    <th>Month</th>
                                    <th>Rates</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($DETAIL); $i++) {
    $jml = 0;
    if (!empty($DETAIL[$i]['detail_month'])) {
        $jml = count($DETAIL[$i]['detail_month']);
    }

    $jml = $jml + 1;

    ?>
                                <tr>
                                    <td rowspan="<?php echo $jml ?>"> <?php echo $DETAIL[$i]['BALANCE_ID']; ?></td>
                                    <td rowspan="<?php echo $jml ?>"> <?php echo $DETAIL[$i]['BALANCE_EN']; ?></td>
                                </tr>
                                <?php if (!empty($DETAIL[$i]['detail_month'])) {for ($j = 0; $j < count($DETAIL[$i]['detail_month']); $j++) {?>
                                <tr>
                                    <td><?php echo $DETAIL[$i]['detail_month'][$j]['MONTH'] ?></td>
                                    <td><?php echo $DETAIL[$i]['detail_month'][$j]['INTEREST_RATE'] ?></td>
                                </tr>
                                <?php }}?>

                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <?php if ($CATEGORY_DEPOSIT == '2') {?>
                <div id="deposit_plus">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Balance ID</th>
                                    <th>Balance EN</th>
                                    <th width="10px"></th>
                                    <th>Min Tabungan Kesra</th>
                                    <th>Interest Rates</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $no = $no + 1; ?></td>
                                    <td><?php echo $row['BALANCE_ID'] ?></td>
                                    <td><?php echo $row['BALANCE_EN'] ?></td>
                                    <td><?php echo $row['OPERATION'] ?></td>
                                    <td><?php echo $row['MIN_TABUNGAN_KESRA'] ?></td>
                                    <td><?php echo $row['INTEREST_RATE'] ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <?php if ($CATEGORY_DEPOSIT == '4') {?>
                <div id="deposit_perpanjangan">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>Interest Rates Before</th>
                                    <th>Interest Rates Next</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $row['MONTH'] ?></td>
                                    <td><?php echo $row['INTEREST_RATE_BEFORE'] ?></td>
                                    <td><?php echo $row['INTEREST_RATE_NEXT'] ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>
                <?php if ($CATEGORY_DEPOSIT == '5') {?>
                <div id="deposit_usd">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Balance ID</th>
                                    <th>Balance EN</th>
                                    <th>Rates</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;foreach ($DETAIL as $row) {?>
                                <tr>
                                    <td><?php echo $no = $no + 1; ?></td>
                                    <td><?php echo $row['BALANCE_ID'] ?></td>
                                    <td><?php echo $row['BALANCE_EN'] ?></td>
                                    <td><?php echo $row['INTEREST_RATE'] ?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php }?>

            </div>
            <?php }?>
        </div>
        <div class="card-footer bg-transparent d-flex justify-content-between mt-2 border-top-0 pt-0">
            <div class="text-right">
                <a href="<?php echo $back ?>"><button type="button" class="btn btn-secondary waves-effect waves-light">
                        Back</button></a>
            </div>
        </div>

    </div>

    <?php endif;?>

</div>