<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Usergroupp_model extends MY_Model
{

    protected $_table_name = 'MS_USER_GROUP_PREVILEGE';
    protected $_primary_key = 'ID';
    protected $_order_by = 'ID';
    protected $_order_by_type = 'ASC';

    public function __construct()
    {
        parent::__construct();
    }

    public function getListMM()
    {
        $queryMainMenu = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=0 AND ACTIVE=1
                        ORDER BY MENU_SEQ ASC";
        return $this->db->query($queryMainMenu)->result();
    }

    public function getMSM($parent)
    {
        $queryMSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=1 AND MENU_PARENT=" . $parent . " AND ACTIVE=1
                    ORDER BY MENU_SEQ ASC";
        return $this->db->query($queryMSM);
    }

    public function getSSM($parent)
    {
        $querySSM = "SELECT * FROM MS_MODULE_PREVILEGE WHERE MENU_LEVEL=2 AND MENU_PARENT=" . $parent . " AND ACTIVE=1
                    ORDER BY MENU_SEQ ASC";
        return $this->db->query($querySSM);
    }

    public function getModAct($modId, $uGID)
    {
        $queryModAct = "SELECT * FROM MS_USER_GROUP_PREVILEGE WHERE MODULE_PREVILEGE_ID = '" . $modId . "' AND USER_GROUP_ID = '" . $uGID . "'";
        return $this->db->query($queryModAct)->row_array();
    }

    public function get_data($col, $tbl, $getAction, $where = null)
    {
        $this->datatables->select($col);
        $this->datatables->from($tbl);

        if ($where) {
            $this->datatables->where($where);
        }

        if ($getAction['is_update'] == 1) {
            $act_edit = '<a onclick="openEdit(\'$1\')" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        } else {
            $act_edit = '';
        }

        $this->datatables->add_column('ACTION',
            '<div class="list-icons">
            	<div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        ' . $act_edit . '
                    </div>
            </div>
        </div>', 'USER_GROUP_ID'
        );

        return $sql = $this->datatables->generate();
    }
}