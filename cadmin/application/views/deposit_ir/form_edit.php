<?php
$is_update = $authorize['is_update'];
$back = isset($data['back']) ? $data['back'] : '';

$deposit_ir = isset($data['deposit_ir']) ? $data['deposit_ir'] : array();
$DETAIL = isset($data['detail']) ? $data['detail'] : array();
$no = isset($data['no']) ? $data['no'] : 0;
$ID_DEPOSIT_IR_HEADER = isset($deposit_ir->ID_DEPOSIT_IR_HEADER) ? $deposit_ir->ID_DEPOSIT_IR_HEADER : "";
$CATEGORY_DEPOSIT = isset($deposit_ir->CATEGORY_DEPOSIT) ? $deposit_ir->CATEGORY_DEPOSIT : "";
$EFFECTIVE_DATE = isset($deposit_ir->EFFECTIVE_DATE) ? $deposit_ir->EFFECTIVE_DATE : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.center-right {
    display: flex;
    align-items: center;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Interest Rate</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Deposito Interest Rate</a>
                <a class="breadcrumb-item active">Edit</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Deposito Interest
                    Rate</span> - Edit
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveDepositIR">
            <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
                <div class="col-md-6">
                    <input type="hidden" name="id_deposit_ir_header" id="id_deposit_ir_header"
                        value="<?php echo $ID_DEPOSIT_IR_HEADER ?>">
                    <div class="form-group">
                        <label>Category</label><span style="color: orange">*</span><br>
                        <select class="form-control select2" name="category" id="category" required="">
                            <option selected disabled>Choose Category</option>
                            <?php
$DEPOSIT_ID = $this->config->item('DEPOSIT_ID');
$DEPOSIT_EN = $this->config->item('DEPOSIT_EN');
$ID_CATEG_DEPOSIT = $this->config->item('ID_CATEG_DEPOSIT');
$Exp_DEPOSIT_ID = explode(":", $DEPOSIT_ID);
$Exp_DEPOSIT_EN = explode(":", $DEPOSIT_EN);
$Exp_ID_CATEG_DEPOSIT = explode(":", $ID_CATEG_DEPOSIT);
for ($i = 0; $i < sizeof($Exp_ID_CATEG_DEPOSIT); $i++) {
    $ID = $Exp_ID_CATEG_DEPOSIT[$i];
    $DESC_EN = $Exp_DEPOSIT_EN[$i];
    $DESC_ID = $Exp_DEPOSIT_ID[$i];
    ?>
                            <option value="<?php echo $ID ?>">
                                <?php echo $DESC_EN . ' / ' . $DESC_ID ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Effective Date<span style="color:orange">*</span></label>

                        <div class="input-group" style="color:#333">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar5"></i></span>
                            </span>
                            <input type="text" name="effective_date" id="effective_date"
                                class="form-control pickadate-selectors" placeholder="Select Date.." required=""
                                data-value="<?php echo $EFFECTIVE_DATE ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body" style="display:none" id="detail">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="card-title">Deposito Interest Rate Detail</h6>

                    </div>
                    <div class="col-md-6">
                        <button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" style="float:right;"
                            type="button" onclick="add_detail();"><b><i class="fas fa-plus"></i></b>
                            Edit
                        </button>
                    </div>
                </div>

                <hr>
                <?php if (!empty($DETAIL)) {?>

                <div id="form_detail">

                    <div id="deposit_umum" style="display:none">
                        <?php echo ($CATEGORY_DEPOSIT == '1' || $CATEGORY_DEPOSIT == '3' ? $DETAIL : "") ?>

                    </div>
                    <div id="deposit_plus" style="display:none">
                        <?php echo ($CATEGORY_DEPOSIT == '2' ? $DETAIL : "") ?>

                    </div>
                    <div id="deposit_perpanjangan" style="display:none">
                        <?php echo ($CATEGORY_DEPOSIT == '4' ? $DETAIL : "") ?>


                    </div>
                    <div id="deposit_usd" style="display:none">
                        <?php echo ($CATEGORY_DEPOSIT == '5' ? $DETAIL : "") ?>

                    </div>
                </div>
                <?php }?>

            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between mt-2 border-top-0 pt-0">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

    <?php endif;?>

</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#category').val("<?php echo $CATEGORY_DEPOSIT ?>").trigger('change');
});
var room = "<?php echo $no ?>";
$(function() {
    $('.select2').select2();
    // Dropdown selectors
    $('.pickadate-selectors').pickadate({
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true
    });
});

// function add_detail() {
//     room++;
//     var objTo = document.getElementById('form_detail');
//     html = '';
//     var category = $('#category').val();
//     if (category == '1') {
//         //deposit umum
//         $('#deposit_umum').show();
//         $("#deposit_umum").find("*").prop("disabled", false);
//         $('#deposit_plus').hide();
//         $("#deposit_plus").find("*").prop("disabled", true);
//         $('#deposit_perpanjangan').hide();
//         $("#deposit_perpanjangan").find("*").prop("disabled", true);
//         $('#deposit_usd').hide();
//         $("#deposit_usd").find("*").prop("disabled", true);

//         $("#deposit_umum").find("input").prop("required", true);
//         $("#deposit_plus").find("input").prop("required", false);
//         $("#deposit_perpanjangan").find("input").prop("required", false);
//         $("#deposit_usd").find("input").prop("required", false);

//         html = '<div class="card border-left-info border-right-info rounded-0" id="umum' + room + '">' +
//             '<div class="card-body"><div class="row">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
//             '<div class="row"> <input type="hidden" value="' + room + '" name="detail_id[]">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_id" name="balance_id[]" placeholder="Balance in Indonesian" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Balance English<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_en" name = "balance_en[]" placeholder = "Balance in English" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 1<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_1" name="month_1[]" placeholder = "Month 1" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 3<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_3" name="month_3[]" placeholder = "Month 3" parsley-trigger="change" required=""> ' +
//             ' </div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 4<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_4" name="month_4[]" placeholder = "Month 4" parsley-trigger="change" required=""> ' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 6<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_6" name="month_6[]" placeholder = "Month 6" parsley-trigger="change" required=""> ' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 12<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_12" name="month_12[]" placeholder = "Month 12" parsley-trigger="change" required=""> ' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
//             '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type = "button" onclick = "remove(\'umum' +
//             room + '\');"> <b> <i class = "fas fa-minus"></i></b>Remove</button>' +
//             '</div>' +
//             '</div></div></div>';
//         $("#deposit_umum").append(html);



//     } else if (category == '2') {
//         //deposit plus
//         $('#deposit_umum').hide();
//         $("#deposit_umum").find("*").prop("disabled", true);
//         $('#deposit_plus').show();
//         $("#deposit_plus").find("*").prop("disabled", false);
//         $('#deposit_perpanjangan').hide();
//         $("#deposit_perpanjangan").find("*").prop("disabled", true);
//         $('#deposit_usd').hide();
//         $("#deposit_usd").find("*").prop("disabled", true);

//         $("#deposit_umum").find("input").prop("required", false);
//         $("#deposit_plus").find("input").prop("required", true);
//         $("#deposit_perpanjangan").find("input").prop("required", false);
//         $("#deposit_usd").find("input").prop("required", false);
//         html = '<div class="card border-left-info border-right-info rounded-0" id="plus' + room + '">' +
//             '<div class="card-body">' +
//             '<div class="row">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
//             '<div class="row">' +
//             '<input type="hidden" value="first" name="detail_id[]">' +

//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_id" name="balance_id[]" placeholder="Balance in Indonesian" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Balance English<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_en" name="balance_en[]" placeholder="Balance in English"parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
//             '<div class="form-group">' +
//             '<label>Operation<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="operation"name="operation[]" placeholder="Operation"parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
//             ' <div class="form-group">' +
//             '<label>Min Tabungan Kesra<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="min_tabungan_kesra"name="min_tabungan_kesra[]" placeholder="Min Tabungan Kesra"parsley-trigger="change" required="">' +
//             ' </div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
//             '<div class="form-group">' +
//             '<label>Interest Rates<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="interest_rate"name="interest_rate[]" placeholder="Interest Rates"parsley-trigger="change" required="">' +
//             '</div>' +
//             ' </div>' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
//             '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"type="button" onclick="remove(\'plus' +
//             room + '\');"><b><i class="fas fa-minus"></i></b>Remove</button>' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '</div>';
//         $("#deposit_plus").append(html);


//     } else if (category == '3') {
//         //deposit premium
//         $('#deposit_umum').show();
//         $("#deposit_umum").find("*").prop("disabled", false);
//         $('#deposit_plus').hide();
//         $("#deposit_plus").find("*").prop("disabled", true);
//         $('#deposit_perpanjangan').hide();
//         $("#deposit_perpanjangan").find("*").prop("disabled", true);
//         $('#deposit_usd').hide();
//         $("#deposit_usd").find("*").prop("disabled", true);

//         $("#deposit_umum").find("input").prop("required", true);
//         $("#deposit_plus").find("input").prop("required", false);
//         $("#deposit_perpanjangan").find("input").prop("required", false);
//         $("#deposit_usd").find("input").prop("required", false);

//         html = '<div class="card border-left-info border-right-info rounded-0" id="premium' + room + '">' +
//             '<div class="card-body"><div class="row">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
//             '<div class="row"> <input type="hidden" value="' + room + '" name="detail_id[]">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_id" name="balance_id[]" placeholder="Balance in Indonesian" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Balance English<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_en" name = "balance_en[]" placeholder = "Balance in English" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 1<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_1" name="month_1[]" placeholder = "Month 1" parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 3<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_3" name="month_3[]" placeholder = "Month 3" parsley-trigger="change" required=""> ' +
//             ' </div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 4<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_4" name="month_4[]" placeholder = "Month 4" parsley-trigger="change" required=""> ' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 6<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_6" name="month_6[]" placeholder = "Month 6" parsley-trigger="change" required=""> ' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">' +
//             '<div class="form-group">' +
//             '<label>Month 12<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="month_12" name="month_12[]" placeholder = "Month 12" parsley-trigger="change" required=""> ' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
//             '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type = "button" onclick = "remove(\'premium' +
//             room + '\');"> <b> <i class = "fas fa-minus"></i></b>Remove</button>' +
//             '</div>' +
//             '</div></div></div>';
//         $("#deposit_umum").append(html);

//     } else if (category == '4') {
//         //deposit perpanjangan
//         $('#deposit_umum').hide();
//         $("#deposit_umum").find("*").prop("disabled", true);
//         $('#deposit_plus').hide();
//         $("#deposit_plus").find("*").prop("disabled", true);
//         $('#deposit_perpanjangan').show();
//         $("#deposit_perpanjangan").find("*").prop("disabled", false);
//         $('#deposit_usd').hide();
//         $("#deposit_usd").find("*").prop("disabled", true);

//         $("#deposit_umum").find("input").prop("required", false);
//         $("#deposit_plus").find("input").prop("required", false);
//         $("#deposit_perpanjangan").find("input").prop("required", true);
//         $("#deposit_usd").find("input").prop("required", false);

//         html = '<div class="card border-left-info border-right-info rounded-0" id="perpanjangan' + room + '">' +
//             '<div class="card-body">' +
//             '<div class="row">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
//             '<div class="row">' +
//             '<input type="hidden" value="first" name="detail_id[]">' +

//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Interest Rate Before<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="interest_rate_before" name="interest_rate_before[]" placeholder="Interest Rate Before" parsley-trigger="change" required="">' +
//             '</div>' +
//             ' </div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
//             '<div class="form-group">' +
//             '<label>Interest Rate Next<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="interest_rate_next"name="interest_rate_next[]" placeholder="Interest Rate Next" parsley-trigger="change" required="">' +
//             '</div>' +
//             ' </div>' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
//             '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"type="button" onclick="remove(\'perpanjangan' +
//             room + '\');"><b><i class="fas fa-minus"></i></b>Remove' +
//             '</button>' +
//             '</div>' +
//             ' </div>' +
//             '</div>' +
//             '</div>';
//         $("#deposit_perpanjangan").append(html);


//     } else if (category == '5') {
//         //deposit usd
//         $('#deposit_umum').hide();
//         $("#deposit_umum").find("*").prop("disabled", true);
//         $('#deposit_plus').hide();
//         $("#deposit_plus").find("*").prop("disabled", true);
//         $('#deposit_perpanjangan').hide();
//         $("#deposit_perpanjangan").find("*").prop("disabled", true);
//         $('#deposit_usd').show();
//         $("#deposit_usd").find("*").prop("disabled", false);

//         $("#deposit_umum").find("input").prop("required", false);
//         $("#deposit_plus").find("input").prop("required", false);
//         $("#deposit_perpanjangan").find("input").prop("required", false);
//         $("#deposit_usd").find("input").prop("required", true);

//         html = '<div class="card border-left-info border-right-info rounded-0" id="usd' + room + '">' +
//             '<div class="card-body">' +
//             '<div class="row">' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
//             '<div class="row">' +
//             '<input type="hidden" value="first" name="detail_id[]">' +

//             '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
//             ' <div class="form-group">' +
//             '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_id"name="balance_id[]" placeholder="Balance in Indonesian"parsley-trigger="change" required="">' +
//             '</div>' +
//             ' </div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
//             '<div class="form-group">' +
//             '<label>Balance English<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="balance_en"name="balance_en[]" placeholder="Balance in English"parsley-trigger="change" required="">' +
//             '</div>' +
//             ' </div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
//             ' <div class="form-group">' +
//             ' <label>Interest Rate<span style="color:orange">*</span></label>' +
//             '<input type="text" class="form-control" id="interest_rate"name="interest_rate[]" placeholder="Interest Rate"parsley-trigger="change" required="">' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
//             '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type="button" onclick="remove(\'usd' +
//             room + '\');"><b><i class="fas fa-minus"></i></b>' +
//             'Remove</button>' +
//             '</div>' +
//             '</div>' +
//             '</div>' +
//             '</div>';
//         $("#deposit_usd").append(html);

//     }

//     $('.form-control-uniform').uniform();
// }
function add_detail(detail = '') {
    room++;
    var objTo = document.getElementById('form_detail');
    html = '';
    var category = $('#category').val();
    if (category == '1') {
        //deposit umum
        $('#deposit_umum').show();
        $("#deposit_umum").find("*").prop("disabled", false);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", true);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", false);

        html = '<div class="card border-left-info border-right-info rounded-0" id="umum' + room + '">' +
            '<div class="card-body">' +
            '<div class="row">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
            '<div class="row">' +
            '<input type="hidden" value="id_' + room + '" name="detail_id[]">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
            '<div class="form-group">' +
            '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_id" name = "balance_id[]" placeholder = "Balance in Indonesian" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6"><div class="form-group">' +
            '<label>Balance English<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_en" name = "balance_en[]" placeholder = "Balance in English" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
            '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type = "button" onclick = "remove(\'umum' +
            room + '\');" ><b><i class = "fas fa-minus"></i></b>Remove</button>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-2">' +
            '<h6 class="card-title">Detail Month</h6>' +
            '</div>' +
            '<div class="col-md-10">' +
            '<button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" style="float:left;" type="button" onclick="add_detail_month(\'detail_ir_' +
            room + '\', \'' + room + '\');"><b><i class="fas fa-plus"></i></b>' +
            'Add Detail Month</button>' +
            '</div>' +
            '</div>' +
            '<div class="row" >' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="detail_ir_' + room + '">' +
            '<div class="row" id="umum_detail_' + room + '">' +
            '<div class="col-4">' +
            '<div class="form-group">' +
            '<label>Month<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="month" onkeyup="validate(this);" name="month[' + room +
            '][]" placeholder = "Month" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '<div class="col-4">' +
            '<div class="form-group">' +
            '<label>Interest Rate<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="interest_rate_month" name = "interest_rate_month[' + room +
            '][]" placeholder = "Interest Rate" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '<div class="col-4 center-right">' +
            // '<button type="button" class="btn bg-teal-400 btn-outline-success" style = "margin-right:5px" onclick = "add_detail_month(\'umum_detail_' +
            // room + '\')" >' +
            // '<i class = "fa fa-plus"></i></button> ' +
            '<button type="button" class="btn btn-outline-danger"><i class = "fa fa-minus" onclick = "remove(\'umum_detail_' +
            room + '\')" > </i></button > ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#deposit_umum").append(html);



    } else if (category == '2') {
        //deposit plus
        $('#deposit_umum').hide();
        $("#deposit_umum").find("*").prop("disabled", true);
        $('#deposit_plus').show();
        $("#deposit_plus").find("*").prop("disabled", false);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", false);
        $("#deposit_plus").find("input").prop("required", true);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", false);
        html = '<div class="card border-left-info border-right-info rounded-0" id="plus' + room + '">' +
            '<div class="card-body">' +
            '<div class="row">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
            '<div class="row">' +
            '<input type="hidden" value="id_' + room + '" name="detail_id[]">' +

            '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
            '<div class="form-group">' +
            '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_id" name="balance_id[]" placeholder="Balance in Indonesian" parsley-trigger="change" required="">' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
            '<div class="form-group">' +
            '<label>Balance English<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_en" name="balance_en[]" placeholder="Balance in English"parsley-trigger="change" required="">' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            '<div class="form-group">' +
            '<label>Operation<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="operation"name="operation[]" placeholder="Operation"parsley-trigger="change" required="">' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            ' <div class="form-group">' +
            '<label>Min Tabungan Kesra<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="min_tabungan_kesra"name="min_tabungan_kesra[]" placeholder="Min Tabungan Kesra"parsley-trigger="change" required="">' +
            ' </div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            '<div class="form-group">' +
            '<label>Interest Rates<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="interest_rate"name="interest_rate[]" placeholder="Interest Rates"parsley-trigger="change" required="">' +
            '</div>' +
            ' </div>' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
            '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"type="button" onclick="remove(\'plus' +
            room + '\');"><b><i class="fas fa-minus"></i></b>Remove</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#deposit_plus").append(html);


    } else if (category == '3') {
        //deposit premium
        $('#deposit_umum').show();
        $("#deposit_umum").find("*").prop("disabled", false);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", true);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", false);

        html = '<div class="card border-left-info border-right-info rounded-0" id="umum' + room + '">' +
            '<div class="card-body">' +
            '<div class="row">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
            '<div class="row">' +
            '<input type="hidden" value="id_' + room + '" name="detail_id[]">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">' +
            '<div class="form-group">' +
            '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_id" name = "balance_id[]" placeholder = "Balance in Indonesian" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6"><div class="form-group">' +
            '<label>Balance English<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_en" name = "balance_en[]" placeholder = "Balance in English" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
            '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type = "button" onclick = "remove(\'umum' +
            room + '\');" > <b><i class = "fas fa-minus"> </i></b>Remove</button>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-2">' +
            '<h6 class="card-title">Detail Month</h6>' +
            '</div>' +
            '<div class="col-md-10">' +
            '<button class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" style="float:left;" type="button" onclick="add_detail_month(\'detail_ir_' +
            room + '\', \'' + room + '\');"><b><i class="fas fa-plus"></i></b>' +
            'Add Detail Month</button>' +
            '</div>' +
            '</div>' +
            '<div class="row" >' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="detail_ir_' + room + '">' +
            '<div class="row" id="umum_detail_' + room + '">' +
            '<div class="col-4">' +
            '<div class="form-group">' +
            '<label>Month<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="month" onkeyup="validate(this);" name="month[' + room +
            '][]" placeholder = "Month" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '<div class="col-4">' +
            '<div class="form-group">' +
            '<label>Interest Rate<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="interest_rate_month" name = "interest_rate_month[' + room +
            '][]" placeholder = "Interest Rate" parsley-trigger = "change" required = "" > ' +
            '</div>' +
            '</div>' +
            '<div class="col-4 center-right">' +
            // '<button type="button" class="btn bg-teal-400 btn-outline-success" style = "margin-right:5px" onclick = "add_detail_month(\'umum_detail_' +
            // room + '\')" >' +
            // '<i class = "fa fa-plus"> </i></button> ' +
            '<button type="button" class="btn btn-outline-danger"><i class = "fa fa-minus" onclick = "remove(\'umum_detail_' +
            room + '\')" > </i></button > ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#deposit_umum").append(html);

    } else if (category == '4') {
        //deposit perpanjangan
        $('#deposit_umum').hide();
        $("#deposit_umum").find("*").prop("disabled", true);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').show();
        $("#deposit_perpanjangan").find("*").prop("disabled", false);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", false);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", true);
        $("#deposit_usd").find("input").prop("required", false);

        html = '<div class="card border-left-info border-right-info rounded-0" id="perpanjangan' + room + '">' +
            '<div class="card-body">' +
            '<div class="row">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
            '<div class="row">' +
            '<input type="hidden" value="id_' + room + '" name="detail_id[]">' +

            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            '<div class="form-group">' +
            '<label>Month<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="month_perpanjang" onkeyup="validate(this);" name="month_perpanjang[]" placeholder="Month" parsley-trigger="change" required="">' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            '<div class="form-group">' +
            '<label>Interest Rate Before<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="interest_rate_before" name="interest_rate_before[]" placeholder="Interest Rate Before" parsley-trigger="change" required="">' +
            '</div>' +
            ' </div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            '<div class="form-group">' +
            '<label>Interest Rate Next<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="interest_rate_next"name="interest_rate_next[]" placeholder="Interest Rate Next" parsley-trigger="change" required="">' +
            '</div>' +
            ' </div>' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
            '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"type="button" onclick="remove(\'perpanjangan' +
            room + '\');"><b><i class="fas fa-minus"></i></b>Remove' +
            '</button>' +
            '</div>' +
            ' </div>' +
            '</div>' +
            '</div>';
        $("#deposit_perpanjangan").append(html);


    } else if (category == '5') {
        //deposit usd
        $('#deposit_umum').hide();
        $("#deposit_umum").find("*").prop("disabled", true);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').show();
        $("#deposit_usd").find("*").prop("disabled", false);

        $("#deposit_umum").find("input").prop("required", false);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", true);

        html = '<div class="card border-left-info border-right-info rounded-0" id="usd' + room + '">' +
            '<div class="card-body">' +
            '<div class="row">' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">' +
            '<div class="row">' +
            '<input type="hidden" value="id_' + room + '" name="detail_id[]">' +

            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            ' <div class="form-group">' +
            '<label>Balance Indonesian<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_id"name="balance_id[]" placeholder="Balance in Indonesian"parsley-trigger="change" required="">' +
            '</div>' +
            ' </div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            '<div class="form-group">' +
            '<label>Balance English<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="balance_en"name="balance_en[]" placeholder="Balance in English"parsley-trigger="change" required="">' +
            '</div>' +
            ' </div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">' +
            ' <div class="form-group">' +
            ' <label>Interest Rate<span style="color:orange">*</span></label>' +
            '<input type="text" class="form-control" id="interest_rate"name="interest_rate[]" placeholder="Interest Rate"parsley-trigger="change" required="">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">' +
            '<button class="btn btn-danger btn-labeled btn-labeled-left rounded-round" type="button" onclick="remove(\'usd' +
            room + '\');"><b><i class="fas fa-minus"></i></b>' +
            'Remove</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#deposit_usd").append(html);

    }

    $('.form-control-uniform').uniform();
}

detail_room = 0;

function add_detail_month(detail = '', id_detail) {
    detail_room++;
    var el = document.createElement("div");
    el.classList.add('row');
    el.id = 'umum_detail_d' + detail_room;
    html = '<div class="col-4">' +
        '<div class="form-group">' +
        '<label>Month<span style="color:orange">*</span></label>' +
        '<input type="text" class="form-control" id="month" onkeyup="validate(this);" name="month[' + id_detail +
        '][]" placeholder="Month" parsley-trigger="change" required="">' +
        '</div>' +
        '</div>' +
        '<div class="col-4">' +
        '<div class="form-group">' +
        '<label>Interest Rate<span style="color:orange">*</span></label>' +
        '<input type="text" class="form-control" id="interest_rate_month" name="interest_rate_month[' + id_detail +
        '][]" placeholder="Interest Rate" parsley-trigger="change" required="">' +
        '</div>' +
        '</div>' +
        '<div class="col-4 center-right">' +
        // '<button type="button" class="btn bg-teal-400 btn-outline-success" style="margin-right:5px" onclick="add_detail_month(\'umum_detail_d' +
        // detail_room + '\', \'' + $user + '\')"><i class="fa fa-plus"></i></button>' +
        '<button type="button" class="btn btn-outline-danger"><i class="fa fa-minus" onclick="remove(\'umum_detail_d' +
        detail_room + '\')"></i></button>' +
        '</div>';
    el.innerHTML = html;
    var div = document.getElementById(detail);
    if (detail.includes('detail_ir_')) {
        $("#" + detail + "").append(el);
    } else {
        insertAfter(div, el);
    }
}

function remove(rid) {
    Swal.fire({
        title: "Are you sure?",
        type: "warning",
        text: "Your will not be able to recover this data!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes",
    }).then((result) => {
        if (result.value) {
            $('#' + rid).remove();
        }
    });

}
$('#category').change(function() {
    $('#detail').show();

    var category = $(this).val();
    if (category == '1') {
        //deposit umum
        $('#deposit_umum').show();
        $("#deposit_umum").find("*").prop("disabled", false);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", true);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", false);


    } else if (category == '2') {
        //deposit plus
        $('#deposit_umum').hide();
        $("#deposit_umum").find("*").prop("disabled", true);
        $('#deposit_plus').show();
        $("#deposit_plus").find("*").prop("disabled", false);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", false);
        $("#deposit_plus").find("input").prop("required", true);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", false);

    } else if (category == '3') {
        //deposit premium
        $('#deposit_umum').show();
        $("#deposit_umum").find("*").prop("disabled", false);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", true);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", false);

    } else if (category == '4') {
        //deposit perpanjangan
        $('#deposit_umum').hide();
        $("#deposit_umum").find("*").prop("disabled", true);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').show();
        $("#deposit_perpanjangan").find("*").prop("disabled", false);
        $('#deposit_usd').hide();
        $("#deposit_usd").find("*").prop("disabled", true);

        $("#deposit_umum").find("input").prop("required", false);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", true);
        $("#deposit_usd").find("input").prop("required", false);

    } else if (category == '5') {
        //deposit usd
        $('#deposit_umum').hide();
        $("#deposit_umum").find("*").prop("disabled", true);
        $('#deposit_plus').hide();
        $("#deposit_plus").find("*").prop("disabled", true);
        $('#deposit_perpanjangan').hide();
        $("#deposit_perpanjangan").find("*").prop("disabled", true);
        $('#deposit_usd').show();
        $("#deposit_usd").find("*").prop("disabled", false);

        $("#deposit_umum").find("input").prop("required", false);
        $("#deposit_plus").find("input").prop("required", false);
        $("#deposit_perpanjangan").find("input").prop("required", false);
        $("#deposit_usd").find("input").prop("required", true);

    }

});
$('#frmsaveDepositIR').on('submit', function(e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        $("#frmsaveDepositIR").parsley().validate();
        var form = $("#frmsaveDepositIR");
        var data = form.serialize();

        if ($("#frmsaveDepositIR").parsley().isValid()) {
            $.ajax({
                url: "<?php echo base_url('deposit_ir/update') ?>",
                type: "POST",
                data: data,
                cache: false,
                dataType: "json",
                success: function(res) {
                    var ErrorMessage = res.ErrorMessage;
                    var ErrorCode = res.ErrorCode;
                    if (ErrorCode != "EC:0000") {
                        Swal.fire({
                            type: "error",
                            html: ErrorMessage,
                            confirmButton: true,
                            confirmButtonColor: "#1FB3E5",
                            confirmButtonText: "Close",
                        });
                    } else {
                        Swal.fire({
                            position: "center",
                            type: "success",
                            text: ErrorMessage,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        setTimeout(function() {
                            var uri = "<?php echo $back; ?>";
                            window.location.href = uri;
                        }, 1500);
                    }
                },
            });
        }
    } else {

    }
});
</script>