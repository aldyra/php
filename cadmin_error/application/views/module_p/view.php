<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">User Management</a>
                <a href="#" class="breadcrumb-item active">Module Privilege</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <table class="table datatable-complex-header" id="tableData">
            <thead>
                <tr>
                    <th>Module Name</th>
                    <th data-orderable="false">Status</th>
                    <th data-orderable="false">Created Date</th>
                    <th>sts</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
var arr = ["MENU_SEQ", "MODULE_NAME", "ACTIVE", "CREATED_DATE"];
var t = $("#tableData").DataTable({
    serverSide: false,
    autoWidth: false,
    ajax: {
        "url": "<?php echo base_url('module_p/get_data') ?>",
        "type": "POST",
        "data": {
            "arr": arr
        },
    },
    columns: [{
            "data": "MODULE_NAME"
        },
        {
            "data": "ACTIVE"
        },
        {
            "data": "CREATED_DATE"
        },
        {
            "data": "MENU_SEQ"
        },
        {
            "data": "STATUS"
        }
    ],
    columnDefs: [{
        "targets": [3, 4],
        "visible": false,
        // "searchable": false
    }],
    keys: !0,
    order: [
        [3, "asc"]
    ],
    autofill: true,
    select: true,
    responsive: true,
    buttons: true,
    length: 5,
    dom: '<"datatable-scroll"t><"datatable-footer"Rrli>p',

});
$('#tableData thead th').each(function() {
    var title = $(this).text();
    if (title == "Action" || title == "Image") {

    } else if (title == "Status") {
        $(this).append(
            '<br><select onchange="changeSelectStatus(this)" class="form-control form-control-sm"  parsley-trigger="change"><option value="">All</option><option value="88">Active</option><option value="0">Inactive</option></select>'
        );
    } else if (title == "Created Date") {
        $(this).append(
            '<br> <div class="input-group" style="color:#333">' +
            '<span class="input-group-prepend">' +
            '<span class="input-group-text"><i class="icon-calendar5"></i></span>' +
            "</span>" +
            '<input type="text" onchange="changeSelectDate(this)" name="created_date" id="created_date" class="form-control pickadate-selectors" placeholder="Select Date..">' +
            "</div>"
        );
        $(".pickadate-selectors").pickadate({
            selectYears: true,
            selectMonths: true,
            format: "dd mmm yyyy",
            formatSubmit: "dd mmm yyyy",
            hiddenName: true,
        });
    } else {
        $(this).append(
            '<input type="text" class="form-control form-control-sm" style="margin-top: 5px;" placeholder="Search..">'
        );
    }
});

t.columns().every(function() {
    var that = this;
    $("input", this.header()).on("keyup", function(e) {
        if (event.keyCode === 8) {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        } else {
            if (this.value.length >= 1) {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            }
        }
    });
});

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(2)
            .search(dis.value)
            .draw();
    } else {
        t.column(2)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDateLog(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(4)
            .search(dis.value)
            .draw();
    } else {
        t.column(4)
            .search(dis.value)
            .draw();
    }

}
$('#frmsaveUserGroup').on('submit', function(e) {
    e.preventDefault();
    if ($(this).valid()) {
        $.ajax({
            url: "<?php echo base_url('user_group/create_action'); ?>",
            data: $(this).serialize(),
            dataType: "JSON",
            type: 'POST',
            success: function(data) {
                if (data == "0: Duplicate Group Name") {
                    swal({
                        type: 'error',
                        title: 'Failed: Duplicate Group Name'
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                } else if (data == "success") {
                    swal({
                        type: 'success',
                        title: 'Success!'
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                } else {
                    swal({
                        type: 'error',
                        title: 'Failed Insert Data.'
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                }
                document.getElementById("frmsaveUserGroup").reset();
                $('#modalAddUserGroup').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    type: 'error',
                    title: 'Failed: ' + textStatus
                }, function() {
                    t.ajax.reload(null, false);
                });
                document.getElementById("frmsaveUserGroup").reset();
                $('#modalAddUserGroup').modal('hide');
            }
        });
    }
});

//EDIT USER GROUP
$('#frmeditUserGroup').on('submit', function(e) {
    e.preventDefault();
    if ($(this).valid()) {
        $.ajax({
            url: "<?php echo base_url('user_group/update_action'); ?>",
            data: $(this).serialize(),
            dataType: "JSON",
            type: 'POST',
            success: function(data) {
                if (!(data.indexOf("Failed") != -1)) {
                    swal({
                        type: 'success',
                        title: 'Update User Group Success!'
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                } else {
                    swal({
                        type: 'error',
                        title: data
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                }
                document.getElementById("frmeditUserGroup").reset();
                $('#modalEditUserGroup').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire(textStatus).then(function() {
                    t.ajax.reload(null, false);
                });
                document.getElementById("frmeditUserGroup").reset();
                $('#modalEditUserGroup').modal('hide');
            }
        });
    }
});
//END EDIT USER GROUP

//Open Edit Form
function openEdit(id) {
    $.ajax({
        url: "<?php echo base_url('user_group/get_edit/'); ?>" + id,
        dataType: "JSON",
        type: 'GET',
        success: function(data) {
            $("#userGroupNameEdit").val(data.USER_GROUP_NAME);
            $("#IDUserGroup").val(data.ID);
            $('#modalEditUserGroup').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal(textStatus);
            document.getElementById("frmeditUserGroup").reset();
        }
    });
}
//END Open Edit Form

//Open Confirm Delete
function confirmDelete(id) {
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: "<?php echo base_url('user_group/delete_action/'); ?>" + id,
                dataType: "JSON",
                type: 'POST',
                success: function(data) {
                    if (data) {
                        swal({
                            type: 'success',
                            title: 'Delete User Group Success'
                        }, function() {
                            t.ajax.reload(null, false);
                        });
                    } else {
                        swal({
                            type: 'error',
                            title: 'Delete User Group Failed'
                        }, function() {
                            t.ajax.reload(null, false);
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        type: 'error',
                        title: textStatus
                    }, function() {
                        t.ajax.reload(null, false);
                    });
                }
            });
        }
    });
}
//END Confirm Delete
</script>