<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Title Indonesian</label><span style="color: orange">*</span><br>
            <input type="text" name="title_id" class="form-control" id="title_id" placeholder="">

        </div>
        <div class="form-group">
            <label>Title English</label><span style="color: orange">*</span><br>
            <input type="text" name="title_en" class="form-control" id="title_en" placeholder="">

        </div>
        <div class="form-group">
            <label>Description Indonesian</label><span style="color: orange">*</span><br>
            <textarea class="form-control" name="description_id" id="DESCRIPTION_ID" rows="4" required=""
                parsley-trigger="change"></textarea>
        </div>
        <div class="form-group">
            <label>Description English</label><span style="color: orange">*</span><br>
            <textarea class="form-control" name="description_en" id="DESCRIPTION_EN" rows="4" required=""
                parsley-trigger="change"></textarea>
        </div>
        <div class="form-group">
            <label>Upload Award</label><span style="color: orange">*</span><br>
            <input type="file" id="file" name="file" class="dropify" required="" />
        </div>
        <a href="#" onclick="openTnC('<?php echo base_url('award/tncupload') ?>')">Terms and Conditions Upload</a>

    </div>

</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="progress" style="display:none">
            <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0%</span>
            </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.dropify').dropify();
    changeformfile();

});
</script>