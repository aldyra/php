<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Saving_ir_model extends MY_Model
{
    protected $_table_name = 'MS_SAVING_IR_HEADER';
    protected $_detail_table_name = 'MS_SAVING_IR_DETAIL';
    protected $_primary_key = 'ID_SAVING_IR_HEADER';
    protected $_detail_primary_key = 'ID_SAVING_IR_DETAIL';
    protected $_order_by = 'ID_SAVING_IR_HEADER';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }
    public function get_edit($id)
    {
        $this->db->where($this->_primary_key, $id);
        $view = $this->db->get($this->_table_name);
        return $view->row();
    }
    public function get_detail_saving_ir($id)
    {
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result();
        return $detail;

    }
    public function get_detail($id)
    {
        $this->db->where($this->_primary_key, $id);
        $detail = $this->db->get($this->_detail_table_name)->result();
        $html = '';
        foreach ($detail as $row) {
            $html .= '<div class="card border-left-info border-right-info rounded-0" id="removeclassedit' . $row->ID_SAVING_IR_DETAIL . '" data-id="' . $row->ID_SAVING_IR_DETAIL . '">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                                            <div class="form-group">
                                                <label>Balance Indonesian<span style="color:orange">*</span></label>
                                                <input type="hidden" value="' . $row->ID_SAVING_IR_DETAIL . '" name="id_detail[]" id="id_detail">
                                                <input type="text" class="form-control" id="balance_id"
                                                    name="balance_id[]" placeholder="Balance in Indonesian" required=""
                                                    parsley-trigger="change" value="' . $row->BALANCE_ID . '">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                                            <div class="form-group">
                                                <label>Balance English<span style="color:orange">*</span></label>
                                                <input type="text" class="form-control" id="balance_en"
                                                    name="balance_en[]" placeholder="Balance in English" required=""
                                                    parsley-trigger="change" value="' . $row->BALANCE_EN . '">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
                                            <div class="form-group">
                                                <label>Rate<span style="color:orange">*</span></label>
                                                <input type="text" class="form-control" id="rate" name="rate[]"
                                                    placeholder="Rate" required="" parsley-trigger="change" value="' . $row->RATE . '">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 center">
                                    <button class="btn btn-danger btn-labeled btn-labeled-left rounded-round"
                                        type="button" onclick="remove(\'edit' . $row->ID_SAVING_IR_DETAIL . '\');"><b><i class="fas fa-minus"></i></b>
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>';
        }
        return $html;
    }

    public function updateaction($param)
    {
        $ID_SAVING_IR_HEADER = isset($param['ID_SAVING_IR_HEADER']) ? $param['ID_SAVING_IR_HEADER'] : "";

        $WHERE['ID_SAVING_IR_HEADER'] = $ID_SAVING_IR_HEADER;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_SAVING_IR_HEADER'] = isset($param['ID_SAVING_IR_HEADER']) ? $param['ID_SAVING_IR_HEADER'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

}