<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Forgotpassword_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Notificationpassword($param)
    {
        $startTime = date("Y-m-d H:i:s");
        $params['EMAIL'] = isset($param['EMAIL']) ? $param['EMAIL'] : "";

        $Q = $this->db->get_where("MS_USERS", $params)->row_array();
        if (sizeof($Q) == 0) {
            return array("ErrorCode" => "EC:001A", "ErrorMessage" => 'Your account is not found!');
        }

        $params['CODE_VERIFICATION'] = isset($param['CODE_VERIFICATION']) ? $param['CODE_VERIFICATION'] : "";
        $params['IP_LOG'] = isset($param['IP_LOG']) ? $param['IP_LOG'] : "";
        $params['EXPIRED_DATE'] = date('Y-m-d H:i:s', strtotime('+1 hour', strtotime($startTime)));

        $Q2 = $this->db->insert("REQUEST_PASSWORD", $params);
        if (!$Q2) {
            return array("ErrorCode" => "EC:001B", "ErrorMessage" => $this->db->error());
        }
        return array("ErrorCode" => "EC:0000", "ErrorMessage" => 'Success');
    }
    public function insertMail($param)
    {
        $params['EMAIL_TO'] = isset($param['EMAIL_TO']) ? $param['EMAIL_TO'] : "";
        $params['EMAIL_CC'] = isset($param['EMAIL_CC']) ? $param['EMAIL_CC'] : "";
        $params['SUBJECT'] = isset($param['SUBJECT']) ? $param['SUBJECT'] : "";
        $params['MESSAGE'] = isset($param['MESSAGE']) ? $param['MESSAGE'] : "";

        $Q = $this->db->insert("MS_SENDMAIL", $params);
        if (!$Q) {
            $JSON = array("ErrorCode" => "EC:001E", "ErrorMessage" => $this->db->error());
            return $JSON;
            die();
        }

        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => 'Please check your email to reset your password!');
        return $JSON;
    }

    public function verificationUser($param)
    {
        $CODE_VERIFICATION = isset($param['CODE_VERIFICATION']) ? $param['CODE_VERIFICATION'] : "";

        $Q = $this->db->get_where("REQUEST_PASSWORD", array("CODE_VERIFICATION" => $CODE_VERIFICATION, "VERIFICATION" => "0"));
        $run = $Q->row_array();
        if (sizeof($run) == 0) {
            $JSON = array("ErrorCode" => "EC:01AA", "ErrorMessage" => 'Code verfication is not found!');
            return $JSON;
            exit();
        }
        $EMAIL = isset($run['EMAIL']) ? $run['EMAIL'] : "";
        $EXPIRED_DATE = isset($run['EXPIRED_DATE']) ? $run['EXPIRED_DATE'] : "";
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => 'Success', "EMAIL" => $EMAIL, "CODE_VERIFICATION" => $CODE_VERIFICATION, "EXPIRED_DATE" => $EXPIRED_DATE);
        return $JSON;
    }

    public function changepassword($param)
    {
        $EMAIL = isset($param['EMAIL']) ? $param['EMAIL'] : "";
        $CODE_VERIFICATION = isset($param['CODE_VERIFICATION']) ? $param['CODE_VERIFICATION'] : "";
        $PASSWORD = isset($param['PASSWORD']) ? $param['PASSWORD'] : "";
        $NEW_PASSWORD = isset($param['NEW_PASSWORD']) ? $param['NEW_PASSWORD'] : "";
        $OVERVIEW = isset($param['OVERVIEW']) ? $param['OVERVIEW'] : "";

        $params1['EMAIL'] = $EMAIL;
        $params1['CODE_VERIFICATION'] = $CODE_VERIFICATION;
        $params1['VERIFICATION'] = "0";

        if (empty($OVERVIEW) || $OVERVIEW != "YES") {
            $Q = $this->db->get_where("REQUEST_PASSWORD", $params1);
            $run = $Q->row_array();
            if (sizeof($run) == 0) {
                $JSON = array("ErrorCode" => "EC:01AA", "ErrorMessage" => 'Code verfication is not found!');
                return $JSON;
                exit();
            }
        }

        $params2['EMAIL'] = $EMAIL;
        $Q2 = $this->db->get_where("MS_USERS", $params2);
        $run2 = $Q2->row_array();
        if (sizeof($run2) == 0) {
            $JSON = array("ErrorCode" => "EC:01AB", "ErrorMessage" => 'Your account is not found!');
            return $JSON;
            exit();
        }

        if (!empty($OVERVIEW) || $OVERVIEW == "YES") {
            $PASSWORD_DB = $run2['PASSWORD'];
            if ($PASSWORD_DB != md5($PASSWORD)) {
                $JSON = array("ErrorCode" => "EC:01AB", "ErrorMessage" => '');
                return $JSON;
                exit();
            }
        }
        $hash = $this->generateRandomString();
        $password = md5($NEW_PASSWORD . $hash);

        $Q3 = "UPDATE MS_USERS SET PASSWORD='" . $password . "', HASH='" . $hash . "', LAST_UPDATE_PASSWORD='" . date('Y-m-d H:i:s') . "' WHERE EMAIL = '" . $EMAIL . "'";
        $run3 = $this->db->query($Q3);
        if (!$run3) {
            $JSON = array("ErrorCode" => "EC:01AD", "ErrorMessage" => $this->db->error());
            return $JSON;
            exit();
        }

        if (empty($OVERVIEW) || $OVERVIEW != "YES") {
            $Q4 = "UPDATE REQUEST_PASSWORD SET VERIFICATION='1' WHERE EMAIL = '" . $EMAIL . "' AND CODE_VERIFICATION='" . $CODE_VERIFICATION . "'";
            $run4 = $this->db->query($Q4);
            if (!$run4) {
                $JSON = array("ErrorCode" => "EC:01AE", "ErrorMessage" => $this->db->error());
                return $JSON;
                exit();
            }
        }

        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => 'Your password has been successfully changed!');
        return $JSON;
    }
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}