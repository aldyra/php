<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobvacancy extends MY_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Jobvacancy_model');
    }

    public function index()
	{
		$post = array();

		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		if($SiteLang == "id"){
        	$control = $this->config->item('url_jobvacancy_id');
        }elseif($SiteLang == "en"){
        	$control = $this->config->item('url_jobvacancy_en');
        }

		$GetModule = $this->Module_model->getmodule($control);
		$MENU_CHILD_FILE_BANNER = isset($GetModule['FILE_BANNER']) ? $GetModule['FILE_BANNER']:"";
		$content['MENU_CHILD_FILE_BANNER'] = $MENU_CHILD_FILE_BANNER;
		$post['content'] = $content;
		
		$GetJobVacancy = $this->Jobvacancy_model->jobvacancy();
		$post['jobvacancy'] = $GetJobVacancy;

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['Content'] = "jobvacancy";
		$this->data['data'] = $post;

		$this->load->view('template', $this->data);
	}

	public function position($param, $param2='')
	{
		if(empty($param2)){
			$param2 = 1;
		}

		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		$code = isset($_GET['code']) ? $_GET['code']:"";
		$post = array();

		if($SiteLang == "id"){
        	$control = $this->config->item('url_jobvacancy_id');
        }elseif($SiteLang == "en"){
        	$control = $this->config->item('url_jobvacancy_en');
        }

		$GetModule = $this->Module_model->getmodule($control);
		$MENU_CHILD_FILE_BANNER = isset($GetModule['FILE_BANNER']) ? $GetModule['FILE_BANNER']:"";
		$this->data['MENU_CHILD_FILE_BANNER'] = $MENU_CHILD_FILE_BANNER;

		$getgroup = $this->Jobvacancy_model->getgroup();
		$id = getIdFromSlug($param, 'NAME', $getgroup);

		if($id){
			$this->data['Content'] = "jobvacancy_list";
		}else{
			$this->data['Content'] = "notfound";
		}

		$GetJobVacancy = $this->Jobvacancy_model->jobvacancy();

		$paging['ID'] = $id;
		$GetTotal = $this->Jobvacancy_model->gettotal($paging);
		// echo sizeof($GetTotal); die();

        $per_page = isset($_SESSION['pagination']['jobvacancy']) ? $_SESSION['pagination']['jobvacancy']:10;
        $page = isset($param2) ? $param2:1;
        $start = ($page - 1) * $per_page;

        $paging['START'] = $start;
        $paging['PERPAGE'] = $per_page;
        
        $post['data'] = $GetJobVacancy;
        $GetDetail = $this->Jobvacancy_model->joblistvacancy($paging);
        $total = sizeof($GetTotal);
        $pages = ceil($total / $per_page);
        $Urlpaging = base_url().$SiteLang."/".$control."/".$this->lang->line('lowker-0')."/".$param.'/';
        
        $post['urlpaging'] = $Urlpaging;
        $post['pages'] = $pages;
        $post['pages_active'] = $param2;
        $post['jobvacancy'] = $GetDetail;
        $post['total'] = sizeof($GetTotal);
        $post['start'] = $start;
        $post['ID_GROUP_JOBVACANCY'] = $id;

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['data'] = $post;
		$this->load->view('template', $this->data);
	}

	public function detail($param)
	{
		$SiteLang = isset($_SESSION['site_lang']) ? $_SESSION['site_lang']:"";
		
		$post = array();

		if($SiteLang == "id"){
        	$control = $this->config->item('url_jobvacancy_id');
        }elseif($SiteLang == "en"){
        	$control = $this->config->item('url_jobvacancy_en');
        }

		$GetModule = $this->Module_model->getmodule($control);
		$MENU_CHILD_FILE_BANNER = isset($GetModule['FILE_BANNER']) ? $GetModule['FILE_BANNER']:"";
		$this->data['MENU_CHILD_FILE_BANNER'] = $MENU_CHILD_FILE_BANNER;

		$getid = $this->Jobvacancy_model->getjobvacancy();
		$id = getIdFromSlug($param, 'CODE', $getid);

		if($id){
			$getjob = $this->Jobvacancy_model->getjobvacancy($id);
			$post['jobvacancy'] = $getjob;
			$this->data['Content'] = "jobvacancy_detail";
		}else{
			$this->data['Content'] = "notfound";
		}

		$this->data['cssName'] = "Css/public.css:Css/content.css";	
		$this->data['jsName'] = "JavaScript/public.js:JavaScript/content.js";
		$this->data['data'] = $post;
		$this->load->view('template', $this->data);
	}
}