<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_banner extends MY_Controller {

    protected $image_input_name = 'image';
    protected $modul_file       = 'home_banner';
    protected $_wt              = 70;
    protected $_ht              = 0;

    function __construct()
    {
        // die('maintenance..');
        parent::__construct();
        $this->load->model('homebanner_model');        
        $this->load->library('joy_image');
    }
    
    public function index()
    {
        $this->load->view('home_banner/view', $this->data); 
    }

    function get_data(){
            // header('Content-Type: application/json');

            // $col = 'ID,TITLE,UPLOAD_FILE,CREATED_DATE';
            $col = implode(',', $this->input->post('arr', TRUE));
            $getAction = $this->data['authorize'];
            $res = $this->homebanner_model->select_col($col, $getAction);
            $tes = json_decode($res);

            foreach ($tes->data as $row) {
                // convert date
                $row->CREATED_DATE = convertDate($row->CREATED_DATE);
                ($row->DATE_LOG != NULL) ? $row->DATE_LOG = convertDate($row->DATE_LOG) : $row->DATE_LOG = '';
                // convert date

                // convert lang and status
                ($row->LANGUAGE == 'eng') ? $row->LANGUAGE = 'English' : $row->LANGUAGE = 'Indonesia';
                switch ($row->STATUS) {
                    case '0':
                        $row->STATUS = 'Inactive';
                        $row->ACTION = str_replace(array('$icon', '$status'), array('icon-check', 'Active'), $row->ACTION);
                        break;
                    case '88':
                        $row->STATUS = 'Active';
                        $row->ACTION = str_replace(array('$icon', '$status'), array('icon-cross', 'Inactive'), $row->ACTION);
                        break;
                    case '99':
                        $row->STATUS = 'Publish';
                        $row->ACTION = str_replace(array('$icon', '$status'), array('icon-cross', 'Inactive'), $row->ACTION);
                        break;
                }
                // convert lang and status

                $thumb_img = str_replace('/', '/thumb_', $row->UPLOAD_FILE);
                if ($row->UPLOAD_FILE == 'no-image.png')
                    $row->IMAGE = str_replace(array('$folder', '$image', '$link'), array($row->UPLOAD_FILE, '', ''), $row->IMAGE);
                else
                    $row->IMAGE = str_replace(array('$folder', '$image', '$link'), array($this->modul_file, '/'.$thumb_img, '/'.$row->UPLOAD_FILE), $row->IMAGE);  
            }

            echo json_encode($tes);
    }

    function get_publish(){
        header('Content-Type: application/json');

            // $col = 'ID,TITLE,UPLOAD_FILE,CREATED_DATE';
            $col = implode(',', $this->input->post('arr', TRUE));
            $where = array('STATUS' => '88');
            $getAction = $this->data['authorize'];
            $res = $this->homebanner_model->select_col($col, $getAction, $where, TRUE);
            $tes = json_decode($res);

            foreach ($tes->data as $row) {
                ($row->LANGUAGE == 'eng') ? $row->LANGUAGE = 'English' : $row->LANGUAGE = 'Indonesia';
                switch ($row->STATUS) {
                    case '0':
                        $row->STATUS = 'Inactive';
                        break;
                    case '88':
                        $row->STATUS = 'Active';
                        break;
                    case '99':
                        $row->STATUS = 'Publish';
                        break;
                }

                $thumb_img = str_replace('/', '/thumb_', $row->UPLOAD_FILE);
                if ($row->UPLOAD_FILE == 'no-image.png')
                    $row->IMAGE = str_replace(array('$folder', '$image', '$link'), array($row->UPLOAD_FILE, '', ''), $row->IMAGE);
                else
                    $row->IMAGE = str_replace(array('$folder', '$image', '$link'), array($this->modul_file, '/'.$thumb_img, '/'.$row->UPLOAD_FILE), $row->IMAGE);  
            }

            echo json_encode($tes);
    }

    function get_edit($id){
            $res = $this->homebanner_model->get($id);
            echo json_encode($res);
    }

    function valid_dimensions($files){
        if (!empty($files['name']))
        {
            $fileinfo = @getimagesize($files['tmp_name']);
            $width = $fileinfo[0];
            $height = $fileinfo[1];

            if (($width <= 1700) && ($height <= 750))
                return TRUE;
            else
                return FALSE;
        }
    }

    function create_action(){
        if ($this->data['authorize']['is_create']) {
            $post = $this->input->post(NULL, TRUE);         
            $cek = $this->homebanner_model->cekDuplicate($post['title']);

            $this->form_validation->set_rules($this->homebanner_model->rules);
            // $this->form_validation->set_rules('link', 'Link', 'required');
            if ($this->form_validation->run() == FALSE) {
                $res = "Failed: ".validation_errors();
            }
            else if(!_linkRegex($post{'link'})){
                $res = "Failed: Invalid URL!";
            }
            else{
                if($cek){
                    if (!$this->valid_dimensions($_FILES[$this->image_input_name])) 
                    {
                        $res = "Failed: The image exceeds the maximum dimensions.";
                    }else{
                        $upload_image = $this->joy_image->upload_image(
                        $this->modul_file, $this->image_input_name, title_url($post['title']), $this->_wt, $this->_ht
                        );
                        if (isset($upload_image['err_message'])) {
                            $res = 'Failed: '.$upload_image['err_message'];
                        }else{
                            $file_name = date('Y-m').'/'.$upload_image['image']['file_name'];
                            $datainsert = array(
                                'TITLE'        => $post['title'],
                                'UPLOAD_FILE'  => $file_name,
                                'LANGUAGE'     => $post['language'],
                                'LINK'         => $post['link'],
                                'CREATED_DATE' => date('Y-m-d H:i:s'),
                                'CREATED_BY'   => $_SESSION['username'],
                                'IP_LOG'       => $_SERVER['REMOTE_ADDR']
                            );

                            // insert home banner table
                            $insBanner = $this->homebanner_model->insert($datainsert);
                            $insBanner ? $res='Success!' : $res='Failed';
                        }
                    }
                }
                else{
                    $res = 'Failed: Duplicate Title';
                }
            }
        }   
        else{
            $res = 'Failed: You do not have access to data creation.';
        }

        echo json_encode($res);
    }

    function update_action(){
        if ($this->data['authorize']['is_update']) {
            $post = $this->input->post(NULL, TRUE);
            $id   = $post['IDHomeBanner'];

            $this->form_validation->set_rules($this->homebanner_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $res = "Failed: ".validation_errors();
            }
            else if(!_linkRegex($post{'link'})){
                $res = "Failed: Invalid URL!";
            }
            else{
                // jika input file diisi
                if (!empty($_FILES['image']['name'])) 
                {
                    $cek = $this->homebanner_model->cekDuplicate($post['title'], $id);                
                    if($cek){
                        // upload image baru
                        if (!$this->valid_dimensions($_FILES[$this->image_input_name])) 
                        {
                            $res = "Failed: The image exceeds the maximum dimensions.";
                        }else{
                            // upload image baru
                            $upload_image = $this->joy_image->upload_image($this->modul_file, $this->image_input_name, title_url($post['title']), $this->_wt, $this->_ht
                            );
                            if (isset($upload_image['err_message'])) 
                            {
                                $res = 'Failed: '.$upload_image['err_message'];
                            }else{
                                // delete image sebelumnya
                                $get_image = $this->homebanner_model->get($id);
                                $this->joy_image->delete_image($this->modul_file, $get_image->UPLOAD_FILE, 'thumb_');
                                
                                $file_name = date('Y-m').'/'.$upload_image['image']['file_name'];
                                $dataupdate = array(
                                    'TITLE'       => $this->input->post('title', TRUE),
                                    'LANGUAGE'    => $post['language'],
                                    'LINK'        => $post['link'],
                                    'UPLOAD_FILE' => $file_name,
                                    'DATE_LOG'    => date('Y-m-d H:i:s'),
                                    'USER_LOG'    => $_SESSION['username'],
                                    'IP_LOG'      => $_SERVER['REMOTE_ADDR']
                                );
                                $this->homebanner_model->update($dataupdate, array('ID' => $id));
                                $res = 'Success!';
                            }
                        }
                    }
                    else{
                        $res = 'Failed: Duplicate Name';
                    }
                }
                else
                {
                    $cek = $this->homebanner_model->cekDuplicate($post['title'], $id);                
                    if($cek){
                        $dataupdate = array(
                            'TITLE'    => $post['title'],
                            'LANGUAGE' => $post['language'],
                            'LINK'     => $post['link'],
                            'DATE_LOG' => date('Y-m-d H:i:s'),
                            'USER_LOG' => $_SESSION['username'],
                            'IP_LOG'   => $_SERVER['REMOTE_ADDR']
                        );
                        $this->homebanner_model->update($dataupdate, array('ID' => $id));
                        $res = 'Success!';
                    }
                    else{
                        $res = 'Failed: Duplicate Name';
                    }
                }
            }
        }
        else{
            $res = 'Failed: You do not have access to update data.';
        }

        echo json_encode($res);
        
    }

    function statusChg($id)
    {
        if ($this->data['authorize']['is_update']) {
            $array_id = array('ID' => $id);
            $get_data = $this->homebanner_model->get($id);

            if ($get_data->STATUS == '88' || $get_data->STATUS == '99') {
                $array_data = array(
                    'STATUS'      => '0',
                    'DATE_LOG'    => date('Y-m-d H:i:s'),
                    'USER_LOG'    => $_SESSION['username'],
                    'IP_LOG'      => $_SERVER['REMOTE_ADDR']
                );
                $res = 'Success: Status deactivated';
            }

            else {
                $array_data = array(
                    'STATUS'      => '88',
                    'DATE_LOG'    => date('Y-m-d H:i:s'),
                    'USER_LOG'    => $_SESSION['username'],
                    'IP_LOG'      => $_SERVER['REMOTE_ADDR']
                );
                $res = 'Success: Status activated!';
            }

            $this->homebanner_model->update($array_data, $array_id);
        }
        else{
            $res = 'Failed: You do not have access to update data.';
        }

        echo json_encode($res);
    }

    function publish()
    {
        if ($this->data['authorize']['is_update']) {
            if($this->input->post('checkbox_pub'))
            {
                $count_id = $this->input->post('checkbox_pub');
                for($count = 0; $count < count($count_id); $count++)
                {
                    // update status publish
                    $id = array('ID' => $count_id[$count]);
                    $dataupdate = array(
                        'STATUS' => '99',
                        'DATE_LOG'    => date('Y-m-d H:i:s'),
                        'USER_LOG'    => $_SESSION['username'],
                        'IP_LOG'      => $_SERVER['REMOTE_ADDR']
                    );
                    $this->homebanner_model->update($dataupdate, $id);
                }
                $res = 'Success: Publish data.';
            }
        }
        else{
            $res = 'Failed: You do not have access to update data.';
        }

        echo json_encode($res);
    }

    function delete_action($id)
    {
        if ($this->data['authorize']['is_delete']) {
            // delete file image
            $get_image = $this->homebanner_model->get($id);
            $this->joy_image->delete_image($this->modul_file, $get_image->UPLOAD_FILE, 'thumb_');

            $res = $this->homebanner_model->delete_row($id);

        }else{
            $res = 'Failed: You do not have access to delete data.';
        }
        echo json_encode($res);
    }

}
