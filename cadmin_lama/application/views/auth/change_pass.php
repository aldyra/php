<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>B2C NAM AIR</title>
	<!-- App favicon -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo.png">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">

	<!-- Sweet Alert css -->
	<link href="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/joy-style.css">

	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url(); ?>assets/js/main/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Sweet Alert -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url(); ?>assets/js/app.js"></script>
	<!-- /theme JS files -->

	<style type="text/css">
		.login-form {
		    width: 25rem;
		}
		.form-control.parsley-error{
			border-color: #ea3a3a;
		}
		ul.parsley-errors-list{
			margin: 5px 0 0;
			padding-left: 15px;
		}
		ul.parsley-errors-list li{
			color: #fd0d0d;
			list-style: none;	
		}
		.swal2-icon.swal2-warning {
		    font-size: 1rem;
		    line-height: normal;
		}
		ul.helper-text {
            padding-left: 15px;
            display: block;
            font-size: 13px;
            color: #808080;
            list-style-type: none;
            margin-bottom: 0;
        }
        li.invalid{
            display: none;
        }
	</style>

</head>

<body>


	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login form -->
				<form id="form_chgPass" class="login-form" method="POST" action="<?php echo base_url('Authentication/actChangePass'); ?>">
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<img class="logo-login" src="<?php echo base_url('assets/images/logo-nam.png') ?>" alt="">
								<h5 class="mb-0">Change Password</h5>
								<span class="d-block text-muted">For security reasons, please change the password.</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" parsley-trigger="change" id="inputPassword32" class="form-control passwordValidator" name="new_pass" placeholder="Password" required="" data-parsley-required-message="Password is required">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
								<ul class="helper-text">
	                                <li class="length invalid" style="color: red;">Must be at least 5 and maximum 8 chars long.</li>
	                                <li class="lowercase invalid" style="color: red;">Must contain a lowercase letter.</li>
	                                <li class="uppercase invalid" style="color:red;">Must contain an uppercase letter.</li>
	                                <li class="numeric invalid" style="color:red;">Must contain a number.</li>
	                                <li class="special invalid" style="color:red;">Must contain a special character.</li>
	                            </ul>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" name="repass" placeholder="Re-Password" data-parsley-equalto="#inputPassword32" id="inputPassword52" required="" data-parsley-required-message="Re-Password is required" data-parsley-equalto-message="Password should be the same.">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Change <i class="icon-circle-right2 ml-2"></i></button>
							</div>
						</div>
					</div>
				</form>
				<!-- /login form -->

			</div>
			<!-- /content area -->


			<!-- OPEN FOOTER -->
			<?php require_once(APPPATH.'views/templates/footer.php'); ?>
			<!-- END FOOTER -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

	<!-- Vendor js -->
	<script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>

	<!-- App js-->
	<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

	<!-- Validation js (Parsleyjs) -->
	<script src="<?php echo base_url(); ?>assets/libs/parsleyjs/parsley.min.js"></script>

	<!-- Sweet Alerts js -->
	<script src="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.js"></script>

	<!-- Sweet alert init js-->
	<script src="<?php echo base_url(); ?>assets/js/pages/sweet-alerts.init.js"></script>

	<script type="text/javascript">
			var validPass = false;
			var helperText = {
					charLength: document.querySelectorAll('.helper-text .length'),
					lowercase: document.querySelectorAll('.helper-text .lowercase'),
					uppercase: document.querySelectorAll('.helper-text .uppercase'),
					numeric: document.querySelectorAll('.helper-text .numeric'),
					special: document.querySelectorAll('.helper-text .special')
				};

			(function(){
				var password = document.getElementById('inputPassword32');
				
				var pattern = {
					charLength: function() {
						if((password.value.length >= 5 ) && (password.value.length <=8)) {
							return true;
						}
					},
					lowercase: function() {
						var regex = /^(?=.*[a-z]).+$/; // Lowercase character pattern

						if( regex.test(password.value) ) {
							return true;
						}
					},
					uppercase: function() {
						var regex = /^(?=.*[A-Z]).+$/; // Uppercase character pattern

						if( regex.test(password.value) ) {
							return true;
						}
					},
					numeric: function(){
						var regex = /^(?=.*[0-9]).+$/; // Special character or number pattern

						if( regex.test(password.value) ) {
							return true;
						}
					},
					special: function() {
						var regex = /^(?=.*[!@#\$%\^&]).+$/; // Special character or number pattern

						if( regex.test(password.value) ) {
							return true;
						}
					}   
				};
				
			 	 password.addEventListener('keyup', function (){
					patternTest( pattern.charLength(), helperText.charLength[0] );
					patternTest( pattern.lowercase(), helperText.lowercase[0] );
					patternTest( pattern.uppercase(), helperText.uppercase[0] );
					patternTest( pattern.numeric(), helperText.numeric[0] );
					patternTest( pattern.special(), helperText.special[0] );
			    
				    if( hasClass(helperText.charLength[0], 'invalid') && hasClass(helperText.lowercase[0], 'invalid') && hasClass(helperText.uppercase[0], 'invalid') && hasClass(helperText.numeric[0], 'invalid') && hasClass(helperText.special[0], 'invalid')) {
						password.style.border='1px solid #ddd';
				    }else {
				     	password.style.border='1px solid red';
				    }
				});
				
				function patternTest(pattern, response) {
					if(pattern) {
				      addClass(response, 'invalid');
				      validPass = true;
				    }
				    else {
				      removeClass(response, 'invalid');
				      validPass = false;
				    }
				}
				
				function addClass(el, className) {
					if (el.classList) {
						el.classList.add(className);
					}
					else {
						el.className += ' ' + className;
					}
				}
				
				function removeClass(el, className) {
					if (el.classList)
							el.classList.remove(className);
						else
							el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
				}
				
				function hasClass(el, className) {
					if (el.classList) {
						// console.log(el.classList);
						return el.classList.contains(className);	
					}
					else {
						new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);	
					}
				}
				
			})();

			$(document).ready(function(){
				$("#form_chgPass").parsley();

					$('#form_chgPass').on('submit',function(e) {
							e.preventDefault();
							if ( ($(this).parsley().isValid()) && (validPass) ) {
									$.ajax({
											url: $(this).attr('action'),
											data:$(this).serialize(),
											dataType : "JSON",
											type:'POST',
											success:function(data){
													if((data.indexOf("Failed") != -1)){
														Swal.fire({type: 'error', title: data});
														document.getElementById("form_chgPass").reset();
													}else{
														Swal.fire({type: 'success', title: data}).then(function(){ window.location.href = "<?php echo base_url('authentication'); ?>"; });
													}
													
											},
											error:function(jqXHR, textStatus, errorThrown){
													Swal.fire(textStatus);
											}
									});
							}
					});

			});
	</script>
</body>

</html>
