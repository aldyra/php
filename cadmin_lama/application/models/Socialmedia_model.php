<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Socialmedia_model extends MY_Model
{

    protected $_table_name = 'MS_SOCIAL_MEDIA';
    protected $_primary_key = 'ID_SOCIAL_MEDIA';
    protected $_order_by = 'ID_SOCIAL_MEDIA';
    protected $_order_by_type = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function add($param)
    {
        $Insert = $this->db->insert($this->_table_name, $param);
        if (!$Insert) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function view()
    {
        $this->db->order_by($this->_order_by, $this->_order_by_type);
        $view = $this->db->get($this->_table_name);
        return $view->result_array();
    }

    public function updateaction($param)
    {
        $ID_SOCIAL_MEDIA = isset($param['ID_SOCIAL_MEDIA']) ? $param['ID_SOCIAL_MEDIA'] : "";
        $WHERE['ID_SOCIAL_MEDIA'] = $ID_SOCIAL_MEDIA;
        $run = $this->db->where($WHERE)->update($this->_table_name, $param);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }

    public function removeaction($param)
    {
        $WHERE['ID_SOCIAL_MEDIA'] = isset($param['ID_SOCIAL_MEDIA']) ? $param['ID_SOCIAL_MEDIA'] : "";
        $run = $this->db->where($WHERE)->delete($this->_table_name);
        if (!$run) {
            $JSON = array("ErrorCode" => "EC:00AA", "ErrorMessage" => json_encode($this->db->error()));
            return $JSON;
        }
        $JSON = array("ErrorCode" => "EC:0000", "ErrorMessage" => "Success");
        return $JSON;
    }
}