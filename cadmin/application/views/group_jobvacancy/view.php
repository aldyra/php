<?php
$Controllers = $this->uri->segment(1);
$paramadd = array();
$Sendparam = base64_encode(json_encode($paramadd));
$action = base_url('group_jobvacancy/add');

$is_create = $authorize['is_create'];
$is_update = $authorize['is_update'];
$is_delete = $authorize['is_delete'];

$Data = isset($Data) ? $Data : "";
$GroupJobVacancy = isset($Data['group_jobvacancy']) ? $Data['group_jobvacancy'] : array();
?>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Career</a>
                <a href="#" class="breadcrumb-item active">Group Job Vacancy</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <?php if ($is_create): ?>
        <div class="card-body">
            <button type="button" class="btn btn-info" id="btnAdd"
                onclick="modal('add', '', 'Add Group Job Vacancy', '<?php echo $Controllers ?>/form_add', '<?php echo $Sendparam ?>', '<?php echo $action ?>')">
                Add Group Job Vacancy
            </button>
        </div>
        <?php endif;?>

        <table id="tableData" class="table datatable-complex-header">
            <thead>
                <tr>
                    <th>Name</th>
                    <th data-orderable="false">Language</th>
                    <th>Created By</th>
                    <th data-orderable="false"  width="200">Created Date</th>
                    <th data-orderable="false">Status</th>
                    <th>sts</th>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <th data-orderable="false">Action</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($GroupJobVacancy as $group_jobvacancy) {
    $ID = isset($group_jobvacancy['ID']) ? $group_jobvacancy['ID'] : "";
    $NAME = isset($group_jobvacancy['NAME']) ? $group_jobvacancy['NAME'] : "";
    $LANGUAGE = isset($group_jobvacancy['LANGUAGE']) ? $group_jobvacancy['LANGUAGE'] : "";
    $STATUS = isset($group_jobvacancy['STATUS']) ? $group_jobvacancy['STATUS'] : "";
    $CREATED_BY = isset($group_jobvacancy['CREATED_BY']) ? $group_jobvacancy['CREATED_BY'] : "";
    $CREATED_DATE = isset($group_jobvacancy['CREATED_DATE']) ? $group_jobvacancy['CREATED_DATE'] : "";
    $USER_LOG = isset($group_jobvacancy['USER_LOG']) ? $group_jobvacancy['USER_LOG'] : "";
    $DATE_LOG = isset($group_jobvacancy['DATE_LOG']) ? $group_jobvacancy['DATE_LOG'] : "";

    if ($STATUS == "0") {
        $STS = "<span class='badge badge-danger'>Inactive</span>";
    } elseif ($STATUS == "88") {
        $STS = "<span class='badge badge-success'>Active</span>";
    } elseif ($STATUS == "99") {
        $STS = "<span class='badge badge-primary'>Publish</span>";
    } else {
        $STS = "";
    }
    if ($LANGUAGE == "EN") {
        $LANGUAGE = "English";
    } elseif ($LANGUAGE == "ID") {
        $LANGUAGE = "Indonesia";
    }

    ?>
                <tr>

                    <td>
                        <?php echo $NAME ?>
                    </td>
                    <td>
                        <?php echo $LANGUAGE ?>

                    </td>
                    <td>
                        <?php echo $CREATED_BY ?>
                    </td>
                    <td>
                        <?php echo convertDate($CREATED_DATE) ?>
                    </td>
                    <td>
                        <?php echo $STS ?>
                    </td>
                    <td>
                        <?php echo $STATUS ?>
                    </td>
                    <?php if (!empty($is_update) || !empty($is_delete)) {?>
                    <td style="text-align: center;">
                        <?php $paramaction['authorize'] = $authorize;
        $param['Controllers'] = $Controllers;
        $param['Url'] = base_url() . $Controllers;
        $param['Data'] = $group_jobvacancy;
        $param['Modal'] = "";
        $enc = base64_encode(json_encode($param));
        ?>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <?php if (!empty($is_update)) {?>
                                    <a onclick="updateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-pencil"></i> Edit</a>

                                    <?php if ($STATUS == "0") {?>

                                    <a onclick="activateaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-on"></i>
                                        Activate</a>

                                    <?}elseif($STATUS == "88"){?>
                                    <a onclick="publishaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-move-up"></i> Publish</a>
                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?}elseif($STATUS == "99"){?>

                                    <a onclick="deactiveaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="fas fa-toggle-off"></i>
                                        Deactive</a>

                                    <?php }?>
                                    <?php }?>
                                    <!-- <?php if (!empty($is_delete)) {?> -->
                                    <!-- <?php if ($STATUS == "0") {?>

                                    <a onclick="deleteaction('<?php echo $enc ?>')" class="dropdown-item"><i
                                            class="icon-trash"></i> Delete</a>
                                    <?php }}?> -->
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php }?>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/table.js"></script>
<script type="text/javascript">
t.column(5).visible(false);

function changeSelectStatus(dis) {
    if (dis.value != "" && dis.value != null) {
        t.column(5)
            .search(dis.value)
            .draw();
    } else {
        t.column(5)
            .search(dis.value)
            .draw();
    }

}

function changeSelectDate(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(3)
            .search(dis.value)
            .draw();
    } else {
        t.column(3)
            .search(dis.value)
            .draw();
    }

}

function changeSelectLang(dis) {

    if (dis.value != "" && dis.value != null) {
        t.column(1)
            .search(dis.value)
            .draw();
    } else {
        t.column(1)
            .search(dis.value)
            .draw();
    }

}
</script>