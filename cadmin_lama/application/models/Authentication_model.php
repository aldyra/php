<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Authentication_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    protected $_table_name = 'MS_USERS';
    protected $_primary_key = 'ID';
    protected $_order_by = 'ID';
    protected $_order_by_type = 'DESC';

    public function AuthLogin($uname = null, $pass = null)
    {
        $this->db->where(array('USERNAME' => $uname, 'PASSWORD' => $pass));
        $data = $this->db->get('MS_USERS');

        if ($data->num_rows() <= 0) {
            $res = false;
        } else {
            $res = $data->result();
        }

        return $res;
    }

    public function getPrivilege2($uname = null, $pass = null)
    {
        $queryGet = "SELECT
            c.MODULE_NAME as MODULE_NAME,
            b.IS_CREATE as IS_CREATE,
            b.IS_READ as IS_READ,
            b.IS_UPDATE as IS_UPDATE,
            b.IS_DELETE as IS_DELETE,
            b.IS_EXPORT as IS_EXPORT,
            b.IS_IMPORT as IS_IMPORT
        FROM MS_USERS a
        JOIN MS_USER_GROUP_PREVILEGE b
        ON a.GROUP_ID = b.USER_GROUP_ID
        JOIN MS_MODULE_PREVILEGE c
        ON b.MODULE_PREVILEGE_ID = c.ID
        WHERE USERNAME='" . $uname . "' AND PASSWORD='" . $pass . "'";

        return $this->db->query($queryGet);
    }

    public function getPrivilege($uname = null, $pass = null)
    {
        $queryGet = "SELECT
        C.ID,
        MODULE_NAME,
        USERNAME,
        D.ID AS USER_ID,
        MENU_LEVEL,
        MENU_PARENT,
        MENU_SEQ,
        A.IS_READ, A.IS_CREATE, A.IS_DELETE, A.IS_UPDATE, A.IS_EXPORT, A.IS_IMPORT,
        C.CONTROLLER, C.FUNCTION, C.ICON
                    FROM MS_USER_GROUP_PREVILEGE A
                    JOIN MS_USER_GROUP B ON A.USER_GROUP_ID = B.ID
                    JOIN MS_MODULE_PREVILEGE C ON A.MODULE_PREVILEGE_ID = C.ID
                    JOIN MS_USERS D ON B.ID = D.GROUP_ID
                    WHERE (D.USERNAME = '" . $uname . "' AND D.PASSWORD = '" . $pass . "')
                    AND IS_READ != '0'
                    ORDER BY MENU_SEQ ASC";

        return $this->db->query($queryGet);
    }

}