<!-- Theme JS files -->
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
<!-- <script src="<?php echo base_url() ?>/assets/js/plugins/forms/styling/uniform.min.js"></script> -->
<!-- <script src="<?php echo base_url() ?>/assets/js/demo_pages/form_layouts.js"></script> -->
<!-- <script src="<?php echo base_url() ?>/assets/js/demo_pages/form_inputs.js"></script> -->
<!-- /theme JS files -->
<?php
$is_update = $authorize['is_update'];
$back = isset($data['back']) ? $data['back'] : '';
$giro_ir = isset($data['giro_ir']) ? $data['giro_ir'] : array();
$DETAIL = isset($data['detail']) ? $data['detail'] : array();

$ID_GIRO_IR_HEADER = isset($giro_ir->ID_GIRO_IR_HEADER) ? $giro_ir->ID_GIRO_IR_HEADER : "";
$CATEGORY_GIRO = isset($giro_ir->CATEGORY_GIRO) ? $giro_ir->CATEGORY_GIRO : "";
$EFFECTIVE_DATE = isset($giro_ir->EFFECTIVE_DATE) ? date('d M Y', strtotime($giro_ir->EFFECTIVE_DATE)) : "";

?>
<style>
.center {
    display: flex;
    justify-content: center;
    align-items: center;
}

.dropify-wrapper {
    height: 100%;
}
</style>
<!-- page headder -->
<div class="page-header page-header-light">
    <div class="breadcrumb-line breadcrumb-line-dark bg-blue-800 header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="<?php echo base_url() ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="#" class="breadcrumb-item">Master</a>
                <a href="#" class="breadcrumb-item">Interest Rate</a>
                <a href="<?php echo $back ?>" class="breadcrumb-item">Giro Interest Rate</a>
                <a class="breadcrumb-item active">Detail</a>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Giro Interest Rate</span> -
                Detail
            </h5>
        </div>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <?php if ($is_update): ?>
    <div class="card">
        <form class="parsleyy" data-parsley-validate="" novalidate="" method="post" id="frmsaveGiroIR">
            <div class="card-header bg-blue-800 text-white d-flex justify-content-between">
                <input type="hidden" name="id_giro_ir_header" id="id_giro_ir_header"
                    value="<?php echo $ID_GIRO_IR_HEADER ?>">
                <input type="hidden" name="listDelete" id="listDelete" value="">
                <?php $GIRO_ID = $this->config->item('GIRO_ID');
$GIRO_EN = $this->config->item('GIRO_EN');
$ID_CATEG_GIRO = $this->config->item('ID_CATEG_GIRO');
$Exp_GIRO_ID = explode(":", $GIRO_ID);
$Exp_GIRO_EN = explode(":", $GIRO_EN);
$Exp_ID_CATEG_GIRO = explode(":", $ID_CATEG_GIRO);
$DESC_EN = '';
$DESC_ID = '';

for ($i = 0; $i < sizeof($Exp_ID_CATEG_GIRO); $i++) {
    $ID = $Exp_ID_CATEG_GIRO[$i];

    if ($CATEGORY_GIRO == $ID) {
        $DESC_EN = $Exp_GIRO_EN[$i];
        $DESC_ID = $Exp_GIRO_ID[$i];
    }
}?>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Description Indonesian</label>
                        <h5><?php echo $DESC_ID ?></h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Description English</label>
                        <h5><?php echo $DESC_EN ?></h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Effective Date</label>
                        <h5><?php echo $EFFECTIVE_DATE ?></h5>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <?php if (!empty($DETAIL)) {?>
                <div id="form_detail">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="workhistory">
                            <thead>
                                <tr>
                                    <th>Balance Indonesia</th>
                                    <th>Balance English</th>
                                    <th>Rate</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($DETAIL as $row) {?>

                                <tr>
                                    <td><?php echo $row->BALANCE_ID ?></td>
                                    <td><?php echo $row->BALANCE_EN ?></td>
                                    <td><?php echo $row->RATE ?></td>
                                </tr>
                                <?php }?>


                            </tbody>
                        </table>
                    </div>

                </div>
                <?php }?>
            </div>

            <div class="card-footer bg-transparent d-flex justify-content-between border-top-0 pt-0">
                <div class="text-right">
                    <a href="<?php echo $back ?>"><button type="button"
                            class="btn btn-secondary waves-effect waves-light">
                            Back</button></a>
                </div>
            </div>
        </form>
    </div>


    <?php endif;?>


</div>