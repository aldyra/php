<?php
$param = isset($param) ? $param : array();
$ID = isset($param['ID']) ? $param['ID'] : "";
$USER_GROUP_NAME = isset($param['USER_GROUP_NAME']) ? $param['USER_GROUP_NAME'] : "";
?>
<div class="form-group row">
    <input id="IDUserGroup" type="hidden" name="IDUserGroup" value="<?php echo $ID ?>">
    <label for="user_group_name" class="col-sm-3 col-form-label">User Group Name<span
            style="color:orange">*</span></label>
    <div class="col-sm-9">
        <input id="userGroupNameEdit" type="text" name="user_group_name" required="" pattern="/^([a-zA-Z0-9 ])+$/"
            data-parsley-maxlength="25" data-parsley-minlength="2"
            data-parsley-required-message="User Group Name is required."
            data-parsley-pattern-message="User Group Name cannot contain any special characters."
            data-parsley-maxlength-message="User Group Name is too long. It should have 25 characters or fewer."
            data-parsley-maxlength-message="User Group Name is too short. It should have 2 characters or more."
            placeholder="Enter User Group Name" class="form-control" value="<?php echo $USER_GROUP_NAME ?>">
    </div>
</div>